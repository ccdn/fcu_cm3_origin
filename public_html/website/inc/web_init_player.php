<?php
  header('Content-Type:text/html;charset=utf-8');
  //版本限制 5.3.0以上
  if (version_compare(PHP_VERSION, '5.3.0', 'lt')) {
    header(sprintf("%s 500 %s", $_SERVER['SERVER_PROTOCOL'], 'VERSION_ERROR'));
    die('PHP 5.3.0+ is required');
  }
  
  error_reporting(E_ALL);
  ini_set('display_errors', 'on');
  //載入設定值Class
  require(F_ROOT . 'lib/class/CM/Conf.php');
  //載入設定檔案
  require(F_ROOT . 'lib/inc/conf.php');
  //初始化載入
  require(CM_Conf::get('PATH_F.LIB') . 'inc/init.php');
  //呼叫控制器
  require(CM_Conf::get('PATH_F.ROOT') . 'website/inc/web_init.php');
  //載入核心類別
  //require(CM_Conf::get('PATH_F.LIB_CLASS') . 'CM.php');
  //載入設定檔案
  foreach (new DirectoryIterator(CM_Conf::get('PATH_F.LIB_CONF')) as $file) {
    if($file->isDot()) continue;

    if($file->isDir()) continue;
    //只接受副檔名為php的設定檔
    if(CM::get_file_ext($file->getFilename()) != 'php') continue;

    require($file->getPathname());
  }
  //版本5.3以上必須要先設定地區位置
  if (version_compare(phpversion(), "5.3.0", ">") === true) date_default_timezone_set(CM_Conf::get('DEF.TIME_ZONE'));
  //設定函式庫路徑
  CM::set_class_path(CM_Conf::get('PATH_F.LIB_CLASS'));
  //設定自動載入類別
  spl_autoload_register(array('CM', 'load_class'));
  
  CM_DB::set_database(CM_Conf::get('DB.HOST'), CM_Conf::get('DB.NAME'), CM_Conf::get('DB.USERNAME'), CM_Conf::get('DB.PASSWORD'));
  //設定語系載入資料夾
  CM_Lang::set_lang_path(CM_Conf::get('PATH_F.LIB_LANG'));
  //載入設定檔案
  //require(F_ROOT . 'webmgr/inc/web_conf.php');
  //設定函式庫路徑
  //CM::set_class_path(CM_Conf::get('PATH_F.ADMIN_CLASS'));
  //設定錯誤訊息處理程式
  set_error_handler(array('Admin', 'error_exception_handler'));
  //預設為繁體中文
  CM_Lang::current(CM_Conf::get('DEF.LANG_ADMIN_DEF'));
  //設定語系載入資料夾
  //CM_Lang::set_lang_path(CM_Conf::get('PATH_F.ADMIN_LANG'));
  //設定樣板載入資料夾
  //CM_Template::set_templates_path(CM_Conf::get('PATH_F.ADMIN_VIEW'));
?>