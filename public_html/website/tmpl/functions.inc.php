<?php


function in_arrays($key, $val, $arr, $strict=false)
{
	reset($arr);
	foreach ($arr as $k=>$v) {
		if ($strict===true) {
			if ($v[$key]===$val) {
				return true;
			}
		} else {
			if ($v[$key]==$val) {
				return true;
			}
		}
	}
	return false;
}
function is_email($str)
{
	//$email_regular_expression="^([-!#\$%&'*+./0-9=?A-Z^_`a-z{|}~])+@([-!#\$%&'*+/0-9=?A-Z^_`a-z{|}~]+\\.)+[a-zA-Z]{2,6}\$";
	//$patten="/".str_replace("/", "\\/", $email_regular_expression)."/";
	$patten="/^([-!#\$%&'*+.\\/0-9=?A-Z^_`a-z{|}~])+@([-!#\$%&'*+\\/0-9=?A-Z^_`a-z{|}~]+\\.)+[a-zA-Z]{2,6}\$/";
	return(preg_match($patten,$str));
}
function date2timestamp($str, $t='yyyy-mm-dd')
{
	if (!is_date($str, $t))
		return false;
	
	$sub= preg_split('[/.- :]', $str);
	switch ($t) {
		case 'yyyy-mm-dd':
		case 'yyyy/mm/dd':
		case 'yyyy.mm.dd':
			return @mktime($sub[3], $sub[4], $sub[5], $sub[1], $sub[2], $sub[0]);
		case 'mm-dd-yyyy':
		case 'mm/dd/yyyy':
		case 'mm.dd.yyyy':
			return @mktime($sub[3], $sub[4], $sub[5], $sub[0], $sub[1], $sub[2]);
		case 'dd-mm-yyyy':
		case 'dd/mm/yyyy':
		case 'dd.mm.yyyy':
			return @mktime($sub[3], $sub[4], $sub[5], $sub[1], $sub[0], $sub[2]);
	}
}


function is_date($str, $t='yyyy-mm-dd', $nullValid=false)
{//checkdate ( int month, int day, int year )
	if ($str=='' && $nullValid)
		return true;
	$sub= preg_split('/[\/.-]/', $str);
	if (count($sub)!=3 || strlen($str)>10)
		return false;
	switch ($t) {
		case 'yyyy-mm-dd':
		case 'yyyy/mm/dd':
		case 'yyyy.mm.dd':
			return @checkdate($sub[1], $sub[2], $sub[0]);
		case 'mm-dd-yyyy':
		case 'mm/dd/yyyy':
		case 'mm.dd.yyyy':
			return @checkdate($sub[0], $sub[1], $sub[2]);
		case 'dd-mm-yyyy':
		case 'dd/mm/yyyy':
		case 'dd.mm.yyyy':
			return @checkdate($sub[1], $sub[0], $sub[2]);
	}
}



function file_ext($str, $t='')
{
	$path_parts = pathinfo($str);
	if ($path_parts['extension'] == '')
		return '';
	else
		return $t.$path_parts['extension'];
}
function int2ip($int) {
	$str = '';
	for ($i = 0; $i < 4; $i++) {
		if ($i) $str = '.' . $str;
		$str = ($int & 0x000000FF) . $str;
		$int = $int >> 8;
	};
	return $str;
}

function ip2int($ip)
{
	$m = valid_ip($ip, 'returnMatches');
	if ($m===false)
		return false;
	
	return $m[1]*16777216 + $m[2]*65536 + $m[3]*256 + $m[4];
}
function set_color($str, $color='red')
{
	return '<font color="'.$color.'">'.$str.'</font>';
}
function valid_ip($ip, $t='')
{	//([0-9]{1,3})\.([0-9]{1,3})\.([0-9]{1,3})\.([0-9]{1,3})
	if (preg_match("/^(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$/", $ip, $m)) {
		if ($t=='returnMatches')
			return $m;
		else 
			return true;
	}
	return false;
}
function is_ints($val, $t='', $emptyValid=false)
{
	if ($emptyValid===true && $val=='')
		return true;
	if (!is_numeric($val))
		return false;

	if (!preg_match("@^([-+]?[1-9][0-9]*|0)$@", $val, $m))
		return false;

	switch ($t) {
		case '++'://1,2,...
			return (floatval($val)>0)?true:false;
		case '+'://0,1,2,...
			return (floatval($val)>=0)?true:false;
		case '-'://...,-2,-1
			return (floatval($val)<0)?true:false;
		default://...,-2,-1,0,1,2,...
			return true;
	}
}
function is_floats($val, $t='', $emptyValid=false)
{
	if ($emptyValid===true && $val=='')
		return true;
	if (!is_numeric($val))
		return false;
	if (is_ints($val, $t))
		return true;
	
	if (!preg_match("/^([-+]?[0-9]*\.?[0-9]+)$/", $val))
		return false;

	switch ($t) {
		case '++'://0.=>
			return (floatval($val)>0)?true:false;
		case '+'://0,0.=>
			return (floatval($val)>=0)?true:false;
		case '-'://<=-0.
			return (floatval($val)<0)?true:false;
		default://<=-0., 0, 0.=>
			return true;
	}
}
function toSqlStr($str)
{
	return mysql_real_escape_string($str);
}
function to_js_quotes($str)
{
	return str_replace("'", "\\'", $str);
	//return preg_replace("%(?<!\\\\)'%", "\\'", $str);
}
function alert_back($msg)
{
	echo "<head><meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" />
		<script>
			alert('".str_replace("\n", '', to_js_quotes($msg))."');
			window.history.back();
		</script>";
	exit;
}
function alert_end($msg)
{
	echo "<head><meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" />
		<script>
			alert('".str_replace("\n", '', to_js_quotes($msg))."');
		</script>";
	exit;
}
function alert_close($msg)
{
	echo "<head><meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" />
		<script>
			alert('".str_replace("\n", '', to_js_quotes($msg))."');
			window.top.close();
		</script>";
	exit;
}
function alert($msg)
{
	echo "<head><meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" />
		<script>
			alert('".str_replace("\n", '', to_js_quotes($msg))."');
		</script>";
}
function alert_to($msg, $url, $frame='')
{
	echo "<head><meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" />
		<script>
			alert('".str_replace("\n", "", to_js_quotes($msg))."');
			window.".($frame==''?'':$frame.'.').'location.href = "'.$url.'";
		</script>';
	exit;
}
function alert_go($msg, $url, $frame='')
{
	echo "<head><meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" />
		<script>
			alert('".str_replace("\n", '', to_js_quotes($msg))."');
			window.".($frame==''?'':$frame.'.').'location.href = "'.$url.'";
		</script>';
	exit;
}
function url_to($url, $frame='')
{
	echo "
		<script>
			window.".($frame==''?'':$frame.'.').'location.href = "'.$url.'";
		</script>';
	exit;
}
function close_openerFocus()
{
	echo '
		<script>
			window.opener.focus();
			window.close();
		</script>
	';
	exit;
}
function modeless_close()
{
	echo '
		<script>
			window.dialogArguments.focus();
			self.close();
		</script>
	';
	exit;
}
/* DateAdd
yyyy year年 
q Quarter季 
m Month月 
y Day of year一年的數 
d Day天 
w Weekday一周的天數 
ww Week of year周 
h Hour小時 
n Minute分 
s Second秒 
w、y和d的作用是完全一樣的，即在目前的日期上加一天，q加3個月，ww加7天。 
*/
function DateAdd ($interval, $number, $date) { 
	$date_time_array = getdate($date); 
	$hours = $date_time_array['hours']; 
	$minutes = $date_time_array['minutes']; 
	$seconds = $date_time_array['seconds']; 
	$month = $date_time_array['mon']; 
	$day = $date_time_array['mday']; 
	$year = $date_time_array['year']; 
	switch ($interval) { 
		case 'yyyy': $year +=$number; break; 
		case 'q': $month +=($number*3); break; 
		case 'm': $month +=$number; break; 
		case 'y': 	
		case 'd': 
		case 'w': $day+=$number; break; 
		case 'ww': $day+=($number*7); break; 
		case 'h': $hours+=$number; break; 
		case 'n': $minutes+=$number; break; 
		case 's': $seconds+=$number; break; 
	} 
	$timestamp = mktime($hours, $minutes, $seconds, $month, $day, $year); 
	return $timestamp;
}
/* DateDiff
"w"(周)
"d"（天）
"h"（小時）
"n"（分鐘）
"s"（秒）
*/
//如果有 bcdiv 可把 floor 改為 bcdiv
Function DateDiff ($interval, $date1, $date2) { 
	// 得到兩日期之間間隔的秒數 
	$timedifference = $date2 - $date1; 
	switch ($interval) { 
		case 'w': $retval = floor($timedifference/604800); break; 
		case 'd': $retval = floor($timedifference/86400); break; 
		case 'h': $retval = floor($timedifference/3600); break; 
		case 'n': $retval = floor($timedifference/60); break; 
		case 's': $retval = $timedifference; break; 
	} 
	return $retval;
}
//判斷是否為中文字
function big5_isBig5($c='')
{

  $bc  = hexdec(bin2hex($c));
  if
  (
     ($bc>=0xa440 && $bc<= 0xc67e) ||
     ($bc>=0xc940 && $bc<= 0xf9fe) ||
     ($bc>=0xa140 && $bc<= 0xa3fe) ||
     ($bc>=0xc6a1 && $bc<= 0xc8fe)
  ) return true;

  return false;
}
function mbTruncate($str, $charset, $lang)
{
	return mb_substr(iconv($charset,$lang,$str), 0, 6);
}
function pagination($base_url, $num_items, $rowsPerPage, $offset, $add_prevnext_text=true, $hrefType=1, $funcPager='pager')
{
	$total_pages = ceil($num_items/$rowsPerPage);
	if ($total_pages==1) {
		return '';
	}

	$on_page = floor($offset / $rowsPerPage) + 1;

	$page_string = '';
	if ($hrefType==1) {
		$href = $base_url;
		$ahref = '<a href="%s" onclick="return fix_args(this, \'offset\', \'%s\')"  onkeypress="return fix_args(this, \'offset\', \'%s\')">%s</a>';
	} elseif ($hrefType==2) {
		$href = 'javascript:;';
		$ahref = '<a href="%s" onclick="'.$funcPager.'(\'%s\')"  onkeypress="return fix_args(this, \'offset\', \'%s\')">%s</a>';
	}
	if ( $total_pages > 10 ) {
		$init_page_max = ( $total_pages > 3 ) ? 3 : $total_pages;
		for($i = 1; $i < $init_page_max + 1; $i++) {
			$page_string .= ( $i == $on_page ) ? '<b>' . $i . '</b>' : sprintf($ahref, $href, (($i-1)*$rowsPerPage), $i, $i);
			if ( $i <  $init_page_max ) {
				$page_string .= ', ';
			}
		}

		if ( $total_pages > 3 ) {
			if ( $on_page > 1  && $on_page < $total_pages ) {
				$page_string .= ( $on_page > 5 ) ? ' ... ' : ', ';

				$init_page_min = ( $on_page > 4 ) ? $on_page : 5;
				$init_page_max = ( $on_page < $total_pages - 4 ) ? $on_page : $total_pages - 4;

				for($i = $init_page_min - 1; $i < $init_page_max + 2; $i++) {
					$page_string .= ($i == $on_page) ? '<b>' . $i . '</b>' : sprintf($ahref, $href, (($i-1)*$rowsPerPage), $i, $i);
					if ( $i <  $init_page_max + 1 ) {
						$page_string .= ', ';
					}
				}

				$page_string .= ( $on_page < $total_pages - 4 ) ? ' ... ' : ', ';
			} else {
				$page_string .= ' ... ';
			}

			for($i = $total_pages - 2; $i < $total_pages + 1; $i++) {
				$page_string .= ( $i == $on_page ) ? '<b>' . $i . '</b>'  : sprintf($ahref, $href, (($i-1)*$rowsPerPage), $i, $i);
				if( $i <  $total_pages ) {
					$page_string .= ', ';
				}
			}
		}
	} else {
		for($i = 1; $i <= $total_pages; $i++) {
			$page_string .= ( $i == $on_page ) ? '<b>' . $i . '</b>' : sprintf($ahref, $href, (($i-1)*$rowsPerPage), $i, $i);
			if ( $i <  $total_pages ) {
				$page_string .= ', ';
			}
		}
	}

	if ( $add_prevnext_text ) {
		if ( $on_page > 1 ) {
	//		$page_string = sprintf($ahref, $href, (($on_page-2)*$rowsPerPage), (($on_page-2)*$rowsPerPage), '<img src="images/icon_brfore.png" width="18" height="18" alt="上一頁" />') . '&nbsp;&nbsp;' . $page_string;
			$page_string = sprintf($ahref, $href, (($on_page-2)*$rowsPerPage), (($on_page-2)*$rowsPerPage), '上一頁') . '&nbsp;&nbsp;' . $page_string;
		}

		if ( $on_page < $total_pages ) {
			//$page_string .= '&nbsp;&nbsp;' . sprintf($ahref, $href, ($on_page*$rowsPerPage), ($on_page*$rowsPerPage),'<img src="images/icon_next.png" width="18" height="18" alt="下一頁" />');
			$page_string .= '&nbsp;&nbsp;' . sprintf($ahref, $href, ($on_page*$rowsPerPage), ($on_page*$rowsPerPage),'下一頁');
		}
	}

	if ($hrefType==1) {
		$page_script = '
			<script>
				function chk_form_page_nav(form)
				{
				
					var reg = new RegExp("^[0-9]+$");
					if (!reg.test(form.page.value))
						return false;
					var offset = (form.page.value-1)*'.$rowsPerPage.';
					window.location.href = fix_location("'.$_SERVER['REQUEST_URI'].'", "offset", offset);
					return false;
				}
			</script>';
	} elseif ($hrefType==2) {
		$page_script = '
			<script>
				function chk_form_page_nav(form)
				{
					var reg = new RegExp("^[0-9]+$");
					if (!reg.test(form.page.value))
						return false;
					var offset = (form.page.value-1)*'.$rowsPerPage.';
					'.$funcPager.'(offset);
					return false;
				}
			</script>';
	}
	//*****************terry edit 20080528 start****************
//	$page_form = '<table border="0" cellspacing="0" cellpadding="0"><tr><td class="style2" nowrap>&nbsp;&nbsp;前往頁面 <input name="page" type="text" size="3">&nbsp;</td><td valign="baseline" nowrap><input type="image" align="bottom" src="/images/botton/sure_page.gif" width="49" height="22"></td></tr></table>';
	return '<form action="javascript:;" onsubmit="return chk_form_page_nav(this)"><div class="number">'.$page_string.$page_form.'</div>'.$page_script.'</form>';
	//*****************terry edit 20080528 end******************
}

/*
function pagination($base_url, $num_items, $rowsPerPage, $offset, $add_prevnext_text=true, $hrefType=1, $funcPager='pager')
{
	$total_pages = ceil($num_items/$rowsPerPage);
	if ($total_pages==1) {
		return '';
	}

	$on_page = floor($offset / $rowsPerPage) + 1;

	$page_string = '';
	if ($hrefType==1) {
		$href = $base_url;
		$ahref = '<a href="%s" onclick="return fix_args(this, \'offset\', \'%s\')">%s</a>';
	} elseif ($hrefType==2) {
		$href = 'javascript:;';
		$ahref = '<a href="%s" onclick="'.$funcPager.'(\'%s\')">%s</a>';
	}
	if ( $total_pages > 10 ) {
		$init_page_max = ( $total_pages > 3 ) ? 3 : $total_pages;
		for($i = 1; $i < $init_page_max + 1; $i++) {
			$page_string .= ( $i == $on_page ) ? '<b>' . $i . '</b>' : sprintf($ahref, $href, (($i-1)*$rowsPerPage), $i);
			if ( $i <  $init_page_max ) {
				$page_string .= ', ';
			}
		}

		if ( $total_pages > 3 ) {
			if ( $on_page > 1  && $on_page < $total_pages ) {
				$page_string .= ( $on_page > 5 ) ? ' ... ' : ', ';

				$init_page_min = ( $on_page > 4 ) ? $on_page : 5;
				$init_page_max = ( $on_page < $total_pages - 4 ) ? $on_page : $total_pages - 4;

				for($i = $init_page_min - 1; $i < $init_page_max + 2; $i++) {
					$page_string .= ($i == $on_page) ? '<b>' . $i . '</b>' : sprintf($ahref, $href, (($i-1)*$rowsPerPage), $i);
					if ( $i <  $init_page_max + 1 ) {
						$page_string .= ', ';
					}
				}

				$page_string .= ( $on_page < $total_pages - 4 ) ? ' ... ' : ', ';
			} else {
				$page_string .= ' ... ';
			}

			for($i = $total_pages - 2; $i < $total_pages + 1; $i++) {
				$page_string .= ( $i == $on_page ) ? '<b>' . $i . '</b>'  : sprintf($ahref, $href, (($i-1)*$rowsPerPage), $i);
				if( $i <  $total_pages ) {
					$page_string .= ', ';
				}
			}
		}
	} else {
		for($i = 1; $i <= $total_pages; $i++) {
			$page_string .= ( $i == $on_page ) ? '<b>' . $i . '</b>' : sprintf($ahref, $href, (($i-1)*$rowsPerPage), $i);
			if ( $i <  $total_pages ) {
				$page_string .= ', ';
			}
		}
	}

	if ( $add_prevnext_text ) {
		if ( $on_page > 1 ) {
			$page_string = sprintf($ahref, $href, (($on_page-2)*$rowsPerPage), '上一頁') . '&nbsp;&nbsp;' . $page_string;
		}

		if ( $on_page < $total_pages ) {
			$page_string .= '&nbsp;&nbsp;' . sprintf($ahref, $href, ($on_page*$rowsPerPage), '下一頁');
		}
	}

	if ($hrefType==1) {
		$page_script = '
			<script>
				function chk_form_page_nav(form)
				{
				
					var reg = new RegExp("^[0-9]+$");
					if (!reg.test(form.page.value))
						return false;
					var offset = (form.page.value-1)*'.$rowsPerPage.';
					window.location.href = fix_location("'.$_SERVER['REQUEST_URI'].'", "offset", offset);
					return false;
				}
			</script>';
	} elseif ($hrefType==2) {
		$page_script = '
			<script>
				function chk_form_page_nav(form)
				{
					var reg = new RegExp("^[0-9]+$");
					if (!reg.test(form.page.value))
						return false;
					var offset = (form.page.value-1)*'.$rowsPerPage.';
					'.$funcPager.'(offset);
					return false;
				}
			</script>';
	}
	//*****************terry edit 20080528 start****************
	$page_form = '<table border="0" cellspacing="0" cellpadding="0"><tr><td class="style2" nowrap>&nbsp;&nbsp;前往頁面 <input name="page" type="text" size="3">&nbsp;</td><td valign="baseline" nowrap><input type="image" align="bottom" src="/images/botton/sure_page.gif" width="49" height="22"></td></tr></table>';
	return '<form action="javascript:;" onsubmit="return chk_form_page_nav(this)"><table border="0" cellspacing="0" cellpadding="0"><tr><td nowrap valign="middle">'.$page_string.'</td><td nowrap valign="middle">'.$page_script.$page_form.'</td></tr></table></form>';
	//*****************terry edit 20080528 end******************
}

*/








function ImageResize($from_filename, $save_filename, $in_width=400, $in_height=300, $quality=100)
{
    $allow_format = array('jpeg', 'png', 'gif');
    $sub_name = $t = '';

    // Get new dimensions
    $img_info = getimagesize($from_filename);
    $width    = $img_info['0'];
    $height   = $img_info['1'];
    $imgtype  = $img_info['2'];
    $imgtag   = $img_info['3'];
    $bits     = $img_info['bits'];
    $channels = $img_info['channels'];
    $mime     = $img_info['mime'];

    list($t, $sub_name) = explode('/', $mime);
    if ($sub_name == 'jpg') {
        $sub_name = 'jpeg';
    }

    if (!in_array($sub_name, $allow_format)) {
        return false;
    }

    // 取得縮在此範圍內的比例
    $percent = getResizePercent($width, $height, $in_width, $in_height);
    $new_width  = $width * $percent;
    $new_height = $height * $percent;

    // Resample
    $image_new = imagecreatetruecolor($new_width, $new_height);

    // $function_name: set function name
    //   => imagecreatefromjpeg, imagecreatefrompng, imagecreatefromgif
    /*
    // $sub_name = jpeg, png, gif
    $function_name = 'imagecreatefrom' . $sub_name;

    if ($sub_name=='png')
        return $function_name($image_new, $save_filename, intval($quality / 10 - 1));

    $image = $function_name($filename); //$image = imagecreatefromjpeg($filename);
    */
    $image = imagecreatefromjpeg($from_filename);

    imagecopyresampled($image_new, $image, 0, 0, 0, 0, $new_width, $new_height, $width, $height);

    return imagejpeg($image_new, $save_filename, $quality);
}

/**
 * 抓取要縮圖的比例
 * $source_w : 來源圖片寬度
 * $source_h : 來源圖片高度
 * $inside_w : 縮圖預定寬度
 * $inside_h : 縮圖預定高度
 *
 * Test:
 *   $v = (getResizePercent(1024, 768, 400, 300));
 *   echo 1024 * $v . "\n";
 *   echo  768 * $v . "\n";
 */
function getResizePercent($source_w, $source_h, $inside_w, $inside_h)
{
    if ($source_w < $inside_w && $source_h < $inside_h) {
        return 1; // Percent = 1, 如果都比預計縮圖的小就不用縮
    }

    $w_percent = $inside_w / $source_w;
    $h_percent = $inside_h / $source_h;

    return ($w_percent > $h_percent) ? $h_percent : $w_percent;
}


function pic_click($i_seq)
{
	global $ntl;
	$objResponse = new xajaxResponse();
	try {
		if (!is_numeric($i_seq))
			throw new ntl_Exception($ntl->fatal_error('錯誤的參數'));
		if ($ntl->db_connect()===false)
			throw new ntl_Exception($ntl->fatal_error(NTL_ERROR_UNCONN));
		
		$sql = "UPDATE ".$ntl->vars['TBL']['media']." SET iMda_times_click=iMda_times_click+1 WHERE iMda_seq=".$i_seq;
		
		if (!$ntl->conn->Execute($sql))
			throw new ntl_Exception($ntl->fatal_error($ntl->conn->ErrorMsg(), $sql));
	} catch (ntl_Exception $e) {
		$msg = $e->getMsg();
		$ntl->log_error($e->getFile(), __FUNCTION__, $e->getLine(), $msg, $e->getCode());
		$objResponse->alert($msg['msg']);
	}
		
	return $objResponse;	

		
		
}






?>