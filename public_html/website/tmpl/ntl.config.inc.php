<?php
session_start();

//define('NTL_DOMAIN', 'localhost');
//define('NTL_DIR', 'C:\AppServ\Apache2\htdocs\public_html');
define('NTL_DOMAIN', CM_Conf::get('HTTP.HOST'));
define('NTL_DIR', CM_Conf::get('PATH_F.ROOT'));
//define('NTL_DIR', '/var/www/html');


define('NTL_ROOT_URL', '/');
define('NTL_FILE_LIVE_URL', NTL_ROOT_URL.'files/live/');
define('NTL_FILE_LIVE_PATH', NTL_DIR.NTL_FILE_LIVE_URL);
define('NTL_FILE_NEWS_URL', NTL_ROOT_URL.'files/news/');
define('NTL_FILE_NEWS_PATH', NTL_DIR.NTL_FILE_NEWS_URL);
define('NTL_FILE_MIX_URL', NTL_ROOT_URL.'files/mix/');
define('NTL_FILE_MIX_PATH', NTL_DIR.NTL_FILE_MIX_URL);
define('NTL_FILE_FAQ_URL', NTL_ROOT_URL.'files/faq/');
define('NTL_FILE_FAQ_PATH', NTL_DIR.NTL_FILE_FAQ_URL);

//影音位置
define('NTL_REAL_PATH', '/new_Content/');


define('NTL_REAL_LIVE_LINK_PATH', '/usr/local/real/live_link/');
define('NTL_REAL_LIVE_USERS_PATH', '/usr/local/real/live_users/');
define('NTL_REAL_BROADCAST_PATH', NTL_REAL_LIVE_LINK_PATH);

//影音路徑
define('NTL_FILE_VOD_MOUNT_PATH', 'cmvod/');
//轉檔來源路徑
define('NTL_FILE_VOD_TRANSFER_PATH', 'cmsource/');

//FTP來源路徑
define('NTL_FILE_VOD_FTP_PATH', 'cmftp/');

//jquery批次上傳路徑
define('NTL_FILE_VOD_BATCH_PATH', 'uploads/');
//PIC FTP來源路徑
define('NTL_FILE_PIC_FTP_PATH', 'cmpic/');

define('NTL_FILE_MEDIA_PATH', NTL_REAL_PATH.NTL_FILE_VOD_MOUNT_PATH);

define('NTL_FILE_TRANSFER_PATH', NTL_REAL_PATH.NTL_FILE_VOD_TRANSFER_PATH);

define('NTL_FILE_FTP_PATH', NTL_REAL_PATH.NTL_FILE_VOD_FTP_PATH);
define('NTL_FILE_BATCH_PATH',NTL_REAL_PATH.NTL_FILE_VOD_BATCH_PATH);

define('NTL_PIC_FTP_PATH', NTL_REAL_PATH.NTL_FILE_PIC_FTP_PATH);
define('NTL_FILE_USER_URL', NTL_ROOT_URL.'files/userFiles/');
define('NTL_FILE_MEDIA_URL', NTL_ROOT_URL.'files/mediaFiles/');
define('NTL_IMG_MEDIA_URL', NTL_ROOT_URL.'files/media/');
define('NTL_XAJAX_URL', NTL_ROOT_URL.'website/tmpl/xajax/');
define('SMARTY_DIR', NTL_DIR.NTL_ROOT_URL.'include/Smarty-2.6.18/libs/');

define('NTL_IMG_MEDIA_PIC_CAT_URL', NTL_ROOT_URL.'files/media/pic_cat/');


define('NTL_ERROR_UNCONN', '無法建立資料庫連線');
define('NTL_ERROR_UNDB', '存取資料庫時發生錯誤');
define('NTL_ERROR_AUTHFAIL', '您沒有操作此功能的權限');
define('NTL_ERROR_UNLOGIN', '您尚未登入');
define('NTL_ERROR_ARGS', '參數有誤');

define('RETRIVE_FRAME', 5);

//影片分級
define('MEDIA_DEGREE_GENERAL', 1);
define('MEDIA_DEGREE_PROTECT', 2);
define('MEDIA_DEGREE_GUIDANCE', 3);
define('MEDIA_DEGREE_RESTRICTED', 4);
define('MEDIA_DEGREE_1', '普遍級');
define('MEDIA_DEGREE_2', '保護級');
define('MEDIA_DEGREE_3', '輔導級');
define('MEDIA_DEGREE_4', '限制級');

define('NTL_COL_cMda_name', 1);
define('NTL_COL_iMda_length', 2);
define('NTL_COL_tMda_publish_date', 3);
define('NTL_COL_tMda_est_datetime', 4);
define('NTL_COL_cMda_author', 5);
define('NTL_COL_cMda_publisher', 6);
define('NTL_COL_iMda_degree', 7);
define('NTL_COL_cMda_intro', 8);
define('NTL_COL_cMda_keyword', 9);
define('NTL_COL_iMda_rank', 10);
define('NTL_COL_iMda_cnt_rank', 11);
define('NTL_COL_iMda_cnt_comment', 12);
define('NTL_COL_iMda_times_play', 13);
define('NTL_COL_iMda_type', 14);


define('MEDIA_TYPE_AUDIO', 1);//聲音
define('MEDIA_TYPE_VIDEO', 2);//影片
define('MEDIA_TYPE_AV', 3);//影片+聲音(節目表)
define('MEDIA_TYPE_PIC', 4);//相片
define('MEDIA_TYPE_EAR', 5);//EAR
define('MEDIA_TYPE_FULLHD', 6);//高解析
define('PHP_EXIT', 'index.php');//登入頁
define('MEDIA_TYPE_PHOTO',4);//相片
//8--播放清單
//9--youtube
define('MEDIA_TYPE_MP3',10);//mp3


define('MEDIA_TRANSFER_WAIT', 0);//等待中
define('MEDIA_TRANSFER_PROCESS', 1);//轉檔中
define('MEDIA_TRANSFER_COMPLETE', 2);//轉檔成功
define('MEDIA_TRANSFER_FAIL', 3);//轉檔失敗



define('MEDIA_PIC_TYPE_REPLACE', 'r');//replace=>用類別縮圖代替
define('MEDIA_PIC_TYPE_UPLOAD', 'u');//upload=>上傳圖片
define('MEDIA_PIC_TYPE_CAPTURE', 'c');//capture=>由影片擷取縮圖
define('MEDIA_PIC_TYPE_VCD', 'v');//vcd=>VCD掃瞄的縮圖
define('MEDIA_PIC_TYPE_DVD', 'd');//dvd=>DVD掃瞄的縮圖

define('DISPLAY_LAYOUT_SIMPLE', 1);//簡要模式
define('DISPLAY_LAYOUT_DETAIL', 2);//詳細模式

define('MEDIA_THUMBNAIL_WIDTH', 450);//上傳影片縮圖最大寬
define('MEDIA_THUMBNAIL_HEIGHT', 300);//上傳影片縮圖最大高

define('REPLY_NOTYET', 0);			//尚未回覆
define('REPLY_DONE', 1);				//已回覆

define('VERIFY_NOTYET', 0);
define('VERIFY_PASS', 1);
define('VERIFY_FAIL', 2);

define('ON', 1);
define('OFF', 0);

define('CHANNEL_SUPERVISOR', 	1);//最高權限-Supervisor
define('CHANNEL_LOGIN', 			2);//登出登入
define('CHANNEL_NORMAL', 			3);//一般管理
define('CHANNEL_SYSTEM', 			4);//系統與使用者管理
define('CHANNEL_MEDIA', 			5);//影音管理
define('CHANNEL_LIVE', 				6);//即時節目管理
define('CHANNEL_STATISTICS', 	7);//統計報表
//define('CHANNEL_INTERFACE', 	8);//顯示介面管理

define('AUTH_SUPERVISOR', 								 1);//最高權限-Supervisor
define('AUTH_LOGIN', 											 2);//登出登入-登出登入
define('AUTH_ADM_INSERT', 								 4);//帳號管理-增刪修
define('AUTH_ADM_UPDATE', 								 4);//帳號管理-增刪修
define('AUTH_ADM_DELETE', 								4);//帳號管理-增刪修
define('AUTH_ADM_BROWSE', 								8);//帳號管理-查詢
define('AUTH_ADM_GROUP', 									16);//帳號管理-身份管理
define('AUTH_ADM_DEPT', 								 32);//帳號管理-部門管理
define('AUTH_NEWS_CAT', 								 64);//常見問題管理-類別管理
define('AUTH_NEWS_INSERT', 							 128);//最新消息管理-增刪修
define('AUTH_NEWS_UPDATE', 							128);//最新消息管理-增刪修
define('AUTH_NEWS_DELETE', 							128);//最新消息管理-增刪修
define('AUTH_NEWS_BROWSE', 							256);//最新消息管理-查詢
define('AUTH_FAQ_CAT', 									512);//常見問題管理-類別管理
define('AUTH_FAQ_INSERT', 						 1024);//常見問題管理-增刪修
define('AUTH_FAQ_UPDATE', 						 1024);//常見問題管理-增刪修
define('AUTH_FAQ_DELETE', 						 1024);//常見問題管理-增刪修
define('AUTH_FAQ_BROWSE', 						2048);//常見問題管理-查詢
define('AUTH_USR_GROUP', 							4096);//使用者管理-群組管理
define('AUTH_USR_INSERT', 						8192);//使用者管理-增刪修
define('AUTH_USR_UPDATE', 					 8192);//使用者管理-增刪修
define('AUTH_USR_DELETE', 					 8192);//使用者管理-增刪修
define('AUTH_USR_BROWSE', 					 16384);//使用者管理-查詢
define('AUTH_MEDIA_CAT', 						 32768);//影片管理-類別管理
define('AUTH_MEDIA_INSERT', 				65536);//影音管理-增刪修
define('AUTH_MEDIA_UPDATE', 				65536);//影音管理-增刪修
define('AUTH_MEDIA_DELETE', 				65536);//影音管理-增刪修
define('AUTH_MEDIA_BROWSE', 			 131072);//影片管理-查詢
define('AUTH_COMMENT', 		 				 262144);//評論管理-審核、刪除
define('AUTH_MIX', 								 524288);//友站連結管理-增刪修
define('AUTH_TYPE_VERSION', 								 524288);//型版變更-修
define('AUTH_LIVE', 							1048576);//Helix即時節目管理-增刪修
define('AUTH_CONTACT_US_CAT',			2097152);//聯絡我們-問題類型管理
define('AUTH_CONTACT_US_REPLY', 	4194304);//聯絡我們-回覆
define('AUTH_REPORT', 						8388608);//統計報表
define('AUTH_FLASH_LIVE', 							16777216);//Flash即時節目管理-增刪修
define('AUTH_ACCOUNT_SWITCH', 								 33554432);//帳號切換-修改


//define(USERLOGING,1);	//前端使用者已登入的狀態
//define(IPLOGIN,2);		//前端使用者的ip在館內的狀態


//require(SMARTY_DIR.'Smarty.class.php');
//require(NTL_DIR.NTL_ROOT_URL.'website/tmpl/xajax/xajax_core/xajax.inc.php');
require(NTL_ROOT_URL.'website/tmpl/xajax/xajax_core/xajax.inc.php');
require(NTL_ROOT_URL.'website/tmpl/adodb504a/adodb-exceptions.inc.php');
require(NTL_ROOT_URL.'website/tmpl/adodb504a/adodb.inc.php');
require(NTL_ROOT_URL.'website/tmpl/functions.inc.php');

function __autoload($class_name) {
	require_once NTL_DIR.NTL_ROOT_URL.'include/class.'.$class_name.'.php';
}

//class ntl_base extends Smarty {
class ntl_base{
	const cookie_seperator = '--';
	const cookie_uniqueID = '3^*i#$68O2$tg!35gt';
	
	public function __construct($caching=2, $cache_lifetime=-1) {
		/*$this->Smarty();
		$this->caching = $caching;
		$this->cache_lifetime = $cache_lifetime;//-1 force the cache will never expire
		
		$this->left_delimiter = '{{';
		$this->right_delimiter = '}}';
		
		$this->template_dir = NTL_DIR.NTL_ROOT_URL.'smarty/templates/';
		$this->compile_dir = NTL_DIR.NTL_ROOT_URL.'smarty/templates_c/';
		$this->config_dir = NTL_DIR.NTL_ROOT_URL.'smarty/configs/';
		$this->cache_dir = NTL_DIR.NTL_ROOT_URL.'smarty/cache/';*/
	}
	
	public function setcookie($name, $value='', $expire=false, $path='', $domain='', $secure=false) {
		setcookie($name, $value.ntl_base::cookie_seperator.md5($value.ntl_base::cookie_uniqueID));
	}

	public function getcookie($name) {
		if ($_COOKIE) {
			if (preg_match_all("|([^\[]+)(\[(.*)\]+)|", $name, $m, PREG_SET_ORDER) == 0) {
				$cut = explode(ntl_base::cookie_seperator, $_COOKIE[$name]);
			} else {
				$str = '$c = $_COOKIE'.'['.$m[0][1].']'.$m[0][2].';';
				eval($str);
				$cut = explode(ntl_base::cookie_seperator, $c);
			}
			if (!is_array($cut) || md5($cut[0].ntl_base::cookie_uniqueID) != $cut[1]) {
				return '';
			}
			return $cut[0];
		}
		return '';
	}
	
	public function redirect($errno, $errmsg='')
	{
		switch ($errno) {
			case '';
				break;
			default:
				$url = NTL_ROOT_URL.'error.php?err='.$errno.'&msg='.$errmsg;
				header('Location: '.$url);
				exit;
		}
	}
	
	public function is_type($yourPriv, $needpriv)
	{
		if (!is_floats($yourPriv, '+') || !is_floats($needpriv, '+'))
			return false;
		return floatval($yourPriv) & floatval($needpriv) ? true : false;
	}

	//檢查權限是否符合
	public function chkAuthority($yourPriv, $needpriv, $redirect=false)
	{
			$pass = ($this->is_type($yourPriv, AUTH_SUPERVISOR) || $this->is_type($yourPriv, $needpriv)) ? true : false;
			if (!$redirect) {
				return $pass;
			} else {
				if (!$pass) {
					$this->redirect('login.php');
				}
			}
	}
	
	public function fatal_error($msg, $sql='')
	{
		$aErr = array('msg'=>$msg, 'sql'=>$sql);
		return $aErr;
	}
	
	//render_xajax_javascript
	public function render_xajax_javascript($otherFunctions=array(), $appendOtherFunctions=true)
	{
		//取第一個斜線和最後一個斜線中間的值
		$uri = strrev(strstr($_SERVER['REQUEST_URI'],'/'));
		$uri = strrev(strstr($uri,'/'));
		$uri = !preg_match('/^[a-z0-9A-Z_-]+$/u', ereg_replace ("/", "", $uri))?'/index/':$uri;//去斜線判斷是否有奇怪的值
		//echo $uri;
		//$xajax = new xajax('http://'.$_SERVER['SERVER_NAME'].$_SERVER['SCRIPT_NAME']);
		//$xajax = new xajax('http://'.$_SERVER['SERVER_NAME'].$_SERVER['REQUEST_URI']);
		$xajax = new xajax('http://'.$_SERVER['SERVER_NAME'].$uri);
		//$xajax = new xajax('');
		//echo ("xajax=".'http://'.$_SERVER['SERVER_NAME'].$_SERVER['SCRIPT_NAME']);
		//$xajax->debugOn();
		//$xajax->configure('debug',true);//新的要用這個
		if (is_string($otherFunctions))
			$otherFunctions=array($otherFunctions);
		if ($appendOtherFunctions) {
			$registerFunction = array_merge(
															//array('show_div_x'),
															$otherFunctions
													);
		} else {
			$registerFunction = $otherFunctions;
		}
		reset($registerFunction);
		while (list($k, $v)=each($registerFunction)) {
			$xajax->registerFunction($v);
		}
		$xajax->processRequest();
		$xajax_javascript = $xajax->getJavascript(NTL_XAJAX_URL);
		
		return $xajax_javascript;
		
		//$this->assign('xajax_javascript', $xajax_javascript);
	}
	
	public function render_templte($template, $area='Div')
	{
		if (!$this->is_cached('template'.$area.'.tpl')) {
			$arr = explode('/', $template);
			$func = $arr[count($arr)-1];
			if (function_exists($func)) {
				$func($this);
			}
			$this->assign('td_content', $this->fetch($template.'.tpl'));
		}
		$this->display('template'.$area.'.tpl');
	}
}

class ntl extends ntl_base {
	public $conn=false;
	
	public function __construct($caching=0, $cache_lifetime=-1) {
		parent::__construct($caching, $cache_lifetime);
	}
	
	public function db_connect() {
		if ($this->conn && $this->conn->IsConnected())
			return true;
		
		try {
			if (strpos($_SERVER['SERVER_SOFTWARE'], 'Win32')===false)
				//$cConnString = 'mssql';
				$cConnString = 'mysql';
			else
				//$cConnString = 'odbtp_unicode';
				$cConnString = 'mysql';
				
			$this->conn = &NewADOConnection($cConnString);
			//$this->conn->Connect('localhost','cmdb','attechatCTU','cmvod');
			//$this->conn->Connect('localhost','root','ccdntech','ccdntech');
			//$this->conn->Connect('localhost','root','ccdntech','ccdn2');
			$this->conn->Connect(CM_Conf::get('DB.HOST'),CM_Conf::get('DB.USERNAME'),CM_Conf::get('DB.PASSWORD'),CM_Conf::get('DB.NAME'));
			

			$this->conn->SetFetchMode(ADODB_FETCH_ASSOC);
		} catch (Exception $e) {
			;//adodb_backtrace($e->gettrace());die('Connection failed');
			return false;
		}
		if (!$this->conn || !$this->conn->IsConnected()) return false;
		/*$this->vars['TBL']['admin']	= 'ntl_admin';
		$this->vars['TBL']['admin_channel']	= 'ntl_admin_channel';
		$this->vars['TBL']['admin_group']	= 'ntl_admin_group';
		$this->vars['TBL']['admin_group_member']	= 'ntl_admin_group_member';
		$this->vars['TBL']['authority']	= 'ntl_authority';
		$this->vars['TBL']['bitRate']	= 'ntl_bitRate';
		$this->vars['TBL']['bleep']	= 'ntl_bleep';
		$this->vars['TBL']['config']	= 'ntl_config';
		$this->vars['TBL']['contact_us']	= 'ntl_contact_us';
		$this->vars['TBL']['contact_us_cat']	= 'ntl_contact_us_cat';
		$this->vars['TBL']['contact_us_detail']	= 'ntl_contact_us_detail';
		$this->vars['TBL']['department']	= 'ntl_department';
		$this->vars['TBL']['faq']	= 'ntl_faq';
		$this->vars['TBL']['faq_cat']	= 'ntl_faq_cat';
		$this->vars['TBL']['faq_file']	= 'ntl_faq_file';
		$this->vars['TBL']['faq_link']	= 'ntl_faq_link';
		$this->vars['TBL']['favorite']	= 'ntl_favorite';
		$this->vars['TBL']['favorite_detail']	= 'ntl_favorite_detail';
		$this->vars['TBL']['internal_ip']	= 'ntl_internal_ip';
		$this->vars['TBL']['live']	= 'ntl_live';
		$this->vars['TBL']['log_error']	= 'ntl_log_error';
		$this->vars['TBL']['log_live_rank']	= 'ntl_log_live_rank';
		$this->vars['TBL']['log_media_rank']	= 'ntl_log_media_rank';
		$this->vars['TBL']['log_query']	= 'ntl_log_query';
		$this->vars['TBL']['log_search_keyword']	= 'ntl_log_search_keyword';
		$this->vars['TBL']['log_search_keyword_sum']	= 'ntl_log_search_keyword_sum';
		$this->vars['TBL']['log_variation']	= 'ntl_log_variation';
		$this->vars['TBL']['media']	= 'ntl_media';
		$this->vars['TBL']['media_carousel'] = 'ntl_media_carousel';
		$this->vars['TBL']['media_comment']	= 'ntl_media_comment';
		$this->vars['TBL']['media_belong_cat']	= 'ntl_media_belong_cat';
		$this->vars['TBL']['media_belong_ext_column']	= 'ntl_media_belong_ext_column';
		$this->vars['TBL']['media_belong_user']	= 'ntl_media_belong_user';
		$this->vars['TBL']['media_belong_user_group']	= 'ntl_media_belong_user_group';
		$this->vars['TBL']['media_cat']	= 'ntl_media_cat';
		$this->vars['TBL']['media_detail']	= 'ntl_media_detail';
		$this->vars['TBL']['media_ext_column']	= 'ntl_media_ext_column';
		$this->vars['TBL']['media_file']	= 'ntl_media_file';
		$this->vars['TBL']['media_playlist']	= 'ntl_media_playlist';
		$this->vars['TBL']['mix']	= 'ntl_mix';
		$this->vars['TBL']['mix_cat']	= 'ntl_mix_cat';
		$this->vars['TBL']['type_version']	= 'ntl_type_version';
		$this->vars['TBL']['news']	= 'ntl_news';
		$this->vars['TBL']['news_cat']	= 'ntl_news_cat';
		$this->vars['TBL']['news_file']	= 'ntl_news_file';
		$this->vars['TBL']['news_link']	= 'ntl_news_link';
		$this->vars['TBL']['rmaccess_log']	= 'ntl_rmaccess_log';
		$this->vars['TBL']['rmaccess_transfrom']	= 'ntl_rmaccess_transfrom';
		$this->vars['TBL']['rss']	= 'ntl_rss';
		$this->vars['TBL']['podcast']	= 'ntl_podcast';
		$this->vars['TBL']['news']	= 'ntl_news';
		$this->vars['TBL']['news_detail']	= 'ntl_news_detail';
		$this->vars['TBL']['user']	= 'ntl_user';
		$this->vars['TBL']['user_area']	= 'ntl_user_area';
		$this->vars['TBL']['user_group']	= 'ntl_user_group';
		$this->vars['TBL']['user_group_member']	= 'ntl_user_group_member';
		$this->vars['TBL']['user_ip']	= 'ntl_user_ip';
		$this->vars['TBL']['student_dep'] = 'ntl_student_dep';
		$this->vars['TBL']['vod_server']	= 'ntl_vod_server';
		
		$this->vars['TBL']['media_owner_user']	= 'ntl_media_owner_user';
		
		$this->vars['TBL']['flash_live']	= 'ntl_flash_live';
		$this->vars['TBL']['wms_live']	= 'ntl_wms_live';
		$this->vars['TBL']['flash_vod_server']	= 'ntl_flash_vod_server';
		$this->vars['TBL']['wms_vod_server']	= 'ntl_wms_vod_server';
		$this->vars['TBL']['server_state']	= 'ntl_server_state';		
		$this->vars['TBL']['media_schedule_detail']	= 'ntl_media_schedule_detail';
		$this->vars['TBL']['media_schedule_belong_date']	= 'ntl_media_schedule_belong_date';
		$this->vars['TBL']['media_schedule_belong_cat']	= 'ntl_media_schedule_belong_cat';
		$this->vars['TBL']['media_schedule_cat']	= 'ntl_media_schedule_cat';
		$this->vars['TBL']['playList']	= 'ntl_playList';
		$this->vars['TBL']['playList_detail']	= 'ntl_playList_detail';
		$this->vars['TBL']['tmp_media_schedule_select']	= 'ntl_tmp_media_schedule_select';
		$this->vars['TBL']['attribute']	= 'ntl_attribute';
		$this->vars['TBL']['media_belong_attribute']	= 'ntl_media_belong_attribute';
		$this->vars['TBL']['log_media_attribute']	= 'ntl_log_media_attribute';		
        $this->vars['TBL']['marquee']	= 'ntl_marquee';
        $this->vars['TBL']['dept_belong_cat']	= 'ntl_dept_belong_cat';
        $this->vars['TBL']['group_belong_cat']	= 'ntl_group_belong_cat';        
        $this->vars['TBL']['pic_cat']	= 'ntl_pic_cat';
        $this->vars['TBL']['pic_belong_cat']	= 'ntl_pic_belong_cat';
        $this->vars['TBL']['media_choice'] = 'ntl_media_choice';
        $this->vars['TBL']['media_cat_allow_domain'] = 'ntl_media_cat_allow_domain';
        $this->vars['TBL']['media_cat_allow_login'] = 'ntl_media_cat_allow_login';
        $this->vars['TBL']['account'] = 'ntl_account';*/
        		
		/*
		if ($this->getcookie('debug')=='ntl')
			$this->conn->LogSQL(); // turn on logging*/

		return true;
	}
	
	public function render_error($file, $function, $line, $err_msg, $err_no=0, $template='processFail', $area='Normal')
	{
		$this->assign('td_content', $this->fetch($template.'.tpl'));
		$this->display('template'.$area.'.tpl');
		$this->log_error($file, $function, $line, $err_msg, $err_no);
		exit;
	}
	
	public function log_error($file, $function, $line, $err_msg, $err_no=0)
	{
		if ($this->conn!==false && $this->conn->IsConnected()) {
			$record['loge_file'] = $file;
			$record['loge_function'] = $function;
			$record['loge_line'] = $line;
			$record['loge_datetime'] = date('Y-m-d H:i:s');
			$record['loge_err_no'] = $err_no;
			$record['loge_err_msg'] = is_array($err_msg) ? implode(chr(13).chr(13), $err_msg) : $err_msg;
			$record['loge_ip'] = $_SERVER['REMOTE_ADDR'];
			$this->conn->AutoExecute($this->vars['TBL']['log_error'], $record, 'INSERT');
		}
		
		//$this->redirect($error_page);
	}
	
	public function log_query($query)
	{
		if ($this->conn!==false && $this->conn->IsConnected()) {
			$record['query'] = $query;
			$this->conn->AutoExecute($this->vars['TBL']['log_query'], $record, 'INSERT');
		}
	}
	
	public function cookieSession($arr)//$arr=array('key1', 'key2')
	{
		if (is_string($arr))
			$arr = array($arr);
		if (!is_array($arr) || empty($arr))
			return '';
		$cnt = count($arr);
		if ($cnt > 2)
			return '';
		switch ($cnt) {
			case 1:
				$key = $arr[0];
				if (!isset($_SESSION[$key]))
					$_SESSION[$key] = $this->getcookie('cookie_ntl['.$key.']');
				return $_SESSION[$key];
				break;
			case 2:
				$key1 = $arr[0];
				$key2 = $arr[1];
				if (!isset($_SESSION[$key1][$key2]))
					$_SESSION[$key1][$key2] = $this->getcookie('cookie_ntl['.$key1.']['.$key2.']');
				return $_SESSION[$key1][$key2];
				break;
		}
	}
	
	// combine=>把陣列裡面的值合起來
	// take apart=>把值拆開放到陣列
	public function bit_or($mix, $act)
	{
		switch (strtolower($act)) {
			case 'combine':
				if (is_floats($mix, '+'))
					return $mix;
				if (!is_array($mix))
					return 0;
				$pow = 0;
				foreach ($mix as $k => $v) {
					if (is_floats($v, '+'))
						$pow |= $v;
					else
						return 0;
				}
				return $pow;
				break;
			case 'take apart':
				$arr = array();
				if (!is_floats($mix) || is_floats($mix, '-') || $mix==0)
					return $arr;
				$comp = 0;
				for ($i=0;;$i++) {
					$p = pow(2, $i);
					if ($p & $mix) {
						array_push($arr, $p);
						$comp |= $p;
						if ($comp == $mix)
							break;
					}
				}
				return $arr;
				break;
			default:
				$this->log_error(__FILE__, __FUNCTION__, __LINE__, $act.' not defined in bit_or()!');
				die($act.' not defined in bit_or()!');
		}
	}
	
	public function html_meta()
	{
		$this->assign('html_title', '建國科技大學圖書館VOD隨選視訊系統');
		
		//lydia add type version 20130912
		if ($this->db_connect()===false)
			throw new ntl_Exception($this->fatal_error(NTL_ERROR_UNCONN));
		
		$sql = 'SELECT cType_filename FROM '.$this->vars['TBL']['type_version'].' WHERE iType_status=1';
		$rs = $this->conn->_Execute($sql);
		if ($rs->fields['cType_filename'] == '') {
			$html_css = 'style.css';			
		} else {
			$html_css = $rs->fields['cType_filename'];
			$c_index = ($html_css == 'style.css') ? '' : '_'.substr($html_css,6,1);
			$this->assign('c_index', $css_index);
		}	
		$this->assign('html_css', $html_css);
	}
	
	//lydia add type version 20130912
	public function html_css()
	{
		if ($this->db_connect()===false)
			throw new ntl_Exception($this->fatal_error(NTL_ERROR_UNCONN));
		
		$sql = 'SELECT cType_filename FROM '.$this->vars['TBL']['type_version'].' WHERE iType_status=1';
		$rs = $this->conn->_Execute($sql);
		if ($rs->fields['cType_filename'] == '') {
			$html_css = 'style.css';			
		} else {
			$html_css = $rs->fields['cType_filename'];
			$css_index = ($html_css == 'styles.css') ? '' : substr($html_css,6,1);
			$this->assign('css_index', $css_index);
			$this->assign('c_index', '_'.$css_index);
		}	
		$this->assign('html_css', $html_css);
		
	}
	public function render_templte($template, $area='Normal')
	{
		if (!empty($_GET['print']))
			$area='Print';
		$arr = explode('/', $template);
		$func = $arr[count($arr)-1];
		$tpl = $template;
		if (function_exists($func)) {
			$tmp = $func($this);
			if (!is_null($tmp))
				$tpl = $tmp;
		}
		$this->assign('td_content', $this->fetch($tpl.'.tpl'));
		$this->html_meta();
		$this->html_css();
		$this->display('template'.$area.'.tpl');
	}
}


?>
