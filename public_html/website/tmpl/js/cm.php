<?php
  //設定值載入
  require('/' . implode('/', array_filter(array_map(function($element) {
    if(in_array($element, array('website', 'tmpl', 'js'))) return false;

    return $element;
  }, explode('/', __DIR__)))) . '/lib/inc/conf.php');
  //初始化載入
  require(CM_Conf::get('PATH_F.LIB') . 'inc/init.php');
  //呼叫控制器
  require(CM_Conf::get('PATH_F.ROOT') . 'website/inc/web_init.php');
  
  CM_Conf::set('PATH_W.HTTP_SERVER', CM_Conf::get('HTTP.SERVER'));
  //Javascript設定 ---------- 開始
  CM_Conf::set('JAVASCRIPT.CURRENT_LANG', CM_Lang::get_current_code());
  CM_Conf::set('JAVASCRIPT.MODE', CM_Conf::get('DEF.DEBUG_WEBMGR'));
  //Javascript設定 ---------- 結束

  echo implode("\n", array(
    'window._cm = {};',
    'window._cm.tmpl = {};',
    'window._cm.lang = {};',
    'window._cm.path = {};',
    'window._cm.setting = {};'
  )) ;
  //預設版型
  foreach(array_merge(
    CM_Template::get_path_tpl(CM_Conf::get('PATH_F.SITE_TMPL') . 'view/javascript/')
  ) as $key => $value) {
    echo "window._cm.tmpl.{$key} = function() {/* {$value} */}.toString();\n";
  }
  
  foreach(CM_Lang::export('Javascript') as $key => $value) echo "window._cm.lang.{$key} = '{$value}';\n";
  
  foreach(CM_Conf::export('PATH_W') as $key => $value) echo "\nwindow._cm.path.{$key} = '{$value}';";
  
  foreach(CM_Conf::export('JAVASCRIPT') as $key => $value) echo "\nwindow._cm.setting.{$key} = '{$value}';";
?>