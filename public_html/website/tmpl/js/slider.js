// JavaScript Document

/*!
 * ANN:slider
 */
(function($){
	
	 var sideCategory={_:0
		,lock:false
		,status:"on"
		,toggle:function(){
			if(sideCategory.lock)return;
			var value="+=-220";
			switch(sideCategory.status){
			case'on':
				sideCategory.status="off";
				$('.toggle')
					.removeClass('hidden')
					.slideDown(0)
					.slideUp(300,function function_name (argument) {
						sideCategory.lock=false;
					});
				break;
			case'off':
				value="+=220";
				sideCategory.status="on";
				$('.toggle')
					.removeClass('hidden')
					.slideUp(0)
					.slideDown(300,function function_name (argument) {
						sideCategory.lock=false;
					})
				break;
			}
			$( ".sideCategory" ).animate({
				left:value
				}, 500, function() {
					sideCategory.lock=false;
					// Animation complete.
			  });
			sideCategory.lock=true;
		}
		,summonSubList:function function_name (target) {
			
		}
	 }
	$('.sideToggle').click(sideCategory.toggle);
	$('.sideCategoryContent li').click(function function_name (e) {
		if(this.unfold){
			$(this)
				.find('.arrowBefore').removeClass('unfold').end()
				.find('.sublist').hide('fast');
			this.unfold=false;
		}
		else{
			$(this)
				.find('.arrowBefore').addClass('unfold').end()
				.find('.sublist').show('fast');
			this.unfold=true;
		}
		return;
		var idTarget=$(this).attr("targetSubList");
		if(!idTarget)return;
		var offset=$(this).offset();
		offset.left+=200;
		$(".subList."+idTarget)
			.show()
			.offset(offset)
			;
	});
	$('.subList').hover(function functionIn (argument) {
		
	},function functionOut (argument) {
		$(this).hide();
	})
	sideCategory.toggle();
})(jQuery);