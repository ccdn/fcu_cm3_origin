<?php
  class Site_Page_Link extends Site_Page {
    const PAGE_NAME = 'link';
	const PATH_MAIN = '/link/';
    
    public function __construct(Site_Path $path) {
      parent::__construct($path, self::PAGE_NAME);
    }
    
	public function newsClass() {
      //分類
      if($this->get_dao('option')->get_array($optionArray, '', array('catalog' => 1))) {
		
		foreach($optionArray AS $key => $optionRows) {
			$optionRows['link'] = Site::href_link(self::PATH_MAIN, array('catalog' => $optionRows['id']));
			if(!empty($this->_get['catalog'])&&is_numeric($this->_get['catalog'])){
				if($optionRows['id']==$this->_get['catalog']){
					$optionRows['selected']=true;
				}
			}
			$newsClassArray[] = $optionRows;
		}
		
        return $newsClassArray;
      }
    } 
	
    public function page_default() {
	
		//列表
		$this->page_list();
		
		//套版測試
		//$this->page_test();

    }
	
	public function page_list() {	
		
		//ajax
		$ntl = $this->_ntl;
		$this->_xajax_javascript = $ntl->render_xajax_javascript(array(array($this,'front_login'),array($this,'front_logout')), false);
		
		//判斷錯誤
		$catalog = !empty($this->_get['catalog'])&&is_numeric($this->_get['catalog'])?$this->_get['catalog']:0;
		
	
	  //麵包屑
	  if(isset($this->_get['catalog']) && $this->get_dao('option')->get_rows_byId($recordRows, '', $catalog)) {
        //加入麵包屑
        $this->_breadcrumb->add($recordRows[Site_DAO_Link::COLUMN_HEADLINE], $this->_path_rows['path'], array(Site::VAR_RECORD_ID => $recordRows[Site_DAO_Link::COLUMN_RECORD_ID]));
	  }
	  
	  //分類
      $data['newsClass'] = $this->newsClass();
	  if(!empty($data['newsClass'])){
		$data['hasClass']=true;
	  }
	  
	  //$params = array('catalog' => 1 ,'status' => 1 );
	  
	  $daoParams = array(
        //'status' => $currentStatus,
        //'sort' => Admin_Mod::to_sort($this->_mod_dao, $this->_get['sc'], $this->_get['sf']),
        //'keyword' => $this->_get['keyword']
      );
	  
	  
	  //欄位名稱
	  $addColumn = array('link' => '', 'linkText' => '','action' => CM_Lang::line('Admin.LIST_ACTION'),'img'=>'','hasMark'=>'');
	  $listColumn = array_merge($this->get_dao(self::PAGE_NAME)->get_column_field_label_array(Site_DAO_Link::GROUP_LIST), $addColumn);//取欄位變數
	  
	  //echo $listColumn['action']."<br>";//動作(欄位標題)
	  
	  
	  //[單筆]
	  //$this->get_dao(self::PAGE_NAME)->get_rows($recordRows, Site_DAO_Link::GROUP_LIST, $params);
	  //echo  "<p><a href=".$recordRows[url].">".$recordRows[headline]."</a></p>";
	  
	  
	  //[多筆]
	  //只取值
	  /*$listJSON = $this->get_dao(self::PAGE_NAME)->get_array($arr,Site_DAO_Link::GROUP_LIST, array_merge($daoParams, array(
		//'keyColumn' => Site_DAO_Link::COLUMN_RECORD_ID,
		'catalog' => 1
	  )));
	  echo count($arr)."<br>";*/
	  
	  
		//變數集
		$params = array(
		  'dao_params' => array(
			'catalog' => $catalog,
			'status' => 1
			//'catalog' => 2
		  ),
		  'get_params' => $this->_get,
		  'split_count' => 10
		);
	  
	  //取值&頁面相關參數
	  $cbThat = $this;
	  $pathRows = $this->get_path_rows();
	  $listJSON = $this->get_dao(self::PAGE_NAME)->get_array_object(Site_DAO_Link::GROUP_LIST, array_merge($params['dao_params'], array(
		//'catalog' => 1,
        //'oNowPage' => isset($this->_get['p']) ? (int)$this->_get['p'] : 1,
		'oNowPage' => isset($params['get_params']['p']) ? (int)$params['get_params']['p'] : 1,
		'oPageSplit' => $params['split_count'],
		'oPageCallback' => function($page, array $other_news, $key) use($pathRows) {
          return Site_Page::cb_default_list_pagesplit($page, $other_news, $key, $pathRows['path'], 'p');
        }
	  )));
	  
	  //print_r($listJSON);
	  
	  //取很多值
	  //$data = $listJSON->{'data'};
	  //print_r(count($data));
	  
	  //$dataSplit = $listJSON->{'dataSplit'};
	  //print_r($dataSplit['pageInfo'])
	  
	  //看要怎麼套版...
	  //echo CM_Template::get_tpl_html('linkDefault', $listJSON);
	  
	  $listJSON->data = CM_Format::to_list_array_format(
          $listJSON->data,
          $listColumn,
          array(
            'link' => function($record, $recordRows = array()) use($pathRows) {
              if(CM::not_null($recordRows['url'])) {
                return $recordRows['url'];
              } 
			  /*else {
                return Site::href_link($pathRows['path'], array(Site::VAR_RECORD_ID => $recordRows[Site_DAO_Link::COLUMN_RECORD_ID], 'type' => '7'));
              }*/
            },
            'img' => function($record, $recordRows = array()) use($pathRows) {
				if(CM::not_null($recordRows['file'])) {
					//圖片
					//return CM_Html::img(CM_Format::to_thumbnail_src($recordRows['file'], '215x190'), $recordRows['headline'], '', '', 'class="hostimg"');
					return CM_Html::img('/upload/'.$recordRows['file'], $recordRows['headline'], '', '', 'class="img-responsive"');
					//return $recordRows['file'];
				}else{
					//預設圖片
					return CM_Html::img('/website/tmpl/img/pre_link.jpg', $recordRows['headline'], '', '', 'class="hostimg"');
				}
            },
			
           
          )
        );
		//print_r($listJSON->data);
		//看要怎麼套版...
		/*echo CM_Template::get_tpl_html('linkDefault', array(
			  'dataClass' => $data['newsClass'],
              'data' => $recordRows,
              'dataList' => $listJSON->data,
              'dataSplit' => $listJSON->dataSplit
        ));*/
		//html(但是加這個原本是html的也會變html...)
		/*array_walk_recursive($listJSON->data, function (&$value) {
			$value = htmlentities($value,ENT_QUOTES,"UTF-8");
		});*/
		//連結要特別處理...
		array_walk_recursive($data['newsClass'], function (&$value) {
			$value = htmlentities($value,ENT_QUOTES,"UTF-8");
		});
		
		
		//套版
		echo $this->main_page(array(
			'm_header' => $this->main_header(array('hasClass' => $data['hasClass'],'dataClass' => $data['newsClass'])),
			'm_main' => CM_Template::get_tpl_html('linkDefault', array(
			  'hasClass' => $data['hasClass'],
			  'dataClass' => $data['newsClass'],
              'data' => $recordRows,
              'dataList' => $listJSON->data,
              //'dataSplit' => $listJSON->dataSplit,
			  //'searchForm' => $this->getSearchForm()
			))
		));
    }
	
  }
?>