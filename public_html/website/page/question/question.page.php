<?php
  class Site_Page_Question extends Site_Page {
    const PAGE_NAME = 'question';
	const PATH_MAIN = '/question/';
	const PATH_DETAIL = '/question/detail/'; //新聞內頁連結
    
    public function __construct(Site_Path $path) {
      parent::__construct($path, self::PAGE_NAME);
    }
	
	
	public function newsClass() {
      //分類
      if($this->get_dao('option')->get_array($optionArray, '', array('catalog' => 2))) {
		
		foreach($optionArray AS $key => $optionRows) {
			$optionRows['link'] = Site::href_link(self::PATH_MAIN, array('catalog' => $optionRows['id']));
			if(!empty($this->_get['catalog'])&&is_numeric($this->_get['catalog'])){
				if($optionRows['id']==$this->_get['catalog']){
					$optionRows['selected']=true;
				}
			}
			$newsClassArray[] = $optionRows;
		}
		
        return $newsClassArray;
		
      }
    }

	//搜尋表單
	public function getSearchForm(){
		return array(
			'formTop' => CM_Html::form('searchForm', Site::href_link(self::PATH_MAIN), 'get', 'class="cm-form form-horizontal" role="form"'),
			'formSearch' => CM_Html::input_field('keyword', $this->_get['keyword']),
			'formButton' => CM_Html::submit_btn('<i class="icofont-search"></i>&nbsp;' .CM_Lang::line('Site.BTN_SEARCH'), array('class' => 'btn btn-primary')),
			'formBottom' => '</form>'
		);
	}
	
	public function page_default() {	  
		//列表
		$this->page_list();
		
		//套版測試
		//$this->page_test();
	  
    }
	
	public function page_test(){
		//套版
		echo $this->main_page(array(
			//'m_main' => CM_Template::get_tpl_html('customersForm', $data)
		));
	}
	
    public function page_list() {
		//ajax
		$ntl = $this->_ntl;
		$this->_xajax_javascript = $ntl->render_xajax_javascript(array(array($this,'front_login'),array($this,'front_logout')), false);
		
		//判斷錯誤
		$catalog = !empty($this->_get['catalog'])&&is_numeric($this->_get['catalog'])?$this->_get['catalog']:0;
	
       //麵包屑
	  if(isset($this->_get['catalog']) && $this->get_dao('option')->get_rows_byId($recordRows, '', $this->$catalog)) {
        //加入麵包屑
        $this->_breadcrumb->add($recordRows[Site_DAO_Link::COLUMN_HEADLINE], $this->_path_rows['path'], array(Site::VAR_RECORD_ID => $recordRows[Site_DAO_Link::COLUMN_RECORD_ID]));
	  }        
		
		//分類
		$data['newsClass'] = $this->newsClass();
		if(!empty($data['newsClass'])){
		$data['hasClass']=true;
	  }
	  
		//變數集
		$params = array(
		  'dao_params' => array(
			'catalog' => $catalog,
			//'keyword' => $this->_get['keyword'],
			'status' =>1,
		  ),
		  'get_params' => $this->_get,
		  'split_count' => 10
		);
	  
		//取值&頁面相關參數
		$pathRows = $this->get_path_rows();
		$listJSON = $this->get_dao(self::PAGE_NAME)->get_array_object(Site_DAO_Question::GROUP_LIST, array_merge($params['dao_params'], array(
			//'oNowPage' => isset($this->_get['p']) ? (int)$this->_get['p'] : 1,
			'oNowPage' => isset($params['get_params']['p']) ? (int)$params['get_params']['p'] : 1,
			'oPageSplit' => $params['split_count'],
			'oPageCallback' => function($page, array $other_news, $key) use($pathRows) {
			return Site_Page::cb_default_list_pagesplit($page, $other_news, $key, $pathRows['path'], 'p');
		}
			
		)));
		//print_r($pathRows);
	  
		$detailLink = self::PATH_DETAIL;
		
		$addColumn = array('link' => '', 'linkText' => '');
		$listColumn = array_merge($this->get_dao(self::PAGE_NAME)->get_column_field_label_array(Site_DAO_Question::GROUP_LIST), $addColumn);//取欄位變數
		$listJSON->data = CM_Format::to_list_array_format(
			$listJSON->data,
			$listColumn,
			array(
				'link' => function($record, $recordRows = array()) use($detailLink) {
				  //return Site::href_link(self::PATH_DETAIL, array( Site::VAR_RECORD_ID => $recordRows[Site_DAO_Question::COLUMN_RECORD_ID] => $recordRows['id']));
				 return Site::href_link($detailLink, array(Site_DAO_Question::COLUMN_CATALOG_ID => $recordRows[Site_DAO_Question::COLUMN_CATALOG_ID], Site::VAR_RECORD_ID => $recordRows[Site_DAO_Question::COLUMN_RECORD_ID]));			 
				 //return $recordRows['id'];
				},
				'headline' => function($record, $recordRows = array()) {
				  return Site::substr($record, 20);
				}
			  )
			);
		
		//print_r($listJSON->data);
	  
		//套版...
		/*echo CM_Template::get_tpl_html('questionDefault', array(
			  'dataClass' => $data['newsClass'],
              //'data' => $recordRows,
              'dataList' => $listJSON->data,
              'dataSplit' => $listJSON->dataSplit,
			  'searchForm' => $this->getSearchForm()
        ));*/
		//print_r($listJSON->data);
		
		//連結要特別處理...
		array_walk_recursive($data['newsClass'], function (&$value) {
			$value = htmlentities($value,ENT_QUOTES,"UTF-8");
		});
		
		//套版
		echo $this->main_page(array(
			'm_header' => $this->main_header(array('hasClass' => $data['hasClass'],'dataClass' => $data['newsClass'])),
			'm_main' => CM_Template::get_tpl_html('questionDefault', array(
			  'hasClass' => $data['hasClass'],
			  'dataClass' => $data['newsClass'],
              //'data' => $recordRows,
              'dataList' => $listJSON->data,
              //'dataSplit' => $listJSON->dataSplit,
			  //'searchForm' => $this->getSearchForm()
			))
		));
		
		
      //} else {
        //列表頁面
	//	echo 'test page';
        /*echo $this->main_page(array(
          //'m_left' => $this->main_column_left(),
          'm_main' => $this->get_widget(self::PAGE_NAME, 'list_split', array('get_params' => $this->_get))
        ));*/
     // }
	  
	  
		
	  
	  
	  
    }
	
	
	//內頁(現在沒用)
	public function page_detail() {
		
		$pathRows = $this->get_path_rows();
		
		//判斷有資料會沒資料會
		if(isset($this->_get[Site::VAR_RECORD_ID]) && $this->get_dao(self::PAGE_NAME)->get_rows($recordRows,Site_DAO_Question::GROUP_DETAIL, array('recordId' => $this->_get[Site::VAR_RECORD_ID], 'isCallback' => true))) {
			//顯示內容
			
			 //取得分類
			$data['newsClass'] = $this->newsClass();
			
			//日期換算
			$recordRows[Site_DAO_Question::COLUMN_CREATE_DATE] = date('Y-m-d', strtotime($recordRows[Site_DAO_Question::COLUMN_CREATE_DATE]) );
			
			//回上一頁
			//print_r( Site::get_all_get_params(array('recordId'), true));
			$recordRows['questionListLink'] = Site::href_link($pathRows['path_parent'], array( Site::get_all_get_params(array('recordId'), true)) );
			
			//套版...
			echo CM_Template::get_tpl_html('questionDetail', array(
			  'dataClass' => $data['newsClass'],
              'data' => $recordRows
            ));
			
		}else{
			//導回列表
			CM_Output::redirect(self::PAGE_NAME);
		}
		
		//echo "this is test page";
	}
	
	
	
	
  }
?>