<?php
  class Site_Page_Media extends Site_Page {
    const PAGE_NAME = 'media';
	const PAGE_PHOTO = 'photo';
	const PATH_MAIN = '/media/';
	const PATH_PHOTO = '/photo/';
	const PATH_DETAIL = '/media/detail/'; //播放頁連結
	const PATH_IFRAME = '/media/iframe/'; //iframe	
	   
    public function __construct(Site_Path $path) {
		parent::__construct($path, self::PAGE_NAME);
		//require_once('/website/tmpl/ntl.config.inc.php');
		//ajax
		//global $ntl;
		//$ntl = new ntl();
		//$this->_ntl = $ntl;
		$ntl=$this->_ntl;
		//ispc
		global $isPc;
		
		//取得server位置
		if ($ntl->db_connect()===false)
			throw new ntl_Exception($ntl->fatal_error(NTL_ERROR_UNCONN));
			
		
    }
	
	//分類
	public function newsClass($catalogMenuaArray) {
      
	  //子分類處理
		foreach($catalogMenuaArray AS $key => $optionRows) {
			$optionRows['link'] = Site::href_link(self::PATH_MAIN, array('catalog' => $optionRows['id']));
			if(!empty($this->_get['catalog'])){
				if($optionRows['id']==$this->_get['catalog']){
					$optionRows['selected']=true;
				}
			}
			if(!empty($optionRows['child'])){
				$newsClassArray2=array();
				foreach($optionRows['child'] AS $key2 => $optionRows2) {
					$optionRows2['link'] = Site::href_link(self::PATH_MAIN, array('catalog' => $optionRows2['id']));
					if(!empty($this->_get['catalog'])){
						if($optionRows2['id']==$this->_get['catalog']){
							$optionRows2['selected']=true;
						}
					}
					$newsClassArray2[]=$optionRows2;
				}
				//print_r($newsClassArray2);
				$optionRows['child']=$newsClassArray2;
				$optionRows['hasChild']=true;
			}
			
			$newsClassArray[] = $optionRows;
		}
		//print_r($newsClassArray);
        return $newsClassArray;
      
    } 
	
	//相片分類
	public function photoClass($catalogMenuaArray) {
      
	  //子分類處理
		foreach($catalogMenuaArray AS $key => $optionRows) {
			$optionRows['link'] = Site::href_link(self::PATH_PHOTO, array('catalog' => $optionRows['id']));
			if(!empty($this->_get['catalog'])){
				if($optionRows['id']==$this->_get['catalog']){
					$optionRows['selected']=true;
				}
			}
			if(!empty($optionRows['child'])){
				$newsClassArray2=array();
				foreach($optionRows['child'] AS $key2 => $optionRows2) {
					$optionRows2['link'] = Site::href_link(self::PATH_PHOTO, array('catalog' => $optionRows2['id']));
					if(!empty($this->_get['catalog'])){
						if($optionRows2['id']==$this->_get['catalog']){
							$optionRows2['selected']=true;
						}
					}
					$newsClassArray2[]=$optionRows2;
				}
				//print_r($newsClassArray2);
				$optionRows['child']=$newsClassArray2;
				$optionRows['hasChild']=true;
			}
			
			$newsClassArray[] = $optionRows;
		}
		//print_r($newsClassArray);
        return $newsClassArray;
      
    } 
	
	
	//取得轉檔設定值
	public function getTransConfig(){
		//取SYSTEM設定值
		$ini_array = parse_ini_file("system-config.properties", true);
		//print_r($ini_array);
		//$TRANS_TYPE = $ini_array['TRANSCODE_SET']['TYPE'];
		//$TRANS_BITRATE =  explode(",", $ini_array['TRANSCODE_SET']['BITRATE']);
		//$TRANS_WIDTH = explode(",", $ini_array['TRANSCODE_SET']['WIDTH']);
		//echo $TRANS_TYPE.'<br>';
		//print_r($TRANS_BITRATE);
		//return $ini_array['TRANSCODE_SET'];
		
		global $TRANS_TYPE,$TRANS_WIDTH,$TRANS_BITRATE,$TRANS_BITRATE_SET,$TRANS_DEFAULT,$HAS_MENU;
		
		$TRANSFORM_SET = $ini_array['TRANSCODE_SET'];
		//print_r($ini_array['TRANSCODE_BITRATE_SET']);
		
		$TRANS_TYPE = $TRANSFORM_SET['TYPE'];
		
		// 判斷 $TRANS_TYPE : 1->固定，不讀資料庫轉檔數量，直接讀固定轉檔bitrate；2->動態，由資料庫欄位判斷對應的轉檔bitrate
		if($TRANS_TYPE==1){
			$TRANS_BITRATE = explode(",", $TRANSFORM_SET['BITRATE']);
		}else{
			$TRANS_BITRATE_SET = $ini_array['TRANSCODE_BITRATE_SET']['BITRATE'];
		}
		
		//$TRANS_WIDTH = explode(",", $TRANSFORM_SET['WIDTH']);
		//$TRANS_BITRATE = explode(",", $TRANSFORM_SET['BITRATE']);
		$TRANS_DEFAULT = $TRANSFORM_SET['DEFAULT'];//預設播哪一支
		
		$HAS_MENU = $ini_array['HAS_MENU'];
		
		//$IS_AUTO = $TRANS_TYPE==1?false:true;//之前是用是否自動判斷要不要有menu,現在改成變數判斷
		
	}
	
	
	
	// 預設頁面
    public function page_default() {
		//echo "this is test page";
		
		$this-> page_list();
		//$this->page_pic();
		//$this->page_test();
		
	}
	
	// 套版測試
	public function page_test(){
	
		echo $this->main_page(array(
			//'m_main' => CM_Template::get_tpl_html('customersForm', $data)
		));
		
	}
	
	// 列表
	public function page_list() {
	
		//ajax
		//$ntl = $GLOBALS['ntl'];
		//$xajax_javascript = $ntl->render_xajax_javascript(array(array($this,'layoutSetuping'),array($this,'front_login')), false);
		$ntl = $this->_ntl;
		$this->_xajax_javascript = $ntl->render_xajax_javascript(array(array($this,'layoutSetuping'),array($this,'front_login'),array($this,'front_logout')), false);
		
		//判斷錯誤
		$catalog = !empty($this->_get['catalog'])&&is_numeric($this->_get['catalog'])?$this->_get['catalog']:0;
		//類型-->1影片,2聲音,3照片,預設為影片
		$type=!empty($this->_get['type'])?$this->_get['type']:1;//預設為影片
		if( $type!=1 && $type!=3 ){
			$this->page_notfound();
			die();
		};
		
		//150804:ANN:沒分類的預設
		if( $catalog==0 && $this->_get['keyword']==null ){
			$this->_get['offset']=1;
		}
		
		$timeNow = date('Y-m-d');
		
		//分類資料庫(linux大小寫要注意)
		$_catalog_type = array(
			1 => 'catalog_Video',
			2 => 'catalog_Sound',
			3 => 'catalog_Picture'
		);
			
		//排序判斷(預設欄位)
		$sortColumn=array('start_date','video_length','clickcount');//上架日期、片長、點擊次數
		$sortHeadline=array('上架日期','片長','觀看次數');		
		if($type==3){
			$sortColumn=array('start_date');//上架日期
			$sortHeadline=array('上架日期');
		}
		
		//$sortType=$this->_get['sort']==null?'DESC':mysql_real_escape_string($this->_get['sort']);
		$sortType=!empty($this->_get['sort'])?mysql_real_escape_string($this->_get['sort']):'DESC';
		$sortType=(strtoupper($sortType)!='DESC'&&strtoupper($sortType)!='ASC')?'DESC':$sortType;
		$orderType=!empty($this->_get['od'])?mysql_real_escape_string($this->_get['od']):$sortColumn[0];
		$orderType=in_array($orderType,$sortColumn)?$orderType:$sortColumn[0];
		$sort = $orderType." ".$sortType;//組sort規則
		/*if($this->_get['od'])=='headline'){
			$sort = "pd.".$orderType." ".$sortType;
		}*/
		
		//echo $sort;
		
		//組前台用排序
		$sortType2 = $sortType=='DESC'?'ASC':'DESC';
		$sortMark = $sortType=='DESC'?'▽':'△';
		$order_list="";
		foreach( $sortColumn as $key => $val ){
			//一樣是反的，其他是一樣
			//echo $val."/".$orderType."<br>";
			if($val==$orderType){
				//$order_list .= "<a href='' onclick=\"return fix_args(this, 'sort', '$sortType2')\">$sortHeadline[$key]</a>";
				$order_list .= "<a href='/media/' onclick=\"return fix_args(this, 'sort', '$sortType2')\"><button class='contentHeaderMenuDate' >$sortHeadline[$key] $sortMark</button></a>";
			}else{
				//$order_list .= "<a href='' onclick=\"return fix_args(this, 'od', '$sortColumn[$key]')\")>$sortHeadline[$key]</a>";
				$order_list .= "<a href='/media/' onclick=\"return fix_args(this, 'od', '$sortColumn[$key]')\")><button class='border contentHeaderMenuDate' >$sortHeadline[$key]</button></a>";
			}
			//$order_list .= $col['sortColumn']."<br>";
			//$order_list .= "key=".$key.",val=".$val;
		}
		//echo $order_list;
		
		//下方頁碼相關
		$rowsPerPage = is_numeric($this->_get['rowsPerPage']) ? $this->_get['rowsPerPage'] : 12;//每頁幾筆
		$offset = is_numeric($this->_get['offset']) ? $this->_get['offset'] : 0;//第幾開始
		
		//搜尋判斷
		if($this->_get['keyword']){
		
			//140930:ANN:太奇怪的字不搜
			//if (!preg_match('/^[x80-xff_a-zA-Z0-9]+$/', $this->_get['keyword'])) { 
			//if (!preg_match('/^[a-z0-9A-Z_\x{4e00}-\x{9fa5}]+$/u', $this->_get['keyword'])) { 
			if (!preg_match('/^[\x{4e00}-\x{9fa5}-A-Za-z0-9_:.()\/]+$/u', $this->_get['keyword'])) { 
				alert("關鍵字有誤，不允許特殊符號");
				$keyword ="";
			}else{ 
			
				//$keyword = substr( strip_tags(addslashes(trim($this->_get['keyword']))),0,40);
				$keyword = strip_tags(addslashes(trim($this->_get['keyword'])));
				//$keyword = mysql_real_escape_string($this->_get['keyword']);
				
				//過濾關鍵字
				$this->get_dao('keyword_Filter')->get_array($keyword_filter,Site_DAO_Keyword_Filter::GROUP_LIST);
				for($i=0;$i<count($keyword_filter);$i++){
					$keyword_filter_list[]=$keyword_filter[$i]['headline'];
				}
				if( false === array_search($keyword,$keyword_filter_list) ){
					//接著判斷資料庫有=>update，沒有=>insert
					$this->get_dao('keyword')->get_rows($recordRows,Site_DAO_Keyword::GROUP_LIST,array("keyword"=>$keyword));
					
					if($recordRows!=null){
						// 更新
						$recordRows['search_count']=$recordRows['search_count']+1;
						$success = $this->get_dao('keyword')->update_rows($rMessage, $rReturn ,$recordRows,$recordRows['id'],Site_DAO_Keyword::GROUP_EDIT);
						//echo "success=".$success."<br>";
					}else{
						// insert keyword
						$recordRows['headline']=$keyword;
						$recordRows['search_count']=1;
						$success = $this->get_dao('keyword')->insert_rows($rMessage, $rReturn ,$recordRows,Site_DAO_Keyword::GROUP_ADD);
						//echo "success=".$success."<br>";
					}
				}else{
					//echo '找到了';
					//顯示alert不給搜...
					alert("關鍵字有誤，請重新搜尋");
					$keyword ="";
				}
			}
			$this->_get['keyword']=$keyword;
			//print_r($keyword_filter);
		}
		print_r($keyword);
		//變數集
		$params = array(
		  'dao_params' => array(
			'catalog' => $catalog,
			'keyword' => $keyword,
			'type' => $type,
			'stauts'=>1,
			'sort'=>$sort,
			'date' => $timeNow,
			'startIndex' => $offset-1,
			//'endIndex' => $offset+$rowsPerPage
			'endIndex' => ($catalog==0 && $this->_get['keyword']==null)?40:$offset+$rowsPerPage,//150804:ANN:沒分類的預設
		  ),
		  'get_params' => $this->_get,
		);
		
		//print_r($params);
		
		// layout cookie
		$layoutMode=$ntl->getcookie('cookie_ntl[layout][mode]');
		if(!$layoutMode)
			$layoutMode=2;
		
		if($layoutMode==1){
			$isLayoutMode1=true;
		}else if($layoutMode==2){
			$isLayoutMode2=true;
		}
		
		// 目錄選單
		$catalogArray = $this->get_dao('video')->choose_catalog($type);
		//print_r($catalogArray);
		
		//麵包屑
		$rootPath = '0';
		$catalogDAO = $this->get_dao($_catalog_type[$type]);
		
		//判斷分類
		//$catalog在前面判斷
		$isCatalogPage = isset($catalog) && $catalogDAO->get_rows_byId($recordRows, '', $catalog);
		
		//在最後面加就好了...
		$fPath=0;
		$catalogParentArray =Array();
		
		//如果有目錄要抓目錄路徑
		$parents=$catalogDAO->tree_func_parent_all($catalog);
		//print_r($parents);
		 //根目錄
         //$currentfPath = $rootPath;
		 //echo (Site::href_link($this->_path_rows['path'],array('catalog'=>$this->_get['catalog'])));
		//$this->_breadcrumb->add($catalogParentRows[Site_DAO_Video::COLUMN_HEADLINE], Site::href_link($this->_path_rows['path'], array(Site::VAR_FPATH => $catalogParentRows[Site_DAO_Video::COLUMN_RECORD_ID])));
		foreach($parents as $row){
			for($i=0;$i<count($catalogArray);$i++){
				if($catalogArray[$i]['id']==$row){
					$this->_breadcrumb->add($catalogArray[$i]['headline'],Site::href_link($this->_path_rows['path'],array('catalog'=>$catalogArray[$i]['id'],'type'=>$type)));
				}
			}
		}
		//自己
		if(isset($catalog)){
			for($i=0;$i<count($catalogArray);$i++){
				if($catalogArray[$i]['id']==$catalog){
					$this->_breadcrumb->add($catalogArray[$i]['headline']);
				}
			}
		}

		
		//只撈一層
		if($catalogArray!=null){
			foreach( $catalogArray as $catalogRow ){
				if($catalogRow['parent_id']==0){
					$catalogMenu[$catalogRow['id']]=$catalogRow;
				}
				if($catalogRow['lv']==1){
					$pid = $catalogRow['parent_id'];
					if($catalogMenu[$pid]['child']==null)
						$catalogMenu[$pid]['child']=array();
					array_push($catalogMenu[$pid]['child'],$catalogRow);
				}
			}
			//要套鬍子的關係讓它從0開始
			$catalogMenuArray=array();
			foreach( $catalogMenu as $catMenuRow ){
				$catalogMenuaArray[]=$catMenuRow;
			}
			//print_r($catalogMenu);
		}
		//print_r($catalogMenuaArray);
		$tree_menu = CM_Template::get_tpl_html('treeMenu',array(
			'catalogMenu' => $catalogMenuaArray,
			'type' => $type
		));
		//$this->newsClass($catalogMenuaArray);
		//echo $tree_menu;
		
		$data['newsClass'] = $this->newsClass($catalogMenuaArray);
		if(!empty($data['newsClass'])){
			$data['hasClass']=true;
		}
	
	
		// 取得分類下資料夾(去掉根目錄)
		if($catalog!=null && $catalog!=0){
			$this->get_dao($_catalog_type[$type])->get_array($dir,Site_DAO_Catalog_Video::GROUP_DIR,array('parent_id' => $catalog));
		}
		//資料夾套版用
		$tempRows=0;
		while($tempRows<count($dir)){
			if($tempRows%4==0){
				//開始
				$dir[$tempRows]['startRows']=1;
			}else if(($tempRows+1)%4==0){
				//換行
				$dir[$tempRows]['endRows']=1;
			}else if($tempRows+1==count($dir)){
				//結束
				if(($tempRows+1)%4!=0){
					$dir[$tempRows]['endRows']=1;
				}
			}
			$tempRows++;
		}
		
		
		//取得列表
		//$this->get_dao('video')->get_rows($recordRows,Site_DAO_Video::GROUP_FRONT_LIST,array_merge($params['dao_params'],$params['get_params'],array("isCount"=>true)));
		$this->get_dao('video')->get_rows($recordRows,Site_DAO_Video::GROUP_FRONT_LIST,array_merge($params['get_params'],$params['dao_params'],array("isCount"=>true)));
		$total = $recordRows['total'];
		//$total = 100;
		//$this->get_dao('video')->get_array($recordRows,Site_DAO_Video::GROUP_FRONT_LIST,array_merge($params['dao_params'],$params['get_params']));
		$this->get_dao('video')->get_array($recordRows,Site_DAO_Video::GROUP_FRONT_LIST,array_merge($params['get_params'],$params['dao_params']));
		//日期換算
		$tempRows=0;
		while($tempRows<count($recordRows)){
			$recordRows[$tempRows]['id']=htmlentities($recordRows[$tempRows]['id']);
			$recordRows[$tempRows]['start_date']= date('Y-m-d', strtotime($recordRows[$tempRows]['start_date']) );
			$recordRows[$tempRows]['clickcount']=$recordRows[$tempRows]['clickcount']==null?0:$recordRows[$tempRows]['clickcount'];
			$recordRows[$tempRows]['isVideo']=$recordRows[$tempRows]['type']==1?1:0;
			$recordRows[$tempRows]['video_length']=$this->getFormatVideoLength($recordRows[$tempRows]['video_length']);
			//141218:ANN:音檔縮圖判斷(取副檔名)
			$extFile=strrchr($recordRows[$tempRows]['file'],".");
			//echo $extFile."<br>";
			if($extFile=='.mp3' || $extFile=='.wma'){
				if($recordRows[$tempRows]['file_img']==null){
					//$recordRows[$tempRows]['file_img']='upload_thumbnail/music.jpg';
					$recordRows[$tempRows]['file_img']='upload_thumbnail/picture.jpg';
				}
			}
			//$recordRows[$tempRows]['id']
			if($tempRows%4==0){
				//開始
				$recordRows[$tempRows]['startRows']=1;
			}else if(($tempRows+1)%4==0){
				//換行
				$recordRows[$tempRows]['endRows']=1;
			}else if($tempRows==count($recordRows)){
				//結束
				if(($tempRows+1)%4!=0){
					$recordRows[$tempRows]['endRows']=1;
				}
			}
			$tempRows++;
		}
		//print_r($recordRows);

		//頁數判斷
		$prePage=array(12,24,36,72);
		$page_options = $this->setOptions($prePage,$this->_get['rowsPerPage']);		
		
		//下方頁碼
		//$ntl->assign('pagination', ($recordCount)?pagination($_SERVER['REQUEST_URI'], $recordCount, $rowsPerPage, $offset):'');
		$pagination=pagination($_SERVER['REQUEST_URI'], $total, $rowsPerPage, $offset);//總,每頁,第幾開始
		
		$file_list = CM_Template::get_tpl_html('layout'.$layoutMode,array('data' => $recordRows));
		//print_r($recordRows);		
		
		//套版
		echo $this->main_page(array(
			'm_header' => $this->main_header(array('hasClass' => $data['hasClass'],'dataClass' => $data['newsClass'])),
			'm_main' => CM_Template::get_tpl_html('mediaDefault', array(
			  'get'=>$params['get_params'],
			  //'xajax_javascript'=>$xajax_javascript,
			  'hasClass' => $data['hasClass'],
			  'dataClass' => $data['newsClass'],
			  //'dataClass'=> $this->newsClass($catalogMenuaArray),
              'data' => $recordRows,
              'dataList' => $listJSON->data,
			  'order_list'=>$order_list,
			  'layout'=>array('isLayoutMode1'=>$isLayoutMode1,'isLayoutMode2'=>$isLayoutMode2),
			  'hasDir'=>count($dir)>0?array('dir'=>$dir):0,
			  'file_list'=>$file_list,
			))
		));
		/*
		echo CM_Template::get_tpl_html('mediaDefault',array(
			'get'=>$params['get_params'],
			'xajax_javascript'=>$xajax_javascript,
			'dir'=>$dir,
			'order_list'=>$order_list,
			'file_list'=>$file_list,
			'page_options'=>$page_options,
			'tree_menu'=>$tree_menu,
			'pagination'=>$pagination,
			'searchForm' => $this->getSearchForm(),
			'breadcrumb' => array(
			  'hasTrail' => true,
			  'trail' => $this->_breadcrumb->trail()
			)
			
		));
		*/
	}

	
	// iframe頁面
    public function page_iframe() {
	  
	  // 取轉檔設定
	  $this->getTransConfig();
	  
	  // 取get值 
	  	$user_id = $this->_get['spoc']!=null? $this->_get['spoc']:'';
		
		//擋掉奇怪的操作
		if($user_id!='' && preg_match('/^[a-za-z0-9]/',$user_id) !=1){
			echo "操作錯誤";
			die();
		}else if($this->_get['recordId']==null){
			echo "操作錯誤";
			die();
		}
	  
	  // 資料庫取資料
	  $this->get_dao('video')->get_rows($recordRows,Site_DAO_Video::GROUP_IFRAME,array('recordId' => $this->_get[Site::VAR_RECORD_ID]));
	  if($recordRows==null){
		echo "操作錯誤";
		die();
	  }
	  
	  //取domain
	  $domain = parse_url($_SERVER['HTTP_REFERER'], PHP_URL_HOST);
	  //判斷網域，帶不同的參數
	  $dno = $this->checkDomain($domain);
	  echo ("<script>console.log('dno: ".$dno."');</script>");
	  
	  //print_r($recordRows);
	  /*$video_id = $recordRows['id'];
	  $file_name = $recordRows['file'];
	  $trans_count = $recordRows['process_count'];*/
	  
	  
	  //echo 'transtype='.$TRANSFORM_SET['TYPE'].'<br>';
		/*echo CM_Template::get_tpl_html('iframeDefault',array(
				'vod_flash_server' => '61.63.6.155',
				'videoData'=>$this->getVideoData($recordRows,$user_id),
				'trans_type' => $GLOBALS['TRANS_TYPE'],
				//'is_auto' => $close_menu==1?$GLOBALS['IS_AUTO']:false
				'has_menu' => $GLOBALS['HAS_MENU']
		));*/
		
		 //權限判斷
		/*$chkMvStatus=1;
		if($recordRows['copyright_status']==2){
			//會員(判斷有沒有登入，有登入就ok，沒登入bye
			$chkMvstatus=0;
		}*/
		
		//裝置判斷
		/*$agent_tag=$this->getAgentType();
		
		if($agent_tag=='web'){
			
			$isPc = true;
			$player = CM_Template::get_tpl_html('box_player',array(
					'vod_flash_server' => $this->_StreamServerPath,
					'videoData'=>$this->getVideoData($recordRows,$user_id),
					'thumbnail'=> $recordRows['file_img'],
					'trans_type' => $GLOBALS['TRANS_TYPE'],
					'has_menu' => $GLOBALS['HAS_MENU']
			));
			
		}else{
			
			$isPic = false;
			
			if($agent_tag=='ipad' ||$agent_tag=='androidtablet'){
				$file_tag=2;
			}else{
				$file_tag=1;
			}
			
			$filename = $this->getFileName($recordRows['file']);
			//$file_url = 'http://'.$this->_StreamServerPath.':8080/vod/_definst_/'.$filename.'_1.mp4/playlist.m3u8?iMda_seq='.$recordRows['id'].'-Mdaf_seq='.'-iUsr_seq='.$user_id.'-iUgp_seq=';
			$file_url = 'http://'.$this->_StreamServerPath.':8080/vod/_definst_/'.$filename.'_'.$file_tag.'.mp4/playlist.m3u8?iMda_seq='.$recordRows['id'].'-Mdaf_seq='.'-iUsr_seq='.$user_id.'-iUgp_seq=';
			
			$thumb_url = "/upload/".$recordRows['file_img'];
		
			$player = CM_Template::get_tpl_html('box_mplayer',array(
				'thumb_url'=>$thumb_url,
				'file_url'=>$file_url,
				'agent_tag'=>$agent_tag
			));
			
		}*/
		//$user_id = $this->_session[Site::VAR_CUSTOMERS_ID];
		$player = $this->getPlayer($recordRows,$user_id,$dno);//iframe的條件應該會不一樣...
		$isPc = $GLOBALS['isPc'];
		
		echo CM_Template::get_tpl_html('iframe',array(
			'isPc'=> $isPc,
			'player' => $player,
		));
	  
    }
	
	// 詳細資訊頁面
    public function page_detail() {
	
		//ajax (放if裡面會壞掉，所以放外面)
		$ntl = $this->_ntl;
		$this->_xajax_javascript = $ntl->render_xajax_javascript(array(array($this,'front_login'),array($this,'front_logout')), false);
	
		//判斷有資料會沒資料會
		if(isset($this->_get[Site::VAR_RECORD_ID]) && $this->get_dao('video')->get_rows($recordRows,Site_DAO_News::GROUP_DETAIL, array('recordId' => $this->_get[Site::VAR_RECORD_ID], 'isCallback' => true))) {
			
	  
			// 取轉檔設定
			$this->getTransConfig();
	  
			//取得欄位(名稱)
			$listColumn = array_merge($this->get_dao('video')->get_column_field_label_array(Site_DAO_Video::GROUP_DETAIL));
			//print_r($listColumn);
	  
			// 資料庫取資料
			$this->get_dao('video')->get_rows($recordRows,Site_DAO_Video::GROUP_DETAIL,array('recordId' => $this->_get[Site::VAR_RECORD_ID]));
			//print_r($recordRows);
	  
			//cpoyright文字判斷...
			if($recordRows['copyright_status']==1){
				//去選資料庫的資料出來
			}
	  
			//日期換算
			$recordRows['start_date']= date('Y-m-d', strtotime($recordRows['start_date']) );
			$recordRows['clickcount']= $recordRows['clickcount']==null?0:$recordRows['clickcount'];
			$recordRows['video_length']=$this->getFormatVideoLength($recordRows['video_length']);
			
			$iframe_url = "<iframe id='ifr' frameborder='0' scrolling='no' width='480px' height='270px' src='http://".$_SERVER['HTTP_HOST']."/iframe/?recordId=".$recordRows['id']."'></iframe>";
	  
			//權限判斷
			//$chkMvStatus=1;
			//if($recordRows['copyright_status']==2){
			//	//會員(判斷有沒有登入，有登入就ok，沒登入bye
			//	$chkMvstatus=0;
			//}
			$user_id = $this->_session[Site::VAR_CUSTOMERS_ID];
			$player = $this->getPlayer($recordRows,$user_id,1);//1是本站
			$isPc = $GLOBALS['isPc'];
			
			//150209:ANN:前台欄位
			$recordRows['login_status']=CM_Lang::line("DAO_Video.COLUMN_LOGIN_STATUS_".$recordRows['login_status']);
			$recordRows['attr_2']=CM_Lang::line("DAO_Video.COLUMN_LANG_".$recordRows['attr_2']);
	  
			//套版
			echo $this->main_page(array(
				//'m_header' => $this->main_header(array('dataClass' => $data['newsClass'])),
				'm_main' => CM_Template::get_tpl_html('mediaDetail', array(
				  'dataClass' => $data['newsClass'],
				  'data' => $recordRows,
				  'dataList' => $listJSON->data,
				  'isPc'=> $isPc,
				  'player' => $player,
				  'iframe_url'=>$iframe_url,
				  //'title' => $title,
				  //'data' => $data,
				))
			));
			
			
		}else{
			//導回列表
			CM_Output::redirect(self::PATH_MAIN);
		}
		
	  
    }
	
	//產生player
	public function getPlayer($recordRows,$user_id,$domain_id){
	
		//echo 'domain_id='.$domain_id."<br>";
		//權限判斷
		$chkStatus=1;
		if($recordRows['login_status']==2){
			//會員(判斷有沒有登入，有登入就ok，沒登入bye
			if($this->_session[Site::VAR_AUTH]==false && empty($this->_session[Site::VAR_AUTH])){
				$chkStatus=0;
			}
		}

	  
		//裝置判斷
		$agent_tag=$this->getAgentType();
		
		if($agent_tag=='web'){
			
			//PC
			$isPc = true;
			//domain>1都可播，其他要check權限
			if($chkStatus==1 || $domain_id>1){
				$player = CM_Template::get_tpl_html('box_player',array(
						'vod_flash_server' => $this->_StreamServerPath,
						'videoData'=>$this->getVideoData($recordRows,$user_id,$domain_id),
						'thumbnail'=> $recordRows['file_img'],
						'trans_type' => $GLOBALS['TRANS_TYPE'],
						'has_menu' => $GLOBALS['HAS_MENU']
				));
			}else if( $chkStatus==0 && $domain_id==null){
				//出現縮圖導回播放頁
				//$player = "<div class='play_intro' style='overflow:hidden;'>back to play page</div>";
				$thumb_url = "http://".$_SERVER['HTTP_HOST']."/upload/".$recordRows['file_img'];
				$play_url = "http://".$_SERVER['HTTP_HOST']."/media/detail/?recordId=".$recordRows['id'];

				$player = CM_Template::get_tpl_html('box_uplayer',array(
					'thumb_url'=>$thumb_url,
					'file_url'=>$play_url,
					'target'=>'_blank'
				));
			}else{
				$player = "<div class='play_intro' style='overflow:hidden;'><img border='0' src='/website/tmpl/img/nv1_1.jpg' width='100%' height='100%'></div>";
			}
			
		}else{
		
			//行動裝置
			$isPc = false;
			
			if($agent_tag=='ipad' || $agent_tag=='androidtablet'){
				$file_tag=2;
			}else{
				$file_tag=1;
			}
			
			//Terry報表判斷要加值
			if($domain_id==null){
				$domain_id=0;
			}
			if($user_id==null){
				$user_id=($domain_id==0)?'null':$domain_id;
			}
			
			$foldername = $this->getFolderName($recordRows['file']);
			$filename = $this->getFileName($recordRows['file']);
			//$file_url = 'http://'.$this->_StreamServerPath.':8080/vod/_definst_/'.$filename.'_1.mp4/playlist.m3u8';
			//$file_url = 'http://'.$this->_StreamServerPath.':8080/vod/_definst_/'.$foldername.$filename.'_'.$file_tag.'.mp4/playlist.m3u8';
			$file_url = 'http://'.$this->_StreamServerPath.':8080/vod/_definst_/'.$foldername.$filename.'_'.$file_tag.'.mp4/playlist.m3u8?iMda_seq='.$recordRows['id'].'-Mdaf_seq='.'-iUsr_seq='.$user_id.'-iUgp_seq='.'-iDom_seq='.$domain_id;
			
			$thumb_url = "/upload/".$recordRows['file_img'];
			
			if($chkStatus==1){
				$player = CM_Template::get_tpl_html('box_mplayer',array(
					'thumb_url'=>$thumb_url,
					'file_url'=>$file_url,
					'agent_tag'=>$agent_tag,
					'test'=>strstr($_SERVER['HTTP_USER_AGENT'],"Mobile")
				));
			}else{
				$player = "<div class='play_intro' style='overflow:hidden;'><img border='0' src='/website/tmpl/img/nv1_1.jpg' width='100%' height='100%'></div>";
			}
		}
		$GLOBALS['isPc']=$isPc;
		return $player;
	
	}
	
	
	//產生每頁筆數選單
	public function setOptions($arr,$selected){
		$str="";
		foreach($arr as $val){
			$str .="<option label=".$val." value=".$val;
			if($val==$selected)
				$str .=" selected=selected ";
			$str .=">".$val."</option>";
		}
		return $str;
	}
	
	
	//版面切換
	public function layoutSetuping($layoutMode)
	{
		if($layoutMode!=1 && $layoutMode!=2){
			$layoutMode=2;//預設
		}
		
		//$ntl = $GLOBALS['ntl'];
		$ntl = $this->_ntl;
		
		//echo 123;
		$objResponse = new xajaxResponse();

		$ntl->setcookie('cookie_ntl[layout][mode]', $layoutMode);
		
		if ($form['url']=='')
			$objResponse->script('window.location.reload()');
			//$objResponse->script("alert(window.location.href + '?layoutMode='+$layoutMode)");
			//$objResponse->script("window.location.href =window.location.href + '?layoutMode='+$layoutMode");
		else
			$objResponse->redirect(htmlentities($form['url']));
		return $objResponse;
	/*	
		$objResponse = new xajaxResponse();
		
		$objResponse->alert($layoutMode);
		
		return $objResponse;
	*/
	/*
		if (trim($layoutMode) == "")
			$objResponse->addAlert("名字尚未輸入!");
		else
			//$objResponse->addAssign("outputDiv","innerHTML",$username.'，你好!!');
			$objResponse->addAlert($layoutMode);
		return $objResponse;	
	*/
	}
	
	//檔案路徑處理
	public function getFolderName($fname){
		$strlen = strpos($fname,'/')+1;
		$endlen = strrpos($fname,'/')+1;
		$foldername = substr($fname,$strlen,$endlen-$strlen);
		return $foldername;
	}
	
	
	//檔名處理
	public function getFileName($fname){
		$strlen = strrpos($fname,'/')+1;
		$endlen = strrpos($fname,'.');
		$filename = substr($fname,$strlen,$endlen-$strlen);
		return $filename;
	}
	
	
	//判斷使用者介面
	public function getAgentType(){

		if(stristr($_SERVER['HTTP_USER_AGENT'],"android")){
			if(stristr($_SERVER['HTTP_USER_AGENT'],"mobile") ){
				$agent_tag='android';
			}else{
				$agent_tag='androidtablet';
			}
		
		}else{
						
			if(strstr($_SERVER['HTTP_USER_AGENT'],"iPhone")){
				$agent_tag='iphone';
			}elseif(strstr($_SERVER['HTTP_USER_AGENT'],"iPad")){
				$agent_tag='ipad';
			}else{
			
				$agent_tag='web';
			}
					
		}
		
		return $agent_tag;
		//$ntl->assign('agent_tag', $agent_tag);
	}
	
	//搜尋表單
	public function getSearchForm(){
		return array(
			//'formTop' => CM_Html::form('searchForm', Site::href_link(self::PATH_MAIN), 'get', 'class="cm-form form-horizontal" role="form"'),
			'formTop' => CM_Html::form('searchForm', '../media/', 'get', 'class="cm-form form-horizontal" role="form"'),
			//'formSearch' => CM_Html::input_field('keyword', $this->_get['keyword']),
			'formSearch' => CM_Html::input_field('keyword'),
			'formButton' => CM_Html::submit_btn('<i class="icofont-search"></i>&nbsp;' .CM_Lang::line('Site.BTN_SEARCH'), array('class' => 'btn btn-primary')),
			'radioButton' => '&nbsp;'.CM_html::radio_field( 'type',  1,  true).'&nbsp;影片&nbsp;'.CM_html::radio_field( 'type',  3,  false). '&nbsp;相片&nbsp;',
			'formBottom' => '</form>'
		);
	}
	
	//搜尋表單
	public function getSearchForm_full(){
		return array(
			//'formTop' => CM_Html::form('searchForm', Site::href_link(self::PATH_MAIN), 'get', 'class="cm-form form-horizontal" role="form"'),
			'formTop' => CM_Html::form('searchForm', '../media/', 'get', 'class="cm-form form-horizontal" role="form"'),
			'formSearch' => '<p>類型：'.CM_html::radio_field( 'type',  1,  true).'&nbsp;影片&nbsp;'.CM_html::radio_field( 'type',  3,  false). '&nbsp;相片</p>'.
			'<p>關鍵字：'.CM_Html::input_field('keyword', $this->_get['keyword']).'</p>'.
			'<p上架日期：'.CM_Html::input_field('date', $this->_get['date']).'</p>'.
			'<p出版、製作日期：'.CM_Html::input_field('keyword', $this->_get['keyword']).'</p>'.
			'<p出版、經銷者：'.CM_Html::input_field('keyword', $this->_get['keyword']).'</p>',
			'formButton' => CM_Html::submit_btn('<i class="icofont-search"></i>&nbsp;' .CM_Lang::line('Site.BTN_SEARCH'), array('class' => 'btn btn-primary')),
			'formBottom' => '</form>'
		);
	}
	
	//進階搜尋
	public function page_search() {
	
		//麵包屑(無分類，不用另外處理)
		
		/*
		echo CM_Template::get_tpl_html('mediaSearch',array(
			'get'=>$params['get_params'],
			'searchForm' => $this->getSearchForm_full(),
			'breadcrumb' => array(
			  'hasTrail' => true,
			  'trail' => $this->_breadcrumb->trail()
			)
		));
		*/
		//取得類別
		$type=1;//預設為影片
		$catalogArray = $this->get_dao('video')->choose_catalog($type);
		//只撈一層
		if($catalogArray!=null){
			foreach( $catalogArray as $catalogRow ){
				if($catalogRow['parent_id']==0){
					$catalogMenu[$catalogRow['id']]=$catalogRow;
				}
				if($catalogRow['lv']==1){
					$pid = $catalogRow['parent_id'];
					if($catalogMenu[$pid]['child']==null)
						$catalogMenu[$pid]['child']=array();
					array_push($catalogMenu[$pid]['child'],$catalogRow);
				}
			}
			//要套鬍子的關係讓它從0開始
			$catalogMenuArray=array();
			foreach( $catalogMenu as $catMenuRow ){
				$catalogMenuaArray[]=$catMenuRow;
			}
		}
		//print_r($catalogMenuaArray);
		
		//套版
		echo $this->main_page(array(
			'm_main' => CM_Template::get_tpl_html('mediaSearch', array(
			  'get'=>$params['get_params'],
			  'searchForm' => $this->getSearchForm_full(),
			  'videoCatalog' => $catalogMenuaArray
			))
		));
	}
	
	//domain判斷
	public function checkDomain($domain_url){
		
		$this->get_dao('domain')->get_array($domainRows,Site_DAO_Domain::GROUP_LIST,array('status' => 1));
		
		for($i=0;$i<count($domainRows);$i++){
			if($domainRows[$i]['domain']==$domain_url){
				$domain_id=$domainRows[$i]['id'];
			}
		}
		
		return $domain_id;
	}
	
	// 下載檔案
	public function page_download() {
		
		if($this->_get['file_url']!=null){
			$tempfile=mysql_real_escape_string($this->_get['file_url']);//檔案名稱
			//這之後可能要切割傳來的檔案分資料夾&檔名
			$output = explode("/", $tempfile);
			$path=$output[0];
			$file=$output[1];
			$url="http://".$_SERVER['HTTP_HOST']."/upload/".$path."/"; //路徑位置
			$num=date("Ymds");	
			//header("Content-type:application");
			//header('Content-type:application/force-download');
			
			header('Content-Description: File Transfer');
			header('Content-type:application/octet-stream ');
			header('Content-Disposition: attachment; filename='.$num.'_'.$file);
			header('Expires: 0');
			header('Cache-Control: public');
			header('Pragma: public');
			ob_clean();
			flush();
			//readfile($fileUrl);
			readfile($url.str_replace('@','',$file));	
			exit(0);
		}else{
			echo "找不到相關檔案....";
		}
	}
	
	//前面條件和list一樣，只是有些資料中文字需要uridecode處理
	public function page_json() {
	
		//把照片列表echo出來(前面帶一樣的變數)
		$timeNow = date('Y-m-d');
		
		//排序判斷(預設欄位)
		$sortColumn=array('start_date','video_length','clickcount');//上架日期、片長、點擊次數
		$sortHeadline=array('上架日期','片長','點擊次數');		
		
		//$sortType=$this->_get['sort']==null?'DESC':mysql_real_escape_string($this->_get['sort']);
		$sortType=!empty($this->_get['sort'])?mysql_real_escape_string($this->_get['sort']):'DESC';
		$sortType=(strtoupper($sortType)!='DESC'&&strtoupper($sortType)!='ASC')?'DESC':$sortType;
		$orderType=!empty($this->_get['od'])?mysql_real_escape_string($this->_get['od']):$sortColumn[0];
		$orderType=in_array($orderType,$sortColumn)?$orderType:$sortColumn[0];
		//$sort = "p.".$orderType." ".$sortType;
		$sort = $orderType." ".$sortType;//有些資料表裡頭沒有...
		
		//echo $sort;
		//組前台用排序
		$sortType2 = ($sortType=='DESC'?'ASC':'DESC');
		$order_list="";
		foreach( $sortColumn as $key => $val ){
			//一樣是反的，其他是一樣
			//echo $val."/".$orderType."<br>";
			if($val==$orderType){
				//$order_list .= "<a href='' onclick=\"return fix_args(this, 'sort', '$sortType2')\">$sortHeadline[$key]</a>";
				$order_list .= "<button class='contentHeaderMenuDate' onclick=\"return fix_args(this, 'sort', '$sortType2')\">$sortHeadline[$key]</button>";
			}else{
				//$order_list .= "<a href='' onclick=\"return fix_args(this, 'od', '$sortColumn[$key]')\")>$sortHeadline[$key]</a>";
				$order_list .= "<button class='border contentHeaderMenuDate' onclick=\"return fix_args(this, 'od', '$sortColumn[$key]')\")>$sortHeadline[$key]</button>";
			}
			//$order_list .= $col['sortColumn']."<br>";
			//$order_list .= "key=".$key.",val=".$val;
		}
		//echo $order_list;
		
		//下方頁碼相關
		$rowsPerPage = is_numeric($this->_get['rowsPerPage']) ? $this->_get['rowsPerPage'] : 12;//每頁幾筆
		$offset = is_numeric($this->_get['offset']) ? $this->_get['offset'] : 0;//第幾開始
		
		//搜尋判斷
		if($this->_get['keyword']){
			//140930:ANN:太奇怪的字不搜
			//$keyword = substr( strip_tags(addslashes(trim($this->_get['keyword']))),0,40);
			//$keyword = mysql_real_escape_string($this->_get['keyword']);
			if (!preg_match('/^[a-z0-9A-Z_\x{4e00}-\x{9fa5}]+$/u', $this->_get['keyword'])) { 
				alert("關鍵字有誤，不允許特殊符號");
				$keyword ="";
			}else{ 
				//過濾關鍵字
				$this->get_dao('keyword_Filter')->get_array($keyword_filter,Site_DAO_Keyword_Filter::GROUP_LIST);
				for($i=0;$i<count($keyword_filter);$i++){
					$keyword_filter_list[]=$keyword_filter[$i]['headline'];
				}
				if( false === array_search($keyword,$keyword_filter_list) ){
					
				}else{
					//echo '找到了';
					//顯示alert不給搜...
					alert("關鍵字有誤，請重新搜尋");
					$keyword ="";
				}
			}
			$this->_get['keyword']=$keyword;
			//print_r($keyword_filter);
		}
		
		//變數集
		$params = array(
		  'dao_params' => array(
			'catalog' => $this->_get['catalog'],
			//'keyword' => $this->_get['keyword'],
			'keyword' => $keyword,
			'type' => $this->_get['type'],
			'stauts'=>1,
			'sort'=>$sort,
			'date' => $timeNow,
			'startIndex' => $offset,
			'endIndex' => $offset+$rowsPerPage,
		  ),
		  'get_params' => $this->_get,
		);
		//print_r($params);
		
		//取得列表
		$this->get_dao('video')->get_rows($recordRows,Site_DAO_Video::GROUP_FRONT_LIST,array_merge($params['dao_params'],$params['get_params'],array("isCount"=>true)));
		$total = $recordRows['total'];
		//$total = 100;
		$this->get_dao('video')->get_array($recordRows,Site_DAO_Video::GROUP_FRONT_LIST,array_merge($params['dao_params'],$params['get_params']));
		
		//日期換算
		$_exceptFile=array('cdr','psd','ai');
		$tempRows=0;
		while($tempRows<count($recordRows)){
			$recordRows[$tempRows]['start_date']= date('Y-m-d', strtotime($recordRows[$tempRows]['start_date']) );
			$recordRows[$tempRows]['isVideo']=$recordRows[$tempRows]['type']==1?1:0;
			//$recordRows[$tempRows]['headline']=urlencode($recordRows[$tempRows]['headline']);
			//$recordRows[$tempRows]['desc']=urlencode($recordRows[$tempRows]['desc']);
			$recordRows[$tempRows]['headline']=($recordRows[$tempRows]['headline']);
			$recordRows[$tempRows]['desc']=($recordRows[$tempRows]['desc']);
			// 圖片判斷psd,cdr,ai
			if($recordRows[$tempRows]['type']==3){
				$extFile = substr(strrchr($recordRows[$tempRows]['file'],'.'),1);
				if(false!==array_search($extFile,$_exceptFile)){
					$recordRows[$tempRows]['hasExt']=$extFile;
				}
			}
			$tempRows++;
		}

		//頁數判斷
		$prePage=array(12,24,36,72);
		$page_options = $this->setOptions($prePage,$this->_get['rowsPerPage']);		
		
		//下方頁碼
		//$ntl->assign('pagination', ($recordCount)?pagination($_SERVER['REQUEST_URI'], $recordCount, $rowsPerPage, $offset):'');
		$pagination=pagination($_SERVER['REQUEST_URI'], $total, $rowsPerPage, $offset);//總,每頁,第幾開始
		
		
		$file_list = CM_Template::get_tpl_html('layout'.$layoutMode,array('data' => $recordRows));
		//print_r($recordRows);
		//echo "this is page_list";
		echo json_encode($recordRows);
	}
	
	
	//臨時寫的，先拿來測pic功能
	// 預設頁面
    public function page_pic() {
		
		//變數集
		$params = array(
		  'dao_params' => array(
			'catalog' => $this->_get['catalog'],
			//'keyword' => $this->_get['keyword'],
			'keyword' => $keyword,
			'type' => 3,
			'stauts'=>1,
			'sort'=>$sort,
		  ),
		  'get_params' => $this->_get,
		);
		
		//取得列表
		$this->get_dao('video')->get_rows($recordRows,Site_DAO_Video::GROUP_FRONT_LIST,array_merge($params['dao_params'],$params['get_params'],array("isCount"=>true)));
		$total = $recordRows['total'];
		//$total = 100;
		$this->get_dao('video')->get_array($recordRows,Site_DAO_Video::GROUP_FRONT_LIST,array_merge($params['dao_params'],$params['get_params']));
		//print_r($recordRows);
		
		//特殊處理
		$_exceptFile=array('cdr','psd','ai');
		$tempRows=0;
		while($tempRows<count($recordRows)){
			// 圖片判斷psd,cdr,ai
			if($recordRows[$tempRows]['type']==3){
				$extFile = substr(strrchr($recordRows[$tempRows]['file'],'.'),1);
				//echo array_search($extFile,$_exceptFile)."<br>";
				if(false!==array_search($extFile,$_exceptFile)){
					$recordRows[$tempRows]['hasExt']=$extFile;
					//這邊被我改掉了，原本想在套版處理，要改到吐json檔那邊處理
					//echo "<br>$extFile<br>";
				}
				//array_search($tempUri,$exceptFile);
				//echo "<br>".$tempUri.":".array_search($tempUri,$exceptFile)."<br>";
			}
			$tempRows++;
		}
		
		//取得全部的列表後塞值?
		echo CM_Template::get_tpl_html('pic',array(
			//'data'=> $recordRows,
			'get_params' => $this->_get,
		));
	}
	
	// 取得檔名
	//{ url: 'mp4:cmvod/1205/1205_1689_one.mp4?iMda_seq=1205-iMdaf_seq=-iUsr_seq=-iUgp_seq=', width: 320, bitrate: 300,label:"300" }
	public function getVideoData($row,$user_id,$domain_id){
		
		//如果是固定，已經有值，照值跑；不固定則根據資料庫數字更動
		if(count($GLOBALS['TRANS_BITRATE'])>0){
			$trans_count=count($GLOBALS['TRANS_BITRATE']);
		}else{
			$trans_count=$row['process_count'];//資料庫取得的
			//$trans_count=2;//資料庫取得的
		}
		//print_r($GLOBALS['TRANS_BITRATE_SET']);
		
		//不固定取對應的值
		if($GLOBALS['TRANS_TYPE']==2){
			$GLOBALS['TRANS_BITRATE']=explode(",",$GLOBALS['TRANS_BITRATE_SET'][$trans_count]);
		}
		
		//預設值超過則由最大bitrate代替
		if($GLOBALS['TRANS_DEFAULT']>count($GLOBALS['TRANS_BITRATE'])){
			$GLOBALS['TRANS_DEFAULT']=count($GLOBALS['TRANS_BITRATE']);
		}
		
		//Terry報表判斷要加值
		if($domain_id==null){
			$domain_id=0;
		}
		if($user_id==null){
			$user_id=($domain_id==0)?'null':$domain_id;
		}
		
		//取得檔名
		//$filename = substr( (strrchr($row['file'],'/')),1);
		//echo $row['file'];
		$foldername = $this->getFolderName($row['file']);
		$filename = $this->getFileName($row['file']);
		
		for($i=0;$i<$trans_count;$i++){
			//檔名組成
			$fileData[]= array(
				/*'url'=>'mp4:cmvod/1205/1205_1689_'.($i+1).'.mp4?iMda_seq='.$row['id'].'-Mdaf_seq='.'-iUsr_seq='.$user_id.'-iUgp_seq=',*/
				'url'=>'mp4:'.$foldername.$filename.'_'.($i+1).'.mp4?iMda_seq='.$row['id'].'-Mdaf_seq='.'-iUsr_seq='.$user_id.'-iUgp_seq='.'-iDom_seq='.$domain_id,
				//'width'=> $GLOBALS['TRANS_WIDTH'][$i],
				'bitrate'=>$GLOBALS['TRANS_BITRATE'][$i],
				'label'=>$GLOBALS['TRANS_BITRATE'][$i],
				'default'=>($i+1)==$GLOBALS['TRANS_DEFAULT']?'isDefault: true':'',
			);
		}
		//print_r($fileData);
		return $fileData;
	}
	
	// 熱門列表
	public function page_hot() {
		//ajax
		$ntl = $this->_ntl;
		$this->_xajax_javascript = $ntl->render_xajax_javascript(array(array($this,'layoutSetuping'),array($this,'front_login'),array($this,'front_logout')), false);
		
		//判斷錯誤
		$catalog = !empty($this->_get['catalog'])&&is_numeric($this->_get['catalog'])?$this->_get['catalog']:0;
		//類型-->1影片,2聲音,3照片,預設為影片
		$type=!empty($this->_get['type'])&&is_numeric($this->_get['type'])?$this->_get['type']:1;//預設為影片
		if( $type!=1 && $type!=3 ){
			$this->page_notfound();
			die();
		};
		
		$timeNow = date('Y-m-d');
		
		//分類資料庫
		$_catalog_type = array(
			1 => 'catalog_Video',
			2 => 'catalog_Sound',
			3 => 'catalog_Picture'
		);
		
		//排序判斷(預設欄位)*這裡和一般列表不太一樣 
		$sortColumn=array('w','m','y');//每周熱門、每月熱門、年度熱門
		$sortHeadline=array('每周熱門','每月熱門','年度熱門');		
		
		//$sortType=$this->_get['sort']==null?'DESC':mysql_real_escape_string($this->_get['sort']);
		$sortType=!empty($this->_get['sort'])?mysql_real_escape_string($this->_get['sort']):'DESC';
		$sortType=(strtoupper($sortType)!='DESC'&&strtoupper($sortType)!='ASC')?'DESC':$sortType;
		$orderType=!empty($this->_get['od'])?mysql_real_escape_string($this->_get['od']):$sortColumn[0];
		$orderType=in_array($orderType,$sortColumn)?$orderType:$sortColumn[0];
		$sort = "clickcount ".$sortType;//組sort規則
		//echo $sort;
		
		//組前台用排序
		$sortType2 = $sortType=='DESC'?'ASC':'DESC';
		$sortMark = $sortType=='DESC'?'▽':'△';
		$order_list="";
		foreach( $sortColumn as $key => $val ){
			//一樣是反的，其他是一樣
			//echo $val."/".$orderType."<br>";
			if($val==$orderType){
				//$order_list .= "<a href='' onclick=\"return fix_args(this, 'sort', '$sortType2')\">$sortHeadline[$key]</a>";
				$order_list .= "<a href='/media/' onclick=\"return fix_args(this, 'sort', '$sortType2')\"><button class='contentHeaderMenuDate' >$sortHeadline[$key] $sortMark</button></a>";
			}else{
				//$order_list .= "<a href='' onclick=\"return fix_args(this, 'od', '$sortColumn[$key]')\")>$sortHeadline[$key]</a>";
				$order_list .= "<a href='/media/' onclick=\"return fix_args(this, 'od', '$sortColumn[$key]')\")><button class='border contentHeaderMenuDate' >$sortHeadline[$key]</button></a>";
			}
			//$order_list .= $col['sortColumn']."<br>";
			//$order_list .= "key=".$key.",val=".$val;
		}
		//echo $order_list;
		
		//下方頁碼相關
		$rowsPerPage = is_numeric($this->_get['rowsPerPage']) ? htmlentities($this->_get['rowsPerPage']) : 12;//每頁幾筆
		$offset = is_numeric($this->_get['offset']) ? htmlentities($this->_get['offset']) : 0;//第幾開始
		
		//搜尋判斷
		if($this->_get['keyword']){
			//140930:ANN:太奇怪的字不搜
			//$keyword = substr( strip_tags(addslashes(trim($this->_get['keyword']))),0,40);
			//$keyword = mysql_real_escape_string($this->_get['keyword']);
			if (!preg_match('/^[a-z0-9A-Z_\x{4e00}-\x{9fa5}]+$/u', $this->_get['keyword'])) { 
				alert("關鍵字有誤，不允許特殊符號");
				$keyword ="";
			}else{ 
			
				//過濾關鍵字
				$this->get_dao('keyword_Filter')->get_array($keyword_filter,Site_DAO_Keyword_Filter::GROUP_LIST);
				for($i=0;$i<count($keyword_filter);$i++){
					$keyword_filter_list[]=$keyword_filter[$i]['headline'];
				}
				if( false === array_search($keyword,$keyword_filter_list) ){
					//接著判斷資料庫有=>update，沒有=>insert
					$this->get_dao('keyword')->get_rows($recordRows,Site_DAO_Keyword::GROUP_LIST,array("keyword"=>$keyword));
					
					if($recordRows!=null){
						// 更新
						$recordRows['search_count']=$recordRows['search_count']+1;
						$success = $this->get_dao('keyword')->update_rows($rMessage, $rReturn ,$recordRows,$recordRows['id'],Site_DAO_Keyword::GROUP_EDIT);
						//echo "success=".$success."<br>";
					}else{
						// insert keyword
						$recordRows['headline']=$keyword;
						$recordRows['search_count']=1;
						$success = $this->get_dao('keyword')->insert_rows($rMessage, $rReturn ,$recordRows,Site_DAO_Keyword::GROUP_ADD);
						//echo "success=".$success."<br>";
					}
				}else{
					//echo '找到了';
					//顯示alert不給搜...
					alert("關鍵字有誤，請重新搜尋");
					$keyword ="";
				}
				
			}
			$this->_get['keyword']=$keyword;
			//print_r($keyword_filter);
		}
		
		//變數集(多傳了od)
		$params = array(
		  'dao_params' => array(
			'catalog' => $catalog,
			'keyword' => $keyword,
			'type' => $type,
			'stauts'=>1,
			'sort'=>$sort,
			//'od'=> empty($this->_get['od'])?'w':$this->_get['od'],
			'od'=>$orderType,
			'date' => $timeNow,
			'startIndex' => $offset,
			'endIndex' => $offset+$rowsPerPage,
		  ),
		  'get_params' => $this->_get,
		);
		
		//print_r($params);
		//ajax
		//$ntl = $GLOBALS['ntl'];
		//$xajax_javascript = $ntl->render_xajax_javascript(array(array($this,'layoutSetuping')), false);
		
		//cookie
		$layoutMode=$ntl->getcookie('cookie_ntl[layout][mode]');
		if(!$layoutMode)
			$layoutMode=2;
		
		if($layoutMode==1){
			$isLayoutMode1=true;
		}else if($layoutMode==2){
			$isLayoutMode2=true;
		}
		
		
		// 目錄選單
		//$type=isset($this->_get['type'])?$this->_get['type']:1;//預設為影片
		$catalogArray = $this->get_dao('video')->choose_catalog($type);
		//print_r($catalogArray);
		
		//麵包屑
		$rootPath = '0';
		$catalogDAO = $this->get_dao($_catalog_type[$type]);
		
		//判斷分類
		//$catalog寫在一開始判斷
		$isCatalogPage = isset($catalog) && $catalogDAO->get_rows_byId($recordRows, '', $catalog);
		
		//在最後面加就好了...
		$fPath=0;
		$catalogParentArray =Array();
		
		//如果有目錄要抓目錄路徑
		$parents=$catalogDAO->tree_func_parent_all($catalog);
		//print_r($parents);
		 //根目錄
         //$currentfPath = $rootPath;
		 //echo (Site::href_link($this->_path_rows['path'],array('catalog'=>$this->_get['catalog'])));
		//$this->_breadcrumb->add($catalogParentRows[Site_DAO_Video::COLUMN_HEADLINE], Site::href_link($this->_path_rows['path'], array(Site::VAR_FPATH => $catalogParentRows[Site_DAO_Video::COLUMN_RECORD_ID])));
		foreach($parents as $row){
			for($i=0;$i<count($catalogArray);$i++){
				if($catalogArray[$i]['id']==$row){
					$this->_breadcrumb->add($catalogArray[$i]['headline'],Site::href_link($this->_path_rows['path'],array('catalog'=>$catalogArray[$i]['id'],'type'=>$type)));
				}
			}
		}
		//自己
		if(isset($catalog)){
			for($i=0;$i<count($catalogArray);$i++){
				if($catalogArray[$i]['id']==$catalog){
					$this->_breadcrumb->add($catalogArray[$i]['headline']);
				}
			}
		}

		
		//只撈一層
		if($catalogArray!=null){
			foreach( $catalogArray as $catalogRow ){
				if($catalogRow['parent_id']==0){
					$catalogMenu[$catalogRow['id']]=$catalogRow;
				}
				if($catalogRow['lv']==1){
					$pid = $catalogRow['parent_id'];
					if($catalogMenu[$pid]['child']==null)
						$catalogMenu[$pid]['child']=array();
					array_push($catalogMenu[$pid]['child'],$catalogRow);
				}
			}
			//要套鬍子的關係讓它從0開始
			$catalogMenuArray=array();
			foreach( $catalogMenu as $catMenuRow ){
				$catalogMenuaArray[]=$catMenuRow;
			}
			//print_r($catalogMenu);
		}
		$tree_menu = CM_Template::get_tpl_html('treeMenu',array(
			'catalogMenu' => $catalogMenuaArray,
			'type' => $type
		));
		
		//echo $tree_menu;
	
		// 取得分類下資料夾(去掉根目錄)
		if($catalog!=null && $catalog!=0){
			$this->get_dao($_catalog_type[$type])->get_array($dir,Site_DAO_Catalog_Video::GROUP_DIR,array('parent_id' => $catalog));
		}
		//資料夾套版用
		$tempRows=0;
		while($tempRows<count($dir)){
			if($tempRows%4==0){
				//開始
				$dir[$tempRows]['startRows']=1;
			}else if(($tempRows+1)%4==0){
				//換行
				$dir[$tempRows]['endRows']=1;
			}else if($tempRows+1==count($dir)){
				//結束
				if(($tempRows+1)%4!=0){
					$dir[$tempRows]['endRows']=1;
				}
			}
			$tempRows++;
		}
		
		
		//取得列表
		$this->get_dao('video')->get_rows($recordRows,Site_DAO_Video::GROUP_FRONT_LIST,array_merge($params['get_params'],$params['dao_params'],array("isCount"=>true)));
		$total = $recordRows['total'];
		//$total = 100;
		//$this->get_dao('video')->get_array($recordRows,Site_DAO_Video::GROUP_FRONT_LIST,array_merge($params['dao_params'],$params['get_params']));
		$this->get_dao('video')->get_array($recordRows,Site_DAO_Video::GROUP_FRONT_LIST,array_merge($params['get_params'],$params['dao_params']));
		
		//日期換算
		$tempRows=0;
		while($tempRows<count($recordRows)){
			$recordRows[$tempRows]['id']=htmlentities($recordRows[$tempRows]['id']);
			$recordRows[$tempRows]['start_date']= date('Y-m-d', strtotime($recordRows[$tempRows]['start_date']) );
			$recordRows[$tempRows]['clickcount']=$recordRows[$tempRows]['clickcount']==null?0:$recordRows[$tempRows]['clickcount'];
			$recordRows[$tempRows]['isVideo']=$recordRows[$tempRows]['type']==1?1:0;
			$recordRows[$tempRows]['video_length']=$this->getFormatVideoLength($recordRows[$tempRows]['video_length']);
			//141218:ANN:音檔縮圖判斷(取副檔名)
			$extFile=strrchr($recordRows[$tempRows]['file'],".");
			//echo $extFile."<br>";
			if($extFile=='.mp3' || $extFile=='.wma'){
				if($recordRows[$tempRows]['file_img']==null){
					//$recordRows[$tempRows]['file_img']='upload_thumbnail/music.jpg';
					$recordRows[$tempRows]['file_img']='upload_thumbnail/picture.jpg';
				}
			}
			if($tempRows%4==0){
				//開始
				$recordRows[$tempRows]['startRows']=1;
			}else if(($tempRows+1)%4==0){
				//換行
				$recordRows[$tempRows]['endRows']=1;
			}else if($tempRows==count($recordRows)){
				//結束
				if(($tempRows+1)%4!=0){
					$recordRows[$tempRows]['endRows']=1;
				}
			}
			$tempRows++;
		}
		//print_r($recordRows);

		//頁數判斷
		$prePage=array(12,24,36,72);
		$page_options = $this->setOptions($prePage,$this->_get['rowsPerPage']);		
		
		//下方頁碼
		//$ntl->assign('pagination', ($recordCount)?pagination($_SERVER['REQUEST_URI'], $recordCount, $rowsPerPage, $offset):'');
		$pagination=pagination($_SERVER['REQUEST_URI'], $total, $rowsPerPage, $offset);//總,每頁,第幾開始
		
		$file_list = CM_Template::get_tpl_html('layout'.$layoutMode,array('data' => $recordRows));
		//print_r($recordRows);		
		
		//套版
		echo $this->main_page(array(
			//'m_header' => $this->main_header(array('dataClass' => $data['newsClass'])),
			'm_main' => CM_Template::get_tpl_html('mediaHot', array(
			  'get'=>$params['get_params'],
			  'xajax_javascript'=>$xajax_javascript,
			  'dataClass' => $data['newsClass'],
              'data' => $recordRows,
              'dataList' => $listJSON->data,
			  'order_list'=>$order_list,
			  'layout'=>array('isLayoutMode1'=>$isLayoutMode1,'isLayoutMode2'=>$isLayoutMode2),
			  'hasDir'=>count($dir)>0?array('dir'=>$dir):0,
			  'file_list'=>$file_list,
			))
		));
		/*
		echo CM_Template::get_tpl_html('mediaDefault',array(
			'get'=>$params['get_params'],
			'xajax_javascript'=>$xajax_javascript,
			'dir'=>$dir,
			'order_list'=>$order_list,
			'file_list'=>$file_list,
			'page_options'=>$page_options,
			'tree_menu'=>$tree_menu,
			'pagination'=>$pagination,
			'searchForm' => $this->getSearchForm(),
			'breadcrumb' => array(
			  'hasTrail' => true,
			  'trail' => $this->_breadcrumb->trail()
			)
			
		));
		*/
	}
	
	// 照片列表
	public function page_photo_list() {
	
		//ajax
		$ntl = $this->_ntl;
		$this->_xajax_javascript = $ntl->render_xajax_javascript(array(array($this,'layoutSetuping'),array($this,'front_login'),array($this,'front_logout')), false);
		
		//判斷錯誤
		$catalog = !empty($this->_get['catalog'])&&is_numeric($this->_get['catalog'])?$this->_get['catalog']:0;
		//類型-->1影片,2聲音,3照片,預設為照片
		$type = !empty($this->_get['type'])&&is_numeric($this->_get['type'])?$this->_get['type']:3;//預設為照片
		if( $type!=1 && $type!=3 ){
			$this->page_notfound();
			die();
		};
		
		$timeNow = date('Y-m-d');
		
		//分類資料庫(linux大小寫要注意)
		$_catalog_type = array(
			1 => 'catalog_Video',
			2 => 'catalog_Sound',
			3 => 'catalog_Picture'
		);
		
		//排序判斷(預設欄位)
		$sortColumn=array('start_date','video_length','clickcount');//上架日期、片長、點擊次數
		$sortHeadline=array('上架日期','片長','觀看次數');		
		if($type==3){
			$sortColumn=array('start_date');//上架日期
			$sortHeadline=array('上架日期');
		}
		
		//$sortType=$this->_get['sort']==null?'DESC':mysql_real_escape_string($this->_get['sort']);
		$sortType=!empty($this->_get['sort'])?mysql_real_escape_string($this->_get['sort']):'DESC';
		$sortType=(strtoupper($sortType)!='DESC'&&strtoupper($sortType)!='ASC')?'DESC':$sortType;
		$orderType=!empty($this->_get['od'])?mysql_real_escape_string($this->_get['od']):$sortColumn[0];
		$orderType=in_array($orderType,$sortColumn)?$orderType:$sortColumn[0];
		$sort = $orderType." ".$sortType;//組sort規則
		/*if($this->_get['od'])=='headline'){
			$sort = "pd.".$orderType." ".$sortType;
		}*/
		
		//echo $sort;
		
		//組前台用排序
		$sortType2 = $sortType=='DESC'?'ASC':'DESC';
		$sortMark = $sortType=='DESC'?'▽':'△';
		$order_list="";
		foreach( $sortColumn as $key => $val ){
			//一樣是反的，其他是一樣
			//echo $val."/".$orderType."<br>";
			if($val==$orderType){
				//$order_list .= "<a href='' onclick=\"return fix_args(this, 'sort', '$sortType2')\">$sortHeadline[$key]</a>";
				$order_list .= "<a href='/media/' onclick=\"return fix_args(this, 'sort', '$sortType2')\"><button class='contentHeaderMenuDate' >$sortHeadline[$key] $sortMark</button></a>";
			}else{
				//$order_list .= "<a href='' onclick=\"return fix_args(this, 'od', '$sortColumn[$key]')\")>$sortHeadline[$key]</a>";
				$order_list .= "<a href='/media/' onclick=\"return fix_args(this, 'od', '$sortColumn[$key]')\")><button class='border contentHeaderMenuDate' >$sortHeadline[$key]</button></a>";
			}
			//$order_list .= $col['sortColumn']."<br>";
			//$order_list .= "key=".$key.",val=".$val;
		}
		//echo $order_list;
		
		//下方頁碼相關
		$rowsPerPage = is_numeric($this->_get['rowsPerPage']) ? $this->_get['rowsPerPage'] : 12;//每頁幾筆
		$offset = is_numeric($this->_get['offset']) ? $this->_get['offset'] : 0;//第幾開始
		
		//搜尋判斷(照片不算關鍵字，所以先關掉)
		/*if($this->_get['keyword']){
			$keyword = mysql_real_escape_string($this->_get['keyword']);
			//過濾關鍵字
			$this->get_dao('keyword_Filter')->get_array($keyword_filter,Site_DAO_Keyword_Filter::GROUP_LIST);
			for($i=0;$i<count($keyword_filter);$i++){
				$keyword_filter_list[]=$keyword_filter[$i]['headline'];
			}
			if( false === array_search($keyword,$keyword_filter_list) ){
				//接著判斷資料庫有=>update，沒有=>insert
				$this->get_dao('keyword')->get_rows($recordRows,Site_DAO_Keyword::GROUP_LIST,array("keyword"=>$keyword));
				
				if($recordRows!=null){
					// 更新
					$recordRows['search_count']=$recordRows['search_count']+1;
					$success = $this->get_dao('keyword')->update_rows($rMessage, $rReturn ,$recordRows,$recordRows['id'],Site_DAO_Keyword::GROUP_EDIT);
					echo "success=".$success."<br>";
				}else{
					// insert keyword
					$recordRows['headline']=$keyword;
					$recordRows['search_count']=1;
					$success = $this->get_dao('keyword')->insert_rows($rMessage, $rReturn ,$recordRows,Site_DAO_Keyword::GROUP_ADD);
					echo "success=".$success."<br>";
				}
			}else{
				//echo '找到了';
				//顯示alert不給搜...
				alert("關鍵字有誤，請重新搜尋");
				$keyword ="";
			}
			$this->_get['keyword']=$keyword;
			//print_r($keyword_filter);
		}*/
		
		//變數集
		$params = array(
		  'dao_params' => array(
			'catalog' => $catalog,
			'keyword' => $keyword,
			'type' => $type,
			'stauts'=>1,
			'sort'=>$sort,
			'date' => $timeNow,
			'startIndex' => $offset,
			'endIndex' => $offset+$rowsPerPage,
		  ),
		  'get_params' => $this->_get,
		);
		
		//print_r($params);
		
		// layout cookie
		$layoutMode=$ntl->getcookie('cookie_ntl[layout][mode]');
		if(!$layoutMode)
			$layoutMode=2;
		
		if($layoutMode==1){
			$isLayoutMode1=true;
		}else if($layoutMode==2){
			$isLayoutMode2=true;
		}
		
		// 目錄選單
		$catalogArray = $this->get_dao('video')->choose_catalog($type);
		//print_r($catalogArray);
		
		//麵包屑
		$rootPath = '0';
		$catalogDAO = $this->get_dao($_catalog_type[$type]);
		
		//判斷分類
		//$catalog寫在前面判斷
		$isCatalogPage = isset($catalog) && $catalogDAO->get_rows_byId($recordRows, '', $catalog);
		
		//在最後面加就好了...
		$fPath=0;
		$catalogParentArray =Array();
		
		//如果有目錄要抓目錄路徑
		$parents=$catalogDAO->tree_func_parent_all($catalog);
		//print_r($parents);
		 //根目錄
         //$currentfPath = $rootPath;
		 //echo (Site::href_link($this->_path_rows['path'],array('catalog'=>$this->_get['catalog'])));
		//$this->_breadcrumb->add($catalogParentRows[Site_DAO_Video::COLUMN_HEADLINE], Site::href_link($this->_path_rows['path'], array(Site::VAR_FPATH => $catalogParentRows[Site_DAO_Video::COLUMN_RECORD_ID])));
		foreach($parents as $row){
			for($i=0;$i<count($catalogArray);$i++){
				if($catalogArray[$i]['id']==$row){
					$this->_breadcrumb->add($catalogArray[$i]['headline'],Site::href_link($this->_path_rows['path'],array('catalog'=>$catalogArray[$i]['id'],'type'=>$type)));
				}
			}
		}
		//自己
		if(isset($catalog)){
			for($i=0;$i<count($catalogArray);$i++){
				if($catalogArray[$i]['id']==$catalog){
					$this->_breadcrumb->add($catalogArray[$i]['headline']);
				}
			}
		}

		
		//只撈一層
		if($catalogArray!=null){
			foreach( $catalogArray as $catalogRow ){
				if($catalogRow['parent_id']==0){
					$catalogMenu[$catalogRow['id']]=$catalogRow;
				}
				if($catalogRow['lv']==1){
					$pid = $catalogRow['parent_id'];
					if($catalogMenu[$pid]['child']==null)
						$catalogMenu[$pid]['child']=array();
					array_push($catalogMenu[$pid]['child'],$catalogRow);
				}
			}
			//要套鬍子的關係讓它從0開始
			$catalogMenuArray=array();
			foreach( $catalogMenu as $catMenuRow ){
				$catalogMenuaArray[]=$catMenuRow;
			}
			//print_r($catalogMenu);
		}
		//print_r($catalogMenuaArray);
		$tree_menu = CM_Template::get_tpl_html('treeMenu',array(
			'catalogMenu' => $catalogMenuaArray,
			'type' => $type
		));
		//$this->newsClass($catalogMenuaArray);
		//echo $tree_menu;
		
		$data['newsClass'] = $this->photoClass($catalogMenuaArray);
		if(!empty($data['newsClass'])){
			$data['hasClass']=true;
		}
	
	
		// 取得分類下資料夾(去掉根目錄)
		if($catalog!=null && $catalog!=0){
			$this->get_dao($_catalog_type[$type])->get_array($dir,Site_DAO_Catalog_Video::GROUP_DIR,array('parent_id' => $catalog));
		}
		//資料夾套版用
		$tempRows=0;
		while($tempRows<count($dir)){
			if($tempRows%4==0){
				//開始
				$dir[$tempRows]['startRows']=1;
			}else if(($tempRows+1)%4==0){
				//換行
				$dir[$tempRows]['endRows']=1;
			}else if($tempRows+1==count($dir)){
				//結束
				if(($tempRows+1)%4!=0){
					$dir[$tempRows]['endRows']=1;
				}
			}
			$tempRows++;
		}
		
		
		//取得列表
		//$this->get_dao('video')->get_rows($recordRows,Site_DAO_Video::GROUP_FRONT_LIST,array_merge($params['dao_params'],$params['get_params'],array("isCount"=>true)));
		$this->get_dao('video')->get_rows($recordRows,Site_DAO_Video::GROUP_FRONT_LIST,array_merge($params['get_params'],$params['dao_params'],array("isCount"=>true)));
		$total = $recordRows['total'];
		//$total = 100;
		//$this->get_dao('video')->get_array($recordRows,Site_DAO_Video::GROUP_FRONT_LIST,array_merge($params['dao_params'],$params['get_params']));
		$this->get_dao('video')->get_array($recordRows,Site_DAO_Video::GROUP_FRONT_LIST,array_merge($params['get_params'],$params['dao_params']));
		
		//日期換算
		$tempRows=0;
		while($tempRows<count($recordRows)){
			$recordRows[$tempRows]['id']=htmlentities($recordRows[$tempRows]['id']);
			$recordRows[$tempRows]['start_date']= date('Y-m-d', strtotime($recordRows[$tempRows]['start_date']) );
			$recordRows[$tempRows]['clickcount']=$recordRows[$tempRows]['clickcount']==null?0:$recordRows[$tempRows]['clickcount'];
			$recordRows[$tempRows]['isVideo']=$recordRows[$tempRows]['type']==1?1:0;
			if($tempRows%4==0){
				//開始
				$recordRows[$tempRows]['startRows']=1;
			}else if(($tempRows+1)%4==0){
				//換行
				$recordRows[$tempRows]['endRows']=1;
			}else if($tempRows==count($recordRows)){
				//結束
				if(($tempRows+1)%4!=0){
					$recordRows[$tempRows]['endRows']=1;
				}
			}
			$tempRows++;
		}
		//print_r($recordRows);

		//頁數判斷
		$prePage=array(12,24,36,72);
		$page_options = $this->setOptions($prePage,$this->_get['rowsPerPage']);		
		
		//下方頁碼
		//$ntl->assign('pagination', ($recordCount)?pagination($_SERVER['REQUEST_URI'], $recordCount, $rowsPerPage, $offset):'');
		$pagination=pagination($_SERVER['REQUEST_URI'], $total, $rowsPerPage, $offset);//總,每頁,第幾開始
		
		$file_list = CM_Template::get_tpl_html('layout'.$layoutMode,array('data' => $recordRows,'catalog'=>$catalog));
		//print_r($recordRows);		
		//print_r($params['get_params']);
		
		//套版
		echo $this->main_page(array(
			'm_header' => $this->main_header(array('hasClass' => $data['hasClass'],'dataClass' => $data['newsClass'])),
			'm_main' => CM_Template::get_tpl_html('photoDefault', array(
			  'get'=>$params['get_params'],
			  //'xajax_javascript'=>$xajax_javascript,
			  'hasClass' => $data['hasClass'],
			  'dataClass' => $data['newsClass'],
			  //'dataClass'=> $this->newsClass($catalogMenuaArray),
              'data' => $recordRows,
              'dataList' => $listJSON->data,
			  'order_list'=>$order_list,
			  'layout'=>array('isLayoutMode1'=>$isLayoutMode1,'isLayoutMode2'=>$isLayoutMode2),
			  'hasDir'=>count($dir)>0?array('dir'=>$dir):0,
			  'file_list'=>$file_list,
			))
		));

	}
	
	// 詳細資訊頁面
    public function page_photo_detail() {	
	
		$isPc = $this->isPc();
		
		//行動裝置不給開
		if($isPc == false){
			$this->page_notfound();
			die();
		}
	
		//ajax
		$ntl = $this->_ntl;
		$this->_xajax_javascript = $ntl->render_xajax_javascript(array(array($this,'front_login'),array($this,'front_logout')), false);		
	  
		//變數集
		$params = array(
		  'dao_params' => array(
			'catalog' => $this->_get['catalog'],
			//'keyword' => $keyword,
			'type' => 3,
			'stauts'=> 1,
		  ),
		  'get_params' => $this->_get,
		);
		
		//麵包屑
		/*$showCatalogAll = true;
		$rootPath = '0';
		$catalogDAO = $this->get_dao('catalog_Picture');
		$isDetail = isset($this->_get[Site::VAR_RECORD_ID]) && $this->get_dao('video')->get_array($recordRows,Site_DAO_Video::GROUP_FRONT_LIST,array_merge($params['dao_params'],$params['get_params']));
		if($isDetail == true) {
			$fPath = $recordRows[0][Site_DAO_Video::COLUMN_CATALOG_ID];
		} else {
			//列表
			$fPath = isset($this->_get[Site::VAR_FPATH]) ? $this->_get[Site::VAR_FPATH] : $rootPath;
		}
		//echo('fpath='.$fPath);
		//echo self::PAGE_NAME;
		$catalogParentArray = self::get_catalog_parent_rows($catalogDAO, $fPath, self::PAGE_PHOTO, $rootPath);
		//print_r($catalogParentArray);
		foreach($catalogParentArray as $catalogParentRows) $this->_breadcrumb->add($catalogParentRows[Site_DAO_Video::COLUMN_HEADLINE], Site::href_link($this->_path_rows['path'], array(Site::VAR_FPATH => $catalogParentRows[Site_DAO_Video::COLUMN_RECORD_ID])));
		*/
		//取得列表
		$this->get_dao('video')->get_rows($recordRows,Site_DAO_Video::GROUP_FRONT_LIST,array_merge($params['dao_params'],$params['get_params'],array("isCount"=>true)));
		$total = $recordRows['total'];
		//$total = 100;
		$this->get_dao('video')->get_array($recordRows,Site_DAO_Video::GROUP_FRONT_LIST,array_merge($params['dao_params'],$params['get_params']));
	  
		//特殊處理
		$_exceptFile=array('cdr','psd','ai');
		$tempRows=120;
		while($tempRows<count($recordRows)){
			$recordRows[$tempRows]['start_date']= date('Y-m-d', strtotime($recordRows[$tempRows]['start_date']) );
			// 圖片判斷psd,cdr,ai
			if($recordRows[$tempRows]['type']==3){
				$extFile = substr(strrchr($recordRows[$tempRows]['file'],'.'),1);
				//echo array_search($extFile,$_exceptFile)."<br>";
				if(false!==array_search($extFile,$_exceptFile)){
					$recordRows[$tempRows]['hasExt']=$extFile;
					//這邊被我改掉了，原本想在套版處理，要改到吐json檔那邊處理
					//echo "<br>$extFile<br>";
				}
				//array_search($tempUri,$exceptFile);
				//echo "<br>".$tempUri.":".array_search($tempUri,$exceptFile)."<br>";
			}
			$tempRows++;
		}
	  
	  
		//擋掉奇怪的操作
		if($this->_get['recordId']==null){
			echo "操作錯誤";
			die();
		}
	  
	  
		$isPc = $GLOBALS['isPc'];
	  
		 //套版
		echo $this->main_page(array(
				//'m_header' => $this->main_header(array('dataClass' => $data['newsClass'])),
				'm_main' => CM_Template::get_tpl_html('photoDetail', array(
				//'dataClass' => $data['newsClass'],
				'data' => $recordRows,
				//'dataList' => $listJSON->data,
				'get_params' => $this->_get,
				'isPc'=> $isPc,
				'total'=>$total
			))
		));

	  
    }
	
  }
?>