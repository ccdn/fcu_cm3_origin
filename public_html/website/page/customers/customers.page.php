<?php
  class Site_Page_Customers extends Site_Page {
    const PAGE_NAME = 'customers';
    
    protected function get_payment_options() {
      $lang = CM_Lang::get_current_id(true);
      $return = array();

      foreach(CM_ShoppingCart_Payment::get_enable_payment() as $payment) {
        $params = $payment->get_params();
        $return[] = array(
          'id' => $payment->_code,
          'text' => $params["MOD_NAME_{$lang}"],
          'intro' => htmlspecialchars_decode($params["MOD_INTRO_{$lang}"])
        );
      }

      return $return;
    }

    protected function main_page_customers($data) {
      $data['headline'] = $this->_path->get_node_item('/customers/')->get_text();
      $data['customersMenu'] = array('items' => array_map(function($element) {
        return array(
          'current' => $element->get_params('active'),
          'link' => $element->get_link_html()
        );
      }, array(
        $this->_path->get_node_item('/customers/'),
        $this->_path->get_node_item('/customers/edit/'),
        $this->_path->get_node_item('/customers/password_change/')
        // $this->_path->get_node_item('/customers/orders/'),
        // $this->_path->get_node_item('/customers/track/')
      )));
      echo $this->main_page(array(
        'm_main' => CM_Template::get_tpl_html('customersDefault', $data)
      ));
    }

    protected function to_shopping_cart_tmpl($itemArray, $totalRows, $params = array()) {
      $params = array_merge(array(
        'linkPath' => '',
        'linkParams' => array()
      ), $params);
      $cbThat = $this;
      $listColumn = array(
        'headline' => CM_Lang::line('DAO_Product.COLUMN_HEADLINE'),
        'qty' => $this->lang('SHOPPING_CART_COLUMN_QTY'),
        'price' => CM_Lang::line('DAO_Product.COLUMN_PRICE'),
        'sub_total' => $this->lang('SHOPPING_CART_COLUMN_SUBTOTAL')
      );
      return array(
        'title' => $this->lang('SHOPPING_CART_TITLE'),
        'product' => Site_Format::to_tmpl_table(
          $itemArray,
          $listColumn,
          array(
            'headline' => function($record, $recordRows = array()) {
              return array(
                'attr' => 'class="col-sm-8 col-md-6"',
                'text' => CM_Template::get_tpl_html('components_mediaObject', array(
                  'img' => isset($recordRows['_file']) ? $recordRows['_file'] : false,
                  'headline' => $record,
                  'desc' => CM_Template::get_tpl_html('box_price', $recordRows['_price'])
                ))
              );
            },
            'qty' => function($record, $recordRows = array()) use($params) {
              return array(
                'attr' => 'class="col-sm-1 col-md-1 text-center"',
                'text' => $record
              );
            },
            'price' => function($record, $recordRows = array()) {
              return array(
                'attr' => 'class="col-sm-1 col-md-1 text-center"',
                'text' => $record
              );
            },
            'sub_total' => function($record, $recordRows = array()) use($params) {
              return array(
                'attr' => "class=\"col-sm-2 col-md-2 text-center\"",
                'text' => $record
              );
            }
          ),
          array(
            'attr' => 'class="table table-hover"',
            'cols_header_callback' => function($columnName, $label) {
              $attr = array(
                'price' => 'class="text-center"',
                'sub_total' => 'class="text-center"'
              );

              if(isset($attr[$columnName])) {
                return array('text' => $label, 'attr' => $attr[$columnName]);
              } else {
                return array('text' => $label);
              }
            },
            'cols_default_callback' => function($record, $recordRows = array()) {
              return array('attr' => 'align="center"', 'text' => $record);
            }
          )
        ),
        'total' => $totalRows
      );
    }

    public function __construct(Site_Path $path) {
      parent::__construct($path, self::PAGE_NAME);
      //載入CSS
      $this->_css[] = self::PAGE_NAME . '.css';
    }
    
    public function page_login() {
      $form = Site_Factory::createCMForm();
      $form->set_form_dao($this->get_dao(self::PAGE_NAME), Site_DAO_Customers::GROUP_LOGIN);
      $form->set_form_default($this->_post);
      $data = array(
        'headline' => $this->_path_rows[Site_DAO_Node::COLUMN_HEADLINE],
        'regUrl' => sprintf('<a class="center" href="%s">%s</a>', Site::href_link('/customers/register/'), $this->lang('LOGIN_REGISTER')),
        'formAlert' => $this->_alert->output('login'),
        'formTop' => Site_Html::form('loginForm', Site::href_link('/customers/login/', array_merge(Site::get_all_get_params(array(Site::VAR_PROCESS), true), array(Site::VAR_PROCESS => 'authorized'))), 'post', 'class="cm-form form-horizontal" role="form"'),
        'formFields' => $form->get_form(),
        'regField' => Site::get_recaptcha_fields(),
        'formButton' => implode('&nbsp;', array(
          Site_Html::icon_submit_btn_primary($this->lang('LOGIN_BTN'), 'key', 'fa'),
          Site_Html::href_btn($this->lang('LOGIN_FORGET_PASSWORD'), Site::href_link('/customers/forget/'))
        )),
        'formBottom' => '</form>'
      );

      echo $this->main_page(array(
        'm_main' => CM_Template::get_tpl_html('customersLogin', $data)
      ));
    }
    
    public function process_login_authorized() {
      $success = false;
      $params = array(
        'email' => $this->_post['email'],
        'password' => $this->_post['password']
      );
      
      if(!Site::validate_recaptcha($returnMessage, $this->_post)) {
        $this->_alert->add_error('login', $returnMessage);
      } elseif(!$this->get_dao(self::PAGE_NAME)->validate_column($returnMessage, $this->_post, Site_DAO_Customers::GROUP_LOGIN)) {
        $this->_alert->add_error('login', $returnMessage);
      } elseif(!$this->get_dao(self::PAGE_NAME)->get_rows($recordRows, '', $params)) {
        $this->_alert->add_error('login', $this->lang('LOGIN_NOT_FOUND'));
      } else {
        $success = true;
        if(!isset($this->_session[Site::VAR_CUSTOMERS])) $this->_session[Site::VAR_CUSTOMERS] = array();

        $this->_session[Site::VAR_CUSTOMERS_ID] = $recordRows[Site_DAO_Customers::COLUMN_RECORD_ID];
        $this->_session[Site::VAR_CUSTOMERS][$recordRows[Site_DAO_Customers::COLUMN_RECORD_ID]] = $recordRows;

        if($this->get_dao(self::PAGE_NAME)->update_rows($rMessage, $rReturn, array(
          'login_count' => $recordRows['login_count'] + 1,
          'login_last_date' => date('Y-m-d H:i:s')
        ), $recordRows[Site_DAO_Customers::COLUMN_RECORD_ID], 'last_login')) {
          $this->_alert->add_success('global_alert', sprintf($this->lang('LOGIN_SUCCESS'), $recordRows[Site_DAO_Customers::COLUMN_HEADLINE]), true);
          Site::location($this->get_back_url(Site::href_link('/customers/')));
        } else {
          $this->_alert->add_error('login', $this->lang('LOGIN_NOT_FOUND'));
        }
      }
      
      if($success == false) $this->page_login();
    }
    
    public function process_logoff() {
      unset($this->_session[Site::VAR_CUSTOMERS_ID]);
      unset($this->_session[Site::VAR_CUSTOMERS][$this->_session[Site::VAR_CUSTOMERS_ID]]);
      $this->_alert->add_success('global_alert', $this->lang('LOGOFF_SUCCESS'), true);
      Site::location($this->get_back_url(Site::href_link('/customers/login/')));
    }
    
    public function page_register() {
      $this->get_dao(self::PAGE_NAME)->set_column('password', array('f_confirm' => true));
      $form = Site_Factory::createCMForm();
      $form->set_form_dao($this->get_dao(self::PAGE_NAME), Site_DAO_Customers::GROUP_ADD);
      $form->set_form_default($this->_post);
      $data = array();
      $data['formAlert'] = $this->_alert->output('register');
      $data['formTop'] = Site_Html::form('loginForm', Site::href_link($this->_path_rows['path'], array(Site::VAR_PROCESS => 'save')), 'post', 'class="cm-form form-horizontal"');
      $data['formBottom'] = '</form>';
      $data['form'] = $form->get_groups_form(array(
        'site_info' => array('intro' => $this->lang('CUSTOMERS_FORM_GROUP_INFO')),
        'site_setting' => array('intro' => $this->lang('CUSTOMERS_FORM_GROUP_SETTING'))
      ));
      $data['formButton'] = Site_Html::icon_submit_btn_primary($this->lang('REGISTER_BTN'), 'ok', 'glyphicon');
      $data['regField'] = Site::get_recaptcha_fields();
      $data['lang'] = array(
        'header' => $this->_path_rows[Site_DAO_Node::COLUMN_HEADLINE]
      );

      echo $this->main_page(array(
        'm_main' => CM_Template::get_tpl_html('customersForm', $data)
      ));
    }
    
    public function process_register_save() {
      $success = false;
      $this->_post[Site_DAO_Customers::COLUMN_STATUS] = '1';
      
      if(!Site::validate_recaptcha($returnMessage, $this->_post)) {
        $this->_alert->add_error('register', $returnMessage);
      } elseif($this->_post['password'] != $this->_post['password_confirm']) {
        $this->_alert->add_error('register', $this->lang('REGISTER_PASSWORD_ERROR'));
      } elseif(!$this->get_dao(self::PAGE_NAME)->insert_rows($returnMessage, $rReturn, $this->_post, Site_DAO_Customers::GROUP_ADD)) {
        $this->_alert->add_error('register', $returnMessage);
      } else {
        //註冊完後自動登入
        $this->_session[Site::VAR_CUSTOMERS_ID] = $rReturn['currentRows'][Site_DAO_Customers::COLUMN_RECORD_ID];
        $this->_session[Site::VAR_CUSTOMERS][$rReturn['currentRows'][Site_DAO_Customers::COLUMN_RECORD_ID]] = $rReturn['currentRows'];
        $this->_alert->add_success('global_alert', sprintf($this->lang('REGISTER_SUCCESS'), $this->_post[Site_DAO_Customers::COLUMN_HEADLINE]), true);
        Site::location($this->get_back_url(Site::href_link('/customers/')));
      }
      
      if($success == false) $this->page_register();
    }
    
    public function page_forget() {
      $data = array();
      $data['formAlert'] = $this->_alert->output('forget');
      $data['formTop'] = Site_Html::form('loginForm', Site::href_link($this->_path_rows['path'], array(Site::VAR_PROCESS => 'send')), 'post', 'class="cm-form form-horizontal"');
      $data['formBottom'] = '</form>';
      $data['formFields'] = Site_Factory::createCMForm()->get_field($this->get_dao(self::PAGE_NAME)->get_column('email'));
      $data['formButton'] = Site_Html::icon_submit_btn_primary($this->lang('PASSWORD_FORGET_BTN'), 'envelope', 'glyphicon');
      $data['regField'] = Site::get_recaptcha_fields();
      $data['lang'] = array(
        'header' => $this->_path_rows[Site_DAO_Node::COLUMN_HEADLINE],
        'pw_label' => $this->lang('PASSWORD_FORGET_LABEL'),
        'pw_tip' => $this->lang('PASSWORD_FORGET_TIP'),
      );
      echo $this->main_page_customers(array(
        'm_main' => CM_Template::get_tpl_html('customersForget', $data)
      ));
    }
    
    public function process_forget_send() {
      $success = false;
      $params = array('email' => $this->_post['email']);
      
      if(!Site::validate_recaptcha($returnMessage, $this->_post)) {
        $this->_alert->add_error('forget', $returnMessage);
      } elseif(!$this->get_dao(self::PAGE_NAME)->get_column('email')->validate($returnMessage, $this->_post['email'])) {
        $this->_alert->add_error('forget', $returnMessage);
      } elseif(!$this->get_dao(self::PAGE_NAME)->get_rows($recordRows, Site_DAO_Customers::GROUP_LIST, $params)) {
        $this->_alert->add_error('forget', sprintf($this->lang('PASSWORD_FORGET_NOT_FOUND'), $this->_post['email']));
      } else {
        $code = Site::get_string_encrypt(implode('|', array(
          md5(microtime()),
          time(),
          $recordRows[Site_DAO_Customers::COLUMN_RECORD_ID],
          md5(microtime())
        )));
        $data['ip'] = Site::get_ip_address();
        $data['datetime'] = date('Y-m-d H:i:s');
        $data['name'] = $recordRows[Site_DAO_Customers::COLUMN_HEADLINE];
        $data['url'] = Site::href_link('/customers/forget/', array(Site::VAR_PROCESS => 'change_password', 'k' => $code));
        Site::mail($recordRows[Site_DAO_Customers::COLUMN_HEADLINE], $recordRows['email'], sprintf($this->lang('PASSWORD_FORGET_MAIL_SUBJECT'), CM_Lang::line('CM.SITE_NAME')), Site::get_mail_body('customersForgetMail', $data));
        $this->_alert->add_success('global_alert', $this->lang('PASSWORD_FORGET_SEND'), true);
        Site::location(Site::href_link('/customers/forget/'));
      }
      
      if($success == false) $this->page_forget();
    }
    
    public function process_forget_change_password() {
      $success = true;
      
      if(!isset($this->_get['k'])) {
        $success = false;
        $this->_alert->add_error('global_alert', $returnMessage, true);
      }
      
      if($success == true) {
        list($time1, $time, $id, $time2) = explode('|', Site::get_string_encrypt($this->_get['k'], 'DECODE'));
        //時間限制
        if((time() - $time) > (60 * 30)) {
          $success = false;
          $this->_alert->add_error('global_alert', $this->lang('PASSWORD_FORGET_TIME_OUT'), true);
        }
      }
      
      if($success == true) {
        if(!$this->get_dao(self::PAGE_NAME)->get_rows($recordRows, Site_DAO_Customers::GROUP_LIST, array('recordId' => $id))) {
          $success = false;
          $this->_alert->add_error('global_alert', $this->lang('PASSWORD_FORGET_ACCOUNT_NOT_FOUND'), true);
        }
      }
      
      if($success == true) {
        $newPassword = substr(md5(time()), 0, 8);
      
        if(!$this->get_dao(self::PAGE_NAME)->update_rows($returnMessage, $rReturn, array('password' => $newPassword), $id, Site_DAO_Customers::GROUP_PASSWORD)) {
          $success = false;
          $this->_alert->add_error('global_alert', $returnMessage, true);
        }
      }
      
      if($success == true) {
        $data = array();
        $data['name'] = $recordRows[Site_DAO_Customers::COLUMN_HEADLINE];
        $data['password'] = $newPassword;
        Site::mail($recordRows[Site_DAO_Customers::COLUMN_HEADLINE], $recordRows['email'], sprintf($this->lang('PASSWORD_FORGET_MAIL_CHANGE_SUBJECT'), CM_Lang::line('CM.SITE_NAME')), Site::get_mail_body('customersPasswordChangeMail', $data));
      }
      
      Site::location(Site::href_link('/customers/change_success/'));
    }

    public function page_edit() {
      $this->login_authorized();
      
      if(!$this->get_dao(self::PAGE_NAME)->get_rows_byId($recordRows, Site_DAO_Customers::GROUP_EDIT, $this->_session[Site::VAR_CUSTOMERS_ID])) {
        $this->_alert->add_error('global_alert', $this->lang('EDIT_ACCOUNT_NOT_FOUND'), true);
        Site::location(Site::href_link('/'));
      }
      
      $form = Site_Factory::createCMForm();
      $form->set_form_dao($this->get_dao(self::PAGE_NAME), Site_DAO_Customers::GROUP_EDIT);
      $form->set_form_default($recordRows);
      $data = array();
      $data['formAlert'] = $this->_alert->output('edit');
      $data['formTop'] = Site_Html::form('edit', Site::href_link($this->_path_rows['path'], array(Site::VAR_PROCESS => 'save')), 'post', 'class="cm-form form-horizontal"');
      $data['formBottom'] = '</form>';
      $data['form'] = $form->get_groups_form(array(
        'site_info' => array('intro' => $this->lang('CUSTOMERS_FORM_GROUP_INFO')),
        'site_setting' => array('intro' => $this->lang('CUSTOMERS_FORM_GROUP_SETTING'))
      ));
      $data['formButton'] = Site_Html::icon_submit_btn_primary($this->lang('EDIT_BTN'), 'ok', 'glyphicon');
      $data['lang'] = array(
        'header' => $this->_path_rows[Site_DAO_Node::COLUMN_HEADLINE]
      );
      echo $this->main_page_customers(array(
        'm_main' => CM_Template::get_tpl_html('customersForm', $data)
      ));
    }
    
    public function process_edit_save() {
      $this->login_authorized();
      $success = false;
        
      if(!$this->get_dao(self::PAGE_NAME)->update_rows($returnMessage, $rReturn, $this->_post, $this->_session[Site::VAR_CUSTOMERS_ID], Site_DAO_Customers::GROUP_EDIT)) {
        $this->_alert->add_error('edit', $returnMessage);
      } else {
        $this->_alert->add_success('global_alert', $this->lang('EDIT_SUCCESS'), true);
        Site::location(Site::href_link('/customers/'));
      }
      
      if($success == false) $this->page_edit();
    }
    
    public function page_password_change() {
      $this->login_authorized();
      $this->get_dao(self::PAGE_NAME)->set_column('password_new', array(
        'disable' => false,
        'f_confirm' => true
      ));
      $form = Site_Factory::createCMForm();
      $form->set_form_dao($this->get_dao(self::PAGE_NAME), Site_DAO_Customers::GROUP_PASSWORD);
      $data = array();
      $data['formAlert'] = $this->_alert->output('password_change');
      $data['formTop'] = Site_Html::form('passwordChangeForm', Site::href_link($this->_path_rows['path'], array(Site::VAR_PROCESS => 'save')), 'post', 'class="cm-form form-horizontal"');
      $data['formBottom'] = '</form>';
      $data['formButton'] = Site_Html::icon_submit_btn_primary($this->lang('PASSWORD_CHANGE_BTN'), 'key', 'fa');
      $data['form'] = array(
        'id' => 'changePasswordForm',
        'fields' => $form->get_form(),
      );
      $data['formIntro'] = $this->lang('PASSWORD_CHANGE_FORM_INTRO');
      $data['lang'] = array(
        'header' => $this->_path_rows[Site_DAO_Node::COLUMN_HEADLINE]
      );
      echo $this->main_page_customers(array(
        'm_main' => CM_Template::get_tpl_html('customersForm', $data)
      ));
    }
    
    public function process_password_change_save() {
      $this->login_authorized();
      $params = array(
        'email' => $this->_session[Site::VAR_CUSTOMERS][$this->_session[Site::VAR_CUSTOMERS_ID]]['email'],
        'password' => $this->_post['password']
      );
      
      if(!$this->get_dao(self::PAGE_NAME)->validate_column($returnMessage, $this->_post, Site_DAO_Customers::GROUP_PASSWORD)) {
        $this->_alert->add_error('password_change', $returnMessage);
      } elseif($this->_post['password_new'] != $this->_post['password_new_confirm']) {
        $this->_alert->add_error('password_change', $this->lang('REGISTER_PASSWORD_ERROR'));
      } elseif(!$this->get_dao(self::PAGE_NAME)->get_rows($recordRows, Site_DAO_Customers::GROUP_LIST, $params)) {
        $this->_alert->add_error('password_change', $this->lang('PASSWORD_CHANGE_ERROR'));
      } elseif(!$this->get_dao(self::PAGE_NAME)->update_rows($returnMessage, $rReturn, array('password' => $this->_post['password_new']), $recordRows[Site_DAO_Customers::COLUMN_RECORD_ID], Site_DAO_Customers::GROUP_PASSWORD)) {
        $this->_alert->add_error('password_change', $returnMessage);
      } else {
        $data = array();
        $data['name'] = $recordRows[Site_DAO_Customers::COLUMN_HEADLINE];
        $data['password'] = $this->_post['password_new'];
        Site::mail($recordRows[Site_DAO_Customers::COLUMN_HEADLINE], $recordRows['email'], sprintf($this->lang('PASSWORD_CHANGE_MAIL_SUBJECT'), CM_Lang::line('CM.SITE_NAME')), Site::get_mail_body('customersPasswordChangeMail', $data));
        $this->_alert->add_success('global_alert', $this->lang('PASSWORD_CHANGE_SUCCESS'), true);
        Site::location(Site::href_link('/customers/'));
      }
      
      $this->page_password_change();
    }
    
    public function page_orders() {
      $this->login_authorized();
      $cbThat = $this;
      $dao = $this->get_dao('product_Orders');
      $daoParams = array(
        'customersId' => $this->_session[Site::VAR_CUSTOMERS_ID]
      );

      if(isset($this->_get[Site::VAR_RECORD_ID]) && $dao->get_orders_detail($rMessage, $rReturn, $this->_get[Site::VAR_RECORD_ID], array('ordersDaoParams' => $daoParams))) {
        $this->_breadcrumb->add($this->lang('ORDERS_DETAIL'), Site::href_link($this->_path_rows['path'], array(Site::VAR_RECORD_ID => $rReturn['orders']['id'])));
        //訂單詳細資料
        $rReturn['product'] = Site_Format::to_product_tmpl($rReturn['product'], array('imgSize' => '160x90'));
        $data = array();
        $data['ordersInfo'] = array(
          'title' => $this->lang('ORDERS_DETAIL'),
          'id' => array(
            'label' => CM_Lang::line('Site.ORDERS_ID'),
            'value' => CM_Format::to_orders_id($rReturn['orders'][Site_DAO_Product_Orders::COLUMN_RECORD_ID])
          ),
          'status' => array(
            'label' => CM_Lang::line('Site.ORDERS_STATUS'),
            'value' => Site::get_options_name($rReturn['orders'][Site_DAO_Product_Orders::COLUMN_STATUS], $this->get_dao('product_Orders')->get_options('status'))
          )
        );
        $data['cart'] = $this->to_shopping_cart_tmpl($rReturn['product'], $rReturn['total']);
        //付款方式
        $payment = Site_Factory::createCMSCartPayment($rReturn['orders']['payment']);
        $data['payment_method'] = array(
          'title' => $this->lang('SHOPPING_CART_PAYMENT_METHOD_TITLE'),
          'method' => array(
            'id' => $payment->_code,
            'text' => $payment->get_param('NAME'),
            'intro' => ''
          )
        );
        //付款人資料
        $ordersform = Site_Factory::createCMForm();
        $ordersform->set_form_dao($this->get_dao('product_Orders'), Site_DAO_Product_Orders::GROUP_ADD);
        $ordersform->set_form_default($rReturn['orders']);
        $data['payment'] = $ordersform->get_group_form('info', array(
          'title' => $this->lang('SHOPPING_CART_PAYMENT_TITLE')
        ), Site_Form::ACTION_DISPLAY, array('cKey' => true));
        //發票資料
        $data['invoice'] = $ordersform->get_group_form('invoice', array(
          'title' => $this->lang('SHOPPING_CART_INVOICE_TITLE'),
          'show' => isset($rReturn['orders']['invoice']) && $rReturn['orders']['invoice'] == 1 ? true : false
        ), Site_Form::ACTION_DISPLAY, array('cKey' => true));
        //寄件人資料
        $data['addressee'] = $ordersform->get_group_form('addressee', array(
          'title' => $this->lang('SHOPPING_CART_ADDRESSEE_TITLE')
        ), Site_Form::ACTION_DISPLAY, array('cKey' => true));
        $data['button'] = implode('&nbsp;', array(
          Site_Html::icon_btn_primary($this->lang('ORDERS_BACK_BTN'), 'chevron-left', 'glyphicon', array('attr' => array(
            'data-cmfunc' => 'prev-page'
          ))),
          Site_Html::icon_btn_success($this->lang('ORDERS_PRINT_BTN'), 'print', 'glyphicon', array('attr' => array(
            'data-cmfunc' => 'print'
          )))
        ));
        echo $this->main_page_customers(array(
          'm_main' => CM_Template::get_tpl_html('shopping_success', $data)
        ));
      } else {
        //訂單列表
        $paymentOptions = $this->get_payment_options();
        $data = array();
        $listJSON = $dao->get_array_object(Site_DAO_Product_Orders::GROUP_SITE_LIST, array_merge(array(
          'keyColumn' => '',
          'oNowPage' => isset($this->_get['p']) ? (int)$this->_get['p'] : 1,
          'oPageCallback' => function($page, array $params, $key) use($cbThat) {
            return $cbThat->cb_default_list_pagesplit($page, $params, $key, '/customers/orders/');
          }
        ), $daoParams));
        $data['headline'] = $this->_path_rows[Site_DAO_Node::COLUMN_HEADLINE];
        $data['hasList'] = $listJSON->dataEmpty ? false : true;

        if($data['hasList'] == true) {
          $data['list'] = Site_Format::to_tmpl_table(
            $listJSON->data,
            array_merge($dao->get_column_field_label_array(Site_DAO_Product_Orders::GROUP_SITE_LIST), array('action' => '&nbsp;')),
            array(
              Site_DAO_Product_Orders::COLUMN_STATUS => function($record, $recordRows = array()) use($dao) {
                return array('text' => Site::get_options_name($record, $dao->get_options('status')));
              },
              'payment' => function($record, $recordRows = array()) use($paymentOptions) {
                return array('text' => Site::get_options_name($record, $paymentOptions));
              },
              'total' => function($record, $recordRows = array()) {
                return array('text' => Site_Format::to_price($record));
              },
              'action' => function($record, $recordRows = array()) use($cbThat) {
                $func = array();
                $func[] = sprintf('<a href="%s">%s</a>',
                  Site::href_link('/customers/orders/', array(Site::VAR_RECORD_ID => $recordRows[Site_DAO_Product_Orders::COLUMN_RECORD_ID])),
                  $cbThat->lang('ORDERS_DETAIL')
                );
                return array('attr' => 'class="text-center"', 'text' => implode('&nbsp;', $func));
              }
            ),
            array(
              'attr' => 'class="table table-bordered table-striped"',
              'cols_header_callback' => function($name, $label) {
                return array('text' => $label);
              },
              'cols_default_callback' => function($record, $recordRows = array()) {
                return array('text' => $record);
              }
            )
          );
          $data['listSplit'] = $listJSON->dataSplit;
        } else {
          $data['listEmpty'] = CM_Lang::line('Site.ERROR_DATA_EMPTY');
        }

        echo $this->main_page_customers(array(
          'm_main' => CM_Template::get_tpl_html('customersOrders', $data)
        ));
      }
    }

    public function page_track() {
      $this->login_authorized();
      $cbThat = $this;
      $daoParams = array(
        'fileCallback' => true,
        'recordIds' => array_map(function($element) {
          return $element['product_id'];
        }, $this->get_dao('product_Track')->get_array($recordArray, '', array('customersId' => $this->_session[Site::VAR_CUSTOMERS_ID])) ? $recordArray : array())
      );
      $listJSON = $this->get_dao('product')->get_array_object(Site_DAO_Product::GROUP_LIST, array_merge($daoParams, array(
        'oNowPage' => isset($this->_get['p']) ? (int)$this->_get['p'] : 1,
        'oPageCallback' => function($page, array $params, $key) use($cbThat) {
          return $cbThat->cb_default_list_pagesplit($page, $params, $key, $cbThat->_path_rows['path']);
        }  
      )));
      $listColumn = array(
        'headline' => CM_Lang::line('DAO_Product.COLUMN_HEADLINE')
      );
      $data = array();
      $data['headline'] = $this->_path_rows[Site_DAO_Node::COLUMN_HEADLINE];
      $data['hasList'] = $listJSON->dataEmpty ? false : true;

      if($data['hasList'] == true) {
        $data['list'] = array_map(function($element) use($cbThat) {
          return array(
            'img' => isset($element['_file']) ? $element['_file'] : false,
            'headline' => $element[Site_DAO_Product::COLUMN_HEADLINE],
            'htmlHeadline' => sprintf('<a href="%s" class="link">%s</a>', $element['_link'], $element[Site_DAO_Product::COLUMN_HEADLINE]),
            'desc' => CM_Template::get_tpl_html('box_trackBox', array(
              'price' => $element['_price'],
              'top' => Site_Html::form('cartForm', Site::href_link('/shopping/', array(Site::VAR_PROCESS => 'add_product')), 'post', 'class="form-inline cm-form"'),
              'hidden' => array(
                Site_Html::hidden_field('checkout', 'false'),
                Site_Html::hidden_field(Site::VAR_BACK_URL, urlencode($cbThat->get_current_url())),
                Site_Html::hidden_field(Site::VAR_RECORD_ID, $element[Site_DAO_Product::COLUMN_RECORD_ID])
              ),
              'button' => implode('&nbsp', array(
                Site_Html::icon_href_btn_danger($cbThat->lang('BTN_REMOVE'), Site::href_link($cbThat->_path_rows['path'], array(
                  Site::VAR_PROCESS => 'remove_track',
                  Site::VAR_RECORD_ID => $element[Site_DAO_Product::COLUMN_RECORD_ID],
                  Site::VAR_BACK_URL => urlencode($cbThat->get_current_url())
                )), 'remove', 'glyphicon', array(
                  'attr' => array('data-cmconfirm' => $cbThat->lang('BTN_REMOVE_CONFIRM'))
                )),
                Site_Html::icon_submit_btn_success($cbThat->lang('BTN_ADD_TO_CART'), 'shopping-cart', 'fa'),
                Site_Html::icon_submit_btn_primary($cbThat->lang('BTN_BUY_NOW'), 'chevron-right', 'glyphicon', array(
                  'attr' => array('data-cmfunc' => 'checkout'),
                  'iconLeft' => false
                ))
              )),
              'bottom' => '</form>'
            ))
          );
        }, Site_Format::to_product_tmpl($listJSON->data, array(
          'imgSize' => '160x90',
          'rowsProcessCallback' => function($rows) use($cbThat) {
            $rows['_itemSizeClass'] = 'col-sm-6 col-md-4';
            $rows['_link'] = Site::href_link('/product/', array(
              Site::VAR_SUB_PAGE => 'detail',
              Site::VAR_RECORD_ID => $rows[Site_DAO_Product::COLUMN_RECORD_ID]
            ));
            return $rows;
          }
        )));
        $data['listSplit'] = $listJSON->dataSplit;
      } else {
        $data['listEmpty'] = CM_Lang::line('Site.ERROR_DATA_EMPTY');
      }

      echo $this->main_page_customers(array(
        'm_main' => CM_Template::get_tpl_html('customersTrack', $data)
      ));
    }

    public function process_add_track() {
      $this->login_authorized();

      if(!isset($this->_get[Site::VAR_RECORD_ID]) || !$this->get_dao('product')->get_rows_byId($recordRows, '', $this->_get[Site::VAR_RECORD_ID])) {
        $this->_alert->add_error('global_alert', CM_Lang::line('CM.ERROR_DATA_NOT_FOUND'), true);
        Site::location(Site::href_link($this->_path_rows['path']));
      }

      if(!$this->get_dao('product_Track')->get_rows($checkRows, '', array(
        'customers_id' => $this->_session[Site::VAR_CUSTOMERS_ID],
        'product_id' => $recordRows[Site_DAO_Product::COLUMN_RECORD_ID]
      ))) {
        if(!$this->get_dao('product_Track')->insert_rows($rMessage, $rReturn, array(
          'customers_id' => $this->_session[Site::VAR_CUSTOMERS_ID],
          'product_id' => $recordRows[Site_DAO_Product::COLUMN_RECORD_ID],
          'note' => ''
        ), Site_DAO_Product_Track::GROUP_ADD) ) {
          $this->_alert->add_error('global_alert', $rMessage, true);
        } else {
          $this->_alert->add_success('global_alert', $rMessage, true);
        }
      } else {
        $this->_alert->add_success('global_alert', Site_DAO_Product_Track::lang('ADD_SUCCESS'), true);
      }

      Site::location(Site::href_link('/product/', array(
        Site::VAR_SUB_PAGE => 'detail',
        Site::VAR_RECORD_ID => $recordRows[Site_DAO_Product::COLUMN_RECORD_ID]
      )));
    }

    public function process_remove_track() {
      if(!isset($this->_get[Site::VAR_RECORD_ID]) || empty($this->_get[Site::VAR_RECORD_ID])) {
        $this->_alert->add_error('global_alert', CM_Lang::line('CM.ERROR_DATA_NOT_FOUND'), true);
      } elseif(!$this->get_dao('product_Track')->get_rows($trackRows, '', array(
        'productId' => (int)$this->_get[Site::VAR_RECORD_ID],
        'customersId' => $this->_session[Site::VAR_CUSTOMERS_ID]
      ))) {
        $this->_alert->add_error('global_alert', CM_Lang::line('CM.ERROR_DATA_NOT_FOUND'), true);
      } else {
        if($this->get_dao('product_Track')->delete_rows($rMessage, $rReturn, $trackRows[Site_DAO_Product_Track::COLUMN_RECORD_ID])) {
          $this->_alert->add_success('global_alert', $rMessage, true);
        } else {
          $this->_alert->add_error('global_alert', $rMessage, true);
        }
      }

      Site::location($this->get_back_url(Site::href_link($this->_path_rows['path'])));
    }

    public function page_default() {
      $this->login_authorized();
      $detailColumn = $this->get_dao(self::PAGE_NAME)->get_column_field_label_array(Site_DAO_Customers::GROUP_INFO);

      if(!$this->get_dao(self::PAGE_NAME)->get_rows_byId($customersRows, Site_DAO_Customers::GROUP_INFO, $this->_session[Site::VAR_CUSTOMERS])) {
        $this->_alert->add_error('global_alert', CM_Lang::line('CM.ERROR_DATA_NOT_FOUND'), true);
        Site::location(Site::href_link($this->_path_rows['path']));
      }

      $customersProfile = array();
      $customersProfileData = array(
        'id' => 'customers-profile',
        'headline' => $customersRows[Site_DAO_Customers::COLUMN_HEADLINE]
      );

      foreach($detailColumn as $columnName => $columnLabel) {
        $column = $this->get_dao(self::PAGE_NAME)->get_column($columnName);
        $columnValue = $customersRows[$columnName];

        if(in_array($column->f_type, array('radio', 'select'))) {
          $columnValue = Site::get_options_name($columnValue, $column->f_options); 
        } elseif($column->f_type == 'simple_file') {
          $customersProfileData['img'] = array(
            'path' => CM_Conf::get('PATH_W.UPLOAD') . $columnValue,
            'name' => $customersRows[Site_DAO_Customers::COLUMN_HEADLINE],
            'desc' => '',
            'thumbnail' => Site_Format::to_thumbnail_src($columnValue, '80x60'),
            'headline' => $customersRows[Site_DAO_Customers::COLUMN_HEADLINE]
          );
          continue;
        }

        $customersProfile[] = array(
          'label' => $columnLabel,
          'value' => $columnValue
        );
      }

      $customersProfileData['desc'] = '<br />' . implode('<br />', array_merge(
        array_map(function($element) {
          return "{$element['label']}：&nbsp;{$element['value']}";
        }, $customersProfile)
      ));
      $customersProfileData['desc'] .= '<br /><br />' . implode('&nbsp;', array_map(function($element) {
        return Site_Html::href_btn($element->get_text(), $element->get_link());
      }, array(
        $this->_path->get_node_item('/customers/edit/'),
        $this->_path->get_node_item('/customers/password_change/')
      )));

      echo $this->main_page_customers(array(
        'm_main' => CM_Template::get_tpl_html('customersInfo', array(
          'headline' => $this->_path_rows[Site_DAO_Node::COLUMN_HEADLINE],
          'detail' => $customersProfileData
        ))
      ));
    }
  }
?>