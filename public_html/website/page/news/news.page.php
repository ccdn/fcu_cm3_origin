<?php
  class Site_Page_News extends Site_Page {
    const PAGE_NAME = 'news';
	const PATH_MAIN = '/news/';
    
    public function __construct(Site_Path $path) {
      parent::__construct($path, self::PAGE_NAME);
    }
	
	public function newsClass() {
      //分類
      if($this->get_dao('option')->get_array($optionArray, '', array('catalog' => 3))) {
		
		foreach($optionArray AS $key => $optionRows) {
			$optionRows['link'] = Site::href_link(self::PATH_MAIN, array('catalog' => $optionRows['id']));
			if(!empty($this->_get['catalog'])&&is_numeric($this->_get['catalog'])){
				if($optionRows['id']==$this->_get['catalog']){
					$optionRows['selected']=true;
				}
			}
			$newsClassArray[] = $optionRows;
		}
		
        return $newsClassArray;
      }
    }
	
	public function page_default() {
	
		//列表
		$this->page_list();
		
		//套版測試
		//$this->page_test();

    }
    
    public function page_list() {
	
		//ajax
		$ntl = $this->_ntl;
		$this->_xajax_javascript = $ntl->render_xajax_javascript(array(array($this,'front_login'),array($this,'front_logout')), false);
		
		//判斷錯誤
		$catalog = !empty($this->_get['catalog'])&&is_numeric($this->_get['catalog'])?$this->_get['catalog']:0;
	
	  //麵包屑
	  if(isset($this->_get['catalog']) && $this->get_dao('option')->get_rows_byId($recordRows, '', $catalog)) {
        //加入麵包屑
        $this->_breadcrumb->add($recordRows[Site_DAO_Link::COLUMN_HEADLINE], $this->_path_rows['path'], array(Site::VAR_RECORD_ID => $recordRows[Site_DAO_Link::COLUMN_RECORD_ID]));
	  }
	  
	  //分類
      $data['newsClass'] = $this->newsClass();
	  if(!empty($data['newsClass'])){
		$data['hasClass']=true;
	  }
	  //print_r($data['newsClass']);
	  
	  //欄位名稱
	  $addColumn = array('start_date'=>'');
	  $listColumn = array_merge($this->get_dao(self::PAGE_NAME)->get_column_field_label_array(Site_DAO_NEWS::GROUP_LIST), $addColumn);//取欄位變數
	  
	  //變數集
		$params = array(
		  'dao_params' => array(
			'catalog' => $catalog,
			'status' =>1,
			//'catalog' => 2
		  ),
		  'get_params' => $this->_get,
		  'split_count' => 10
		);
		
		//取值&頁面相關參數
		$cbThat = $this;
		$pathRows = $this->get_path_rows();
		$listJSON = $this->get_dao(self::PAGE_NAME)->get_array_object(Site_DAO_NEWS::GROUP_LIST, array_merge($params['dao_params'], array(
			//'catalog' => 1,
			//'oNowPage' => isset($this->_get['p']) ? (int)$this->_get['p'] : 1,
			'oNowPage' => isset($params['get_params']['p']) ? (int)$params['get_params']['p'] : 1,
			'oPageSplit' => $params['split_count'],
			'oPageCallback' => function($page, array $other_news, $key) use($pathRows) {
			  return Site_Page::cb_default_list_pagesplit($page, $other_news, $key, $pathRows['path'], 'p');
			}
		)));
		//echo "agent=".$this->isPc();
		
		$listJSON->data = CM_Format::to_list_array_format(
          $listJSON->data,
          $listColumn,
          array(
			//日期換算
			'start_date' => function($record, $recordRows = array()) use($pathRows) {
                return date('Y-m-d', strtotime($recordRows[Site_DAO_NEWS::COLUMN_START_DATE]) );
            },
			
           
          )
        );
		//print_r($listJSON->data);
		//html(但是加這個原本是html的也會變html...)
		/*array_walk_recursive($listJSON->data, function (&$value) {
			$value = htmlentities($value,ENT_QUOTES,"UTF-8");
		});*/
		//連結要特別處理...
		array_walk_recursive($data['newsClass'], function (&$value) {
			$value = htmlentities($value,ENT_QUOTES,"UTF-8");
		});

	    //套版
		echo $this->main_page(array(
			'm_header' => $this->main_header(array('hasClass' => $data['hasClass'],'dataClass' => $data['newsClass'])),
			'm_main' => CM_Template::get_tpl_html('newsDefault', array(
			  'hasClass' => $data['hasClass'],
			  'dataClass' => $data['newsClass'],
              'data' => $recordRows,
              'dataList' => $listJSON->data,
			  'isPc'=>$this->isPc()
              //'dataSplit' => $listJSON->dataSplit,
			  //'searchForm' => $this->getSearchForm()
			))
		));
		
	  
      /*
	  if(isset($this->_get[Site::VAR_RECORD_ID]) && $this->get_dao(self::PAGE_NAME)->get_rows_byId($recordRows, '', $this->_get[Site::VAR_RECORD_ID])) {
        //加入麵包屑
        $this->_breadcrumb->add($recordRows[CM_DAO_News::COLUMN_HEADLINE], $this->_path_rows['path'], array(Site::VAR_RECORD_ID => $recordRows[CM_DAO_News::COLUMN_RECORD_ID]));
        echo $this->main_page(array(
          'm_left' => $this->main_column_left(),
          'm_main' =>
            $this->get_widget(self::PAGE_NAME, 'detail', array(
              'detail_rows' => $recordRows
            )) .
            $this->get_widget(self::PAGE_NAME, 'other_list', array(
              'lang_title' => $this->lang('OTHER_TITLE'),
              'excludeId' => $recordRows[CM_DAO_News::COLUMN_RECORD_ID],
              'limit' => 5
            ))
        ));
      } else {
        //列表頁面
        echo $this->main_page(array(
          'm_left' => $this->main_column_left(),
          'm_main' => $this->get_widget(self::PAGE_NAME, 'list_split', array('get_params' => $this->_get))
        ));
      }
	  */
    }
	
	public function page_Detail() {
	
		$pathRows = $this->get_path_rows();
		
		//判斷有資料會沒資料會
		if(isset($this->_get[Site::VAR_RECORD_ID]) && $this->get_dao(self::PAGE_NAME)->get_rows($recordRows,Site_DAO_News::GROUP_DETAIL, array('recordId' => $this->_get[Site::VAR_RECORD_ID], 'isCallback' => true))) {
			//ajax
			$ntl = $this->_ntl;
			$this->_xajax_javascript = $ntl->render_xajax_javascript(array(array($this,'front_login'),array($this,'front_logout')), false);
			
			
			//顯示內容
			
			 //取得分類
			$data['newsClass'] = $this->newsClass();
			
			//日期換算
			$recordRows[Site_DAO_Question::COLUMN_CREATE_DATE] = date('Y-m-d', strtotime($recordRows[Site_DAO_News::COLUMN_CREATE_DATE]) );
			
			//回上一頁
			//print_r( Site::get_all_get_params(array('recordId'), true));
			$recordRows['newsListLink'] = Site::href_link($pathRows['path_parent']);
			
			//html(但是加這個原本是html的也會變html...)
			/*foreach($recordRows as $key => $val)
			{
				$recordRows[$key] = htmlentities($val,ENT_QUOTES,"UTF-8");
			}*/
			
			//套版
			echo $this->main_page(array(
				'm_header' => $this->main_header(array('hasClass' => $data['hasClass'],'dataClass' => $data['newsClass'])),
				'm_main' => CM_Template::get_tpl_html('newsDetail', array(
				  'hasClass' => $data['hasClass'],
				  'dataClass' => $data['newsClass'],
				  'data' => $recordRows,
				  'dataList' => $listJSON->data,
				  'isPc'=>$this->isPc()
				  //'dataSplit' => $listJSON->dataSplit,
				  //'searchForm' => $this->getSearchForm()
				))
			));
			
		}else{
			//導回列表
			CM_Output::redirect(self::PATH_MAIN);
		}
	
    }
  }
?>