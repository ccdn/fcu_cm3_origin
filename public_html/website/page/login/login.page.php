<?php
  class Site_Page_Login extends Site_Page {
    const PAGE_NAME = 'login';
    
    public function __construct(Site_Path $path) {
      parent::__construct($path, self::PAGE_NAME);
    }
    
    public function page_default() {
		
		//echo "this is test page";
		//$this->_breadcrumb->add('test111');
		
		//如果已經登入變轉回首頁
		//echo $this->_session[Site::VAR_AUTH];
		if($this->isPc()){
			if($this->_session[Site::VAR_AUTH]!=false && !empty($this->_session[Site::VAR_AUTH])){
				Site::location(Site::href_link('/index/'));
				die;
			}
		}
	  
		//ajax
		$ntl = $this->_ntl;
		$this->_xajax_javascript = $ntl->render_xajax_javascript(array(array($this,'front_login'),array($this,'front_logout')), false);
	  
		//套版
		echo $this->main_page(array(
			//'m_header' => $this->main_header(array('dataClass' => $data['newsClass'])),
			'm_main' => CM_Template::get_tpl_html('loginDefault', array(
			))
		));
	  
    }
  }
?>