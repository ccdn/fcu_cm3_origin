<?php
  class Site_Page_Contact extends Site_Page {
    const PAGE_NAME = 'contact';
	const PATH_MAIN = '/contact/';
    
    public function __construct(Site_Path $path) {
      parent::__construct($path, self::PAGE_NAME);
    }
    
	
    public function page_default() {
	
		//列表
		$this->page_list();
		
		//套版測試
		//$this->page_test();

    }
	
	public function page_list() {	
	
		//ajax
		$ntl = $this->_ntl;
		$this->_xajax_javascript = $ntl->render_xajax_javascript(array(array($this,'front_login')), false);
	  
	  //$this->get_dao(self::PAGE_NAME)->set_column('password', array('f_confirm' => true));
      $form = Site_Factory::createCMForm();
      $form->set_form_dao($this->get_dao(self::PAGE_NAME), Site_DAO_Contact::GROUP_ADD);
      $form->set_form_default($this->_post);
	 
      $data = array();
      $data['formAlert'] = $this->_alert->output('contact');
      $data['formTop'] = CM_Html::form('contactForm', Site::href_link($this->_path_rows['path'], array(Site::VAR_PROCESS => 'save')), 'post', 'class="cm-form form-horizontal"');
      $data['formBottom'] = '</form>';
	  
      $data['form'] = $form->get_groups_form(array(
        'site_info' => array('intro' => ''),
       // 'site_setting' => array('intro' => $this->lang('CUSTOMERS_FORM_GROUP_SETTING'))
      ));
	  //print_r($data['form']);
      $data['formButton'] = CM_Html::submit_btn($this->lang('SUBMIT_BTN'), array('class' => 'btn btn-primary btn-large'));
      $data['regField'] = Site::get_recaptcha_fields();
	  //print_r($data);
	
      echo $this->main_page(array(
        'm_main' => CM_Template::get_tpl_html('contactForm', $data)
      ));
	  
	  
    }
	
	public function process_save() {
      $success = false;
      $this->_post[Site_DAO_Contact::COLUMN_STATUS] = '1';
      
      if(!Site::validate_recaptcha($returnMessage, $this->_post)) {
        $this->_alert->add_error('contact', $returnMessage);
      } elseif(!$this->get_dao(self::PAGE_NAME)->insert_rows($returnMessage, $rReturn, $this->_post, Site_DAO_Customers::GROUP_ADD)) {
        $this->_alert->add_error('contact', $returnMessage);
      } else {
        //填寫完導回首頁
        $this->_alert->add_success('global_alert', sprintf($this->lang('CONTACT_SUCCESS'), $this->_post[Site_DAO_Contact::COLUMN_HEADLINE]), true);
        Site::location(Site::href_link('/contact/'));
      }
      
      if($success == false) $this->page_default();
    }
	
  }
?>