<?php
  class Site_Page_Index extends Site_Page {
    const PAGE_NAME = 'index';
	const PRE_ROW =4;//一列幾個
    
    public function __construct(Site_Path $path) {
      parent::__construct($path, self::PAGE_NAME);
    }
    
    public function page_default() {
	
		//ajax
		$ntl = $this->_ntl;
		$this->_xajax_javascript = $ntl->render_xajax_javascript(array(array($this,'front_login'),array($this,'front_logout')), false);
	
		 //首頁輪播
		$this->get_dao('video_Carousel')->get_rows($videoIdRows,Site_DAO_VIDEO_CAROUSEL::GROUP_VIDEO);
		$videos = explode(",", $videoIdRows['video']);
		for($i=0;$i<count($videos);$i++){
			$this->get_dao('video')->get_rows_byId($tempVideoRows,Site_DAO_VIDEO::GROUP_LIST,$videos[$i]);
			$videoRows[]=$tempVideoRows;
		}
		//print_r($videoRows);
		//判斷播放器
		if(!empty($videoRows)){
			$player=$this->getIndexPlayer($videoRows);
		}
		//echo $player;
	  
		//關鍵字
		//取前10筆
		//$this->get_dao('keyword')->get_array($keyRows,Site_DAO_Keyword::GROUP_LIST,array('sort'=>'search_count DESC','offset'=>1,'startIndex'=>0,'endIndex'=>9));
		$this->get_dao('tagscloud')->get_array($keyRows,Site_DAO_Keyword::GROUP_LIST,array('sort'=>'count DESC','offset'=>1,'startIndex'=>0,'endIndex'=>15,'isLimit'=>true));
		//print_r($keyRows);
		$position = range(0,count($keyRows)-1);//亂數排
		shuffle($position);
	  
		$tempno=5;//套CSS用，5=多,1=少,5-1一循環
		for($i=0;$i<count($keyRows);$i++){
			$style[]=$tempno;
			$tempno--;
			if($tempno==0){
				$tempno=5;
			}
		}
		if(count($style)>0){
			arsort($style);//算好後從大到小依序排
		}
	  
		//判斷css怎麼套...(等版出來再說)
		for($i=0;$i<count($keyRows);$i++){
			//$keyword_area.= "<a href='../media/?type=1&keyword=".$keyRows[$i]['headline']." class='btn dmbutton btn-default tag tag-".$style[$i]." title=".$keyRows[$i]['headline'].">".$keyRows[$i]['headline']."</a>";
			$pi=$position[$i];
			//$keyword_area.= "<a href='../media/?type=1&keyword=".$keyRows[$pi]['headline']."' class='btn dmbutton btn-default tag tag-".$style[$pi]."' title=".$keyRows[$pi]['headline'].">".$keyRows[$pi]['headline']."</a>";
			$keyword_area.= "<a href='../media/?type=1&keyword=".$keyRows[$pi]['keyword']."' class='btn dmbutton btn-default tag tag-".$style[$pi]."' title=".$keyRows[$pi]['keyword'].">".$keyRows[$pi]['keyword']."</a>";
			$temp+=1;
		}
		//echo $keyword_area;
		$timeNow = date('Y-m-d');
	 
		//一列幾個
		$pre_row=self::PRE_ROW;
		//最新上傳
		//echo "==最新上傳==<br>";
		//不指定type便會撈全部，但問題是image和影片不太一樣，預設process_ok就不同了... 列表縮圖位置也不同...
		//$this->get_dao('video')->get_array($newRows,Site_DAO_Video::GROUP_FRONT_LIST,array('type'=>1,'sort'=>'create_date DESC','offset'=>1,'startIndex'=>0,'endIndex'=>7));
		$this->get_dao('video')->get_array($newRows,Site_DAO_Video::GROUP_FRONT_LIST,array('type'=>1,'date'=>$timeNow,'sort'=>'start_date DESC','offset'=>1,'startIndex'=>0,'endIndex'=>7));
		//print_r($newRows);
		//日期換算
		$tempRows=0;
		while($tempRows<count($newRows)){
			$newRows[$tempRows]['start_date']= date('Y-m-d', strtotime($newRows[$tempRows]['start_date']) );
			$newRows[$tempRows]['clickcount']=$newRows[$tempRows]['clickcount']==null?0:$newRows[$tempRows]['clickcount'];
			$newRows[$tempRows]['isVideo']=$newRows[$tempRows]['type']==1?1:0;
			$newRows[$tempRows]['video_length']=$this->getFormatVideoLength($newRows[$tempRows]['video_length']);
			//141218:ANN:音檔縮圖判斷(取副檔名)
			$extFile=strrchr($newRows[$tempRows]['file'],".");
			//echo $extFile."<br>";
			if($extFile=='.mp3' || $extFile=='.wma'){
				if($newRows[$tempRows]['file_img']==null){
					//$newRows[$tempRows]['file_img']='upload_thumbnail/music.jpg';
					$newRows[$tempRows]['file_img']='upload_thumbnail/picture.jpg';
				}
			}
			if($tempRows%$pre_row==0){
				//開始
				$newRows[$tempRows]['startRows']=1;
			}else if(($tempRows+1)%$pre_row==0){
				//換行
				$newRows[$tempRows]['endRows']=1;
			}
			//改到more的判斷
			/*}else if($tempRows==count($newRows)){
				//結束
				if(($tempRows+1)%$pre_row!=0){
					$newRows[$tempRows]['endRows']=1;
				}
			}*/
			$tempRows++;
		}
		$newData['data']=$newRows;
		$newRowsMore=$this->chkMoreBtn(count($newRows),'/media/?od=start_date');//連結位置
		$newData['more']=$newRowsMore;
		
		//print_r($newRows);
		//echo "<br><br>";
		
		//熱門
		//$this->get_dao('video')->get_array($hotRows,Site_DAO_Video::GROUP_FRONT_LIST,array('type'=>1,'od'=>'clickcount','sort'=>'clickcount DESC','offset'=>1,'startIndex'=>0,'endIndex'=>7));
		$this->get_dao('video')->get_array($hotRows,Site_DAO_Video::GROUP_FRONT_LIST,array('type'=>1,'od'=>'clickcount','date'=>$timeNow,'sort'=>'clickcount DESC','offset'=>1,'startIndex'=>0,'endIndex'=>7));
		//print_r($hotRows);
		//日期換算
		$tempRows=0;
		while($tempRows<count($hotRows)){
			$hotRows[$tempRows]['start_date']= date('Y-m-d', strtotime($hotRows[$tempRows]['start_date']) );
			$hotRows[$tempRows]['clickcount']=$hotRows[$tempRows]['clickcount']==null?0:$hotRows[$tempRows]['clickcount'];
			$hotRows[$tempRows]['isVideo']=$hotRows[$tempRows]['type']==1?1:0;
			$hotRows[$tempRows]['video_length']=$this->getFormatVideoLength($hotRows[$tempRows]['video_length']);
			//141218:ANN:音檔縮圖判斷(取副檔名)
			$extFile=strrchr($hotRows[$tempRows]['file'],".");
			//echo $extFile."<br>";
			if($extFile=='.mp3' || $extFile=='.wma'){
				if($hotRows[$tempRows]['file_img']==null){
					//$hotRows[$tempRows]['file_img']='upload_thumbnail/music.jpg';
					$hotRows[$tempRows]['file_img']='upload_thumbnail/picture.jpg';
				}
			}
			if($tempRows%$pre_row==0){
				//開始
				$hotRows[$tempRows]['startRows']=1;
			}else if(($tempRows+1)%$pre_row==0){
				//換行
				$hotRows[$tempRows]['endRows']=1;
			}
			/*}else if($tempRows==count($hotRows)){
				//結束
				if(($tempRows+1)%$pre_row!=0){
					$hotRows[$tempRows]['endRows']=1;
				}
			}*/
			$tempRows++;
		}
		
		$hotData['data']=$hotRows;
		$hotRowsMore=$this->chkMoreBtn(count($hotRows),'/hot/');//連結位置
		$hotData['more']=$hotRowsMore;
	 
	  
	  echo $this->main_page(array(
		'bodyClass' => 'index',
        'm_main' => 
          CM_Template::get_tpl_html('indexDefault', array(
			'newRows' => $newData,
			'hotRows' => $hotData,
			'keywordArea' => $keyword_area,
			'player'=>$player
		  ))
      ));
	 
	  
    }
	
	//產生player
	//public function getIndexPlayer($recordRows,$user_id,$domain_id){
	public function getIndexPlayer($recordRows){
	
		//echo 'domain_id='.$domain_id."<br>";
		//權限判斷
		$chkStatus=1;
		if($recordRows['login_status']==2){
			//會員(判斷有沒有登入，有登入就ok，沒登入bye
			$chkStatus=0;
		}
	  
		//裝置判斷
		$agent_tag=$this->getAgentType();
		//$agent_tag='mobile-test';
		if($agent_tag=='web'){
			
			//PC
			$isPc = true;
			$player = CM_Template::get_tpl_html('box_playerIndex',array(
					'vod_flash_server' => $this->_StreamServerPath,
					'videoData'=>$this->getVideoData($recordRows),
					//'thumbnail'=> $recordRows['file_img']
			));
			//print_r($this->getVideoData($recordRows));
		}
		else{
		
			//行動裝置
			$isPc = false;
			
			if($agent_tag=='ipad' || $agent_tag=='androidtablet'){
				$file_tag=2;
			}else{
				$file_tag=1;
			}
			
			//Terry報表判斷要加值
			if($domain_id==null){
				$domain_id=0;
			}
			if($user_id==null){
				$user_id=($domain_id==0)?'null':$domain_id;
			}
			
			$foldername = $this->getFolderName($recordRows[0]['file']);
			$filename = $this->getFileName($recordRows[0]['file']);
			
			//$file_url = 'http://'.$this->serverPath.':8080/vod/_definst_/'.$filename.'_1.mp4/playlist.m3u8';
			//$file_url = 'http://'.$this->_StreamServerPath.':8080/vod/_definst_/'.$foldername.$filename.'_'.$file_tag.'.mp4/playlist.m3u8';
			$file_url = 'http://'.$this->_StreamServerPath.':8080/vod/_definst_/'.$foldername.$filename.'_'.$file_tag.'.mp4/playlist.m3u8?iMda_seq='.$recordRows[0]['id'].'-Mdaf_seq='.'-iUsr_seq='.$user_id.'-iUgp_seq='.'-iDom_seq='.$domain_id;
			
			//$thumb_url = "/upload/".$recordRows['file_img'];
			
			$player = CM_Template::get_tpl_html('box_playerIndexM',array(
					'fileUrl' => $file_url,
					'videoData'=>$this->getVideoDataM($recordRows,$file_tag),
					//'thumbnail'=> $recordRows['file_img']
			)); 
			
			
		}
		$GLOBALS['isPc']=$isPc;
		return $player;
	
	}
	
	//判斷影片連結網址
	public function getVideoData($row){	

		//Terry報表判斷要加值
		if($domain_id==null){
			$domain_id=0;
		}
		if($user_id==null){
			$user_id=($domain_id==0)?'null':$domain_id;
		}
		
		for($i=0;$i<count($row);$i++){
			$foldername = $this->getFolderName($row[$i]['file']);
			$filename = $this->getFileName($row[$i]['file']);
			//echo $filename."<br>";
			//檔名組成
			$fileData[]= array(
				//'url'=>'mp4:cmvod/1205/1205_1689_'.($i+1).'.mp4?iMda_seq='.$row['id'].'-Mdaf_seq='.'-iUsr_seq='.$user_id.'-iUgp_seq=',
				//'url'=>'mp4:'.$foldername.$filename.'_'.'1'.'.mp4?iMda_seq='.$row[$i]['id'].'-Mdaf_seq='.'-iUsr_seq='.$user_id.'-iUgp_seq='.'-iDom_seq='.$domain_id,
				'url'=>'mp4:'.$foldername.$filename.'_'.'1'.'.mp4?iMda_seq='.$row[$i]['id'].'-Mdaf_seq='.'-iUsr_seq='.$user_id.'-iUgp_seq='.'-iDom_seq='.$domain_id,
			);
		}
		return $fileData;
	}
	
	//判斷影片連結網址(mobile)
	public function getVideoDataM($row,$file_tag){	

		//Terry報表判斷要加值
		if($domain_id==null){
			$domain_id=0;
		}
		if($user_id==null){
			$user_id=($domain_id==0)?'null':$domain_id;
		}
		
		for($i=0;$i<count($row);$i++){
			$foldername = $this->getFolderName($row[$i]['file']);
			$filename = $this->getFileName($row[$i]['file']);
			//echo $filename."<br>";
			//檔名組成
			$fileData[]= array(
				//'url'=>'mp4:cmvod/1205/1205_1689_'.($i+1).'.mp4?iMda_seq='.$row['id'].'-Mdaf_seq='.'-iUsr_seq='.$user_id.'-iUgp_seq=',
				//'url'=>'http://'.$this->_StreamServerPath.':8080/vod/_definst_/'.$foldername.$filename.'_'.$file_tag.'.mp4/playlist.m3u8',
				'url'=>'http://'.$this->_StreamServerPath.':8080/vod/_definst_/'.$foldername.$filename.'_'.$file_tag.'.mp4/playlist.m3u8?iMda_seq='.$row[$i]['id'].'-Mdaf_seq='.'-iUsr_seq='.$user_id.'-iUgp_seq='.'-iDom_seq='.$domain_id,
			);
		}
		return $fileData;
	}
	
	//檔案路徑處理
	public function getFolderName($fname){
		$strlen = strpos($fname,'/')+1;
		$endlen = strrpos($fname,'/')+1;
		$foldername = substr($fname,$strlen,$endlen-$strlen);
		return $foldername;
	}
	
	//檔名處理
	public function getFileName($fname){
		$strlen = strrpos($fname,'/')+1;
		$endlen = strrpos($fname,'.');
		$filename = substr($fname,$strlen,$endlen-$strlen);
		return $filename;
	}
	
	// moreButton樣式判斷
	public function chkMoreBtn($rowCount,$url){
		//剛好整除自己換新一行，否則接在後面
		if(($rowCount+1)%4==0){
			$moreBtn .="<div class='col-sm-6 col-lg-3 text-center'>";
			$moreBtn .="<a href=$url>";
			$moreBtn .="<img src='website/tmpl/img/buttonMore.png' alt='buttonMore'>";
			$moreBtn .="</a>";
			$moreBtn .="</div>";
			$moreBtn .="</div>";//關<div class='row'> 
		}else{
			$moreBtn = "<div class='row'>";//開<div class='row'> 
			$moreBtn .="<div class='col-sm-6 col-lg-3 text-center'>";
			$moreBtn .="<a href=$url>";
			$moreBtn .="<img src='website/tmpl/img/buttonMore.png' alt='buttonMore'>";
			$moreBtn .="</a>";
			$moreBtn .="</div>";
			$moreBtn .="</div>";//關<div class='row'> 
		}
		return $moreBtn;
	}
	
    /*
    public function page_contact() {
      $dao = Site_Factory::createCMDAO('contact');
      $data = array();
      $articleDao = Site_Factory::createCMDAO('article');
      $data['intro'] = $articleDao->get_rows_byId($recordRows, CM_DAO_Article::GROUP_STATIC, 1) ? $recordRows['desc'] : '';
      $form = CM_Factory::createCMForm();
      $form->set_form_dao($dao, CM_DAO_Contact::GROUP_SITE_ADD);
      $form->set_form_default($this->_post);
      $data['formAlert'] = $this->_alert->output('contact');
      $data['formTop'] = CM_Html::form('contactForm', Site::href_link($this->_path_rows['path'], array(Site::VAR_PROCESS => 'save')), 'post', 'class="cm-form"');
      $data['formBottom'] = '</form>';
      $data['form'] = $form->get_group_form('site_info', array());
      $data['formButton'] = CM_Html::submit_btn(self::lang('CONTACT_BTN'), array('class' => 'btn btn-primary btn-large'));
      $data['regField'] = $this->get_captcha_fields();
      $data['lang'] = array(
        'header' => $this->_path_rows[CM_DAO_Node::COLUMN_HEADLINE]
      );
      echo $this->main_page(array(
        'm_main' => CM_Template::get_tpl_html('indexContact', $data)
      ));
    }
    
    public function process_contact_save() {
      $success = false;
      $dao = Site_Factory::createCMDAO('contact');
      $this->_post['note'] = Site::filter_textarea($_POST['note']);
      $this->_post[CM_DAO_Contact::COLUMN_STATUS] = '1';
      
      if(!Site::validate_captcha($returnMessage, $this->_post)) {
        $this->_alert->add_error('contact', $returnMessage);
      } elseif(!$dao->insert_rows($returnMessage, $rReturn, $this->_post, CM_DAO_Contact::GROUP_SITE_ADD)) {
        $this->_alert->add_error('contact', $returnMessage);
      } else {
        $this->_post['sex'] = Site::get_options_name($this->_post['sex'], CM_DAO_Contact::get_sex_options());
        $data = array('info' => array());
        
        foreach($dao->get_column_field_label_array(CM_DAO_Contact::GROUP_SITE_MAIL) as $columnName => $columnLabel) $data['info'][] = "<b>{$columnLabel}：</b><br />{$this->_post[$columnName]}";
        
        $data['ip'] = Site::get_ip_address();
        $data['datetime'] = date('Y-m-d H:i:s');
        Site::mail_admin($this->_post['name'], $this->_post['email'], sprintf($this->lang('CONTACT_MAIL_SUBJECT'), CM_Lang::line('CM.SITE_NAME')), Site::get_mail_body('indexContactMail', $data));
        $this->_alert->add_success('global_alert', $this->lang('CONTACT_MAIL_SEND_SUCCESS'), true);
        Site::location(Site::href_link('/contact/'));
      }
      
      if($success == false) $this->page_contact();
    }
    
    public function page_about() {
      $this->_css[] = 'about.css';
      $this->page_static();
    }

    public function page_password_change_success() {
      $this->_css[] = 'static.css';
      $data['title'] = $this->lang('PASSWORD_FORGET_SUCCESS_TITLE');
      $data['desc'] = sprintf($this->lang('PASSWORD_FORGET_SUCCESS_CONTENT'), Site::href_link('/customers/login/'));
      echo $this->main_page(array(
        'm_main' => CM_Template::get_tpl_html('indexStatic', $data)
      ));
    }
    
    public function page_static() {
      $this->_css[] = 'static.css';
      $data = array();
      $current = $this->_path->get_node();
      $dao = Site_Factory::createCMDAO('article');
      
      if(!$dao->get_rows($recordRows, CM_DAO_Article::GROUP_STATIC, json_decode($current['mod_params'], true))) {
        $this->_alert->add_error('global_alert', '找不到指定的文案', true);
        Site::location(Site::href_link('/'));
      }
      
      $data['title'] = $recordRows[CM_DAO_Article::COLUMN_HEADLINE];
      $data['desc'] = $recordRows['desc'];
      echo $this->main_page(array(
        'm_main' => CM_Template::get_tpl_html('indexStatic', $data)
      ));
    }*/
  }      
?>