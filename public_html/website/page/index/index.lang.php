<?php
  CM_Lang::import('index', array(
    CM_Lang::CODE_TW => array(
      'CONTACT_BTN' => '送出訊息',
      'INDEX_PRODUCT_NEW' => '最新商品',
      'INDEX_PRODUCT_RECOMMEND' => '推薦商品',
      'INDEX_NEWS' => '最新消息',
      'CONTACT_MAIL_SUBJECT' => '一封來自%s的客戶詢問信件',
      'CONTACT_MAIL_SEND_SUCCESS' => '您的訊息已送出，感謝您',
      'PASSWORD_FORGET_SUCCESS_TITLE' => '密碼重新設定成功',
      'PASSWORD_FORGET_SUCCESS_CONTENT' => '<p>新密碼已經寄送到您的信箱了，請使用信件中的新密碼登入系統。</p><p>(收件夾未顯示來信請檢視垃圾郵件,謝謝)</p><p>請按&nbsp;<a href="%s">[這裡]</a>&nbsp;登入</p>',
    ),
    CM_Lang::CODE_CN => array(
      'CONTACT_BTN' => '送出讯息',
      'INDEX_PRODUCT_NEW' => '最新商品',
      'INDEX_PRODUCT_RECOMMEND' => '推荐商品',
      'INDEX_NEWS' => '最新消息',
      'CONTACT_MAIL_SUBJECT' => '一封来自%s的客户询问信件',
      'CONTACT_MAIL_SEND_SUCCESS' => '您的讯息已送出，感谢您',
      'PASSWORD_FORGET_SUCCESS_TITLE' => '密码重新设定成功',
      'PASSWORD_FORGET_SUCCESS_CONTENT' => '<p>新密码已经寄送到您的信箱了，请使用信件中的新密码登入系统。</p><p>(收件夹未显示来信请检视垃圾邮件,谢谢)</p><p>请按&nbsp;<a href="%s">[这里]</a>&nbsp;登入</p>',
    )
  ));
?>