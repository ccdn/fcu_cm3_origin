<?php
  class Site_Page {
    const URL_MODE = 'rewrite';
    
    const VAR_BACK_PATH = '_bPath';
    
    const VAR_BACK_PARAMS = '_bParams';

    const VAR_SHOPPING_CART = '_shoppingCart';
    
    const VAR_ORDERS_INFO = '_ordersInfo';
    
    const VAR_PAYMENT_ORDERS_ID = '_paymentOrdersId';
    
    protected $_cart = null;
    
    protected $_customers_rows = array();
    //名稱
    protected $_page_name = '';
    //要載入的css
    protected $_css = array();
    //要載入的javascript
    protected $_javascript = array();
    //目前路徑
    protected $_path = null;
    //目前路徑參數
    protected $_path_rows = array();
    /**
     * 經過過濾的$_GET變數(複製)
     */
    protected $_get = array();
    /**
     * 經過過濾的$_POST變數(複製)
     */
    protected $_post = array();
    /**
     * 等同於原本的$_SESSION變數
     */
    protected $_session = array();
    //暫存的DAO
    protected $_dao = array();
    //暫存的Widget
    protected $_widget = array();
    //訊息物件
    public $_alert = null;
    //麵包屑物件
    public $_breadcrumb = null;
	
	//server路徑
	public $_StreamServerPath=null;
	
	//ntl
	public $_ntl=null;
	public $_xajax_javascript=null;
    
    static public function get_page_folder($pageName, $fPath = true) {
      return ($fPath == true ? CM_Conf::get('PATH_F.SITE_PAGE') : CM_Conf::get('PATH_W.SITE_PAGE')) . $pageName . '/';
    }

    static public function cb_default_list_pagesplit($page, array $params, $key, $path) {
      $attr = array();
      $attr['class'] = array();
      $pageLink = '';
      
      if($params['disable'] == true) {
        $href = 'javascript:;';

        if($params['current'] == true) {
          $attr['class'][] = 'active';
        } else {
          $attr['class'][] = 'disabled';
        }
      } else {
        $href = Site::href_link($path, array_merge(Site::get_all_get_params(array('p'), true), array('p' => $page)));

        if($params['current'] == true) $attr['class'][] = 'active';
      }
      
      if($key === 'prevPage') {
        $attr['class'][] = 'prev';
      } 
      
      if($key === 'nextPage') {
        $attr['class'][] = 'next';
      }
      
      $attr['class'] = implode(' ', $attr['class']);
      $tmpAttr = array();
      
      foreach($attr as $key => $value) $tmpAttr[] = "{$key}=\"{$value}\"";
      
      return sprintf('<li %s><a href="%s">%s</a></li>', implode(' ', $tmpAttr), $href, $params['text']);
    }

    static public function get_widget_class_name($widgetName, $name) {
      return str_replace('get_box_', $widgetName . '_widget_', $name);
    }

    static public function get_catalog_tmpl_menu(CM_DAO_Catalog $dao, $code, $path = 0, $params = array(), $level = 0) {
      $return = array();
      $params = array_merge(array(
        'level' => 0,
        'current' => array(),
        'callback' => null,
        'dao_params' => array()
      ), $params);
      $chLevel = $level + 1;
      $daoParams = $params['dao_params'];
      $daoParams[CM_DAO_Catalog::COLUMN_CODE] = $code;
      $daoParams['tree'] = array(
        'func' => 'children',
        'id' => $path
      );

      if($dao->get_array($recordArray, CM_DAO_Catalog::GROUP_ID, $daoParams)) {
        foreach($recordArray as $recordRows) {
          $subRows = self::get_catalog_tmpl_menu($dao, $code, $recordRows[CM_DAO_Catalog::COLUMN_RECORD_ID], $params, $chLevel);
          $rows = array(
            'text' => $recordRows[CM_DAO_Catalog::COLUMN_HEADLINE], //文字
            'hasSub' => !empty($subRows), //是否有底層
            'sub' => $subRows //底層
          );

          if(is_callable($params['callback'])) $rows = call_user_func($params['callback'], $rows, $recordRows, $params, $level);

          $return[] = $rows;          
        }

        
      }

      return $return;
    }

    static public function get_catalog_parent_rows(CM_DAO_Catalog $dao, $path, $code, $rootId = 0) {
      $return = array();

      if(empty($path)) {
        //參數空白: 回傳空值 = 設定的根目錄
        return $return;
      } elseif($path == $rootId) {
        //和根目錄相同: 回傳空值 = 設定的根目錄
        return $return;
      } elseif(!$dao->get_rows_byId($recordRows,'', (int)$path)) {
        //找不到目錄: 回傳空值 = 設定的根目錄
        return $return;
      } else {
        //處理層級架構
        $catalogDAOParams = array(
          'code' => $code,
          'tree' => array(
            'func' => 'parent_all',
            //'id' => $recordRows[CM_DAO_Catalog::COLUMN_RECORD_ID],
			'id' => $recordRows['id'],
            'params' => array('rootId' => $rootId)
          )
        );
        //取得上層目錄
        if($dao->get_array($parentArray, 'id', $catalogDAOParams)) {
          foreach($parentArray as $parentRows) $return[] = $parentRows;
        }

        $return[] = $recordRows;
        return $return;
      }
    }
    
    public function __construct(Site_Path $path, $pageName) {	
	  require_once('website/tmpl/ntl.config.inc.php');
      $this->_css = array(
        /*'bootstrap.min.css',
        'bootstrap-theme.min.css',
        'font-awesome.min.css',
        'base.css'*/
		'font-awesome.css',
		'bootstrap.css',
		'style.css',
		'style_override.css',
		'tagsCloud.css',
		//'jquery-ui.css'
      );
      //載入頁面專屬CSS
      if(is_file(CM_Conf::get('PATH_F.SITE_CSS') . 'page_' . $pageName . '.css')) $this->_css[] = 'page_' . $pageName . '.css';
	  //把音樂控制會衝突的js槓掉
	  $tempPath=$path->get_id();
	  if($pageName=="sound" && $tempPath['path_name']=="detail"){
		$this->_javascript = array(
		//CM_Conf::get('PATH_W.SITE_JS') . 'jquery.js',
		//CM_Conf::get('PATH_W.SITE_JS') .'jquery-1.10.2.min.js',
		CM_Conf::get('PATH_W.SITE_JS') .'bootstrap.min.js',	
		//'http://maps.google.com/maps/api/js?sensor=false',
		//CM_Conf::get('PATH_W.SITE_JS') . 'mapmarker.jquery.js',
		CM_Conf::get('PATH_W.SITE_JS') . 'jquery.easing.1.3.js',
		CM_Conf::get('PATH_W.SITE_JS') . 'jquery.mb.YTPlayer.js',
		CM_Conf::get('PATH_W.SITE_JS') . 'retina-1.1.0.js',
		CM_Conf::get('PATH_W.SITE_JS') . 'jquery.nav.js',
		CM_Conf::get('PATH_W.SITE_JS') . 'jquery.hoverex.js',
		CM_Conf::get('PATH_W.SITE_JS') . 'jquery.scrollTo.js',
		CM_Conf::get('PATH_W.SITE_JS') . 'jquery.unveilEffects.js',
		CM_Conf::get('PATH_W.SITE_JS') . 'jquery.isotope.min.js',
		CM_Conf::get('PATH_W.SITE_JS') . 'jquery.hoverdir.js',
		CM_Conf::get('PATH_W.SITE_JS') . 'jquery.superslides.js',
		//CM_Conf::get('PATH_W.SITE_JS') . 'jquery.animate-enhanced.min.js',
		CM_Conf::get('PATH_W.SITE_JS') . 'jquery.easypiechart.min.js',
		CM_Conf::get('PATH_W.SITE_JS') . 'testimonials.js',
		CM_Conf::get('PATH_W.SITE_JS') . 'jquery.jigowatt.js',
		CM_Conf::get('PATH_W.SITE_JS') . 'jquery.prettyPhoto.js',
		CM_Conf::get('PATH_W.SITE_JS') . 'custom.js',
		CM_Conf::get('PATH_W.SITE_JS') . 'holder.js',
		CM_Conf::get('PATH_W.SITE_JS') . 'slider.js',
		'http://code.jquery.com/ui/1.11.1/jquery-ui.js'
		);
	  }else{
		  $this->_javascript = array(
			//CM_Conf::get('PATH_W.SITE_JS') . 'jquery.js',
			CM_Conf::get('PATH_W.SITE_JS') .'jquery-1.10.2.min.js',
			CM_Conf::get('PATH_W.SITE_JS') .'bootstrap.min.js',	
			//'http://maps.google.com/maps/api/js?sensor=false',
			//CM_Conf::get('PATH_W.SITE_JS') . 'mapmarker.jquery.js',
			CM_Conf::get('PATH_W.SITE_JS') . 'jquery.easing.1.3.js',
			CM_Conf::get('PATH_W.SITE_JS') . 'jquery.mb.YTPlayer.js',
			CM_Conf::get('PATH_W.SITE_JS') . 'retina-1.1.0.js',
			CM_Conf::get('PATH_W.SITE_JS') . 'jquery.nav.js',
			CM_Conf::get('PATH_W.SITE_JS') . 'jquery.hoverex.js',
			CM_Conf::get('PATH_W.SITE_JS') . 'jquery.scrollTo.js',
			CM_Conf::get('PATH_W.SITE_JS') . 'jquery.unveilEffects.js',
			CM_Conf::get('PATH_W.SITE_JS') . 'jquery.isotope.min.js',
			CM_Conf::get('PATH_W.SITE_JS') . 'jquery.hoverdir.js',
			CM_Conf::get('PATH_W.SITE_JS') . 'jquery.superslides.js',
			CM_Conf::get('PATH_W.SITE_JS') . 'jquery.animate-enhanced.min.js',
			CM_Conf::get('PATH_W.SITE_JS') . 'jquery.easypiechart.min.js',
			CM_Conf::get('PATH_W.SITE_JS') . 'testimonials.js',
			CM_Conf::get('PATH_W.SITE_JS') . 'jquery.jigowatt.js',
			CM_Conf::get('PATH_W.SITE_JS') . 'jquery.prettyPhoto.js',
			CM_Conf::get('PATH_W.SITE_JS') . 'custom.js',
			CM_Conf::get('PATH_W.SITE_JS') . 'holder.js',
			CM_Conf::get('PATH_W.SITE_JS') . 'slider.js',
			'http://code.jquery.com/ui/1.11.1/jquery-ui.js'
		  );
	  }
      //$this->_javascript = array_merge($this->_javascript, Site::load_package_javascript(CM_Conf::get('PATH_F.SITE_JS') . 'jquery.api/', CM_Conf::get('PATH_W.SITE_JS') . 'jquery.api/'));
      //載入頁面專屬Javascript
      if(is_file(self::get_page_folder($pageName) . $pageName . '.js')) $this->_javascript[] = self::get_page_folder($pageName, false) . $pageName . '.js';
      
      $this->_path = $path;
      $this->_path_rows = $path->get_id();
      $this->_get = Site::filter_input($_GET);
      $this->_post = Site::filter_input($_POST);
      $this->_session = &$_SESSION;
      $this->_alert = Site_Factory::createSiteAlert();
      $this->_page_name = $pageName;
      $this->_breadcrumb = Site_Factory::createCMBreadcrumb();
      $pathArray = $this->_path->get_parent_all();
	  
	  //ANN:取得stream server設定
	  $this->get_dao('stream_Server')->get_rows_byId($streamServer,'',1);
	  $this->_StreamServerPath = $streamServer['cVod_server_domain'];
	  
	  //ANN:取得ntl設定
	  $this->_ntl=new ntl();

      if($this->_path_rows['menu_status'] != '0') $pathArray[] = $this->_path_rows;

      foreach($pathArray as $pathRows) $this->_breadcrumb->add($pathRows['headline'], Site::href_link( htmlspecialchars($pathRows['path']) ));

      //echo("<script>console.log('PHP: ".$this->_path_rows['path']."');</script>");
	  // $this->init_shopping_cart();
    }

    protected function get_orders_byId(&$rMessage, &$rData, $ordersId, $params = array()) {
      $params = array_merge(array(
        'op_column' => array(), //追加顯示的欄位
        'op_column_cb' => array()
      ), $params);
      $rMessage = '';
      $ordersDAO = $this->get_dao('orders');
      $ordersProductDAO = $this->get_dao('orders_Product');
      $ordersTotalDAO = $this->get_dao('orders_Total');

      if(
        empty($ordersId) ||
        !$ordersDAO->get_rows_byId($ordersRows, '', $ordersId) ||
        !$ordersProductDAO->get_array($ordersProductArray, '', array('ordersId' => $ordersRows[CM_DAO_Orders::COLUMN_RECORD_ID])) ||
        !$ordersTotalDAO->get_array($ordersTotalArray, '', array('ordersId' => $ordersRows[CM_DAO_Orders::COLUMN_RECORD_ID]))
      ) {
        $rMessage = CM_Lang::line('Site.ERROR_DATA_NOT_FOUND');
        return false;
      }
      
      $fileDAO = $this->get_dao('uploadFile');
      $productDAO = $this->get_dao('product');
      $rData = array();
      //訂單資訊
      $rData['ordersInfo'] = array(
        'id' => array(
          'label' => CM_Lang::line('Site.ORDERS_ID'),
          'value' => CM_Format::to_orders_id($ordersRows[CM_DAO_Orders::COLUMN_RECORD_ID])
        ),
        'status' => array(
          'label' => CM_Lang::line('Site.ORDERS_STATUS'),
          'value' => Site::get_options_name($ordersRows[CM_DAO_Orders::COLUMN_STATUS], CM_DAO_Orders::get_status_options())
        )
      );
      //訂單商品
      $rData['cart'] = array(
        'title' => $this->lang('SHOPPING_CART_TITLE'),
        'product' => CM_Format::to_tmpl_table(
          $ordersProductArray,
          array_merge($params['op_column'], array(
            'id' => CM_Lang::line('CM_DAO_Product.COLUMN_ID'),
            'img' => CM_Lang::line('CM_DAO_Product.COLUMN_FILE'),
            'headline' => CM_Lang::line('CM_DAO_Product.COLUMN_HEADLINE'),
            'price_text' => CM_Lang::line('CM_DAO_Product.COLUMN_PRICE'),
            'qty' => $this->lang('SHOPPING_CART_COLUMN_QTY'),
            'total_text' => $this->lang('SHOPPING_CART_COLUMN_SUBTOTAL')
          )),
          array_merge($params['op_column_cb'], array(
            'img' => function($record, $recordRows = array()) use($fileDAO, $productDAO) {
              if($productDAO->get_rows_byId($productRows, CM_DAO_Product::GROUP_LIST, $recordRows['product_id'])) {
                return array('attr' => 'align="center"', 'text' => CM_Html::img($fileDAO->get_image($productRows['file'], '80x60'), $productRows[CM_DAO_Product::COLUMN_HEADLINE]));
              } else {
                return array('attr' => 'align="center"', 'text' => '--');
              }
            }
          )),
          array(
            'hcallback' => function($columnName, $label, $funcParams) { return array('attr' => 'class="header_' . $columnName . '" align="center" valign="center"', 'text' => $label); },
            'hcallback_def' => function($record, $recordRows = array()) { return array('attr' => 'align="center"', 'text' => $record); }
          )
        ),
        'total' => $ordersTotalArray
      );
      //付款方式
      $payment = Site_Factory::createCMSCartPayment($ordersRows['payment']);
      $rData['payment_method'] = array(
        'title' => $this->lang('SHOPPING_CART_PAYMENT_METHOD_TITLE'),
        'method' => array(
          'id' => $payment::CODE,
          'text' => $payment->get_name(),
          'intro' => ''
        )
      );
      //付款人資料
      $ordersform = Site_Factory::createCMForm();
      $ordersform->set_form_dao($ordersDAO, CM_DAO_Orders::GROUP_ADD);
      $ordersform->set_form_default($ordersRows);
      $rData['payment'] = $ordersform->get_group_form('info', array(
        'title' => $this->lang('SHOPPING_CART_PAYMENT_TITLE')
      ), CM_Form::ACTION_DISPLAY, array('cKey' => true));
      //發票資料
      $rData['invoice'] = $ordersform->get_group_form('invoice', array(
        'title' => $this->lang('SHOPPING_CART_INVOICE_TITLE'),
        'show' => isset($ordersRows['invoice']) && $ordersRows['invoice'] == 1 ? true : false
      ), CM_Form::ACTION_DISPLAY, array('cKey' => true));
      //寄件人資料
      $rData['addressee'] = $ordersform->get_group_form('addressee', array(
        'title' => $this->lang('SHOPPING_CART_ADDRESSEE_TITLE')
      ), CM_Form::ACTION_DISPLAY, array('cKey' => true));
      return true;
    }
    
    protected function login_authorized() {
      if(Site::check_is_login() == false) {
        $this->_alert->add_error('global_alert', CM_Lang::line('Site.ERROR_NOT_LOGIN'), true);
        Site::location(Site::href_link('/customers/login/'));
      }
    }

    public function lang($key) {
      return CM_Lang::line($this->_page_name . '.' . $key, '', self::get_page_folder($this->_page_name));
    }

    public function get_path() {
      return $this->_path;
    }

    public function get_path_rows() {
      return $this->_path_rows;
    }

    public function get_http_get_params() {
      return $this->_get;
    }

    public function get_http_post_params() {
      return $this->_post;
    }

    public function get_dao($name) {
      if(!isset($this->_dao[$name])) $this->_dao[$name] = Site_Factory::createCMDAO($name);

      return $this->_dao[$name];
    }

    public function get_current_url() {
      return Site::href_link($this->_path_rows['path'], Site::get_all_get_params());
    }

    public function get_login_url() {
      return Site::href_link('/customers/login/', array(Site::VAR_BACK_URL => urlencode($this->get_current_url())));
    }

    public function check_back_url($url) {
      $url = parse_url($url);
      
      if(!isset($url['host']) || $url['host'] != CM_Conf::get('HTTP.HOST')) {
        return false;
      } elseif(!isset($url['host']) || !$this->_path->check_path_in_tree($url['path'])) {
        return false;
      } else {
        return true;
      }
    }

    protected function init_shopping_cart() {
      //建立購物車內容的Session
      if(!isset($this->_session[self::VAR_SHOPPING_CART])) $this->_session[self::VAR_SHOPPING_CART] = array();

      if(!isset($this->_session[self::VAR_ORDERS_INFO])) $this->_session[self::VAR_ORDERS_INFO] = array();

      if(Site::check_is_login() == true) {
        //取得會員資料
        if(!$this->get_dao('customers')->get_rows_byId($this->_customers_rows, CM_DAO_Customers::GROUP_SITE_INFO, $this->_session[Site::VAR_CUSTOMERS_ID])) {
          $this->_alert->add_error('global_alert', CM_Lang::line('Site.ERROR_DATA_NOT_FOUND'), true);
          unset($this->_session[Site::VAR_CUSTOMERS_ID]);
          unset($this->_session[Site::VAR_CUSTOMERS_NAME]);
          unset($this->_session[Site::VAR_CUSTOMERS_EMAIL]);
          Site::location(Site::href_link('/product/'));
        }
      }
      //將Session設定到購物車物件內
      $this->_cart = Site_Factory::createCMSCart($this->_session[self::VAR_SHOPPING_CART]);
      //設定Total物件
      //$this->_cart->set_total(CM_Factory::createCMSCartTotal('discount'));
      //$this->_cart->set_total(CM_Factory::createCMSCartTotal('freight'));
      //設定Payment物件
      $this->_cart->set_payment(CM_Factory::createCMSCartPayment('ATM'));
      //$this->_cart->set_payment(CM_Factory::createCMSCartPayment('ECBankPaypal'));
      //$this->_cart->set_payment(CM_Factory::createCMSCartPayment('ECBankVirtualAccount'));
      //$this->_cart->set_payment(CM_Factory::createCMSCartPayment('ECBankVirtualCode'));
      //$this->_cart->set_payment(CM_Factory::createCMSCartPayment('ECBankIbon'));
    }

    public function main_meta($data = array()) {
	  //登入判斷
	  $ntl = $this->_ntl;
	  //$xajax_javascript = $ntl->render_xajax_javascript(array(array($this,'front_login')), false);
	  //$data = array_merge(array('xajax_javascript'=>$xajax_javascript), $data);
	  
	  //其他變數
      $data = array_merge(array(
        'title' => CM_Lang::line('CM.SITE_NAME'),
        'base_url' => CM_Conf::get('HTTP.SERVER'),
        'css' => array()
      ), $data);
	  //print_r($data);
      $path = CM_Conf::get('PATH_W.SITE_CSS');
      $css = array();
      
      foreach(array_merge($data['css'], $this->_css) as $name) $css[] = $path . $name;
      
      $data['css'] = $css;
	  return CM_Template::get_tpl_html('meta', $data);
      //return CM_Template::get_tpl_html('mainMeta', $data);
    }

    /*public function main_header($data = array()) {
      $data = array_merge(array(), $data);
      //首頁設定
      $topPage = $this->_path->get_id('/');
      $currentPath = array();
      $currentPath = Site::get_key_array((array)$this->_path->get_parent_all(), 'path');
      $currentPath[] = $this->_path->get_path();
      //右邊選單
      //$rightMenu = array();
      //$rightMenu[] = array($this->_path->get_node_item('/shopping/'));
      //是否開啟多語系
      if(CM_Conf::get('DEF.SITE_LANG_MODE') == 'true') {
        $langMenu = array();
        $langOptions = CM_Lang::get_code_options(true);
        //把目前的語系放在前面
        if(Site::in_options(CM_Lang::get_current_code(true), $langOptions, $currentLang)) {
          $langMenu[] = Site_Factory::createSitePathMenu($currentLang['text'], Site::href_link($this->_path_rows['path'], Site::get_all_get_params(), array('langCode' => $currentLang['id'])));
        }
        
        foreach($langOptions as $options) {
          if($options['id'] == CM_Lang::get_current_code(true)) continue;
          
          $langMenu[] = Site_Factory::createSitePathMenu($options['text'], Site::href_link($this->_path_rows['path'], Site::get_all_get_params(), array('langCode' => $options['id'])));
        }

        if(!empty($langMenu)) $rightMenu[] = $langMenu;
      }

      if(Site::check_is_login() == true) {
        //會員選單
        $rightMenu[] = array(
          $this->_path->get_node_item('/customers/'),
          $this->_path->get_node_item('/customers/edit/'),
          $this->_path->get_node_item('/customers/password_change/'),
         // $this->_path->get_node_item('/customers/orders/'),
          Site_Factory::createSitePathMenu(CM_Lang::line('Site.PROCESS_LOGOFF'), Site::href_link('/customers/', array(Site::VAR_PROCESS => 'logoff')))
        );
      } else {
        //非會員選單
        $rightMenu[] = array(
          $this->_path->get_node_item('/customers/'),
          $this->_path->get_node_item('/customers/register/'),
          $this->_path->get_node_item('/customers/forget/')
        );
      }

      $data['topMenu'] = array(
        'id' => 'mainmenu',
        'header' => array(
          'url' => CM_Conf::get('HTTP.SERVER') . CM_Conf::get('PATH_W.ROOT'),
          'text' => CM_Lang::line('CM.SITE_NAME')
        ),
        'hasItems' => true,
        'items' => implode('', array(
          CM_Template::get_tpl_html('components_navbarItemsUl', array(
            'isRight' => false,
            'items' => Site_Format::to_menu_item_tmpl(array(
              array($this->_path->get_node_item('/')),
              array($this->_path->get_node_item('/news/')),
			  array($this->_path->get_node_item('/customers/')),
              //array($this->_path->get_node_item('/product/')),
              //array($this->_path->get_node_item('/contact/'))
            ))
          )),
          CM_Template::get_tpl_html('components_navbarItemsUl', array(
            'isRight' => true,
            'items' => Site_Format::to_menu_item_tmpl($rightMenu)
          ))
        ))
      );
      return CM_Template::get_tpl_html('mainHeader', $data);
    }*/
	
	public function main_header($data = array()) {
	
      //判斷pc
	  $isPc=$this->isPc();
	  //$data = array_merge(array('isPc'=>$isPc), $data);
	  $checkPathData = $this->checkPath();
	  //判斷是否登入
	  //if(Site::check_is_login() == false) {
	  //print_r(print_r($this->_session));
	  if($this->_session[Site::VAR_AUTH]==false && empty($this->_session[Site::VAR_AUTH])){
		//登入區塊
		$loginForm = CM_Template::get_tpl_html('box_loginForm', $data);
		$loginForm_m = "<a class='' href='/login_m/'>登入</a>";
	  }else{
		//登出區塊
		$username = $this->_session[Site::VAR_CUSTOMERS_ID];
		$data = array_merge(array('username'=>$username),$data);
		$loginForm = CM_Template::get_tpl_html('box_logoutForm', $data);
		$loginForm_m = "<a class='' href='javascript:;' onclick='return xajax_front_logout()'>登出</a>";
	  }
	  $data = array_merge($checkPathData, $data);
	  $data = array_merge(array('isPc'=>$isPc,'loginForm'=>$loginForm,'loginForm_m'=>$loginForm_m), $data);
	  
	  //$data = array_merge(array(), $data);
      //上方menu
      //$data['text'] = CM_Lang::line('Site.COPYRIGHT');
      return CM_Template::get_tpl_html('header', $data);
    }
    
    public function main_footer($data = array()) {
      $data = array_merge(array(), $data);
      //頁尾文章
      //$data['text'] = CM_Lang::line('Site.COPYRIGHT');
      return CM_Template::get_tpl_html('footer', $data);
    }

    public function main_left_column($data = array()) {
      return CM_Template::get_tpl_html('mainLeftColumn', $data);
    }
    
    public function main_page($data = array()) {
	 
	  if($this->_path_rows['path']=='/'){
		$isIndex=true;
	  }
		
      $data = array_merge(array(
        'm_meta' => '',
        'm_header' => '',
        'm_footer' => '',
        'm_main' => '',
        'm_hasLeft' => false,
        'm_left' => ''
      ), $data);
      
      if(empty($data['m_meta'])) $data['m_meta'] = $this->main_meta();
      
	  //有少原件會壞掉先槓掉
      if(empty($data['m_header'])) $data['m_header'] = $this->main_header();
      
      if(empty($data['m_footer'])) $data['m_footer'] = $this->main_footer();

      if($data['m_hasLeft'] == true) {
        if(empty($data['m_left'])) $data['m_left'] = $this->main_left_column();
      }
      //建立購物車內容的Session
      /*
      if(!isset($this->_session[CM_ShoppingCart::VAR_NAME])) $this->_session[CM_ShoppingCart::VAR_NAME] = array();

      if(!empty($this->_session[CM_ShoppingCart::VAR_NAME])) {
        $data['shopping_cart'] = array(
          'count' => count($this->_session[CM_ShoppingCart::VAR_NAME]),
          'url' => Site::href_link('/shopping/')
        );
      }
      */

      return CM_Template::get_tpl_html('main', array_merge($data, array(
		'isIndex' => $isIndex,
        'global_alert' => $this->_alert->output('global_alert'),
		'xajax_javascript'=>$this->_xajax_javascript,
        'javascript' => $this->_javascript,
        'pageName' => $this->_page_name,
        'breadcrumb' => array(
          'hasTrail' => true,
          'trail' => $this->_breadcrumb->trail()
        )
      )));
    }

    public function simple_main_page($data = array()) {
      $data = array_merge(array(
        'm_meta' => '',
        'm_main' => ''
      ), $data);
      
      if(empty($data['m_meta'])) $data['m_meta'] = $this->main_meta();

      $data['m_header'] = array(
        'id' => 'mainmenu',
        'header' => array(
          'url' => CM_Conf::get('HTTP.SERVER') . CM_Conf::get('PATH_W.ROOT'),
          'text' => CM_Lang::line('CM.SITE_NAME')
        ),
        'hasItems' => false,
        'items' => ''
      );

      return CM_Template::get_tpl_html('simpleMain', array_merge($data, array(
        'global_alert' => $this->_alert->output('global_alert'),
        'javascript' => $this->_javascript
      )));
    }

    public function page_notfound() {
      //找不到指定的頁面
      echo $this->simple_main_page(array('m_main' => sprintf(CM_Lang::line('Site.PAGE_NOT_FOUND'), CM_Conf::get('HTTP.SERVER'))));
    }
    
    public function page_sitemode_auth() {
      //需要驗證的畫面
      $form = Site_Factory::createCMForm(array(
        'account' => Site_Factory::createCMColumn('account', array(
          'label' => CM_Lang::line('Site.AUTH_ACCOUNT_LABEL'),
          'f_class' => 'form-control',
          'f_placeholder' => sprintf(CM_Lang::line('Site.PLACEHOLDER_DEFAULT'), CM_Lang::line('Site.AUTH_ACCOUNT_LABEL'))
        ), 'name'),
        'password' => Site_Factory::createCMColumn('password', array(
          'label' => CM_Lang::line('Site.AUTH_PASSWORD_LABEL'),
          'f_class' => 'form-control',
          'f_confirm' => false,
          'f_placeholder' => sprintf(CM_Lang::line('Site.PLACEHOLDER_DEFAULT'), CM_Lang::line('Site.AUTH_PASSWORD_LABEL'))
        ), 'password')
      ));

      echo $this->simple_main_page(array(
        'm_main' => CM_Template::get_tpl_html('simple_auth', array(
          'formIntro' => nl2br(CM_Conf::get('DEF.SITEMODE_AUTH_TEXT')),
          'formTop' => CM_Html::form('authForm', Site::href_link($this->_path_rows['path'], array(Site::VAR_PROCESS => 'auth')), 'post', 'class="cm-form form-horizontal" role="form"'),
          'formField' => $form->get_form(),
          'regField' => Site::get_recaptcha_fields(),
          'formButton' => CM_Html::submit_btn(sprintf('<i class="fa fa-key"></i>&nbsp;%s', CM_Lang::line('Site.AUTH_SUBMIT_BTN')), array('class' => 'btn btn-primary')),
          'formBottom' => '</form>'
        ))
      ));
    }

    public function process_sitemode_auth() {
      if(!Site::validate_recaptcha($returnMessage, $this->_post)) {
        $this->_alert->add_error('global_alert', $returnMessage, true);
      } elseif($this->_post['account'] != CM_Conf::get('DEF.SITEMODE_AUTH_ACCOUNT') || $this->_post['password'] != CM_Conf::get('DEF.SITEMODE_AUTH_PASSWORD')) {
        $this->_alert->add_error('global_alert', CM_Lang::line('Site.AUTH_ERROR'), true);
      } else {
        $this->_session[Site::VAR_AUTH] = true;
      }

      Site::location(Site::href_link($this->_path_rows['path']));
    }
    
    public function page_sitemode_off() {
      //網站關閉畫面
      echo $this->simple_main_page(array('m_main' => nl2br(CM_Conf::get('DEF.SITEMODE_OFF_TEXT'))));
    }
	
	//ANN:140825:agent
	//判斷使用者介面
	public function getAgentType(){

		if(stristr($_SERVER['HTTP_USER_AGENT'],"android")){
			if(stristr($_SERVER['HTTP_USER_AGENT'],"mobile") ){
				$agent_tag='android';
			}else{
				$agent_tag='androidtablet';
			}
		
		}else{
						
			if(strstr($_SERVER['HTTP_USER_AGENT'],"iPhone")){
				$agent_tag='iphone';
			}elseif(strstr($_SERVER['HTTP_USER_AGENT'],"iPad")){
				$agent_tag='ipad';
			}else{
			
				$agent_tag='web';
			}
					
		}
		
		return $agent_tag;
		//$ntl->assign('agent_tag', $agent_tag);
	}
	
	public function isPc(){
		$agent_tag=$this->getAgentType();
		if($agent_tag=='web'){
			//PC
			$isPc = true;
		}else{
			//行動裝置
			$isPc = false;
		}
		return $isPc;
	}
	
	
	
	public function front_login($form)
	{
	
		$ntl = $this->_ntl;		
		$objResponse = new xajaxResponse();
		
		// 測試機
		$login_id = htmlentities($form['cUsr_uid']);
		$login_pd = htmlentities($form['cUsr_password']);
		//之後有可能要改成 htmlentities ,mysql_real_escape_string 
		
		//登入成功記在session
		if($login_id=='a'&& $login_pd=='a'){
		
			$this->_session[Site::VAR_CUSTOMERS_ID]=$login_id;
			$this->_session[Site::VAR_CUSTOMERS_NAME]=$login_id;
			$this->_session[Site::VAR_AUTH] = true;
			//一般是重讀頁面，手機是回到首頁
			$isPc = $this->isPc();
			if($isPc==1 && $this->_path_rows['path']!='/login_m/'){
				$objResponse->script('window.location.reload()');
			}else{
				$objResponse->redirect('/index/');
			}
			//$objResponse->alert("success");
			//$loginForm = CM_Template::get_tpl_html('box_logoutForm', $data);
			//$objResponse->assign("loginDiv","innerHTML",$loginForm);
		}else{
			$objResponse->alert("帳號或密碼錯誤");
		}
		return $objResponse;
		
		// 正式機
		/*$login_id = htmlentities($form['cUsr_uid']);
		//$login_pd = htmlentities($form['cUsr_password']);				
		$login_pd = urlencode($form['cUsr_password']);		
		$url='http://140.134.131.88/ldap_auth.php?login_id='.$login_id.'&login_pd='.$login_pd;		
		$apiContent=file_get_contents($url);
		$apiContent=trim($apiContent);
		
		if ($apiContent=="1"){
				
			//認證有通過
			//echo "passed!";
			//預設為uid=但逢甲為cn=
			
			$this->_session[Site::VAR_CUSTOMERS_ID]=$login_id;
			$this->_session[Site::VAR_CUSTOMERS_NAME]=$login_id;
			$this->_session[Site::VAR_AUTH] = true;
			//一般是重讀頁面，手機是回到首頁
			$isPc = $this->isPc();
			if($isPc==1 && $this->_path_rows['path']!='/login_m/'){
				$objResponse->script('window.location.reload()');
				return $objResponse;
			}else{
				$objResponse->redirect('/index/');
				return $objResponse;
			}
			//$objResponse->alert("success");
			//$loginForm = CM_Template::get_tpl_html('box_logoutForm', $data);
			//$objResponse->assign("loginDiv","innerHTML",$loginForm);			
		
		}else{
			
			//認證沒有通過
			//echo "failed!";
			
			$objResponse->alert("帳號或密碼錯誤");
			
			return $objResponse;
			
		}
		*/
	
	}
	
	
	public function front_logout()
	{
		unset($this->_session[Site::VAR_CUSTOMERS_ID]);
		unset($this->_session[Site::VAR_CUSTOMERS_NAME]);
		$this->_session[Site::VAR_AUTH] = false;
	
		$ntl = $this->_ntl;
		
		$objResponse = new xajaxResponse();
	
		$objResponse->alert('您已經登出系統');
		
		//一般是重讀頁面，手機是回到首頁
		$isPc = $this->isPc();
		if($isPc==1 && $this->_path_rows['path']!='/login_m/'){
			$objResponse->script('window.location.reload()');
		}else{
			$objResponse->redirect('/index/');
		}
		
		//$objResponse->script('window.location.reload()');
		//$loginForm = CM_Template::get_tpl_html('box_loginForm', $data);
		//$objResponse->assign("loginDiv","innerHTML",$loginForm);
		
		return $objResponse;
	
	}
	
	//判斷menu css
	public function checkPath(){
		//echo $this->_path_rows['path'];
		$data=array();
		//echo "/media/ /news /question/ /link/ /contact/ /map/ /photo/";
		if($this->_path_rows['path']=='/media/'){
			$data['isMedia']=true;
		}else if($this->_path_rows['path']=='/sound/'){
			$data['isSound']=true;
		}else if($this->_path_rows['path']=='/news/'){
			$data['isNews']=true;
		}else if($this->_path_rows['path']=='/question/'){
			$data['isQuestion']=true;
		}else if($this->_path_rows['path']=='/link/'){
			$data['isLink']=true;
		}else if($this->_path_rows['path']=='/contact/'){
			$data['isContact']=true;
		}else if($this->_path_rows['path']=='/map/'){
			$data['isMap']=true;
		}else if($this->_path_rows['path']=='/photo/'){
			$data['isPhoto']=true;
		}
		return $data;
	}
	
	//141024:ANN:片長表示(換算秒數)
	public function getFormatVideoLength($sec) {
		$s=$sec%60;
		$m=floor($sec/60)%60;
		$h=floor(floor($sec/60)/60);
		$str="";
		if($h!=0){
			$str.=$h.'時';
		}
		if(($m!=0) || $h!=0){
			$str.=$m.'分';
		}
		$str.=$s.'秒';
		//$h=
		//echo $h.'時'.$m.'分'.$s.'秒';
		return $str;
	}
	
	//141023:ANN:麵包屑去尾
	public function removeBreadcrumb($popNum=1){
		//去尾加現在標題
		$_bread=$this->_breadcrumb->trail();
		for($i=0;$i<$popNum;$i++){
			array_pop($_bread);
		}
		$this->_breadcrumb->reset();
		foreach($_bread as $value){
			$this->_breadcrumb->add($value['title'], $value['link']);
		}
	}
	

  }
?>