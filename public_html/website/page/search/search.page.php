<?php
  class Site_Page_Search extends Site_Page {
    const PAGE_NAME = 'search';
	const PATH_MAIN = '/search/';
	   
    public function __construct(Site_Path $path) {
		parent::__construct($path, self::PAGE_NAME);
		//require_once('/website/tmpl/ntl.config.inc.php');
		//ajax
		//global $ntl;
		//$ntl = new ntl();
		
		//ispc
		global $isPc;
		
		$ntl= $this->_ntl;
		//取得server位置
		if ($ntl->db_connect()===false)
			throw new ntl_Exception($ntl->fatal_error(NTL_ERROR_UNCONN));
			
    }
	
	//分類要怎麼套進來要再想想
	public function newsClass() {
      //分類
      if($this->get_dao('option')->get_array($optionArray, '', array('catalog' => 1))) {
		
		foreach($optionArray AS $key => $optionRows) {
			$optionRows['link'] = Site::href_link(self::PATH_MAIN, array('catalog' => $optionRows['id']));
			if(!empty($catalog)){
				if($optionRows['id']==$catalog){
					$optionRows['selected']=true;
				}
			}
			$newsClassArray[] = $optionRows;
		}
		
        return $newsClassArray;
      }
    } 
	
	
	
	// 預設頁面
    public function page_default() {
		//echo "this is test page";
		//print_r($this->_get);
		if( empty($this->_get['hasSearch']) && $this->_get['hasSearch']!=1 ){
			$this->page_search_form();
		}else{
			$this->page_check();
		}
		
	}
	
	public function page_check() {
		$chk = true;
		//欄位合理判斷
		if(!empty($this->_get['date_start'])){
			if($this->is_date($this->_get['date_start'])==false){
				$this->_alert->add_error('search', '上架日期設定錯誤');
				$chk=false;
			}
		}
		if(!empty($this->_get['date_end'])){
			if($this->is_date($this->_get['date_end'])==false){
				$this->_alert->add_error('search', '上架日期設定錯誤');
				$chk=false;
			}
		}
		if(!empty($this->_get['date_start']) && !empty($this->_get['date_end'])){
			if( strtotime($this->_get['date_start']) >= strtotime($this->_get['date_end'])){
				$this->_alert->add_error('search', '上架日期區間設定錯誤');
				$chk=false;
			}
		}
		//if (!empty($this->_get['keyword']) && !preg_match('/^[a-z0-9A-Z_\x{4e00}-\x{9fa5}]+$/u', $this->_get['keyword'])) { 
		if (!empty($this->_get['keyword']) && !preg_match('/^[\x{4e00}-\x{9fa5}-A-Za-z0-9_:.()\/]+$/u', $this->_get['keyword'])) { 
			$this->_alert->add_error('search', '關鍵字有誤，不允許特殊符號');
			$chk=false;
		}
		//if (!empty($this->_get['publisher']) && !preg_match('/^[a-z0-9A-Z_\x{4e00}-\x{9fa5}]+$/u', $this->_get['publisher'])) { 
		if (!empty($this->_get['publisher']) && !preg_match('/^[\x{4e00}-\x{9fa5}-A-Za-z0-9_:.()\/]+$/u', $this->_get['publisher'])) { 
			$this->_alert->add_error('search', '出版者有誤，不允許特殊符號');
			$chk=false;
		}
		if (!empty($this->_get['language']) && !preg_match('/^[a-z0-9A-z_\x{4e00}-\x{9fa5}]+$/u', $this->_get['language'])) { 
			$this->_alert->add_error('search', '語言有誤，不允許特殊符號');
			$chk=false;
		}
		if($chk == false){
			$this->_get['hasSearch']=0;
			$this->page_default();
		}else{
			$this->page_list();
		}
	
	}
	
	// 列表
	public function page_list() {
	
		//麵包屑
		$this->_breadcrumb->add('搜尋結果', $this->_path_rows['path'], array(Site::VAR_RECORD_ID => $recordRows[Site_DAO_Search::COLUMN_RECORD_ID]));
	
		//ajax
		$ntl = $this->_ntl;
		$this->_xajax_javascript = $ntl->render_xajax_javascript(array(array($this,'layoutSetuping'),array($this,'front_login'),array($this,'front_logout')), false);
		
		//判斷錯誤
		$catalog = !empty($this->_get['catalog'])&&is_numeric($this->_get['catalog'])?$this->_get['catalog']:0;
		//類型-->1影片,2聲音,3照片,預設為影片
		$type=!empty($this->_get['type'])?$this->_get['type']:1;//預設為影片
		if( $type!=1 && $type!=3 ){
			$this->page_notfound();
			die();
		};
		
		$timeNow = date('Y-m-d');
		
		//分類資料庫(linux大小寫要注意)
		$_catalog_type = array(
			1 => 'catalog_Video',
			2 => 'catalog_Sound',
			3 => 'catalog_Picture'
		);
		
		//排序判斷(預設欄位)
		$sortColumn=array('start_date','video_length','clickcount','headline');//上架日期、片長、點擊次數
		$sortHeadline=array('上架日期','片長','觀看次數','影片名稱');		
		if($type==3){
			$sortColumn=array('start_date');//上架日期
			$sortHeadline=array('上架日期');
		}
		
		//$sortType=$this->_get['sort']==null?'DESC':mysql_real_escape_string($this->_get['sort']);
		$sortType=!empty($this->_get['sort'])?mysql_real_escape_string($this->_get['sort']):'DESC';
		$sortType=(strtoupper($sortType)!='DESC'&&strtoupper($sortType)!='ASC')?'DESC':$sortType;
		$orderType=!empty($this->_get['od'])?mysql_real_escape_string($this->_get['od']):$sortColumn[0];
		$orderType=in_array($orderType,$sortColumn)?$orderType:$sortColumn[0];
		$sort = $orderType." ".$sortType;//組sort規則
		if($this->_get['od']=='headline'){
			$sort = "pd.".$orderType." ".$sortType;
		}
		
		//echo $sort;
		
		//組前台用排序
		$sortType2 = $sortType=='DESC'?'ASC':'DESC';
		$sortMark = $sortType=='DESC'?'▽':'△';
		$order_list="";
		foreach( $sortColumn as $key => $val ){
			//一樣是反的，其他是一樣
			//echo $val."/".$orderType."<br>";
			if($val==$orderType){
				//$order_list .= "<a href='' onclick=\"return fix_args(this, 'sort', '$sortType2')\">$sortHeadline[$key]</a>";
				$order_list .= "<a href='/media/' onclick=\"return fix_args(this, 'sort', '$sortType2')\"><button class='contentHeaderMenuDate' >$sortHeadline[$key] $sortMark</button></a>";
			}else{
				//$order_list .= "<a href='' onclick=\"return fix_args(this, 'od', '$sortColumn[$key]')\")>$sortHeadline[$key]</a>";
				$order_list .= "<a href='/media/' onclick=\"return fix_args(this, 'od', '$sortColumn[$key]')\")><button class='border contentHeaderMenuDate' >$sortHeadline[$key]</button></a>";
			}
			//$order_list .= $col['sortColumn']."<br>";
			//$order_list .= "key=".$key.",val=".$val;
		}
		//echo $order_list;
		
		//下方頁碼相關
		$rowsPerPage = is_numeric($this->_get['rowsPerPage']) ? $this->_get['rowsPerPage'] : 12;//每頁幾筆
		$offset = is_numeric($this->_get['offset']) ? $this->_get['offset'] : 0;//第幾開始
		
		//搜尋判斷//先關掉
		if($this->_get['keyword']){
			//140930:ANN:太奇怪的字不搜
			//$keyword = substr( strip_tags(addslashes(trim($this->_get['keyword']))),0,40);
			$keyword = strip_tags(addslashes(trim($this->_get['keyword'])));
			//$keyword = mysql_real_escape_string($this->_get['keyword']);
			//過濾關鍵字
			$this->get_dao('keyword_Filter')->get_array($keyword_filter,Site_DAO_Keyword_Filter::GROUP_LIST);
			for($i=0;$i<count($keyword_filter);$i++){
				$keyword_filter_list[]=$keyword_filter[$i]['headline'];
			}
			if( false === array_search($keyword,$keyword_filter_list) ){
				//接著判斷資料庫有=>update，沒有=>insert
				$this->get_dao('keyword')->get_rows($recordRows,Site_DAO_Keyword::GROUP_LIST,array("keyword"=>$keyword));
				
				if($recordRows!=null){
					// 更新
					$recordRows['search_count']=$recordRows['search_count']+1;
					$success = $this->get_dao('keyword')->update_rows($rMessage, $rReturn ,$recordRows,$recordRows['id'],Site_DAO_Keyword::GROUP_EDIT);
					//echo "success=".$success."<br>";
				}else{
					// insert keyword
					$recordRows['headline']=$keyword;
					$recordRows['search_count']=1;
					$success = $this->get_dao('keyword')->insert_rows($rMessage, $rReturn ,$recordRows,Site_DAO_Keyword::GROUP_ADD);
					//echo "success=".$success."<br>";
				}
			}else{
				//echo '找到了';
				//顯示alert不給搜...讓它回到搜尋頁
				$this->_alert->add_error('global_alert', '關鍵字有誤，請重新搜尋', true);
				Site::location(Site::href_link('/search/'));
			}
			$this->_get['keyword']=$keyword;
			//print_r($keyword_filter);
		}
		
		//變數集
		$params = array(
		  'dao_params' => array(
			'catalog' => $catalog,
			'keyword' => $keyword,
			'type' => $type,
			'stauts'=>1,
			'sort'=>$sort,
			'date' => $timeNow,
			'startIndex' => $offset,
			'endIndex' => $offset+$rowsPerPage,
		  ),
		  'get_params' => $this->_get,
		);
		
		//print_r($params);
		
		// layout cookie
		$layoutMode=$ntl->getcookie('cookie_ntl[layout][mode]');
		if(!$layoutMode)
			$layoutMode=2;
		
		if($layoutMode==1){
			$isLayoutMode1=true;
		}else if($layoutMode==2){
			$isLayoutMode2=true;
		}
		
		// 目錄選單
		$catalogArray = $this->get_dao('video')->choose_catalog($type);
		//print_r($catalogArray);
		
		//麵包屑
		$rootPath = '0';
		$catalogDAO = $this->get_dao($_catalog_type[$type]);
		
		//判斷分類
		//$catalog前面判斷
		$isCatalogPage = isset($catalog) && $catalogDAO->get_rows_byId($recordRows, '', $catalog);
		
		//在最後面加就好了...
		$fPath=0;
		$catalogParentArray =Array();
		
		//如果有目錄要抓目錄路徑
		$parents=$catalogDAO->tree_func_parent_all($catalog);
		//print_r($parents);
		 //根目錄
         //$currentfPath = $rootPath;
		 //echo (Site::href_link($this->_path_rows['path'],array('catalog'=>$this->_get['catalog'])));
		//$this->_breadcrumb->add($catalogParentRows[Site_DAO_Video::COLUMN_HEADLINE], Site::href_link($this->_path_rows['path'], array(Site::VAR_FPATH => $catalogParentRows[Site_DAO_Video::COLUMN_RECORD_ID])));
		foreach($parents as $row){
			for($i=0;$i<count($catalogArray);$i++){
				if($catalogArray[$i]['id']==$row){
					$this->_breadcrumb->add($catalogArray[$i]['headline'],Site::href_link($this->_path_rows['path'],array('catalog'=>$catalogArray[$i]['id'],'type'=>$type)));
				}
			}
		}
		//自己
		if(isset($catalog)){
			for($i=0;$i<count($catalogArray);$i++){
				if($catalogArray[$i]['id']==$this->_get['catalog']){
					$this->_breadcrumb->add($catalogArray[$i]['headline']);
				}
			}
		}

		
		//只撈一層
		if($catalogArray!=null){
			foreach( $catalogArray as $catalogRow ){
				if($catalogRow['parent_id']==0){
					$catalogMenu[$catalogRow['id']]=$catalogRow;
				}
				if($catalogRow['lv']==1){
					$pid = $catalogRow['parent_id'];
					if($catalogMenu[$pid]['child']==null)
						$catalogMenu[$pid]['child']=array();
					array_push($catalogMenu[$pid]['child'],$catalogRow);
				}
			}
			//要套鬍子的關係讓它從0開始
			$catalogMenuArray=array();
			foreach( $catalogMenu as $catMenuRow ){
				$catalogMenuaArray[]=$catMenuRow;
			}
			//print_r($catalogMenu);
		}
		//print_r($catalogMenuaArray);
		$tree_menu = CM_Template::get_tpl_html('treeMenu',array(
			'catalogMenu' => $catalogMenuaArray,
			'type' => $type
		));
		//$this->newsClass($catalogMenuaArray);
		//echo $tree_menu;
		
		$data['newsClass'] = $this->newsClass($catalogMenuaArray);
		if(!empty($data['newsClass'])){
			$data['hasClass']=true;
		}
	
	
		// 取得分類下資料夾(去掉根目錄)
		if($catalog!=null && $catalog!=0){
			$this->get_dao($_catalog_type[$type])->get_array($dir,Site_DAO_Catalog_Video::GROUP_DIR,array('parent_id' => $catalog));
		}
		//資料夾套版用
		$tempRows=0;
		while($tempRows<count($dir)){
			if($tempRows%4==0){
				//開始
				$dir[$tempRows]['startRows']=1;
			}else if(($tempRows+1)%4==0){
				//換行
				$dir[$tempRows]['endRows']=1;
			}else if($tempRows+1==count($dir)){
				//結束
				if(($tempRows+1)%4!=0){
					$dir[$tempRows]['endRows']=1;
				}
			}
			$tempRows++;
		}
		
		
		//取得列表
		//$this->get_dao('video')->get_rows($recordRows,Site_DAO_Video::GROUP_FRONT_LIST,array_merge($params['dao_params'],$params['get_params'],array("isCount"=>true)));
		$this->get_dao('search')->get_rows($recordRows,Site_DAO_Video::GROUP_FRONT_LIST,array_merge($params['get_params'],$params['dao_params'],array("isCount"=>true)));
		$total = $recordRows['total'];
		//$total = 100;
		//$this->get_dao('video')->get_array($recordRows,Site_DAO_Video::GROUP_FRONT_LIST,array_merge($params['dao_params'],$params['get_params']));
		$this->get_dao('search')->get_array($recordRows,Site_DAO_Video::GROUP_FRONT_LIST,array_merge($params['get_params'],$params['dao_params']));
		
		//日期換算
		$tempRows=0;
		while($tempRows<count($recordRows)){
			$recordRows[$tempRows]['start_date']= date('Y-m-d', strtotime($recordRows[$tempRows]['start_date']) );
			$recordRows[$tempRows]['clickcount']=$recordRows[$tempRows]['clickcount']==null?0:$recordRows[$tempRows]['clickcount'];
			$recordRows[$tempRows]['isVideo']=$recordRows[$tempRows]['type']==1?1:0;
			$recordRows[$tempRows]['video_length']=$this->getFormatVideoLength($recordRows[$tempRows]['video_length']);
			//141218:ANN:音檔縮圖判斷(取副檔名)
			$extFile=strrchr($recordRows[$tempRows]['file'],".");
			//echo $extFile."<br>";
			if($extFile=='.mp3' || $extFile=='.wma'){
				if($recordRows[$tempRows]['file_img']==null){
					//$recordRows[$tempRows]['file_img']='upload_thumbnail/music.jpg';
					$recordRows[$tempRows]['file_img']='upload_thumbnail/picture.jpg';
				}
			}
			if($tempRows%4==0){
				//開始
				$recordRows[$tempRows]['startRows']=1;
			}else if(($tempRows+1)%4==0){
				//換行
				$recordRows[$tempRows]['endRows']=1;
			}else if($tempRows==count($recordRows)){
				//結束
				if(($tempRows+1)%4!=0){
					$recordRows[$tempRows]['endRows']=1;
				}
			}
			$tempRows++;
		}
		//print_r($recordRows);

		//頁數判斷
		$prePage=array(12,24,36,72);
		$page_options = $this->setOptions($prePage,$this->_get['rowsPerPage']);		
		
		//下方頁碼
		//$ntl->assign('pagination', ($recordCount)?pagination($_SERVER['REQUEST_URI'], $recordCount, $rowsPerPage, $offset):'');
		$pagination=pagination($_SERVER['REQUEST_URI'], $total, $rowsPerPage, $offset);//總,每頁,第幾開始
		
		$file_list = CM_Template::get_tpl_html('layout'.$layoutMode,array('data' => $recordRows));
		//print_r($recordRows);	
		
		//套版
		echo $this->main_page(array(
			'bodyClass'=> 'pageSerachResult',
			'm_header' => $this->main_header(array('hasClass' => $data['hasClass'],'dataClass' => $data['newsClass'])),
			'm_main' => CM_Template::get_tpl_html('searchDefault', array(
			  'get'=>$params['get_params'],
			  //'xajax_javascript'=>$xajax_javascript,
			  'hasClass' => $data['hasClass'],
			  'dataClass' => $data['newsClass'],
			  //'dataClass'=> $this->newsClass($catalogMenuaArray),
              'data' => $recordRows,
              'dataList' => $listJSON->data,
			  'order_list'=>$order_list,
			  'layout'=>array('isLayoutMode1'=>$isLayoutMode1,'isLayoutMode2'=>$isLayoutMode2),
			  'hasDir'=>count($dir)>0?array('dir'=>$dir):0,
			  'file_list'=>$file_list,
			  'total'=>$total
			))
		));
		
	}
	
	//產生每頁筆數選單
	public function setOptions($arr,$selected){
		$str="";
		foreach($arr as $val){
			$str .="<option label=".$val." value=".$val;
			if($val==$selected)
				$str .=" selected=selected ";
			$str .=">".$val."</option>";
		}
		return $str;
	}
	
	
	//版面切換
	public function layoutSetuping($layoutMode)
	{
	
		if($layoutMode!=1 && $layoutMode!=2){
			$layoutMode=2;//預設
		}
		
		$ntl = $this->_ntl;
		
		$objResponse = new xajaxResponse();

		$ntl->setcookie('cookie_ntl[layout][mode]', $layoutMode);
		
		if ($form['url']=='')
			$objResponse->script('window.location.reload()');
			//$objResponse->script("alert(window.location.href + '?layoutMode='+$layoutMode)");
			//$objResponse->script("window.location.href =window.location.href + '?layoutMode='+$layoutMode");
		else
			$objResponse->redirect(htmlentities($form['url']));
		return $objResponse;
	
	}
	
	
	//檔名處理
	public function getFileName($fname){
		$strlen = strrpos($fname,'/')+1;
		$endlen = strrpos($fname,'.');
		$filename = substr($fname,$strlen,$endlen-$strlen);
		return $filename;
	}
	
	
	//搜尋表單
	public function getSearchForm(){
		return array(
			//'formTop' => CM_Html::form('searchForm', Site::href_link(self::PATH_MAIN), 'get', 'class="cm-form form-horizontal" role="form"'),
			'formTop' => CM_Html::form('searchForm', '../media/', 'get', 'class="cm-form form-horizontal" role="form"'),
			//'formSearch' => CM_Html::input_field('keyword', $this->_get['keyword']),
			'formSearch' => CM_Html::input_field('keyword'),
			'formButton' => CM_Html::submit_btn('<i class="icofont-search"></i>&nbsp;' .CM_Lang::line('Site.BTN_SEARCH'), array('class' => 'btn btn-primary')),
			'radioButton' => '&nbsp;'.CM_html::radio_field( 'type',  1,  true).'&nbsp;影片&nbsp;'.CM_html::radio_field( 'type',  3,  false). '&nbsp;相片&nbsp;',
			'formBottom' => '</form>'
		);
	}
	
	//搜尋表單
	/*public function getSearchForm_full(){
		return array(
			//'formTop' => CM_Html::form('searchForm', Site::href_link(self::PATH_MAIN), 'get', 'class="cm-form form-horizontal" role="form"'),
			'formTop' => CM_Html::form('searchForm', '../media/', 'get', 'class="cm-form form-horizontal" role="form"'),
			'formSearch' => '<p>類型：'.CM_html::radio_field( 'type',  1,  true).'&nbsp;影片&nbsp;'.CM_html::radio_field( 'type',  3,  false). '&nbsp;相片</p>'.
			'<p>關鍵字：'.CM_Html::input_field('keyword', $this->_get['keyword']).'</p>'.
			'<p上架日期：'.CM_Html::input_field('date', $this->_get['date']).'</p>'.
			'<p出版、製作日期：'.CM_Html::input_field('keyword', $this->_get['keyword']).'</p>'.
			'<p出版、經銷者：'.CM_Html::input_field('keyword', $this->_get['keyword']).'</p>',
			'formButton' => CM_Html::submit_btn('<i class="icofont-search"></i>&nbsp;' .CM_Lang::line('Site.BTN_SEARCH'), array('class' => 'btn btn-primary')),
			'formBottom' => '</form>'
		);
	}*/
	
	//進階搜尋(表單)
	public function page_search_form() {
	
		//ajax
		$ntl = $this->_ntl;
		$this->_xajax_javascript = $ntl->render_xajax_javascript(array(array($this,'front_login'),array($this,'front_logout')), false);
		
		//套版
		echo $this->main_page(array(
			'm_main' => CM_Template::get_tpl_html('searchForm', array(
			  'formAlert' => $this->_alert->output('search'),
			  //'get'=>$params['get_params'],
			  //'searchForm' => $this->getSearchForm_full(),
			  //'videoCatalog' => $catalogMenuaArray
			))
		));
	}
	
	function is_date($time)
	{
		$pattern = "/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/";
	
		return preg_match($pattern, $time);
	}
	
  }
?>