<?php
  require(__DIR__ . '/init.php');

  if(!Admin::http_request($rMessage, $rResponse, "http://events.rti.org.tw/openapi/realnewsxmlCCDN.aspx")) die($rMessage);
  
  $rResponse = preg_replace('/[\x00-\x08\x0b-\x0c\x0e-\x1f\x7f]/', '', $rResponse);
  $xml = simplexml_load_string($rResponse);

  $itemArray = array();
  $dao = Admin_Factory::createCMDAO('news');
  $mDAO = Admin_Factory::createCMDAO('material');
  $dateNow = date('Y-m-d');
  $maxId = CM_DB::get_sql_rows($idRows, sprintf("SELECT MAX(`record_id`) AS `id` FROM `news` WHERE `date` BETWEEN '%s 00:00:00' AND '%s 23:59:59'", $dateNow, $dateNow)) ? $idRows['id'] : 450000;
  $maxId -= 50;
  $repeatCheck=array();
  
  foreach($xml->channel->item as $itemRows) {
  
    $rows = array();

    foreach($itemRows as $key => $value) $rows[$key] = (string)$value;

    $rows['sound'] = '';

    $insertString = '';

    if($rows['guid'] < $maxId) continue;

    if(isset($rows['sound_file']) && !empty($rows['sound_file'])) {
      $soundFile = 'realnews/' . $rows['sound_file'] . '.mp4';
      $insertString .= "編彩譯編號: " . $rows['guid'] . "\n";
      $insertString .= "找到音檔: " . $soundFile . "\n";

      if(CM_DB::get_sql_rows($mRows, "SELECT * FROM `material` WHERE `file` = '{$soundFile}'")) {
        $rows['sound'] = $mRows['id'];
        $insertString .= "音檔來自資料庫: " . $mRows['id'] . "\n";
      } else {
        $today = date('Y-m-d H:i:s');
        $insertId = 0;
        CM_DB::insert($insertId, 'material', array(
          Admin_DAO_Material::COLUMN_HEADLINE => $rows['title'],
          'type' => '2',
          'lang' => '1',
          'catalog' => '1',
          'file' => $soundFile,
          Admin_DAO_Material::COLUMN_STATUS => '1',
          'expire_date' => '2099-12-31',
          Admin_DAO_Material::COLUMN_CREATE_DATE => $today,
          Admin_DAO_Material::COLUMN_MODIFY_DATE => $today
        ));
        $rows['sound'] = $insertId;
        $insertString .= "音檔新建: " . $insertId . "\n";
      }
    }

    $timestamp = strtotime($rows['pubDate']);
    $dateTime = date('Y-m-d H:i:s', $timestamp);

	if(!in_array($rows['guid'],$repeatCheck)){//目前抓到的資料先查看這輪資料是否ID重複過//Neko20150424
	$repeatCheck[] = $rows['guid'];
		$data = array(
		  Admin_DAO_News::COLUMN_HEADLINE => $rows['title'],
		  'record_id' => $rows['guid'],
		  'desc' => nl2br($rows['description']),
		  'catalog' => $rows['class'],
		  'attr' => $rows['attribute'],
		  'author' => $rows['author'],
		  'source' => $rows['source'],
		  'editor' => '0',
		  'date' => $dateTime,
		  'img' => '',
		  'video' => '',
		  'sound' => $rows['sound'],
		  'sort' => '0',
		  'url' => '',
		  'important' => '0',
		  'important_tag' => $rows['important'] == 'True' ? '1' : '0',
		  Admin_DAO_News::COLUMN_STATUS => '1',
		  'lang_status' => '1'
		  //'create_date' => $time,
		  //'modify_date' => $item
		);
		//$total = CM_DB::get_sql_rows($recordRows, "SELECT * FROM `news` WHERE `record_id` = '{$rows['guid']}'") ? count($recordRows) : 0;
		$total = CM_DB::get_sql_rows($recordRows, "SELECT count(*) FROM `news` WHERE `record_id` = '{$rows['guid']}'") ? ($recordRows['count(*)']) : 0;

		if($total <= 0) {
		  echo $insertString;
		  echo '插入資料 編采譯邊號 = ' . $rows['guid'];
		  if(!$dao->insert_rows($rMessage, $data, Admin_DAO_News::GROUP_ADD)) $errorMsg[] = $rows['guid'] . ' - ' . $rMessage;
		}
		else if($total == 2){
			echo 'DB資料重複';
		}
	}
	else {
	echo '新聞已重複';
	}
    /*
    if(!$dao->get_rows($recordRows, Admin_DAO_News::GROUP_LIST, array('nocache' => true, 'rId' => $rows['guid']))) {
      if(!$dao->insert_rows($rMessage, $data, Admin_DAO_News::GROUP_ADD)) $errorMsg[] = $rows['guid'] . ' - ' . $rMessage;
    } else {
      if(!$dao->update_rows($rMessage, $rReturn, $data, $recordRows[Admin_DAO_News::COLUMN_RECORD_ID], Admin_DAO_News::GROUP_EDIT)) $errorMsg[] = $rMessage;
    }
    */
  }

  if(!empty($errorMsg)) {
    echo implode('<br />' . "\n", $errorMsg);
  } else {
    echo "加入新聞完成";
  }

  echo '處理時間 - ' . date('Y-m-d H:i:s') . "\n\n";
?>