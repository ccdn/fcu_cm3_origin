<?php
include('crontab/adodb/adodb.inc.php');
//include('adodb/adodb.inc.php');//正式機

try {

	date_default_timezone_set('Asia/Taipei');

	//取得server位置
	$db = NewADOConnection('mysql');
	$db->Connect("localhost", "root", "ccdntech", "ccdn2");	
			
	if(!$db->IsConnected()){
		throw new exception("無法建立資料庫連線");

	} 
	$db->EXECUTE("set names 'utf8'"); 
	$tags_log = array();	
	//抓關鍵字
	$sql = "SELECT headline_sub_2 as keyword FROM video_lang vl LEFT JOIN video v ON vl.id=v.id WHERE v.status=1";
	$rs = $db->Execute($sql);

	if (!$rs)
	  throw new exception($sql.",".$db->ErrorMsg().",".$db->ErrorNo()); 			
	while (!$rs->EOF) {
		//$tags_log[] = $rs->fields;
		if($rs->fields['keyword']!=''){
			array_push($tags_log, $rs->fields['keyword']);
		}
		$rs->MoveNext();
	}

	//print_r($tags_log);

	if(count($tags_log)>0){
		//將關鍵字按照,隔開後統計次數
		for($i=0;$i<count($tags_log);$i++){
			$arr=explode(",",$tags_log[$i]);
			for($j=0;$j<count($arr);$j++){
				$keyword=$arr[$j];
				if(empty($count[$keyword])){
					$count[$keyword]=0;
				}
				$count[$keyword]=$count[$keyword]+1;
			}
		}
		//如果有結果再寫入資料表
		if($count>0){
			print_r($count);
			//清空資料表
			$turncate_sql = "TRUNCATE TABLE tagscloud";
			$rs_turncate = $db->Execute($turncate_sql);
			//寫入資料表
			foreach ($count as $key => $value) {
				//echo "Key: $key; Value: $value<br />\n";
				$temp_sql_i = "INSERT INTO tagscloud (`keyword`, `count`) VALUES ('$key',$value)";
				$rs_i = $db->Execute($temp_sql_i);
				if (!$rs_i)
					throw new exception($temp_sql_i.",".$db->ErrorMsg().",".$db->ErrorNo());
			}
		}
	}

	//結束
	$rs = $db->Close();	

} catch (Exception $e) {

  echo  $e->getFile().','.$e->getLine().',crontab:'.$e->getMessage().','.$e->getCode();

	
}

?>