<?php
  $rootPath = dirname(__DIR__);
  //設定值Class載入
  require($rootPath . '/lib/class/CM/Conf.php');
  //設定值載入
  require($rootPath . '/lib/inc/conf.php');
  //初始化載入
  require(CM_Conf::get('PATH_F.LIB') . 'inc/init.php');
  //呼叫控制器
  require(CM_Conf::get('PATH_F.ROOT') . 'webmgr/inc/web_init.php');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <meta charset="utf-8" />
    <base href="<?php echo CM_Conf::get('HTTP.SERVER'); ?>" />
    <title><?php echo CM_Lang::line('Admin.HEADER_TITLE'); ?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta name="description" content="" />
    <meta name="author" content="dgFactor" />
<?php
  foreach(array(
    'bootstrap.css',
    'bootstrap-responsive.css',
    'responsive-tables.css',
    'DT_bootstrap.css',
    'select2.css',
    'datepicker.css',
    'datetimepicker.css',
    'stilearn.css',
    'stilearn-responsive.css',
    'stilearn-helper.css',
    'stilearn-icon.css',
    'font-awesome.css',
    'elusive-webfont.css',
    'animate.css',
    'uniform.default.css',
    'jquery.pnotify.default.css',
    'jquery.fileupload-ui.css',
    'zTree.css',
    'jquery.jqplot.css',
    'system.css'
  ) as $styles) echo sprintf('<link rel="stylesheet" type="text/css" href="%s%s" />', CM_Conf::get('PATH_W.ADMIN_CSS'), $styles);
?>
    <!--[if lt IE 9]>
    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
  </head>
  <body>
    <div id="mainBody" rel="main"></div>
    <script src="<?php echo CM_Conf::get('PATH_W.ADMIN_JS'); ?>jquery.js"></script>
    <script src="<?php echo CM_Conf::get('PATH_W.ADMIN_JS'); ?>jquery-migrate.js"></script>
    <script src="<?php echo CM_Conf::get('PATH_W.ADMIN_JS'); ?>jquery-ui.min.js"></script>
    <script src="<?php echo CM_Conf::get('PATH_W.ADMIN_JS'); ?>tinymce4/tinymce.min.js"></script>
    <script src="<?php echo CM_Conf::get('PATH_W.ADMIN_JS'); ?>tinymce4/jquery.tinymce.min.js"></script>
    <script src="<?php echo CM_Conf::get('PATH_W.ADMIN_JS'); ?>jqplot/jquery.jqplot.min.js"></script>
    <script src="<?php echo CM_Conf::get('PATH_W.ADMIN_JS'); ?>jqplot/jqplot.barRenderer.min.js"></script>
    <script src="<?php echo CM_Conf::get('PATH_W.ADMIN_JS'); ?>jqplot/jqplot.categoryAxisRenderer.min.js"></script>
    <script src="<?php echo CM_Conf::get('PATH_W.ADMIN_JS'); ?>jqplot/jqplot.highlighter.min.js"></script>
    <!--[if lt IE 9]><script src="<?php echo CM_Conf::get('PATH_W.ADMIN_JS'); ?>jqplot/excanvas.min.js"></script><![endif]-->  
    <!--[if gte IE 8]><script src="<?php echo CM_Conf::get('PATH_W.ADMIN_JS'); ?>jquery.xdr-transport.js"></script><![endif]-->
    <script src="<?php echo CM_Conf::get('PATH_W.ADMIN_JS'); ?>cm.api.php"></script>
<?php
  foreach(Admin::load_package_javascript(CM_Conf::get('PATH_F.ADMIN_JS') . 'jquery.api/', CM_Conf::get('PATH_W.ADMIN_JS') . 'jquery.api/') as $path) echo sprintf('<script type="text/javascript" language="javascript" src="%s"></script>', $path);
?>
  </body>
</html>