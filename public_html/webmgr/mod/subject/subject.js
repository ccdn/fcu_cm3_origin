;(function($, cm, cmApi, cmLang) {
  var mMain = $(cm.setting.CONTAINER),
      mModName = 'subject',
      mSelectInit = function(jqMain) {
        $('[data-cmfunc="select-contents"]', jqMain).cmSelectContents({
          lang: mLang,
          create_load: function(api, callback) {
            if(api.element.val() != '') {
              cmApi.call_server_api({
                history: false,
                nolock: true,
                url: cmApi.request_to_url({
                  module: mModName,
                  request: 'video_list'
                }),
                caller: api.element,
                param: {videoId: api.element.val()},
                success: function(data) { callback(data.video); }
              });
            }
          },
          func_select_contents: function(api) {
            cmApi.call_server_api({
              history: false,
              nolock: true,
              url: cmApi.request_to_url({
                module: 'video',
                request: 'pop_list'
              }),
              caller: api.element
            });
          }
        });
      };
  
  if(typeof cm.mod[mModName] == 'object') {
    var mLang = cm.mod[mModName][cm.setting.VAR_LANGUAGES],
        mCallback = cm.mod[mModName][cm.setting.VAR_CALLBACK] = {};
    //加入目錄callback
    for(var k in cm.tmp.cb_catalog) mCallback[k] = cm.tmp.cb_catalog[k];
    //加入模組預設的callback
    for(var k in cm.tmp.cb_mod) mCallback[k] = cm.tmp.cb_mod[k];
    //設定預設的callbcak
    mCallback.default_onLoad = function(data) {
      mCallback.list_onLoad(data);
    };
    mCallback.add_onLoad = function(data) {
      cm.tmp.cb_mod.add_onLoad.call(this, data);
      mSelectInit($('#columnCenter', mMain));
    };
    mCallback.edit_onLoad = function(data) {
      cm.tmp.cb_mod.edit_onLoad.call(this, data);
      mSelectInit($('#columnCenter', mMain));
    };
    mCallback.video_carousel_onLoad = function(data) {
      mCallback.edit_onLoad.call(this, data);
    };
    mCallback.video_carousel_save_onLoad = function(data) {
      cmApi.alert_message(data.message, 'success');
    }
  } else {
    cmApi.alert_message(cmLang.ERROR_MODULE_NOT_FOUND);
  }
})(jQuery, window._cm, window._cm.func, window._cm.lang);