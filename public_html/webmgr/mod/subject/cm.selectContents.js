;(function($, cm, cmApi, cmLang) {
  $.widget('dgfactor.cmSelectContents', {
    options: {
      lang: {
        ELEMENT_ERROR: '選擇檔案的元件必須要是Input'
      },
      create_load: function(api, callback) {
        var data = {};
        callback(data);
      },
      func_select_contents: function(api) {

      }
    },
    _create: function() {
      var that = this;

      if(that.element.is('input') === true) {
        that.element.attr('type', 'hidden');
        that.containerElement = $(cmApi.templates_add('subject_selectContents', {
          id: that.element.attr('id') + '_selectContents',
          lang: that.options.lang
        }))
          .insertAfter(that.element)
          .addClass('cm-select-contents')
          .on('click.cmSelectContents', '[data-cmfield-func="select-contents"]', function(e) {
            that.options.func_select_contents.call(this, that);
          });
        that.listElement = $('[data-cmfield-func="item-list"]', this.containerElement);
        that.listElement
          .on('click.cmSelectContents', '[data-cmfield-func="item-remove"]', function(e) {
            var jqItem = $(this).parents('[data-cmfield-func="content-item"]');
            cmApi.confirm_message($(this).data('cmfieldConfirm'), function() {
              jqItem.fadeOut('fast', function() { 
                $(this).remove();
                that.refresh_item();
              });
            });
          })
          .sortable({
            cursor: 'move',
            update: function(event, ui) { that.refresh_item(); }
          });
        that.options.create_load(that, function(data) { that.add_item(data); });
      } else {
        cmApi.alert_message(that.options.ELEMENT_ERROR);
      }
    },
    add_item: function(data) {
      if($.isArray(data) && data.length > 0) {
        var that = this,
            html = '';

        for(var k in data) {
          if($('[data-cmfield-data-id="' + data[k].id + '"]', that.listElement).length > 0) continue;

          html += cmApi.templates_add('subject_selectItems', data[k]); 
        }

        $(html).prependTo(that.listElement);
        that.refresh_item();
      }
    },
    refresh_item: function() {
      var that = this,
          selectedId = [],
          jqItemList = $('[data-cmfield-func="item-list"]', that.containerElement);

      $('[data-cmfield-data-id]', jqItemList).each(function() {
        selectedId.push(parseInt($(this).data('cmfieldDataId'), 10));
      });
      
      if(selectedId.length > 0) {
        that.element.val(selectedId.join(','));
      } else {
        that.element.val('');
      }
    },
    destroy: function() {
      this.element
        .removeClass('cm-select-contents')
        .off('.cmSelectContents')
        .empty();
      $.Widget.prototype.destroy.call(this);
    }
  });
})(jQuery, window._cm, window._cm.func, window._cm.lang);