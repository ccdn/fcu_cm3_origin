;(function($, cm, cmApi, cmLang) {
  var mMain = $(cm.setting.CONTAINER),
      mModName = 'core';
  
  if(typeof cm.mod[mModName] == 'object') {
    var mLang = cm.mod[mModName][cm.setting.VAR_LANGUAGES],
        mCallback = cm.mod[mModName][cm.setting.VAR_CALLBACK] = {};
    mCallback.cm_forget_password_onLoad = function(data) {
      if($('#' + data.tplData.mId).length > 0) $('#' + data.tplData.mId).remove();
      
      data.tplData['mBody'] = cmApi.templates_add('form_fields', data.tplData.mData);
      var jqModal = $(cmApi.templates_add('modal', data.tplData))
        .prependTo(mMain)
        .modal({
          keyboard: false,
          show : true
        });
      $('form', jqModal).cmForm();
    };
    mCallback.cm_forget_password_send_mail_onLoad = function(data) {
      var formId = 'passwordForgetForm';
      cmApi.alert_message(data.message, 'success');
      $('#' + formId).modal('hide');
    };
    mCallback.cm_forget_password_change_password_onLoad = function(data) {
      cmApi.alert_message(data.message, 'success');
      cmApi.call_server_api({
        url : 'index.php'
      });
    };
    mCallback.cm_login_onLoad = function(data) {
      mMain.html(cmApi.templates_add('login', data.tplData));
      $('form', mMain).cmForm().fadeIn();
    };
    mCallback.cm_login_authorized_onLoad = function(data) {
      cmApi.call_server_api({
        url: cmApi.request_to_url()
      });
      delete cm.tmp.login_cache;
    };
    mCallback.cm_logoff_onLoad = function(data) {
      cm.tmp.main_loading = false;
      cmApi.alert_message(data.message, 'success');
      cmApi.call_server_api({
        url : cmApi.request_to_url({request: 'cm_login'})
      });
    };
    mCallback.account_info_onLoad = function(data) {
      var jqMain = $('#columnCenter', mMain);
      cmApi.templates_create('column_center_form', data.tplData, jqMain);
      $('form', jqMain).cmForm({editlock: true});
    };
    mCallback.account_info_save_onLoad = function(data) {
      cmApi.alert_message(data.message, 'success');
    };
    mCallback.account_password_onLoad = function(data) {
      if($('#' + data.tplData.mId).length > 0) $('#' + data.tplData.mId).remove();
      
      data.tplData.mBody = cmApi.templates_add('modal_form', data.tplData.mData);
      var jqForm = $(cmApi.templates_add('modal', data.tplData)).prependTo('#mainBody');
      $('form', jqForm).cmForm();
      jqForm.modal({
        keyboard: false,
        show : true
      }).on('hidden', function() {
        jqForm.remove();
      });
    };
    mCallback.account_password_save_onLoad = function(data) {
      var jqForm = $(this).parents('div.modal');
      jqForm.modal('hide');
      cmApi.alert_message(data.message, 'success');
    };
    mCallback.account_password_reset_onLoad = function(data) {
      if($('#' + data.tplData.mId).length > 0) $('#' + data.tplData.mId).remove();
      
      data.tplData.mBody = cmApi.templates_add('modal_form', data.tplData.mData);
      var jqForm = $(cmApi.templates_add('modal', data.tplData)).prependTo('#mainBody');
      $('form', jqForm).cmForm();
      jqForm.modal({
        keyboard: false,
        show : true
      }).on('hidden', function() {
        jqForm.remove();
      });
    };
    mCallback.account_password_reset_save_onLoad = function(data) {
      var jqForm = $(this).parents('div.modal');
      jqForm.modal('hide');
      cmApi.alert_message(data.message, 'success');
    };
    mCallback.account_add_onLoad = function(data) {
      var jqMain = $('#columnCenter', mMain);
      cmApi.templates_create('column_center_form', data.tplData, jqMain);
      $('form', jqMain).cmForm({editlock: true});
    };
    mCallback.account_add_save_onLoad = function(data) {
      cmApi.alert_message(data.message, 'success');
      cmApi.call_server_api({url : decodeURIComponent(data.url)});
    };
    mCallback.account_edit_onLoad = function(data) {
      var jqMain = $('#columnCenter', mMain);
      cmApi.templates_create('column_center_form', data.tplData, jqMain);
      $('form', jqMain).cmForm({editlock: true});
    };
    mCallback.account_edit_save_onLoad = function(data) {
      cmApi.alert_message(data.message, 'success');
      cmApi.call_server_api({url : decodeURIComponent(data.url)});
    };
    mCallback.account_edit_list_status_onLoad = function(data) {
      cmApi.alert_message(data.message, 'success');
      cmApi.call_server_api({url : location.href});
    };
    mCallback.account_edit_list_group_onLoad = function(data) {
      mCallback.account_edit_list_status_onLoad.call(this, data);
    };
    mCallback.account_remove_onLoad = function(data) {
      cmApi.alert_message(data.message, 'success');
      cmApi.call_server_api({url : data.url});
    };
    mCallback.account_remove_list_onLoad = function(data) {
      cmApi.alert_message(data.message, 'success');
      cmApi.call_server_api({url : location.href});
    };
    mCallback.account_group_add_onLoad = function(data) {
      var jqMain = $('#columnCenter', mMain);
      cmApi.templates_create('column_center_form', data.tplData, jqMain);
      $('form', jqMain).cmForm({editlock: true});
    };
    mCallback.account_group_add_save_onLoad = function(data) {
      cmApi.alert_message(data.message, 'success');
      cmApi.call_server_api({url : decodeURIComponent(data.url)});
    };
    mCallback.account_group_edit_onLoad = function(data) {
      var jqMain = $('#columnCenter', mMain);
      cmApi.templates_create('column_center_form', data.tplData, jqMain);
      $('form', jqMain).cmForm({editlock: true});
    };
    mCallback.account_group_edit_save_onLoad = function(data) {
      cmApi.alert_message(data.message, 'success');
      cmApi.call_server_api({url : decodeURIComponent(data.url)});
    };
    mCallback.account_group_remove_onLoad = function(data) {
      cmApi.alert_message(data.message, 'success');
      cmApi.call_server_api({url : data.url});
    };
    mCallback.account_group_remove_list_onLoad = function(data) {
      cmApi.alert_message(data.message, 'success');
      cmApi.call_server_api({url : location.href});
    };
    mCallback.account_group_list_onLoad = function(data) {
      var jqMain = $('#columnCenter', mMain);
      $('.boxList', cmApi.templates_create('column_center_listSingle', data.tplData, jqMain)).cmAdminList();
    };
    mCallback.account_list_onLoad = function(data) {
      var jqMain = $('#columnCenter', mMain);
      $('.boxList', cmApi.templates_create('column_center_listSingle', data.tplData, jqMain)).cmAdminList();
    };
    mCallback.upload_files_list_onLoad = function(data) {
      var jqMain = $('#columnCenter', mMain);
      $('.boxList', cmApi.templates_create('column_center_listSingle', data.tplData, jqMain)).cmAdminList();
    };
    mCallback.upload_files_edit_onLoad = function(data) {
      if($('#' + data.tplData.mId).length > 0) $('#' + data.tplData.mId).remove();
      
      var jqModal = $(cmApi.templates_add('modal', data.tplData))
        .prependTo(mMain)
        .modal({
          keyboard: false,
          show : true
        })
        .data('caller', this);
      $('form', jqModal).cmForm();
    };
    mCallback.upload_files_edit_save_onLoad = function(data) {
      var jqModal = $(this).parents('div.modal'),
          jqItem = $(jqModal.data('caller')).parents('[data-cmfield-func="file-item"]');
      $('.media-heading', jqItem).html(data['rows']['name']);
      $('.media-desc', jqItem).html(data['rows']['desc']);
      jqModal.modal('hide');
      cmApi.alert_message(data.message, 'success');
    };
    mCallback.upload_files_preview_onLoad = function(data) {
      if($('#' + data.tplData.mId).length > 0) $('#' + data.tplData.mId).remove();
      
      var jqModal = $(cmApi.templates_add('modal', data.tplData))
        .prependTo(mMain)
        .modal({
          keyboard: false,
          show : true
        })
        .data('caller', this);
      $('form', jqModal).cmForm();
      
      if($('input[name="file"]', jqModal).length > 0) {
        var streamServer = $('input[name="streamServer"]', jqModal).val();
        fplayer = flowplayer('player', {    src:'http://releases.flowplayer.org/swf/flowplayer.commercial-3.2.16.swf',    wmode: 'opaque'     }, {        
          key:'#$aa6bc10e8f64995429a',
		  clip:  {scaling: 'fit',autoPlay: false,autoBuffering: true,
            onBeforeFinish: function (clip) {
              return clip.index < this.getPlaylist().length - 1;
            },
            provider: 'rtmp',
          },
          playlist: [
            {url:'mp4:' + $('input[name="file"]', jqModal).val()}
          ],
          canvas: {backgroundColor: '#000000',backgroundGradient: 'none',borderRadius: 10}, 
          plugins: {
            rtmp: {url: "flowplayer.rtmp-3.2.12.swf",netConnectionUrl: 'rtmp://' + streamServer + '/vod'},
          }  
        });
      }
    };
    mCallback.upload_files_remove_list_onLoad = function(data) {
      cmApi.alert_message(data.message, 'success');
      cmApi.call_server_api({url : location.href});
    };
    mCallback.configure_onLoad = function(data) {
      var jqMain = $('#columnCenter', mMain);
      cmApi.templates_create('column_center_form', data.tplData, jqMain);
      var jqForm = $('form', jqMain);
      jqForm.cmForm({editlock: true});
      $('input[type="radio"][name="sitemode_type"]', jqForm).on('change', function() {
        var columnShow = {
              normal: [],
              auth: ['sitemode_auth_text', 'sitemode_auth_account', 'sitemode_auth_password'],
              off: ['sitemode_off_text']
            },
            columnHide = {
              normal: ['sitemode_auth_text', 'sitemode_auth_account', 'sitemode_auth_password', 'sitemode_off_text'],
              auth: ['sitemode_off_text'],
              off: ['sitemode_auth_text', 'sitemode_auth_account', 'sitemode_auth_password']
            },
            val = $(this).val();

        for(var k in columnShow[val]) $('[name="' + columnShow[val][k] + '"]', jqForm).parents('div.control-group').fadeIn();

        for(var k in columnHide[val]) $('[name="' + columnHide[val][k] + '"]', jqForm).parents('div.control-group').fadeOut();
      });
      $('input[type="radio"][name="sitemode_type"]:checked', jqForm).trigger('change');
    };
    mCallback.configure_save_onLoad = function(data) {
      cmApi.alert_message(data.message, 'success');
      cmApi.call_server_api({url : location.href});
    };
    mCallback.permission_onLoad = function(data) {
      var jqMain = $('#columnCenter', mMain);
      cmApi.templates_create('column_center_permission', data.tplData, jqMain);
    };
    mCallback.permission_account_onLoad = function(data) {
      var jqMain = $('#columnCenter', mMain);
      cmApi.templates_create('column_center_permission', data.tplData, jqMain);
      $('form', jqMain)
        .on('click', 'div.permission-all input[type="checkbox"]', function(e) {
          jqParent = $(this).parents('div.permission-entry'),
          jqSub = $('table.permission-sub', jqParent);

          if($(this).prop('checked') == true) {
            $('input[type="checkbox"]', jqSub).prop('checked', true);
          } else {
            $('input[type="checkbox"]', jqSub).prop('checked', false);
          }
        })
        .on('click', 'table.permission-sub input[type="checkbox"]', function(e) {
          jqParent = $(this).parents('div.permission-entry'),
          jqSub = $('table.permission-sub', jqParent);

          if($(this).prop('checked') == true) {
            if($('input[type="checkbox"]', jqSub).length == $('input[type="checkbox"]:checked', jqSub).length) $('div.permission-all input[type="checkbox"]', jqParent).prop('checked', true);
          } else {
            $('div.permission-all input[type="checkbox"]', jqParent).prop('checked', false);
          }
        });
    };
    mCallback.permission_account_save_onLoad = function(data) {
      cmApi.alert_message(data.message, 'success');
      cmApi.call_server_api({url : decodeURIComponent(data.url)});
    };
    mCallback.permission_account_group_onLoad = function(data) {
      mCallback.permission_account_onLoad.call(this, data);
    };
    mCallback.permission_account_group_save_onLoad = function(data) {
      mCallback.permission_account_save_onLoad.call(this, data);
    };
    mCallback.log_onLoad = function(data) {
      cm.tmp.cb_mod.list_onLoad.call(this, data);
    };
    mCallback.log_detail_onLoad = function(data) {
      if($('#' + data.tplData.mId).length > 0) $('#' + data.tplData.mId).remove();
      
      data.tplData['mBody'] = cmApi.templates_add('modal_form', data.tplData.mData);
      var jqModal = $(cmApi.templates_add('modal', data.tplData))
        .prependTo(mMain)
        .modal({
          keyboard: false,
          show : true
        });
    };
    mCallback.log_remove_onLoad = function(data) {
      cm.tmp.cb_mod.edit_list_status_onLoad.call(this, data);
    };
    mCallback.log_remove_search_onLoad = function(data) {
      if($('#' + data.tplData.mId).length > 0) $('#' + data.tplData.mId).remove();
      
      var jqForm = $(cmApi.templates_add('modal', data.tplData)).prependTo('#mainBody');
      $('form', jqForm).cmForm();
      jqForm.modal({
        keyboard: false,
        show : true
      }).on('hidden', function() {
        jqForm.remove();
      });
    };
    mCallback.log_remove_search_process_onLoad = function(data) {
      var jqForm = $(this).parents('div.modal');
      jqForm.modal('hide');
      cmApi.alert_message(data.message, 'success');
      cmApi.call_server_api({url : location.href});
    };
    mCallback.file_onLoad = function(data) {
      cm.tmp.cb_mod.list_onLoad.call(this, data);
    };
    mCallback.file_add_onLoad = function(data) {
      cm.tmp.cb_mod.add_onLoad.call(this, data);
    };
    mCallback.file_add_save_onLoad = function(data) {
      cm.tmp.cb_mod.add_save_onLoad.call(this, data);
    };
    mCallback.file_edit_onLoad = function(data) {
      cm.tmp.cb_mod.edit_onLoad.call(this, data);
    };
    mCallback.file_edit_save_onLoad = function(data) {
      cm.tmp.cb_mod.edit_save_onLoad.call(this, data);
    };
    mCallback.file_remove_onLoad = function(data) {
      cm.tmp.cb_mod.remove_onLoad.call(this, data);
    };
    mCallback.file_remove_list_onLoad = function(data) {
      cm.tmp.cb_mod.remove_list_onLoad.call(this, data);
    };
    mCallback.pop_file_onLoad = function(data) {
      if($(this).parents('div.modal').length > 0) {
        var jqModal = $(this).parents('div.modal');
      } else {
        var jqModal = cmApi.create_autoresize_modal({
              mTmpl: {mHeader: cmLang.EDITOR_FILE_HEADLINE}
            }),
            that = this;
        jqModal
          .on('click', '[data-cmfunc="import-file"]', function() {
            var jqParent = $(this).parents('tr');

            if($('[data-cmdata]', jqParent).length > 0) {
              var json = $('[data-cmdata]', jqParent).data('cmdata');
              //檔案更新
              that.val(json.url);
              //描述更新
              $('input.mce-textbox', that.parents('div.mce-container').next('div.mce-container')).val(json.headline);
            }

            $(this).parents('div.modal').modal('hide');
          })
          .modal('show');
      }

      var jqMain = $('div.modal-body', jqModal);
      $('.boxList', cmApi.templates_create('column_center_listSingle', data.tplData, jqMain)).cmAdminList();

      $('form', jqMain).cmForm();
    };
    mCallback.pop_file_add_onLoad = function(data) {
      cm.tmp.cb_mod.pop_add_onLoad.call(this, data);
    };
    mCallback.pop_file_add_save_onLoad = function(data) {
      cm.tmp.cb_mod.pop_add_save_onLoad.call(this, data);
    };
    mCallback.pop_file_edit_onLoad = function(data) {
      cm.tmp.cb_mod.pop_edit_onLoad.call(this, data);
    };
    mCallback.pop_file_edit_save_onLoad = function(data) {
      cm.tmp.cb_mod.pop_edit_save_onLoad.call(this, data);
    };
    mCallback.pop_file_remove_onLoad = function(data) {
      cm.tmp.cb_mod.pop_remove_onLoad.call(this, data);
    };
    mCallback.pop_file_remove_list_onLoad = function(data) {
      cm.tmp.cb_mod.pop_remove_onLoad.call(this, data);
    };
    mCallback.default_onLoad = function(data) {
      mCallback.account_info_onLoad(data);
    };
    mCallback.admin_shortcut_add_onLoad = function(data) {
      if($('#' + data.tplData.mId).length > 0) $('#' + data.tplData.mId).remove();
      
      data.tplData['mBody'] = cmApi.templates_add('modal_form', data.tplData.mData);
      var jqModal = $(cmApi.templates_add('modal', data.tplData))
        .prependTo(mMain)
        .modal({
          keyboard: false,
          show : true
        });
      $('form', jqModal).cmForm();
    };
    mCallback.admin_shortcut_add_save_onLoad = function(data) {
      var jqForm = $(this).parents('div.modal');
      //重新讀取主要畫面
      cm.tmp.main_loading = undefined;
      jqForm.modal('hide');
      cmApi.alert_message(data.message, 'success');
      cmApi.call_server_api({
        history : false,
        url : location.href
      });
    };
    mCallback.admin_shortcut_edit_onLoad = function(data) {
      cm.tmp.cb_mod.edit_onLoad.call(this, data);
    };
    mCallback.admin_shortcut_edit_save_onLoad = function(data) {
      //重新讀取主要畫面
      cm.tmp.main_loading = undefined;
      cm.tmp.cb_mod.edit_save_onLoad.call(this, data);
    };
    mCallback.admin_shortcut_remove_onLoad = function(data) {
      //重新讀取主要畫面
      cm.tmp.main_loading = undefined;
      cm.tmp.cb_mod.remove_onLoad.call(this, data);
    };
    mCallback.admin_shortcut_remove_list_onLoad = function(data) {
      //重新讀取主要畫面
      cm.tmp.main_loading = undefined;
      cm.tmp.cb_mod.remove_list_onLoad.call(this, data);
    };
    mCallback.admin_shortcut_list_onLoad = function(data) {
      cm.tmp.cb_mod.list_onLoad.call(this, data);
    };
  } else {
    cmApi.alert_message(cmLang.ERROR_MODULE_NOT_FOUND);
  }
})(jQuery, window._cm, window._cm.func, window._cm.lang);