<?php
  CM_Lang::import('core', array(
    CM_Lang::CODE_TW => array(
      'R_LOGIN' => '登入系統',
      'R_LOGIN_INTRO' => '登入系統界面',
      'R_LOGIN_AUTHORIZED_INTRO' => '輸入資料後的驗證',
      'R_LOGOFF' => '登出系統',
      'R_LOGOFF_INTRO' => '登出管理系統',
      'R_PASSWORD_FORGET' => '忘記密碼',
      'R_PASSWORD_FORGET_INTRO' => '忘記密碼表單填寫',
      'R_PASSWORD_FORGET_SEND_MAIL_INTRO' => '忘記密碼表單填寫完成後寄出含有密碼重設連結信件',
      'R_PASSWORD_FORGET_CHANGE_PASSWORD_INTRO' => '重設連結驗證後亂數產生新密碼並寄送新密碼通知給使用者',
      'R_ACCOUNT' => '系統管理',
      'R_ACCOUNT_INTRO' => '預設動作為我的帳號',
      'R_ACCOUNT_INFO' => '我的帳號',
      'R_ACCOUNT_INFO_INTRO' => '檢視和編輯登入者的帳號資料',
      'R_ACCOUNT_INFO_SAVE_INTRO' => '變更登入者帳號資料',
      'R_ACCOUNT_PASSWORD' => '變更密碼',
      'R_ACCOUNT_PASSWORD_INTRO' => '變更密碼界面',
      'R_ACCOUNT_PASSWORD_SAVE_INTRO' => '變更密碼並寄出新密碼信件',
      'R_ACCOUNT_LIST' => '帳號列表',
      'R_ACCOUNT_LIST_INTRO' => '檢視可以進入管理系統帳號',
      'R_ACCOUNT_ADD' => '新增帳號',
      'R_ACCOUNT_ADD_INTRO' => '新增帳號表單界面',
      'R_ACCOUNT_ADD_SAVE_INTRO' => '帳號資料新增',
      'R_ACCOUNT_EDIT' => '編輯帳號',
      'R_ACCOUNT_EDIT_INTRO' => '編輯帳號表單界界面',
      'R_ACCOUNT_EDIT_SAVE_INTRO' => '帳號資料編輯',
      'R_ACCOUNT_EDIT_L_STATUS_INTRO' => '列表上多個帳號狀態變更',
      'R_ACCOUNT_EDIT_L_GROUP_INTRO' => '列表上多個帳號群組變更',
      'R_ACCOUNT_REMOVE' => '移除帳號',
      'R_ACCOUNT_REMOVE_INTRO' => '帳號資料移除',
      'R_ACCOUNT_REMOVE_L_INTRO' => '列表上多個帳號移除',
      'R_ACCOUNT_GROUP_LIST' => '群組列表',
      'R_ACCOUNT_GROUP_LIST_INTRO' => '檢視可以管理系統帳號的群組',
      'R_ACCOUNT_GROUP_ADD' => '新增群組',
      'R_ACCOUNT_GROUP_ADD_INTRO' => '新增群組表單界面',
      'R_ACCOUNT_GROUP_ADD_SAVE_INTRO' => '群組資料新增',
      'R_ACCOUNT_GROUP_EDIT' => '編輯群組',
      'R_ACCOUNT_GROUP_EDIT_INTRO' => '編輯群組表單界界面',
      'R_ACCOUNT_GROUP_EDIT_SAVE_INTRO' => '群組資料編輯',
      'R_ACCOUNT_GROUP_REMOVE' => '移除群組',
      'R_ACCOUNT_GROUP_REMOVE_INTRO' => '群組資料移除',
      'R_ACCOUNT_GROUP_REMOVE_L_INTRO' => '列表上多個群組移除',
      'R_ACCOUNT_PASSWORD_RESET' => '重新設定密碼',
      'R_ACCOUNT_PASSWORD_RESET_INTRO' => '重新設定管理者密碼',
      'R_ACCOUNT_PASSWORD_RESET_SAVE_INTRO' => '重新設定管理者密碼',
      'R_UPLOAD_FILES' => '檔案上傳',
      'R_UPLOAD_FILES_INTRO' => '上傳所選擇的檔案',
      'R_UPLOAD_FILES_REMOVE_INTRO' => '移除上傳的檔案',
      'R_UPLOAD_FILES_LIST' => '檔案列表',
      'R_UPLOAD_FILES_LIST_INTRO' => '檢視已經上傳的檔案',
      'R_UPLOAD_FILES_EDIT' => '編輯檔案資料',
      'R_UPLOAD_FILES_EDIT_INTRO' => '編輯檔案資料表單界面',
      'R_UPLOAD_FILES_EDIT_SAVE_INTRO' => '檔案資料驗證後存檔',
      'R_UPLOAD_FILES_PREVIEW' => '檔案預覽資料',
      'R_UPLOAD_FILES_PREVIEW_INTRO' => '檔案預覽資料表單界面',
      'R_UPLOAD_FILES_REMOVE' => '移除檔案',
      'R_UPLOAD_FILES_REMOVE_INTRO' => '檔案資料移除',
      'R_UPLOAD_FILES_REMOVE_L_INTRO' => '列表上多個資料移除',
      'R_PERMISSION' => '權限設定',
      'R_PERMISSION_INTRO' => '設定網站權限',
      'R_PERMISSION_ACCOUNT' => '設定帳號權限',
      'R_PERMISSION_ACCOUNT_INTRO' => '設定後台系統帳號的權限',
      'R_PERMISSION_ACCOUNT_GROUP' => '設定群組權限',
      'R_PERMISSION_ACCOUNT_GROUP_INTRO' => '設定後台系統帳號群組的權限',
      'R_LOG' => '操作記錄',
      'R_LOG_INTRO' => '觀看後台帳號的操作記錄',
      'R_LOG_DETAIL_INTRO' => '檢視操作記錄詳細資料',
      'R_LOG_REMOVE_INTRO' => '移除操作記錄',
      'R_LOG_REMOVE_SEARCH' => '指定時間記錄移除',
      'R_LOG_EXPORT_INTRO' => '將操作記錄輸出成Excel',
      'R_CONFIG' => '網站設定',
      'R_CONFIG_INTRO' => '設定網站的一些設定值',
      'R_CONFIG_SAVE_INTRO' => '網站設定存檔',
      'R_ADMIN_SHORTCUT' => '捷徑管理',
      'R_ADMIN_SHORTCUT_INTRO' => '預設動作為捷徑列表',
      'R_ADMIN_SHORTCUT_LIST' => '捷徑',
      'R_ADMIN_SHORTCUT_LIST_INTRO' => '檢視捷徑列表',
      'R_ADMIN_SHORTCUT_ADD' => '新增捷徑',
      'R_ADMIN_SHORTCUT_ADD_INTRO' => '新增捷徑表單界面',
      'R_ADMIN_SHORTCUT_ADD_SAVE_INTRO' => '捷徑資料新增',
      'R_ADMIN_SHORTCUT_EDIT' => '編輯捷徑',
      'R_ADMIN_SHORTCUT_EDIT_INTRO' => '編輯捷徑表單界界面',
      'R_ADMIN_SHORTCUT_EDIT_SAVE_INTRO' => '捷徑資料編輯',
      'R_ADMIN_SHORTCUT_REMOVE' => '移除捷徑',
      'R_ADMIN_SHORTCUT_REMOVE_INTRO' => '捷徑資料移除',
      'R_ADMIN_SHORTCUT_REMOVE_L_INTRO' => '列表上多個捷徑移除',
      'R_ADMIN_SHORTCUT_EXPORT' => '資料輸出',
      'R_ADMIN_SHORTCUT_EXPORT_INTRO' => '將捷徑資料輸出成Excel',
      'R_FILE' => '檔案列表',
      'R_FILE_INTRO' => '編輯器上傳的檔案列表',
      'R_FILE_ADD' => '新增檔案',
      'R_FILE_ADD_INTRO' => '新增編輯器可以使用的檔案',
      'R_FILE_ADD_SAVE_INTRO' => '檔案資料新增',
      'R_FILE_EDIT' => '修改檔案',
      'R_FILE_EDIT_INTRO' => '修改編輯器可以使用的檔案',
      'R_FILE_EDIT_SAVE_INTRO' => '檔案資料修改',
      'R_FILE_REMOVE' => '移除檔案',
      'R_FILE_REMOVE_INTRO' => '移除檔案資料',
      'R_FILE_REMOVE_L_INTRO' => '列表上多個檔案移除',
      'LOGIN_TITLE' => '請輸入帳號(E-mail)和密碼登入系統',
      'LOGIN_BUTTON' => '登入管理系統',
      'LOGIN_REMEMBER_BUTTON' => '自動登入',
      'LOGIN_ERROR' => '您輸入的帳號或密碼不正確',
      'LOGIN_SUCCESS' => '%s 歡迎登入管理系統',
      'LOGOFF_CONFIRM' => '確定要登出系統嗎?',
      'LOGOFF_SUCCESS' => '已經登出系統',
      'PASSWORD_FORGET_TITLE_L' => '忘記密碼',
      'PASSWORD_FORGET_TITLE_S' => '重新設定您的密碼',
      'PASSWORD_FORGET_INTRO' => '請輸入您的帳號(E-mail)，系統將會寄送密碼重新設定通知到您的信箱',
      'PASSWORD_FORGET_SUBMIT' => '發送重新設定通知',
      'PASSWORD_FORGET_NOT_FOUND' => '找不到您輸入的帳號資料',
      'PASSWORD_FORGET_MAIL_SUBJECT' => '%s 密碼重設通知',
      'PASSWORD_FORGET_MAIL_BODY' => '%s您好：<br />您在%s的時候從位址%s要求修改密碼，請點選下面的連結更改密碼。<br /><br /><a href="%s">請點擊這裡更改密碼</a><br />*這個連結有效的時間為30分鐘<br /><br />&lt;如果您不想要變更密碼請忽略本信件&gt;',
      'PASSWORD_FORGET_MAIL_SUCCESS' => '密碼重設信件已經寄出，請點擊信件上的連結重新設定密碼',
      'PASSWORD_CHANGE_TITLE_L' => '變更密碼',
      'PASSWORD_CHANGE_INTRO' => '為了確認您的身份，您必須輸入目前使用的密碼才能更新密碼',
      'PASSWORD_CHANGE_SUBMIT' => '變更您的密碼',
      'PASSWORD_CHANGE_ERROR_PASSWORD' => '您輸入的密碼不正確',
      'PASSWORD_CHANGE_ERROR_PASSWORD_CONFIRM' => '您輸入的密碼和確認密碼不相同',
      'PASSWORD_CHANGE_MAIL_SUBJECT' => '%s 密碼已經變更完成',
      'PASSWORD_CHANGE_MAIL_BODY' => '%s您好：<br /><br />您的密碼已經變更完成。<br />您的新密碼是: %s<br /><br />&lt;再次提醒：請牢記您的密碼，並請妥善保存&gt;<br />若您沒有在本站註冊，請忽略本信函，並與客服人員連絡，謝謝。',
      'PASSWORD_CHANGE_MAIL_ERROR_TIMEOUT' => '這個連結已經過期',
      'PASSWORD_CHANGE_MAIL_SUCCESS' => '密碼通知信件已經寄出，新密碼將包含在這封信件內',
      'PASSWORD_RESET_TITLE_L' => '重新設定&nbsp;<b>%s</b>&nbsp;的密碼',
      'PASSWORD_RESET_INTRO' => '重設後會發送變更密碼信件給管理者&nbsp;%s',
      'PASSWORD_RESET_SUBMIT' => '重新設定密碼',
      'PASSWORD_RESET_MAIL_SUBJECT' => '%s 密碼已經重設',
      'PASSWORD_RESET_MAIL_BODY' => '%s您好：<br /><br />您的密碼已經重設完成。<br />您的新密碼是: %s<br /><br />&lt;再次提醒：請牢記您的密碼，並請妥善保存&gt;<br />若您沒有在本站註冊，請忽略本信函，並與客服人員連絡，謝謝。',
      'PASSWORD_RESET_MAIL_SUCCESS' => '密碼通知信件已經寄出，新密碼將包含在這封信件內',
      'ACCOUNT_FORM_GROUP_CONTACT' => '聯絡資訊',
      'ACCOUNT_FORM_GROUP_SETTING' => '帳號資訊',
      'ACCOUNT_ADD_INTRO' => '新增管理系統帳號',
      'ACCOUNT_EDIT_INTRO' => '修改管理系統帳號資料',
      'ACCOUNT_EDIT_L_SUCCESS' => '您所選擇的帳號資料已更新',
      'ACCOUNT_LIST_ACTION_GROUP' => '將選擇的項目群組變更為 %s',
      'ACCOUNT_LIST_ACTION_GROUP_CONFIRM' => '確定將選擇的項目群組變更為 %s?',
      'ACCOUNT_REMOVE_SUCCESS' => '帳號已移除',
      'ACCOUNT_REMOVE_L_SUCCESS' => '您所選擇的帳號已移除',
      'ACCOUNT_REMOVE_CONFIRM' => '確定要移除帳號&nbsp;<b>%s</b>&nbsp;嗎?',
      'ACCOUNT_INFO_INTRO' => '變更您的帳號資料',
      'ACCOUNT_INFO_SUCCESS' => '您的帳號資料已經儲存',
      'ACCOUNT_GROUP_FORM_GROUP_INFO' => '群組資訊',
      'ACCOUNT_GROUP_ADD_INTRO' => '新增帳號群組',
      'ACCOUNT_GROUP_REMOVE_L_SUCCESS' => '您所選擇的群組已移除',
      'ACCOUNT_GROUP_REMOVE_CONFIRM' => '確定要移除群組&nbsp;<b>%s</b>&nbsp;嗎?',
      'ACCOUNT_GROUP_EDIT' => '帳號群組資料修改',
      'ACCOUNT_GROUP_EDIT_INTRO' => '修改管理系統帳號群組設定',
      'ACCOUNT_LIST_INTRO' => '檢視可以進入管理系統的帳號',
      'ACCOUNT_LIST_ACCOUNT_TITLE' => '帳號列表',
      'ACCOUNT_LIST_ACCOUNT_GROUP_TITLE' => '群組列表',
      'PERMISSION_BTN' => '權限',
      'PERMISSION_ACCOUNT_INTRO' => '設定使用者&nbsp;%s&nbsp;的帳號權限',
      'PERMISSION_ACCOUNT_GROUP_INTRO' => '設定群組&nbsp;%s&nbsp;的權限',
      'PERMISSION_COLUMN_FUNC' => '功能名稱',
      'PERMISSION_COLUMN_DESC' => '功能敘述',
      'PASSWORD_RESET_BTN' => '重設',
      'LOG_DETAIL_TITLE' => '操作記錄詳細資料',
      'LOG_DETAIL_TITLE_S' => '(操作編號:%d)',
      'LOG_SEARCH_ACCOUNT' => '帳號名稱',
      'LOG_SEARCH_KEYWORD' => '關鍵字',
      'LOG_SEARCH_DATE_START' => '從',
      'LOG_SEARCH_DATE_END' => '到',
      'LOG_REMOVE_L_SUCCESS' => '您所選擇的記錄已移除',
      'LOG_REMOVE_SEARCH' => '移除記錄',
      'LOG_REMOVE_SEARCH_S' => '請選擇想要移除記錄的時間區間',
      'LOG_REMOVE_SEARCH_BTN' => '確定移除',
      'LOG_REMOVE_SEARCH_PROCESS_COMFIRM' => '確定要移除這個時間內的所有操作記錄?',
      'LOG_REMOVE_ERROR' => '指定的時間內沒有操作記錄',
      'LOG_REMOVE_SUCCESS_1' => '已移除%d筆記錄',
      'LOG_REMOVE_SUCCESS_2' => '已移除%d筆資料，失敗%d筆資料，失敗資料訊息:<br />%s',
      'LOG_EXPORT_BTN' => '匯出記錄',
      'CONFIG_SAVE_SUCCESS' => '設定值已儲存',
      'HEADER_MENU_LANG_TITLE' => '語言選擇',
      'HEADER_MENU_FUNC_TITLE' => '功能捷徑',
      'ADMIN_SHORTCUT_FORM_GROUP_INFO' => '捷徑資訊',
      'ADMIN_SHORTCUT_ADD_INTRO' => '新增管理系統捷徑',
      'ADMIN_SHORTCUT_EDIT_INTRO' => '修改管理系統捷徑資料',
      'ADMIN_SHORTCUT_EDIT_L_SUCCESS' => '您所選擇的捷徑資料已更新',
      'ADMIN_SHORTCUT_REMOVE_SUCCESS' => '捷徑已移除',
      'ADMIN_SHORTCUT_REMOVE_L_SUCCESS' => '您所選擇的捷徑已移除',
      'ADMIN_SHORTCUT_REMOVE_CONFIRM' => '確定要移除捷徑&nbsp;<b>%s</b>&nbsp;嗎?',
      'ADMIN_SHORTCUT_LIST_INTRO' => '檢視可以進入管理系統的捷徑',
      'ADMIN_SHORTCUT_LIST_TITLE' => '捷徑列表',
      'UPLOAD_EDIT_HEADLINE' => '檔案資訊',
      'UPLOAD_EDIT_SHEADLINE' => '修改上傳檔案的資訊',
      'UPLOAD_EDIT_BTN' => '修改檔案資訊',
      'FILE_FORM_GROUP_INFO' => '檔案資料',
      'FILE_REMOVE_CONFIRM' => '確定要移除檔案 %s 嗎?',
      'FILE_REMOVE_L_SUCCESS' => '您所選擇的檔案已經移除',
      'FILE_IMPORT_M' => '將勾選的檔案加入',
      'FILE_IMPORT' => '加入'
    ),
    CM_Lang::CODE_CN => array(
      'R_LOGIN' => '登入系统',
      'R_LOGIN_INTRO' => '登入系统界面',
      'R_LOGIN_AUTHORIZED_INTRO' => '输入资料後的验证',
      'R_LOGOFF' => '登出系统',
      'R_LOGOFF_INTRO' => '登出管理系统',
      'R_PASSWORD_FORGET' => '忘记密码',
      'R_PASSWORD_FORGET_INTRO' => '忘记密码表单填写',
      'R_PASSWORD_FORGET_SEND_MAIL_INTRO' => '忘记密码表单填写完成後寄出含有密码重设连结信件',
      'R_PASSWORD_FORGET_CHANGE_PASSWORD_INTRO' => '重设连结验证後乱数产生新密码并寄送新密码通知给使用者',
      'R_ACCOUNT' => '系统管理',
      'R_ACCOUNT_INTRO' => '预设动作为我的帐号',
      'R_ACCOUNT_INFO' => '我的帐号',
      'R_ACCOUNT_INFO_INTRO' => '检视和编辑登入者的帐号资料',
      'R_ACCOUNT_INFO_SAVE_INTRO' => '变更登入者帐号资料',
      'R_ACCOUNT_PASSWORD' => '变更密码',
      'R_ACCOUNT_PASSWORD_INTRO' => '变更密码界面',
      'R_ACCOUNT_PASSWORD_SAVE_INTRO' => '变更密码并寄出新密码信件',
      'R_ACCOUNT_LIST' => '帐号列表',
      'R_ACCOUNT_LIST_INTRO' => '检视可以进入管理系统帐号',
      'R_ACCOUNT_ADD' => '新增帐号',
      'R_ACCOUNT_ADD_INTRO' => '新增帐号表单界面',
      'R_ACCOUNT_ADD_SAVE_INTRO' => '帐号资料新增',
      'R_ACCOUNT_EDIT' => '编辑帐号',
      'R_ACCOUNT_EDIT_INTRO' => '编辑帐号表单界界面',
      'R_ACCOUNT_EDIT_SAVE_INTRO' => '帐号资料编辑',
      'R_ACCOUNT_EDIT_L_STATUS_INTRO' => '列表上多个帐号状态变更',
      'R_ACCOUNT_EDIT_L_GROUP_INTRO' => '列表上多个帐号群组变更',
      'R_ACCOUNT_REMOVE' => '移除帐号',
      'R_ACCOUNT_REMOVE_INTRO' => '帐号资料移除',
      'R_ACCOUNT_REMOVE_L_INTRO' => '列表上多个帐号移除',
      'R_ACCOUNT_GROUP_LIST' => '群组列表',
      'R_ACCOUNT_GROUP_LIST_INTRO' => '检视可以管理系统帐号的群组',
      'R_ACCOUNT_GROUP_ADD' => '新增群组',
      'R_ACCOUNT_GROUP_ADD_INTRO' => '新增群组表单界面',
      'R_ACCOUNT_GROUP_ADD_SAVE_INTRO' => '群组资料新增',
      'R_ACCOUNT_GROUP_EDIT' => '编辑群组',
      'R_ACCOUNT_GROUP_EDIT_INTRO' => '编辑群组表单界界面',
      'R_ACCOUNT_GROUP_EDIT_SAVE_INTRO' => '群组资料编辑',
      'R_ACCOUNT_GROUP_REMOVE' => '移除群组',
      'R_ACCOUNT_GROUP_REMOVE_INTRO' => '群组资料移除',
      'R_ACCOUNT_GROUP_REMOVE_L_INTRO' => '列表上多个群组移除',
      'R_ACCOUNT_PASSWORD_RESET' => '重新设定密码',
      'R_ACCOUNT_PASSWORD_RESET_INTRO' => '重新设定管理者密码',
      'R_ACCOUNT_PASSWORD_RESET_SAVE_INTRO' => '重新设定管理者密码',
      'R_UPLOAD_FILES' => '档案上传',
      'R_UPLOAD_FILES_INTRO' => '上传所选择的档案',
      'R_UPLOAD_FILES_REMOVE_INTRO' => '移除上传的档案',
      'R_UPLOAD_FILES_LIST' => '档案列表',
      'R_UPLOAD_FILES_LIST_INTRO' => '检视已经上传的档案',
      'R_UPLOAD_FILES_EDIT' => '编辑档案资料',
      'R_UPLOAD_FILES_EDIT_INTRO' => '编辑档案资料表单界面',
      'R_UPLOAD_FILES_EDIT_SAVE_INTRO' => '档案资料验证後存档',
      'R_UPLOAD_FILES_PREVIEW' => '档案预览资料',
      'R_UPLOAD_FILES_PREVIEW_INTRO' => '档案预览资料表单界面',
      'R_UPLOAD_FILES_REMOVE' => '移除档案',
      'R_UPLOAD_FILES_REMOVE_INTRO' => '档案资料移除',
      'R_UPLOAD_FILES_REMOVE_L_INTRO' => '列表上多个资料移除',
      'R_PERMISSION' => '权限设定',
      'R_PERMISSION_INTRO' => '设定网站权限',
      'R_PERMISSION_ACCOUNT' => '设定帐号权限',
      'R_PERMISSION_ACCOUNT_INTRO' => '设定後台系统帐号的权限',
      'R_PERMISSION_ACCOUNT_GROUP' => '设定群组权限',
      'R_PERMISSION_ACCOUNT_GROUP_INTRO' => '设定後台系统帐号群组的权限',
      'R_LOG' => '操作记录',
      'R_LOG_INTRO' => '观看後台帐号的操作记录',
      'R_LOG_DETAIL_INTRO' => '检视操作记录详细资料',
      'R_LOG_REMOVE_INTRO' => '移除操作记录',
      'R_LOG_REMOVE_SEARCH' => '指定时间记录移除',
      'R_LOG_EXPORT_INTRO' => '将操作记录输出成Excel',
      'R_CONFIG' => '网站设定',
      'R_CONFIG_INTRO' => '设定网站的一些设定值',
      'R_CONFIG_SAVE_INTRO' => '网站设定存档',
      'R_ADMIN_SHORTCUT' => '捷径管理',
      'R_ADMIN_SHORTCUT_INTRO' => '预设动作为捷径列表',
      'R_ADMIN_SHORTCUT_LIST' => '捷径',
      'R_ADMIN_SHORTCUT_LIST_INTRO' => '检视捷径列表',
      'R_ADMIN_SHORTCUT_ADD' => '新增捷径',
      'R_ADMIN_SHORTCUT_ADD_INTRO' => '新增捷径表单界面',
      'R_ADMIN_SHORTCUT_ADD_SAVE_INTRO' => '捷径资料新增',
      'R_ADMIN_SHORTCUT_EDIT' => '编辑捷径',
      'R_ADMIN_SHORTCUT_EDIT_INTRO' => '编辑捷径表单界界面',
      'R_ADMIN_SHORTCUT_EDIT_SAVE_INTRO' => '捷径资料编辑',
      'R_ADMIN_SHORTCUT_REMOVE' => '移除捷径',
      'R_ADMIN_SHORTCUT_REMOVE_INTRO' => '捷径资料移除',
      'R_ADMIN_SHORTCUT_REMOVE_L_INTRO' => '列表上多个捷径移除',
      'R_ADMIN_SHORTCUT_EXPORT' => '资料输出',
      'R_ADMIN_SHORTCUT_EXPORT_INTRO' => '将捷径资料输出成Excel',
      'R_FILE' => '档案列表',
      'R_FILE_INTRO' => '编辑器上传的档案列表',
      'R_FILE_ADD' => '新增档案',
      'R_FILE_ADD_INTRO' => '新增编辑器可以使用的档案',
      'R_FILE_ADD_SAVE_INTRO' => '档案资料新增',
      'R_FILE_EDIT' => '修改档案',
      'R_FILE_EDIT_INTRO' => '修改编辑器可以使用的档案',
      'R_FILE_EDIT_SAVE_INTRO' => '档案资料修改',
      'R_FILE_REMOVE' => '移除档案',
      'R_FILE_REMOVE_INTRO' => '移除档案资料',
      'R_FILE_REMOVE_L_INTRO' => '列表上多个档案移除',
      'LOGIN_TITLE' => '请输入帐号(E-mail)和密码登入系统',
      'LOGIN_BUTTON' => '登入管理系统',
      'LOGIN_REMEMBER_BUTTON' => '自动登入',
      'LOGIN_ERROR' => '您输入的帐号或密码不正确',
      'LOGIN_SUCCESS' => '%s 欢迎登入管理系统',
      'LOGOFF_CONFIRM' => '确定要登出系统吗?',
      'LOGOFF_SUCCESS' => '已经登出系统',
      'PASSWORD_FORGET_TITLE_L' => '忘记密码',
      'PASSWORD_FORGET_TITLE_S' => '重新设定您的密码',
      'PASSWORD_FORGET_INTRO' => '请输入您的帐号(E-mail)，系统将会寄送密码重新设定通知到您的信箱',
      'PASSWORD_FORGET_SUBMIT' => '发送重新设定通知',
      'PASSWORD_FORGET_NOT_FOUND' => '找不到您输入的帐号资料',
      'PASSWORD_FORGET_MAIL_SUBJECT' => '%s 密码重设通知',
      'PASSWORD_FORGET_MAIL_BODY' => '%s您好：<br />您在%s的时候从位址%s要求修改密码，请点选下面的连结更改密码。<br /><br /><a href="%s">请点击这里更改密码</a><br />*这个连结有效的时间为30分钟<br /><br />&lt;如果您不想要变更密码请忽略本信件&gt;',
      'PASSWORD_FORGET_MAIL_SUCCESS' => '密码重设信件已经寄出，请点击信件上的连结重新设定密码',
      'PASSWORD_CHANGE_TITLE_L' => '变更密码',
      'PASSWORD_CHANGE_INTRO' => '为了确认您的身份，您必须输入目前使用的密码才能更新密码',
      'PASSWORD_CHANGE_SUBMIT' => '变更您的密码',
      'PASSWORD_CHANGE_ERROR_PASSWORD' => '您输入的密码不正确',
      'PASSWORD_CHANGE_ERROR_PASSWORD_CONFIRM' => '您输入的密码和确认密码不相同',
      'PASSWORD_CHANGE_MAIL_SUBJECT' => '%s 密码已经变更完成',
      'PASSWORD_CHANGE_MAIL_BODY' => '%s您好：<br /><br />您的密码已经变更完成。<br />您的新密码是: %s<br /><br />&lt;再次提醒：请牢记您的密码，并请妥善保存&gt;<br />若您没有在本站注册，请忽略本信函，并与客服人员连络，谢谢。',
      'PASSWORD_CHANGE_MAIL_ERROR_TIMEOUT' => '这个连结已经过期',
      'PASSWORD_CHANGE_MAIL_SUCCESS' => '密码通知信件已经寄出，新密码将包含在这封信件内',
      'PASSWORD_RESET_TITLE_L' => '重新设定&nbsp;<b>%s</b>&nbsp;的密码',
      'PASSWORD_RESET_INTRO' => '重设後会发送变更密码信件给管理者&nbsp;%s',
      'PASSWORD_RESET_SUBMIT' => '重新设定密码',
      'PASSWORD_RESET_MAIL_SUBJECT' => '%s 密码已经重设',
      'PASSWORD_RESET_MAIL_BODY' => '%s您好：<br /><br />您的密码已经重设完成。<br />您的新密码是: %s<br /><br />&lt;再次提醒：请牢记您的密码，并请妥善保存&gt;<br />若您没有在本站注册，请忽略本信函，并与客服人员连络，谢谢。',
      'PASSWORD_RESET_MAIL_SUCCESS' => '密码通知信件已经寄出，新密码将包含在这封信件内',
      'ACCOUNT_FORM_GROUP_CONTACT' => '联络资讯',
      'ACCOUNT_FORM_GROUP_SETTING' => '帐号资讯',
      'ACCOUNT_ADD_INTRO' => '新增管理系统帐号',
      'ACCOUNT_EDIT_INTRO' => '修改管理系统帐号资料',
      'ACCOUNT_EDIT_L_SUCCESS' => '您所选择的帐号资料已更新',
      'ACCOUNT_LIST_ACTION_GROUP' => '将选择的项目群组变更为 %s',
      'ACCOUNT_LIST_ACTION_GROUP_CONFIRM' => '确定将选择的项目群组变更为 %s?',
      'ACCOUNT_REMOVE_SUCCESS' => '帐号已移除',
      'ACCOUNT_REMOVE_L_SUCCESS' => '您所选择的帐号已移除',
      'ACCOUNT_REMOVE_CONFIRM' => '确定要移除帐号&nbsp;<b>%s</b>&nbsp;吗?',
      'ACCOUNT_INFO_INTRO' => '变更您的帐号资料',
      'ACCOUNT_INFO_SUCCESS' => '您的帐号资料已经储存',
      'ACCOUNT_GROUP_FORM_GROUP_INFO' => '群组资讯',
      'ACCOUNT_GROUP_ADD_INTRO' => '新增帐号群组',
      'ACCOUNT_GROUP_REMOVE_L_SUCCESS' => '您所选择的群组已移除',
      'ACCOUNT_GROUP_REMOVE_CONFIRM' => '确定要移除群组&nbsp;<b>%s</b>&nbsp;吗?',
      'ACCOUNT_GROUP_EDIT' => '帐号群组资料修改',
      'ACCOUNT_GROUP_EDIT_INTRO' => '修改管理系统帐号群组设定',
      'ACCOUNT_LIST_INTRO' => '检视可以进入管理系统的帐号',
      'ACCOUNT_LIST_ACCOUNT_TITLE' => '帐号列表',
      'ACCOUNT_LIST_ACCOUNT_GROUP_TITLE' => '群组列表',
      'PERMISSION_BTN' => '权限',
      'PERMISSION_ACCOUNT_INTRO' => '设定使用者&nbsp;%s&nbsp;的帐号权限',
      'PERMISSION_ACCOUNT_GROUP_INTRO' => '设定群组&nbsp;%s&nbsp;的权限',
      'PERMISSION_COLUMN_FUNC' => '功能名称',
      'PERMISSION_COLUMN_DESC' => '功能叙述',
      'PASSWORD_RESET_BTN' => '重设',
      'LOG_DETAIL_TITLE' => '操作记录详细资料',
      'LOG_DETAIL_TITLE_S' => '(操作编号:%d)',
      'LOG_SEARCH_ACCOUNT' => '帐号名称',
      'LOG_SEARCH_KEYWORD' => '关键字',
      'LOG_SEARCH_DATE_START' => '从',
      'LOG_SEARCH_DATE_END' => '到',
      'LOG_REMOVE_L_SUCCESS' => '您所选择的记录已移除',
      'LOG_REMOVE_SEARCH' => '移除记录',
      'LOG_REMOVE_SEARCH_S' => '请选择想要移除记录的时间区间',
      'LOG_REMOVE_SEARCH_BTN' => '确定移除',
      'LOG_REMOVE_SEARCH_PROCESS_COMFIRM' => '确定要移除这个时间内的所有操作记录?',
      'LOG_REMOVE_ERROR' => '指定的时间内没有操作记录',
      'LOG_REMOVE_SUCCESS_1' => '已移除%d笔记录',
      'LOG_REMOVE_SUCCESS_2' => '已移除%d笔资料，失败%d笔资料，失败资料讯息:<br />%s',
      'LOG_EXPORT_BTN' => '汇出记录',
      'CONFIG_SAVE_SUCCESS' => '设定值已储存',
      'HEADER_MENU_LANG_TITLE' => '语言选择',
      'HEADER_MENU_FUNC_TITLE' => '功能捷径',
      'ADMIN_SHORTCUT_FORM_GROUP_INFO' => '捷径资讯',
      'ADMIN_SHORTCUT_ADD_INTRO' => '新增管理系统捷径',
      'ADMIN_SHORTCUT_EDIT_INTRO' => '修改管理系统捷径资料',
      'ADMIN_SHORTCUT_EDIT_L_SUCCESS' => '您所选择的捷径资料已更新',
      'ADMIN_SHORTCUT_REMOVE_SUCCESS' => '捷径已移除',
      'ADMIN_SHORTCUT_REMOVE_L_SUCCESS' => '您所选择的捷径已移除',
      'ADMIN_SHORTCUT_REMOVE_CONFIRM' => '确定要移除捷径&nbsp;<b>%s</b>&nbsp;吗?',
      'ADMIN_SHORTCUT_LIST_INTRO' => '检视可以进入管理系统的捷径',
      'ADMIN_SHORTCUT_LIST_TITLE' => '捷径列表',
      'UPLOAD_EDIT_HEADLINE' => '档案资讯',
      'UPLOAD_EDIT_SHEADLINE' => '修改上传档案的资讯',
      'UPLOAD_EDIT_BTN' => '修改档案资讯',
      'FILE_FORM_GROUP_INFO' => '档案资料',
      'FILE_REMOVE_CONFIRM' => '确定要移除档案 %s 吗?',
      'FILE_REMOVE_L_SUCCESS' => '您所选择的档案已经移除',
      'FILE_IMPORT_M' => '将勾选的档案加入',
      'FILE_IMPORT' => '加入'
    )
  ));
?>