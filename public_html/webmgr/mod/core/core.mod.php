<?php
  class modCore extends Admin_Mod {
    const MODULE_NAME = 'core';

    protected $_pop_mod = false;
    
    static public function lang($key) {
      return CM_Lang::line(self::MODULE_NAME . '.' . $key, '', CM_Conf::get('PATH_F.ADMIN_MOD') . self::MODULE_NAME . '/');
    }
    
    protected function get_header() {
      $header = array();
      
      if(isset($this->_session[Admin::VAR_ADMIN_ID])) {
        $thisDAO = Admin_Factory::createCMDAO('account');
        $thisDAO->init_options();
        $shortcutDAO = Admin_Factory::createCMDAO('adminShortcut');
        
        if($thisDAO->get_rows($accountRows, 'list', array('recordId' => $this->_session[Admin::VAR_ADMIN_ID]))) {
          $hasShortcut = $shortcutDAO->get_array($shortcutArray, Admin_DAO_AdminShortcut::GROUP_LIST, array(
            'accountId' => $this->_session[Admin::VAR_ADMIN_ID],
            'startIndex' => 0,
            'endIndex' => CM_Conf::get('DEF.SHORTCUT_COUNT')
          ));
          //有登入
          $header['account'] = array_merge(array(
            'hasShortcut' => $hasShortcut,
            'shortcut' => array_map(function($element) {
              return sprintf('<li><a class="request" title="%s" href="%s">%s</a></li>',
                $element[Admin_DAO_AdminShortcut::COLUMN_HEADLINE],
                $element['url'],
                $element[Admin_DAO_AdminShortcut::COLUMN_HEADLINE]
              );
            }, $shortcutArray),
            'shortcutLink' => $this->get_request('admin_shortcut_list')->set_text(self::lang('R_ADMIN_SHORTCUT'))->get_link(),
            'lang' => CM_Lang::get_code_options(),
            'langTitle' => self::lang('HEADER_MENU_LANG_TITLE'),
            'func' => array(),
            'funcTitle' => self::lang('HEADER_MENU_FUNC_TITLE'),
            'image_s' => CM_Conf::get('PATH_W.ADMIN_IMG') . 'user-thumb.png',
            'image_l' => CM_Conf::get('PATH_W.ADMIN_IMG') . 'user.jpg',
            'group' => Admin::get_options_name($accountRows[Admin_DAO_Account::COLUMN_GROUP_ID], $thisDAO->get_options('accountGroup')),
            'linkPasswordChange' => $this->get_request('account_password')->get_link(array('class' => 'btn btn-primary btn-small btn-block')),
            'button' => 
              $this->get_request('cm_logoff')->get_link(array('class' => 'btn btn-small pull-right')) .
              $this->get_request('account_info')->get_link(array('class' => 'btn btn-small'))
          ), $accountRows);
        }
      }
      
      $header['link'] = CM_Conf::get('PATH_W.ADMIN_ROOT');
      $header['logo'] = CM_Html::img(CM_Conf::get('PATH_W.ADMIN_IMG') . 'logo.png', CM_Lang::line('CM.SITE_NAME'), 154, 44);
      return array('header' => $header);
    }
    
    public function __construct($modName, $requestName = '', $actionName = '') {
      parent::__construct($modName, $requestName, $actionName);
      //設定功能-登入
      $this->add_request('cm_login', self::lang('R_LOGIN'), self::lang('R_LOGIN_INTRO'), array('setting' => false));
      $this->add_request_sub('cm_login', 'authorized', self::lang('R_LOGIN_AUTHORIZED_INTRO'));
      //設定功能-登出
      $this->add_request('cm_logoff', self::lang('R_LOGOFF'), self::lang('R_LOGOFF_INTRO'), array(
        'setting' => false,
        'attrParams' => array(
          'isHistory' => false,
          'isConfirm' => true,
          'confirmMsg' => self::lang('LOGOFF_CONFIRM')
        )
      ));
      //設定功能-忘記密碼
      $this->add_request('cm_forget_password', self::lang('R_PASSWORD_FORGET'), self::lang('R_PASSWORD_FORGET_INTRO'), array('setting' => false));
      $this->add_request_sub('cm_forget_password', 'send_mail', self::lang('R_PASSWORD_FORGET_SEND_MAIL_INTRO'));
      $this->add_request_sub('cm_forget_password', 'change_password', self::lang('R_PASSWORD_FORGET_CHANGE_PASSWORD_INTRO'));
      //設定功能-預設功能
      $this->add_request('default', self::lang('R_ACCOUNT'), self::lang('R_ACCOUNT_INTRO'), array('setting' => false));
      //設定功能-我的帳號
      $this->add_request('account_info', self::lang('R_ACCOUNT_INFO'), self::lang('R_ACCOUNT_INFO_INTRO'), array('setting' => false));
      $this->add_request_sub('account_info', 'save', self::lang('R_ACCOUNT_INFO_SAVE_INTRO'));
      //設定功能-變更密碼
      $this->add_request('account_password', self::lang('R_ACCOUNT_PASSWORD'), self::lang('R_ACCOUNT_PASSWORD_INTRO'), array(
        'setting' => false,
        'attrParams' => array('isHistory' => false)
      ));
      $this->add_request_sub('account_password', 'save', self::lang('R_ACCOUNT_PASSWORD_SAVE_INTRO'));
      //設定功能-帳號列表
      $this->add_request('account_list', self::lang('R_ACCOUNT_LIST'), self::lang('R_ACCOUNT_LIST_INTRO'));
      //設定功能-新增帳號
      $this->add_request('account_add', self::lang('R_ACCOUNT_ADD'), self::lang('R_ACCOUNT_ADD_INTRO'));
      $this->add_request_sub('account_add', 'save', self::lang('R_ACCOUNT_ADD_SAVE_INTRO'));
      //設定功能-編輯帳號
      $this->add_request('account_edit', self::lang('R_ACCOUNT_EDIT'), self::lang('R_ACCOUNT_EDIT_INTRO'));
      $this->add_request_sub('account_edit', 'save', self::lang('R_ACCOUNT_EDIT_SAVE_INTRO'));
      $this->add_request_sub('account_edit', 'list_status', self::lang('R_ACCOUNT_EDIT_L_STATUS_INTRO'));
      $this->add_request_sub('account_edit', 'list_group', self::lang('R_ACCOUNT_EDIT_L_GROUP_INTRO'));
      //設定功能-移除帳號
      $this->add_request('account_remove', self::lang('R_ACCOUNT_REMOVE'), self::lang('R_ACCOUNT_REMOVE_INTRO'), array(
        'attrParams' => array(
          'isHistory' => false,
          'isConfirm' => true,
          'confirmMsg' => self::lang('ACCOUNT_REMOVE_CONFIRM')
        )
      ));
      $this->add_request_sub('account_remove', 'list', self::lang('R_ACCOUNT_REMOVE_L_INTRO'));
      //設定功能-群組列表
      $this->add_request('account_group_list', self::lang('R_ACCOUNT_GROUP_LIST'), self::lang('R_ACCOUNT_GROUP_LIST_INTRO'));
      //設定功能-新增群組
      $this->add_request('account_group_add', self::lang('R_ACCOUNT_GROUP_ADD'), self::lang('R_ACCOUNT_GROUP_ADD_INTRO'));
      $this->add_request_sub('account_group_add', 'save', self::lang('R_ACCOUNT_GROUP_ADD_SAVE_INTRO'));
      //設定功能-編輯群組
      $this->add_request('account_group_edit', self::lang('R_ACCOUNT_GROUP_EDIT'), self::lang('R_ACCOUNT_GROUP_EDIT_INTRO'));
      $this->add_request_sub('account_group_edit', 'save', self::lang('R_ACCOUNT_GROUP_EDIT_SAVE_INTRO'));
      //設定功能-移除群組
      $this->add_request('account_group_remove', self::lang('R_ACCOUNT_GROUP_REMOVE'), self::lang('R_ACCOUNT_GROUP_REMOVE_INTRO'), array(
        'attrParams' => array(
          'isHistory' => false,
          'isConfirm' => true,
          'confirmMsg' => self::lang('ACCOUNT_GROUP_REMOVE_CONFIRM')
        )
      ));
      $this->add_request_sub('account_group_remove', 'list', self::lang('R_ACCOUNT_GROUP_REMOVE_L_INTRO'));
      //重設密碼
      $this->add_request('account_password_reset', self::lang('R_ACCOUNT_PASSWORD_RESET'), self::lang('R_ACCOUNT_PASSWORD_RESET_INTRO'), array(
        'setting' => false,
        'attrParams' => array('isHistory' => false)
      ));
      $this->add_request_sub('account_password_reset', 'save', self::lang('R_ACCOUNT_PASSWORD_RESET_SAVE_INTRO'));
      //設定功能-檔案上傳 
      $this->add_request('upload_files', self::lang('R_UPLOAD_FILES'), self::lang('R_UPLOAD_FILES_INTRO'), array('setting' => false));
      $this->add_request_sub('upload_files', 'upload', self::lang('R_UPLOAD_FILES_UPLOAD_INTRO'));
      $this->add_request_sub('upload_files', 'remove', self::lang('R_UPLOAD_FILES_REMOVE_INTRO'), array(
        'attrParams' => array(
          'isHistory' => false,
          'isConfirm' => true,
          'confirmMsg' => self::lang('UPLOAD_FILES_REMOVE_CONFIRM')
        )
      ));
      $this->add_request('upload_files_edit', self::lang('R_UPLOAD_FILES_EDIT'), self::lang('R_UPLOAD_FILES_EDIT_INTRO'));
      $this->add_request_sub('upload_files_edit', 'save', self::lang('R_UPLOAD_FILES_EDIT_SAVE_INTRO'), array(
        'attrParams' => array(
          'isHistory' => false,
          'nolock' => true
        )
      ));
	  $this->add_request('upload_files_preview', self::lang('R_UPLOAD_FILES_PREVIEW'), self::lang('R_UPLOAD_FILES_PREVIEW_INTRO'));

      $this->add_request('file', self::lang('R_FILE'), self::lang('R_FILE_INTRO'));
      $this->add_request('file_add', self::lang('R_FILE_ADD'), self::lang('R_FILE_ADD_INTRO'));
      $this->add_request_sub('file_add', 'save', self::lang('R_FILE_ADD_SAVE_INTRO'));
      $this->add_request('file_edit', self::lang('R_FILE_EDIT'), self::lang('R_FILE_EDIT_INTRO'));
      $this->add_request_sub('file_edit', 'save', self::lang('R_FILE_EDIT_SAVE_INTRO'));
      $this->add_request('file_remove', self::lang('R_FILE_REMOVE'), self::lang('R_FILE_REMOVE_INTRO'), array(
        'attrParams' => array(
          'isHistory' => false,
          'isConfirm' => true,
          'confirmMsg' => self::lang('FILE_REMOVE_CONFIRM')
        )
      ));
      $this->add_request_sub('file_remove', 'list', self::lang('R_REMOVE_L_INTRO'));

      $this->add_request('pop_file', self::lang('R_FILE'), self::lang('R_FILE_INTRO'));
      $this->add_request('pop_file_add', self::lang('R_FILE_ADD'), self::lang('R_FILE_ADD_INTRO'));
      $this->add_request_sub('pop_file_add', 'save', self::lang('R_FILE_ADD_SAVE_INTRO'));
      $this->add_request('pop_file_edit', self::lang('R_FILE_EDIT'), self::lang('R_FILE_EDIT_INTRO'));
      $this->add_request_sub('pop_file_edit', 'save', self::lang('R_FILE_EDIT_SAVE_INTRO'));
      $this->add_request('pop_file_remove', self::lang('R_FILE_REMOVE'), self::lang('R_FILE_REMOVE_INTRO'), array(
        'attrParams' => array(
          'isHistory' => false,
          'isConfirm' => true,
          'confirmMsg' => self::lang('FILE_REMOVE_CONFIRM')
        )
      ));
      $this->add_request_sub('pop_file_remove', 'list', self::lang('R_REMOVE_L_INTRO'));
      //網站全域設定
      $this->add_request('configure', self::lang('R_CONFIG'), self::lang('R_CONFIG_INTRO'));
      $this->add_request_sub('configure', 'save', self::lang('R_CONFIG_SAVE_INTRO'));
      //權限設定
      $this->add_request('permission', self::lang('R_PERMISSION'), self::lang('R_PERMISSION_INTRO'));
      $this->add_request('permission_account', self::lang('R_PERMISSION_ACCOUNT'), self::lang('R_PERMISSION_ACCOUNT_INTRO'));
      $this->add_request_sub('permission_account', 'save', self::lang('R_PERMISSION_ACCOUNT_SAVE_INTRO'));
      $this->add_request('permission_account_group', self::lang('R_PERMISSION_ACCOUNT_GROUP'), self::lang('R_PERMISSION_ACCOUNT_GROUP_INTRO'));
      $this->add_request_sub('permission_account_group', 'save', self::lang('R_PERMISSION_ACCOUNT_GROUP_SAVE_INTRO'));
      //操作記錄
      $this->add_request('log', self::lang('R_LOG'), self::lang('R_LOG_INTRO'));
      $this->add_request_sub('log', 'detail', self::lang('R_LOG_DETAIL_INTRO'), array(
        'attrParams' => array('isHistory' => false)
      ));
      $this->add_request_sub('log', 'remove', self::lang('R_LOG_REMOVE_INTRO'));
      //照時間移除查詢
      $this->add_request_sub('log', 'remove_search', self::lang('R_LOG_REMOVE_SEARCH'), array(
        'attrParams' => array('isHistory' => false)
      ));
      $this->add_request_sub('log', 'remove_search_process', self::lang('R_LOG_REMOVE_SEARCH_PROCESS'), array(
        'attrParams' => array(
          'isHistory' => false,
          'isConfirm' => true,
          'confirmMsg' => self::lang('LOG_REMOVE_SEARCH_PROCESS_COMFIRM')
        )
      ));
      //操作記錄輸出
      $this->add_request_sub('log', 'export', self::lang('R_LOG_EXPORT_INTRO'), array(
        'attrParams' => array(
          'ajax' => false,
          'data-target' => 'blank'
        )
      ));
      //捷徑功能
      $this->add_request('admin_shortcut_list', self::lang('R_ADMIN_SHORTCUT_LIST'), self::lang('R_ADMIN_SHORTCUT_LIST_INTRO'));
      $this->add_request('admin_shortcut_add', self::lang('R_ADMIN_SHORTCUT_ADD'), self::lang('R_ADMIN_SHORTCUT_ADD_INTRO'), array(
        'attrParams' => array('isHistory' => false)
      ));
      $this->add_request_sub('admin_shortcut_add', 'save', self::lang('R_ADMIN_SHORTCUT_ADD_SAVE_INTRO'));
      $this->add_request('admin_shortcut_edit', self::lang('R_ADMIN_SHORTCUT_EDIT'), self::lang('R_ADMIN_SHORTCUT_EDIT_INTRO'));
      $this->add_request_sub('admin_shortcut_edit', 'save', self::lang('R_ADMIN_SHORTCUT_EDIT_SAVE_INTRO'));
      $this->add_request('admin_shortcut_remove', self::lang('R_ADMIN_SHORTCUT_REMOVE'), self::lang('R_ADMIN_SHORTCUT_REMOVE_INTRO'), array(
        'attrParams' => array(
          'isHistory' => false,
          'isConfirm' => true,
          'confirmMsg' => self::lang('ADMIN_SHORTCUT_REMOVE_CONFIRM')
        )
      ));
      $this->add_request_sub('admin_shortcut_remove', 'list', self::lang('R_ADMIN_SHORTCUT_REMOVE_L_INTRO'));
      //設定Breadcrumb
      $this->_breadcrumb->add(self::lang('R_ACCOUNT'), $this->get_request('default')->set_text_format('<i class="icofont-cog"></i>&nbsp;%s')->get_link());
    }
    
    protected function get_account_list_func() {
      return array(
        array(
          'text' => sprintf('<i class="icofont-tasks"></i>&nbsp;%s', CM_Lang::line('Admin.LIST_CHANGE')),
          'has_sub' => true,
          'sub' => array(
            array('link' => $this->get_request('account_list')->set_text_format('<i class="icofont-user"></i>&nbsp;%s')->get_link(), 'has_sub' => false, 'sub' => array()),
            array('link' => $this->get_request('account_group_list')->set_text_format('<i class="icofont-group"></i>&nbsp;%s')->get_link(), 'has_sub' => false, 'sub' => array())
          )
        )
      );
    }

    protected function to_tmpl_permission($permissionLine) {
      $permissionArray = array();
      $permissionCheck = Admin_Permission::line_to_check_array($permissionLine);

      if(is_readable(CM_Conf::get('PATH_F.ADMIN_PERMISSION'))) {
        foreach (new DirectoryIterator(CM_Conf::get('PATH_F.ADMIN_PERMISSION')) as $file) {
          if($file->isDot()) continue;

          if($file->isDir()) continue;
          //只接受副檔名為json的設定檔案
          if(Admin::get_file_ext($file->getFilename()) != 'json') continue;

          $path = $file->getPathname();

          if(!is_readable($path)) continue;

          $permissionRows = json_decode(file_get_contents($path), true);
          $permissionMainCheck = in_array($permissionRows['id'], $permissionCheck['main']) ? true : false;
          $permissionRows['fields'] = sprintf('<input type="checkbox" name="permission[%s][_all]" value="1"%s />', $permissionRows['id'], $permissionMainCheck ? ' checked="checked"' : '');

          foreach($permissionRows['permission'] as $key => $value) {
            $check = isset($permissionCheck['sub'][$permissionRows['id']]) && in_array($key, $permissionCheck['sub'][$permissionRows['id']]);
            $permissionRows['permission'][$key]['fields'] = sprintf('<input type="checkbox" name="permission[%s][%s]" value="1"%s />',
              $permissionRows['id'],
              $key,
              $check || $permissionMainCheck ? ' checked="checked"' : ''
            );
          }

          $permissionRows['permission'] = CM_Format::to_num_array($permissionRows['permission']);
          $permissionArray[] = $permissionRows;
        }
      }

      return array_chunk($permissionArray, 2);
    }

    public function get_request($requestName, $actionName = '') {
      if($this->_pop_mod == true) {
        $requestName = "pop_{$requestName}";
        $request = parent::get_request($requestName, $actionName);
        return $request->set_attr_params(array(
          'isHistory' => false,
          'nolock' => true
        ));
      } else {
        return parent::get_request($requestName, $actionName);
      }
    }
    
    public function response_load_module() {
      parent::response_load_module();
    }
    
    public function response_cm_login() {
      $thisDAO = Admin_Factory::createCMDAO('account');
      $thisDAO->set_column('email', array('f_class' => 'input-block-level'));
      $thisDAO->set_column('password', array('f_class' => 'input-block-level'));
      $thisForm = Admin_Factory::createCMForm();
      $thisForm->set_form_dao($thisDAO, Admin_DAO_Account::GROUP_LOGIN);
      $formLogin = $this->get_request('cm_login', 'authorized')->get_form('loginForm');
      $linkForgetPassword = $this->get_request('cm_forget_password')->get_link(array(
        'class' => 'link',
        'isHistory' => false
      ));
      CM_Output::json('cm_login_ok', true, Admin::format_templates_data(array_merge($this->get_header(), array(
        'form' => array_merge($formLogin, array(
          'formTitle' => self::lang('LOGIN_TITLE'),
          'formColumn'  => $thisForm->get_form(),
          'btnLogin' => self::lang('LOGIN_BUTTON'),
          'btnRemember' => self::lang('LOGIN_REMEMBER_BUTTON'),
          'linkForgetPassword' => $linkForgetPassword
        ))
      ))));
    }
    
    public function response_cm_login_authorized() {
      $accountDAO = Admin_Factory::createCMDAO('account');
      $accountGroupDAO = Admin_Factory::createCMDAO('accountGroup');
      $params = array(
        'email' => $this->_post['email'],
        'password' => $this->_post['password'],
        'status' => '1'
      );
      
      if(!$accountDAO->validate_column($returnMessage, $this->_post, Admin_DAO_Account::GROUP_LOGIN)) {
        Admin::response_error($returnMessage);
      } elseif(!$accountDAO->get_rows($accountRows, '', $params)) {
        Admin::response_error(self::lang('LOGIN_ERROR'));
      } elseif(!$accountGroupDAO->get_array($accountGroupArray, '', array('recordIds' => $accountRows['group_id']))) {
        Admin::response_error(self::lang('LOGIN_ERROR'));
      } else {
        if(!$accountDAO->update_rows($rMessage, $rReturn, array(
          'login_count' => (int)$accountRows['login_count'] + 1,
          'login_last_date' => date('Y-m-d H:i:s')
        ), $accountRows[Admin_DAO_Account::COLUMN_RECORD_ID], Admin_DAO_Account::GROUP_LOGIN_COUNT)) Admin::response_error($rMessage);

        $this->_session[Admin::VAR_ADMIN_ID] = $accountRows['id'];
        $this->_session[Admin::VAR_ADMIN_NAME] = $accountRows['name'];
        $this->_session[Admin::VAR_ADMIN_EMAIL] = $accountRows['email']; 

        foreach(array('video', 'sound', 'picture') as $catalogName) {
          $catalogNameLarge = ucfirst($catalogName);
          $catalogDAO = Admin_Factory::createCMDAO("catalog_{$catalogNameLarge}");
          $allowCatalog = array();
          $hasCatalog = $catalogDAO->get_array_tree_childrens($catalogArray, $catalogIdArray, constant("Admin_DAO_Catalog_{$catalogNameLarge}::GROUP_LIST"), '0') ? true : false;

          foreach($accountGroupArray as $accountGroupRows) {
            //取得允許的影片目錄
            //if($hasCatalog == true && !empty($accountGroupRows["{$catalogName}_catalog"])) {
			if(!empty($accountGroupRows["{$catalogName}_catalog"])) {
              foreach(explode(',', $accountGroupRows["{$catalogName}_catalog"]) as $catalogId) {
                $allowCatalog[] = $catalogId;
                //將子目錄也加進來
                if(isset($catalogArray[$catalogId]) && isset($catalogArray[$catalogId]['_childrens']) && Admin::not_empty_array($catalogArray[$catalogId]['_childrens'])) $allowCatalog = array_merge($allowCatalog, $catalogArray[$catalogId]['_childrens']);
              }
            }
          }

          $this->_session["allow_{$catalogName}_catalog"] = array_values(array_unique($allowCatalog));
        }
        //設定群組編號和權限
        $groupId = array();
        $permission =  array();
        $permission[] = $accountRows['permission'];

        foreach($accountGroupArray as $accountGroupRows) {
          $permission[] = $accountGroupRows['permission'];
          $groupId[] = $accountGroupRows[Admin_DAO_AccountGroup::COLUMN_RECORD_ID];
        }

        $this->_session[Admin::VAR_ADMIN_GROUPS_ID] = $groupId;
        $this->_session['permission'] = $permission;

        $this->log_request(self::lang('R_LOGIN'));
        Admin::response_success(sprintf(self::lang('LOGIN_SUCCESS'), $accountRows[Admin::VAR_ADMIN_NAME]), true);
      }
    }
    
    public function response_cm_logoff() {
      $this->log_request(self::lang('R_LOGOFF'));
      unset($this->_session[Admin::VAR_ADMIN_ID]);
      unset($this->_session[Admin::VAR_ADMIN_GROUPS_ID]);
      unset($this->_session[Admin::VAR_ADMIN_NAME]);
      Admin::response_success(self::lang('LOGOFF_SUCCESS'));
    }
    
    public function response_cm_forget_password() {
      $thisDAO = Admin_Factory::createCMDAO('account');
      $formForgetPassword = $this->get_request('cm_forget_password', 'send_mail')->get_form('forgetPasswordForm');
      CM_Output::json('cm_forget_password_ok', true, Admin::format_templates_data(array(
        'mId' => 'passwordForgetForm',
        'mTop' => $formForgetPassword['formTop'],
        'mBottom' => $formForgetPassword['formBottom'],
        'mHeader' => self::lang('PASSWORD_FORGET_TITLE_L'),
        'mSHeader' => self::lang('PASSWORD_FORGET_TITLE_S'),
        'mButton' => 
          CM_Html::btn(CM_Lang::line('Admin.BTN_CANCEL'), 'button', array(
            'class' => 'btn',
            'data-dismiss' => 'modal',
            'aria-hidden' => 'true'
          )) .
          CM_Html::btn(self::lang('PASSWORD_FORGET_SUBMIT'), 'submit', array(
            'class' => 'btn btn-primary'
          )),
        'mData' => array(
          'field' => Admin_Factory::createCMForm()->get_field($thisDAO->get_column('email')),
          'tip' => self::lang('PASSWORD_FORGET_INTRO')
        )
      )));
    }
    
    public function response_cm_forget_password_send_mail() {
      $thisDAO = Admin_Factory::createCMDAO('account');
      $params = array('email' => $this->_post['email']);
      
      if(!$thisDAO->get_column('email')->validate($returnMessage, $this->_post['email'])) {
        Admin::response_error($returnMessage);
      } elseif(!$thisDAO->get_rows($accountRows, Admin_DAO_Account::GROUP_LIST, $params)) {
        Admin::response_error(self::lang('PASSWORD_FORGET_NOT_FOUND'));
      } else {
        $code = Admin::get_string_encrypt(implode('|', array(
          md5(microtime()),
          time(),
          $accountRows[Admin_DAO_Account::COLUMN_RECORD_ID],
          md5(microtime())
        )));
        $mailSubject = sprintf(self::lang('PASSWORD_FORGET_MAIL_SUBJECT'), CM_Lang::line('Admin.HEADER_TITLE'));
        $mailBody = sprintf(self::lang('PASSWORD_FORGET_MAIL_BODY'),
          $accountRows[Admin_DAO_Account::COLUMN_HEADLINE],
          date('Y-m-d H:i:s'),
          Admin::get_ip_address(),
          $this->get_request('cm_forget_password', 'change_password')->get_href(array('k' => $code))
        );
        $mailBody = CM_Template::get_tpl_html('mail_default', array(
          'mail_header' => $mailSubject,
          'mail_body' => $mailBody,
          'mail_footer' => Admin::get_mail_footer()
        ));
        Admin::mail($accountRows[Admin_DAO_Account::COLUMN_HEADLINE], $accountRows['email'], $mailSubject, $mailBody);
        Admin::response_success(self::lang('PASSWORD_FORGET_MAIL_SUCCESS'));
      }
    }
    
    public function response_cm_forget_password_change_password() {
      if(!isset($this->_get['k'])) Admin::response_error(self::lang('PASSWORD_FORGET_NOT_FOUND'));
      
      list($time1, $time, $id, $time2) = explode('|', Admin::get_string_encrypt($this->_get['k'], 'DECODE'));
      $thisDAO = Admin_Factory::createCMDAO('account');
      //時間限制
      if((time() - $time) > (60 * 30)) Admin::response_error(self::lang('PASSWORD_CHANGE_MAIL_ERROR_TIMEOUT'));
      
      if(!$thisDAO->get_rows($accountRows, Admin_DAO_Account::GROUP_LIST, array('recordId' => $id))) Admin::response_error(self::lang('PASSWORD_FORGET_NOT_FOUND'));
      
      $newPassword = substr(md5(time()), 0, 8);
      
      if(!$thisDAO->update_rows($returnMessage, $rReturn, array('password' => $newPassword), $id, Admin_DAO_Account::GROUP_PASSWORD)) Admin::response_error($returnMessage);
      
      $mailSubject = sprintf(self::lang('PASSWORD_CHANGE_MAIL_SUBJECT'), CM_Lang::line('Admin.HEADER_TITLE'));
      $mailBody = sprintf(self::lang('PASSWORD_CHANGE_MAIL_BODY'),
        $accountRows[Admin_DAO_Account::COLUMN_HEADLINE],
        $newPassword
      );
      $mailBody = CM_Template::get_tpl_html('mail_default', array(
        'mail_header' => $mailSubject,
        'mail_body' => $mailBody,
        'mail_footer' => Admin::get_mail_footer()
      ));
      Admin::mail($accountRows[Admin_DAO_Account::COLUMN_HEADLINE], $accountRows['email'], $mailSubject, $mailBody);
      Admin::response_success(self::lang('PASSWORD_CHANGE_MAIL_SUCCESS'));
    }
    
    public function response_get_page_main() {
      //使用者資訊
      CM_Output::json('get_page_index_ok', true, Admin::format_templates_data(array_merge(
        $this->get_header(),
        array(
          'columnLeft' => array(
            'menu' => Admin_Menu::to_menu_tmpl()
          ),
          'columnRight' => ''
        )
      )));
    }
    
    public function response_account_info() {
      $this->_breadcrumb->add(self::lang('R_ACCOUNT_INFO'), $this->get_request('account_info')->get_link());
      $thisDAO = Admin_Factory::createCMDAO('account');
      $thisForm = Admin_Factory::createCMForm();
      $thisForm->set_form_dao($thisDAO, Admin_DAO_Account::GROUP_INFO);
      $thisForm->set_form_default($thisDAO->get_rows_byId($accountRows, Admin_DAO_Account::GROUP_INFO, $this->_session[Admin::VAR_ADMIN_ID]) ? $accountRows : array());
      $form = $this->get_request('account_info', 'save')->get_form('accountEditForm');
      CM_Output::json('account_info_form_ok', true, Admin::format_templates_data(array_merge($form, array(
        'formButton' => 
          CM_Html::submit_btn(CM_Lang::line('Admin.BTN_EDIT'), array('class' => 'btn btn-primary')) . '&nbsp;' . 
          CM_Html::reset_btn(CM_Lang::line('Admin.BTN_RESET'), array('class' => 'btn')),
        'header' => array(
          'icon' => 'icofont-edit',
          'title' => self::lang('R_ACCOUNT_INFO'),
          'intro' => self::lang('ACCOUNT_INFO_INTRO'),
          'breadcrumb' => $this->_breadcrumb->trail()
        ),
        'formGroup' => $thisForm->get_groups_form(array(
          'account_contact' => array('title' => self::lang('ACCOUNT_FORM_GROUP_CONTACT')),
          'account_setting' => array('title' => self::lang('ACCOUNT_FORM_GROUP_SETTING'))
        ), CM_Form::ACTION_UPDATE)
      ))));
    }
    
    public function response_account_info_save() {
      $thisDAO = Admin_Factory::createCMDAO('account');
      
      if(!$thisDAO->update_rows($rMessage, $rReturn, $this->_post, $this->_session[Admin::VAR_ADMIN_ID], Admin_DAO_Account::GROUP_INFO)) {
        Admin::response_error($rMessage);
      } else {
        Admin::response_success(self::lang('ACCOUNT_INFO_SUCCESS'));
      }
    }
    
    public function response_account_password() {
      $thisDAO = Admin_Factory::createCMDAO('account');
      $thisDAO->set_column('password_new', array(
        'disable' => false,
        'f_confirm' => true
      ));
      $thisForm = Admin_Factory::createCMForm();
      $thisForm->set_form_dao($thisDAO, Admin_DAO_Account::GROUP_PASSWORD);
      $formForgetPassword = $this->get_request('account_password', 'save')->get_form('changePasswordForm');
      CM_Output::json('change_password_form_ok', true, Admin::format_templates_data(array(
        'mId' => 'changePasswordForm',
        'mTop' => $formForgetPassword['formTop'],
        'mBottom' => $formForgetPassword['formBottom'],
        'mHeader' => self::lang('PASSWORD_CHANGE_TITLE_L'),
        'mData' => array(
          'intro' => self::lang('PASSWORD_CHANGE_INTRO'),
          'fields' => $thisForm->get_form()
        ),
        'mButton' => 
          CM_Html::submit_btn(self::lang('PASSWORD_CHANGE_SUBMIT'), array(
            'class' => 'btn btn-primary'
          )) .
          CM_Html::btn(CM_Lang::line('Admin.BTN_CANCEL'), 'button', array(
            'class' => 'btn',
            'data-dismiss' => 'modal',
            'aria-hidden'=> 'true'
          ))
      )));
    }
    
    public function response_account_password_save() {
      $thisDAO = Admin_Factory::createCMDAO('account');
      $params = array(
        'email' => $this->_session[Admin::VAR_ADMIN_EMAIL],
        'password' => $this->_post['password']
      );
      
      if(!$thisDAO->validate_column($returnMessage, $this->_post, Admin_DAO_Account::GROUP_PASSWORD)) {
        Admin::response_error($returnMessage);
      } elseif($this->_post['password_new'] != $this->_post['password_new_confirm']) {
        Admin::response_error(self::lang('PASSWORD_CHANGE_ERROR_PASSWORD_CONFIRM'));
      } elseif(!$thisDAO->get_rows($accountRows, Admin_DAO_Account::GROUP_LIST, $params)) {
        Admin::response_error(self::lang('PASSWORD_CHANGE_ERROR_PASSWORD'));
      } else {
        if(!$thisDAO->update_rows($returnMessage, $rReturn, array('password' => $this->_post['password_new']), $accountRows[Admin_DAO_Account::COLUMN_RECORD_ID], Admin_DAO_Account::GROUP_PASSWORD)) Admin::response_error($returnMessage);
        
        $mailSubject = sprintf(self::lang('PASSWORD_CHANGE_MAIL_SUBJECT'), CM_Lang::line('CM.SITE_NAME'));
        $mailBody = sprintf(self::lang('PASSWORD_CHANGE_MAIL_BODY'),
          $this->_session[Admin::VAR_ADMIN_NAME],
          $this->_post['password_new']
        );
        $mailBody = CM_Template::get_tpl_html('mail_default', array(
          'mail_header' => $mailSubject,
          'mail_body' => $mailBody,
          'mail_footer' => Admin::get_mail_footer()
        ));
        Admin::mail($this->_session[Admin::VAR_ADMIN_NAME], $this->_session[Admin::VAR_ADMIN_EMAIL], $mailSubject, $mailBody);
        Admin::response_success(self::lang('PASSWORD_CHANGE_MAIL_SUCCESS'));
      }
    }

    public function response_account_password_reset() {
      $thisDAO = Admin_Factory::createCMDAO('account');

      if(!isset($this->_get[Admin::VAR_RECORD_ID]) || !$thisDAO->get_rows_byId($recordRows, Admin_DAO_Account::GROUP_LIST, (int)$this->_get[Admin::VAR_RECORD_ID])) Admin::response_error(CM_lang::line('CM.ERROR_DATA_NOT_FOUND'));

      $thisDAO->set_column('password_new', array(
        'disable' => false,
        'f_confirm' => true
      ));
      $thisForm = Admin_Factory::createCMForm();
      $thisForm->set_form_dao($thisDAO, Admin_DAO_Account::GROUP_PASSWORD_RESET);
      $formForgetPassword = $this->get_request('account_password_reset', 'save')->set_params(array(
        Admin::VAR_RECORD_ID => $recordRows[Admin_DAO_Account::COLUMN_RECORD_ID]
      ))->get_form('resetPasswordForm');
      CM_Output::json('reset_password_form_ok', true, Admin::format_templates_data(array(
        'mId' => 'resetPasswordForm',
        'mTop' => $formForgetPassword['formTop'],
        'mBottom' => $formForgetPassword['formBottom'],
        'mHeader' => sprintf(self::lang('PASSWORD_RESET_TITLE_L'), $recordRows[Admin_DAO_Account::COLUMN_HEADLINE]),
        'mData' => array(
          'intro' => sprintf(self::lang('PASSWORD_RESET_INTRO'), $recordRows[Admin_DAO_Account::COLUMN_HEADLINE]),
          'fields' => $thisForm->get_form()
        ),
        'mButton' => 
          CM_Html::submit_btn(self::lang('PASSWORD_RESET_SUBMIT'), array(
            'class' => 'btn btn-primary'
          )) .
          CM_Html::btn(CM_Lang::line('Admin.BTN_CANCEL'), 'button', array(
            'class' => 'btn',
            'data-dismiss' => 'modal',
            'aria-hidden'=> 'true'
          ))
      )));
    }

    public function response_account_password_reset_save() {
      $thisDAO = Admin_Factory::createCMDAO('account');

      if(!isset($this->_get[Admin::VAR_RECORD_ID])) {
        Admin::response_error(CM_lang::line('CM.ERROR_DATA_NOT_FOUND'));
      } if(!$thisDAO->validate_column($returnMessage, $this->_post, Admin_DAO_Account::GROUP_PASSWORD_RESET)) {
        Admin::response_error($returnMessage);
      } elseif($this->_post['password_new'] != $this->_post['password_new_confirm']) {
        Admin::response_error(self::lang('PASSWORD_CHANGE_ERROR_PASSWORD_CONFIRM'));
      } elseif(!$thisDAO->get_rows_byId($accountRows, Admin_DAO_Account::GROUP_LIST, (int)$this->_get[Admin::VAR_RECORD_ID])) {
        Admin::response_error(CM_lang::line('CM.ERROR_DATA_NOT_FOUND'));
      } else {
        if(!$thisDAO->update_rows($returnMessage, $rReturn, array('password' => $this->_post['password_new']), $accountRows[Admin_DAO_Account::COLUMN_RECORD_ID], Admin_DAO_Account::GROUP_PASSWORD)) Admin::response_error($returnMessage);
        
        $mailSubject = sprintf(self::lang('PASSWORD_RESET_MAIL_SUBJECT'), CM_Lang::line('CM.SITE_NAME'));
        $mailBody = sprintf(self::lang('PASSWORD_RESET_MAIL_BODY'),
          $accountRows[Admin_DAO_Account::COLUMN_HEADLINE],
          $this->_post['password_new']
        );
        $mailBody = CM_Template::get_tpl_html('mail_default', array(
          'mail_header' => $mailSubject,
          'mail_body' => $mailBody,
          'mail_footer' => Admin::get_mail_footer()
        ));
        Admin::mail($accountRows[Admin_DAO_Account::COLUMN_HEADLINE], $accountRows['email'], $mailSubject, $mailBody);
        Admin::response_success(self::lang('PASSWORD_RESET_MAIL_SUCCESS'));
      }
    }
    
    public function response_account_add() {
      $this->_breadcrumb->add(self::lang('R_ACCOUNT_LIST'), $this->get_request('account_list')->get_link());
      $this->_breadcrumb->add(self::lang('R_ACCOUNT_ADD'), $this->get_request('account_add')->get_link());

      $thisDAO = Admin_Factory::createCMDAO('account');
      $thisDAO->init_options();
      $thisDAO->set_column('password', array('f_confirm' => true));

      $thisForm = Admin_Factory::createCMForm();
      $thisForm->set_form_dao($thisDAO, Admin_DAO_Account::GROUP_ADD);
      $thisForm->set_form_default(array('group_id' => $this->_get['groupId']));
      $form = $this->get_request('account_add', 'save')->get_form('accountAddForm');

      CM_Output::json('account_add_form_ok', true, Admin::format_templates_data(array_merge($form, array(
        'hiddenColumn' => array(
          CM_Html::hidden_field(Admin::VAR_BACK_URL, $this->_get[Admin::VAR_BACK_URL])
        ),
        'formButton' => 
          CM_Html::submit_btn(CM_Lang::line('Admin.BTN_ADD'), array('class' => 'btn btn-primary')) . '&nbsp;' . 
          CM_Html::reset_btn(CM_Lang::line('Admin.BTN_CANCEL'), array('class' => 'btn prevBtn')) . '&nbsp;' .
          CM_Html::reset_btn(CM_Lang::line('Admin.BTN_RESET'), array('class' => 'btn')),
        'header' => array(
          'icon' => 'icofont-edit',
          'title' => self::lang('R_ACCOUNT_ADD'),
          'intro' => self::lang('ACCOUNT_ADD_INTRO'),
          'breadcrumb' => $this->_breadcrumb->trail()
        ),
        'formGroup' => $thisForm->get_groups_form(array(
          'account_contact' => array('title' => self::lang('ACCOUNT_FORM_GROUP_CONTACT')),
          'account_setting' => array('title' => self::lang('ACCOUNT_FORM_GROUP_SETTING'))
        ), CM_Form::ACTION_INSERT)
      ))));
    }
    
    public function response_account_add_save() {
      $thisDAO = Admin_Factory::createCMDAO('account');
      
      if(!$thisDAO->insert_rows($rMessage, $rReturn, $this->_post, Admin_DAO_Account::GROUP_ADD)) {
        Admin::response_error($rMessage);
      } else {
        $this->log_request($this->_post[Admin_DAO_Account::COLUMN_HEADLINE], $thisDAO->to_column_group_rows(Admin_DAO_Account::GROUP_ADD, $this->_post));
        CM_Output::json($rMessage, true, array('url' => isset($this->_post[Admin::VAR_BACK_URL]) ? Admin::filter_input($this->_post[Admin::VAR_BACK_URL], false) : $this->get_request('account_list')->get_href()));
      }
    }
    
    public function response_account_edit() {
      $thisDAO = Admin_Factory::createCMDAO('account');
      
      if(!isset($this->_get[Admin::VAR_RECORD_ID]) || !$thisDAO->get_rows_byId($accountRows, Admin_DAO_Account::GROUP_EDIT, $this->_get[Admin::VAR_RECORD_ID], array('exceptionId' => $this->_session[Admin::VAR_ADMIN_ID]))) Admin::response_error(CM_Lang::line('Admin.ERROR_NOT_FOUND'));
      
      $this->_breadcrumb->add(self::lang('R_ACCOUNT_LIST'), $this->get_request('account_list')->get_link());
      $this->_breadcrumb->add(self::lang('R_ACCOUNT_EDIT'), $this->get_request('account_edit')->get_link());

      $thisDAO->init_options();
      $thisForm = Admin_Factory::createCMForm();
      $thisForm->set_form_dao($thisDAO, Admin_DAO_Account::GROUP_EDIT);
      $thisForm->set_form_default($accountRows);
      $form = $this->get_request('account_edit', 'save')->get_form('editForm');

      CM_Output::json('account_update_form_ok', true, Admin::format_templates_data(array_merge($form, array(
        'hiddenColumn' => array(
          CM_Html::hidden_field(Admin::VAR_BACK_URL, $this->_get[Admin::VAR_BACK_URL]),
          CM_Html::hidden_field(Admin_DAO_Account::COLUMN_RECORD_ID, $accountRows[Admin_DAO_Account::COLUMN_RECORD_ID])
        ),
        'formButton' => 
          CM_Html::submit_btn(CM_Lang::line('Admin.BTN_EDIT'), array('class' => 'btn btn-primary')) . '&nbsp;' . 
          CM_Html::reset_btn(CM_Lang::line('Admin.BTN_CANCEL'), array('class' => 'btn prevBtn')) . '&nbsp;' .
          CM_Html::reset_btn(CM_Lang::line('Admin.BTN_RESET'), array('class' => 'btn')),
        'header' => array(
          'icon' => 'icofont-edit',
          'title' => self::lang('R_ACCOUNT_EDIT'),
          'intro' => self::lang('ACCOUNT_EDIT_INTRO'),
          'breadcrumb' => $this->_breadcrumb->trail()
        ),
        'formGroup' => $thisForm->get_groups_form(array(
          'account_contact' => array('title' => self::lang('ACCOUNT_FORM_GROUP_CONTACT')),
          'account_setting' => array('title' => self::lang('ACCOUNT_FORM_GROUP_SETTING'))
        ), CM_Form::ACTION_UPDATE)
      ))));
    }
    
    public function response_account_edit_save() {
      $thisDAO = Admin_Factory::createCMDAO('account');
      $params = array('daoParams' => array('exceptionId' => $this->_session[Admin::VAR_ADMIN_ID]));

      if(!isset($this->_post[Admin_DAO_Account::COLUMN_RECORD_ID]) || !$thisDAO->update_rows($rMessage, $rReturn, $this->_post, $this->_post[Admin_DAO_Account::COLUMN_RECORD_ID], Admin_DAO_Account::GROUP_EDIT, $params)) {
        Admin::response_error($rMessage);
      } else {
        $this->log_request($this->_post[Admin_DAO_Account::COLUMN_HEADLINE], $thisDAO->to_column_group_rows(Admin_DAO_Account::GROUP_EDIT, $this->_post));
        CM_Output::json($rMessage, true, array('url' => isset($this->_post[Admin::VAR_BACK_URL]) ? Admin::filter_input($this->_post[Admin::VAR_BACK_URL], false) : $this->get_request('account_list')->get_href()));
      }
    }

    public function response_account_edit_list_status() {
      $error = false;
      $errorMessage = array();
      $editIdArray = array();

      if(isset($this->_get[Admin::VAR_RECORD_ID])) {
        $editIdArray[] = (int)$this->_get[Admin::VAR_RECORD_ID];
      } elseif(isset($this->_post['listActionId']) && !empty($this->_post['listActionId'])) {
        $editIdArray = array_map(function($element) { return (int)$element; }, $this->_post['listActionId']);
      } else {
        $error = true;
      }

      if($error == true) Admin::response_error(CM_Lang::line('Admin.LIST_ACTION_ID_EMPTY'));

      $thisDAO = Admin_Factory::createCMDAO('account');
      $params = array('daoParams' => array('exceptionId' => $this->_session[Admin::VAR_ADMIN_ID]));
      $status = isset($this->_get[Admin_DAO_Account::COLUMN_STATUS]) ? (int)$this->_get[Admin_DAO_Account::COLUMN_STATUS] : '0';
      
      foreach($editIdArray as $index => $id) {
        if(!$thisDAO->update_rows($rMessage, $rReturn, array(Admin_DAO_Account::COLUMN_STATUS => $status), $id, Admin_DAO_Account::GROUP_STATUS, $params)) {
          $error = true;
          $errorMessage[] = $rMessage;
        } else {
          $log[] = implode('<br />', array_map(function($element) {
            return sprintf('<b>%s</b>: %s', $element['id'], $element['text']);
          }, array(
            array('id' => Admin_DAO_Account::COLUMN_RECORD_ID, 'text' => $id),
            array('id' => Admin_DAO_Account::COLUMN_STATUS, 'text' => $status),
            array('id' => Admin_DAO_Account::COLUMN_HEADLINE, 'text' => $rReturn['currentRows'][Admin_DAO_Account::COLUMN_HEADLINE]),
          )));
        }
      }

      if($error == true) {
        Admin::response_error(implode('<br />', $errorMessage));
      } else {
        $this->log_request(self::lang('R_ACCOUNT_EDIT_L_STATUS_INTRO'), array('edit' => implode('<br /><br />', $log)));
        Admin::response_success(self::lang('ACCOUNT_EDIT_L_SUCCESS'), true);
      }
    }

    public function response_account_edit_list_group() {
      $error = false;
      $errorMessage = array();
      $editIdArray = array();

      if(isset($this->_get[Admin::VAR_RECORD_ID])) {
        $editIdArray[] = (int)$this->_get[Admin::VAR_RECORD_ID];
      } elseif(isset($this->_post['listActionId']) && !empty($this->_post['listActionId'])) {
        $editIdArray = array_map(function($element) { return (int)$element; }, $this->_post['listActionId']);
      } else {
        $error = true;
      }

      if($error == true) Admin::response_error(CM_Lang::line('Admin.LIST_ACTION_ID_EMPTY'));

      $thisDAO = Admin_Factory::createCMDAO('account');
      $thisDAO->init_options();

      if(!isset($this->_get['group_id']) || !Admin::in_options($this->_get['group_id'], $thisDAO->get_options('accountGroup'), $currentOptions)) Admin::response_error(CM_lang::line('CM.ERROR_DATA_NOT_FOUND'));

      $params = array('daoParams' => array(
        'exceptionId' => $this->_session[Admin::VAR_ADMIN_ID]
      ));

      foreach($editIdArray as $index => $id) {
        if(!$thisDAO->update_rows($rMessage, $rReturn, array(Admin_DAO_Account::COLUMN_GROUP_ID => array($currentOptions['id'])), $id, Admin_DAO_Account::GROUP_GROUP, $params)) {
          $error = true;
          $errorMessage[] = $rMessage;
        } else {
          $log[] = implode('<br />', array_map(function($element) {
            return sprintf('<b>%s</b>: %s', $element['id'], $element['text']);
          }, array(
            array('id' => Admin_DAO_Account::COLUMN_RECORD_ID, 'text' => $id),
            array('id' => Admin_DAO_Account::COLUMN_GROUP_ID, 'text' => $currentOptions['id']),
            array('id' => 'group_name', 'text' => $currentOptions['text']),
            array('id' => Admin_DAO_Account::COLUMN_HEADLINE, 'text' => $rReturn['currentRows'][Admin_DAO_Account::COLUMN_HEADLINE]),
          )));
        }
      }

      if($error == true) {
        Admin::response_error(implode('<br />', $errorMessage));
      } else {
        $this->log_request(self::lang('R_ACCOUNT_GROUP_LIST_INTRO'), array('edit' => implode('<br /><br />', $log)));
        Admin::response_success(self::lang('ACCOUNT_EDIT_L_SUCCESS'), true);
      }
    }

    public function response_account_remove() {
      $thisDAO = Admin_Factory::createCMDAO('account');
      $params = array('daoParams' => array('exceptionId' => $this->_session[Admin::VAR_ADMIN_ID]));

      if(!isset($this->_get[Admin::VAR_RECORD_ID]) || !$thisDAO->delete_rows($rMessage, $rReturn, $this->_get[Admin::VAR_RECORD_ID], $params)) {
        Admin::response_error($rMessage);
      } else {
        $this->log_request($rReturn['currentRows'][Admin_DAO_Account::COLUMN_HEADLINE], $rReturn['currentRows']);
        CM_Output::json($rMessage, true, array('url' => isset($_GET[Admin::VAR_BACK_URL]) ? Admin::filter_input($_GET[Admin::VAR_BACK_URL], false) : $this->get_request('account_list')->get_href()));
      }
    }
    
    public function response_account_remove_list() {
      if(!isset($this->_post['listActionId']) || empty($this->_post['listActionId'])) Admin::response_error(CM_Lang::line('Admin.LIST_ACTION_ID_EMPTY'));

      $thisDAO = Admin_Factory::createCMDAO('account');
      $params = array('daoParams' => array('exceptionId' => $this->_session[Admin::VAR_ADMIN_ID]));
      $errorMessage = array();
      $error = false;
      $log = array();
      
      foreach($this->_post['listActionId'] as $index => $id) {
        if(!$thisDAO->delete_rows($rMessage, $rReturn, $id, $params)) {
          $error = true;
          $errorMessage[] = $rMessage;
        } else {
          $log[] = implode('<br />', array_map(function($element) {
            return sprintf('<b>%s</b>: %s', $element['id'], $element['text']);
          }, array(
            array('id' => Admin_DAO_Account::COLUMN_RECORD_ID, 'text' => $id),
            array('id' => Admin_DAO_Account::COLUMN_HEADLINE, 'text' => $rReturn['currentRows'][Admin_DAO_Account::COLUMN_HEADLINE])
          )));
        }
      }
      
      if($error == true) {
        Admin::response_error(implode('<br />', $errorMessage));
      } else {
        $this->log_request(self::lang('R_ACCOUNT_REMOVE_L_INTRO'), array('remove' => implode('<br /><br />', $log)));
        Admin::response_success(self::lang('ACCOUNT_REMOVE_L_SUCCESS'), true);
      }
    }

    public function response_account_list() {
      $cbThat = $this;
      $this->_breadcrumb->add(self::lang('R_ACCOUNT_LIST'), $this->get_request('account_list')->get_link());
      //設定關鍵字
      if(isset($this->_get['keyword'])) Admin_Mod::set_breadcrumb_keyword($this->_breadcrumb, $this->get_request('account_list'), $this->_get['keyword']);
      //帳號列表
      $thisDAO = Admin_Factory::createCMDAO('account');
      $thisDAO->init_options();
      $currentGroup = isset($this->_get['groupId']) ? $this->_get['groupId'] : '';
      $daoParams = array(
        'exceptionId' => $this->_session[Admin::VAR_ADMIN_ID],
        'groupId' => $currentGroup,
        'sort' => Admin_Mod::to_sort($thisDAO, $this->_get['sc'], $this->_get['sf']),
        'keyword' => $this->_get['keyword'],
      );

      if(Admin::is_root_user() == false) $daoParams['exceptionId'] = array_merge(array($daoParams['exceptionId']), Admin::get_root_user_id());

      $listColumn = array_merge($thisDAO->get_column_field_label_array(Admin_DAO_Account::GROUP_LIST), array(
        'action' => CM_Lang::line('Admin.LIST_ACTION')
      ));
      $listJSON = $thisDAO->get_array_object(Admin_DAO_Account::GROUP_LIST, array_merge($daoParams, array(
        'keyColumn' => Admin_DAO_Account::COLUMN_RECORD_ID,
        'oNowPage' => isset($this->_get['p']) ? (int)$this->_get['p'] : 1,
        'oPageCallback' => function($page, array $params, $key) use ($cbThat) {
          return $cbThat->cb_default_pagesplit($page, $params, $key, $cbThat->get_request('account_list'), array('vpage' => 'p'));
        }
      )));

      $checkboxFunc = array();
      $checkboxFunc[] = array(
        'link' => $this->get_request('account_edit', 'list_status')->set_params(array(Admin_DAO_Account::COLUMN_STATUS => '2'))->get_href(),
        'text' => CM_Lang::line('Admin.LIST_ACTION_ID_DISABLE'),
        'confirm' => CM_lang::line('Admin.LIST_ACTION_ID_DISABLE_CONFIRM')
      );
      $checkboxFunc[] = array(
        'link' => $this->get_request('account_edit', 'list_status')->set_params(array(Admin_DAO_Account::COLUMN_STATUS => '1'))->get_href(),
        'text' => CM_Lang::line('Admin.LIST_ACTION_ID_ENABLE'),
        'confirm' => CM_lang::line('Admin.LIST_ACTION_ID_ENABLE_CONFIRM')
      );
      $checkboxFunc[] = array('separate' => true);

      foreach($thisDAO->get_options('accountGroup') as $options) {
        $checkboxFunc[] = array(
          'link' => $this->get_request('account_edit', 'list_group')
            ->set_params(array('group_id' => $options['id']))->get_href(),
          'text' => sprintf(self::lang('ACCOUNT_LIST_ACTION_GROUP'), $options['text']),
          'confirm' => sprintf(self::lang('ACCOUNT_LIST_ACTION_GROUP_CONFIRM'), $options['text'])
        );
      }

      $checkboxFunc[] = array('separate' => true);
      $checkboxFunc[] = array(
        'link' => $this->get_request('account_remove', 'list')->get_href(),
        'text' => CM_Lang::line('Admin.LIST_ACTION_ID_REMOVE'),
        'confirm' => CM_lang::line('Admin.LIST_ACTION_ID_REMOVE_CONFIRM')
      );

      CM_Output::json('list_ok', true, Admin::format_templates_data(array(
        'header' => array(
          'icon' => 'icofont-th-list',
          'title' => self::lang('R_ACCOUNT_LIST'),
          'intro' => self::lang('R_ACCOUNT_LIST_INTRO'),
          'breadcrumb' => $this->_breadcrumb->trail(),
          'func' => $this->get_account_list_func()
        ),
        'list' => Admin_Mod::to_tmpl_table_list($listJSON, $listColumn, array(
          'addon' => Admin_Mod::to_tmpl_catalog($this->get_request('account_list'), $thisDAO, array(
            'daoParams' => $daoParams,
            'linkParams' => Admin::get_all_get_params(array('groupId'), true),
            'catalog' => $currentGroup,
            'catalogColumn' => 'groupId',
            'options' => $thisDAO->get_options('accountGroup')
          )),
          'id' => 'accountList',
          'title' => self::lang('ACCOUNT_LIST_ACCOUNT_TITLE'),
          'action' => array(
            'checkboxFunc' => array(
              'title' => CM_Lang::line('Admin.LIST_ACTION_MENU'),
              'list' => $checkboxFunc
            ),
            'listFunc' => array(
              $this->get_request('account_add')
                ->set_params(array(
                  Admin::VAR_BACK_URL => urlencode($this->get_request('account_list')->get_href(Admin::get_all_get_params(array(), true))),
                  'groupId' => $this->_get['groupId']
                ))
                ->set_text_format('<i class="icofont-plus"></i>&nbsp;%s')
                ->get_btn(array('class' => 'btn btn-primary'))
            ),
            'search' => array_merge(array(
              'hiddenColumn' => array(CM_Html::hidden_field('groupId', $currentGroup)),
              'formSearch' => CM_Html::input_field('keyword', $this->_get['keyword'], sprintf('placeholder="%s"', CM_Lang::line('Admin.SEARCH_FIELD_PLACEHOLDER'))),
              'formButton' => CM_Html::submit_btn('<i class="icofont-search"></i>&nbsp;' . CM_Lang::line('Admin.BTN_SEARCH'), array('class' => 'btn'))
            ), $this->get_request('account_list')->get_form('searchForm', array('method' => 'get', 'class' => 'form-inline')))
          ),
          'isSearch' => Admin::not_null($this->_get['keyword']),
          'callback' => array(
            'group_id' => function($record, $recordRows = array()) use($thisDAO) {
              if(!empty($record)) {
                $options = $thisDAO->get_options('accountGroup');
                return implode('<br />', array_map(function($element) use($options) {
                  return Admin::get_options_name($element, $options);
                }, explode(',', $record)));
              } else {
                return '--';
              }
            },
            'status' => function($record, $recordRows = array()) use($thisDAO, $cbThat) {
              $options = $thisDAO->get_options('status');
              $statusLabel = array(
                1 => 'label-success',
                2 => 'label-important'
              );

              if(Admin::in_options($record, $options, $currentOptions)) {
                if($cbThat->_session[Admin::VAR_ADMIN_ID] != $recordRows[Admin_DAO_Account::COLUMN_RECORD_ID]) {
                  return $cbThat->get_request('account_edit', 'list_status')
                    ->set_params(array(
                      Admin_DAO_Account::COLUMN_STATUS => $currentOptions['id'] == '1' ? '2' : '1',
                      Admin::VAR_RECORD_ID => $recordRows[Admin_DAO_Account::COLUMN_RECORD_ID]
                    ))
                    ->set_text(sprintf('<span class="label %s">%s</span>', $statusLabel[$currentOptions['id']], $currentOptions['text']))
                    ->get_link(array('isHistory' => false));
                } else {
                  return sprintf('<span class="label %s">%s</span>', $statusLabel[$currentOptions['id']], $currentOptions['text']);
                }
              } else {
                return '--';
              }
            },
            'action' => function($record, $recordRows = array()) use($cbThat, $cbThatLang) {
              if($cbThat->_session[Admin::VAR_ADMIN_ID] != $recordRows[Admin_DAO_Account::COLUMN_RECORD_ID]) {
                $action = array();
                //修改或刪除回來的網址
                $backurl = urlencode($cbThat->get_request('account_list')->get_href(Admin::get_all_get_params(array(Admin::VAR_RECORD_ID, Admin::VAR_ACTION), true)));
                //權限
                $requestPermission = $cbThat->get_request('permission_account')->set_params(array(
                  Admin::VAR_RECORD_ID => $recordRows[Admin_DAO_Account::COLUMN_RECORD_ID],
                  Admin::VAR_BACK_URL => $backurl
                ));
                //重設密碼
                $requestPassword = $cbThat->get_request('account_password_reset')->set_params(array(
                  Admin::VAR_RECORD_ID => $recordRows[Admin_DAO_Account::COLUMN_RECORD_ID]
                ));
                //修改
                $requestAccountEdit = $cbThat->get_request('account_edit')->set_params(array(
                  Admin::VAR_RECORD_ID => $recordRows[Admin_DAO_Account::COLUMN_RECORD_ID],
                  Admin::VAR_BACK_URL => $backurl
                ));
                //刪除
                $requestAccountRemove = $cbThat->get_request('account_remove')->set_params(array(
                  Admin::VAR_RECORD_ID => $recordRows[Admin_DAO_Account::COLUMN_RECORD_ID],
                  Admin::VAR_BACK_URL => $backurl
                ));
                
                if($requestPermission->check_permission()) {
                  $action[] = $requestPermission->get_link(array('class' => 'link'), sprintf('<i class="icofont-lock"></i>&nbsp;%s', $cbThat->lang('PERMISSION_BTN')));
                }

                if($requestPassword->check_permission()) {
                  $action[] = $requestPassword->get_link(array('class' => 'link'), sprintf('<i class="icofont-key"></i>&nbsp;%s', $cbThat->lang('PASSWORD_RESET_BTN')));
                }

                if($requestAccountEdit->check_permission()) {
                  $action[] = $requestAccountEdit->get_link(array('class' => 'link'), sprintf('<i class="icofont-edit"></i>&nbsp;%s', CM_Lang::line('Admin.LIST_ACTION_EDIT')));
                }
                
                if($requestAccountRemove->check_permission()) {
                  $action[] = $requestAccountRemove->get_link(array(
                    'class' => 'link',
                    'confirmMsg' => sprintf(modCore::lang('ACCOUNT_REMOVE_CONFIRM'), $recordRows['email'])
                  ), sprintf('<i class="icofont-trash"></i>&nbsp;%s', CM_Lang::line('Admin.LIST_ACTION_REMOVE')));
                }
                
                return implode('&nbsp;|&nbsp;', $action);
              } else {
                return '';
              }
            }
          ),
          'h_callback' => function($rows, $name) use($cbThat) {
            return $cbThat->cb_default_sort($rows, $name, $cbThat
              ->get_request('account_list'), array(
                'link_params' => $cbThat->_get,
                'sort_column' => array(Admin_DAO_Account::COLUMN_RECORD_ID, Admin_DAO_Account::COLUMN_HEADLINE, 'email', 'phone', 'cellphone', Admin_DAO_Account::COLUMN_STATUS, 'create_date')
              )
            );
          }
        ))
      )));
    }
    
    public function response_account_group_add() {
      $this->_breadcrumb->add(self::lang('R_ACCOUNT_GROUP_LIST'), $this->get_request('account_group_list')->get_link());
      $this->_breadcrumb->add(self::lang('R_ACCOUNT_GROUP_ADD'), $this->get_request('account_group_add')->get_link());

      $thisDAO = Admin_Factory::createCMDAO('accountGroup');
      $thisDAO->init_options();

      $thisForm = Admin_Factory::createCMForm();
      $thisForm->set_form_dao($thisDAO, Admin_DAO_AccountGroup::GROUP_ADD);
      $form = $this->get_request('account_group_add', 'save')->get_form('accountGroupAddForm');

      CM_Output::json('account_group_add_form_ok', true, Admin::format_templates_data(array_merge($form, array(
        'hiddenColumn' => array(
          CM_Html::hidden_field(Admin::VAR_BACK_URL, $this->_get[Admin::VAR_BACK_URL])
        ),
        'formButton' => 
          CM_Html::submit_btn(CM_Lang::line('Admin.BTN_ADD'), array('class' => 'btn btn-primary')) . '&nbsp;' . 
          CM_Html::reset_btn(CM_Lang::line('Admin.BTN_CANCEL'), array('class' => 'btn prevBtn')) . '&nbsp;' .
          CM_Html::reset_btn(CM_Lang::line('Admin.BTN_RESET'), array('class' => 'btn')),
        'header' => array(
          'icon' => 'icofont-edit',
          'title' => self::lang('R_ACCOUNT_GROUP_ADD'),
          'intro' => self::lang('ACCOUNT_GROUP_ADD_INTRO'),
          'breadcrumb' => $this->_breadcrumb->trail()
        ),
        'formGroup' => array(
          array('title' => self::lang('ACCOUNT_GROUP_FORM_GROUP_INFO'), 'fields' => $thisForm->get_form(CM_Form::ACTION_INSERT))
        )
      ))));
    }
    
    public function response_account_group_add_save() {
      $thisDAO = Admin_Factory::createCMDAO('accountGroup');
      
      if(!$thisDAO->insert_rows($rMessage, $rReturn, $this->_post, Admin_DAO_AccountGroup::GROUP_ADD)) {
        Admin::response_error($rMessage);
      } else {
        $this->log_request($this->_post[Admin_DAO_AccountGroup::COLUMN_HEADLINE], $thisDAO->to_column_group_rows(Admin_DAO_AccountGroup::GROUP_ADD, $this->_post));
        CM_Output::json($rMessage, true, array('url' => isset($this->_post[Admin::VAR_BACK_URL]) ? Admin::filter_input($this->_post[Admin::VAR_BACK_URL], false) : $this->get_request('account_group_list')->get_href()));
      }
    }
    
    public function response_account_group_edit() {
      $thisDAO = Admin_Factory::createCMDAO('accountGroup');
      
      if(!isset($this->_get[Admin::VAR_RECORD_ID]) || !$thisDAO->get_rows_byId($accountGroupRows, Admin_DAO_AccountGroup::GROUP_EDIT, $this->_get[Admin::VAR_RECORD_ID])) Admin::response_error(CM_Lang::line('Admin.ERROR_NOT_FOUND'));
      
      $this->_breadcrumb->add(self::lang('R_ACCOUNT_GROUP_LIST'), $this->get_request('account_group_list')->get_link());
      $this->_breadcrumb->add(self::lang('R_ACCOUNT_GROUP_EDIT'), $this->get_request('account_group_edit')->get_link());

      $thisDAO->init_options();

      $thisForm = Admin_Factory::createCMForm();
      $thisForm->set_form_dao($thisDAO, Admin_DAO_AccountGroup::GROUP_EDIT);
      $thisForm->set_form_default($accountGroupRows);
      $form = $this->get_request('account_group_edit', 'save')->get_form('accountGroupEditForm');

      CM_Output::json('response ok', true, Admin::format_templates_data(array_merge($form, array(
        'hiddenColumn' => array(
          CM_Html::hidden_field(Admin::VAR_BACK_URL, $this->_get[Admin::VAR_BACK_URL]),
          CM_Html::hidden_field(Admin_DAO_AccountGroup::COLUMN_RECORD_ID, $accountGroupRows[Admin_DAO_AccountGroup::COLUMN_RECORD_ID])
        ),
        'formButton' => 
          CM_Html::submit_btn(CM_Lang::line('Admin.BTN_EDIT'), array('class' => 'btn btn-primary')) . '&nbsp;' . 
          CM_Html::reset_btn(CM_Lang::line('Admin.BTN_CANCEL'), array('class' => 'btn prevBtn')) . '&nbsp;' .
          CM_Html::reset_btn(CM_Lang::line('Admin.BTN_RESET'), array('class' => 'btn')),
        'header' => array(
          'icon' => 'icofont-edit',
          'title' => self::lang('R_ACCOUNT_GROUP_EDIT'),
          'intro' => self::lang('ACCOUNT_GROUP_EDIT_INTRO'),
          'breadcrumb' => $this->_breadcrumb->trail()
        ),
        'formGroup' => array(
          array('title' => self::lang('ACCOUNT_GROUP_FORM_GROUP_INFO'), 'fields' => $thisForm->get_form(CM_Form::ACTION_UPDATE))
        )
      ))));
    }
    
    public function response_account_group_edit_save() {
      $thisDAO = Admin_Factory::createCMDAO('accountGroup');
      
      if(!isset($this->_post[Admin_DAO_AccountGroup::COLUMN_RECORD_ID]) || !$thisDAO->update_rows($rMessage, $rReturn, $this->_post, $this->_post[Admin_DAO_AccountGroup::COLUMN_RECORD_ID], Admin_DAO_AccountGroup::GROUP_EDIT)) {
        Admin::response_error($rMessage);
      } else {
        $this->log_request($this->_post[Admin_DAO_AccountGroup::COLUMN_HEADLINE], $thisDAO->to_column_group_rows(Admin_DAO_AccountGroup::GROUP_EDIT, $this->_post));
        CM_Output::json($rMessage, true, array('url' => isset($this->_post[Admin::VAR_BACK_URL]) ? Admin::filter_input($this->_post[Admin::VAR_BACK_URL], false) : $this->get_request('account_group_list')->get_href()));
      }
    }

    public function response_account_group_remove() {
      $thisDAO = Admin_Factory::createCMDAO('accountGroup');

      if(!isset($this->_get[Admin::VAR_RECORD_ID]) || !$thisDAO->delete_rows($rMessage, $rReturn, $this->_get[Admin::VAR_RECORD_ID])) {
        Admin::response_error($rMessage);
      } else {
        $this->log_request($rReturn['currentRows'][Admin_DAO_AccountGroup::COLUMN_HEADLINE], $rReturn['currentRows']);
        CM_Output::json($rMessage, true, array('url' => isset($_GET[Admin::VAR_BACK_URL]) ? Admin::filter_input($_GET[Admin::VAR_BACK_URL], false) : $this->get_request('account_group_list')->get_href()));
      }
    }

    public function response_account_group_remove_list() {
      if(!isset($this->_post['listActionId']) || empty($this->_post['listActionId'])) Admin::response_error(CM_Lang::line('Admin.LIST_ACTION_ID_EMPTY'));

      $thisDAO = Admin_Factory::createCMDAO('accountGroup');
      $errorMessage = array();
      $error = false;
      $log = array();
      
      foreach($this->_post['listActionId'] as $index => $id) {
        if(!$thisDAO->delete_rows($rMessage, $rReturn, $id)) {
          $error = true;
          $errorMessage[] = $rMessage;
        } else {
          $log[] = implode('<br />', array_map(function($element) {
            return sprintf('<b>%s</b>: %s', $element['id'], $element['text']);
          }, array(
            array('id' => Admin_DAO_AccountGroup::COLUMN_RECORD_ID, 'text' => $id),
            array('id' => Admin_DAO_AccountGroup::COLUMN_HEADLINE, 'text' => $rReturn['currentRows'][Admin_DAO_AccountGroup::COLUMN_HEADLINE])
          )));
        }
      }
      
      if($error == true) {
        Admin::response_error(implode('<br />', $errorMessage));
      } else {
        $this->log_request(self::lang('R_ACCOUNT_GROUP_REMOVE_L_INTRO'), array('remove' => implode('<br /><br />', $log)));
        Admin::response_success(self::lang('ACCOUNT_GROUP_REMOVE_L_SUCCESS'), true);
      }
    }
    
    public function response_account_group_list() {
      $cbThat = $this;
      $this->_breadcrumb->add(self::lang('R_ACCOUNT_GROUP_LIST'), $this->get_request('account_group_list')->get_link());
      $thisDAO = Admin_Factory::createCMDAO('accountGroup');
      $thisDAO->init_options();
      //設定關鍵字
      if(isset($this->_get['keyword'])) Admin_Mod::set_breadcrumb_keyword($this->_breadcrumb, $this->get_request('account_group_list'), $this->_get['keyword']);
      //列表欄位
      $listColumn = array_merge($thisDAO->get_column_field_label_array(Admin_DAO_AccountGroup::GROUP_LIST), array(
        'action' => CM_Lang::line('Admin.LIST_ACTION')
      ));
      $listJSON = $thisDAO->get_array_object(Admin_DAO_AccountGroup::GROUP_LIST, array(
        'keyColumn' => Admin_DAO_AccountGroup::COLUMN_RECORD_ID,
        'sort' => Admin_Mod::to_sort($thisDAO, $this->_get['sc'], $this->_get['sf']),
        'keyword' => $this->_get['keyword'],
        'oNowPage' => isset($this->_get['p']) ? (int)$this->_get['p'] : 1,
        'oPageCallback' => function($page, array $params, $key) use($cbThat) {
          return $cbThat->cb_default_pagesplit($page, $params, $key, $cbThat->get_request('account_group_list'), array('vpage' => 'p'));
        }
      ));
      CM_Output::json('list_ok', true, Admin::format_templates_data(array(
        'header' => array(
          'icon' => 'icofont-th-list',
          'title' => self::lang('R_ACCOUNT_GROUP_LIST'),
          'intro' => self::lang('R_ACCOUNT_GROUP_LIST_INTRO'),
          'breadcrumb' => $this->_breadcrumb->trail(),
          'func' => $this->get_account_list_func()
        ),
        'list' =>  Admin_Mod::to_tmpl_table_list($listJSON, $listColumn, array(
          'id' => 'accountGroupList',
          'title' => self::lang('ACCOUNT_LIST_ACCOUNT_GROUP_TITLE'),
          'action' => array(
            'checkboxFunc' => array(
              'title' => CM_Lang::line('Admin.LIST_ACTION_MENU'),
              'list' => array(
                array(
                  'link' => $this->get_request('account_group_remove', 'list')->get_href(),
                  'text' => CM_Lang::line('Admin.LIST_ACTION_ID_REMOVE'),
                  'confirm' => CM_lang::line('Admin.LIST_ACTION_ID_REMOVE_CONFIRM')
                )
              )
            ),
            'listFunc' => array(
              $this->get_request('account_group_add')
                ->set_params(array(
                  Admin::VAR_BACK_URL => urlencode($this->get_request('account_group_list')->get_href(Admin::get_all_get_params(array(), true)))
                ))
                ->set_text_format('<i class="icofont-plus"></i>&nbsp;%s')
                ->get_btn(array('class' => 'btn btn-primary'))
            ),
            'search' => array_merge(array(
              'formSearch' => CM_Html::input_field('keyword', $this->_get['keyword'], sprintf('placeholder="%s"', CM_Lang::line('Admin.SEARCH_FIELD_PLACEHOLDER'))),
              'formButton' => CM_Html::submit_btn('<i class="icofont-search"></i>&nbsp;' . CM_Lang::line('Admin.BTN_SEARCH'), array('class' => 'btn'))
            ), $this->get_request('account_group_list')->get_form('searchForm', array('method' => 'get', 'class' => 'form-inline')))
          ),
          'isSearch' => Admin::not_null($this->_get['keyword']),
          'callback' => array(
            'video_catalog' => function($record, $recordRows = array()) use($thisDAO) {
              if(!empty($record)) {
                $options = $thisDAO->get_options('video_catalog');
                return implode('<br /><br />', array_map(function($element) use($options) {
                  return Admin::get_options_name($element, $options);
                }, explode(',', $record)));
              } else {
                return '--';
              }
            },
            'sound_catalog' => function($record, $recordRows = array()) use($thisDAO) {
              if(!empty($record)) {
                $options = $thisDAO->get_options('sound_catalog');
                return implode('<br /><br />', array_map(function($element) use($options) {
                  return Admin::get_options_name($element, $options);
                }, explode(',', $record)));
              } else {
                return '--';
              }
            },
            'picture_catalog' => function($record, $recordRows = array()) use($thisDAO) {
              if(!empty($record)) {
                $options = $thisDAO->get_options('picture_catalog');
                return implode('<br /><br />', array_map(function($element) use($options) {
                  return Admin::get_options_name($element, $options);
                }, explode(',', $record)));
              } else {
                return '--';
              }
            },
            'action' => function($record, $recordRows = array()) use($cbThat, $cbThatLang) {
              $action = array();
              //修改或刪除回來的網址
              $backurl = urlencode($cbThat->get_request('account_group_list')->get_href(Admin::get_all_get_params(array(Admin::VAR_RECORD_ID, Admin::VAR_ACTION), true)));
              //權限
              $requestPermission = $cbThat->get_request('permission_account_group')->set_params(array(
                Admin::VAR_RECORD_ID => $recordRows[Admin_DAO_AccountGroup::COLUMN_RECORD_ID],
                Admin::VAR_BACK_URL => $backurl
              ));
              //修改
              $requestAccountGroupEdit = $cbThat->get_request('account_group_edit')->set_params(array(
                Admin::VAR_RECORD_ID => $recordRows[Admin_DAO_Account::COLUMN_RECORD_ID],
                Admin::VAR_BACK_URL => $backurl
              ));
              //刪除
              $requestAccountGroupRemove = $cbThat->get_request('account_group_remove')->set_params(array(
                Admin::VAR_RECORD_ID => $recordRows[Admin_DAO_Account::COLUMN_RECORD_ID],
                Admin::VAR_BACK_URL => $backurl
              ));
              
              if($requestPermission->check_permission()) {
                $action[] = $requestPermission->get_link(array('class' => 'link'), sprintf('<i class="icofont-lock"></i>&nbsp;%s', $cbThat->lang('PERMISSION_BTN')));
              }

              if($requestAccountGroupEdit->check_permission()) {
                $action[] = $requestAccountGroupEdit->get_link(array('class' => 'link'), sprintf('<i class="icofont-edit"></i>&nbsp;%s', CM_Lang::line('Admin.LIST_ACTION_EDIT')));
              }
              
              if($requestAccountGroupRemove->check_permission()) {
                $action[] = $requestAccountGroupRemove->get_link(array(
                  'class' => 'link',
                  'confirmMsg' => sprintf(modCore::lang('ACCOUNT_GROUP_REMOVE_CONFIRM'), $recordRows[Admin_DAO_AccountGroup::COLUMN_HEADLINE])
                ), sprintf('<i class="icofont-trash"></i>&nbsp;%s', CM_Lang::line('Admin.LIST_ACTION_REMOVE')));
              }
              
              return implode('&nbsp;|&nbsp;', $action);
            }
          ),
          'h_callback' => function($rows, $name) use($cbThat) { //標題列Callback
            return $cbThat->cb_default_sort($rows, $name, $cbThat->get_request('account_group_list'), array(
              'link_params' => $cbThat->_get,
              'sort_column' => array(Admin_DAO_AccountGroup::COLUMN_RECORD_ID, Admin_DAO_AccountGroup::COLUMN_HEADLINE)
            ));
          }
        ))
      )));
    }
    
    public function response_upload_files() {
      
    }
    
    public function response_upload_files_upload() {
      $fileDAO = Admin_Factory::createCMDAO('file');
      $prefix = isset($this->_post['prefix']) && !empty($this->_post['prefix']) ? $this->_post['prefix'] . '/' . date('Ym') : date('Ym');
      
      try {
        $path = Admin::upload_file($_FILES['files'], $prefix);
      } catch(CM_Exception $e) {
        Admin::response_error($e->getMessage());
      }
      
      if(empty($this->_post[Admin_DAO_File::COLUMN_HEADLINE])) $this->_post[Admin_DAO_File::COLUMN_HEADLINE] = $_FILES['files']['name'];

      $insertRows = array_merge($this->_post, array(
        'desc' => '',
        'size' => $_FILES['files']['size'],
        'file' => $path,
        'link' => '0',
        'dao' => strtolower($this->_post['dao'])
      ));
      
	  /*
      if($fileDAO->insert_rows($rMessage, $rReturn, $insertRows, Admin_DAO_File::GROUP_ADD)) {
        CM_Output::json($rMessage, true, array('files' => Admin_Format::to_upload_file_list(array($insertRows))));
      } else {
        Admin::response_error($rMessage);
      }
	  */
	  if($fileDAO->insert_rows($rMessage, $rReturn, $insertRows, Admin_DAO_File::GROUP_ADD)) {
        if(
          isset($this->_post['dao']) && isset($this->_post['column']) && isset($this->_post['rId']) &&
          $this->_post['dao'] == 'Video' &&
          $this->_post['column'] == 'file' &&
          !empty($this->_post['rId'])
        ) {
          CM_DB::query(sprintf("UPDATE `video` SET `file` = '%s' WHERE `id` = %d", $path, $this->_post['rId']));
        } elseif(
          isset($this->_post['dao']) && isset($this->_post['column']) && isset($this->_post['rId']) &&
          $this->_post['dao'] == 'Sound' &&
          $this->_post['column'] == 'file' &&
          !empty($this->_post['rId'])
        ) {
          CM_DB::query(sprintf("UPDATE `sound` SET `file` = '%s' WHERE `id` = %d", $path, $this->_post['rId']));
        }

        CM_Output::json($rMessage, true, array('files' => Admin_Format::to_upload_file_list(array($insertRows))));
      } else {
        Admin::response_error($rMessage);
      }
    }
    
    public function response_upload_files_edit() {
      $isFileInfo = isset($this->_post['record_id']) && !empty($this->_post['record_id']) && isset($this->_post['dao']) && !empty($this->_post['dao']) && isset($this->_post['column']) && !empty($this->_post['column']) ? true : false;

      if($isFileInfo == true) {
        //檔案資訊
        $groupName = Admin_DAO_File_Info::GROUP_EDIT;
        $hiddenColumn = array(
          CM_Html::hidden_field('record_id', $this->_post['record_id']),
          CM_Html::hidden_field('dao', $this->_post['dao']),
          CM_Html::hidden_field('column', $this->_post['column'])
        );
        $dao = Admin_Factory::createCMDAO('file_Info');
        $daoParams = array(
          'rId' => $this->_post['record_id'],
          'dao' => $this->_post['dao'],
          'column' => $this->_post['column']
        );

        if(isset($this->_post['lang']) && !empty($this->_post['lang'])) {
          $daoParams['lang'] = $this->_post['lang'];
          $hiddenColumn[] = CM_Html::hidden_field('lang', $this->_post['lang']);
        }
      } else {
        //檔案
        $groupName = Admin_DAO_File::GROUP_EDIT;
        $hiddenColumn = array();
        $dao = Admin_Factory::createCMDAO('file');
        $daoParams = array();
      }

      if(!$dao->get_rows_byId($fileRows, $groupName, $this->_post['file'], $daoParams)) Admin::response_error(CM_Lang::line('Admin.ERROR_NOT_FOUND'));

      $hiddenColumn[] = CM_Html::hidden_field('file', $this->_post['file']);
      $thisForm = Admin_Factory::createCMForm();
      $thisForm->set_form_dao($dao, $groupName);
      $thisForm->set_form_default($fileRows);
      $form = $this->get_request('upload_files_edit', 'save')->get_form('editForm');

      CM_Output::json('upload_files_edit_ok', true, Admin::format_templates_data(array(
        'mId' => 'uploadFormEdit',
        'mTop' => $form['formTop'],
        'mBottom' => $form['formBottom'],
        'mHeader' => $this->lang('UPLOAD_EDIT_HEADLINE'),
        'mSHeader' => $this->lang('UPLOAD_EDIT_SHEADLINE'),
        'mBody' => implode('', array_map(function($element) {
          return CM_Template::get_tpl_html('form_fields', $element);
        }, $thisForm->get_group_fiedls('info', CM_Form::ACTION_UPDATE))),
        'mButton' => implode('', array_merge(array(
          CM_Html::btn(CM_Lang::line('Admin.BTN_CANCEL'), 'button', array(
            'class' => 'btn',
            'data-dismiss' => 'modal',
            'aria-hidden' => 'true'
          )),
          CM_Html::btn($this->lang('UPLOAD_EDIT_BTN'), 'submit', array(
            'class' => 'btn btn-primary'
          ))
        ), $hiddenColumn))
      )));
    }
    
    public function response_upload_files_edit_save() {
      $isFileInfo = isset($this->_post['record_id']) && !empty($this->_post['record_id']) && isset($this->_post['dao']) && !empty($this->_post['dao']) && isset($this->_post['column']) && !empty($this->_post['column']) ? true : false;

      if($isFileInfo == true) {
        //檔案資訊
        $groupName = Admin_DAO_File_Info::GROUP_EDIT;
        $dao = Admin_Factory::createCMDAO('file_Info');
        $daoParams = array(
          'rId' => $this->_post['record_id'],
          'dao' => $this->_post['dao'],
          'column' => $this->_post['column']
        );

        if(isset($this->_post['lang']) && !empty($this->_post['lang'])) $daoParams['lang'] = $this->_post['lang'];
      } else {
        //檔案
        $groupName = Admin_DAO_File::GROUP_EDIT;
        $dao = Admin_Factory::createCMDAO('file');
        $daoParams = array();
      }
      
      if(!isset($this->_post['file']) || !$dao->update_rows($rMessage, $rReturn, $this->_post, $this->_post['file'], $groupName, $daoParams)) {
        Admin::response_error($rMessage);
      } else {
        $this->log_request($this->_post['file'], $dao->to_column_group_rows($groupName, $this->_post));
        CM_Output::json($rMessage, true, array('rows' => array(
          'name' => $rReturn['currentRows']['name'],
          'desc' => Admin::substr($rReturn['currentRows']['desc'], 80)
        )));
      }
    }

    public function response_upload_files_remove() {
      $isFileInfo = isset($this->_post['record_id']) && !empty($this->_post['record_id']) && isset($this->_post['dao']) && !empty($this->_post['dao']) && isset($this->_post['column']) && !empty($this->_post['column']) ? true : false;
      $dao = Admin_Factory::createCMDAO('file');

      if(!isset($this->_post['file']) || empty($this->_post['file'])) Admin::response_error(CM_Lang::line('Admin.ERROR_NOT_FOUND'));

      if($isFileInfo == true) {
        $daoInfo = Admin_Factory::createCMDAO('file_Info');
        $daoParams = array(
          'rId' => $this->_post['record_id'],
          'dao' => $this->_post['dao'],
          'column' => $this->_post['column']
        );

        if(isset($this->_post['lang']) && !empty($this->_post['lang'])) $daoParams['lang'] = $this->_post['lang'];

        $success = false;

        try {
          CM_DB::transaction_start();

          if($daoInfo->delete_rows($rMessage, $rReturn, $this->_post['file'], $daoParams)) {
            //移除完成後
            $isDelete = false;
            $dao->update_rows($rfMessage, $rfReturn, array(), $this->_post['file'], Admin_DAO_File::GROUP_LINK, array(
              'columnCallback' => function($inputRows, $dataRows) use(&$isDelete) {
                $link = $dataRows['link'] - 1;
                //如果連結數為0，移除原本的檔案庫的檔案
                if($link <= 0) $isDelete = true;

                return array_merge($inputRows, array('link' => $link));
              }
            ));

            if($isDelete == true) $dao->delete_rows($rfMessage, $rfReturn, $this->_post['file']);

            $success = true;
          }
          
          CM_DB::transaction_end();

          if($success == true) {
            CM_Output::json($rMessage, true);
          } else {
            Admin::response_error($rMessage);
          }
        } catch(PDOException $e) {
          CM_DB::transaction_rollback();
          Admin::response_error('移除檔案發生問題，請稍後再試');
        }
      } else {
        /*
        $dao->delete_rows($rMessage, $rReturn, $this->_post['file'], array(
          'checkCallable' => function(&$rMessage, $inputRows, $dataRows) {
            if($dataRows['link'] == 0) {
              return true;
            } else {
              $rMessage = '還有其他關聯';
              return false;
            }
          }
        ));
        */
        CM_Output::json('檔案已經移除', true);
      }
    }
	
	//150407:ANN:新增瀏覽功能
	 public function response_upload_files_preview() {
      $fileNameArray = explode('video/', $this->_post['file']);
      $hiddenColumn = array();
      
      if(!empty($this->_post['record_id']) && !empty($this->_post['dao'])) {
        $dao = Admin_Factory::createCMDAO(strtolower($this->_post['dao']));
        
        if(!empty($fileNameArray[1]) && $dao->get_rows_byId($dataRows, '', $this->_post['record_id'], array()) && $dataRows['process_count'] > 0) {
          $fileTNameArray = explode('.', $fileNameArray[1]);
          $fileName = $fileTNameArray[0] . '_' . $dataRows['process_count'] . '.' . $fileTNameArray[1];
          $hiddenColumn[] = CM_Html::hidden_field('file', $fileName);
          $hiddenColumn[] = CM_Html::hidden_field('streamServer', CM_Conf::get('STREAMSERVER.HOST'));
        }
      }
      $data = array();

      CM_Output::json('upload_files_edit_ok', true, Admin::format_templates_data(array(
        'mId' => 'uploadFormEdit',
        'mHeader' => $this->lang('R_UPLOAD_FILES_PREVIEW'),
        'mSHeader' => $this->lang('R_UPLOAD_FILES_PREVIEW_INTRO'),
        'mBody' => CM_Template::get_tpl_html('upload_preview', $data),
        'mButton' => implode('', array_merge(array(
          CM_Html::btn(CM_Lang::line('Admin.BTN_CANCEL'), 'button', array(
            'class' => 'btn',
            'data-dismiss' => 'modal',
            'aria-hidden' => 'true'
          ))
        ), $hiddenColumn))
      )));
    }

    public function response_configure() {
      if(!is_readable(CM_Conf::get('PATH_F.ADMIN_CONF'))) Admin::response_error(sprintf(CM_Lang::line('Admin.ERROR_PATH_NOT_READABLE'), CM_Conf::get('PATH_F.ADMIN_CONF')));

      $conf = array();
      $catalogOptions = array();
      $isRootUser = Admin::is_root_user();
      //從目錄裡取得設定檔的欄位設定 
      foreach (new DirectoryIterator(CM_Conf::get('PATH_F.ADMIN_CONF')) as $file) {
        if($file->isDot()) continue;

        if($file->isDir()) continue;
        //只接受副檔名為json的設定檔案
        if(Admin::get_file_ext($file->getFilename()) != 'php') continue;

        $path = $file->getPathname();

        if(!is_readable($path)) continue;

        $baseName = $file->getBasename('.php');
        $conf[$baseName] = require($path);
        //不允許更動的設定
        if($isRootUser == false && $conf[$baseName]['allow'] == false) continue;

        $catalogOptions[] = array('id' => $baseName, 'text' => $conf[$baseName]['name']);
      }
      //找不到設定檔的時候
      if(empty($conf)) Admin::response_error(CM_Lang::line('Admin.ERROR_DATA_EMPTY'));

      $this->_breadcrumb->add(self::lang('R_CONFIG'), $this->get_request('configure')->get_link());

      if(isset($this->_get['catalog']) && isset($conf[$this->_get['catalog']])) {
        //其他分類
        $catalog = $this->_get['catalog'];
      } else {
        //沒有選擇的時候預設第一個
        $catalog = array_shift(array_keys($conf));
      }

      $this->_breadcrumb->add(
        $conf[$catalog]['name'],
        $this->get_request('configure')
          ->set_params(array('catalog' => $catalog))
          ->set_text($conf[$catalog]['name'])
          ->get_link()
      );
      $formCatalog = array();

      foreach($catalogOptions as $options) {
        $formCatalog[] = array(
          'current' => $catalog == $options['id'] ? true : false,
          'link' => $this->get_request('configure')
            ->set_params(array('catalog' => $options['id']))
            ->set_text($options['text'])
            ->get_link()
        );
      }
      //預設值
      $defaultRows = array_change_key_case(CM_Conf::export(strtoupper($conf[$catalog]['id']), CASE_LOWER));
      //設定欄位
      $columnArray = array();

      foreach($conf[$catalog]['column'] as $columnName => $columnRows) {
        //判斷是否為允許的設定
        if(!in_array($columnName, $conf[$catalog]['allowcolumn']) && $isRootUser == false) continue; 

        $columnArray[$columnName] = Admin_Factory::createCMColumn($columnName, array_merge(array(
          'label' => $columnRows['name'],
          'f_form_params' => array('tip' => $columnRows['desc'])
        ), isset($columnRows['column']) ? (array)$columnRows['column'] : array()));
      }
      //設定表單群組
      $columnGroupArray = array();
      $columnGroupInfoArray = array();

      foreach($conf[$catalog]['groups'] as $groupName => $groupsRows) {
        $columnGroupArray[$groupName] = $groupsRows['column'];
        $columnGroupInfoArray[$groupName] = array('title' => $groupsRows['name']);
      }
      
      //建立表單物件
      $thisForm = Admin_Factory::createCMForm($columnArray, $columnGroupArray, $defaultRows);
      CM_Output::json('configure_ok', true, Admin::format_templates_data(array_merge(
        $this->get_request('configure', 'save')->get_form('configureForm'),
        array(
          'hiddenColumn' => array(
            CM_Html::hidden_field('catalog', $catalog)
          ),
          'hasLang' => true,
          'formLang' => $formCatalog,
          'formButton' => 
            CM_Html::submit_btn(CM_Lang::line('Admin.BTN_EDIT'), array('class' => 'btn btn-primary')) . '&nbsp;' . 
            CM_Html::reset_btn(CM_Lang::line('Admin.BTN_CANCEL'), array('class' => 'btn prevBtn')) . '&nbsp;' .
            CM_Html::reset_btn(CM_Lang::line('Admin.BTN_RESET'), array('class' => 'btn')),
          'header' => array(
            'icon' => 'icofont-edit',
            'title' => $conf[$catalog]['name'],
            'intro' => $conf[$catalog]['desc'],
            'breadcrumb' => $this->_breadcrumb->trail()
          ),
          'formGroup' => $thisForm->get_groups_form($columnGroupInfoArray)
        )
      )));
    }

    public function response_configure_save() {
      if(!isset($this->_post['catalog'])) Admin::response_error(CM_Lang::line('Admin.ERROR_NOT_FOUND'));

      $confSettingPath = CM_Conf::get('PATH_F.ADMIN_CONF') . $this->_post['catalog'] . '.php';

      if(!is_readable($confSettingPath)) Admin::response_error(sprintf(CM_Lang::line('Admin.ERROR_FILE_NOT_READABLE'), $confSettingPath));

      $isRootUser = Admin::is_root_user();
      $confSetting = require($confSettingPath);
      //不允許更動的設定
      if($isRootUser == false && $confSetting['allow'] == false) Admin::response_error(CM_Lang::line('Admin.ERROR_NOT_FOUND'));

      $confRows = array_change_key_case(CM_Conf::export(strtoupper($this->_post['catalog']), CASE_LOWER));

      foreach($confSetting['column'] as $columnName => $columnRows) {
        //不允許更動的值用原來的值蓋回
        if($isRootUser == false && !in_array($columnName, $confSetting['allowcolumn'])) continue;

        $confRows[$columnName] = $this->_post[$columnName];
      }

      if(CM_Conf::save_conf($rMessage, $confSetting['id'], $confRows, $confSetting['column'], array('factoryColumn' => 'Admin_Factory::createCMColumn'))) {
        CM_Output::json($rMessage, true);
      } else {
        Admin::response_error($rMessage);
      }
    }
    
    public function response_permission() {
      $modArray = array();
      $modProcess = function(Admin_Mod $mod, $modName) {
        $return = array();

        foreach($mod->_request as $requestName => $requestRows) {
          $subRows = array();

          foreach($requestRows['sub'] as $requestSubName => $requestSubRows) {
            $subRows[] = array(
              'name' => $requestSubName,
              'title' => "{$requestRows['title']}&nbsp;-&nbsp;{$requestSubName}",
              'intro' => $requestSubRows['intro']
            );
          }

          $return[] = array(
            'name' => $requestName,
            'title' => $requestRows['title'],
            'intro' => $requestRows['intro'],
            'fields' => Admin_Permission::check($modName, $requestName) ? '<i class="icofont-unlock" style="color: green;"></i>' : '<i class="icofont-lock" style="color: red;"></i>',
            'sub' => $subRows
          );
        }

        return array(
          'name' => $modName,
          'func' => $return
        );
      };

      if(Admin::is_root_user() == true) {
        foreach (new DirectoryIterator(CM_Conf::get('PATH_F.ADMIN_MOD')) as $file) {
          if($file->isDot()) continue;
          //不是資料夾忽略
          if($file->isDir() == false) continue;

          $modName = $file->getFilename();
          $modPath = CM_Conf::get('PATH_F.ADMIN_MOD') . $modName . '/' . $modName . '.mod.php';
          //沒有模組檔案忽略
          if(!is_file($modPath)) continue;
          //核心模組自動排除
          if($modName != self::MODULE_NAME) require($modPath);

          $modArray[] = call_user_func($modProcess, Admin_Factory::createCMModule($modName), $modName);
        }
      } else {
        foreach(CM_Conf::get('ADMIN.MENU_MOD') as $modName) {
          $modPath = CM_Conf::get('PATH_F.ADMIN_MOD') . $modName . '/' . $modName . '.mod.php';
          //沒有模組檔案忽略
          if(!is_file($modPath)) continue;
          //核心模組自動排除
          if($modName != self::MODULE_NAME) require($modPath);

          $modArray[] = call_user_func($modProcess, Admin_Factory::createCMModule($modName), $modName);
        }
      }

      $this->_breadcrumb->add('權限設定', $this->get_request('permission')->get_link());
      CM_Output::json('permission_ok', true, Admin::format_templates_data(array(
        'header' => array(
          'icon' => 'icofont-lock',
          'title' => '權限設定',
          'intro' => '設定使用者或群組的權限',
          'breadcrumb' => $this->_breadcrumb->trail(),
          'func' => array()
        ),
        'list' => $modArray
      )));
    }

    public function response_permission_account() {
      $thisDAO = Admin_Factory::createCMDAO('account');

      if(
        !isset($this->_get[Admin::VAR_RECORD_ID]) ||
        !$thisDAO->get_rows_byId($recordRows, Admin_DAO_Account::GROUP_PERMISSION, $this->_get[Admin::VAR_RECORD_ID])
      ) CM_Output::json(CM_Lang::line('Admin.ERROR_NOT_FOUND'));

      $this->_breadcrumb->add(self::lang('R_PERMISSION_ACCOUNT'), $this->get_request('permission_account')->get_link());
      $form = $this->get_request('permission_account', 'save')->get_form('permissionForm');
      CM_Output::json('permission_account_ok', true, Admin::format_templates_data(array(
        'header' => array(
          'icon' => 'icofont-lock',
          'title' => self::lang('R_PERMISSION_ACCOUNT'),
          'intro' => sprintf(self::lang('PERMISSION_ACCOUNT_INTRO'), $recordRows[Admin_DAO_Account::COLUMN_HEADLINE]),
          'breadcrumb' => $this->_breadcrumb->trail(),
          'func' => array()
        ),
        'lang' => array(
          'permission_column_func' => self::lang('PERMISSION_COLUMN_FUNC'),
          'permission_column_desc' => self::lang('PERMISSION_COLUMN_DESC')
        ),
        'form' => array(
          'top' => $form['formTop'],
          'bottom' => $form['formBottom'],
          'params' => array(
            CM_Html::hidden_field(Admin::VAR_BACK_URL, $this->_get[Admin::VAR_BACK_URL]),
            CM_Html::hidden_field(Admin::VAR_RECORD_ID, $recordRows[Admin_DAO_Account::COLUMN_RECORD_ID])
          )
        ),
        'list' => $this->to_tmpl_permission($recordRows['permission'])
      )));
    }

    public function response_permission_account_save() {
      $thisDAO = Admin_Factory::createCMDAO('account');
      $permissionLine = array();

      if(
        !isset($this->_post[Admin::VAR_RECORD_ID]) ||
        !$thisDAO->get_rows_byId($recordRows, Admin_DAO_Account::GROUP_PERMISSION, $this->_post[Admin::VAR_RECORD_ID])
      ) CM_Output::json(CM_Lang::line('Admin.ERROR_NOT_FOUND'));

      if(!empty($this->_post['permission']) && is_array($this->_post['permission'])) {
        foreach($this->_post['permission'] as $id => $rows) {
          if(isset($rows['_all']) && $rows['_all'] == '1') {
            //全部允許
            $permissionLine[] = $id;
            continue;
          }

          foreach($rows as $subId => $value) {
            if($subId == '_all') continue;

            $permissionLine[] = "{$id}.{$subId}";
          }
        }

        if(!empty($permissionLine)) $permissionLine = implode(',', $permissionLine);
      }

      $updateRows = array(
        Admin_DAO_Account::COLUMN_HEADLINE => $recordRows[Admin_DAO_Account::COLUMN_HEADLINE],
        'permission' => $permissionLine
      );

      if(!$thisDAO->update_rows($returnMessage, $rReturn, $updateRows, $recordRows[Admin_DAO_Account::COLUMN_RECORD_ID], Admin_DAO_Account::GROUP_PERMISSION)) {
        Admin::response_error($returnMessage);
      } else {
        CM_Output::json($returnMessage, true, array('url' => isset($this->_post[Admin::VAR_BACK_URL]) ? Admin::filter_input($this->_post[Admin::VAR_BACK_URL], false) : $this->get_request('account_list')->get_href()));
      }
    }

    public function response_permission_account_group() {
      $thisDAO = Admin_Factory::createCMDAO('accountGroup');

      if(
        !isset($this->_get[Admin::VAR_RECORD_ID]) ||
        !$thisDAO->get_rows_byId($recordRows, Admin_DAO_AccountGroup::GROUP_PERMISSION, $this->_get[Admin::VAR_RECORD_ID])
      ) CM_Output::json(CM_Lang::line('Admin.ERROR_NOT_FOUND'));

      $this->_breadcrumb->add(self::lang('R_PERMISSION_ACCOUNT_GROUP'), $this->get_request('permission_account_group')->get_link());
      $form = $this->get_request('permission_account_group', 'save')->get_form('permissionForm');
      CM_Output::json('permission_account_ok', true, Admin::format_templates_data(array(
        'header' => array(
          'icon' => 'icofont-lock',
          'title' => self::lang('R_PERMISSION_ACCOUNT_GROUP'),
          'intro' => sprintf(self::lang('PERMISSION_ACCOUNT_GROUP_INTRO'), $recordRows[Admin_DAO_Account::COLUMN_HEADLINE]),
          'breadcrumb' => $this->_breadcrumb->trail(),
          'func' => array()
        ),
        'lang' => array(
          'permission_column_func' => self::lang('PERMISSION_COLUMN_FUNC'),
          'permission_column_desc' => self::lang('PERMISSION_COLUMN_DESC')
        ),
        'form' => array(
          'top' => $form['formTop'],
          'bottom' => $form['formBottom'],
          'params' => array(
            CM_Html::hidden_field(Admin::VAR_BACK_URL, $this->_get[Admin::VAR_BACK_URL]),
            CM_Html::hidden_field(Admin::VAR_RECORD_ID, $recordRows[Admin_DAO_Account::COLUMN_RECORD_ID])
          )
        ),
        'list' => $this->to_tmpl_permission($recordRows['permission'])
      )));
    }

    public function response_permission_account_group_save() {
      $thisDAO = Admin_Factory::createCMDAO('accountGroup');
      $permissionLine = array();

      if(
        !isset($this->_post[Admin::VAR_RECORD_ID]) ||
        !$thisDAO->get_rows_byId($recordRows, Admin_DAO_AccountGroup::GROUP_PERMISSION, $this->_post[Admin::VAR_RECORD_ID])
      ) CM_Output::json(CM_Lang::line('Admin.ERROR_NOT_FOUND'));

      if(!empty($this->_post['permission']) && is_array($this->_post['permission'])) {
        foreach($this->_post['permission'] as $id => $rows) {
          if(isset($rows['_all']) && $rows['_all'] == '1') {
            //全部允許
            $permissionLine[] = $id;
            continue;
          }

          foreach($rows as $subId => $value) {
            if($subId == '_all') continue;

            $permissionLine[] = "{$id}.{$subId}";
          }
        }

        if(!empty($permissionLine)) $permissionLine = implode(',', $permissionLine);
      }

      $updateRows = array(
        Admin_DAO_AccountGroup::COLUMN_HEADLINE => $recordRows[Admin_DAO_AccountGroup::COLUMN_HEADLINE],
        'permission' => $permissionLine
      );

      if(!$thisDAO->update_rows($returnMessage, $rReturn, $updateRows, $recordRows[Admin_DAO_AccountGroup::COLUMN_RECORD_ID], Admin_DAO_AccountGroup::GROUP_PERMISSION)) {
        Admin::response_error($returnMessage);
      } else {
        CM_Output::json($returnMessage, true, array('url' => isset($this->_post[Admin::VAR_BACK_URL]) ? Admin::filter_input($this->_post[Admin::VAR_BACK_URL], false) : $this->get_request('account_group_list')->get_href()));
      }
    }

    public function response_log() {
      $cbThat = $this;
      $this->_breadcrumb->add(self::lang('R_LOG'), $this->get_request('log')->get_link());
      //設定條件搜尋
      if(
        Admin::not_null($this->_get['keyword']) ||
        Admin::not_null($this->_get['start_date']) ||
        Admin::not_null($this->_get['end_date'])
      ) $this->_breadcrumb->add(CM_Lang::line('Admin.LIST_SEARCH'), $this->get_request('log')->set_params(Admin::get_all_get_params(array(), true))->get_link());

      $startDate = Admin::not_null($this->_get['start_date']) ? $this->_get['start_date'] : '1970-01-01';
      $endDate = Admin::not_null($this->_get['end_date']) ? $this->_get['end_date'] : date('Y-m-d');
      //帳號列表
      $thisDAO = Admin_Factory::createCMDAO('adminLog');
      $daoParams = array(
        'sort' => Admin_Mod::to_sort($thisDAO, $this->_get['sc'], $this->_get['sf']),
        'account' => $this->_get['account'],
        'keyword' => $this->_get['keyword'],
        'date' => array(
          'start' => $startDate,
          'end' => $endDate
        )
      );
      $listColumn = array_merge($thisDAO->get_column_field_label_array(Admin_DAO_AdminLog::GROUP_LIST), array(
        'action' => CM_Lang::line('Admin.LIST_ACTION')
      ));
      $listJSON = $thisDAO->get_array_object(Admin_DAO_AdminLog::GROUP_LIST, array_merge($daoParams, array(
        'keyColumn' => Admin_DAO_AdminLog::COLUMN_RECORD_ID,
        'oNowPage' => isset($this->_get['p']) ? (int)$this->_get['p'] : 1,
        'oPageCallback' => function($page, array $params, $key) use ($cbThat) {
          return $cbThat->cb_default_pagesplit($page, $params, $key, $cbThat->get_request('log'), array('vpage' => 'p'));
        }
      )));
      CM_Output::json('list_ok', true, Admin::format_templates_data(array(
        'header' => array(
          'icon' => 'icofont-th-list',
          'title' => self::lang('R_LOG'),
          'intro' => self::lang('R_LOG_INTRO'),
          'breadcrumb' => $this->_breadcrumb->trail()
        ),
        'list' => Admin_Mod::to_tmpl_table_list($listJSON, $listColumn, array(
          'id' => 'logList',
          'title' => self::lang('ACCOUNT_LIST_ACCOUNT_TITLE'),
          'action' => array(
            'checkboxFunc' => array(
              'title' => CM_Lang::line('Admin.LIST_ACTION_MENU'),
              'list' => array(
                array(
                  'link' => $this->get_request('log', 'remove')->get_href(),
                  'text' => CM_Lang::line('Admin.LIST_ACTION_ID_REMOVE'),
                  'confirm' => CM_lang::line('Admin.LIST_ACTION_ID_REMOVE_CONFIRM')
                )
              )
            ),
            'listFunc' => array(
              $this->get_request('log', 'export')
                ->set_text(sprintf('<i class="icofont-share"></i>&nbsp;%s', self::lang('LOG_EXPORT_BTN')))
                ->set_params($this->_get)
                ->get_btn(array('class' => 'btn')),
              $this->get_request('log', 'remove_search')
                ->set_text(sprintf('<i class="icofont-trash"></i>&nbsp;%s', self::lang('R_LOG_REMOVE_SEARCH')))
                ->get_btn(array('class' => 'btn btn-danger'))
            ),
            'search' => array_merge(array(
              'hiddenColumn' => array(),
              'formSearch' => 
                sprintf('<label>%s&nbsp;</label>', CM_Html::input_field('account', $this->_get['account'], sprintf('class="input-small" placeholder="%s"', self::lang('LOG_SEARCH_ACCOUNT')))) .
                sprintf('<label>%s&nbsp;</label>', CM_Html::input_field('keyword', $this->_get['keyword'], sprintf('class="input-small" placeholder="%s"', self::lang('LOG_SEARCH_KEYWORD')))) .
                sprintf('<label>%s&nbsp;%s&nbsp;</label>', self::lang('LOG_SEARCH_DATE_START'), CM_Html::input_field('start_date', $this->_get['start_date'], 'class="hasDatepicker input-small"')) .
                sprintf('<label>%s&nbsp;%s</label>', self::lang('LOG_SEARCH_DATE_END'), CM_Html::input_field('end_date', $this->_get['end_date'], 'class="hasDatepicker input-small"')),
              'formButton' => CM_Html::submit_btn('<i class="icofont-search"></i>&nbsp;' . CM_Lang::line('Admin.BTN_SEARCH'), array('class' => 'btn'))
            ), $this->get_request('log')->get_form('searchForm', array('method' => 'get', 'class' => 'form-inline')))
          ),
          'isSearch' => Admin::not_null($this->_get['keyword']),
          'callback' => array(
            Admin_DAO_AdminLog::COLUMN_HEADLINE => function($record, $recordRows = array()) use($cbThat) {
              return CM_Format::to_fix_length_text($record, 15);
            },
            'action' => function($record, $recordRows = array()) use($cbThat) {
              $action = array();
              //權限
              $requestLog = $cbThat->get_request('log', 'detail')->set_params(array(
                Admin::VAR_RECORD_ID => $recordRows[Admin_DAO_AdminLog::COLUMN_RECORD_ID]
              ));

              if($requestLog->check_permission()) {
                $action[] = $requestLog->get_link(array('class' => 'link'), sprintf('<i class="icofont-eye-open"></i>&nbsp;%s', CM_Lang::line('Admin.LIST_ACTION_DETAIL')));
              }
              
              return implode('&nbsp;|&nbsp;', $action);
            }
          ),
          'h_callback' => function($rows, $name) use($cbThat) {
            if($name == 'action') $rows['attr'] = 'width="50"';

            return $cbThat->cb_default_sort($rows, $name, $cbThat
              ->get_request('log'), array(
                'link_params' => $cbThat->_get,
                'sort_column' => array(Admin_DAO_AdminLog::COLUMN_RECORD_ID, Admin_DAO_AdminLog::COLUMN_HEADLINE, 'account_id', 'request_name', 'account_name', 'create_date')
              )
            );
          }
        ))
      )));
    }

    public function response_log_detail() {
      $thisDAO = Admin_Factory::createCMDAO('adminLog');

      if(!isset($this->_get[Admin::VAR_RECORD_ID]) || !$thisDAO->get_rows_byId($recordRows, Admin_DAO_AdminLog::GROUP_DETAIL, $this->_get[Admin::VAR_RECORD_ID])) Admin::response_error(CM_Lang::line('Admin.ERROR_NOT_FOUND'));

      $thisForm = Admin_Factory::createCMForm();
      $thisForm->set_form_dao($thisDAO, Admin_DAO_AdminLog::GROUP_DETAIL);
      $thisForm->set_form_default($recordRows);

      CM_Output::json('cm_forget_password_ok', true, Admin::format_templates_data(array(
        'mId' => 'logDetail',
        'mTop' => '',
        'mBottom' => '',
        'mHeader' => self::lang('LOG_DETAIL_TITLE'),
        'mSHeader' => sprintf(self::lang('LOG_DETAIL_TITLE_S'), $recordRows[Admin_DAO_AdminLog::COLUMN_RECORD_ID]),
        'mButton' => 
          CM_Html::btn(CM_Lang::line('Admin.BTN_CANCEL'), 'button', array(
            'class' => 'btn',
            'data-dismiss' => 'modal',
            'aria-hidden' => 'true'
          )),
        'mData' => array(
          'intro' => '',
          'fields' => $thisForm->get_form(CM_Form::ACTION_DISPLAY)
        )
      )));
    }

    public function response_log_remove() {
      if(!isset($this->_post['listActionId']) || empty($this->_post['listActionId'])) Admin::response_error(CM_Lang::line('Admin.LIST_ACTION_ID_EMPTY'));

      $thisDAO = Admin_Factory::createCMDAO('adminLog');
      $errorMessage = array();
      
      foreach($this->_post['listActionId'] as $index => $id) {
        if(!$thisDAO->delete_rows($rMessage, $rReturn, $id)) $errorMessage[] = $rMessage;
      }
      
      if(empty($errorMessage)) {
        Admin::response_success(self::lang('LOG_REMOVE_L_SUCCESS'), true);
      } else {
        Admin::response_error(implode('<br />', $errorMessage));
      }
    }

    public function response_log_remove_search() {
      $logRemoveSearchForm = $this->get_request('log', 'remove_search_process')->get_form('logRemoveSearchForm');
      CM_Output::json('log_remove_search_ok', true, Admin::format_templates_data(array(
        'mId' => 'logRemoveSearchForm',
        'mTop' => $logRemoveSearchForm['formTop'],
        'mBottom' => $logRemoveSearchForm['formBottom'],
        'mHeader' => self::lang('LOG_REMOVE_SEARCH'),
        'mSHeader' => self::lang('LOG_REMOVE_SEARCH_S'),
        'mBody' => 
          '<div class="form-inline">' .
          sprintf('<label>%s&nbsp;%s&nbsp;</label>', self::lang('LOG_SEARCH_DATE_START'), CM_Html::input_field('start_date', date('Y-m-d', strtotime('now -90 day')), 'data-validate=\'{"required": true}\'class="hasDatepicker input-small"')) .
          sprintf('<label>%s&nbsp;%s</label>', self::lang('LOG_SEARCH_DATE_END'), CM_Html::input_field('end_date', date('Y-m-d'), 'data-validate=\'{"required": true}\' class="hasDatepicker input-small"')) .
          '</div>',
        'mButton' => 
          CM_Html::submit_btn(self::lang('LOG_REMOVE_SEARCH_BTN'), array(
            'class' => 'btn btn-primary'
          )) .
          CM_Html::btn(CM_Lang::line('Admin.BTN_CANCEL'), 'button', array(
            'class' => 'btn',
            'data-dismiss' => 'modal',
            'aria-hidden'=> 'true'
          ))
      )));
    }

    public function response_log_remove_search_process() {
      $daoParams = array(
        'date' => array(
          'start' => $this->_post['start_date'],
          'end' => $this->_post['end_date']
        )
      );
      $thisDAO = Admin_Factory::createCMDAO('adminLog');


      if($thisDAO->get_array($recordArray, Admin_DAO_AdminLog::GROUP_LIST, $daoParams)) {
        $errorMessage = array();
        $errorCount = 0;
        $successCount = 0;

        foreach($recordArray as $recordRows) {
          if(!$thisDAO->delete_rows($rMessage, $rReturn, $recordRows[Admin_DAO_AdminLog::COLUMN_RECORD_ID])) {
            $errorMessage[] = $rMessage;
            $errorCount ++; 
          }

          $successCount ++;
        }

        if(empty($errorMessage)) {
          $message = sprintf($this->lang('LOG_REMOVE_SUCCESS_1'), $successCount);
        } else {
          $message = sprintf($this->lang('LOG_REMOVE_SUCCESS_2'), $successCount, $errorCount, implode('<br />', $errorMessage));
        }

        CM_Output::json($message, true);
      } else {
        Admin::response_error($this->lang('LOG_REMOVE_ERROR'));
      }
    }

    public function response_log_export() {
      $cbThat = $this;
      $thisDAO = Admin_Factory::createCMDAO('adminLog');
      $startDate = Admin::not_null($this->_get['start_date']) ? $this->_get['start_date'] : '1970-01-01';
      $endDate = Admin::not_null($this->_get['end_date']) ? $this->_get['end_date'] : date('Y-m-d');
      $daoParams = array(
        'account' => $this->_get['account'],
        'keyword' => $this->_get['keyword'],
        'date' => array(
          'start' => $startDate,
          'end' => $endDate
        )
      );
      $listColumn = $thisDAO->get_column_field_label_array(Admin_DAO_AdminLog::GROUP_LIST);
      $listColumnFormat = array();
      
      header('Content-type:text/html; charset=utf-8');
      header('Content-Type: application/octet-stream');
      header("Content-Disposition: attachment; filename=log_export.xls;");
      echo '<HTML xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40">';
      echo '<head><meta http-equiv="content-type" content="application/vnd.ms-excel; charset=UTF-8"></head>';
      echo '<body><table border="1">';
      echo '<tr>';
      
      foreach($listColumn as $columnName => $columnLabel) echo "<td>{$columnLabel}</td>";
      
      echo '</tr>';
      
      if($thisDAO->get_array($recordArray, Admin_DAO_AdminLog::GROUP_LIST, $daoParams)) {
        $recordArray = CM_Format::to_list_array_format($recordArray, $listColumn, $listColumnFormat);
        
        foreach($recordArray as $recordRows) {
          echo '<tr>';
          
          foreach($listColumn as $columnName => $columnLabel) echo "<td>{$recordRows[$columnName]}</td>";
          
          echo '</tr>';
        }
      }
      
      echo '</table></body></html>';
      exit;
    }

    public function response_file_add($responseParams = array()) {
      $responseParams = array_merge(array(
        'pop' => false //跳出模式
      ), $responseParams);
      $data = array();

      if($responseParams['pop'] == true) {
        $this->_pop_mod = true;
        $cancalBtn = $this->get_request('file')
          ->set_text(CM_Lang::line('Admin.BTN_CANCEL'))
          ->get_btn(array('class' => 'btn'));
      } else {
        $this->_breadcrumb->add(self::lang('R_FILE'), $this->get_request('file')->get_link());
        $this->_breadcrumb->add(self::lang('R_FILE_ADD'), $this->get_request('file_add')->get_link());
        $data['header'] = array(
          'icon' => 'icofont-edit',
          'title' => self::lang('R_FILE_ADD'),
          'intro' => self::lang('R_FILE_ADD_INTRO'),
          'breadcrumb' => $this->_breadcrumb->trail()
        );
        $cancalBtn = CM_Html::reset_btn(CM_Lang::line('Admin.BTN_CANCEL'), array('class' => 'btn prevBtn'));
      }

      $thisDAO = Admin_Factory::createCMDAO('file_Editor');
      $thisForm = Admin_Factory::createCMForm();
      $thisForm->set_form_dao($thisDAO, Admin_DAO_File_Editor::GROUP_ADD);
      $thisForm->set_form_default(array('permission' => 2));
      $form = $this->get_request('file_add', 'save')->get_form('addForm');
      $data['formButton'] = implode('&nbsp;', array(
        CM_Html::submit_btn(CM_Lang::line('Admin.BTN_ADD'), array('class' => 'btn btn-primary')),
        $cancalBtn,
        CM_Html::reset_btn(CM_Lang::line('Admin.BTN_RESET'), array('class' => 'btn'))
      ));
      $data['hiddenColumn'] = array(
        CM_Html::hidden_field(Admin::VAR_BACK_URL, $this->_get[Admin::VAR_BACK_URL])
      );
      $data['formGroup'] = $thisForm->get_groups_form(array(
        'info' => array('title' => self::lang('FILE_FORM_GROUP_INFO'))
      ), CM_Form::ACTION_INSERT);
      CM_Output::json('add_form_ok', true, Admin::format_templates_data(array_merge($form, $data)));
    }
    
    public function response_file_add_save() {
      $thisDAO = Admin_Factory::createCMDAO('file_Editor');
      $this->_post['owner'] = $this->_session[Admin::VAR_ADMIN_ID];

      if(!$thisDAO->insert_rows($rMessage, $rReturn, $this->_post, Admin_DAO_File_Editor::GROUP_ADD)) {
        Admin::response_error($rMessage);
      } else {
        $this->log_request($this->_post[Admin_DAO_File_Editor::COLUMN_HEADLINE], $thisDAO->to_column_group_rows(Admin_DAO_File_Editor::GROUP_ADD, $this->_post));
        CM_Output::json($rMessage, true, array('url' => isset($this->_post[Admin::VAR_BACK_URL]) ? Admin::filter_input($this->_post[Admin::VAR_BACK_URL], false) : $this->get_request('file')->get_href()));
      }
    }
    
    public function response_file_edit($responseParams = array()) {
      $responseParams = array_merge(array(
        'pop' => false //跳出模式
      ), $responseParams);
      $thisDAO = Admin_Factory::createCMDAO('file_Editor');
      $data = array();

      if(!isset($this->_get[Admin::VAR_RECORD_ID]) || !$thisDAO->get_rows_byId($recordRows, Admin_DAO_File_Editor::GROUP_EDIT, $this->_get[Admin::VAR_RECORD_ID], array('accountId' => $this->_session[Admin::VAR_ADMIN_ID]))) Admin::response_error(CM_Lang::line('Admin.ERROR_NOT_FOUND'));

      if($responseParams['pop'] == true) {
        $this->_pop_mod = true;
        $cancalBtn = $this->get_request('file')
          ->set_text(CM_Lang::line('Admin.BTN_CANCEL'))
          ->get_btn(array('class' => 'btn'));
      } else {
        $this->_breadcrumb->add(self::lang('R_FILE'), $this->get_request('file')->get_link());
        $this->_breadcrumb->add(self::lang('R_FILE_EDIT'), $this->get_request('file_edit')->get_link());
        $data['header'] = array(
          'icon' => 'icofont-edit',
          'title' => self::lang('R_FILE_EDIT'),
          'intro' => self::lang('FILE_EDIT_INTRO'),
          'breadcrumb' => $this->_breadcrumb->trail()
        );
        $cancalBtn = CM_Html::reset_btn(CM_Lang::line('Admin.BTN_CANCEL'), array('class' => 'btn prevBtn'));
      }
      
      $thisForm = Admin_Factory::createCMForm();
      $thisForm->set_form_dao($thisDAO, Admin_DAO_File_Editor::GROUP_EDIT);
      $thisForm->set_form_default($recordRows);
      $form = $this->get_request('file_edit', 'save')->set_params(array(
        Admin::VAR_RECORD_ID => $recordRows[Admin_DAO_File_Editor::COLUMN_RECORD_ID]
      ))->get_form('editForm');

      $data['hiddenColumn'] = array(
        CM_Html::hidden_field(Admin::VAR_BACK_URL, $this->_get[Admin::VAR_BACK_URL]),
        CM_Html::hidden_field(Admin_DAO_File_Editor::COLUMN_RECORD_ID, $recordRows[Admin_DAO_File_Editor::COLUMN_RECORD_ID])
      );
      $data['formButton'] = implode('&nbsp;', array(
        CM_Html::submit_btn(CM_Lang::line('Admin.BTN_EDIT'), array('class' => 'btn btn-primary')),
        $cancalBtn,
        CM_Html::reset_btn(CM_Lang::line('Admin.BTN_RESET'), array('class' => 'btn'))
      ));
      $data['formGroup'] = $thisForm->get_groups_form(array(
        'info' => array('title' => self::lang('FILE_FORM_GROUP_INFO'))
      ), CM_Form::ACTION_UPDATE);
      CM_Output::json('update_form_ok', true, Admin::format_templates_data(array_merge($form, $data)));
    }
    
    public function response_file_edit_save() {
      $thisDAO = Admin_Factory::createCMDAO('file_Editor');
      $params = array(
        'daoParams' => array('accountId' => $this->_session[Admin::VAR_ADMIN_ID])
      );

      if(!isset($this->_get[Admin::VAR_RECORD_ID]) || !$thisDAO->update_rows($rMessage, $rReturn, $this->_post, $this->_get[Admin::VAR_RECORD_ID], Admin_DAO_File_Editor::GROUP_EDIT, $params)) {
        Admin::response_error($rMessage);
      } else {
        $this->log_request($this->_post[Admin_DAO_File_Editor::COLUMN_HEADLINE], $thisDAO->to_column_group_rows(Admin_DAO_File_Editor::GROUP_EDIT, $this->_post));
        CM_Output::json($rMessage, true, array('url' => isset($this->_post[Admin::VAR_BACK_URL]) ? Admin::filter_input($this->_post[Admin::VAR_BACK_URL], false) : $this->get_request('file')->get_href()));
      }
    }

    public function response_file_remove() {
      $thisDAO = Admin_Factory::createCMDAO('file_Editor');
      $params = array('daoParams' => array());

      if(Admin::is_root_user() != true) $params['daoParams']['permissionOwner'] = $this->_session[Admin::VAR_ADMIN_ID];

      if(!isset($this->_get[Admin::VAR_RECORD_ID]) || !$thisDAO->delete_rows($rMessage, $rReturn, $this->_get[Admin::VAR_RECORD_ID], $params)) {
        Admin::response_error($rMessage);
      } else {
        $this->log_request($rReturn['currentRows'][Admin_DAO_File_Editor::COLUMN_HEADLINE], $rReturn['currentRows']);
        CM_Output::json($rMessage, true, array('url' => isset($_GET[Admin::VAR_BACK_URL]) ? Admin::filter_input($_GET[Admin::VAR_BACK_URL], false) : $this->get_request('file')->get_href()));
      }
    }
    
    public function response_file_remove_list() {
      $thisDAO = Admin_Factory::createCMDAO('file_Editor');
      $params = array('daoParams' => array());

      if(Admin::is_root_user() != true) $params['daoParams']['permissionOwner'] = $this->_session[Admin::VAR_ADMIN_ID];

      if(!isset($this->_post['listActionId']) || empty($this->_post['listActionId'])) Admin::response_error(CM_Lang::line('Admin.LIST_ACTION_ID_EMPTY'));

      $errorMessage = array();
      $error = false;
      $log = array();
      
      foreach($this->_post['listActionId'] as $index => $id) {
        if(!$thisDAO->delete_rows($rMessage, $rReturn, $id, $params)) {
          $error = true;
          $errorMessage[] = $rMessage;
        } else {
          $log[] = implode('<br />', array_map(function($element) {
            return sprintf('<b>%s</b>: %s', $element['id'], $element['text']);
          }, array(
            array('id' => Admin_DAO_File_Editor::COLUMN_RECORD_ID, 'text' => $id),
            array('id' => Admin_DAO_File_Editor::COLUMN_HEADLINE, 'text' => $rReturn['currentRows'][Admin_DAO_File_Editor::COLUMN_HEADLINE]),
          )));
        }
      }
      
      if($error == true) {
        Admin::response_error(implode('<br />', $errorMessage));
      } else {
        $this->log_request(self::lang('R_FILE_REMOVE_L_INTRO'), array('remove' => implode('<br /><br />', $log)));
        CM_Output::json(self::lang('FILE_REMOVE_L_SUCCESS'), true, array('url' => isset($_GET[Admin::VAR_BACK_URL]) ? Admin::filter_input($_GET[Admin::VAR_BACK_URL], false) : urlencode($this->get_request('pop_file')->get_href())));
      }
    }
    
    public function response_file($responseParams = array()) {
      $responseParams = array_merge(array(
        'pop' => false, //跳出模式
        'multiple' => false //選擇多個檔案
      ), $responseParams);
      $data = array();
      $cbThat = $this;
      $thisDAO = Admin_Factory::createCMDAO('file_Editor');
      $listFunc = array();

      if($responseParams['pop'] == true) {
        $this->_pop_mod = true;

        if($responseParams['multiple'] == true) $listFunc[] = CM_Html::func_btn(sprintf('<i class="icofont-plus"></i>&nbsp;', $this->lang('FILE_IMPORT_M')), array('class' => 'btn btn-success', 'data-cmfunc' => 'import-files'));
      } else {
        $this->_breadcrumb->add(self::lang('R_FILE'), $this->get_request('file')->get_link());
      
        if(isset($this->_get['keyword']) && !empty($this->_get['keyword'])) Admin_Mod::set_breadcrumb_keyword($this->_breadcrumb, $this->get_request('file'), $this->_get['keyword']);

        $data['header'] = array(
          'icon' => 'icofont-th-list',
          'title' => self::lang('R_FILE'),
          'intro' => self::lang('R_FILE_INTRO')
        );
        $data['breadcrumb'] = $this->_breadcrumb->trail();
      }

      $listFunc[] = $this->get_request('file_add')
        ->set_params(array(
          Admin::VAR_BACK_URL => urlencode($this->get_request('file')->get_href($this->_get))
        ))
        ->set_text_format('<i class="icofont-plus"></i>&nbsp;%s')
        ->get_btn(array('class' => 'btn btn-primary'));

      $listColumn = array_merge($thisDAO->get_column_field_label_array(Admin_DAO_File_Editor::GROUP_LIST), array(
        'action' => CM_Lang::line('Admin.LIST_ACTION')
      ));
      $daoParams = array(
        'keyword' => $this->_get['keyword'],
        'sort' => Admin_Mod::to_sort($thisDAO, $this->_get['sc'], $this->_get['sf'])
      );

      if(Admin::is_root_user() != true) $daoParams['permissionOwner'] = $this->_session[Admin::VAR_ADMIN_ID];

      $listJSON = $thisDAO->get_array_object(Admin_DAO_File_Editor::GROUP_LIST, array_merge($daoParams, array(
        'keyColumn' => Admin_DAO_File_Editor::COLUMN_RECORD_ID,
        'fileCallback' => true,
        'oNowPage' => isset($this->_get['p']) ? (int)$this->_get['p'] : 1,
        'oPageCallback' => function($page, array $params, $key) use($cbThat) {
          return Admin_Mod::cb_default_pagesplit($page, $params, $key, $cbThat->get_request('file'), array('vpage' => 'p'));
        }
      )));
      $ownerCache = array();
      $ownerDAO = Admin_Factory::createCMDAO('account');
      $data['list'] = Admin_Mod::to_tmpl_table_list($listJSON, $listColumn, array(
        'id' => "{$this->_current_request['mod']}List",
        'title' => CM_Lang::line('Admin.LIST_ACTION_MENU'),
        'action' => array(
          'checkboxFunc' => array(
            'title' => CM_Lang::line('Admin.LIST_ACTION_MENU'),
            'list' => array(
              array(
                'link' => $this->get_request('file_remove', 'list')->get_href(),
                'text' => CM_Lang::line('Admin.LIST_ACTION_ID_REMOVE'),
                'confirm' => CM_lang::line('Admin.LIST_ACTION_ID_REMOVE_CONFIRM')
              )
            )
          ),
          'listFunc' => $listFunc,
          'search' => array_merge(array(
            'hiddenColumn' => array(),
            'formSearch' => CM_Html::input_field('keyword', $this->_get['keyword'], sprintf('placeholder="%s"', CM_Lang::line('Admin.SEARCH_FIELD_PLACEHOLDER'))),
            'formButton' => CM_Html::submit_btn('<i class="icofont-search"></i>&nbsp;' . CM_Lang::line('Admin.BTN_SEARCH'), array('class' => 'btn'))
          ), $this->get_request('file')->get_form('searchForm', array('method' => 'get', 'class' => 'form-inline')))
        ),
        'isSearch' => Admin::not_null($this->_get['keyword']),
        'callback' => array(
          Admin_DAO_File_Editor::COLUMN_RECORD_ID => function($record, $recordRows = array()) use($responseParams) {
            if($responseParams['pop'] == false) return $record;

            if(Admin::not_empty_array($recordRows['file'])) {
              $recordRows['file'] = array_shift($recordRows['file']);
              $recordRows['url'] = CM_Conf::get('PATH_W.UPLOAD') . $recordRows['file'];
            } else {
              $recordRows['file'] = '';
            }

            return sprintf('<span data-cmdata=\'%s\'>%s</span>', Admin::json_encode($recordRows), $record);
          },
          'file' => function($record, $recordRows = array()) {
            return Admin_Format::to_upload_file_thumbnail('file', $recordRows);
          },
          'permission' => function($record, $recordRows = array()) use($thisDAO) {
            return Admin::get_options_name($record, $thisDAO->get_options('permission'));
          },
          'owner' => function($record, $recordRows = array()) use(&$ownerCache, $ownerDAO) {
            if(!isset($ownerCache[$record])) $ownerCache[$record] = $ownerDAO->get_rows_byId($rows, '', $record) ? $rows[Admin_DAO_Account::COLUMN_RECORD_ID] . ' - ' . $rows[Admin_DAO_Account::COLUMN_HEADLINE] : '';

            return $ownerCache[$record];
          },
          'action' => function($record, $recordRows = array()) use($cbThat, $responseParams) {
            $action = array();

            if($responseParams['pop'] == true && $responseParams['multiple'] == false) {
              $action[] = sprintf('<a data-cmfunc="import-file" class="link" href="javascript:;"><i class="icofont-plus"></i>&nbsp;%s</a>', $cbThat->lang('FILE_IMPORT'));
            }

            //修改或刪除回來的網址
            $backurl = urlencode($cbThat->get_request('file')->get_href(Admin::get_all_get_params(array(Admin::VAR_RECORD_ID, Admin::VAR_ACTION), true)));
            //修改
            $thisRequestEdit = $cbThat->get_request('file_edit')->set_params(array(
              Admin::VAR_RECORD_ID => $recordRows[Admin_DAO_File_Editor::COLUMN_RECORD_ID],
              Admin::VAR_BACK_URL => $backurl
            ));
            //刪除
            $thisRequestRemove = $cbThat->get_request('file_remove')->set_params(array(
              Admin::VAR_RECORD_ID => $recordRows[Admin_DAO_File_Editor::COLUMN_RECORD_ID],
              Admin::VAR_BACK_URL => $backurl
            ));
            
            if($thisRequestEdit->check_permission()) {
              $action[] = $thisRequestEdit->get_link(array('class' => 'link'), sprintf('<i class="icofont-edit"></i>&nbsp;%s', CM_Lang::line('Admin.LIST_ACTION_EDIT')));
            }
            
            if($thisRequestRemove->check_permission()) {
              $action[] = $thisRequestRemove->get_link(array(
                'class' => 'link',
                'confirmMsg' => sprintf($cbThat->lang('FILE_REMOVE_CONFIRM'), $recordRows[Admin_DAO_File_Editor::COLUMN_HEADLINE])
              ), sprintf('<i class="icofont-trash"></i>&nbsp;%s', CM_Lang::line('Admin.LIST_ACTION_REMOVE')));
            }
            
            return implode('&nbsp;|&nbsp;', $action);
          }
        ),
        'h_callback' => function($rows, $name) use($cbThat) {
          return Admin_Mod::cb_default_sort($rows, $name, $cbThat->get_request('file'), array(
            'link_params' => $cbThat->_get,
            'sort_column' => array(Admin_DAO_File_Editor::COLUMN_RECORD_ID, 'file', Admin_DAO_File_Editor::COLUMN_HEADLINE, 'permission', 'owner', 'create_date')
          ));
        }
      ));
      
      CM_Output::json('list_ok', true, Admin::format_templates_data($data));
    }

    public function response_pop_file_add() {
      $this->response_file_add(array('pop' => true));
    }

    public function response_pop_file_add_save() {
      $this->response_file_add_save();
    }

    public function response_pop_file_edit() {
      $this->response_file_edit(array('pop' => true));
    }

    public function response_pop_file_edit_save() {
      $this->response_file_edit_save();
    }

    public function response_pop_file_remove() {
      $this->response_file_remove();
    }

    public function response_pop_file_remove_list() {
      $this->response_file_remove_list();
    }

    public function response_pop_file() {
      $this->response_file(array('pop' => true));
    }

    public function response_admin_shortcut_add() {
      $thisDAO = Admin_Factory::createCMDAO('adminShortcut');
      $thisForm = Admin_Factory::createCMForm();
      $thisForm->set_form_dao($thisDAO, Admin_DAO_Account::GROUP_ADD);
      $formArray = $thisForm->get_group_form('info', array(), CM_Form::ACTION_INSERT);
      $form = $this->get_request('admin_shortcut_add', 'save')->get_form('addForm');
      CM_Output::json('add_form_ok', true, Admin::format_templates_data(array(
        'mId' => 'addShortcutForm',
        'mTop' => $form['formTop'],
        'mBottom' => $form['formBottom'],
        'mHeader' => self::lang('R_ADMIN_SHORTCUT_ADD'),
        'mSHeader' => self::lang('R_ADMIN_SHORTCUT_ADD_INTRO'),
        'mData' => array(
          'intro' => self::lang('ADMIN_SHORTCUT_FORM_GROUP_INFO'),
          'fields' => $formArray['fields']
        ),
        'mButton' => 
          CM_Html::hidden_field('url', $this->_get['url']) .
          CM_Html::submit_btn(CM_Lang::line('Admin.BTN_SUBMIT'), array(
            'class' => 'btn btn-primary'
          )) .
          CM_Html::btn(CM_Lang::line('Admin.BTN_CANCEL'), 'button', array(
            'class' => 'btn',
            'data-dismiss' => 'modal',
            'aria-hidden'=> 'true'
          ))
      )));
    }
    
    public function response_admin_shortcut_add_save() {
      $thisDAO = Admin_Factory::createCMDAO('adminShortcut');
      $this->_post['account_id'] = $this->_session[Admin::VAR_ADMIN_ID];

      if(!$thisDAO->insert_rows($rMessage, $rReturn, $this->_post, Admin_DAO_AdminShortcut::GROUP_ADD)) {
        Admin::response_error($rMessage);
      } else {
        $this->log_request($this->_post[Admin_DAO_AdminShortcut::COLUMN_HEADLINE], $thisDAO->to_column_group_rows(Admin_DAO_AdminShortcut::GROUP_ADD, $this->_post));
        CM_Output::json($rMessage, true, array('url' => isset($this->_post[Admin::VAR_BACK_URL]) ? Admin::filter_input($this->_post[Admin::VAR_BACK_URL], false) : $this->get_request('admin_shortcut_list')->get_href()));
      }
    }
    
    public function response_admin_shortcut_edit() {
      $thisDAO = Admin_Factory::createCMDAO('adminShortcut');

      if(!isset($this->_get[Admin::VAR_RECORD_ID]) || !$thisDAO->get_rows_byId($recordRows, Admin_DAO_AdminShortcut::GROUP_EDIT, $this->_get[Admin::VAR_RECORD_ID], array('accountId' => $this->_session[Admin::VAR_ADMIN_ID]))) Admin::response_error(CM_Lang::line('Admin.ERROR_NOT_FOUND'));
      
      $this->_breadcrumb->add(self::lang('R_ADMIN_SHORTCUT_LIST'), $this->get_request('admin_shortcut_list')->get_link());
      $this->_breadcrumb->add(self::lang('R_ADMIN_SHORTCUT_EDIT'), $this->get_request('admin_shortcut_edit')->get_link());
      $thisForm = Admin_Factory::createCMForm();
      $thisForm->set_form_dao($thisDAO, Admin_DAO_AdminShortcut::GROUP_EDIT);
      $thisForm->set_form_default($recordRows);
      $form = $this->get_request('admin_shortcut_edit', 'save')->set_params(array(
        Admin::VAR_RECORD_ID => $recordRows[Admin_DAO_AdminShortcut::COLUMN_RECORD_ID]
      ))->get_form('editForm');
      
      CM_Output::json('update_form_ok', true, Admin::format_templates_data(array_merge($form, array(
        'hiddenColumn' => array(
          CM_Html::hidden_field(Admin::VAR_BACK_URL, $this->_get[Admin::VAR_BACK_URL]),
          CM_Html::hidden_field(Admin_DAO_AdminShortcut::COLUMN_RECORD_ID, $recordRows[Admin_DAO_AdminShortcut::COLUMN_RECORD_ID])
        ),
        'formButton' => 
          CM_Html::submit_btn(CM_Lang::line('Admin.BTN_EDIT'), array('class' => 'btn btn-primary')) . '&nbsp;' . 
          CM_Html::reset_btn(CM_Lang::line('Admin.BTN_CANCEL'), array('class' => 'btn prevBtn')) . '&nbsp;' .
          CM_Html::reset_btn(CM_Lang::line('Admin.BTN_RESET'), array('class' => 'btn')),
        'header' => array(
          'icon' => 'icofont-edit',
          'title' => self::lang('R_ADMIN_SHORTCUT_EDIT'),
          'intro' => self::lang('ADMIN_SHORTCUT_EDIT_INTRO'),
          'breadcrumb' => $this->_breadcrumb->trail()
        ),
        'formGroup' => $thisForm->get_groups_form(array(
          'info' => array('title' => self::lang('ADMIN_SHORTCUT_FORM_GROUP_INFO'))
        ), CM_Form::ACTION_UPDATE)
      ))));
    }
    
    public function response_admin_shortcut_edit_save() {
      $thisDAO = Admin_Factory::createCMDAO('adminShortcut');
      $params = array(
        'daoParams' => array('accountId' => $this->_session[Admin::VAR_ADMIN_ID])
      );

      if(!isset($this->_get[Admin::VAR_RECORD_ID]) || !$thisDAO->update_rows($rMessage, $rReturn, $this->_post, $this->_get[Admin::VAR_RECORD_ID], Admin_DAO_AdminShortcut::GROUP_EDIT, $params)) {
        Admin::response_error($rMessage);
      } else {
        $this->log_request($this->_post[Admin_DAO_AdminShortcut::COLUMN_HEADLINE], $thisDAO->to_column_group_rows(Admin_DAO_AdminShortcut::GROUP_EDIT, $this->_post));
        CM_Output::json($rMessage, true, array('url' => isset($this->_post[Admin::VAR_BACK_URL]) ? Admin::filter_input($this->_post[Admin::VAR_BACK_URL], false) : $this->get_request('admin_shortcut_list')->get_href()));
      }
    }

    public function response_admin_shortcut_remove() {
      $thisDAO = Admin_Factory::createCMDAO('adminShortcut');
      $params = array(
        'daoParams' => array('accountId' => $this->_session[Admin::VAR_ADMIN_ID])
      );

      if(!isset($this->_get[Admin::VAR_RECORD_ID]) || !$thisDAO->delete_rows($rMessage, $rReturn, $this->_get[Admin::VAR_RECORD_ID], $params)) {
        Admin::response_error($rMessage);
      } else {
        $this->log_request($rReturn['currentRows'][Admin_DAO_AdminShortcut::COLUMN_HEADLINE], $rReturn['currentRows']);
        CM_Output::json($rMessage, true, array('url' => isset($_GET[Admin::VAR_BACK_URL]) ? Admin::filter_input($_GET[Admin::VAR_BACK_URL], false) : $this->get_request('admin_shortcut_list')->get_href()));
      }
    }
    
    public function response_admin_shortcut_remove_list() {
      $thisDAO = Admin_Factory::createCMDAO('adminShortcut');
      $params = array(
        'daoParams' => array('accountId' => $this->_session[Admin::VAR_ADMIN_ID])
      );

      if(!isset($this->_post['listActionId']) || empty($this->_post['listActionId'])) Admin::response_error(CM_Lang::line('Admin.LIST_ACTION_ID_EMPTY'));

      $errorMessage = array();
      $error = false;
      $log = array();
      
      foreach($this->_post['listActionId'] as $index => $id) {
        if(!$thisDAO->delete_rows($rMessage, $rReturn, $id, $params)) {
          $error = true;
          $errorMessage[] = $rMessage;
        } else {
          $log[] = implode('<br />', array_map(function($element) {
            return sprintf('<b>%s</b>: %s', $element['id'], $element['text']);
          }, array(
            array('id' => Admin_DAO_AdminShortcut::COLUMN_RECORD_ID, 'text' => $id),
            array('id' => Admin_DAO_AdminShortcut::COLUMN_HEADLINE, 'text' => $rReturn['currentRows'][Admin_DAO_AdminShortcut::COLUMN_HEADLINE]),
          )));
        }
      }
      
      if($error == true) {
        Admin::response_error(implode('<br />', $errorMessage));
      } else {
        $this->log_request(self::lang('R_ADMIN_SHORTCUT_REMOVE_L_INTRO'), array('remove' => implode('<br /><br />', $log)));
        Admin::response_success(self::lang('ADMIN_SHORTCUT_REMOVE_L_SUCCESS'), true);
      }
    }
    
    public function response_admin_shortcut_list() {
      $cbThat = $this;
      $thisDAO = Admin_Factory::createCMDAO('adminShortcut');
      $this->_breadcrumb->add(self::lang('R_ADMIN_SHORTCUT_LIST'), $this->get_request('admin_shortcut_list')->get_link());
      
      if(isset($this->_get['keyword']) && !empty($this->_get['keyword'])) Admin_Mod::set_breadcrumb_keyword($this->_breadcrumb, $this->get_request('admin_shortcut_list'), $this->_get['keyword']);
      
      $listColumn = array_merge($thisDAO->get_column_field_label_array(Admin_DAO_AdminShortcut::GROUP_LIST), array(
        'action' => CM_Lang::line('Admin.LIST_ACTION')
      ));
      $daoParams = array(
        'accountId' => $this->_session[Admin::VAR_ADMIN_ID],
        'sort' => Admin_Mod::to_sort($thisDAO, $this->_get['sc'], $this->_get['sf'])
      );
      $listJSON = $thisDAO->get_array_object(Admin_DAO_AdminShortcut::GROUP_LIST, array_merge($daoParams, array(
        'keyColumn' => Admin_DAO_AdminShortcut::COLUMN_RECORD_ID,
        'oNowPage' => isset($this->_get['p']) ? (int)$this->_get['p'] : 1,
        'oPageCallback' => function($page, array $params, $key) use($cbThat) {
          return Admin_Mod::cb_default_pagesplit($page, $params, $key, $cbThat->get_request('admin_shortcut_list'), array('vpage' => 'p'));
        }
      )));
      CM_Output::json('list_ok', true, Admin::format_templates_data(array(
        'header' => array(
          'icon' => 'icofont-th-list',
          'title' => self::lang('R_ADMIN_SHORTCUT_LIST'),
          'intro' => self::lang('R_ADMIN_SHORTCUT_LIST_INTRO')
        ),
        'breadcrumb' => $this->_breadcrumb->trail(),
        'list' => Admin_Mod::to_tmpl_table_list($listJSON, $listColumn, array(
          'id' => "{$this->_current_request['mod']}List",
          'title' => CM_Lang::line('Admin.LIST_ACTION_MENU'),
          'action' => array(
            'checkboxFunc' => array(
              'title' => CM_Lang::line('Admin.LIST_ACTION_MENU'),
              'list' => array(
                array(
                  'link' => $this->get_request('admin_shortcut_remove', 'list')->get_href(),
                  'text' => CM_Lang::line('Admin.LIST_ACTION_ID_REMOVE'),
                  'confirm' => CM_lang::line('Admin.LIST_ACTION_ID_REMOVE_CONFIRM')
                )
              )
            ),
            'listFunc' => array(),
            'search' => array_merge(array(
              'hiddenColumn' => array(),
              'formSearch' => CM_Html::input_field('keyword', $this->_get['keyword'], sprintf('placeholder="%s"', CM_Lang::line('Admin.SEARCH_FIELD_PLACEHOLDER'))),
              'formButton' => CM_Html::submit_btn('<i class="icofont-search"></i>&nbsp;' . CM_Lang::line('Admin.BTN_SEARCH'), array('class' => 'btn'))
            ), $this->get_request('admin_shortcut_list')->get_form('searchForm', array('method' => 'get', 'class' => 'form-inline')))
          ),
          'isSearch' => Admin::not_null($this->_get['keyword']),
          'callback' => array(
            'url' => function($record, $recordRows = array()) {
              return sprintf('<a class="link" target="_blank" href="%s">%s</a>',
                $record,
                CM_Format::to_fix_length_text($record, 20)                
              );
            },
            'action' => function($record, $recordRows = array()) use($cbThat) {
              $action = array();
              //修改或刪除回來的網址
              $backurl = urlencode($cbThat->get_request('admin_shortcut_list')->get_href(Admin::get_all_get_params(array(Admin::VAR_RECORD_ID, Admin::VAR_ACTION), true)));
              //修改
              $thisRequestEdit = $cbThat->get_request('admin_shortcut_edit')->set_params(array(
                Admin::VAR_RECORD_ID => $recordRows[Admin_DAO_AdminShortcut::COLUMN_RECORD_ID],
                Admin::VAR_BACK_URL => $backurl
              ));
              //刪除
              $thisRequestRemove = $cbThat->get_request('admin_shortcut_remove')->set_params(array(
                Admin::VAR_RECORD_ID => $recordRows[Admin_DAO_AdminShortcut::COLUMN_RECORD_ID],
                Admin::VAR_BACK_URL => $backurl
              ));
              
              if($thisRequestEdit->check_permission()) {
                $action[] = $thisRequestEdit->get_link(array('class' => 'link'), sprintf('<i class="icofont-edit"></i>&nbsp;%s', CM_Lang::line('Admin.LIST_ACTION_EDIT')));
              }
              
              if($thisRequestRemove->check_permission()) {
                $action[] = $thisRequestRemove->get_link(array(
                  'class' => 'link',
                  'confirmMsg' => sprintf($cbThat->lang('ADMIN_SHORTCUT_REMOVE_CONFIRM'), $recordRows[Admin_DAO_AdminShortcut::COLUMN_HEADLINE])
                ), sprintf('<i class="icofont-trash"></i>&nbsp;%s', CM_Lang::line('Admin.LIST_ACTION_REMOVE')));
              }
              
              return implode('&nbsp;|&nbsp;', $action);
            }
          ),
          'h_callback' => function($rows, $name) use($cbThat) {
            return Admin_Mod::cb_default_sort($rows, $name, $cbThat->get_request('admin_shortcut_list'), array(
              'link_params' => $cbThat->_get,
              'sort_column' => array(Admin_DAO_AdminShortcut::COLUMN_RECORD_ID, Admin_DAO_AdminShortcut::COLUMN_HEADLINE, 'url', 'sort', 'create_date', 'modify_date')
            ));
          }
        ))
      )));
    }

    public function response_default() {
      $this->response_account_info();
    }
  }
?>