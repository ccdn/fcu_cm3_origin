;(function($, cm, cmApi, cmLang) {
  var mMain = $(cm.setting.CONTAINER),
      mModName = 'customers';
  
  if(typeof cm.mod[mModName] == 'object') {
    var mLang = cm.mod[mModName][cm.setting.VAR_LANGUAGES],
        mCallback = cm.mod[mModName][cm.setting.VAR_CALLBACK] = {};
    //加入模組預設的callback
    for(var k in cm.tmp.cb_mod) mCallback[k] = cm.tmp.cb_mod[k];
    //設定預設的callbcak
    mCallback.list_onLoad = function(data) {
      cm.tmp.cb_mod.list_onLoad.call(this, data);
      
      $('[data-cmfunc="import_finish"]', mMain).on('click', function(e) {
        var that = this,
            formId = 'importFinishForm',
            formParams = {},
            html = '<form id="' + formId + '" class="file-upload-modal-cache" method="post" enctype="multipart/form-data" style="display: none;" action="' + $(this).data('cmurl') + '">';
        formParams[cm.setting.VAR_MODULE] = mModName;
        formParams[cm.setting.VAR_REQUEST] = 'import';

        for(var k in formParams) html += '<input type="hidden" name="' + k + '" value="' + formParams[k] + '" />';

        html += '<input type="file" name="files" />';
        //html += '<input type="file" name="files" multiple="multiple" />';
        html += '</form>';

        if($('#' + formId).length > 0) $('#' + formId).remove();

        $(html)
          .prependTo('body')
          .fileupload({
            sequentialUploads: true,
            limitConcurrentUploads: 1,
            dataType: 'json',
            add: function (e, data) {
              $('[data-cmfunc="process"]', that).text(' - 0%');
              var jqXHR = data.submit().error(function (jqXHR, textStatus, errorThrown) {
                cmApi.alert_message(jqXHR.responseText, 'error');
                $('[data-cmfunc="process"]', that).text('');
                $('#' + formId).remove();
              });
            },
            progressall: function (e, data) {
              var processText = parseInt(data.loaded / data.total * 100, 10) + '%';
              $('[data-cmfunc="process"]', that).text(' - ' + processText);
            },
            done: function (e, data) {
              $('[data-cmfunc="process"]', that).text(' - 100%');
              setTimeout(function() {
                $('[data-cmfunc="process"]', that).text('');
                $('#' + formId).remove();
              }, 3000);

              if(data.result.success == true) {
                cmApi.alert_message(data.result.message, 'success');
                cmApi.call_server_api({
                  history : true,
                  url : cmApi.request_to_url({
                    module : mModName,
                    request : 'list'
                  }),
                  param : {type : $(that).data('type')}
                });
              } else {
                cmApi.alert_message(data.result.message);
              }
            }
          })
          .find('input[type="file"]')
          .trigger('click');
      });
    };
    mCallback.default_onLoad = function(data) {
      mCallback.list_onLoad(data);
    };
    mCallback.password_reset_onLoad = function(data) {
      if($('#' + data.tplData.mId).length > 0) $('#' + data.tplData.mId).remove();
      
      data.tplData.mBody = cmApi.templates_add('modal_form', data.tplData.mData);
      var jqForm = $(cmApi.templates_add('modal', data.tplData)).prependTo('#mainBody');
      $('form', jqForm).cmForm();
      jqForm.modal({
        keyboard: false,
        show : true
      }).on('hidden', function() {
        jqForm.remove();
      });
    };
    mCallback.password_reset_save_onLoad = function(data) {
      var jqForm = $(this).parents('div.modal');
      jqForm.modal('hide');
      cmApi.alert_message(data.message, 'success');
    };
    mCallback.edit_list_group_onLoad = function(data) {
      mCallback.edit_list_status_onLoad.call(this, data);
    };
    mCallback.group_add_onLoad = function(data) {
      cm.tmp.cb_mod.add_onLoad.call(this, data);
    };
    mCallback.group_add_save_onLoad = function(data) {
      cm.tmp.cb_mod.add_save_onLoad.call(this, data);
    };
    mCallback.group_edit_onLoad = function(data) {
      cm.tmp.cb_mod.edit_onLoad.call(this, data);
    };
    mCallback.group_edit_save_onLoad = function(data) {
      cm.tmp.cb_mod.edit_save_onLoad.call(this, data);
    };
    mCallback.group_remove_onLoad = function(data) {
      cm.tmp.cb_mod.remove_onLoad.call(this, data);
    };
    mCallback.group_remove_list_onLoad = function(data) {
      cm.tmp.cb_mod.remove_list_onLoad.call(this, data);
    };
    mCallback.group_list_onLoad = function(data) {
      cm.tmp.cb_mod.list_onLoad.call(this, data);
    };
  } else {
    cmApi.alert_message(cmLang.ERROR_MODULE_NOT_FOUND);
  }
})(jQuery, window._cm, window._cm.func, window._cm.lang);