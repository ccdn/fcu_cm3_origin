<?php
  class modCustomers extends Admin_Mod {
    const MODULE_NAME = 'customers';
    
    public $_mod_dao;
    
    static public function lang($key) {
      return CM_Lang::line(self::MODULE_NAME . '.' . $key, '', CM_Conf::get('PATH_F.ADMIN_MOD') . self::MODULE_NAME . '/');
    }
    
    public function __construct($modName, $requestName = '', $actionName = '') {
      parent::__construct($modName, $requestName, $actionName);
      //設定功能-預設功能
      $this->add_request('default', self::lang('R_MOD'), self::lang('R_INTRO'));
      //設定功能-客戶列表
      $this->add_request('list', self::lang('R_LIST'), self::lang('R_LIST_INTRO'));
      //設定功能-新增客戶
      $this->add_request('add', self::lang('R_ADD'), self::lang('R_ADD_INTRO'));
      $this->add_request_sub('add', 'save', self::lang('R_ADD_SAVE_INTRO'));
      //設定功能-編輯客戶
      $this->add_request('edit', self::lang('R_EDIT'), self::lang('R_EDIT_INTRO'));
      $this->add_request_sub('edit', 'save', self::lang('R_EDIT_SAVE_INTRO'));
      $this->add_request_sub('edit', 'list_status', self::lang('R_EDIT_L_STATUS_INTRO'));
      $this->add_request_sub('edit', 'list_group', self::lang('R_EDIT_L_GROUP_INTRO'));
      //設定功能-移除客戶
      $this->add_request('remove', self::lang('R_REMOVE'), self::lang('R_REMOVE_INTRO'), array(
        'attrParams' => array(
          'isHistory' => false,
          'isConfirm' => true,
          'confirmMsg' => self::lang('REMOVE_CONFIRM')
        )
      ));
      $this->add_request_sub('remove', 'list', self::lang('R_REMOVE_L_INTRO'));
      //重設密碼
      $this->add_request('password_reset', self::lang('R_CUSTOMERS_PASSWORD_RESET'), self::lang('R_CUSTOMERS_PASSWORD_RESET_INTRO'), array(
        'setting' => false,
        'attrParams' => array('isHistory' => false)
      ));
      $this->add_request_sub('password_reset', 'save', self::lang('R_CUSTOMERS_PASSWORD_RESET_SAVE_INTRO'));
      //設定功能-資料輸出
      $this->add_request('export', self::lang('R_EXPORT'), self::lang('R_INTRO'), array(
        'attrParams' => array(
          'ajax' => false,
          'data-target' => 'blank'
        )
      ));

      $this->add_request('import', self::lang('R_IMPORT'), self::lang('R_IMPORT_INTRO'), array(
        'attrParams' => array(
          'ajax' => false,
          'data-target' => 'blank'
        )
      ));
      //設定功能-群組列表
      $this->add_request('group_list', self::lang('R_GROUP_LIST'), self::lang('R_GROUP_LIST_INTRO'));
      //設定功能-新增群組
      $this->add_request('group_add', self::lang('R_GROUP_ADD'), self::lang('R_GROUP_ADD_INTRO'));
      $this->add_request_sub('group_add', 'save', self::lang('R_GROUP_ADD_SAVE_INTRO'));
      //設定功能-編輯群組
      $this->add_request('group_edit', self::lang('R_GROUP_EDIT'), self::lang('R_GROUP_EDIT_INTRO'));
      $this->add_request_sub('group_edit', 'save', self::lang('R_GROUP_EDIT_SAVE_INTRO'));
      //設定功能-移除群組
      $this->add_request('group_remove', self::lang('R_GROUP_REMOVE'), self::lang('R_GROUP_REMOVE_INTRO'), array(
        'attrParams' => array(
          'isHistory' => false,
          'isConfirm' => true,
          'confirmMsg' => self::lang('REMOVE_CONFIRM')
        )
      ));
      $this->add_request_sub('group_remove', 'list', self::lang('R_GROUP_REMOVE_L_INTRO'));
      //設定Breadcrumb
      $this->_breadcrumb->add(self::lang('R_MOD'), $this->get_request('default')->get_link(array(), sprintf('<i class="icofont-user"></i> %s', self::lang('R_MOD'))));
      $this->_mod_dao = Admin_Factory::createCMDAO(self::MODULE_NAME);
    }
    
    public function response_load_module() {
      parent::response_load_module();
    }
    
    public function response_add() {
      $this->_breadcrumb->add(self::lang('R_LIST'), $this->get_request('list')->get_link());
      $this->_breadcrumb->add(self::lang('R_ADD'), $this->get_request('add')->get_link());
      $this->_mod_dao->init_options();
      $thisForm = Admin_Factory::createCMForm();
      $thisForm->set_form_dao($this->_mod_dao, Admin_DAO_Customers::GROUP_ADD);
      $thisForm->set_form_default(array('date' => date('Y-m-d')));
      $form = $this->get_request('add', 'save')->get_form('addForm');
      CM_Output::json('add_form_ok', true, Admin::format_templates_data(array_merge($form, array(
        'formButton' => 
          CM_Html::submit_btn(CM_Lang::line('Admin.BTN_ADD'), array('class' => 'btn btn-primary')) . '&nbsp;' . 
          CM_Html::reset_btn(CM_Lang::line('Admin.BTN_CANCEL'), array('class' => 'btn prevBtn')) . '&nbsp;' .
          CM_Html::reset_btn(CM_Lang::line('Admin.BTN_RESET'), array('class' => 'btn')),
        'hiddenColumn' => array(CM_Html::hidden_field(Admin::VAR_BACK_URL, $this->_get[Admin::VAR_BACK_URL])),
        'header' => array(
          'icon' => 'icofont-edit',
          'title' => self::lang('R_ADD'),
          'intro' => self::lang('ADD_INTRO'),
          'breadcrumb' => $this->_breadcrumb->trail()
        ),
        'formGroup' => $thisForm->get_groups_form(array(
          'info' => array('title' => self::lang('FORM_GROUP_INFO')),
          'setting' => array('title' => self::lang('FORM_GROUP_SETTING'))
        ), CM_Form::ACTION_INSERT)
      ))));
    }
    
    public function response_add_save() {
      if(!$this->_mod_dao->insert_rows($rMessage, $rReturn, $this->_post, Admin_DAO_Customers::GROUP_ADD)) {
        Admin::response_error($rMessage);
      } else {
        $this->log_request($this->_post[Admin_DAO_Customers::COLUMN_HEADLINE], $this->_mod_dao->to_column_group_rows(Admin_DAO_Customers::GROUP_ADD, $this->_post));
        CM_Output::json($rMessage, true, array('url' => isset($this->_post[Admin::VAR_BACK_URL]) ? Admin::filter_input($this->_post[Admin::VAR_BACK_URL], false) : $this->get_request('list')->get_href()));
      }
    }
    
    public function response_edit() {
      if(!isset($this->_get[Admin::VAR_RECORD_ID]) || !$this->_mod_dao->get_rows_byId($recordRows, Admin_DAO_Customers::GROUP_EDIT, $this->_get[Admin::VAR_RECORD_ID])) Admin::response_error(CM_Lang::line('Admin.ERROR_NOT_FOUND'));
      
      $this->_breadcrumb->add(self::lang('R_LIST'), $this->get_request('list')->get_link());
      $this->_breadcrumb->add(self::lang('R_EDIT'), $this->get_request('edit')->get_link());
      $this->_mod_dao->init_options();
      $thisForm = Admin_Factory::createCMForm();
      $thisForm->set_form_dao($this->_mod_dao, Admin_DAO_Customers::GROUP_EDIT);
      $thisForm->set_form_default($recordRows);
      $form = $this->get_request('edit', 'save')->set_params(array(
        Admin::VAR_RECORD_ID => $recordRows[Admin_DAO_Customers::COLUMN_RECORD_ID]
      ))->get_form('editForm');
      
      CM_Output::json('update_form_ok', true, Admin::format_templates_data(array_merge($form, array(
        'hiddenColumn' => array(
          CM_Html::hidden_field(Admin::VAR_BACK_URL, $this->_get[Admin::VAR_BACK_URL]),
          CM_Html::hidden_field(Admin_DAO_Customers::COLUMN_RECORD_ID, $recordRows[Admin_DAO_Customers::COLUMN_RECORD_ID])
        ),
        'formButton' => 
          CM_Html::submit_btn(CM_Lang::line('Admin.BTN_EDIT'), array('class' => 'btn btn-primary')) . '&nbsp;' . 
          CM_Html::reset_btn(CM_Lang::line('Admin.BTN_CANCEL'), array('class' => 'btn prevBtn')) . '&nbsp;' .
          CM_Html::reset_btn(CM_Lang::line('Admin.BTN_RESET'), array('class' => 'btn')),
        'header' => array(
          'icon' => 'icofont-edit',
          'title' => self::lang('R_EDIT'),
          'intro' => self::lang('EDIT_INTRO'),
          'breadcrumb' => $this->_breadcrumb->trail()
        ),
        'formGroup' => $thisForm->get_groups_form(array(
          'info' => array('title' => self::lang('FORM_GROUP_INFO')),
          'setting' => array('title' => self::lang('FORM_GROUP_SETTING'))
        ), CM_Form::ACTION_UPDATE)
      ))));
    }
    
    public function response_edit_save() {
      if(!isset($this->_get[Admin::VAR_RECORD_ID]) || !$this->_mod_dao->update_rows($rMessage, $rReturn, $this->_post, $this->_get[Admin::VAR_RECORD_ID], Admin_DAO_Customers::GROUP_EDIT)) {
        Admin::response_error($rMessage);
      } else {
        $this->log_request($this->_post[Admin_DAO_Customers::COLUMN_HEADLINE], $this->_mod_dao->to_column_group_rows(Admin_DAO_Customers::GROUP_EDIT, $this->_post));
        CM_Output::json($rMessage, true, array('url' => isset($this->_post[Admin::VAR_BACK_URL]) ? Admin::filter_input($this->_post[Admin::VAR_BACK_URL], false) : $this->get_request('list')->get_href()));
      }
    }
    
    public function response_edit_list_status() {
      $error = false;
      $errorMessage = array();
      $editIdArray = array();

      if(isset($this->_get[Admin::VAR_RECORD_ID])) {
        $editIdArray[] = (int)$this->_get[Admin::VAR_RECORD_ID];
      } elseif(isset($this->_post['listActionId']) && !empty($this->_post['listActionId'])) {
        $editIdArray = array_map(function($element) { return (int)$element; }, $this->_post['listActionId']);
      } else {
        $error = true;
      }

      if($error == true) Admin::response_error(CM_Lang::line('Admin.LIST_ACTION_ID_EMPTY'));

      $status = isset($this->_get[Admin_DAO_Customers::COLUMN_STATUS]) ? (int)$this->_get[Admin_DAO_Customers::COLUMN_STATUS] : '2';
      
      foreach($editIdArray as $index => $id) {
        if(!$this->_mod_dao->update_rows($rMessage, $rReturn, array(Admin_DAO_Customers::COLUMN_STATUS => $status), $id, Admin_DAO_Customers::GROUP_STATUS)) {
          $error = true;
          $errorMessage[] = $rMessage;
        } else {
          $log[] = implode('<br />', array_map(function($element) {
            return sprintf('<b>%s</b>: %s', $element['id'], $element['text']);
          }, array(
            array('id' => Admin_DAO_Customers::COLUMN_RECORD_ID, 'text' => $id),
            array('id' => Admin_DAO_Customers::COLUMN_STATUS, 'text' => $status),
            array('id' => Admin_DAO_Customers::COLUMN_HEADLINE, 'text' => $rReturn['currentRows'][Admin_DAO_Customers::COLUMN_HEADLINE]),
          )));
        }
      }

      if($error == true) {
        Admin::response_error(implode('<br />', $errorMessage));
      } else {
        $this->log_request(self::lang('R_EDIT_L_STATUS_INTRO'), array('edit' => implode('<br /><br />', $log)));
        Admin::response_success(self::lang('EDIT_L_SUCCESS'), true);
      }
    }

    public function response_edit_list_group() {
      $error = false;
      $errorMessage = array();
      $editIdArray = array();

      if(isset($this->_get[Admin::VAR_RECORD_ID])) {
        $editIdArray[] = (int)$this->_get[Admin::VAR_RECORD_ID];
      } elseif(isset($this->_post['listActionId']) && !empty($this->_post['listActionId'])) {
        $editIdArray = array_map(function($element) { return (int)$element; }, $this->_post['listActionId']);
      } else {
        $error = true;
      }

      if($error == true) Admin::response_error(CM_Lang::line('Admin.LIST_ACTION_ID_EMPTY'));

      $this->_mod_dao->init_options();

      if(!isset($this->_get['group_id']) || !Admin::in_options($this->_get['group_id'], $this->_mod_dao->get_options('group'), $currentOptions)) Admin::response_error(CM_lang::line('CM.ERROR_DATA_NOT_FOUND'));

      foreach($editIdArray as $index => $id) {
        if(!$this->_mod_dao->update_rows($rMessage, $rReturn, array('group_id' => array($currentOptions['id'])), $id, Admin_DAO_Customers::GROUP_GROUP)) {
          $error = true;
          $errorMessage[] = $rMessage;
        } else {
          $log[] = implode('<br />', array_map(function($element) {
            return sprintf('<b>%s</b>: %s', $element['id'], $element['text']);
          }, array(
            array('id' => Admin_DAO_Customers::COLUMN_RECORD_ID, 'text' => $id),
            array('id' => 'group_id', 'text' => $currentOptions['id']),
            array('id' => 'group_name', 'text' => $currentOptions['text']),
            array('id' => Admin_DAO_Customers::COLUMN_HEADLINE, 'text' => $rReturn['currentRows'][Admin_DAO_Customers::COLUMN_HEADLINE]),
          )));
        }
      }

      if($error == true) {
        Admin::response_error(implode('<br />', $errorMessage));
      } else {
        $this->log_request(self::lang('R_EDIT_L_GROUP_INTRO'), array('edit' => implode('<br /><br />', $log)));
        Admin::response_success(self::lang('EDIT_L_SUCCESS'), true);
      }
    }
    
    public function response_remove() {
      if(!isset($this->_get[Admin::VAR_RECORD_ID]) || !$this->_mod_dao->delete_rows($rMessage, $rReturn, $this->_get[Admin::VAR_RECORD_ID])) {
        Admin::response_error($rMessage);
      } else {
        $this->log_request($rReturn['currentRows'][Admin_DAO_Customers::COLUMN_HEADLINE], $rReturn['currentRows']);
        CM_Output::json($rMessage, true, array('url' => isset($_GET[Admin::VAR_BACK_URL]) ? Admin::filter_input($_GET[Admin::VAR_BACK_URL], false) : $this->get_request('list')->get_href()));
      }
    }
    
    public function response_remove_list() {
      if(!isset($this->_post['listActionId']) || empty($this->_post['listActionId'])) Admin::response_error(CM_Lang::line('Admin.LIST_ACTION_ID_EMPTY'));

      $errorMessage = array();
      $error = false;
      $log = array();
      
      foreach($this->_post['listActionId'] as $index => $id) {
        if(!$this->_mod_dao->delete_rows($rMessage, $rReturn, $id)) {
          $error = true;
          $errorMessage[] = $rMessage;
        } else {
          $log[] = implode('<br />', array_map(function($element) {
            return sprintf('<b>%s</b>: %s', $element['id'], $element['text']);
          }, array(
            array('id' => Admin_DAO_Customers::COLUMN_RECORD_ID, 'text' => $id),
            array('id' => Admin_DAO_Customers::COLUMN_HEADLINE, 'text' => $rReturn['currentRows'][Admin_DAO_Customers::COLUMN_HEADLINE]),
          )));
        }
      }
      
      if($error == true) {
        Admin::response_error(implode('<br />', $errorMessage));
      } else {
        $this->log_request(self::lang('R_REMOVE_L_INTRO'), array('remove' => implode('<br /><br />', $log)));
        Admin::response_success(self::lang('REMOVE_L_SUCCESS'), true);
      }
    }
    
    public function response_list() {
      $cbThat = $this;
      $this->_mod_dao->init_options();
      $this->_breadcrumb->add(self::lang('R_LIST'), $this->get_request('list')->get_link());

      $groupOptions = $this->_mod_dao->get_options('group');
      $currentGroup = '';

      if(isset($this->_get['group_id']) && Admin::in_options($this->_get['group_id'], $groupOptions, $currentOptions)) $currentGroup = $currentOptions['id'];

      $searchColumn = array('start_date', 'end_date', 'last_start_date', 'last_end_date', 'keyword');
      $isSearch = false;

      foreach($searchColumn as $column) {
        if(!empty($this->_get[$column])) {
          $isSearch = true;
          break;
        }
      }

      if($isSearch == true) {
        $this->_breadcrumb->add(CM_Lang::line('Admin.TOP_SEARCH_BREADCRUMB'), $this->get_request('list')->set_params(Admin::get_all_get_params(array(), true))->get_link());
        $daoParams = array(
          'keyword' => $this->_get['keyword'],
          'date' => array(
            'start' => Admin::not_null($this->_get['start_date']) ? $this->_get['start_date'] : '0000-00-00',
            'end' => Admin::not_null($this->_get['end_date']) ? $this->_get['end_date'] : date('Y-m-d')
          ),
          'last_date' => array(
            'start' => Admin::not_null($this->_get['last_start_date']) ? $this->_get['last_start_date'] : '0000-00-00',
            'end' => Admin::not_null($this->_get['last_end_date']) ? $this->_get['last_end_date'] : date('Y-m-d')
          )
        );
      } else {
        $daoParams = array();
      }

      $daoParams = array_merge($daoParams, array(
        'groupId' => $currentGroup,
        'sort' => Admin_Mod::to_sort($this->_mod_dao, $this->_get['sc'], $this->_get['sf']),
      ));
      $listColumn = array_merge($this->_mod_dao->get_column_field_label_array(Admin_DAO_Customers::GROUP_LIST), array(
        'action' => CM_Lang::line('Admin.LIST_ACTION')
      ));
      $listJSON = $this->_mod_dao->get_array_object(Admin_DAO_Customers::GROUP_LIST, array_merge($daoParams, array(
        'keyColumn' => Admin_DAO_Customers::COLUMN_RECORD_ID,
        'oNowPage' => isset($this->_get['p']) ? (int)$this->_get['p'] : 1,
        'oPageCallback' => function($page, array $params, $key) use($cbThat) {
          return $cbThat->cb_default_pagesplit($page, $params, $key, $cbThat->get_request('list'), array('vpage' => 'p'));
        }
      )));
      $checkboxFunc = array();
      $checkboxFunc[] = array(
        'link' => $this->get_request('edit', 'list_status')->set_params(array(Admin_DAO_Customers::COLUMN_STATUS => '2'))->get_href(),
        'text' => CM_Lang::line('Admin.LIST_ACTION_ID_DISABLE'),
        'confirm' => CM_lang::line('Admin.LIST_ACTION_ID_DISABLE_CONFIRM')
      );
      $checkboxFunc[] = array(
        'link' => $this->get_request('edit', 'list_status')->set_params(array(Admin_DAO_Customers::COLUMN_STATUS => '1'))->get_href(),
        'text' => CM_Lang::line('Admin.LIST_ACTION_ID_ENABLE'),
        'confirm' => CM_lang::line('Admin.LIST_ACTION_ID_ENABLE_CONFIRM')
      );
      $checkboxFunc[] = array('separate' => true);

      foreach($this->_mod_dao->get_options('group') as $options) {
        $checkboxFunc[] = array(
          'link' => $this->get_request('edit', 'list_group')
            ->set_params(array('group_id' => $options['id']))->get_href(),
          'text' => sprintf(self::lang('LIST_ACTION_GROUP'), $options['text']),
          'confirm' => sprintf(self::lang('LIST_ACTION_GROUP_CONFIRM'), $options['text'])
        );
      }

      $checkboxFunc[] = array('separate' => true);
      $checkboxFunc[] = array(
        'link' => $this->get_request('remove', 'list')->get_href(),
        'text' => CM_Lang::line('Admin.LIST_ACTION_ID_REMOVE'),
        'confirm' => CM_lang::line('Admin.LIST_ACTION_ID_REMOVE_CONFIRM')
      );
      
      CM_Output::json('list_ok', true, Admin::format_templates_data(array(
        'header' => array(
          'icon' => 'icofont-th-list',
          'title' => self::lang('R_LIST'),
          'intro' => self::lang('R_LIST_INTRO')
        ),
        'breadcrumb' => $this->_breadcrumb->trail(),
        'topSearch' => array_merge(array(
          'title' => CM_Lang::line('Admin.TOP_SEARCH'),
          'fields' => array(           
            array(              
              array(
                'label' => self::lang('TOP_SEARCH_COLUMN_DATE'),
                'field' => 
                  CM_Html::input_field('start_date', $this->_get['start_date'], 'class="hasDatepicker input-small"') . '&nbsp;~&nbsp;' .
                  CM_Html::input_field('end_date', $this->_get['end_date'], 'class="hasDatepicker input-small"'),
                'attr' => 'class="span4"'
              ),
              array(
                'label' => self::lang('TOP_SEARCH_COLUMN_LASTDATE'),
                'field' => 
                  CM_Html::input_field('last_start_date', $this->_get['last_start_date'], 'class="hasDatepicker input-small"') . '&nbsp;~&nbsp;' .
                  CM_Html::input_field('last_end_date', $this->_get['last_end_date'], 'class="hasDatepicker input-small"'),
                'attr' => 'class="span4"'
              ),
              array(
                'label' => self::lang('TOP_SEARCH_COLUMN_KEYWORD'),
                'field' => CM_Html::input_field('keyword', $this->_get['keyword'], 'class="input-large"'),
                'attr' => 'class="span4"'
              )
            )
          ),
          'submitBtn' => CM_Lang::line('Admin.TOP_SEARCH_BTN')
        ), $this->get_request('list')->get_form('searchForm', array('method' => 'get', 'class' => 'form-inline'))),
        'list' => Admin_Mod::to_tmpl_table_list($listJSON, $listColumn, array(
          'addon' => Admin_Mod::to_tmpl_catalog($this->get_request('list')->set_params(Admin::get_all_get_params(array('group_id'), true)), $this->_mod_dao, array(
            'daoParams' => $daoParams,
            'catalog' => $currentGroup,
            'catalogColumn' => 'groupId',
            'paramsColumn' => 'group_id',
            'options' => $this->_mod_dao->get_options('group')
          )),
          'id' => "{$this->_current_request['mod']}List",
          'title' => CM_Lang::line('Admin.LIST_ACTION_MENU'),
          'action' => array(
            'funcSize' => '11',
            'searchSize' => '1',
            'checkboxFunc' => array(
              'title' => CM_Lang::line('Admin.LIST_ACTION_MENU'),
              'list' => $checkboxFunc
            ),
            'listFunc' => array(
              $this->get_request('add')
                ->set_params(array(Admin::VAR_BACK_URL => urlencode($this->get_request('list')->get_href(Admin::get_all_get_params(array(), true)))))
                ->set_text_format('<i class="icofont-plus"></i>&nbsp;%s')
                ->get_btn(array('class' => 'btn btn-primary')),
              $this->get_request('export')
                ->set_params($this->_get)
                ->set_text_format('<i class="icofont-share"></i>&nbsp;%s')
                ->get_btn(array('class' => 'btn')),
              CM_Html::func_btn(sprintf('<i class="icofont-upload"></i>&nbsp;%s<span data-cmfunc="process"></span>', $this->lang('R_IMPORT')), array(
                'class' => 'btn',
                'data-cmfunc' => 'import_finish',
                'data-cmurl' => Admin::href_admin_link('controller.php')
              ))
            ),
            'search' => array()
          ),
          'isSearch' => Admin::not_null($this->_get['keyword']),
          'callback' => array(
            'group_id' => function($record, $recordRows = array()) use($cbThat) {
              if(!empty($record)) {
                $options = $cbThat->_mod_dao->get_options('group');
                return implode('<br />', array_map(function($element) use($options) {
                  return Admin::get_options_name($element, $options);
                }, explode(',', $record)));
              } else {
                return '--';
              }
            },
            'status' => function($record, $recordRows = array()) use($cbThat) {
              $options = $cbThat->_mod_dao->get_options('status');
              $statusLabel = array(
                1 => 'label-success',
                2 => 'label-important'
              );

              if(Admin::in_options($record, $options, $currentOptions)) {
                return $cbThat->get_request('edit', 'list_status')
                  ->set_params(array(
                    Admin_DAO_Link::COLUMN_STATUS => $currentOptions['id'] == '1' ? '2' : '1',
                    Admin::VAR_RECORD_ID => $recordRows[Admin_DAO_Link::COLUMN_RECORD_ID]
                  ))
                  ->set_text(sprintf('<span class="label %s">%s</span>', $statusLabel[$currentOptions['id']], $currentOptions['text']))
                  ->get_link(array('isHistory' => false));
              } else {
                return '--';
              }
            },
            'file' => function($record, $recordRows = array()) use($cbThat) {
              return CM_Html::img(Admin_Format::to_thumbnail_src($record, '80x60'), $record, 80, 60, 'class="thumbnail"');
            },
            'action' => function($record, $recordRows = array()) use($cbThat) {
              $action = array();
              //修改或刪除回來的網址
              $backurl = urlencode($cbThat->get_request('list')->get_href(Admin::get_all_get_params(array(Admin::VAR_RECORD_ID, Admin::VAR_ACTION), true)));
              //密碼重設
              $thisRequestReset = $cbThat->get_request('password_reset')->set_params(array(
                Admin::VAR_RECORD_ID => $recordRows[Admin_DAO_Customers::COLUMN_RECORD_ID]
              ));
              //修改
              $thisRequestEdit = $cbThat->get_request('edit')->set_params(array(
                Admin::VAR_RECORD_ID => $recordRows[Admin_DAO_Customers::COLUMN_RECORD_ID],
                Admin::VAR_BACK_URL => $backurl
              ));
              //刪除
              $thisRequestRemove = $cbThat->get_request('remove')->set_params(array(
                Admin::VAR_RECORD_ID => $recordRows[Admin_DAO_Customers::COLUMN_RECORD_ID],
                Admin::VAR_BACK_URL => $backurl
              ));

              if($thisRequestReset->check_permission()) {
                $action[] = $thisRequestReset->set_text_format('<i class="icofont-key"></i>&nbsp;%s')->get_link(array('class' => 'link'));
              }
              
              if($thisRequestEdit->check_permission()) {
                $action[] = $thisRequestEdit->get_link(array('class' => 'link'), sprintf('<i class="icofont-edit"></i>&nbsp;%s', CM_Lang::line('Admin.LIST_ACTION_EDIT')));
              }
              
              if($thisRequestRemove->check_permission()) {
                $action[] = $thisRequestRemove->get_link(array(
                  'class' => 'link',
                  'confirmMsg' => sprintf($cbThat->lang('REMOVE_CONFIRM'), $recordRows['email'])
                ), sprintf('<i class="icofont-trash"></i>&nbsp;%s', CM_Lang::line('Admin.LIST_ACTION_REMOVE')));
              }
              
              return implode('&nbsp;|&nbsp;', $action);
            }
          ),
          'h_callback' => function($rows, $name) use($cbThat) {
            if($name == 'action') {
              $rows['attr'] = 'width="200"'; 
            } elseif($name == 'login_last_date') {
              $rows['attr'] = 'width="100"'; 
            }

            return $cbThat->cb_default_sort($rows, $name, $cbThat->get_request('list'), array(
              'link_params' => $cbThat->_get,
              'sort_column' => array(Admin_DAO_Customers::COLUMN_RECORD_ID, Admin_DAO_Customers::COLUMN_HEADLINE, 'group_id', 'sex', 'email', 'ip', Admin_DAO_Customers::COLUMN_STATUS, 'login_count', 'login_last_date')
            ));
          }
        ))
      )));
    }

    public function response_import() {
      $fileDAO = Admin_Factory::createCMDAO('file');
      $ftpPath = CM_Conf::get('PATH_F.UPLOAD') . 'ftp/';
      $XMLfile = $_FILES['files']['tmp_name'];
      $XMLfile_size = $_FILES['files']['size'];
      
      if(empty($_FILES['files'])) {
        Admin::response_error(CM_Lang::line('CM.ERROR_UPLOAD_EMPTY'));
      } elseif(Admin::get_file_ext($_FILES['files']['name']) != 'xml') {
        Admin::response_error(self::lang('IMPORT_ERROR_1'));
      } else {
        $errorMessage = array();
        $updateCount = 0;
        $xml = simplexml_load_file($XMLfile);
        $items = $xml->Customers;

        if(empty($items)) Admin::response_error(self::lang('IMPORT_ERROR_2'));
        
        foreach($items AS $itemsRows) {
          $itemsRows = array_map(function($element) {
            return (string)$element;
          }, (array)$itemsRows);

          if(!empty($itemsRows['group_id'])) {
            $itemsRows['group_id'] = explode(',', $itemsRows['group_id']);
          } else {
            $itemsRows['group_id'] = array();
          }

          if(!$this->_mod_dao->insert_rows($rMessage, $rReturn, $itemsRows, Admin_DAO_Customers::GROUP_ADD)) {
            $errorMessage[] = $rMessage;
          } else {
            $updateCount ++;
          }
        }
        
        if(empty($errorMessage)) {
          CM_Output::json(sprintf(self::lang('IMPORT_SUCCESS_1'), $updateCount), true);
        } else {
          Admin::response_error(sprintf(self::lang('IMPORT_SUCCESS_2'),
            $updateCount,
            count($errorMessage),
            implode('<br />', $errorMessage)
          ));
        }
      }
    }
    
    public function response_export() {
      $cbThat = $this;
      $this->_mod_dao->init_options();

      $groupOptions = $this->_mod_dao->get_options('group');
      $currentGroup = '';

      if(isset($this->_get['group_id']) && Admin::in_options($this->_get['group_id'], $groupOptions, $currentOptions)) $currentGroup = $currentOptions['id'];

      $daoParams = array(
        'groupId' => $currentGroup,
        'keyword' => $this->_get['keyword'],
        'date' => array(
          'start' => Admin::not_null($this->_get['start_date']) ? $this->_get['start_date'] : '1970-01-01',
          'end' => Admin::not_null($this->_get['end_date']) ? $this->_get['end_date'] : date('Y-m-d')
        ),
        'last_date' => array(
          'start' => Admin::not_null($this->_get['last_start_date']) ? $this->_get['last_start_date'] : '1970-01-01',
          'end' => Admin::not_null($this->_get['last_end_date']) ? $this->_get['last_end_date'] : date('Y-m-d')
        )
      );
      $listColumn = $this->_mod_dao->get_column_field_label_array(Admin_DAO_Customers::GROUP_EXPORT);
      $listColumnFormat = array(
        'sex' => function($record, $recordRows = array()) use($cbThat) {
          return Admin::get_options_name($record, $cbThat->_mod_dao->get_options('sex'));
        },
        'group_id' => function($record, $recordRows = array()) use($cbThat) {
          return Admin::get_options_name($record, $cbThat->_mod_dao->get_options('group'));
        },
        'status' => function($record, $recordRows = array()) use($cbThat) {
          return Admin::get_options_name($record, $cbThat->_mod_dao->get_options('status'));
        },
        'newsletter' => function($record, $recordRows = array()) use($cbThat) {
          return Admin::get_options_name($record, $cbThat->_mod_dao->get_options('newsletter'));
        }
      );
      
      header('Content-type:text/html; charset=utf-8');
      header('Content-Type: application/octet-stream');
      header("Content-Disposition: attachment; filename={$this->_current_request['mod']}_export.xls;");
      echo '<HTML xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40">';
      echo '<head><meta http-equiv="content-type" content="application/vnd.ms-excel; charset=UTF-8"></head>';
      echo '<body><table border="1">';
      echo '<tr>';
      
      foreach($listColumn as $columnName => $columnLabel) echo "<td>{$columnLabel}</td>";
      
      echo '</tr>';
      
      if($this->_mod_dao->get_array($recordArray, Admin_DAO_Customers::GROUP_EXPORT, $daoParams)) {
        $recordArray = CM_Format::to_list_array_format($recordArray, $listColumn, $listColumnFormat);
        
        foreach($recordArray as $recordRows) {
          echo '<tr>';
          
          foreach($listColumn as $columnName => $columnLabel) echo "<td>{$recordRows[$columnName]}</td>";
          
          echo '</tr>';
        }
      }
      
      echo '</table></body></html>';
      exit;
    }

    public function response_password_reset() {
      if(!isset($this->_get[Admin::VAR_RECORD_ID]) || !$this->_mod_dao->get_rows_byId($recordRows, Admin_DAO_Customers::GROUP_LIST, (int)$this->_get[Admin::VAR_RECORD_ID])) Admin::response_error(CM_lang::line('CM.ERROR_DATA_NOT_FOUND'));

      $this->_mod_dao->set_column('password_new', array(
        'disable' => false,
        'f_confirm' => true
      ));
      $thisForm = Admin_Factory::createCMForm();
      $thisForm->set_form_dao($this->_mod_dao, Admin_DAO_Customers::GROUP_PASSWORD_RESET);
      $formForgetPassword = $this->get_request('password_reset', 'save')->set_params(array(
        Admin::VAR_RECORD_ID => $recordRows[Admin_DAO_Customers::COLUMN_RECORD_ID]
      ))->get_form('resetPasswordForm');
      CM_Output::json('reset_password_form_ok', true, Admin::format_templates_data(array(
        'mId' => 'resetPasswordForm',
        'mTop' => $formForgetPassword['formTop'],
        'mBottom' => $formForgetPassword['formBottom'],
        'mHeader' => sprintf(self::lang('PASSWORD_RESET_TITLE_L'), $recordRows[Admin_DAO_Customers::COLUMN_HEADLINE]),
        'mData' => array(
          'intro' => sprintf(self::lang('PASSWORD_RESET_INTRO'), $recordRows[Admin_DAO_Customers::COLUMN_HEADLINE]),
          'fields' => $thisForm->get_form()
        ),
        'mButton' => 
          CM_Html::submit_btn(self::lang('PASSWORD_RESET_SUBMIT'), array(
            'class' => 'btn btn-primary'
          )) .
          CM_Html::btn(CM_Lang::line('Admin.BTN_CANCEL'), 'button', array(
            'class' => 'btn',
            'data-dismiss' => 'modal',
            'aria-hidden'=> 'true'
          ))
      )));
    }

    public function response_password_reset_save() {
      if(!isset($this->_get[Admin::VAR_RECORD_ID])) {
        Admin::response_error(CM_lang::line('CM.ERROR_DATA_NOT_FOUND'));
      } if(!$this->_mod_dao->validate_column($returnMessage, $this->_post, Admin_DAO_Customers::GROUP_PASSWORD_RESET)) {
        Admin::response_error($returnMessage);
      } elseif($this->_post['password_new'] != $this->_post['password_new_confirm']) {
        Admin::response_error(self::lang('PASSWORD_CHANGE_ERROR_PASSWORD_CONFIRM'));
      } elseif(!$this->_mod_dao->get_rows_byId($accountRows, Admin_DAO_Customers::GROUP_LIST, (int)$this->_get[Admin::VAR_RECORD_ID])) {
        Admin::response_error(CM_lang::line('CM.ERROR_DATA_NOT_FOUND'));
      } else {
        if(!$this->_mod_dao->update_rows($returnMessage, $rReturn, array('password' => $this->_post['password_new']), $accountRows[Admin_DAO_Customers::COLUMN_RECORD_ID], Admin_DAO_Customers::GROUP_PASSWORD)) Admin::response_error($returnMessage);
        
        $mailSubject = sprintf(self::lang('PASSWORD_RESET_MAIL_SUBJECT'), CM_Lang::line('CM.SITE_NAME'));
        $mailBody = sprintf(self::lang('PASSWORD_RESET_MAIL_BODY'),
          $accountRows[Admin_DAO_Customers::COLUMN_HEADLINE],
          $this->_post['password_new']
        );
        $mailBody = CM_Template::get_tpl_html('mail_default', array(
          'mail_header' => $mailSubject,
          'mail_body' => $mailBody,
          'mail_footer' => Admin::get_mail_footer()
        ));
        Admin::mail($accountRows[Admin_DAO_Customers::COLUMN_HEADLINE], $accountRows['email'], $mailSubject, $mailBody);
        Admin::response_success(self::lang('PASSWORD_RESET_MAIL_SUCCESS'));
      }
    }
    
    public function response_default() {
      $this->response_list();
    }

    public function response_group_add() {
      $dao = Admin_Factory::createCMDAO('customers_Group');
      $dao->init_options();
      $this->_breadcrumb->add(self::lang('R_GROUP_LIST'), $this->get_request('group_list')->get_link());
      $this->_breadcrumb->add(self::lang('R_GROUP_ADD'), $this->get_request('group_add')->get_link());
      $thisForm = Admin_Factory::createCMForm();
      $thisForm->set_form_dao($dao, Admin_DAO_Customers_Group::GROUP_ADD);
      $thisForm->set_form_default(array('date' => date('Y-m-d')));
      $form = $this->get_request('group_add', 'save')->get_form('addForm');
      CM_Output::json('add_form_ok', true, Admin::format_templates_data(array_merge($form, array(
        'formButton' => 
          CM_Html::submit_btn(CM_Lang::line('Admin.BTN_ADD'), array('class' => 'btn btn-primary')) . '&nbsp;' . 
          CM_Html::reset_btn(CM_Lang::line('Admin.BTN_CANCEL'), array('class' => 'btn prevBtn')) . '&nbsp;' .
          CM_Html::reset_btn(CM_Lang::line('Admin.BTN_RESET'), array('class' => 'btn')),
        'hiddenColumn' => array(CM_Html::hidden_field(Admin::VAR_BACK_URL, $this->_get[Admin::VAR_BACK_URL])),
        'header' => array(
          'icon' => 'icofont-edit',
          'title' => self::lang('R_GROUP_ADD'),
          'intro' => self::lang('ADD_INTRO'),
          'breadcrumb' => $this->_breadcrumb->trail()
        ),
        'formGroup' => array(
          array('title' => self::lang('CUSTOMERS_GROUP_FORM_GROUP_INFO'), 'fields' => $thisForm->get_form(CM_Form::ACTION_INSERT))
        )
      ))));
    }
    
    public function response_group_add_save() {
      $dao = Admin_Factory::createCMDAO('customers_Group');

      if(!$dao->insert_rows($rMessage, $rReturn, $this->_post, Admin_DAO_Customers_Group::GROUP_ADD)) {
        Admin::response_error($rMessage);
      } else {
        $this->log_request($this->_post[Admin_DAO_Customers_Group::COLUMN_HEADLINE], $dao->to_column_group_rows(Admin_DAO_Customers_Group::GROUP_ADD, $this->_post));
        CM_Output::json($rMessage, true, array('url' => isset($this->_post[Admin::VAR_BACK_URL]) ? Admin::filter_input($this->_post[Admin::VAR_BACK_URL], false) : $this->get_request('group_list')->get_href()));
      }
    }
    
    public function response_group_edit() {
      $dao = Admin_Factory::createCMDAO('customers_Group');
      $dao->init_options();

      if(!isset($this->_get[Admin::VAR_RECORD_ID]) || !$dao->get_rows_byId($recordRows, Admin_DAO_Customers_Group::GROUP_EDIT, $this->_get[Admin::VAR_RECORD_ID])) Admin::response_error(CM_Lang::line('Admin.ERROR_NOT_FOUND'));
      
      $this->_breadcrumb->add(self::lang('R_GROUP_LIST'), $this->get_request('group_list')->get_link());
      $this->_breadcrumb->add(self::lang('R_GROUP_EDIT'), $this->get_request('group_edit')->get_link());
      $thisForm = Admin_Factory::createCMForm();
      $thisForm->set_form_dao($dao, Admin_DAO_Customers_Group::GROUP_EDIT);
      $thisForm->set_form_default($recordRows);
      $form = $this->get_request('group_edit', 'save')->set_params(array(
        Admin::VAR_RECORD_ID => $recordRows[Admin_DAO_Customers_Group::COLUMN_RECORD_ID]
      ))->get_form('editForm');
      
      CM_Output::json('update_form_ok', true, Admin::format_templates_data(array_merge($form, array(
        'hiddenColumn' => array(
          CM_Html::hidden_field(Admin::VAR_BACK_URL, $this->_get[Admin::VAR_BACK_URL]),
          CM_Html::hidden_field(Admin_DAO_Customers_Group::COLUMN_RECORD_ID, $recordRows[Admin_DAO_Customers_Group::COLUMN_RECORD_ID])
        ),
        'formButton' => 
          CM_Html::submit_btn(CM_Lang::line('Admin.BTN_EDIT'), array('class' => 'btn btn-primary')) . '&nbsp;' . 
          CM_Html::reset_btn(CM_Lang::line('Admin.BTN_CANCEL'), array('class' => 'btn prevBtn')) . '&nbsp;' .
          CM_Html::reset_btn(CM_Lang::line('Admin.BTN_RESET'), array('class' => 'btn')),
        'header' => array(
          'icon' => 'icofont-edit',
          'title' => self::lang('R_GROUP_EDIT'),
          'intro' => self::lang('EDIT_INTRO'),
          'breadcrumb' => $this->_breadcrumb->trail()
        ),
        'formGroup' => array(
          array('title' => self::lang('CUSTOMERS_GROUP_FORM_GROUP_INFO'), 'fields' => $thisForm->get_form(CM_Form::ACTION_INSERT))
        )
      ))));
    }
    
    public function response_group_edit_save() {
      $dao = Admin_Factory::createCMDAO('customers_Group');

      if(!isset($this->_get[Admin::VAR_RECORD_ID]) || !$dao->update_rows($rMessage, $rReturn, $this->_post, $this->_get[Admin::VAR_RECORD_ID], Admin_DAO_Customers_Group::GROUP_EDIT)) {
        Admin::response_error($rMessage);
      } else {
        $this->log_request($this->_post[Admin_DAO_Customers_Group::COLUMN_HEADLINE], $dao->to_column_group_rows(Admin_DAO_Customers_Group::GROUP_EDIT, $this->_post));
        CM_Output::json($rMessage, true, array('url' => isset($this->_post[Admin::VAR_BACK_URL]) ? Admin::filter_input($this->_post[Admin::VAR_BACK_URL], false) : $this->get_request('group_list')->get_href()));
      }
    }
    
    public function response_group_remove() {
      $dao = Admin_Factory::createCMDAO('customers_Group');

      if(!isset($this->_get[Admin::VAR_RECORD_ID]) || !$dao->delete_rows($rMessage, $rReturn, $this->_get[Admin::VAR_RECORD_ID])) {
        Admin::response_error($rMessage);
      } else {
        $this->log_request($rReturn['currentRows'][Admin_DAO_Customers_Group::COLUMN_HEADLINE], $rReturn['currentRows']);
        CM_Output::json($rMessage, true, array('url' => isset($_GET[Admin::VAR_BACK_URL]) ? Admin::filter_input($_GET[Admin::VAR_BACK_URL], false) : $this->get_request('group_list')->get_href()));
      }
    }
    
    public function response_group_remove_list() {
      if(!isset($this->_post['listActionId']) || empty($this->_post['listActionId'])) Admin::response_error(CM_Lang::line('Admin.LIST_ACTION_ID_EMPTY'));

      $errorMessage = array();
      $error = false;
      $log = array();
      $dao = Admin_Factory::createCMDAO('customers_Group');
      
      foreach($this->_post['listActionId'] as $index => $id) {
        if(!$dao->delete_rows($rMessage, $rReturn, $id)) {
          $error = true;
          $errorMessage[] = $rMessage;
        } else {
          $log[] = implode('<br />', array_map(function($element) {
            return sprintf('<b>%s</b>: %s', $element['id'], $element['text']);
          }, array(
            array('id' => Admin_DAO_Customers_Group::COLUMN_RECORD_ID, 'text' => $id),
            array('id' => Admin_DAO_Customers_Group::COLUMN_HEADLINE, 'text' => $rReturn['currentRows'][Admin_DAO_Customers_Group::COLUMN_HEADLINE]),
          )));
        }
      }
      
      if($error == true) {
        Admin::response_error(implode('<br />', $errorMessage));
      } else {
        $this->log_request(self::lang('R_GROUP_REMOVE_L_INTRO'), array('remove' => implode('<br /><br />', $log)));
        Admin::response_success(self::lang('REMOVE_L_SUCCESS'), true);
      }
    }
    
    public function response_group_list() {
      $this->_breadcrumb->add(self::lang('R_GROUP_LIST'), $this->get_request('group_list')->get_link());
      $cbThat = $this;
      $dao = Admin_Factory::createCMDAO('customers_Group');
      $dao->init_options();
      
      if(isset($this->_get['keyword']) && !empty($this->_get['keyword'])) {
        $this->_breadcrumb->add(
          sprintf(CM_Lang::line('Admin.LIST_SEARCH_KEYWORD'), $this->_get['keyword']),
          $this->get_request('group_list')
            ->set_params(Admin::get_all_get_params(array(), true))
            ->set_text(sprintf(CM_Lang::line('Admin.LIST_SEARCH_KEYWORD'), $this->_get['keyword']))
            ->get_link()
        );
      }
      
      $listColumn = array_merge($dao->get_column_field_label_array(Admin_DAO_Customers_Group::GROUP_LIST), array(
        'action' => CM_Lang::line('Admin.LIST_ACTION')
      ));
      $daoParams = array(
        'sort' => Admin_Mod::to_sort($dao, $this->_get['sc'], $this->_get['sf']),
        'keyword' => $this->_get['keyword']
      );
      $listJSON = $dao->get_array_object(Admin_DAO_Customers_Group::GROUP_LIST, array_merge($daoParams, array(
        'keyColumn' => Admin_DAO_Customers_Group::COLUMN_RECORD_ID,
        'oNowPage' => isset($this->_get['p']) ? (int)$this->_get['p'] : 1,
        'oPageCallback' => function($page, array $params, $key) use($cbThat) {
          return $cbThat->cb_default_pagesplit($page, $params, $key, $cbThat->get_request('group_list'), array('vpage' => 'p'));
        }
      )));
      
      CM_Output::json('list_ok', true, Admin::format_templates_data(array(
        'header' => array(
          'icon' => 'icofont-th-list',
          'title' => self::lang('R_GROUP_LIST'),
          'intro' => self::lang('R_GROUP_LIST_INTRO')
        ),
        'breadcrumb' => $this->_breadcrumb->trail(),
        'list' => Admin_Mod::to_tmpl_table_list($listJSON, $listColumn, array(
          'addon' => array(),
          'id' => "{$this->_current_request['mod']}List",
          'title' => CM_Lang::line('Admin.LIST_ACTION_MENU'),
          'action' => array(
            'checkboxFunc' => array(
              'title' => CM_Lang::line('Admin.LIST_ACTION_MENU'),
              'list' => array(
                array(
                  'link' => $this->get_request('group_remove', 'list')->get_href(),
                  'text' => CM_Lang::line('Admin.LIST_ACTION_ID_REMOVE'),
                  'confirm' => CM_lang::line('Admin.LIST_ACTION_ID_REMOVE_CONFIRM')
                )
              )
            ),
            'listFunc' => array(
              $this->get_request('group_add')
                ->set_params(array(Admin::VAR_BACK_URL => urlencode($this->get_request('group_list')->get_href(Admin::get_all_get_params(array(), true)))))
                ->set_text_format('<i class="icofont-plus"></i>&nbsp;%s')
                ->get_btn(array('class' => 'btn btn-primary'))
            ),
            'search' => array_merge(array(
              'formSearch' => CM_Html::input_field('keyword', $this->_get['keyword']),
              'formButton' => CM_Html::submit_btn('<i class="icofont-search"></i>&nbsp;' . CM_Lang::line('Admin.BTN_SEARCH'), array('class' => 'btn'))
            ), $this->get_request('group_list')->get_form('searchForm', array('method' => 'get', 'class' => 'form-inline')))
          ),
          'isSearch' => Admin::not_null($this->_get['keyword']),
          'callback' => array(
            'permission' => function($record, $recordRows = array()) use($dao) {
              if(empty($record)) return '--';

              $permissionOptions = $dao->get_options('permission');
              return implode('、', array_map(function($element) use($permissionOptions) {
                return Admin::get_options_name($element, $permissionOptions);
              }, explode(',', $record)));
            },
            'catalog' => function($record, $recordRows = array()) use($dao) {
              if(!empty($record)) {
                $options = $dao->get_options('catalog');
                return implode('<br /><br />', array_map(function($element) use($options) {
                  return Admin::get_options_name($element, $options);
                }, explode(',', $record)));
              } else {
                return '--';
              }
            },
            'action' => function($record, $recordRows = array()) use($cbThat) {
              $action = array();
              //修改或刪除回來的網址
              $backurl = urlencode($cbThat->get_request('group_list')->get_href(Admin::get_all_get_params(array(Admin::VAR_RECORD_ID, Admin::VAR_ACTION), true)));
              //修改
              $thisRequestEdit = $cbThat->get_request('group_edit')->set_params(array(
                Admin::VAR_RECORD_ID => $recordRows[Admin_DAO_Customers_Group::COLUMN_RECORD_ID],
                Admin::VAR_BACK_URL => $backurl
              ));
              //刪除
              $thisRequestRemove = $cbThat->get_request('group_remove')->set_params(array(
                Admin::VAR_RECORD_ID => $recordRows[Admin_DAO_Customers_Group::COLUMN_RECORD_ID],
                Admin::VAR_BACK_URL => $backurl
              ));
              
              if($thisRequestEdit->check_permission()) {
                $action[] = $thisRequestEdit->get_link(array('class' => 'link'), sprintf('<i class="icofont-edit"></i>&nbsp;%s', CM_Lang::line('Admin.LIST_ACTION_EDIT')));
              }
              
              if($thisRequestRemove->check_permission()) {
                $action[] = $thisRequestRemove->get_link(array(
                  'class' => 'link',
                  'confirmMsg' => sprintf($cbThat->lang('REMOVE_CONFIRM'), $recordRows['email'])
                ), sprintf('<i class="icofont-trash"></i>&nbsp;%s', CM_Lang::line('Admin.LIST_ACTION_REMOVE')));
              }
              
              return implode('&nbsp;|&nbsp;', $action);
            }
          ),
          'h_callback' => function($rows, $name) use($cbThat) {
            return $cbThat->cb_default_sort($rows, $name, $cbThat->get_request('group_list'), array(
              'link_params' => $cbThat->_get,
              'sort_column' => array(Admin_DAO_Customers_Group::COLUMN_RECORD_ID, Admin_DAO_Customers_Group::COLUMN_HEADLINE, 'permission')
            ));
          }
        ))
      )));
    }
  }
?>