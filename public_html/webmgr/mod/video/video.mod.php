<?php
  class modVideo extends Admin_Mod {
    const MODULE_NAME = 'video';
    
    public $_mod_dao;

    public $_pop_mode = false;
    
    static public function lang($key) {
      return CM_Lang::line(self::MODULE_NAME . '.' . $key, '', CM_Conf::get('PATH_F.ADMIN_MOD') . self::MODULE_NAME . '/');
    }
    
    public function __construct($modName, $requestName = '', $actionName = '') {
      parent::__construct($modName, $requestName, $actionName);
      //設定功能-預設功能
      $this->add_request('default', self::lang('R_MOD'), self::lang('R_INTRO'));
      //設定功能-消息列表
      $this->add_request('list', self::lang('R_LIST'), self::lang('R_LIST_INTRO'));
      //設定功能-新增消息
      $this->add_request('add', self::lang('R_ADD'), self::lang('R_ADD_INTRO'));
      $this->add_request_sub('add', 'save', self::lang('R_ADD_SAVE_INTRO'));
	  $this->add_request('add_video', self::lang('R_ADD'), self::lang('R_ADD_INTRO'));
      $this->add_request_sub('add_video', 'save', self::lang('R_ADD_SAVE_INTRO'));
      //設定功能-編輯消息
      $this->add_request('edit', self::lang('R_EDIT'), self::lang('R_EDIT_INTRO'));
      $this->add_request_sub('edit', 'save', self::lang('R_EDIT_SAVE_INTRO'));
      $this->add_request_sub('edit', 'list_status', self::lang('R_EDIT_L_STATUS_INTRO'));

      $this->add_request('catalog_edit', self::lang('R_EDIT_CATALOG'), self::lang('R_EDIT_CATALOG_INTRO'), array(
        'attrParams' => array('isHistory' => false)
      ));
      $this->add_request_sub('catalog_edit', 'save', self::lang('R_EDIT_CATALOG_SAVE_INTRO'));
      //設定功能-移除消息
      $this->add_request('remove', self::lang('R_REMOVE'), self::lang('R_REMOVE_INTRO'), array(
        'attrParams' => array(
          'isHistory' => false,
          'isConfirm' => true,
          'confirmMsg' => self::lang('REMOVE_CONFIRM')
        )
      ));
      $this->add_request_sub('remove', 'list', self::lang('R_REMOVE_L_INTRO'));
      $this->add_request('export', self::lang('R_EXPORT'), self::lang('R_INTRO'), array(
        'attrParams' => array(
          'ajax' => false,
          'data-target' => 'blank'
        )
      ));
	  $this->add_request('download', self::lang('R_XML_SAMPLE'), self::lang('R_INTRO'), array(
        'attrParams' => array(
          'ajax' => false,
          'data-target' => 'blank'
        )
      ));
	 

      $this->add_request('pop_list', self::lang('R_LIST'), self::lang('R_LIST_INTRO'));
      $this->add_request('pop_add', self::lang('R_ADD'), self::lang('R_ADD_INTRO'));
      $this->add_request_sub('pop_add', 'save', self::lang('R_ADD_SAVE_INTRO'));
      $this->add_request('pop_edit', self::lang('R_EDIT'), self::lang('R_EDIT_INTRO'));
      $this->add_request_sub('pop_edit', 'save', self::lang('R_EDIT_SAVE_INTRO'));
      $this->add_request_sub('pop_edit', 'list_status', self::lang('R_EDIT_L_STATUS_INTRO'));
      $this->add_request('pop_catalog_edit', self::lang('R_EDIT_CATALOG'), self::lang('R_EDIT_CATALOG_INTRO'), array(
        'attrParams' => array('isHistory' => false)
      ));
      $this->add_request_sub('pop_catalog_edit', 'save', self::lang('R_EDIT_CATALOG_SAVE_INTRO'));
      $this->add_request('pop_remove', self::lang('R_REMOVE'), self::lang('R_REMOVE_INTRO'), array(
        'attrParams' => array(
          'isHistory' => false,
          'isConfirm' => true,
          'confirmMsg' => self::lang('REMOVE_CONFIRM')
        )
      ));
      $this->add_request_sub('pop_remove', 'list', self::lang('R_REMOVE_L_INTRO'));

      $this->add_request('import', self::lang('R_IMPORT'), self::lang('R_IMPORT_INTRO'), array(
        'attrParams' => array(
          'ajax' => false,
          'data-target' => 'blank'
        )
      ));
      $this->add_request('import_org', self::lang('R_IMPORT_ORG'), self::lang('R_IMPORT_ORG_INTRO'), array(
        'attrParams' => array(
          'ajax' => false,
          'data-target' => 'blank'
        )
      ));
      //設定Breadcrumb
      $this->_breadcrumb->add(self::lang('R_MOD'), $this->get_request('default')->get_link(array(), sprintf('<i class="icofont-flag"></i> %s', self::lang('R_MOD'))));
      $this->_mod_dao = Admin_Factory::createCMDAO(self::MODULE_NAME);
    }

    public function get_request($requestName, $actionName = '') {
      if($this->_pop_mode == true) {
        $requestName = "pop_{$requestName}";
        $request = parent::get_request($requestName, $actionName);
        return $request->set_attr_params(array(
          'isHistory' => false,
          'nolock' => true
        ));
      } else {
        return parent::get_request($requestName, $actionName);
      }
    }
    
    public function response_load_module() {
      parent::response_load_module();
    }
    
    public function response_add($responseParams = array()) {
      $responseParams = array_merge(array(
        'pop' => false, //跳出模式
      ), $responseParams);
      $this->_pop_mode = $responseParams['pop'];
      $this->_mod_dao->init_options($this->_session['allow_video_catalog']);
      $thisForm = Admin_Factory::createCMForm();
      $thisForm->set_form_dao($this->_mod_dao, Admin_DAO_Video::GROUP_ADD);
      $thisForm->set_form_default(array(
        //'release_date' => date('Y-m-d'),
		'release_date' => date('Y-m'),
        'start_date' => date('Y-m-d H:00:00'),
        //'end_date' => date('Y-m-d H:00:00', strtotime('now +5 year')),
		//原本設2099，但不知道為什麼驗證會不過???
		'end_date' => date('2037-m-d H:00:00'),
      ));
      $form = $this->get_request('add', 'save')->get_form('addForm');

      $data = array();
      $data['hiddenColumn'] = array(
        CM_Html::hidden_field(Admin::VAR_BACK_URL, $this->_get[Admin::VAR_BACK_URL])
      );
      $data['formGroup'] = $thisForm->get_groups_form(array(
        //'info' => array('title' => self::lang('FORM_GROUP_INFO')),
        //'setting' => array('title' => self::lang('FORM_GROUP_SETTING'))
		'add_info' => array('title' => self::lang('FORM_GROUP_INFO')),
        'add_setting' => array('title' => self::lang('FORM_GROUP_SETTING'))
      ), CM_Form::ACTION_INSERT);

      if($responseParams['pop'] == false) {
        $this->_breadcrumb->add(self::lang('R_LIST'), $this->get_request('list')->get_link());
        $this->_breadcrumb->add(self::lang('R_ADD'), $this->get_request('add')->get_link());
        $data['header'] = array(
          'icon' => 'icofont-edit',
          'title' => self::lang('R_ADD'),
          'intro' => self::lang('ADD_INTRO'),
          'breadcrumb' => $this->_breadcrumb->trail()
        );
        $data['formButton'] = implode('&nbsp;', array(
          //CM_Html::submit_btn(CM_Lang::line('Admin.BTN_ADD'), array('class' => 'btn btn-primary')),
		  CM_Html::submit_btn('下一步', array('class' => 'btn btn-primary')),
          CM_Html::reset_btn(CM_Lang::line('Admin.BTN_CANCEL'), array('class' => 'btn prevBtn')),
          CM_Html::reset_btn(CM_Lang::line('Admin.BTN_RESET'), array('class' => 'btn'))
        ));
      } else {
        $data['formButton'] = implode('&nbsp;', array(
          CM_Html::submit_btn(CM_Lang::line('Admin.BTN_ADD'), array('class' => 'btn btn-primary')),
          sprintf('<a data-history="false" class="request btn" href="%s">%s</a>', urldecode($this->_get[Admin::VAR_BACK_URL]), CM_Lang::line('Admin.BTN_CANCEL'))
        ));
      }

      CM_Output::json('add_form_ok', true, Admin::format_templates_data(array_merge($form, $data)));
    }
    
    public function response_add_save($responseParams = array()) {
      $responseParams = array_merge(array(
        'pop' => false, //跳出模式
      ), $responseParams);
      //$this->_post['owner'] = $this->_session[Admin::VAR_ADMIN_ID];
	  
	  //流程修正處理
	  $this->_post = array_merge($this->_post, array(
        'owner' => $this->_session[Admin::VAR_ADMIN_ID],
        'status' => '1',
        'lang_status' => '1'
      ));
	  
	  //例外處理
	  if( $this->_post['release_date']!=null){
		//只有年要補00
		//年月要補日
		if(strlen (  $this->_post['release_date'] )==4){
			 $this->_post['release_date']=date( $this->_post['release_date']."-01-01");
		}else if(strpos(  $this->_post['release_date'],"-")>0){
			 //取值
			 $year= substr($this->_post['release_date'], 0, 4);
			 $month= substr($this->_post['release_date'], 5, 2);
			 $this->_post['release_date']=date( $year."-".$month."-01");
		}else{
			 $this->_post['release_date']=null;
		}
	  }

      if(!$this->_mod_dao->insert_rows($rMessage, $rReturn, $this->_post, Admin_DAO_Video::GROUP_ADD)) {
        Admin::response_error($rMessage);
      } else {
        $this->log_request($this->_post[Admin_DAO_Video::COLUMN_HEADLINE], $this->_mod_dao->to_column_group_rows(Admin_DAO_Video::GROUP_ADD, $this->_post));
        $this->_pop_mode = $responseParams['pop'];
        //CM_Output::json($rMessage, true, array('url' => isset($this->_post[Admin::VAR_BACK_URL]) ? Admin::filter_input($this->_post[Admin::VAR_BACK_URL], false) : $this->get_request('list')->get_href()));
		
		CM_Output::json($rMessage, true, array(
          'url' => $this->get_request('add_video')
            ->set_params(array(
              Admin::VAR_RECORD_ID => $rReturn['insertId'],
              Admin::VAR_BACK_URL => isset($this->_post[Admin::VAR_BACK_URL]) ? Admin::filter_input($this->_post[Admin::VAR_BACK_URL], false) : $this->get_request('list')->get_href()
            ))
            ->get_href()
        ));
      }
    }
	
	//上傳影片
	public function response_add_video($responseParams = array()) {
      $responseParams = array_merge(array(
        'pop' => false, //跳出模式
      ), $responseParams);
      $this->_pop_mode = $responseParams['pop'];
      $this->_mod_dao->init_options($this->_session['allow_video_catalog']);

      $langId = isset($this->_get[Admin::VAR_LANG]) ? $this->_get[Admin::VAR_LANG] : CM_Lang::get_current_id(true);
      
      if(!isset($this->_get[Admin::VAR_RECORD_ID]) || !$this->_mod_dao->get_rows_byId($recordRows, Admin_DAO_Video::GROUP_EDIT, $this->_get[Admin::VAR_RECORD_ID], array(
        'lang' => $langId,
        'allowCatalog' => $this->_session['allow_video_catalog']
      ))) Admin::response_error(CM_Lang::line('Admin.ERROR_NOT_FOUND'));

      $thisForm = Admin_Factory::createCMForm();
      $thisForm->set_form_dao($this->_mod_dao, 'video');
      $thisForm->set_form_default($recordRows);
      $form = $this->get_request('add_video', 'save')->set_params(array(
        Admin::VAR_RECORD_ID => $recordRows[Admin_DAO_Video::COLUMN_RECORD_ID]
      ))->get_form('editForm');

      $data = array();
      $data['hiddenColumn'] = array(
        CM_Html::hidden_field(Admin::VAR_LANG, $langId),
        CM_Html::hidden_field(Admin::VAR_BACK_URL, $this->_get[Admin::VAR_BACK_URL]),
        CM_Html::hidden_field(Admin_DAO_Video::COLUMN_RECORD_ID, $recordRows[Admin_DAO_Video::COLUMN_RECORD_ID])
      );
      
      $data['formGroup'] = $thisForm->get_groups_form(array(
        'add_video' => array('title' => '影片檔案')
      ), CM_Form::ACTION_UPDATE);
      $this->_breadcrumb->add(self::lang('R_LIST'), $this->get_request('list')->get_link());
      $this->_breadcrumb->add(self::lang('R_EDIT'), $this->get_request('edit')->get_link());
      $data['header'] = array(
        'icon' => 'icofont-edit',
        'title' => self::lang('R_EDIT'),
        'intro' => self::lang('EDIT_INTRO'),
        'breadcrumb' => $this->_breadcrumb->trail()
      );
      $data['formButton'] = implode('&nbsp;', array(
        CM_Html::submit_btn(CM_Lang::line('Admin.BTN_ADD'), array('class' => 'btn btn-primary')),
        CM_Html::reset_btn(CM_Lang::line('Admin.BTN_CANCEL'), array('class' => 'btn prevBtn')),
        CM_Html::reset_btn(CM_Lang::line('Admin.BTN_RESET'), array('class' => 'btn'))
      ));
      CM_Output::json('update_form_ok', true, Admin::format_templates_data(array_merge($form, $data)));
    }
	
	//上傳影片後續
	public function response_add_video_save($responseParams = array()) {
      $responseParams = array_merge(array(
        'pop' => false, //跳出模式
      ), $responseParams);

      if(!isset($this->_get[Admin::VAR_RECORD_ID]) || !$this->_mod_dao->update_rows($rMessage, $rReturn, $this->_post, $this->_get[Admin::VAR_RECORD_ID], 'video')) {
        Admin::response_error($rMessage);
      } else {
        $this->log_request('多媒體檔案新增 - 上傳檔案', $this->_mod_dao->to_column_group_rows('video', $this->_post));
        $this->_pop_mode = $responseParams['pop'];
        CM_Output::json($rMessage, true, array('url' => isset($this->_post[Admin::VAR_BACK_URL]) ? Admin::filter_input($this->_post[Admin::VAR_BACK_URL], false) : $this->get_request('list')->get_href()));
      }
    }
	
    public function response_edit($responseParams = array()) {
      $responseParams = array_merge(array(
        'pop' => false, //跳出模式
      ), $responseParams);
      $this->_pop_mode = $responseParams['pop'];

      $langId = isset($this->_get[Admin::VAR_LANG]) ? $this->_get[Admin::VAR_LANG] : CM_Lang::get_current_id(true);
      
      if(!isset($this->_get[Admin::VAR_RECORD_ID]) || !$this->_mod_dao->get_rows_byId($recordRows, Admin_DAO_Video::GROUP_EDIT, $this->_get[Admin::VAR_RECORD_ID], array(
        'lang' => $langId,
        'allowCatalog' => $this->_session['allow_video_catalog']
      ))) Admin::response_error(CM_Lang::line('Admin.ERROR_NOT_FOUND'));

      $this->_mod_dao->init_options($this->_session['allow_video_catalog']);
      $thisForm = Admin_Factory::createCMForm();
      $thisForm->set_form_dao($this->_mod_dao, Admin_DAO_Video::GROUP_EDIT);
	  
	  //例外
	  if($recordRows['release_date']!=null){
		$recordRows['release_date']=date('Y-m',strtotime($recordRows['release_date']));
	  }
	  
      $thisForm->set_form_default($recordRows);
      $form = $this->get_request('edit', 'save')->set_params(array(
        Admin::VAR_RECORD_ID => $recordRows[Admin_DAO_Video::COLUMN_RECORD_ID]
      ))->get_form('editForm');
      $formLang = Admin_Mod::to_tmpl_lang_catalog($this->get_request('edit'), array('current' => $langId, Admin::VAR_BACK_URL => urldecode($_GET[Admin::VAR_BACK_URL])));

      $data = array();
      $data['hiddenColumn'] = array(
        CM_Html::hidden_field(Admin::VAR_LANG, $langId),
        CM_Html::hidden_field(Admin::VAR_BACK_URL, $this->_get[Admin::VAR_BACK_URL]),
        CM_Html::hidden_field(Admin_DAO_Video::COLUMN_RECORD_ID, $recordRows[Admin_DAO_Video::COLUMN_RECORD_ID])
      );
      $data['hasLang'] = count($formLang) > 1 ? true : false;
      $data['formLang'] = $formLang;
      $data['formGroup'] = $thisForm->get_groups_form(array(
        'info' => array('title' => self::lang('FORM_GROUP_INFO')),
        'setting' => array('title' => self::lang('FORM_GROUP_SETTING'))
      ), CM_Form::ACTION_UPDATE);

      if($responseParams['pop'] == false) {
        $this->_breadcrumb->add(self::lang('R_LIST'), $this->get_request('list')->get_link());
        $this->_breadcrumb->add(self::lang('R_EDIT'), $this->get_request('edit')->get_link());
        $data['header'] = array(
          'icon' => 'icofont-edit',
          'title' => self::lang('R_EDIT'),
          'intro' => self::lang('EDIT_INTRO'),
          'breadcrumb' => $this->_breadcrumb->trail()
        );
        $data['formButton'] = implode('&nbsp;', array(
          CM_Html::submit_btn(CM_Lang::line('Admin.BTN_EDIT'), array('class' => 'btn btn-primary')),
          CM_Html::reset_btn(CM_Lang::line('Admin.BTN_CANCEL'), array('class' => 'btn prevBtn')),
          CM_Html::reset_btn(CM_Lang::line('Admin.BTN_RESET'), array('class' => 'btn'))
        ));
      } else {
        $data['formButton'] = implode('&nbsp;', array(
          CM_Html::submit_btn(CM_Lang::line('Admin.BTN_EDIT'), array('class' => 'btn btn-primary')),
          sprintf('<a data-history="false" class="request btn" href="%s">%s</a>', urldecode($this->_get[Admin::VAR_BACK_URL]), CM_Lang::line('Admin.BTN_CANCEL'))
        ));
      }
      
      CM_Output::json('update_form_ok', true, Admin::format_templates_data(array_merge($form, $data)));
    }
    
    public function response_edit_save($responseParams = array()) {
      $responseParams = array_merge(array(
        'pop' => false, //跳出模式
      ), $responseParams);
	  
	  //例外處理
	  if( $this->_post['release_date']!=null){
		//只有年要補月日，年月要補日
		if(strlen ($this->_post['release_date'] )==4){
			 $this->_post['release_date']=date( $this->_post['release_date']."-01-01");
		}else if(strpos(  $this->_post['release_date'],"-")>0){
			 //取值
			 $year= substr($this->_post['release_date'], 0, 4);
			 $month= substr($this->_post['release_date'], 5, 2);
			 $this->_post['release_date']=date( $year."-".$month."-01");
		}
	  }

      if(!isset($this->_get[Admin::VAR_RECORD_ID]) || !$this->_mod_dao->update_rows($rMessage, $rReturn, $this->_post, $this->_get[Admin::VAR_RECORD_ID], Admin_DAO_Video::GROUP_EDIT)) {
        Admin::response_error($rMessage);
      } else {
        $this->log_request($this->_post[Admin_DAO_Video::COLUMN_HEADLINE], $this->_mod_dao->to_column_group_rows(Admin_DAO_Video::GROUP_EDIT, $this->_post));
        $this->_pop_mode = $responseParams['pop'];
        CM_Output::json($rMessage, true, array('url' => isset($this->_post[Admin::VAR_BACK_URL]) ? Admin::filter_input($this->_post[Admin::VAR_BACK_URL], false) : $this->get_request('list')->get_href()));
      }
    }
    
    public function response_edit_list_status($responseParams = array()) {
      $responseParams = array_merge(array(
        'pop' => false, //跳出模式
      ), $responseParams);

      $error = false;
      $errorMessage = array();
      $editIdArray = array();

      if(isset($this->_get[Admin::VAR_RECORD_ID])) {
        $editIdArray[] = (int)$this->_get[Admin::VAR_RECORD_ID];
      } elseif(isset($this->_post['listActionId']) && !empty($this->_post['listActionId'])) {
        $editIdArray = array_map(function($element) { return (int)$element; }, $this->_post['listActionId']);
      } else {
        $error = true;
      }

      if($error == true) Admin::response_error(CM_Lang::line('Admin.LIST_ACTION_ID_EMPTY'));

      $status = isset($this->_get[Admin_DAO_Video::COLUMN_STATUS]) ? (int)$this->_get[Admin_DAO_Video::COLUMN_STATUS] : '2';
      
      foreach($editIdArray as $index => $id) {
        if(!$this->_mod_dao->update_rows($rMessage, $rReturn, array(Admin_DAO_Video::COLUMN_STATUS => $status), $id, Admin_DAO_Video::GROUP_STATUS)) {
          $error = true;
          $errorMessage[] = $rMessage;
        } else {
          $log[] = implode('<br />', array_map(function($element) {
            return sprintf('<b>%s</b>: %s', $element['id'], $element['text']);
          }, array(
            array('id' => Admin_DAO_Video::COLUMN_RECORD_ID, 'text' => $id),
            array('id' => Admin_DAO_Video::COLUMN_STATUS, 'text' => $status),
            array('id' => Admin_DAO_Video::COLUMN_HEADLINE, 'text' => $rReturn['currentRows'][Admin_DAO_Video::COLUMN_HEADLINE]),
          )));
        }
      }

      if($error == true) {
        Admin::response_error(implode('<br />', $errorMessage));
      } else {
        $this->log_request(self::lang('R_EDIT_CATALOG_SAVE_INTRO'), array('edit' => implode('<br /><br />', $log)));
        Admin::response_success(self::lang('EDIT_L_SUCCESS'), true);
      }
    }

    public function response_catalog_edit($responseParams = array()) {
      $responseParams = array_merge(array(
        'pop' => false, //跳出模式
      ), $responseParams);

      $error = false;
      $errorMessage = array();
      $editIdArray = array();

      if(isset($this->_get[Admin::VAR_RECORD_ID])) {
        $editIdArray[] = (int)$this->_get[Admin::VAR_RECORD_ID];
      } elseif(isset($this->_post['listActionId']) && !empty($this->_post['listActionId'])) {
        $editIdArray = array_map(function($element) { return (int)$element; }, $this->_post['listActionId']);
      } else {
        $error = true;
      }

      if($error == true) Admin::response_error(CM_Lang::line('Admin.LIST_ACTION_ID_EMPTY'));

      $this->_mod_dao->init_options($this->_session['allow_video_catalog']);
      $form = $this->get_request('catalog_edit', 'save')->get_form('catalogEditForm');
      CM_Output::json('catalog_edit_form_ok', true, Admin::format_templates_data(array(
        'mId' => 'catalogEditForm',
        'mTop' => implode('', array(
          $form['formTop'],
          CM_Html::hidden_field('recordIds', implode(',', $editIdArray))
        )) ,
        'mBottom' => $form['formBottom'],
        'mHeader' => self::lang('CATALOG_EDIT'),
        'mData' => array(
          'intro' => self::lang('CATALOG_EDIT_INTRO'),
          'fields' => array(array(
            'label' => Admin_DAO_Video::lang('COLUMN_CATALOG'),
            'field' => CM_Html::select_field('catalog[]', $this->_mod_dao->get_options('catalog'), $this->_get['catalog'], 'class="input-xlarge" multiple="multiple"')
          ))
        ),
        'mButton' => 
          CM_Html::submit_btn(self::lang('CATALOG_EDIT_BTN'), array(
            'class' => 'btn btn-primary'
          )) .
          CM_Html::btn(CM_Lang::line('Admin.BTN_CANCEL'), 'button', array(
            'class' => 'btn',
            'data-dismiss' => 'modal',
            'aria-hidden'=> 'true'
          ))
      )));
    }

    public function response_catalog_edit_save($responseParams = array()) {
      if(!isset($this->_post['recordIds']) && empty($this->_post['recordIds'])) Admin::response_error(CM_Lang::line('Admin.LIST_ACTION_ID_EMPTY'));
      //分類處理
      if(isset($this->_post['catalog']) && is_array($this->_post['catalog'])) {
        $allowCatalog = $this->_session['allow_video_catalog'];
        $catalogAll = in_array('-1', $allowCatalog) ? true : false;
        $catalog = array_filter(array_map(function($element) use($catalogAll, $allowCatalog) {
          if($catalogAll == false && !in_array($element, $allowCatalog)) return false;

          return (int)$element;
        }, $this->_post['catalog']));
      } else {
        $catalg = array();
      }

      foreach(explode(',', $this->_post['recordIds']) as $id) {
        if(!$this->_mod_dao->update_rows($rMessage, $rReturn, array('catalog' => $catalog), $id, Admin_DAO_Video::GROUP_CATALOG)) {
          $error = true;
          $errorMessage[] = $rMessage;
        } else {
          $log[] = implode('<br />', array_map(function($element) {
            return sprintf('<b>%s</b>: %s', $element['id'], $element['text']);
          }, array(
            array('id' => Admin_DAO_Video::COLUMN_RECORD_ID, 'text' => $id),
            array('id' => 'catalog', 'text' => $rReturn['currentRows']['catalog']),
            array('id' => Admin_DAO_Video::COLUMN_HEADLINE, 'text' => $rReturn['currentRows'][Admin_DAO_Video::COLUMN_HEADLINE]),
          )));
        }
      }

      if($error == true) {
        Admin::response_error(implode('<br />', $errorMessage));
      } else {
        $this->log_request(self::lang('R_EDIT_L_STATUS_INTRO'), array('edit' => implode('<br /><br />', $log)));
        Admin::response_success(self::lang('EDIT_L_SUCCESS'), true);
      }
    }
    
    public function response_remove($responseParams = array()) {
      $responseParams = array_merge(array(
        'pop' => false, //跳出模式
      ), $responseParams);

      if(!isset($this->_get[Admin::VAR_RECORD_ID]) || !$this->_mod_dao->delete_rows($rMessage, $rReturn, $this->_get[Admin::VAR_RECORD_ID])) {
        Admin::response_error($rMessage);
      } else {
        $this->log_request($rReturn['currentRows'][Admin_DAO_Video::COLUMN_HEADLINE], $rReturn['currentRows']);
        $this->_pop_mode = $responseParams['pop'];
        CM_Output::json($rMessage, true, array('url' => isset($_GET[Admin::VAR_BACK_URL]) ? Admin::filter_input($_GET[Admin::VAR_BACK_URL], false) : $this->get_request('list')->get_href()));
      }
    }
    
    public function response_remove_list($responseParams = array()) {
      $responseParams = array_merge(array(
        'pop' => false, //跳出模式
      ), $responseParams);

      if(!isset($this->_post['listActionId']) || empty($this->_post['listActionId'])) Admin::response_error(CM_Lang::line('Admin.LIST_ACTION_ID_EMPTY'));

      $errorMessage = array();
      $error = false;
      $log = array();
      
      foreach($this->_post['listActionId'] as $index => $id) {
        if(!$this->_mod_dao->delete_rows($rMessage, $rReturn, $id)) {
          $error = true;
          $errorMessage[] = $rMessage;
        } else {
          $log[] = implode('<br />', array_map(function($element) {
            return sprintf('<b>%s</b>: %s', $element['id'], $element['text']);
          }, array(
            array('id' => Admin_DAO_Video::COLUMN_RECORD_ID, 'text' => $id),
            array('id' => Admin_DAO_Video::COLUMN_HEADLINE, 'text' => $rReturn['currentRows'][Admin_DAO_Video::COLUMN_HEADLINE]),
          )));
        }
      }
      
      if($error == true) {
        Admin::response_error(implode('<br />', $errorMessage));
      } else {
        $this->log_request(self::lang('R_REMOVE_L_INTRO'), array('remove' => implode('<br /><br />', $log)));
        Admin::response_success(self::lang('REMOVE_L_SUCCESS'), true);
      }
    }
    
    public function response_list($responseParams = array()) {
      $responseParams = array_merge(array(
        'pop' => false, //跳出模式
      ), $responseParams);
      $cbThat = $this;
      $this->_pop_mode = $responseParams['pop'];
      $this->_mod_dao->init_options($this->_session['allow_video_catalog']);
      $this->_breadcrumb->add(self::lang('R_LIST'), $this->get_request('list')->get_link());
      $searchColumn = array('catalog', 'copyright', 'startDate', 'endDate', 'keyword','login_status','attr_2','startReleaseDate','endReleaseDate');
      $isSearch = false;

      foreach($searchColumn as $column) {
        if(!empty($this->_get[$column])) {
          $isSearch = true;
          break;
        }
      }

      if($isSearch == true) {
        $this->_breadcrumb->add(CM_Lang::line('Admin.TOP_SEARCH_BREADCRUMB'), $this->get_request('list')->set_params(Admin::get_all_get_params(array(), true))->get_link());
        $daoParams = array(
          'keyword' => $this->_get['keyword'],
          'date' => array(
            'start' => $this->_get['startDate'],
            'end' => $this->_get['endDate']
          ),
		  'releaseDate' => array(
            'start' => $this->_get['startReleaseDate'],
            'end' => $this->_get['endReleaseDate']
          )
        );

        if(isset($this->_get['catalog']) && Admin::in_options($this->_get['catalog'], $this->_mod_dao->get_options('catalog'), $currentOptions)) $daoParams['catalog'] = $currentOptions['id'];

        if(isset($this->_get['copyright']) && Admin::in_options($this->_get['copyright'], $this->_mod_dao->get_options('copyright'), $currentOptions)) $daoParams['copyright'] = $currentOptions['id'];
		
		if(isset($this->_get['attr_2']) && Admin::in_options($this->_get['attr_2'], $this->_mod_dao->get_options('attr_2'), $currentOptions)) $daoParams['attr_2'] = $currentOptions['id'];
		
		if(isset($this->_get['login_status']) && Admin::in_options($this->_get['login_status'], $this->_mod_dao->get_options('login_status'), $currentOptions)) $daoParams['login_status'] = $currentOptions['id'];
		
      } else {
        $daoParams = array();
      }

      $currentStatus = isset($this->_get[Admin_DAO_Video::COLUMN_STATUS]) ? $this->_get[Admin_DAO_Video::COLUMN_STATUS] : '';
      $daoParams = array_merge($daoParams, array(
        'status' => $currentStatus,
        'allowCatalog' => $this->_session['allow_video_catalog'],
        'sort' => Admin_Mod::to_sort($this->_mod_dao, $this->_get['sc'], $this->_get['sf'])
      ));
      $listColumn = array_merge($this->_mod_dao->get_column_field_label_array(Admin_DAO_Video::GROUP_LIST), array(
        'action' => CM_Lang::line('Admin.LIST_ACTION')
      ));
	  //150526:Ann:把file加回來(顯示檔名)
	  //unset($listColumn['file']);
      //$listJSON = $this->_mod_dao->get_array_object(Admin_DAO_Video::GROUP_LIST, array_merge($daoParams, array(
	  $listJSON = $this->_mod_dao->get_array_object(Admin_DAO_Video::GROUP_LIST_DATA, array_merge($daoParams, array(
        'fileCallback' => true,
        'keyColumn' => Admin_DAO_Video::COLUMN_RECORD_ID,
        'oNowPage' => isset($this->_get['p']) ? (int)$this->_get['p'] : 1,
        'oPageCallback' => function($page, array $params, $key) use($cbThat) {
          return Admin_Mod::cb_default_pagesplit($page, $params, $key, $cbThat->get_request('list'), array('vpage' => 'p'));
        }
      )));
      $data = array();
      $listFunc = array();
      /*$listFunc[] = $this->get_request('add')
        ->set_params(array(
          Admin::VAR_BACK_URL => urlencode($this->get_request('list')->get_href(Admin::get_all_get_params(array('type'), true)))
        ))
        ->set_text_format('<i class="icofont-plus"></i>&nbsp;%s')
        ->get_btn(array('class' => 'btn btn-primary'));*/
      $actionAddon = '
        <div class="bar-st bar-gradient green" style="display: none;" data-cmfield-func="file-process">
          <p>0%</p>
          <span style="width: 0%;"></span>
        </div>
      ';

      if($responseParams['pop'] == false) {
		//移到裡面
		$listFunc[] = $this->get_request('add')
          ->set_params(array(
            Admin::VAR_BACK_URL => urlencode($this->get_request('list')->get_href(Admin::get_all_get_params(array('type'), true)))
          ))
          ->set_text_format('<i class="icofont-plus"></i>&nbsp;%s')
          ->get_btn(array('class' => 'btn btn-primary'));
        $listFunc[] = CM_Html::func_btn(sprintf('<i class="icofont-plus"></i>&nbsp;%s<span data-cmfunc="process"></span>', $this->lang('R_ADD_LIST')), array(
          'class' => 'btn',
          'data-cmfield-func' => 'file-upload',
          'data-cmurl' => Admin::href_admin_link('controller.php', Admin::VAR_ACTION . '=uploadVideos')
        ));
        $listFunc[] = CM_Html::func_btn(sprintf('<i class="icofont-upload"></i>&nbsp;%s<span data-cmfunc="process"></span>', $this->lang('R_IMPORT')), array(
          'class' => 'btn',
          'data-cmfunc' => 'import_finish',
          'data-cmurl' => Admin::href_admin_link('controller.php')
        ));
        $listFunc[] = CM_Html::func_btn(sprintf('<i class="icofont-upload"></i>&nbsp;%s<span data-cmfunc="process"></span>', $this->lang('R_IMPORT_ORG')), array(
          'class' => 'btn',
          'data-cmfunc' => 'import_org_finish',
          'data-cmurl' => Admin::href_admin_link('controller.php')
        ));
        $listFunc[] = $this->get_request('export')
          ->set_params($this->_get)
          ->set_text_format('<i class="icofont-share"></i>&nbsp;%s')
          ->get_btn(array('class' => 'btn'));
		$listFunc[] = $this->get_request('download')
          ->set_params($this->_get)
          ->set_text_format('<i class="icofont-share"></i>&nbsp;%s')
          ->get_btn(array('class' => 'btn'));
        $data['header'] = array(
          'icon' => 'icofont-th-list',
          'title' => self::lang('R_LIST'),
          'intro' => self::lang('R_LIST_INTRO')
        );
        $data['breadcrumb'] = $this->_breadcrumb->trail();
        $catalog = Admin_Mod::to_tmpl_catalog($this->get_request('list')->set_params(Admin::get_all_get_params(array(Admin_DAO_Video::COLUMN_STATUS), true)), $this->_mod_dao, array(
          'daoParams' => $daoParams,
          'linkParams' => Admin::get_all_get_params(array(Admin_DAO_Video::COLUMN_STATUS), true),
          'catalog' => $currentStatus,
          'catalogColumn' => Admin_DAO_Video::COLUMN_STATUS,
          'options' => $this->_mod_dao->get_options('status')
        ));
      } else {
        $listFunc[] = CM_Html::func_btn('<i class="icofont-plus"></i>&nbsp;' . $this->lang('SELECTED_INSERT'), array('class' => 'btn btn-success', 'data-cmfunc' => 'selected-insert'));
        $actionAddon = '';
        $catalog = array();
      }

      $ownerCache = array();
      $ownerDAO = Admin_Factory::createCMDAO('account');
      $data['list'] = Admin_Mod::to_tmpl_table_list($listJSON, $listColumn, array(
        'addon' => $catalog,
        'id' => "{$this->_current_request['mod']}List",
        'title' => CM_Lang::line('Admin.LIST_ACTION_MENU'),
        'action' => array(
          'funcSize' => '11',
          'searchSize' => '1',
          'checkboxFunc' => array(
            'title' => CM_Lang::line('Admin.LIST_ACTION_MENU'),
            'list' => array(
              array(
                'link' => $this->get_request('edit', 'list_status')->set_params(array(Admin_DAO_Video::COLUMN_STATUS => '2'))->get_href(),
                'text' => CM_Lang::line('Admin.LIST_ACTION_ID_DISABLE'),
                'confirm' => CM_lang::line('Admin.LIST_ACTION_ID_DISABLE_CONFIRM')
              ),
              array(
                'link' => $this->get_request('edit', 'list_status')->set_params(array(Admin_DAO_Video::COLUMN_STATUS => '1'))->get_href(),
                'text' => CM_Lang::line('Admin.LIST_ACTION_ID_ENABLE'),
                'confirm' => CM_lang::line('Admin.LIST_ACTION_ID_ENABLE_CONFIRM')
              ),
              array('separate' => true),
              array(
                'link' => $this->get_request('catalog_edit')->get_href(),
                'text' => self::lang('R_EDIT_CATALOG')
              ),
              array('separate' => true),
              array(
                'link' => $this->get_request('remove', 'list')->get_href(),
                'text' => CM_Lang::line('Admin.LIST_ACTION_ID_REMOVE'),
                'confirm' => CM_lang::line('Admin.LIST_ACTION_ID_REMOVE_CONFIRM')
              )
            )
          ),
          'listFunc' => $listFunc,
          'actionAddon' => $actionAddon,
          'search' => array()
        ),
        'isSearch' => Admin::not_null($this->_get['keyword']),
        'callback' => array(
          Admin_DAO_Video::COLUMN_RECORD_ID => function($record, $recordRows) use($cbThat, $responseParams) {
            if($responseParams['pop'] == false) {
              return $record;
            } else {
              return sprintf("<span data-cmdata='%s'>%s</span>", Admin::json_encode(Admin_Dgfactor::to_select_item_rows($recordRows)), $record);
            }
          },
          /*'file' => function($record, $recordRows = array()) {
            //return Admin_Format::to_upload_file_thumbnail('file', $recordRows);
			return Admin_Format::to_upload_file_thumbnail('file_img', $recordRows);
          },*/
		  'file'=>function($record, $recordRows = array()) {
			$endlen = strrpos($record,'.');
			$record = substr($record,0,$endlen);
			$temp_file_name=str_replace('video/','',$record);
			return $temp_file_name!=null?$temp_file_name."_1.mp4":"--";
            //return '/new_Content/'.$temp_file_name;
          },
          'file_size' => function($record, $recordRows = array()) {
            return Admin_Format::to_bytes_size($record);
          },
          'file_img' => function($record, $recordRows = array()) {
            return Admin_Format::to_upload_file_thumbnail('file_img', $recordRows);
          },
          'process_ok' => function($record, $recordRows = array()) use($cbThat) {
            $options = $cbThat->_mod_dao->get_options('process_ok');
            $processLabel = array(
              2 => 'label-success',
              3 => 'label-important',
              4 => 'label-info'
            );

            if(Admin::in_options($record, $options, $currentOptions)) {
              return sprintf('<span class="label %s">%s</span>', $processLabel[$currentOptions['id']], $currentOptions['text']);
            } else {
              return '--';
            }
          },
          'owner' => function($record, $recordRows = array()) use(&$ownerCache, $ownerDAO) {
            if(!isset($ownerCache[$record])) $ownerCache[$record] = $ownerDAO->get_rows_byId($rows, '', $record) ? $rows[Admin_DAO_Account::COLUMN_RECORD_ID] . ' - ' . $rows[Admin_DAO_Account::COLUMN_HEADLINE] : '';

            return $ownerCache[$record];
          },
          'desc' => function($record, $recordRows = array()) {
            return Admin::substr($record, 50);
          },
          'status' => function($record, $recordRows = array()) use($cbThat) {
            $options = $cbThat->_mod_dao->get_options('status');
            $statusLabel = array(
              1 => 'label-success',
              2 => 'label-important'
            );

            if(Admin::in_options($record, $options, $currentOptions)) {
              return $cbThat->get_request('edit', 'list_status')
                ->set_params(array(
                  Admin_DAO_Video::COLUMN_STATUS => $currentOptions['id'] == '1' ? '2' : '1',
                  Admin::VAR_RECORD_ID => $recordRows[Admin_DAO_Video::COLUMN_RECORD_ID]
                ))
                ->set_text(sprintf('<span class="label %s">%s</span>', $statusLabel[$currentOptions['id']], $currentOptions['text']))
                ->get_link(array('isHistory' => false));
            } else {
              return '--';
            }
          },
          'action' => function($record, $recordRows = array()) use($cbThat) {
            $action = array();
            //修改或刪除回來的網址
            $backurl = urlencode($cbThat->get_request('list')->get_href(Admin::get_all_get_params(array(Admin::VAR_RECORD_ID, Admin::VAR_ACTION), true)));
            //修改
            $thisRequestEdit = $cbThat->get_request('edit')->set_params(array(
              'type' => $cbThat->_type['id'],
              Admin::VAR_RECORD_ID => $recordRows[Admin_DAO_Video::COLUMN_RECORD_ID],
              Admin::VAR_BACK_URL => $backurl
            ));
            //刪除
            $thisRequestRemove = $cbThat->get_request('remove')->set_params(array(
              'type' => $cbThat->_type['id'],
              Admin::VAR_RECORD_ID => $recordRows[Admin_DAO_Video::COLUMN_RECORD_ID],
              Admin::VAR_BACK_URL => $backurl
            ));
            
            if($thisRequestEdit->check_permission()) {
              $action[] = $thisRequestEdit->get_link(array('class' => 'link'), sprintf('<i class="icofont-edit"></i>&nbsp;%s', CM_Lang::line('Admin.LIST_ACTION_EDIT')));
            }
            
            if($thisRequestRemove->check_permission()) {
              $action[] = $thisRequestRemove->get_link(array(
                'class' => 'link',
                'confirmMsg' => sprintf($cbThat->lang('REMOVE_CONFIRM'), $recordRows[Admin_DAO_Video::COLUMN_HEADLINE])
              ), sprintf('<i class="icofont-trash"></i>&nbsp;%s', CM_Lang::line('Admin.LIST_ACTION_REMOVE')));
            }
            
            return implode('&nbsp;|&nbsp;', $action);
          }
        ),
        'h_callback' => function($rows, $name) use($cbThat) {
          $attr = array(
            'owner' => 'width="110"',
            'start_date' => 'width="100"',
            'end_date' => 'width="100"',
            'desc' => 'width="200"'
          );
          
          if(isset($attr[$name])) $rows['attr'] = $attr[$name];

          return Admin_Mod::cb_default_sort($rows, $name, $cbThat->get_request('list'), array(
            'link_params' => $cbThat->_get,
            'sort_column' => array(Admin_DAO_Video::COLUMN_RECORD_ID, Admin_DAO_Video::COLUMN_HEADLINE, 'video_length', 'file', 'file_size', 'file_img', 'owner', 'process_ok', 'start_date', 'end_date', Admin_DAO_Video::COLUMN_STATUS)
          ));
        }
      ));
	  //搜尋表單
      $data['topSearch'] = array_merge(array(
        'title' => CM_Lang::line('Admin.TOP_SEARCH'),
        'fields' => array(           
          array(              
            array(
              'label' => self::lang('TOP_SEARCH_COLUMN_DATE'),
              'field' => 
                CM_Html::input_field('startDate', $this->_get['startDate'], 'class="hasDatepicker input-small"') . '&nbsp;~&nbsp;' .
                CM_Html::input_field('endDate', $this->_get['endDate'], 'class="hasDatepicker input-small"'),
              'attr' => 'class="span8"'
            ),
            array(
              'label' => self::lang('TOP_SEARCH_COLUMN_KEYWORD'),
              'field' => CM_Html::input_field('keyword', $this->_get['keyword'], 'class="input-large"'),
              'attr' => 'class="span4"'
            )
          ), 
          array(
            array(
              'label' => self::lang('TOP_SEARCH_COLUMN_CATALOG'),
              'field' => CM_Html::select_field('catalog', array_merge(array(
                array('id' => '', 'text' => '&nbsp;')
              ), $this->_mod_dao->get_options('catalog')), $this->_get['catalog'], 'class="input-xxlarge"'),
              'attr' => 'class="span8"'
            ),
            array(
              'label' => self::lang('TOP_SEARCH_COPYRIGHT'),
              'field' => CM_Html::select_field('copyright', array_merge(array(
                array('id' => '', 'text' => '&nbsp;')
              ), $this->_mod_dao->get_options('copyright')), $this->_get['copyright'], 'class="input-large"'),
              'attr' => 'class="span4"'
            )
          ),
		  array(              
            array(
              'label' => self::lang('TOP_SEARCH_COLUMN_RELEASE_DATE'),
              'field' => 
                CM_Html::input_field('startReleaseDate', $this->_get['startReleaseDate'], 'class="hasDatepicker input-small"') . '&nbsp;~&nbsp;' .
                CM_Html::input_field('endReleaseDate', $this->_get['endReleaseDate'], 'class="hasDatepicker input-small"'),
              'attr' => 'class="span8"'
            ),
            array(
              'label' => self::lang('TOP_SEARCH_COLUMN_LANGUAGE'),
              'field' => CM_Html::select_field('attr_2', array_merge(array(
                array('id' => '', 'text' => '&nbsp;')
              ), $this->_mod_dao->get_options('attr_2')), $this->_get['attr_2'], 'class="input-large"'),
              'attr' => 'class="span4"'
            )
          ),
		  array(              
            array(
              'label' => self::lang('TOP_SEARCH_COLUMN_LOGIN_STATUS'),
              'field' => CM_Html::select_field('login_status', array_merge(array(
                array('id' => '', 'text' => '&nbsp;')
              ), $this->_mod_dao->get_options('login_status')), $this->_get['login_status'], 'class="input-xlarge"'),
              'attr' => 'class="span8"'
            )
          ), 
        ),
        'submitBtn' => CM_Lang::line('Admin.TOP_SEARCH_BTN')
      ), $this->get_request('list')->get_form('searchForm', array('method' => 'get', 'class' => 'form-inline')));

      CM_Output::json('list_ok', true, Admin::format_templates_data($data));
    }

    public function response_import() {
      $fileDAO = Admin_Factory::createCMDAO('file');
      $ftpPath = CM_Conf::get('PATH_F.UPLOAD') . 'ftp/';
      $XMLfile = $_FILES['files']['tmp_name'];
      $XMLfile_size = $_FILES['files']['size'];
      
      if(empty($_FILES['files'])) {
        Admin::response_error(CM_Lang::line('CM.ERROR_UPLOAD_EMPTY'));
      } elseif(Admin::get_file_ext($_FILES['files']['name']) != 'xml') {
        Admin::response_error(self::lang('IMPORT_ERROR_1'));
      } else {
        $errorMessage = array();
        $updateCount = 0;
        $xml = simplexml_load_file($XMLfile);
        $items = $xml->Video;

        if(empty($items)) Admin::response_error(self::lang('IMPORT_ERROR_2'));
        
        foreach($items AS $itemsRows) {
          $itemsRows = array_map(function($element) {
            return (string)$element;
          }, (array)$itemsRows);

          if(!empty($itemsRows['catalog'])) {
            $itemsRows['catalog'] = explode(',', $itemsRows['catalog']);
          } else {
            $itemsRows['catalog'] = array();
          }

          $itemsRows['owner'] = $this->_session[Admin::VAR_ADMIN_ID];
          $pathWFolder = 'video/' . date('Ym') . '/';
          $pathFFolder = CM_Conf::get('PATH_F.UPLOAD') . $pathWFolder;
          $orgFilePath = $ftpPath . $itemsRows['file'];
          $newFileName = time() . substr(microtime(), 2, 5) . '.' . Admin::get_file_ext($itemsRows['file']);
          $newFilePath = $pathFFolder . $newFileName;

          if(empty($itemsRows['file'])) {
            $errorMessage[] = self::lang('IMPORT_ERROR_3');
          } elseif(!is_dir($pathFFolder) && !mkdir($pathFFolder)) {
            $errorMessage[] = sprintf(self::lang('IMPORT_ERROR_4'), $pathFFolder);
          } elseif(!is_file($orgFilePath)) {
            $errorMessage[] = sprintf(self::lang('IMPORT_ERROR_5'), $orgFilePath);
          } elseif(!copy($orgFilePath, $newFilePath)) {
		  //} elseif(!rename($orgFilePath, $newFilePath)) {
            $errorMessage[] = sprintf(self::lang('IMPORT_ERROR_6'), $orgFilePath, $newFilePath);
          } elseif(!$fileDAO->insert_rows($rMessage, $rReturn, array(
            Admin_DAO_File::COLUMN_HEADLINE => $itemsRows[Admin_DAO_Video::COLUMN_HEADLINE],
            'desc' => '',
            'size' => filesize($orgFilePath),
            'file' => $pathWFolder . $newFileName,
            'link' => '0',
			//150428:ANN:加了音樂後多的欄位
			'dao' => 'video'
          ), Admin_DAO_File::GROUP_ADD)) {
            $errorMessage[] = $rMessage;
          } elseif(!$this->_mod_dao->insert_rows($rMessage, $rReturn, array_merge(
            $itemsRows,
            array(
              'file' => $pathWFolder . $newFileName,
              'file_img' => '',
              'lang_status' => '1'
            )
		  //150317:ANN:寫入欄位修正
          //), Admin_DAO_Video::GROUP_ADD)) {
		  ), Admin_DAO_Video::GROUP_ADD_MASS)) {
            $errorMessage[] = $rMessage;
          } else {
            // @unlink($orgFilePath);
            $updateCount ++;
          }
        }
        
        if(empty($errorMessage)) {
          CM_Output::json(sprintf(self::lang('IMPORT_SUCCESS_1'), $updateCount), true);
        } else {
          Admin::response_error(sprintf(self::lang('IMPORT_SUCCESS_2'),
            $updateCount,
            count($errorMessage),
            implode('<br />', $errorMessage)
          ));
        }
      }
    }

    public function response_import_org() {
      $fileDAO = Admin_Factory::createCMDAO('file');
      $filePath = CM_Conf::get('PATH_F.ROOT') . 'new_Content/';
      $XMLfile = $_FILES['files']['tmp_name'];
      $XMLfile_size = $_FILES['files']['size'];
      
      if(empty($_FILES['files'])) {
        Admin::response_error(CM_Lang::line('CM.ERROR_UPLOAD_EMPTY'));
      } elseif(Admin::get_file_ext($_FILES['files']['name']) != 'xml') {
        Admin::response_error(self::lang('IMPORT_ERROR_1'));
      } else {
        $errorMessage = array();
        $updateCount = 0;
        $xml = simplexml_load_file($XMLfile);
        $items = $xml->Video;

        if(empty($items)) Admin::response_error(self::lang('IMPORT_ERROR_2'));
        
        foreach($items AS $itemsRows) {
          $itemsRows = array_map(function($element) {
            return (string)$element;
          }, (array)$itemsRows);

          if(!empty($itemsRows['catalog'])) {
            $itemsRows['catalog'] = explode(',', $itemsRows['catalog']);
          } else {
            $itemsRows['catalog'] = array();
          }

          $itemsRows['owner'] = $this->_session[Admin::VAR_ADMIN_ID];

          if(empty($itemsRows['file'])) {
            $errorMessage[] = self::lang('IMPORT_ERROR_3');
          } else {
            $fileName = explode('.', $itemsRows['file']);
            $fileExt = array_pop($fileName);
            $fileName = implode('.', $fileName);
            $orgFile = "{$fileName}_1.{$fileExt}";
            $orgFilePath = $filePath . $orgFile;
            $newFileName = 'video/' . $itemsRows['file'];

            if(!is_file($orgFilePath)) {
              $errorMessage[] = sprintf(self::lang('IMPORT_ERROR_5'), $orgFilePath);
            } elseif(!$fileDAO->insert_rows($rMessage, $rReturn, array(
              Admin_DAO_File::COLUMN_HEADLINE => $itemsRows[Admin_DAO_Video::COLUMN_HEADLINE],
              'desc' => '',
              'size' => filesize($orgFilePath),
              'file' => $newFileName,
              'link' => '0',
			  //150428:ANN:加了音樂後多的欄位
			  'dao' => 'video'
			//150317:ANN:寫入欄位修正
            //), Admin_DAO_File::GROUP_ADD)) {
			), Admin_DAO_Video::GROUP_ADD_MASS)) {
              $errorMessage[] = $rMessage."ADD_MASS";
            } elseif(!$this->_mod_dao->insert_rows($rMessage, $rReturn, array_merge(
              $itemsRows,
              array(
                'file' => $newFileName,
                'file_img' => '',
				'file_size' => filesize($orgFilePath),
                'lang_status' => '1',
                'process_ok' => '2'
              )
            ), Admin_DAO_Video::GROUP_ORG_ADD)) {
              $errorMessage[] = $rMessage."ADD";
            } else {
              $updateCount ++;
            }
          }
        }
        
        if(empty($errorMessage)) {
          CM_Output::json(sprintf(self::lang('IMPORT_SUCCESS_1'), $updateCount), true);
        } else {
          Admin::response_error(sprintf(self::lang('IMPORT_SUCCESS_2'),
            $updateCount,
            count($errorMessage),
            implode('<br />', $errorMessage)
          ));
        }
      }
    }    
	
	public function response_download(){
		//var_dump("download");
		//$file_name="video2.xml";
		$file_name="xml.zip";
		$file = CM_Conf::get('PATH_F.ROOT').CM_Conf::get('PATH.WEBMGR').$file_name ;
		//$file = basename($file);
		if(file_exists($file)){
			header("Expires: 0");
			header("Pragma: no-cache");
			header("Cache-Control: no-store, no-cache, must-revalidate, post-check=0, pre-check=0");
			header("Content-Type: application/force-download");
			header("Content-Type: application/octet-stream");
			header("Content-Type: application/download");
			header('Content-disposition: attachment; filename=' . basename($file));
			header("Content-Type: application/pdf");
			header("Content-Transfer-Encoding: binary");
			header('Content-Length: ' . filesize($file));
			@readfile($file);
			exit;
		}
	}

    public function response_export() {
      $cbThat = $this;
      $ownerCache = array();
      $ownerDAO = Admin_Factory::createCMDAO('account');
      $this->_mod_dao->init_options($this->_session['allow_video_catalog']);
      $daoParams = array(
        'status' => $this->_get['status'],
        'allowCatalog' => $this->_session['allow_video_catalog'],
        'keyword' => $this->_get['keyword'],
        'date' => array(
          'start' => $this->_get['startDate'],
          'end' => $this->_get['endDate']
        )
      );

      if(isset($this->_get['catalog']) && Admin::in_options($this->_get['catalog'], $this->_mod_dao->get_options('catalog'), $currentOptions)) $daoParams['catalog'] = $currentOptions['id'];

      if(isset($this->_get['catalog']) && Admin::in_options($this->_get['copyright'], $this->_mod_dao->get_options('copyright'), $currentOptions)) $daoParams['copyright'] = $currentOptions['id'];

      $catalogOptions = $this->_mod_dao->get_options('catalog');
      $listColumn = $this->_mod_dao->get_column_field_label_array(Admin_DAO_Video::GROUP_EXPORT);
      $listColumnFormat = array(
		'attr_2' => function($record, $recordRows = array()) use($cbThat) {
          return Admin::get_options_name($record, $cbThat->_mod_dao->get_options('attr_2'));
        },
        'copyright' => function($record, $recordRows = array()) use($cbThat) {
          return Admin::get_options_name($record, $cbThat->_mod_dao->get_options('copyright'));
        },
        'status' => function($record, $recordRows = array()) use($cbThat) {
          return Admin::get_options_name($record, $cbThat->_mod_dao->get_options('status'));
        },
        'login_status' => function($record, $recordRows = array()) use($cbThat) {
          return Admin::get_options_name($record, $cbThat->_mod_dao->get_options('login_status'));
        },
        'copyright_status' => function($record, $recordRows = array()) use($cbThat) {
          return Admin::get_options_name($record, $cbThat->_mod_dao->get_options('copyright_status'));
        },
        'video_original' => function($record, $recordRows = array()) use($cbThat) {
          return Admin::get_options_name($record, $cbThat->_mod_dao->get_options('video_original'));
        },
        'process_ok' => function($record, $recordRows = array()) use($cbThat) {
          return Admin::get_options_name($record, $cbThat->_mod_dao->get_options('process_ok'));
        },
        'catalog' => function($record, $recordRows = array()) use($cbThat, $catalogOptions) {
          if(!empty($record)) {
            return implode(", ", array_map(function($element) use($catalogOptions) {
              return Admin::get_options_name($element, $catalogOptions);
            }, explode(',', $record)));
          } else {
            return '--';
          }
        },
        'owner' => function($record, $recordRows = array()) use(&$ownerCache, $ownerDAO) {
          if(!isset($ownerCache[$record])) $ownerCache[$record] = $ownerDAO->get_rows_byId($rows, '', $record) ? $rows[Admin_DAO_Account::COLUMN_RECORD_ID] . ' - ' . $rows[Admin_DAO_Account::COLUMN_HEADLINE] : '';

          return $ownerCache[$record];
        },
		//不確定是否報價
		/*'desc' => function($record, $recordRows = array()) use($cbThat) {
			$data = str_replace('<p>','',$record);
			$data = str_replace('</p>','<br style="mso-data-placement:same-cell;" />',$data);
			//$data = str_replace('</p>','&#10;',$data);
			return str_replace('<br />','<br style="mso-data-placement:same-cell;" />',$data);
        }*/
		 
      );
      
      header('Content-type:text/html; charset=utf-8');
      header('Content-Type: application/octet-stream');
      header("Content-Disposition: attachment; filename={$this->_current_request['mod']}_export.xls;");
      echo '<HTML xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40">';
      echo '<head><meta http-equiv="content-type" content="application/vnd.ms-excel; charset=UTF-8"></head>';
      echo '<body><table border="1">';
      echo '<tr>';
      
      foreach($listColumn as $columnName => $columnLabel) echo "<td>{$columnLabel}</td>";
      
      echo '</tr>';
      
      if($this->_mod_dao->get_array($recordArray, Admin_DAO_Video::GROUP_EXPORT, $daoParams)) {
        $recordArray = CM_Format::to_list_array_format($recordArray, $listColumn, $listColumnFormat);
        
        foreach($recordArray as $recordRows) {
          echo '<tr>';
          
          foreach($listColumn as $columnName => $columnLabel) echo "<td>{$recordRows[$columnName]}</td>";
          
          echo '</tr>';
        }
      }
      
      echo '</table></body></html>';
      exit;
    }
    
    public function response_default() {
      $this->response_list();
    }

    public function response_pop_add() {
      $this->response_add(array('pop' => true));
    }

    public function response_pop_add_save() {
      $this->response_add_save(array('pop' => true));
    }

    public function response_pop_edit() {
      $this->response_edit(array('pop' => true));
    }

    public function response_pop_edit_save() {
      $this->response_edit_save(array('pop' => true));
    }

    public function response_pop_edit_list_status() {
      $this->response_edit_list_status(array('pop' => true));
    }

    public function response_pop_catalog_edit() {
      $this->response_catalog_edit(array('pop' => true));
    }

    public function response_pop_catalog_edit_save() {
      $this->response_catalog_edit_save(array('pop' => true));
    }

    public function response_pop_remove() {
      $this->response_remove(array('pop' => true));
    }

    public function response_pop_remove_list() {
      $this->response_remove_list(array('pop' => true));
    }

    public function response_pop_list() {
      $this->response_list(array('pop' => true));
    }
  
    public function response_video_uploadVideos() {
      $fileDAO = Admin_Factory::createCMDAO('file');
      $this->_mod_dao->init_options($this->_session['allow_video_catalog']);
      $fileColumn = $this->_mod_dao->get_column('file');
      
      try {
		//上傳路徑修正
        //$path = Admin::upload_file($_FILES['files'], date('Ym'), array(
        $path = Admin::upload_file($_FILES['files'], 'video/'.date('Ym'), array(
          'max' => $fileColumn->fu_options['maxFileSize'],
          'ext' => $fileColumn->fu_options['acceptFileTypes']
        ));
      } catch(CM_Exception $e) {
        Admin::response_error($e->getMessage());
      }
      
      if(empty($this->_post[Admin_DAO_File::COLUMN_HEADLINE])) $this->_post[Admin_DAO_File::COLUMN_HEADLINE] = $_FILES['files']['name'];

      $insertRows = array_merge($this->_post, array(
        'desc' => '',
        'size' => $_FILES['files']['size'],
        'file' => $path,
        'link' => '0'
      ));
      
      if($fileDAO->insert_rows($rMessage, $rReturn, $insertRows, Admin_DAO_File::GROUP_ADD)) {
        $videoData = array(
          'owner' => $this->_session[Admin::VAR_ADMIN_ID],
          'headline' => $_FILES['files']['name'],
          //'status' => '2',
		  'status' => '1',
          'file' => $path,
          'type' => $this->_post['type'],
          'lang_status' => '1',
          'start_date' => date('Y-m-d H:i:s'),
          //'end_date' => date("Y-m-d H:i:s", mktime(0, 0, 0, date('m'), date('d'), date('Y') + 1))
		  'end_date' => date("2037-m-d H:i:s"),
		  'release_date' => date('Y-m-01')
        );
        
        if(!$this->_mod_dao->insert_rows($rMessage, $rReturn, $videoData, Admin_DAO_Video::GROUP_ADD_LIST)) {
          Admin::response_error($rMessage);
        } else {
          CM_Output::json($rMessage, true, array('url' => isset($this->_post[Admin::VAR_BACK_URL]) ? Admin::filter_input($this->_post[Admin::VAR_BACK_URL], false) : $this->get_request('list')->get_href()));
        }
      } else {
        Admin::response_error($rMessage);
      }
    }
  }
?>