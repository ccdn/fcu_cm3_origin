;(function($, cm, cmApi, cmLang) {
  var mMain = $(cm.setting.CONTAINER),
      mModName = 'video';
  
  if(typeof cm.mod[mModName] == 'object') {
    var mLang = cm.mod[mModName][cm.setting.VAR_LANGUAGES],
        mCallback = cm.mod[mModName][cm.setting.VAR_CALLBACK] = {};
    //加入模組預設的callback
    for(var k in cm.tmp.cb_mod) mCallback[k] = cm.tmp.cb_mod[k];
    //設定預設的callbcak
    mCallback.default_onLoad = function(data) {
      mCallback.list_onLoad(data);
    };
    mCallback.list_onLoad = function(data) {
      var jqMain = $('#columnCenter', $(cm.setting.CONTAINER));
      $('.boxList', cmApi.templates_create('column_center_listSingle', data.tplData, jqMain)).cmAdminList();

      if(typeof data.tplData.topSearch == 'object') {
        var jqTopSearch = cmApi.templates_create('list_topSearch', data.tplData.topSearch, $('div.top-search', jqMain));
      }

      $('form', jqMain).cmForm();
      
      $('div#videoList').on('click', '[data-cmfield-func="file-upload"]', function(e) {
        var uploadFormId = 'upload_videos',
            params = $(this).data(),
            acceptFileTypes = '/(\\.|\\/)(flv|mp4)$/i',
            maxNumberOfFiles = 1,
            maxFileSize = 20480000,
            minFileSize = 10;
        if($('#' + uploadFormId).length > 0) $('#' + uploadFormId).remove(); 
        if($('#' + uploadFormId).length <= 0) {
          //加入這個模組產生的唯一編號
          //_addTmpId(uploadFormId);
          //產生暫存表單
          var html = '<form id="' + uploadFormId + '" class="file-upload-modal-cache" method="post" enctype="multipart/form-data" style="display: none;" action="' + params.cmurl + '">';

          html += '<input type="hidden" name="type" value="' + params.type + '" />';
          html += '<input type="hidden" name="_request" value="video" />';
          html += '<input type="hidden" name="_module" value="video" />';
          html += '<input type="hidden" name="dao" value="Video">';
          //html += '<input type="hidden" name="column" value="file">';

          html += '<input type="file" name="files" multiple="multiple"/>';
          html += '</form>';

          $(html).prependTo('body').fileupload({
            sequentialUploads: true,
            limitConcurrentUploads: 1,
            dataType: 'json',
            add: function (e, data) {
              var success = true,
                  errorMsg = '',
                  reg = new RegExp(acceptFileTypes, 'i');
              
              for(var k in data.files) {
                var file = data.files[k];
                
                /*if(!(reg.test(file.type) || reg.test(file.name))) {
                  success = false;
                  errorMsg = cmLang.UPLOAD_FILE_ERROR_EXT;
                } else */if(maxFileSize && file.size > maxFileSize) {
                  success = false;
                  errorMsg = $.sprintf(cmLang.UPLOAD_FILE_ERROR_TOO_BIG, cmApi.format_file_size(file.size), cmApi.format_file_size(parseInt(maxFileSize, 10)));
                } else if (typeof file.size === 'number' && file.size < minFileSize) {
                  success = false;
                  errorMsg = $.sprintf(cmLang.UPLOAD_FILE_ERROR_TOO_SMALL, cmApi.format_file_size(file.size), cmApi.format_file_size(parseInt(minFileSize, 10)));
                }

                if(success == false) break;
              }

              if(success == true) {
                var jqProcess = $('[data-cmfield-func=file-process]', 'div#videoList');
                jqProcess.fadeIn();
                $('p', jqProcess).text('5%');
                $('span', jqProcess).width('0%');
                
                var jqXHR = data.submit().error(function (jqXHR, textStatus, errorThrown) {
                  cmApi.alert_message(jqXHR.responseText, 'error');
                  setTimeout(function() {
                    $('[data-cmfield-func="file-process"]', 'div#videoList').fadeOut();
                  }, 1500);
                });        
              } else {
                cmApi.alert_message(errorMsg, 'error');
              }
            },
            progressall: function (e, data) {
              var jqProcess = $('[data-cmfield-func=file-process]', 'div#videoList');
              var processText = parseInt(data.loaded / data.total * 100, 10) + '%';
              $('p', jqProcess).text(processText);
              $('span', jqProcess).width(processText);
            },
            done: function (e, data) {
              var jqProcess = $('[data-cmfield-func=file-process]', 'div#videoList');
              $('p', jqProcess).text('100%');
              $('span', jqProcess).width('100%');

              setTimeout(function() {
                jqProcess.fadeOut();
              }, 1500);

              if(data.result.success == true) {
                /*
                if($('li[data-cmfield-func="file-item"]', jqFileList).length >= maxNumberOfFiles) {
                  cmApi.alert_message(cmLang.UPLOAD_FILE_ERROR_COUNT, 'error');
                } else {
                  jqFileList.prepend(cmApi.templates_add('upload_files', data.result.files.shift()));
                }*/
                cmApi.alert_message(cmLang.UPLOAD_FILE_SUCCESS, 'success');
              } else {
                cmApi.alert_message(data.result.message, 'warring');
              }

              //jqUpload.trigger('cmuploadrefresh');
              
            }
          }).bind('fileuploadstop', function (e, data) {
            cmApi.call_server_api({
              history: true,
              url: cmApi.request_to_url({
                module: 'video',
                request: 'list',
                queryHashKey: {type: params.type}
              }),
              caller: this
            });
          });
        }

        $('input[type="file"]', $('#' + uploadFormId)).trigger('click');
      });
      $('[data-cmfunc="import_finish"], [data-cmfunc="import_org_finish"]', mMain).on('click', function(e) {
        var that = this,
            formId = 'importFinishForm',
            formParams = {},
            html = '<form id="' + formId + '" class="file-upload-modal-cache" method="post" enctype="multipart/form-data" style="display: none;" action="' + $(this).data('cmurl') + '">';
        formParams[cm.setting.VAR_MODULE] = mModName;
        formParams[cm.setting.VAR_REQUEST] = $(that).data('cmfunc') == 'import_finish' ? 'import' : 'import_org';

        for(var k in formParams) html += '<input type="hidden" name="' + k + '" value="' + formParams[k] + '" />';

        html += '<input type="file" name="files" />';
        //html += '<input type="file" name="files" multiple="multiple" />';
        html += '</form>';

        if($('#' + formId).length > 0) $('#' + formId).remove();

        $(html)
          .prependTo('body')
          .fileupload({
            sequentialUploads: true,
            limitConcurrentUploads: 1,
            dataType: 'json',
            add: function (e, data) {
              $('[data-cmfunc="process"]', that).text(' - 0%');
              var jqXHR = data.submit().error(function (jqXHR, textStatus, errorThrown) {
                cmApi.alert_message(jqXHR.responseText, 'error');
                $('[data-cmfunc="process"]', that).text('');
                $('#' + formId).remove();
              });
            },
            progressall: function (e, data) {
              var processText = parseInt(data.loaded / data.total * 100, 10) + '%';
              $('[data-cmfunc="process"]', that).text(' - ' + processText);
            },
            done: function (e, data) {
              $('[data-cmfunc="process"]', that).text(' - 100%');
              setTimeout(function() {
                $('[data-cmfunc="process"]', that).text('');
                $('#' + formId).remove();
              }, 3000);

              if(data.result.success == true) {
                cmApi.alert_message(data.result.message, 'success');
                cmApi.call_server_api({
                  history : true,
                  url : cmApi.request_to_url({
                    module : mModName,
                    request : 'list'
                  }),
                  param : {type : $(that).data('type')}
                });
              } else {
                cmApi.alert_message(data.result.message);
              }
            }
          })
          .find('input[type="file"]')
          .trigger('click');
      });
    };
	mCallback.add_video_onLoad = function(data) {
      cm.tmp.cb_mod.add_onLoad.call(this, data);
    };
    mCallback.add_video_save_onLoad = function(data) {
      cm.tmp.cb_mod.add_save_onLoad.call(this, data);
    };
    mCallback.catalog_edit_onLoad = function(data) {
      if($('#' + data.tplData.mId).length > 0) $('#' + data.tplData.mId).remove();
      
      data.tplData.mBody = cmApi.templates_add('modal_form', data.tplData.mData);
      var jqForm = $(cmApi.templates_add('modal', data.tplData)).prependTo('#mainBody');
      $('form', jqForm).cmForm();
      jqForm
        .modal({
          keyboard: false,
          show : true
        })
        .css('z-index', 1052)
        .on('shown', function(e) {
          if($('div.modal-backdrop').length > 1) $('div.modal-backdrop').eq($('div.modal-backdrop').length - 1).css('z-index', 1051);
        })
        .on('hidden', function() {
          jqForm.remove();
        });
    };
    mCallback.catalog_edit_save_onLoad = function(data) {
      var jqForm = $(this).parents('div.modal');
      jqForm.modal('hide');
      cmApi.alert_message(data.message, 'success');
    };
    mCallback.pop_list_onLoad = function(data) {
      if($(this).parents('div.modal').length > 0) {
        var jqModal = $(this).parents('div.modal');
      } else {
        var jqModal = cmApi.create_autoresize_modal({
              mTmpl: {mHeader: cmLang.SELECT_CONTENTS_HEADLINE}
            }),
            that = this;
        jqModal
          .on('click', '[data-cmfunc="selected-insert"]', function(e) {
            var jqList = $(this).parents('div.boxList'),
                jqListForm = $('form[name="listForm"]', jqList),
                jqCheckedItem = $('input[type="checkbox"][name="listActionId[]"]:checked', jqListForm);

            if(jqCheckedItem.length > 0) {
              var json = [];
              jqCheckedItem.each(function() {
                var itemJSON = $('span[data-cmdata]', $(this).parents('tr')).data('cmdata');

                if(itemJSON !== false) json.push(itemJSON);
              });
              that.cmSelectContents('add_item', json);
              $(this).parents('div.modal').modal('hide');
            } else {
              cmApi.alert_message(cmLang.SELECT_CONTENTS_ERROR_SELECTED_EMPTY);
            }
          })
          .modal('show');
      }

      var jqMain = $('div.modal-body', jqModal);
      $('.boxList', cmApi.templates_create('column_center_listSingle', data.tplData, jqMain)).cmAdminList();

      if(typeof data.tplData.topSearch == 'object') var jqTopSearch = cmApi.templates_create('list_topSearch', data.tplData.topSearch, $('div.top-search', jqMain));

      $('form', jqMain).cmForm();
    };
    mCallback.pop_add_onLoad = function(data) {
      cm.tmp.cb_mod.pop_add_onLoad.call(this, data);
    };
    mCallback.pop_add_save_onLoad = function(data) {
      cm.tmp.cb_mod.pop_add_save_onLoad.call(this, data);
    };
    mCallback.pop_edit_onLoad = function(data) {
      cm.tmp.cb_mod.pop_edit_onLoad.call(this, data);
    };
    mCallback.pop_edit_save_onLoad = function(data) {
      cm.tmp.cb_mod.pop_edit_save_onLoad.call(this, data);
    };
    mCallback.pop_remove_onLoad = function(data) {
      cm.tmp.cb_mod.pop_remove_onLoad.call(this, data);
    };
    mCallback.pop_remove_list_onLoad = function(data) {
      cm.tmp.cb_mod.pop_remove_onLoad.call(this, data);
    };
    mCallback.pop_catalog_edit_onLoad = function(data) {
      mCallback.catalog_edit_onLoad.call(this, data);
    };
    mCallback.pop_catalog_edit_save_onLoad = function(data) {
      mCallback.catalog_edit_save_onLoad.call(this, data);
    };
  } else {
    cmApi.alert_message(cmLang.ERROR_MODULE_NOT_FOUND);
  }
})(jQuery, window._cm, window._cm.func, window._cm.lang);