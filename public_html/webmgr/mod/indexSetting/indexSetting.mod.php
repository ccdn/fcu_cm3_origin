<?php
  class modIndexSetting extends Admin_Mod {
    const MODULE_NAME = 'indexSetting';
    const TABLE_SETTING = 'index_setting';
    
    static public function lang($key) {
      return CM_Lang::line(self::MODULE_NAME . '.' . $key, '', CM_Conf::get('PATH_F.ADMIN_MOD') . self::MODULE_NAME . '/');
    }
    
    public function __construct($modName, $requestName = '', $actionName = '') {
      parent::__construct($modName, $requestName, $actionName);
      //設定功能-預設功能
      $this->add_request('default', self::lang('R_INDEX_SETTING'), self::lang('R_INDEX_SETTING_INTRO'));
      $this->add_request_sub('default', 'save', self::lang('R_INDEX_SETTING_INTRO_SAVE'));
      //設定Breadcrumb
      $this->_breadcrumb->add(self::lang('R_INDEX_SETTING'), $this->get_request('default')->get_link(array(), sprintf('<i class="icofont-home"></i> %s', self::lang('R_INDEX_SETTING'))));
    }
    
    protected function load_languages() {
      return array(
        'ERROR_DUPLICATE_ITEM' => self::lang('ERROR_DUPLICATE_ITEM'),
        'REMOVE_CONFIRM' => self::lang('REMOVE_CONFIRM')
      );
    }

    public function response_load_module() {
      parent::response_load_module();
    }

    protected function get_drag_list(array $id) {
      $return = array();

      if(in_array('product', $id)) {
        $return[] = array(
          'id' => sprintf('select_product_list_%d', substr(microtime(), 2, 5)),
          'class' => '',
          'title' => self::lang('DRAG_TITLE_PRODUCT'),
          'pane_class' => 'select-product-list',
          'url' => 'index.php#/product/pop_list'
        );
      }

      return $return;
    }

    protected function get_drop_list($dropSetting) {
      $dao = Admin_Factory::createCMDAO('indexSetting');
      $dropList = array();

      if($dao->get_array($recordArray, Admin_DAO_IndexSetting::GROUP_LIST, array(
        'get_item' => true,
        'recordId' => array_keys($dropSetting)
      ))) {
        foreach($recordArray as $recordRows) {
          $tmpSetting = $dropSetting[$recordRows[Admin_DAO_IndexSetting::COLUMN_RECORD_ID]];
          $dropList[] = array_merge(array(
            'title' => $recordRows[Admin_DAO_IndexSetting::COLUMN_HEADLINE],
            'column' => sprintf("item_id[%s]", $recordRows[Admin_DAO_IndexSetting::COLUMN_RECORD_ID]),
            'list' => $recordRows['items']
          ), $tmpSetting);
        }
      }

      return $dropList;
    }

    protected function output_response($drop, $drag) {
      $form = $this->get_request('default', 'save')
        ->set_params(array('type' => $this->_get['type']))
        ->get_form('selectItemForm');
      $formHidden = array();

      foreach($drop as $dropRows) {
        foreach($dropRows['list'] as $dropItemRows) $formHidden[] = CM_Html::hidden_field($dropItemRows['column'], Admin::implode_key_array(',', $dropItemRows['list'], 'id'));
      }

      CM_Output::json('default_ok', true, Admin::format_templates_data(array(
        'header' => array(
          'icon' => 'icofont-th-list',
          'title' => self::lang('R_INDEX_SETTING'),
          'intro' => self::lang('R_INDEX_SETTING_INTRO'),
          'func' => array()
        ),
        'breadcrumb' => $this->_breadcrumb->trail(),
        'drop' => $drop,
        'drag' => $drag,
        'form' => array(
          'hidden' => $formHidden,
          'top' => $form['formTop'],
          'bottom' => $form['formBottom']
        )
      )));
    }

    public function response_type_product() {
      $this->_breadcrumb->add(self::lang('PRODUCT_INDEX_SETTING'), $this->get_request('default')->set_params(Admin::get_all_get_params(array(), true))->get_link());
      $drag = $this->get_drag_list(array('product'));
      $drop[] = array(
        'title' => self::lang('PRODUCT_INDEX_SETTING'),
        'list' => $this->get_drop_list(array(
          1 => array('class' => 'select-product', 'drop_accept_id' => "#{$drag[0]['id']} tr", 'drop_text' => self::lang('DROP_PRODUCT_TEXT')),
          2 => array('class' => 'select-product', 'drop_accept_id' => "#{$drag[0]['id']} tr", 'drop_text' => self::lang('DROP_PRODUCT_TEXT')),
          3 => array('class' => 'select-product', 'drop_accept_id' => "#{$drag[0]['id']} tr", 'drop_text' => self::lang('DROP_PRODUCT_TEXT'))
        ))
      );
      $this->output_response($drop, $drag);
    }
    
    public function response_default() {
      if(!isset($this->_get['type'])) Admin::response_error(CM_Lang::line('CM.ERROR_DATA_NOT_FOUND'));

      $drop = array();
      $callback = array($this, "response_type_{$this->_get['type']}");

      if(!is_callable($callback)) Admin::response_error(CM_Lang::line('CM.ERROR_DATA_NOT_FOUND'));

      call_user_func($callback);
    }

    public function response_default_save() {
      if(isset($this->_post['item_id']) && is_array($this->_post['item_id']) && !empty($this->_post['item_id'])) {
        $dao = Admin_Factory::createCMDAO('indexSetting');
        $error = false;
        $errorMsg = array();

        foreach($this->_post['item_id'] as $key => $value) {
          $updateRows = array('item_id' => $value);

          if(!$dao->update_rows($rMessage, $rReturn, array('item_id' => $value), $key, Admin_DAO_IndexSetting::GROUP_EDIT)) {
            $error = true;
            $errorMsg[] = $rMessage;
          } else {
            $log[] = implode('<br />', array_map(function($element) {
              return sprintf('<b>%s</b>: %s', $element['id'], $element['text']);
            }, array(
              array('id' => Admin_DAO_IndexSetting::COLUMN_RECORD_ID, 'text' => $key),
              array('id' => Admin_DAO_IndexSetting::COLUMN_HEADLINE, 'text' => $rReturn['currentRows'][Admin_DAO_IndexSetting::COLUMN_HEADLINE]),
              array('id' => 'item_id', 'text' => $value)
            )));
          }
        }

        if($error == true) {
          Admin::response_error(implode('<br />', $errorMessage));
        } else {
          $this->log_request(self::lang('R_INDEX_SETTING'), array('select' => implode('<br /><br />', $log)));
          Admin::response_success(self::lang('INDEX_SETTING_SUCCESS'), true);
        }
      } else {
        Admin::response_success(self::lang('INDEX_SETTING_SUCCESS'), true);
      }
    }
  }
?>