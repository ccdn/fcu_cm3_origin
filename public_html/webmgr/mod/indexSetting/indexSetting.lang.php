<?php
  CM_Lang::import('indexSetting', array(
    CM_Lang::CODE_TW => array(
      'R_INDEX_SETTING' => '首頁設定',
      'R_INDEX_SETTING_INTRO' => '設定首頁內容',
      'INDEX_SETTING_SUCCESS' => '設定值已儲存',
      'PRODUCT_INDEX_SETTING' => '首頁產品設定',
      'DRAG_TITLE_PRODUCT' => '產品',
      'DROP_PRODUCT_TEXT' => '請拖拉產品到此',
      'ERROR_DUPLICATE_ITEM' => '您的列表內已經有相同編號的內容',
      'REMOVE_CONFIRM' => '確定要移除這個項目?'
    ),
    CM_Lang::CODE_CN => array(
      'R_INDEX_SETTING' => '首页设定',
      'R_INDEX_SETTING_INTRO' => '设定首页内容',
      'INDEX_SETTING_SUCCESS' => '设定值已储存',
      'PRODUCT_INDEX_SETTING' => '首页产品设定',
      'DRAG_TITLE_PRODUCT' => '产品',
      'DROP_PRODUCT_TEXT' => '请拖拉产品到此',
      'ERROR_DUPLICATE_ITEM' => '您的列表内已经有相同编号的内容',
      'REMOVE_CONFIRM' => '确定要移除这个项目?'
    ),
    CM_Lang::CODE_EN => array(
    
    )
  ));
?>