;(function($, cm, cmApi, cmLang) {
  var mMain = $(cm.setting.CONTAINER),
      mModName = 'indexSetting';
  
  if(typeof cm.mod[mModName] == 'object') {
    var mLang = cm.mod[mModName][cm.setting.VAR_LANGUAGES],
        mCallback = cm.mod[mModName][cm.setting.VAR_CALLBACK] = {};
    //設定預設的callbcak
    mCallback.default_onLoad = function(data) {
      var jqMain = $('#columnCenter', $(cm.setting.CONTAINER));
      cmApi.templates_create(mModName + '_main', data.tplData, jqMain);

      var jqThis = $('div.drop-drag-main', jqMain),
          jqDrag = $('div.drag-box', jqThis),
          refreshSelect = function(jqSelected, jqThis) {
            var tmpId = []
            $('[data-id]', jqSelected).each(function() { tmpId.push($(this).data('id')); });
            $('input[name="' + jqSelected.data('column') + '"]', jqThis).val(cmApi.implode(',', tmpId));
          },
          jqSelected = $('ul.selected', jqThis);
      jqSelected.sortable({
        update: function(e, ui) {
          //排序更新寫回欄位
          refreshSelect(ui.item.parents('[data-column]'), jqThis);
        }
      });
      jqSelected.disableSelection();
      jqDrag
        .on('click', 'li > a[data-url][href]', function(e) {
          var jqDrapList = $($(this).attr('href'), jqDrag);

          if(jqDrapList.length > 0 && jqDrapList.html() == '') {
            cmApi.call_server_api({
              history: false,
              url: $(this).data('url'),
              caller: jqThis[0]
            });
          }
        }).find('li > a[data-url][href]').eq(0).trigger('click');
      $('div.drop-box', jqThis)
        .on('click', 'button.remove-item-entry', function(e) {
          var jqParent = $(this).parents('li'),
              jqSelected = $(this).parents('[data-column]');
          cmApi.confirm_message(cmLang.REMOVE_CONFIRM, function() {
            jqParent.fadeOut('fast', function() {
              $(this).remove();
              //移除後更新欄位
              refreshSelect(jqSelected, jqThis);
            });
          });
        });
      $('div[data-accept-id].drop-entry', jqThis).each(function() {
        $(this).droppable({
          accept: $(this).data('accept-id'),
          activeClass: "select-drop-highlight",
          drop: function(e, ui) {
            if($('[data-file-info]', ui.draggable).length > 0) {
              var tmplData = $('[data-file-info]', ui.draggable).data('file-info'),
                  jqSelected = $('ul', this);

              if($('[data-id="' + tmplData.id + '"]', jqSelected).length <= 0) {
                $(cmApi.templates_add(mModName + '_itemEntry', tmplData)).appendTo(jqSelected);
                refreshSelect(jqSelected, jqThis);
              } else {
                cmApi.alert_message(mLang.ERROR_DUPLICATE_ITEM);
              }
            }
          }
        });
      });
    };
    mCallback.default_save_onLoad = function(data) {
      cmApi.alert_message(data.message, 'success');
    };
  } else {
    cmApi.alert_message(cmLang.ERROR_MODULE_NOT_FOUND);
  }
})(jQuery, window._cm, window._cm.func, window._cm.lang);