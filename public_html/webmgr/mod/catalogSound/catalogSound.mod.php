<?php
  class modCatalogSound extends Admin_Mod_Tree {
    const MODULE_NAME = 'catalogSound';
    
    public $_mod_dao;

    public $_tree_mode = false;
    
    static public function lang($key) {
      return CM_Lang::line(self::MODULE_NAME . '.' . $key, '', CM_Conf::get('PATH_F.ADMIN_MOD') . self::MODULE_NAME . '/');
    }

    protected function tree_to_node_tmpl($nodeArray) {
      $cbThat = $this;
      $return = array();
      $return = parent::tree_to_node_tmpl($nodeArray);

      if($return['hasNodes'] == true && Admin::not_empty_array($return['nodes'])) {
        $return['nodes'] = array_map(function($element) use($cbThat) {
          $element['edit'] = $cbThat->check_permission($element[Admin_DAO_Catalog_Sound::COLUMN_RECORD_ID]);
          return $element;
        }, $return['nodes']);
      }

      return $return;
    }

    public function check_permission($catalogId) {
      return in_array('-1', $this->_session['allow_sound_catalog']) || in_array($catalogId, $this->_session['allow_sound_catalog']);
    }

    public function __construct($modName, $requestName = '', $actionName = '') {
      parent::__construct(Admin_Factory::createCMDAO('catalog_Sound'), $modName, $requestName, $actionName);
      //設定功能-預設功能
      $this->add_request('default', self::lang('R_MOD'), self::lang('R_INTRO'));
      //設定Breadcrumb
      $this->_breadcrumb->add(self::lang('R_MOD'), $this->get_request('default')->get_link(array(), sprintf('<i class="icofont-th-large"></i> %s', self::lang('R_MOD'))));
      $this->_mod_dao = Admin_Factory::createCMDAO('catalog_Sound');
    }

    public function response_tree_add() {
      if($this->check_permission($this->_post['parent_id']) == false) Admin::response_error(sprintf(self::lang('CATALOG_PERMISSION_DENIED'), $this->_post['parent_id']));

      $this->tree_response_add();
    }

    public function response_tree_add_save() {
      if($this->check_permission($this->_post['parent_id']) == false) Admin::response_error(sprintf(self::lang('CATALOG_PERMISSION_DENIED'), $this->_post['parent_id']));

      $this->tree_response_add_save();
    }

    public function response_tree_edit() {
      if($this->check_permission($this->_get[Admin::VAR_RECORD_ID]) == false) Admin::response_error(sprintf(self::lang('CATALOG_PERMISSION_DENIED'), $this->_get[Admin::VAR_RECORD_ID]));

      $this->tree_response_edit();
    }

    public function response_tree_edit_save() {
      if($this->check_permission($this->_get[Admin::VAR_RECORD_ID]) == false) Admin::response_error(sprintf(self::lang('CATALOG_PERMISSION_DENIED'), $this->_get[Admin::VAR_RECORD_ID]));

      $this->tree_response_edit_save();
    }

    public function response_tree_edit_list_status() {
      $editIdArray = array();
      $errorMessage = array();

      if(isset($this->_get[Admin::VAR_RECORD_ID])) {
        $editIdArray[] = (int)$this->_get[Admin::VAR_RECORD_ID];
      } elseif(isset($this->_post['listActionId']) && !empty($this->_post['listActionId'])) {
        $editIdArray = array_map(function($element) { return (int)$element; }, $this->_post['listActionId']);
      }

      foreach($editIdArray as $id) {
        if($this->check_permission($id) == false) $errorMessage[] = sprintf(self::lang('CATALOG_PERMISSION_DENIED'), $id);
      }

      if(!empty($errorMessage)) Admin::response_error(implode('<br />', $errorMessage));

      $this->tree_response_edit_list_status();
    }

    public function response_tree_remove() {
      if($this->check_permission($this->_get[Admin::VAR_RECORD_ID]) == false) Admin::response_error(sprintf(self::lang('CATALOG_PERMISSION_DENIED'), $this->_get[Admin::VAR_RECORD_ID]));

      $this->tree_response_remove();
    }

    public function response_tree_remove_list() {
      $editIdArray = array();
      $errorMessage = array();

      if(isset($this->_get[Admin::VAR_RECORD_ID])) {
        $editIdArray[] = (int)$this->_get[Admin::VAR_RECORD_ID];
      } elseif(isset($this->_post['listActionId']) && !empty($this->_post['listActionId'])) {
        $editIdArray = array_map(function($element) { return (int)$element; }, $this->_post['listActionId']);
      }

      foreach($editIdArray as $id) {
        if($this->check_permission($id) == false) $errorMessage[] = sprintf(self::lang('CATALOG_PERMISSION_DENIED'), $id);
      }

      if(!empty($errorMessage)) Admin::response_error(implode('<br />', $errorMessage));

      $this->tree_response_remove_list();
    }

    public function response_tree_list() {
      $this->tree_respone_list(array(
        'tmpl' => self::MODULE_NAME . '_tree',
        'nodeParams' => array(
          'rootRows' => array(
            Admin_DAO_Catalog_Sound::COLUMN_RECORD_ID => '0',
            Admin_DAO_Catalog_Sound::COLUMN_HEADLINE => '根目錄',
            Admin_DAO_Catalog_Sound::COLUMN_STATUS => true,
            'isRoot' => true,
            'edit' => $this->check_permission('0')
          )
        )
      ));
    }

    public function response_tree_list_catalog() {
      $this->tree_response_list_catalog();
    }

    public function response_tree_list_sort_save() {
      if($this->check_permission($this->_post['record_id']) == false) Admin::response_error(sprintf(self::lang('CATALOG_PERMISSION_DENIED'), $this->_post['record_id']));
      
      if($this->check_permission($this->_post['parent_id']) == false) Admin::response_error(sprintf(self::lang('CATALOG_PERMISSION_DENIED'), $this->_post['parent_id']));

      $this->tree_response_list_sort_save();
    }
    
    public function response_default() {
      $this->response_tree_list();
    }
  }
?>