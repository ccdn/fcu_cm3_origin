;(function($, cm, cmApi, cmLang) {
  var mMain = $(cm.setting.CONTAINER),
      mModName = 'adBanners';
  
  if(typeof cm.mod[mModName] == 'object') {
    var mLang = cm.mod[mModName][cm.setting.VAR_LANGUAGES],
        mCallback = cm.mod[mModName][cm.setting.VAR_CALLBACK] = {};
    //加入目錄callback
    for(var k in cm.tmp.cb_catalog) mCallback[k] = cm.tmp.cb_catalog[k];
    //加入模組預設的callback
    for(var k in cm.tmp.cb_mod) mCallback[k] = cm.tmp.cb_mod[k];
    //設定預設的callbcak
    mCallback.default_onLoad = function(data) {
      mCallback.list_onLoad(data);
    };
    mCallback.list_onLoad = function(data) {
      var jqMain = $('#columnCenter', $(cm.setting.CONTAINER));
      var sortEdit = function(args) {
        var conf = $.extend({
          'sort'     : '',
          'recordId' : '',
          'headline' : '',
          'backurl'  : ''
        }, args);
        
        cmApi.call_server_api({
          history : false,
          url : cmApi.request_to_url({
            module : mModName,
            request : 'sort_save'
          }),
          param : {sort : conf.sort, recordId : conf.recordId, headline : conf.headline, _backurl : conf.backurl, dao : conf.dao}
        });
      };
      $('.boxList', cmApi.templates_create('column_center_listSingle', data.tplData, jqMain)).cmAdminList()
        .on('focusout', 'input.sort', function(e) {
          if($(this).val() != $(this).data('sort')) {
            sortEdit({
              'sort'     : $(this).val(),
              'recordId' : $(this).data('id'),
              'headline' : $(this).data('headline'),
              'backurl'  : $(this).data('backurl')
            });
          }
        })
        .on('keyup', 'input.sort', function(e) {
          if(e.keyCode == 13) {
            sortEdit({
              'sort'     : $(this).val(),
              'recordId' : $(this).data('id'),
              'headline' : $(this).data('headline'),
              'backurl'  : $(this).data('backurl')
            });
          }
        });

      if(typeof data.tplData.topSearch == 'object') {
        var jqTopSearch = cmApi.templates_create('list_topSearch', data.tplData.topSearch, $('div.top-search', jqMain));
      }

      $('form', jqMain).cmForm();
    };
    mCallback.sort_save_onLoad = function(data) {
      cmApi.alert_message(data.message, 'success');
      cmApi.call_server_api({url : decodeURIComponent(data.url)});
    };
  } else {
    cmApi.alert_message(cmLang.ERROR_MODULE_NOT_FOUND);
  }
})(jQuery, window._cm, window._cm.func, window._cm.lang);