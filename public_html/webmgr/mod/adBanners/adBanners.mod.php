<?php
  class modAdBanners extends Admin_Mod {
    const MODULE_NAME = 'adBanners';
    
    public $_mod_dao;
    
    static public function lang($key) {
      return CM_Lang::line(self::MODULE_NAME . '.' . $key, '', CM_Conf::get('PATH_F.ADMIN_MOD') . self::MODULE_NAME . '/');
    }
    
    public function __construct($modName, $requestName = '', $actionName = '') {
      parent::__construct($modName, $requestName, $actionName);
      //設定功能-預設功能
      $this->add_request('default', self::lang('R_MOD'), self::lang('R_INTRO'));
      //設定功能-廣告列表
      $this->add_request('list', self::lang('R_LIST'), self::lang('R_LIST_INTRO'));
      //設定功能-新增廣告
      $this->add_request('add', self::lang('R_ADD'), self::lang('R_ADD_INTRO'));
      $this->add_request_sub('add', 'save', self::lang('R_ADD_SAVE_INTRO'));
      //設定功能-編輯廣告
      $this->add_request('edit', self::lang('R_EDIT'), self::lang('R_EDIT_INTRO'));
      $this->add_request_sub('edit', 'save', self::lang('R_EDIT_SAVE_INTRO'));
      $this->add_request_sub('edit', 'list_status', self::lang('R_EDIT_L_STATUS_INTRO'));
      //設定功能-移除廣告
      $this->add_request('remove', self::lang('R_REMOVE'), self::lang('R_REMOVE_INTRO'), array(
        'attrParams' => array(
          'isHistory' => false,
          'isConfirm' => true,
          'confirmMsg' => self::lang('REMOVE_CONFIRM')
        )
      ));
      $this->add_request_sub('remove', 'list', self::lang('R_REMOVE_L_INTRO'));
      //設定功能-編輯排序
      $this->add_request('sort_save', self::lang('R_SORT_SAVE'), self::lang('R_SORT_SAVE_INTRO'));
      //設定Breadcrumb
      $this->_breadcrumb->add(self::lang('R_MOD'), $this->get_request('default')->get_link(array(), sprintf('<i class="icofont-picture"></i> %s', self::lang('R_MOD'))));
      $this->_mod_dao = Admin_Factory::createCMDAO(self::MODULE_NAME);
    }
    
    public function response_load_module() {
      parent::response_load_module();
    }
    
    public function response_add() {
      $this->_breadcrumb->add(self::lang('R_LIST'), $this->get_request('list')->get_link());
      $this->_breadcrumb->add(self::lang('R_ADD'), $this->get_request('add')->get_link());
      $thisForm = Admin_Factory::createCMForm();
      $thisForm->set_form_dao($this->_mod_dao, Admin_DAO_AdBanners::GROUP_ADD);
      $thisForm->set_form_default(array(
        'date' => date('Y-m-d'),
        'banner_section' => $this->_get['banner_section']
      ));
      $form = $this->get_request('add', 'save')->get_form('addForm');
      CM_Output::json('add_form_ok', true, Admin::format_templates_data(array_merge($form, array(
        'formButton' => 
          CM_Html::submit_btn(CM_Lang::line('Admin.BTN_ADD'), array('class' => 'btn btn-primary')) . '&nbsp;' . 
          CM_Html::reset_btn(CM_Lang::line('Admin.BTN_CANCEL'), array('class' => 'btn prevBtn')) . '&nbsp;' .
          CM_Html::reset_btn(CM_Lang::line('Admin.BTN_RESET'), array('class' => 'btn')),
        'hiddenColumn' => array(
          CM_Html::hidden_field(Admin::VAR_BACK_URL, $this->_get[Admin::VAR_BACK_URL])
        ),
        'header' => array(
          'icon' => 'icofont-edit',
          'title' => self::lang('R_ADD'),
          'intro' => self::lang('ADD_INTRO'),
          'breadcrumb' => $this->_breadcrumb->trail()
        ),
        'formGroup' => $thisForm->get_groups_form(array(
          'info' => array('title' => self::lang('FORM_GROUP_INFO')),
          'setting' => array('title' => self::lang('FORM_GROUP_SETTING'))
        ), CM_Form::ACTION_INSERT)
      ))));
    }
    
    public function response_add_save() {
      if(!CM::not_null($this->_post['sort']) && $this->_mod_dao->get_rows($dataRows, Admin_DAO_AdBanners::GROUP_ADD, array('sort' => 'sort DESC', 'banner_section' => $this->_post['banner_section']))) {
        $this->_post['sort'] = $dataRows['sort'] + 2;
      } elseif(!CM::not_null($this->_post['sort'])) {
        $this->_post['sort'] = 1;
      }
      
      if(!$this->_mod_dao->insert_rows($rMessage, $rReturn, $this->_post, Admin_DAO_AdBanners::GROUP_ADD)) {
        Admin::response_error($rMessage);
      } else {
        $this->log_request($this->_post[Admin_DAO_AdBanners::COLUMN_HEADLINE], $this->_mod_dao->to_column_group_rows(Admin_DAO_AdBanners::GROUP_ADD, $this->_post));
        CM_Output::json($rMessage, true, array('url' => isset($this->_post[Admin::VAR_BACK_URL]) ? Admin::filter_input($this->_post[Admin::VAR_BACK_URL], false) : $this->get_request('list')->get_href()));
      }
    }

    public function response_edit() {
      $langId = isset($this->_get[Admin::VAR_LANG]) ? $this->_get[Admin::VAR_LANG] : CM_Lang::get_current_id(true);
      
      if(!isset($this->_get[Admin::VAR_RECORD_ID]) || !$this->_mod_dao->get_rows_byId($recordRows, Admin_DAO_AdBanners::GROUP_EDIT, $this->_get[Admin::VAR_RECORD_ID], array('lang' => $langId))) Admin::response_error(CM_Lang::line('Admin.ERROR_NOT_FOUND'));
      
      $this->_breadcrumb->add(self::lang('R_LIST'), $this->get_request('list')->get_link());
      $this->_breadcrumb->add(self::lang('R_EDIT'), $this->get_request('edit')->get_link());
      $thisForm = Admin_Factory::createCMForm();
      $thisForm->set_form_dao($this->_mod_dao, Admin_DAO_AdBanners::GROUP_EDIT);
      $thisForm->set_form_default($recordRows);
      $form = $this->get_request('edit', 'save')->set_params(array(
        Admin::VAR_RECORD_ID => $recordRows[Admin_DAO_AdBanners::COLUMN_RECORD_ID]
      ))->get_form('editForm');
      $formLang = Admin_Mod::to_tmpl_lang_catalog($this->get_request('edit'), array('current' => $langId, Admin::VAR_BACK_URL => urldecode($_GET[Admin::VAR_BACK_URL])));
      CM_Output::json('update_form_ok', true, Admin::format_templates_data(array_merge($form, array(
        'hiddenColumn' => array(
          CM_Html::hidden_field(Admin::VAR_BACK_URL, $this->_get[Admin::VAR_BACK_URL]),
          CM_Html::hidden_field(Admin::VAR_LANG, $langId),
          CM_Html::hidden_field(Admin_DAO_AdBanners::COLUMN_RECORD_ID, $recordRows[Admin_DAO_AdBanners::COLUMN_RECORD_ID])
        ),
        'formButton' => 
          CM_Html::submit_btn(CM_Lang::line('Admin.BTN_EDIT'), array('class' => 'btn btn-primary')) . '&nbsp;' . 
          CM_Html::reset_btn(CM_Lang::line('Admin.BTN_CANCEL'), array('class' => 'btn prevBtn')) . '&nbsp;' .
          CM_Html::reset_btn(CM_Lang::line('Admin.BTN_RESET'), array('class' => 'btn')),
        'header' => array(
          'icon' => 'icofont-edit',
          'title' => self::lang('R_EDIT'),
          'intro' => self::lang('EDIT_INTRO'),
          'breadcrumb' => $this->_breadcrumb->trail()
        ),
        'hasLang' => count($formLang) > 1 ? true : false,
        'formLang' => $formLang,
        'formGroup' => $thisForm->get_groups_form(array(
          'info' => array('title' => self::lang('FORM_GROUP_INFO')),
          'setting' => array('title' => self::lang('FORM_GROUP_SETTING'))
        ), CM_Form::ACTION_UPDATE)
      ))));
    }
    
    public function response_edit_save() {
      if(!isset($this->_get[Admin::VAR_RECORD_ID]) || !$this->_mod_dao->update_rows($rMessage, $rReturn, $this->_post, $this->_get[Admin::VAR_RECORD_ID], Admin_DAO_AdBanners::GROUP_EDIT)) {
        Admin::response_error($rMessage);
      } else {
        $this->log_request($this->_post[Admin_DAO_AdBanners::COLUMN_HEADLINE], $this->_mod_dao->to_column_group_rows(Admin_DAO_AdBanners::GROUP_EDIT, $this->_post));
        CM_Output::json($rMessage, true, array('url' => isset($this->_post[Admin::VAR_BACK_URL]) ? Admin::filter_input($this->_post[Admin::VAR_BACK_URL], false) : $this->get_request('list')->get_href()));
      }
    }
    
    public function response_edit_list_status() {
      $error = false;
      $errorMessage = array();
      $editIdArray = array();

      if(isset($this->_get[Admin::VAR_RECORD_ID])) {
        $editIdArray[] = (int)$this->_get[Admin::VAR_RECORD_ID];
      } elseif(isset($this->_post['listActionId']) && !empty($this->_post['listActionId'])) {
        $editIdArray = array_map(function($element) { return (int)$element; }, $this->_post['listActionId']);
      } else {
        $error = true;
      }

      if($error == true) Admin::response_error(CM_Lang::line('Admin.LIST_ACTION_ID_EMPTY'));

      $status = isset($this->_get[Admin_DAO_AdBanners::COLUMN_STATUS]) ? (int)$this->_get[Admin_DAO_AdBanners::COLUMN_STATUS] : '2';
      
      foreach($editIdArray as $index => $id) {
        if(!$this->_mod_dao->update_rows($rMessage, $rReturn, array(Admin_DAO_AdBanners::COLUMN_STATUS => $status), $id, Admin_DAO_AdBanners::GROUP_STATUS)) {
          $error = true;
          $errorMessage[] = $rMessage;
        } else {
          $log[] = implode('<br />', array_map(function($element) {
            return sprintf('<b>%s</b>: %s', $element['id'], $element['text']);
          }, array(
            array('id' => Admin_DAO_AdBanners::COLUMN_RECORD_ID, 'text' => $id),
            array('id' => Admin_DAO_AdBanners::COLUMN_STATUS, 'text' => $status),
            array('id' => Admin_DAO_AdBanners::COLUMN_HEADLINE, 'text' => $rReturn['currentRows'][Admin_DAO_AdBanners::COLUMN_HEADLINE]),
          )));
        }
      }

      if($error == true) {
        Admin::response_error(implode('<br />', $errorMessage));
      } else {
        $this->log_request(self::lang('R_EDIT_L_STATUS_INTRO'), array('edit' => implode('<br /><br />', $log)));
        Admin::response_success(self::lang('EDIT_L_SUCCESS'), true);
      }
    }
    
    public function response_remove() {
      if(!isset($this->_get[Admin::VAR_RECORD_ID]) || !$this->_mod_dao->delete_rows($rMessage, $rReturn, $this->_get[Admin::VAR_RECORD_ID])) {
        Admin::response_error($rMessage);
      } else {
        $this->log_request($rReturn['currentRows'][Admin_DAO_AdBanners::COLUMN_HEADLINE], $rReturn['currentRows']);
        CM_Output::json($rMessage, true, array('url' => isset($_GET[Admin::VAR_BACK_URL]) ? Admin::filter_input($_GET[Admin::VAR_BACK_URL], false) : $this->get_request('list')->get_href()));
      }
    }
    
    public function response_remove_list() {
      if(!isset($this->_post['listActionId']) || empty($this->_post['listActionId'])) Admin::response_error(CM_Lang::line('Admin.LIST_ACTION_ID_EMPTY'));

      $errorMessage = array();
      $error = false;
      $log = array();
      
      foreach($this->_post['listActionId'] as $index => $id) {
        if(!$this->_mod_dao->delete_rows($rMessage, $rReturn, $id)) {
          $error = true;
          $errorMessage[] = $rMessage;
        } else {
          $log[] = implode('<br />', array_map(function($element) {
            return sprintf('<b>%s</b>: %s', $element['id'], $element['text']);
          }, array(
            array('id' => Admin_DAO_AdBanners::COLUMN_RECORD_ID, 'text' => $id),
            array('id' => Admin_DAO_AdBanners::COLUMN_HEADLINE, 'text' => $rReturn['currentRows'][Admin_DAO_AdBanners::COLUMN_HEADLINE]),
          )));
        }
      }
      
      if($error == true) {
        Admin::response_error(implode('<br />', $errorMessage));
      } else {
        $this->log_request(self::lang('R_REMOVE_L_INTRO'), array('remove' => implode('<br /><br />', $log)));
        Admin::response_success(self::lang('REMOVE_L_SUCCESS'), true);
      }
    }
    
    public function response_list() {
      $cbThat = $this;
      $this->_breadcrumb->add(self::lang('R_LIST'), $this->get_request('list')->get_link());
      
      if(isset($this->_get['keyword']) && !empty($this->_get['keyword'])) Admin_Mod::set_breadcrumb_keyword($this->_breadcrumb, $this->get_request('list'), $this->_get['keyword']);
      
      $currentSection = isset($this->_get['banner_section']) ? $this->_get['banner_section'] : '';
      $daoParams = array(
        'banner_section' => $currentSection,
        'sort' => Admin_Mod::to_sort($this->_mod_dao, $this->_get['sc'], $this->_get['sf']),
        'keyword' => $this->_get['keyword']
      );
      
      $listColumn = array_merge($this->_mod_dao->get_column_field_label_array(Admin_DAO_AdBanners::GROUP_LIST), array(
        'action' => CM_Lang::line('Admin.LIST_ACTION')
      ));
      $listJSON = $this->_mod_dao->get_array_object(Admin_DAO_AdBanners::GROUP_LIST, array_merge($daoParams, array(
        'keyColumn' => Admin_DAO_AdBanners::COLUMN_RECORD_ID,
        'oNowPage' => isset($this->_get['p']) ? (int)$this->_get['p'] : 1,
        'oPageCallback' => function($page, array $params, $key) use($cbThat) {
          return Admin_Mod::cb_default_pagesplit($page, $params, $key, $cbThat->get_request('list'), array('vpage' => 'p'));
        },
        'fileCallback' => true
      )));
      CM_Output::json('list_ok', true, Admin::format_templates_data(array(
        'header' => array(
          'icon' => 'icofont-th-list',
          'title' => self::lang('R_LIST'),
          'intro' => self::lang('R_LIST_INTRO')
        ),
        'breadcrumb' => $this->_breadcrumb->trail(),
        'list' => Admin_Mod::to_tmpl_table_list($listJSON, $listColumn, array(
          'addon' => Admin_Mod::to_tmpl_catalog($this->get_request('list'), $this->_mod_dao, array(
            'daoParams' => $daoParams,
            'linkParams' => Admin::get_all_get_params(array('banner_section'), true),
            'catalog' => $currentSection,
            'catalogColumn' => 'banner_section',
            'options' => $this->_mod_dao->get_options('banner_section')
          )),
          'id' => "{$this->_current_request['mod']}List",
          'title' => CM_Lang::line('Admin.LIST_ACTION_MENU'),
          'action' => array(
            'checkboxFunc' => array(
              'title' => CM_Lang::line('Admin.LIST_ACTION_MENU'),
              'list' => array(
                array(
                  'link' => $this->get_request('edit', 'list_status')->set_params(array(Admin_DAO_AdBanners::COLUMN_STATUS => '2'))->get_href(),
                  'text' => CM_Lang::line('Admin.LIST_ACTION_ID_DISABLE'),
                  'confirm' => CM_lang::line('Admin.LIST_ACTION_ID_DISABLE_CONFIRM')
                ),
                array(
                  'link' => $this->get_request('edit', 'list_status')->set_params(array(Admin_DAO_AdBanners::COLUMN_STATUS => '1'))->get_href(),
                  'text' => CM_Lang::line('Admin.LIST_ACTION_ID_ENABLE'),
                  'confirm' => CM_lang::line('Admin.LIST_ACTION_ID_ENABLE_CONFIRM')
                ),
                array('separate' => true),
                array(
                  'link' => $this->get_request('remove', 'list')->get_href(),
                  'text' => CM_Lang::line('Admin.LIST_ACTION_ID_REMOVE'),
                  'confirm' => CM_lang::line('Admin.LIST_ACTION_ID_REMOVE_CONFIRM')
                )
              )
            ),
            'listFunc' => array(
              $this->get_request('add')
                ->set_params(array(
                  Admin::VAR_BACK_URL => urlencode($this->get_request('list')->get_href(Admin::get_all_get_params(array(), true))),
                  'banner_section' => $currentSection
                ))
                ->set_text_format('<i class="icofont-plus"></i>&nbsp;%s')
                ->get_btn(array('class' => 'btn btn-primary'))
            ),
            'search' => array_merge(array(
              'hiddenColumn' => array(),
              'formSearch' => CM_Html::input_field('keyword', $this->_get['keyword']),
              'formButton' => CM_Html::submit_btn('<i class="icofont-search"></i>&nbsp;' . CM_Lang::line('Admin.BTN_SEARCH'), array('class' => 'btn'))
            ), $this->get_request('list')->get_form('searchForm', array('method' => 'get', 'class' => 'form-inline')))
          ),
          'isSearch' => Admin::not_null($this->_get['keyword']),
          'callback' => array(
            'file' => function($record, $recordRows = array()) {
              return Admin_Format::to_upload_file_thumbnail('file', $recordRows);
            },
            'banner_section' => function($record, $recordRows = array()) use($cbThat) {
              return Admin::get_options_name($record, $cbThat->_mod_dao->get_options('banner_section'));
            },
            'status' => function($record, $recordRows = array()) use($cbThat) {
              $options = $cbThat->_mod_dao->get_options('status');
              $statusLabel = array(
                1 => 'label-success',
                2 => 'label-important'
              );

              if(Admin::in_options($record, $options, $currentOptions)) {
                return $cbThat->get_request('edit', 'list_status')
                  ->set_params(array(
                    Admin_DAO_AdBanners::COLUMN_STATUS => $currentOptions['id'] == '1' ? '2' : '1',
                    Admin::VAR_RECORD_ID => $recordRows[Admin_DAO_AdBanners::COLUMN_RECORD_ID]
                  ))
                  ->set_text(sprintf('<span class="label %s">%s</span>', $statusLabel[$currentOptions['id']], $currentOptions['text']))
                  ->get_link(array('isHistory' => false));
              } else {
                return '--';
              }
            },
            'sort' => function($record, $recordRows = array()) use($cbThat, $isPop) {
              if($isPop == true) {
                return $record;
              } else {
                $backurl = urlencode($cbThat->get_request('list')->get_href(Admin::get_all_get_params(array(Admin::VAR_RECORD_ID, Admin::VAR_ACTION), true)));
                return CM_Html::input_field('sort_' . $recordRows['id'], $record, 'class="sort" style="width: 30px;" data-sort="' . $record . '" data-id="' . $recordRows[Admin_DAO_AdBanners::COLUMN_RECORD_ID] . '" data-headline="' . $recordRows[Admin_DAO_AdBanners::COLUMN_HEADLINE] . '" data-backurl="' . $backurl . '"', 'text');
              }
            },
            'action' => function($record, $recordRows = array()) use($cbThat) {
              $action = array();
              //修改或刪除回來的網址
              $backurl = urlencode($cbThat->get_request('list')->get_href(Admin::get_all_get_params(array(Admin::VAR_RECORD_ID, Admin::VAR_ACTION), true)));
              //修改
              $thisRequestEdit = $cbThat->get_request('edit')->set_params(array(
                Admin::VAR_RECORD_ID => $recordRows[Admin_DAO_AdBanners::COLUMN_RECORD_ID],
                Admin::VAR_BACK_URL => $backurl
              ));
              //刪除
              $thisRequestRemove = $cbThat->get_request('remove')->set_params(array(
                Admin::VAR_RECORD_ID => $recordRows[Admin_DAO_AdBanners::COLUMN_RECORD_ID],
                Admin::VAR_BACK_URL => $backurl
              ));
              
              if($thisRequestEdit->check_permission()) {
                $action[] = $thisRequestEdit->get_link(array('class' => 'link'), sprintf('<i class="icofont-edit"></i>&nbsp;%s', CM_Lang::line('Admin.LIST_ACTION_EDIT')));
              }
              
              if($thisRequestRemove->check_permission()) {
                $action[] = $thisRequestRemove->get_link(array(
                  'class' => 'link',
                  'confirmMsg' => sprintf($cbThat->lang('REMOVE_CONFIRM'), $recordRows[Admin_DAO_AdBanners::COLUMN_HEADLINE])
                ), sprintf('<i class="icofont-trash"></i>&nbsp;%s', CM_Lang::line('Admin.LIST_ACTION_REMOVE')));
              }
              
              return implode('&nbsp;|&nbsp;', $action);
            }
          ),
          'h_callback' => function($rows, $name) use($cbThat) {
            return Admin_Mod::cb_default_sort($rows, $name, $cbThat->get_request('list'), array(
              'link_params' => $cbThat->_get,
              'sort_column' => array(Admin_DAO_AdBanners::COLUMN_RECORD_ID, Admin_DAO_AdBanners::COLUMN_HEADLINE, 'file', 'sort', 'date_start', 'date_end', 'create_date', Admin_DAO_AdBanners::COLUMN_STATUS, 'banner_section')
            ));
          }
        )))
      ));
    }
    
    public function response_default() {
      $this->response_list();
    }
    
    public function response_sort_save() {
      if(!isset($this->_post[Admin::VAR_RECORD_ID]) || !$this->_mod_dao->update_rows($rMessage, $rReturn, $this->_post, $this->_post[Admin::VAR_RECORD_ID], Admin_DAO_AdBanners::GROUP_SORT_EDIT)) {
        Admin::response_error($rMessage);
      } else {
        $this->log_request($this->_post['headline'], $this->_mod_dao->to_column_group_rows(Admin_DAO_AdBanners::GROUP_SORT_LOG, $this->_post));
        CM_Output::json($rMessage, true, array('url' => isset($this->_post[Admin::VAR_BACK_URL]) ? Admin::filter_input($this->_post[Admin::VAR_BACK_URL], false) : $this->get_request('list')->get_href()));
      }
    }
  }
?>