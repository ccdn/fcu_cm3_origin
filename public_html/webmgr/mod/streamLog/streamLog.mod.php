<?php
  class modStreamLog extends Admin_Mod {
    const MODULE_NAME = 'streamLog';
    
    public $_mod_dao;
    
    static public function lang($key) {
      return CM_Lang::line(self::MODULE_NAME . '.' . $key, '', CM_Conf::get('PATH_F.ADMIN_MOD') . self::MODULE_NAME . '/');
    }
	
	public function sec_to_time($time) {
      if(is_numeric($time)){
        $value = array(
          "year" => 0, "day" => 0, "hour" => 0,
          "min" => 0, "sec" => 0,
        );

        if($time >= 31556926){
          $value["year"] = floor($time / 31556926);
          $time = ($time % 31556926);
        }

        if($time >= 86400){
          $value["day"] = floor($time / 86400);
          $time = ($time % 86400);
        }

        if($time >= 3600){
          $value["hour"] = floor($time / 3600);
          $time = ($time % 3600);
        }

        if($time >= 60){
          $value["min"] = floor($time / 60);
          $time = ($time % 60);
        }

        $value["sec"] = floor($time);
        return implode('&nbsp;', array_filter(array_map(function($element, $key) {
          if($element <= 0) return false;

          return "{$element}&nbsp;{$key}";
        }, $value, array_keys($value))));
      }else{
        return (bool)false;
      }
    }

    protected function get_mode_options() {
      return array(
        array(
          'id' => 'month',
          'text' => $this->lang('COUNT_CHANGE_MODE_1')
        ),
        array(
          'id' => 'day',
          'text' => $this->lang('COUNT_CHANGE_MODE_2')
        ),
        array(
          'id' => 'hour',
          'text' => $this->lang('COUNT_CHANGE_MODE_3')
        )
      );
    }

    protected function get_device_options() {
      return array(
        array(
          'id' => 'PC',
          'text' => 'PC'
        ),
        array(
          'id' => 'Android',
          'text' => 'Android'
        ),
        array(
          'id' => 'iOS',
          'text' => 'iOS'
        )
      );
    }
    
    public function __construct($modName, $requestName = '', $actionName = '') {
      parent::__construct($modName, $requestName, $actionName);
      
      $this->add_request('default', self::lang('R_MOD'), self::lang('R_INTRO'));
      $this->add_request('count', self::lang('R_COUNT'), self::lang('R_COUNT_INTRO'));
      $this->add_request_sub('count', 'export', self::lang('R_COUNT_EXPORT'), array(
        'attrParams' => array(
          'ajax' => false,
          'data-target' => 'blank'
        )
      ));
      $this->add_request('rank', self::lang('R_RANK'), self::lang('R_RANK_INTRO'));
      $this->add_request_sub('rank', 'export', self::lang('R_RANK_EXPORT'), array(
        'attrParams' => array(
          'ajax' => false,
          'data-target' => 'blank'
        )
      ));
      $this->add_request('connection', self::lang('R_CONNECTION'), self::lang('R_CONNECTION_INTRO'));
      $this->add_request_sub('connection', 'export', self::lang('R_CONNECTION_EXPORT'), array(
        'attrParams' => array(
          'ajax' => false,
          'data-target' => 'blank'
        )
      ));
      $this->add_request('connectionDetail', self::lang('R_CONNECTION_DETAIL'), self::lang('R_CONNECTION_DETAIL_INTRO'));
      $this->add_request_sub('connectionDetail', 'export', self::lang('R_CONNECTION_DETAIL_EXPORT'), array(
        'attrParams' => array(
          'ajax' => false,
          'data-target' => 'blank'
        )
      ));
      $this->add_request('country', self::lang('R_COUNTRY'), self::lang('R_COUNTRY_INTRO'));
      $this->add_request_sub('country', 'export', self::lang('R_COUNTRY_EXPORT'), array(
        'attrParams' => array(
          'ajax' => false,
          'data-target' => 'blank'
        )
      ));
      $this->add_request('device', self::lang('R_DEVICE'), self::lang('R_DEVICE_INTRO'));
      $this->add_request_sub('device', 'export', self::lang('R_DEVICE_EXPORT'), array(
        'attrParams' => array(
          'ajax' => false,
          'data-target' => 'blank'
        )
      ));
      //設定Breadcrumb
      $this->_breadcrumb->add(self::lang('R_MOD'), $this->get_request('default')->get_link(array(), sprintf('<i class="icofont-user"></i> %s', self::lang('R_MOD'))));
      $this->_mod_dao = Admin_Factory::createCMDAO('stream_Log');
    }

    public function response_load_module() {
      parent::response_load_module();
    }

    public function get_func_options($currentFunc) {
      $returnOptions = array();

      foreach(array('count', 'rank', 'connection', 'country', 'device') as $funcName) {
        $request = $this->get_request($funcName);

        if($request->check_permission()) {
          $returnOptions[] = array(
            'current' => $funcName == $currentFunc ? true : false,
            'link' => $request->get_link()
          );
        }
      }

      return $returnOptions;
    }

    public function response_count() {
      $this->_breadcrumb->add(self::lang('R_COUNT'), $this->get_request('count')->get_link());
      $cbThat = $this;
      $data = array();
      $modeOptions = $this->get_mode_options();

      if(!isset($this->_get['mode']) || !Admin::in_options($this->_get['mode'], $modeOptions, $currentModeOptions)) $currentModeOptions = array_shift($modeOptions);

      $data['changeMode'] = array_merge(array(
        'form' => implode('&nbsp;', array(
          sprintf('<label for="change-time-mode" style="cursor: default;">%s：</label>', $this->lang('COUNT_CHANGE_MODE')),
          CM_Html::select_field('mode', $this->get_mode_options(), $this->_get['mode'], 'class="input-small"')
        ))
      ), $this->get_request('count')->get_form('form', array(
        'method' => 'get',
        'class' => 'form-inline'
      )));
      $data['searchForm'] = array_merge(array(
        'form' => implode('&nbsp;', array(
          CM_Html::hidden_field('mode', $currentModeOptions['id']),
          sprintf('<label style="cursor: default;">%s：</label>', $this->lang('COUNT_SEARCH_DATE')),
          CM_Html::input_field('startDate', $this->_get['startDate'], 'class="hasDatepicker input-small"'),
          '~',
          CM_Html::input_field('endDate', $this->_get['endDate'], 'class="hasDatepicker input-small"'),
          CM_Html::submit_btn(sprintf('<i class="icofont-search"></i>&nbsp;%s', $this->lang('COUNT_SEARCH_BTN')), array('class' => 'btn')),
          $this->get_request('count', 'export')
            ->set_params($this->_get)
            ->set_text(sprintf('<i class="icofont-share"></i>&nbsp;%s', $this->lang('R_COUNT_EXPORT')))
            ->get_btn(array('class' => 'btn'))
        ))
      ), $this->get_request('count')->get_form('form', array(
        'method' => 'get',
        'class' => 'form-inline'
      )));
	  
	  $max = 0;
      $min = 0;

      if($currentModeOptions['id'] == 'month') {
        //每月
        $data['countChart'] = array(
          'id' => 'count_chart_' . time(),
          'headline' => $this->lang('R_COUNT'),
          'xLabel' => $this->lang('COUNT_CHART_MONTH_X'),
          'yLabel' => $this->lang('COUNT_CHART_Y'),
          'xData' => array(),
          'yData' => array()
        );
        $startDate = isset($this->_get['startDate']) && !empty($this->_get['startDate']) ? date('Y-m-d', strtotime($this->_get['startDate'])) : date('Y-m-d', strtotime('now -1 year'));
        $endDate = isset($this->_get['endDate']) && !empty($this->_get['endDate']) ? date('Y-m-d', strtotime($this->_get['endDate'])) : date('Y-m-d');
        //$listArray = array_map(function($date) use($cbThat, &$data) {
		$listArray = array_map(function($date) use($cbThat, &$data, &$max, &$min) {
          $start = date('Y-m-01 00:00:00', strtotime($date));
          $end = date('Y-m-d 23:59:59', strtotime('now +1 month -1 day', strtotime($start)));
          $daoParams = array(
            'isCount' => true,
            'timestamp' => array(
              'start' => $start,
              'end' => $end
            )
          );
          $total = $cbThat->_mod_dao->get_rows($recordRows, '', $daoParams) ? $recordRows['total'] : 0;
          $label = date('Y年n月', strtotime($date));
          $data['countChart']['xData'][] = $label;
          $data['countChart']['yData'][] = $total;
		  
		  if($total > $max) $max = $total;

          if($total < $min) $min = $total;
		  
          return array('label' => $label, 'count' => $total);
        }, array_merge(
          array($startDate),
          Admin_Dgfactor::get_interval_time($startDate, $endDate, 'month')
        ));
        $listColumn = array(
          'label' => $this->lang('COUNT_CHART_MONTH_X'),
          'count' => $this->lang('COUNT_CHART_Y')
        );
        $listCallback = array();
      } elseif($currentModeOptions['id'] == 'day') {
        //每日
        $data['countChart'] = array(
          'id' => 'count_chart_' . time(),
          'headline' => $this->lang('R_COUNT'),
          'xLabel' => $this->lang('COUNT_CHART_DAY_X'),
          'yLabel' => $this->lang('COUNT_CHART_Y'),
          'xData' => array(),
          'yData' => array()
        );
        $startDate = isset($this->_get['startDate']) && !empty($this->_get['startDate']) ? date('Y-m-d', strtotime($this->_get['startDate'])) : date('Y-m-d', strtotime('now -1 month'));
        $endDate = isset($this->_get['endDate']) && !empty($this->_get['endDate']) ? date('Y-m-d', strtotime($this->_get['endDate'])) : date('Y-m-d');
        //$listArray = array_map(function($date) use($cbThat, &$data) {
		$listArray = array_map(function($date) use($cbThat, &$data, &$max, &$min) {
          $start = date('Y-m-d 00:00:00', strtotime($date));
          $end = date('Y-m-d 23:59:59', strtotime($date));
          $daoParams = array(
            'isCount' => true,
            'timestamp' => array(
              'start' => $start,
              'end' => $end
            )
          );
          $total = $cbThat->_mod_dao->get_rows($recordRows, '', $daoParams) ? $recordRows['total'] : 0;
          $label = date('Y年n月j日', strtotime($date));
          $data['countChart']['xData'][] = date("n\n月\nj\n日", strtotime($date));
          $data['countChart']['yData'][] = $total;
		  
		  if($total > $max) $max = $total;

          if($total < $min) $min = $total;
		  
          return array('label' => $label, 'count' => $total);
        }, array_merge(
          array($startDate),
          Admin_Dgfactor::get_interval_time($startDate, $endDate, 'day')
        ));
        $listColumn = array(
          'label' => $this->lang('COUNT_CHART_DAY_X'),
          'count' => $this->lang('COUNT_CHART_Y')
        );
        $listCallback = array();
      } else {
        //每小時
        $data['countChart'] = array(
          'id' => 'count_chart_' . time(),
          'headline' => $this->lang('R_COUNT'),
          'xLabel' => $this->lang('COUNT_CHART_DAY_X'),
          'yLabel' => $this->lang('COUNT_CHART_Y'),
          'xData' => array(),
          'yData' => array()
        );
        $startDate = isset($this->_get['startDate']) && !empty($this->_get['startDate']) ? date('Y-m-d', strtotime($this->_get['startDate'])) : '1970-01-01';
        $endDate = isset($this->_get['endDate']) && !empty($this->_get['endDate']) ? date('Y-m-d', strtotime($this->_get['endDate'])) : date('Y-m-d');
        $hourOptions = array();

        for($i = 0; $i <= 23; $i ++) $hourOptions[] = $i;

        //$listArray = array_map(function($hour) use($cbThat, &$data, $startDate, $endDate) {
		$listArray = array_map(function($hour) use($cbThat, &$data, &$max, &$min, $startDate, $endDate) {
          $daoParams = array(
            'isCount' => true,
            'hour' => $hour,
            'timestamp' => array(
              'start' => $startDate,
              'end' => $endDate
            )
          );
          //$total = $cbThat->_mod_dao->get_rows($recordRows, '', $daoParams) ? $recordRows['total'] : 0;
		  $total = $cbThat->_mod_dao->get_rows($recordRows, '', $daoParams) ? (int)$recordRows['total'] : 0;
          $label = "{$hour}時";
          $data['countChart']['xData'][] = "{$hour}時";
          $data['countChart']['yData'][] = $total;
		  
		  if($total > $max) $max = $total;

          if($total < $min) $min = $total;
		  
          return array('label' => $label, 'count' => $total);
        }, $hourOptions);
        $listColumn = array(
          'label' => $this->lang('COUNT_CHART_HOUR_X'),
          'count' => $this->lang('COUNT_CHART_Y')
        );
        $listCallback = array();
      }

      $data['list'] = Admin_Format::to_tmpl_table($listArray, $listColumn, $listCallback, array(
        'attr' => 'class="table table-bordered table-striped responsive"',
        'hasResponsive' => false
      ));
	  
	  $data['countChart']['yMax'] = $max;
      $data['countChart']['yMin'] = $min;

      CM_Output::json('count_ok', true, Admin::format_templates_data(array(
        'header' => array(
          'icon' => 'icofont-th-list',
          'title' => self::lang('R_COUNT'),
          'intro' => self::lang('R_COUNT_INTRO'),
          'func' => array()
        ),
        'breadcrumb' => $this->_breadcrumb->trail(),
        'log' => array(
          'func' => $this->get_func_options('count')
        ),
        'bodyData' => $data
      )));
    }

    public function response_count_export() {
      $modeOptions = $this->get_mode_options();
      $cbThat = $this;

      if(!isset($this->_get['mode']) || !Admin::in_options($this->_get['mode'], $modeOptions, $currentModeOptions)) $currentModeOptions = array_shift($modeOptions);

      if($currentModeOptions['id'] == 'month') {
        //每月
        $startDate = isset($this->_get['startDate']) && !empty($this->_get['startDate']) ? date('Y-m-d', strtotime($this->_get['startDate'])) : date('Y-m-d', strtotime('now -1 year'));
        $endDate = isset($this->_get['endDate']) && !empty($this->_get['endDate']) ? date('Y-m-d', strtotime($this->_get['endDate'])) : date('Y-m-d');
        $listArray = array_map(function($date) use($cbThat) {
          $start = date('Y-m-01 00:00:00', strtotime($date));
          $end = date('Y-m-d 23:59:59', strtotime('now +1 month -1 day', strtotime($start)));
          $daoParams = array(
            'isCount' => true,
            'timestamp' => array(
              'start' => $start,
              'end' => $end
            )
          );
          $total = $cbThat->_mod_dao->get_rows($recordRows, '', $daoParams) ? $recordRows['total'] : 0;
          $label = date('Y年n月', strtotime($date));
          return array('label' => $label, 'count' => $total);
        }, array_merge(
          array($startDate),
          Admin_Dgfactor::get_interval_time($startDate, $endDate, 'month')
        ));
        $listColumn = array(
          'label' => $this->lang('COUNT_CHART_MONTH_X'),
          'count' => $this->lang('COUNT_CHART_Y')
        );
      } elseif($currentModeOptions['id'] == 'day') {
        //每日
        $startDate = isset($this->_get['startDate']) && !empty($this->_get['startDate']) ? date('Y-m-d', strtotime($this->_get['startDate'])) : date('Y-m-d', strtotime('now -1 month'));
        $endDate = isset($this->_get['endDate']) && !empty($this->_get['endDate']) ? date('Y-m-d', strtotime($this->_get['endDate'])) : date('Y-m-d');
        $listArray = array_map(function($date) use($cbThat, &$data) {
          $start = date('Y-m-d 00:00:00', strtotime($date));
          $end = date('Y-m-d 23:59:59', strtotime($date));
          $daoParams = array(
            'isCount' => true,
            'timestamp' => array(
              'start' => $start,
              'end' => $end
            )
          );
          $total = $cbThat->_mod_dao->get_rows($recordRows, '', $daoParams) ? $recordRows['total'] : 0;
          $label = date('Y年n月j日', strtotime($date));
          return array('label' => $label, 'count' => $total);
        }, array_merge(
          array($startDate),
          Admin_Dgfactor::get_interval_time($startDate, $endDate, 'day')
        ));
        $listColumn = array(
          'label' => $this->lang('COUNT_CHART_DAY_X'),
          'count' => $this->lang('COUNT_CHART_Y')
        );
      } else {
        //每小時
        $startDate = isset($this->_get['startDate']) && !empty($this->_get['startDate']) ? date('Y-m-d', strtotime($this->_get['startDate'])) : '1970-01-01';
        $endDate = isset($this->_get['endDate']) && !empty($this->_get['endDate']) ? date('Y-m-d', strtotime($this->_get['endDate'])) : date('Y-m-d');
        $hourOptions = array();

        for($i = 0; $i <= 23; $i ++) $hourOptions[] = $i;

        $listArray = array_map(function($hour) use($cbThat, &$data, $startDate, $endDate) {
          $daoParams = array(
            'isCount' => true,
            'hour' => $hour,
            'timestamp' => array(
              'start' => $startDate,
              'end' => $endDate
            )
          );
          $total = $cbThat->_mod_dao->get_rows($recordRows, '', $daoParams) ? $recordRows['total'] : 0;
          $label = "{$hour}時";
          return array('label' => $label, 'count' => $total);
        }, $hourOptions);
        $listColumn = array(
          'label' => $this->lang('COUNT_CHART_HOUR_X'),
          'count' => $this->lang('COUNT_CHART_Y')
        );
      }

      $listColumnCount = count($listColumn);
      header('Content-type:text/html; charset=utf-8');
      header('Content-Type: application/octet-stream');
      header("Content-Disposition: attachment; filename=count_{$currentModeOptions['id']}_export.xls;");
      echo '<HTML xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40">';
      echo '<head><meta http-equiv="content-type" content="application/vnd.ms-excel; charset=UTF-8"></head>';
      echo '<body><table border="1">';
      echo sprintf('<tr><td colspan="%d">%s - %s</td></tr>', $listColumnCount, $this->lang('R_COUNT'), $currentModeOptions['text']);
      echo sprintf('<tr><td colspan="%d">%s ~ %s</td></tr>', $listColumnCount, $startDate, $endDate);
      echo '<tr>';

      foreach($listColumn as $columnName => $columnLabel) echo "<td>{$columnLabel}</td>";

      echo '</tr>';

      foreach($listArray as $listRows) {
        echo '<tr>';

        foreach($listColumn as $columnName => $columnLabel) {
          $columnValue = isset($listRows[$columnName]) ? $listRows[$columnName] : 0;
          echo "<td>{$columnValue}</td>";
        }

        echo '</tr>';
      }

      echo '</table></body></html>';
      exit;
    }

    public function response_rank() {
      $this->_breadcrumb->add(self::lang('R_RANK'), $this->get_request('rank')->get_link());
      $cbThat = $this;
      $dao = Admin_Factory::createCMDAO('video');
      $startDate = isset($this->_get['startDate']) && !empty($this->_get['startDate']) ? date('Y-m-d', strtotime($this->_get['startDate'])) : '1970-01-01';
      $endDate = isset($this->_get['endDate']) && !empty($this->_get['endDate']) ? date('Y-m-d', strtotime($this->_get['endDate'])) : date('Y-m-d');
      $listJSON = $dao->get_array_object(Admin_DAO_Video::GROUP_LIST, array(
        'type' => '1',
        'subSort' => '`viewCount` DESC',
        'viewCount' => array(
          'start' => $startDate,
          'end' => $endDate
        ),
        'viewTime' => array(
          'start' => $startDate,
          'end' => $endDate
        ),
        'keyColumn' => Admin_DAO_Video::COLUMN_RECORD_ID,
        'oNowPage' => isset($this->_get['p']) ? (int)$this->_get['p'] : 1,
        'oPageCallback' => function($page, array $params, $key) use($cbThat) {
          return Admin_Mod::cb_default_pagesplit($page, $params, $key, $cbThat->get_request('rank'), array('vpage' => 'p'));
        }
      ));
      $data = array();
      $data['searchForm'] = array_merge(array(
        'form' => implode('&nbsp;', array(
          sprintf('<label style="cursor: default;">%s：</label>', $this->lang('COUNT_SEARCH_DATE')),
          CM_Html::input_field('startDate', $this->_get['startDate'], 'class="hasDatepicker input-small"'),
          '~',
          CM_Html::input_field('endDate', $this->_get['endDate'], 'class="hasDatepicker input-small"'),
          CM_Html::submit_btn(sprintf('<i class="icofont-search"></i>&nbsp;%s', $this->lang('COUNT_SEARCH_BTN')), array('class' => 'btn')),
          $this->get_request('rank', 'export')
            ->set_params($this->_get)
            ->set_text(sprintf('<i class="icofont-share"></i>&nbsp;%s', $this->lang('R_RANK_EXPORT')))
            ->get_btn(array('class' => 'btn'))
        ))
      ), $this->get_request('rank')->get_form('form', array(
        'method' => 'get',
        'class' => 'form-inline'
      )));
      $data['hasList'] = $listJSON->dataEmpty ? false : true;

      if($data['hasList'] == true) {
        $data['list'] = Admin_Format::to_tmpl_table($listJSON->data, array(
          Admin_DAO_Video::COLUMN_RECORD_ID => Admin_DAO_Video::lang('COLUMN_ID'),
          Admin_DAO_Video::COLUMN_HEADLINE => $this->lang('RANK_COLUMN_VIDEO_HEADLINE'),
          'viewCount' => $this->lang('RANK_COLUMN_VIEW'),
          'viewTimeAVG' => $this->lang('RANK_COLUMN_VIEW_TIME')
        ), array(
          /*'viewTimeAVG' => function($record, $recordRows = array()) {
            if($recordRows['viewCount'] <= 0 || $recordRows['viewTime'] <= 0) return array('text' => '--');

            return array('text' => sprintf('%.2f', $recordRows['viewTime'] / $recordRows['viewCount']));
          }*/
		  'viewTimeAVG' => function($record, $recordRows = array()) use($cbThat) {
            if($recordRows['viewCount'] <= 0 || $recordRows['viewTime'] <= 0) return array('text' => '--');

            return array('text' => $cbThat->sec_to_time(sprintf('%.2f', $recordRows['viewTime'] / $recordRows['viewCount'])));
		   }
        ), array(
          'attr' => 'class="table table-bordered table-striped responsive"',
          'hasResponsive' => false
        ));
        $data['listSplit'] = $listJSON->dataSplit;
      } else {
        $data['listEmpty'] = CM_Lang::line('Admin.ERROR_DATA_EMPTY');
      }

      CM_Output::json('rank_ok', true, Admin::format_templates_data(array(
        'header' => array(
          'icon' => 'icofont-th-list',
          'title' => self::lang('R_RANK'),
          'intro' => self::lang('R_RANK_INTRO'),
          'func' => array()
        ),
        'breadcrumb' => $this->_breadcrumb->trail(),
        'log' => array(
          'func' => $this->get_func_options('rank')
        ),
        'bodyData' => $data
      )));
    }

    public function response_rank_export() {
      $startDate = isset($this->_get['startDate']) && !empty($this->_get['startDate']) ? date('Y-m-d', strtotime($this->_get['startDate'])) : '1970-01-01';
      $endDate = isset($this->_get['endDate']) && !empty($this->_get['endDate']) ? date('Y-m-d', strtotime($this->_get['endDate'])) : date('Y-m-d');
      $dao = Admin_Factory::createCMDAO('video');
      $daoParams = array(
        'type' => '1',
        'subSort' => '`viewCount` DESC',
        'viewCount' => array(
          'start' => $startDate,
          'end' => $endDate
        ),
        'viewTime' => array(
          'start' => $startDate,
          'end' => $endDate
        ),
        'keyColumn' => Admin_DAO_Video::COLUMN_RECORD_ID
      );
      $listColumn = array(
        Admin_DAO_Video::COLUMN_RECORD_ID => Admin_DAO_Video::lang('COLUMN_ID'),
        Admin_DAO_Video::COLUMN_HEADLINE => $this->lang('RANK_COLUMN_VIDEO_HEADLINE'),
        'viewCount' => $this->lang('RANK_COLUMN_VIEW'),
        'viewTimeAVG' => $this->lang('RANK_COLUMN_VIEW_TIME')
      );
      $listArray = $dao->get_array($listArray, Admin_DAO_Video::GROUP_LIST, $daoParams) ? $listArray : array();
      $listColumnCount = count($listColumn);
      header('Content-type:text/html; charset=utf-8');
      header('Content-Type: application/octet-stream');
      header("Content-Disposition: attachment; filename=rank_export.xls;");
      echo '<HTML xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40">';
      echo '<head><meta http-equiv="content-type" content="application/vnd.ms-excel; charset=UTF-8"></head>';
      echo '<body><table border="1">';
      echo sprintf('<tr><td colspan="%d">%s</td></tr>', $listColumnCount, $this->lang('R_RANK'));
      echo sprintf('<tr><td colspan="%d">%s ~ %s</td></tr>', $listColumnCount, $startDate, $endDate);
      echo '<tr>';

      foreach($listColumn as $columnName => $columnLabel) echo "<td>{$columnLabel}</td>";

      echo '</tr>';

      foreach($listArray as $listRows) {
        echo '<tr>';

        foreach($listColumn as $columnName => $columnLabel) {
          $columnValue = isset($listRows[$columnName]) ? $listRows[$columnName] : ''; 

          if($columnName == 'viewTimeAVG') {
            //if($listRows['viewCount'] <= 0 || $listRows['viewTime'] <= 0) $columnValue = '--';
            //$columnValue = sprintf('%.2f', $listRows['viewTime'] / $listRows['viewCount']);
			 if($listRows['viewCount'] <= 0 || $listRows['viewTime'] <= 0) {
              $columnValue = '--'; 
            } else {
              $columnValue = $this->sec_to_time(sprintf('%.2f', $listRows['viewTime'] / $listRows['viewCount']));
            }
          }

          echo "<td>{$columnValue}</td>";
        }

        echo '</tr>';
      }

      echo '</table></body></html>';
      exit;
    }

    public function response_connection() {
      $cbThat = $this;
      $cDAO = Admin_Factory::createCMDAO('customers');
      $customersOptions = array_map(function($element) {
        return array(
          'id' => $element[Admin_DAO_Customers::COLUMN_RECORD_ID],
          'text' => $element[Admin_DAO_Customers::COLUMN_RECORD_ID] . '&nbsp;-&nbsp;' . $element[Admin_DAO_Customers::COLUMN_HEADLINE]
          //'id' => $element[Site_DAO_Domain::COLUMN_RECORD_ID],
          //'text' => $element[Site_DAO_Domain::COLUMN_RECORD_ID] . '&nbsp;-&nbsp;' . $element[Admin_DAO_Customers::COLUMN_HEADLINE]
        );
      }, $cDAO->get_array($customersOptions, '') ? $customersOptions : array());
      
      //var_dump($customersOptions);
      
      //$cgDAO = Admin_Factory::createCMDAO('customers_Group');
      $cgDAO = Admin_Factory::createCMDAO('domain');
      $customersGroupOptions = array_map(function($element) {
        return array(
          //'id' => $element[Admin_DAO_Customers_Group::COLUMN_RECORD_ID],
          //'text' => $element[Admin_DAO_Customers_Group::COLUMN_RECORD_ID] . '&nbsp;-&nbsp;' . $element[Admin_DAO_Customers_Group::COLUMN_HEADLINE]
          'id' => $element[Admin_DAO_Domain::COLUMN_RECORD_ID],
          'text' => $element[Admin_DAO_Domain::COLUMN_RECORD_ID] . '&nbsp;-&nbsp;' . $element[Admin_DAO_Domain::COLUMN_HEADLINE]
        );
      }, $cgDAO->get_array($customersGroupOptions, '') ? $customersGroupOptions : array());

      
      //var_dump($customersGroupOptions);
      
      $customersOptions = array_merge(array(
        array('id' => '', 'text' => ''),
		array('id' => 'a', 'text' => 'ALL')
      ), $customersOptions);
      $customersGroupOptions = array_merge(array(
        array('id' => '', 'text' => ''),
		array('id' => 'a', 'text' => 'ALL')
      ), $customersGroupOptions);
      $currentType = isset($this->_get['customersType']) && $this->_get['customersType'] == 'customersGroup' ? 'customersGroup' : 'customersId';
      $this->_breadcrumb->add(self::lang('R_CONNECTION'), $this->get_request('connection')->get_link());
      $data = array();
      $daoParams = array(
        'keyColumn' => Admin_DAO_Customers::COLUMN_RECORD_ID,
        'oNowPage' => isset($this->_get['p']) ? (int)$this->_get['p'] : 1,
        'oPageCallback' => function($page, array $params, $key) use($cbThat) {
          return Admin_Mod::cb_default_pagesplit($page, $params, $key, $cbThat->get_request('connection'), array('vpage' => 'p'));
        }
      );

      //if(isset($this->_get[$currentType])) {
	  if(!empty($currentType) && isset($this->_get[$currentType]) && $this->_get[$currentType] != 'a') {
        if($currentType == 'customersId' && Admin::in_options($this->_get[$currentType], $customersOptions, $currentOptions)) {
          $daoParams = array('recordId' => $currentOptions['id']);
        } elseif($currentType == 'customersGroup' && Admin::in_options($this->_get[$currentType], $customersGroupOptions, $currentOptions) ) {
          $daoParams = array('groupId' => $currentOptions['id']);
        }
      }

      $listJSON = $cDAO->get_array_object(Admin_DAO_Customers::GROUP_LIST, $daoParams);
      $data['hasList'] = $listJSON->dataEmpty ? false : true;

      if($data['hasList'] == true) {
        $startDate = isset($this->_get['startDate']) && !empty($this->_get['startDate']) ? date('Y-m-d', strtotime($this->_get['startDate'])) : '1970-01-01';
        $endDate = isset($this->_get['endDate']) && !empty($this->_get['endDate']) ? date('Y-m-d', strtotime($this->_get['endDate'])) : date('Y-m-d');
        $logDaoParams = array(
          'viewCount' => true,
          'viewTime' => true,
          'timestamp' => array(
            'start' => $startDate,
            'end' => $endDate
          )
        );
        $logCache = array();
        $data['list'] = Admin_Format::to_tmpl_table($listJSON->data, array(
          Admin_DAO_Customers::COLUMN_RECORD_ID => Admin_DAO_Customers::lang('COLUMN_ID'),
          Admin_DAO_Customers::COLUMN_HEADLINE => Admin_DAO_Customers::lang('COLUMN_NAME'),
          'viewCount' => $this->lang('CONNECTION_COLUMN_VIEW'),
          'viewAll' => $this->lang('CONNECTION_COLUMN_VIEW_ALL'),
          'viewTimeAVG' => $this->lang('CONNECTION_COLUMN_VIEW_TIME'),
          'action' => $this->lang('CONNECTION_COLUMN_DETAIL')
        ), array(
          'viewCount' => function($record, $recordRows = array()) use($cbThat, $logDaoParams, &$logCache) {
            if(!isset($logCache[$recordRows[Admin_DAO_Customers::COLUMN_RECORD_ID]])) {
              $tmpParams = array_merge($logDaoParams, array(
                //'customersId' => $recordRows[Admin_DAO_Customers::COLUMN_RECORD_ID]
                //Table:stream_log的iUsr_seq,原本是比對Table:customers的id,改為比對name
              	'customersId' => $recordRows[Admin_DAO_Customers::COLUMN_HEADLINE]
              ));
              $logCache[$recordRows[Admin_DAO_Customers::COLUMN_RECORD_ID]] = $cbThat->_mod_dao->get_rows($logRows, '', $tmpParams) ? $logRows : array();
            }

            return array('text' => $logCache[$recordRows[Admin_DAO_Customers::COLUMN_RECORD_ID]]['viewCount']);
          },
          /*'viewAll' => function($record, $recordRows = array()) use(&$logCache) {
            return array('text' => $logCache[$recordRows[Admin_DAO_Customers::COLUMN_RECORD_ID]]['viewTime']);
          },*/
		  'viewAll' => function($record, $recordRows = array()) use(&$logCache, $cbThat) {
            return array('text' => $cbThat->sec_to_time($logCache[$recordRows[Admin_DAO_Customers::COLUMN_RECORD_ID]]['viewTime']));
          },
          /*'viewTimeAVG' => function($record, $recordRows = array()) use(&$logCache) {
            $viewCount = $logCache[$recordRows[Admin_DAO_Customers::COLUMN_RECORD_ID]]['viewCount'];
            $viewTime = $logCache[$recordRows[Admin_DAO_Customers::COLUMN_RECORD_ID]]['viewTime'];

            if($viewCount <= 0) return array('text' => '--');

            if($viewTime <= 0) {
              return array('text' => '--');
            } else {
              return array('text' => sprintf('%.2f', $viewTime / $viewCount));
            }
          },*/
		  'viewTimeAVG' => function($record, $recordRows = array()) use(&$logCache, $cbThat) {
            $viewCount = $logCache[$recordRows[Admin_DAO_Customers::COLUMN_RECORD_ID]]['viewCount'];
            $viewTime = $logCache[$recordRows[Admin_DAO_Customers::COLUMN_RECORD_ID]]['viewTime'];

            if($viewCount <= 0) return array('text' => '--');

            if($viewTime <= 0) {
              return array('text' => '--');
            } else {
              return array('text' => $cbThat->sec_to_time(sprintf('%.2f', $viewTime / $viewCount)));
            }
          },
          'action' => function($record, $recordRows = array()) use($cbThat) {
            $requestDetail = $cbThat->get_request('connectionDetail')
              ->set_params(array(Admin::VAR_RECORD_ID => $recordRows[Admin_DAO_Customers::COLUMN_RECORD_ID]))
              ->set_attr_params(array('class' => 'link'))
              ->set_text(sprintf('<i class="icofont-bar-chart"></i>&nbsp;%s', $cbThat->lang('R_CONNECTION_DETAIL')));

            if($requestDetail->check_permission() == true) {
              return array('text' => $requestDetail->get_link());
            } else {
              return array('text' => '&nbsp;');
            }
          }
        ), array(
          'attr' => 'class="table table-bordered table-striped responsive"',
          'hasResponsive' => false
        ));
        $data['listSplit'] = $listJSON->dataSplit;
      } else {
        $data['listEmpty'] = CM_Lang::line('Admin.ERROR_DATA_EMPTY');
      }
      
      $data['searchForm'] = array_merge(array(
        'form' => implode('&nbsp;', array(
          CM_Html::radio_field('customersType', 'customersId', $currentType == 'customersId' ? true : false) .
          CM_Html::select_field('customersId', $customersOptions, $this->_get['customersId'], sprintf('class="input-medium" data-placeholder="%s"', $this->lang('CONNECTION_CUSTOMERS_ID_PLACEHOLDER'))),
          CM_Html::radio_field('customersType', 'customersGroup', $currentType == 'customersGroup' ? true : false) .
          CM_Html::select_field('customersGroup', $customersGroupOptions, $this->_get['customersGroup'], sprintf('class="input-medium" data-placeholder="%s"', $this->lang('CONNECTION_CUSTOMERS_GROUP_ID_PLACEHOLDER'))),
          sprintf('<label style="cursor: default;">%s：</label>', $this->lang('COUNT_SEARCH_DATE')),
          CM_Html::input_field('startDate', $this->_get['startDate'], 'class="hasDatepicker input-small"'),
          '~',
          CM_Html::input_field('endDate', $this->_get['endDate'], 'class="hasDatepicker input-small"'),
          CM_Html::submit_btn(sprintf('<i class="icofont-search"></i>&nbsp;%s', $this->lang('COUNT_SEARCH_BTN')), array('class' => 'btn')),
          $this->get_request('connection', 'export')
            ->set_params($this->_get)
            ->set_text(sprintf('<i class="icofont-share"></i>&nbsp;%s', $this->lang('R_CONNECTION_EXPORT')))
            ->get_btn(array('class' => 'btn'))
        ))
      ), $this->get_request('connection')->get_form('form', array(
        'method' => 'get',
        'class' => 'form-inline'
      )));

      CM_Output::json('connection_ok', true, Admin::format_templates_data(array(
        'header' => array(
          'icon' => 'icofont-th-list',
          'title' => self::lang('R_CONNECTION'),
          'intro' => self::lang('R_CONNECTION_INTRO'),
          'func' => array()
        ),
        'breadcrumb' => $this->_breadcrumb->trail(),
        'log' => array(
          'func' => $this->get_func_options('connection')
        ),
        'bodyData' => $data
      )));
    }

    public function response_connection_export() {
      $currentType = isset($this->_get['customersType']) && $this->_get['customersType'] == 'customersGroup' ? 'customersGroup' : 'customersId';
      $daoParams = array();

      if(isset($this->_get[$currentType])) {
        if($currentType == 'customersId' && Admin::in_options($this->_get[$currentType], $customersOptions, $currentOptions)) {
          $daoParams = array('recordId' => $currentOptions['id']);
        } elseif($currentType == 'customersGroup' && Admin::in_options($this->_get[$currentType], $customersGroupOptions, $currentOptions) ) {
          $daoParams = array('groupId' => $currentOptions['id']);
        }
      }

      $startDate = isset($this->_get['startDate']) && !empty($this->_get['startDate']) ? date('Y-m-d', strtotime($this->_get['startDate'])) : '1970-01-01';
      $endDate = isset($this->_get['endDate']) && !empty($this->_get['endDate']) ? date('Y-m-d', strtotime($this->_get['endDate'])) : date('Y-m-d');
      $dao = Admin_Factory::createCMDAO('customers');
      $listColumn = array(
        Admin_DAO_Customers::COLUMN_RECORD_ID => Admin_DAO_Customers::lang('COLUMN_ID'),
        Admin_DAO_Customers::COLUMN_HEADLINE => Admin_DAO_Customers::lang('COLUMN_NAME'),
        'viewCount' => $this->lang('CONNECTION_COLUMN_VIEW'),
        'viewAll' => $this->lang('CONNECTION_COLUMN_VIEW_ALL'),
        'viewTimeAVG' => $this->lang('CONNECTION_COLUMN_VIEW_TIME')
      );
      $listArray = $dao->get_array($listArray, Admin_DAO_Customers::GROUP_LIST, $daoParams) ? $listArray : array();
      $listColumnCount = count($listColumn);
      $logDaoParams = array(
        'viewCount' => true,
        'viewTime' => true,
        'timestamp' => array(
          'start' => $startDate,
          'end' => $endDate
        )
      );
      $logCache = array();
      header('Content-type:text/html; charset=utf-8');
      header('Content-Type: application/octet-stream');
      header("Content-Disposition: attachment; filename=connection_export.xls;");
      echo '<HTML xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40">';
      echo '<head><meta http-equiv="content-type" content="application/vnd.ms-excel; charset=UTF-8"></head>';
      echo '<body><table border="1">';
      echo sprintf('<tr><td colspan="%d">%s</td></tr>', $listColumnCount, $this->lang('R_CONNECTION'));
      echo sprintf('<tr><td colspan="%d">%s ~ %s</td></tr>', $listColumnCount, $startDate, $endDate);
      echo '<tr>';

      foreach($listColumn as $columnName => $columnLabel) echo "<td>{$columnLabel}</td>";

      echo '</tr>';

      foreach($listArray as $listRows) {
        echo '<tr>';

        if(!isset($logCache[$listRows[Admin_DAO_Customers::COLUMN_RECORD_ID]])) {
          $tmpParams = array_merge($logDaoParams, array(
            //'customersId' => $listRows[Admin_DAO_Customers::COLUMN_RECORD_ID]
          	//Table:stream_log的iUsr_seq,原本是比對Table:customers的id,改為比對name
          	'customersId' => $listRows[Admin_DAO_Customers::COLUMN_HEADLINE]
          		        	
          		
          ));
          $logCache[$listRows[Admin_DAO_Customers::COLUMN_RECORD_ID]] = $this->_mod_dao->get_rows($logRows, '', $tmpParams) ? $logRows : array();
        }

        $viewTime = $logCache[$listRows[Admin_DAO_Customers::COLUMN_RECORD_ID]]['viewTime'];
        $viewCount = $logCache[$listRows[Admin_DAO_Customers::COLUMN_RECORD_ID]]['viewCount'];

        foreach($listColumn as $columnName => $columnLabel) {
          $columnValue = isset($listRows[$columnName]) ? $listRows[$columnName] : ''; 

          if($columnName == 'viewCount') {
            //$columnValue = $viewTime;
			$columnValue = $this->sec_to_time($viewTime);
          } elseif($columnName == 'viewAll') {
            $columnValue = $viewCount;
          } elseif($columnName == 'viewTimeAVG') {
            //if($viewCount <= 0 || $viewTime <= 0) $columnValue = '--';

            //$columnValue = sprintf('%.2f', $viewTime / $viewCount);
			 if($viewCount <= 0 || $viewTime <= 0) {
              $columnValue = '--';
            } else {
              $columnValue = $this->sec_to_time(sprintf('%.2f', $viewTime / $viewCount));
            }
          }

          echo "<td>{$columnValue}</td>";
        }

        echo '</tr>';
      }

      echo '</table></body></html>';
      exit;
    }

    public function response_connectionDetail() {
      $cbThat = $this;
      $cDAO = Admin_Factory::createCMDAO('customers');

      if(!isset($this->_get[Admin::VAR_RECORD_ID]) || !$cDAO->get_rows_byId($customersRows, '', (int)$this->_get[Admin::VAR_RECORD_ID])) {
        Admin::response_error(CM_Lang::line('CM.ERROR_DATA_NOT_FOUND')); 
      //} elseif(!$this->_mod_dao->get_array($logArray, '', array('customersId' => $customersRows[Admin_DAO_Customers::COLUMN_RECORD_ID]))) {
      //Table:stream_log的iUsr_seq,原本是比對Table:customers的id,改為比對name
      } elseif(!$this->_mod_dao->get_array($logArray, '', array('customersId' => $customersRows[Admin_DAO_Customers::COLUMN_HEADLINE]))) {	
        Admin::response_error($this->lang('ERROR_DETAIL_NOT_FOUND')); 
      }

      $videoId = array();

      foreach($logArray as $logRows) $videoId[$logRows['iMda_seq']] = 1;

      $videoId = array_keys($videoId);
      $data = array();
      $vDAO = Admin_Factory::createCMDAO('video');
      $startDate = isset($this->_get['startDate']) && !empty($this->_get['startDate']) ? date('Y-m-d', strtotime($this->_get['startDate'])) : '1970-01-01';
      $endDate = isset($this->_get['endDate']) && !empty($this->_get['endDate']) ? date('Y-m-d', strtotime($this->_get['endDate'])) : date('Y-m-d');
      $listJSON = $vDAO->get_array_object(Admin_DAO_Video::GROUP_LIST, array(
        'recordIds' => $videoId,
        'type' => '1',
        'subSort' => '`viewCount` DESC',
        'viewCount' => array(
          'start' => $startDate,
          'end' => $endDate
        ),
        'viewTime' => array(
          'start' => $startDate,
          'end' => $endDate
        ),
        //'viewCustomersId' => $customersRows[Admin_DAO_Customers::COLUMN_RECORD_ID],
      	//Table:stream_log的iUsr_seq,原本是比對Table:customers的id,改為比對name
      	'viewCustomersId' => $customersRows[Admin_DAO_Customers::COLUMN_HEADLINE],
        //
      	'keyColumn' => Admin_DAO_Video::COLUMN_RECORD_ID,
        'oNowPage' => isset($this->_get['p']) ? (int)$this->_get['p'] : 1,
        'oPageCallback' => function($page, array $params, $key) use($cbThat) {
          return Admin_Mod::cb_default_pagesplit($page, $params, $key, $cbThat->get_request('connectionDetail'), array('vpage' => 'p'));
        }
      ));
      $data['hasList'] = $listJSON->dataEmpty ? false : true;

      if($data['hasList'] == true) {
        $data['list'] = Admin_Format::to_tmpl_table($listJSON->data, array(
          'customersId' => $this->lang('CONNECTION_COLUMN_CUSTOMERS_ID'),
          Admin_DAO_Video::COLUMN_HEADLINE => $this->lang('CONNECTION_COLUMN_VIDEO_HEADLINE'),
          'viewCount' => $this->lang('CONNECTION_COLUMN_VIEW'),
          'viewTime' => $this->lang('CONNECTION_COLUMN_VIEW_ALL'),
          'viewTimeAVG' => $this->lang('CONNECTION_COLUMN_VIEW_TIME')
        ), array(
          'customersId' => function($record, $recordRows = array()) use($customersRows) {
            //
        	return array('text' => $customersRows[Admin_DAO_Customers::COLUMN_HEADLINE]);
          },
		  'viewTime' => function($record, $recordRows = array()) use($cbThat) {
            return array('text' => $cbThat->sec_to_time($record));
          },
          /*'viewTimeAVG' => function($record, $recordRows = array()) {
            if($recordRows['viewCount'] <= 0 || $recordRows['viewTime'] <= 0) return array('text' => '--');

            return array('text' => sprintf('%.2f', $recordRows['viewTime'] / $recordRows['viewCount']));
          }*/
		  'viewTimeAVG' => function($record, $recordRows = array()) use($cbThat) {
            if($recordRows['viewCount'] <= 0 || $recordRows['viewTime'] <= 0) return array('text' => '--');

            return array('text' => $cbThat->sec_to_time(sprintf('%.2f', $recordRows['viewTime'] / $recordRows['viewCount'])));
          }
        ), array(
          'attr' => 'class="table table-bordered table-striped responsive"',
          'hasResponsive' => false
        ));
        $data['listSplit'] = $listJSON->dataSplit;
      } else {
        $data['listEmpty'] = CM_Lang::line('Admin.ERROR_DATA_EMPTY');
      }

      $data['searchForm'] = array_merge(array(
        'form' => implode('&nbsp;', array(
          sprintf('<label style="cursor: default;">%s：</label>', $this->lang('COUNT_SEARCH_DATE')),
          CM_Html::input_field('startDate', $this->_get['startDate'], 'class="hasDatepicker input-small"'),
          '~',
          CM_Html::input_field('endDate', $this->_get['endDate'], 'class="hasDatepicker input-small"'),
          CM_Html::submit_btn(sprintf('<i class="icofont-search"></i>&nbsp;%s', $this->lang('COUNT_SEARCH_BTN')), array('class' => 'btn')) .
          //
          CM_Html::hidden_field(Admin::VAR_RECORD_ID, $customersRows[Admin_DAO_Customers::COLUMN_RECORD_ID]),
          $this->get_request('connectionDetail', 'export')
            ->set_params($this->_get)
            ->set_text(sprintf('<i class="icofont-share"></i>&nbsp;%s', $this->lang('R_CONNECTION_DETAIL_EXPORT')))
            ->get_btn(array('class' => 'btn')),
          CM_Html::func_btn(sprintf('<i class="icofont-arrow-left"></i>&nbsp;%s', $this->lang('CONNECTION_PREV_BTN')), array('class' => 'btn prevBtn'))
        ))
      ), $this->get_request('connectionDetail')->get_form('form', array(
        'method' => 'get',
        'class' => 'form-inline'
      )));

      CM_Output::json('connection_detail_ok', true, Admin::format_templates_data(array(
        'header' => array(
          'icon' => 'icofont-th-list',
          'title' => self::lang('R_CONNECTION_DETAIL'),
          'intro' => self::lang('R_CONNECTION_DETAIL_INTRO'),
          'func' => array()
        ),
        'breadcrumb' => $this->_breadcrumb->trail(),
        'log' => array(
          'func' => $this->get_func_options('connection')
        ),
        'bodyData' => $data
      )));
    }

    public function response_connectionDetail_export() {
      $cDAO = Admin_Factory::createCMDAO('customers');

      if(!isset($this->_get[Admin::VAR_RECORD_ID]) || !$cDAO->get_rows_byId($customersRows, '', (int)$this->_get[Admin::VAR_RECORD_ID])) {
        Admin::response_error(CM_Lang::line('CM.ERROR_DATA_NOT_FOUND')); 
      } elseif(!$this->_mod_dao->get_array($logArray, '', array('customersId' => $customersRows[Admin_DAO_Customers::COLUMN_RECORD_ID]))) {
        Admin::response_error($this->lang('ERROR_DETAIL_NOT_FOUND')); 
      }

      $videoId = array();

      foreach($logArray as $logRows) $videoId[$logRows['iMda_seq']] = 1;

      $videoId = array_keys($videoId);
      $startDate = isset($this->_get['startDate']) && !empty($this->_get['startDate']) ? date('Y-m-d', strtotime($this->_get['startDate'])) : '1970-01-01';
      $endDate = isset($this->_get['endDate']) && !empty($this->_get['endDate']) ? date('Y-m-d', strtotime($this->_get['endDate'])) : date('Y-m-d');
      $dao = Admin_Factory::createCMDAO('video');
      $daoParams = array(
        'recordIds' => $videoId,
        'type' => '1',
        'subSort' => '`viewCount` DESC',
        'viewCount' => array(
          'start' => $startDate,
          'end' => $endDate
        ),
        'viewTime' => array(
          'start' => $startDate,
          'end' => $endDate
        ),
        'viewCustomersId' => $customersRows[Admin_DAO_Customers::COLUMN_RECORD_ID],
        'keyColumn' => Admin_DAO_Video::COLUMN_RECORD_ID
      );
      $listColumn = array(
        'customersId' => $this->lang('CONNECTION_COLUMN_CUSTOMERS_ID'),
        Admin_DAO_Video::COLUMN_HEADLINE => $this->lang('CONNECTION_COLUMN_VIDEO_HEADLINE'),
        'viewCount' => $this->lang('CONNECTION_COLUMN_VIEW'),
        'viewTime' => $this->lang('CONNECTION_COLUMN_VIEW_TIME'),
        'viewTimeAVG' => $this->lang('CONNECTION_COLUMN_VIEW_TIME')
      );
      $listArray = $dao->get_array($listArray, Admin_DAO_Video::GROUP_LIST, $daoParams) ? $listArray : array();
      $listColumnCount = count($listColumn);
      header('Content-type:text/html; charset=utf-8');
      header('Content-Type: application/octet-stream');
      header("Content-Disposition: attachment; filename=connectionDetail_export.xls;");
      echo '<HTML xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40">';
      echo '<head><meta http-equiv="content-type" content="application/vnd.ms-excel; charset=UTF-8"></head>';
      echo '<body><table border="1">';
      echo sprintf('<tr><td colspan="%d">%s - %s</td></tr>', $listColumnCount, $this->lang('R_CONNECTION'), $this->lang('R_CONNECTION_DETAIL'));
      echo sprintf('<tr><td colspan="%d">%s ~ %s</td></tr>', $listColumnCount, $startDate, $endDate);
      echo '<tr>';

      foreach($listColumn as $columnName => $columnLabel) echo "<td>{$columnLabel}</td>";

      echo '</tr>';

      foreach($listArray as $listRows) {
        echo '<tr>';

        foreach($listColumn as $columnName => $columnLabel) {
          $columnValue = isset($listRows[$columnName]) ? $listRows[$columnName] : ''; 

          if($columnName == 'customersId') {
            $columnValue = $customersRows[Admin_DAO_Customers::COLUMN_RECORD_ID];
		  } elseif($columnName == 'viewTime') {
            $columnValue = $this->sec_to_time($listRows['viewTime']);
          } elseif($columnName == 'viewTimeAVG') {
            //if($listRows['viewCount'] <= 0 || $listRows['viewTime'] <= 0) $columnValue = '--';
           //$columnValue = sprintf('%.2f', $listRows['viewTime'] / $listRows['viewCount']);
		    if($listRows['viewCount'] <= 0 || $listRows['viewTime'] <= 0) {
              $columnValue = '--'; 
            } else {
              $columnValue = $this->sec_to_time(sprintf('%.2f', $listRows['viewTime'] / $listRows['viewCount']));
            }
          }

          echo "<td>{$columnValue}</td>";
        }

        echo '</tr>';
      }

      echo '</table></body></html>';
      exit;
    }

    public function response_country() {
      $this->_breadcrumb->add(self::lang('R_COUNTRY'), $this->get_request('country')->get_link());
      $startDate = isset($this->_get['startDate']) && !empty($this->_get['startDate']) ? date('Y-m-d', strtotime($this->_get['startDate'])) : '1970-01-01';
      $endDate = isset($this->_get['endDate']) && !empty($this->_get['endDate']) ? date('Y-m-d', strtotime($this->_get['endDate'])) : date('Y-m-d');
      $data = array();
      $data['searchForm'] = array_merge(array(
        'form' => implode('&nbsp;', array(
          sprintf('<label style="cursor: default;">%s：</label>', $this->lang('COUNT_SEARCH_DATE')),
          CM_Html::input_field('startDate', $this->_get['startDate'], 'class="hasDatepicker input-small"'),
          '~',
          CM_Html::input_field('endDate', $this->_get['endDate'], 'class="hasDatepicker input-small"'),
          CM_Html::submit_btn(sprintf('<i class="icofont-search"></i>&nbsp;%s', $this->lang('COUNT_SEARCH_BTN')), array('class' => 'btn')),
          $this->get_request('country', 'export')
            ->set_params($this->_get)
            ->set_text(sprintf('<i class="icofont-share"></i>&nbsp;%s', $this->lang('R_COUNTRY_EXPORT')))
            ->get_btn(array('class' => 'btn'))
        ))
      ), $this->get_request('country')->get_form('form', array(
        'method' => 'get',
        'class' => 'form-inline'
      )));
      $daoParams = array(
        'viewCount' => true,
        'viewTime' => true,
        'timestamp' => array(
          'start' => $startDate,
          'end' => $endDate
        )
      );
      $data['countryChart'] = array(
        'id' => 'country_chart_' . time(),
        'headline' => $this->lang('R_COUNTRY'),
        'xLabel' => $this->lang('COUNTRY_CHART_X'),
        'yLabel' => $this->lang('COUNTRY_CHART_Y'),
        'xData' => array(),
        'yData' => array()
      );
      $listColumn = array(
        'label' => $this->lang('COUNTRY_COLUMN_COUNTRY'),
        'viewCount' => $this->lang('COUNTRY_COLUMN_VIEW_COUNT'),
        'viewTimeAVG' => $this->lang('COUNTRY_COLUMN_VIEW_TIME')
      );
      $listArray = array();
	  $max = 0;
      $min = 0;

      foreach(CM_Country::get_options() as $options) {
        $tmpParams = array_merge($daoParams, array(
          'country' => CM_Country::get_item($options['id'], 'iso2')
        ));
        $this->_mod_dao->get_rows($recordRows, '', $tmpParams);
  
        if($recordRows['viewCount'] <= 0) continue;

        if($recordRows['viewTime'] <= 0) {
          $recordRows['viewTimeAVG'] = '--';
        } else {
          //$recordRows['viewTimeAVG'] = sprintf('%.2f', $recordRows['viewTime'] / $recordRows['viewCount']);
		  $recordRows['viewTimeAVG'] = $this->sec_to_time(sprintf('%.2f', $recordRows['viewTime'] / $recordRows['viewCount']));
        }

        $recordRows['label'] = $options['text'];
        $listArray[] = $recordRows;

        $data['countryChart']['xData'][] = $options['text'];
        $data['countryChart']['yData'][] = $recordRows['viewCount'];
		
		if($recordRows['viewCount'] > $max) $max = $recordRows['viewCount'];

        if($recordRows['viewCount'] < $min) $min = $recordRows['viewCount'];
      }
	  
	  $data['countryChart']['yMax'] = $max;
      $data['countryChart']['yMin'] = $min;

      if(count($listArray) > 0) {
        $data['hasList'] =  true;
        $data['list'] = Admin_Format::to_tmpl_table($listArray, $listColumn, array(), array(
          'attr' => 'class="table table-bordered table-striped responsive"',
          'hasResponsive' => false
        ));
      } else {
        $data['hasList'] =  false;
        $data['listEmpty'] = CM_Lang::line('Admin.ERROR_DATA_EMPTY');
      }

      CM_Output::json('country_ok', true, Admin::format_templates_data(array(
        'header' => array(
          'icon' => 'icofont-th-list',
          'title' => self::lang('R_COUNTRY'),
          'intro' => self::lang('R_COUNTRY_INTRO'),
          'func' => array()
        ),
        'breadcrumb' => $this->_breadcrumb->trail(),
        'log' => array(
          'func' => $this->get_func_options('country')
        ),
        'bodyData' => $data
      )));
    }

    public function response_country_export() {
      $startDate = isset($this->_get['startDate']) && !empty($this->_get['startDate']) ? date('Y-m-d', strtotime($this->_get['startDate'])) : '1970-01-01';
      $endDate = isset($this->_get['endDate']) && !empty($this->_get['endDate']) ? date('Y-m-d', strtotime($this->_get['endDate'])) : date('Y-m-d');
      $daoParams = array(
        'viewCount' => true,
        'viewTime' => true,
        'timestamp' => array(
          'start' => $startDate,
          'end' => $endDate
        )
      );
      $listColumn = array(
        'label' => $this->lang('COUNTRY_COLUMN_COUNTRY'),
        'viewCount' => $this->lang('COUNTRY_COLUMN_VIEW_COUNT'),
        'viewTimeAVG' => $this->lang('COUNTRY_COLUMN_VIEW_TIME')
      );
      $listArray = array();

      foreach(CM_Country::get_options() as $options) {
        $tmpParams = array_merge($daoParams, array(
          'country' => CM_Country::get_item($options['id'], 'iso2')
        ));
        $this->_mod_dao->get_rows($recordRows, '', $tmpParams);
  
        if($recordRows['viewCount'] <= 0) continue;

        if($recordRows['viewTime'] <= 0) {
          $recordRows['viewTimeAVG'] = '--';
        } else {
          //$recordRows['viewTimeAVG'] = sprintf('%.2f', $recordRows['viewTime'] / $recordRows['viewCount']);
		  $recordRows['viewTimeAVG'] = $this->sec_to_time(sprintf('%.2f', $recordRows['viewTime'] / $recordRows['viewCount']));
        }

        $recordRows['label'] = $options['text'];
        $listArray[] = $recordRows;
      }

      $listColumnCount = count($listColumn);
      header('Content-type:text/html; charset=utf-8');
      header('Content-Type: application/octet-stream');
      header("Content-Disposition: attachment; filename=country_export.xls;");
      echo '<HTML xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40">';
      echo '<head><meta http-equiv="content-type" content="application/vnd.ms-excel; charset=UTF-8"></head>';
      echo '<body><table border="1">';
      echo sprintf('<tr><td colspan="%d">%s</td></tr>', $listColumnCount, $this->lang('R_COUNTRY'));
      echo sprintf('<tr><td colspan="%d">%s ~ %s</td></tr>', $listColumnCount, $startDate, $endDate);
      echo '<tr>';

      foreach($listColumn as $columnName => $columnLabel) echo "<td>{$columnLabel}</td>";

      echo '</tr>';

      foreach($listArray as $listRows) {
        echo '<tr>';

        foreach($listColumn as $columnName => $columnLabel) {
          echo "<td>{$listRows[$columnName]}</td>";
        }

        echo '</tr>';
      }

      echo '</table></body></html>';
      exit;
    }

    public function response_device() {
      $this->_breadcrumb->add(self::lang('R_DEVICE'), $this->get_request('device')->get_link());
      $startDate = isset($this->_get['startDate']) && !empty($this->_get['startDate']) ? date('Y-m-d', strtotime($this->_get['startDate'])) : '1970-01-01';
      $endDate = isset($this->_get['endDate']) && !empty($this->_get['endDate']) ? date('Y-m-d', strtotime($this->_get['endDate'])) : date('Y-m-d');
      $data = array();
      $data['searchForm'] = array_merge(array(
        'form' => implode('&nbsp;', array(
          sprintf('<label style="cursor: default;">%s：</label>', $this->lang('COUNT_SEARCH_DATE')),
          CM_Html::input_field('startDate', $this->_get['startDate'], 'class="hasDatepicker input-small"'),
          '~',
          CM_Html::input_field('endDate', $this->_get['endDate'], 'class="hasDatepicker input-small"'),
          CM_Html::submit_btn(sprintf('<i class="icofont-search"></i>&nbsp;%s', $this->lang('COUNT_SEARCH_BTN')), array('class' => 'btn')),
          $this->get_request('device', 'export')
            ->set_params($this->_get)
            ->set_text(sprintf('<i class="icofont-share"></i>&nbsp;%s', $this->lang('R_DEVICE_EXPORT')))
            ->get_btn(array('class' => 'btn'))
        ))
      ), $this->get_request('device')->get_form('form', array(
        'method' => 'get',
        'class' => 'form-inline'
      )));
      $listColumn = array();
      $listRows = array();
      $daoParams = array(
        'isCount' => true,
        'timestamp' => array(
          'start' => $startDate,
          'end' => $endDate
        )
      );
      $data['deviceChart'] = array(
        'id' => 'device_chart_' . time(),
        'headline' => $this->lang('R_DEVICE'),
        'xLabel' => $this->lang('DEVICE_CHART_X'),
        'yLabel' => $this->lang('DEVICE_CHART_Y'),
        'xData' => array(),
        'yData' => array()
      );

      $listColumn['view'] = $this->lang('DEVICE_CHART_X');
      $listRows['view'] = $this->lang('DEVICE_CHART_Y');
	  $max = 0;
      $min = 0;

      foreach($this->get_device_options() as $options) {
        $total = $this->_mod_dao->get_rows($recordRows, '', array_merge($daoParams, array('device' => $options['id']))) ? $recordRows['total'] : '0';
        $listColumn[$options['id']] = $options['text'];
        $listRows[$options['id']] = $total;
        $data['deviceChart']['xData'][] = $options['text'];
        $data['deviceChart']['yData'][] = $total;
		if($total > $max) $max = $total;

        if($total < $min) $min = $total;
      }

      $data['deviceChart']['yMax'] = $max;
      $data['deviceChart']['yMin'] = $min;
	  $data['list'] = Admin_Format::to_tmpl_table(array($listRows), $listColumn, array(), array(
        'attr' => 'class="table table-bordered table-striped responsive"',
        'hasResponsive' => false,
        'cols_header_callback' => function($name, $label) {
          if($name == 'view') {
            return array('text' => $label, 'attr' => 'width="80"');
          } else {
            return array('text' => $label);
          }
        }
      ));

      CM_Output::json('device_ok', true, Admin::format_templates_data(array(
        'header' => array(
          'icon' => 'icofont-th-list',
          'title' => self::lang('R_DEVICE'),
          'intro' => self::lang('R_DEVICE_INTRO'),
          'func' => array()
        ),
        'breadcrumb' => $this->_breadcrumb->trail(),
        'log' => array(
          'func' => $this->get_func_options('device')
        ),
        'bodyData' => $data
      )));
    }

    public function response_device_export() {
      $startDate = isset($this->_get['startDate']) && !empty($this->_get['startDate']) ? date('Y-m-d', strtotime($this->_get['startDate'])) : '1970-01-01';
      $endDate = isset($this->_get['endDate']) && !empty($this->_get['endDate']) ? date('Y-m-d', strtotime($this->_get['endDate'])) : date('Y-m-d');
      $listColumn = array();
      $listRows = array();
      $daoParams = array(
        'isCount' => true,
        'timestamp' => array(
          'start' => $startDate,
          'end' => $endDate
        )
      );
      $listColumn['view'] = $this->lang('DEVICE_CHART_X');
      $listRows['view'] = $this->lang('DEVICE_CHART_Y');

      foreach($this->get_device_options() as $options) {
        $total = $this->_mod_dao->get_rows($recordRows, '', array_merge($daoParams, array('device' => $options['id']))) ? $recordRows['total'] : '0';
        $listColumn[$options['id']] = $options['text'];
        $listRows[$options['id']] = $total;
      }

      $listArray = array($listRows);
      $listColumnCount = count($listColumn);
      header('Content-type:text/html; charset=utf-8');
      header('Content-Type: application/octet-stream');
      header("Content-Disposition: attachment; filename=device_export.xls;");
      echo '<HTML xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40">';
      echo '<head><meta http-equiv="content-type" content="application/vnd.ms-excel; charset=UTF-8"></head>';
      echo '<body><table border="1">';
      echo sprintf('<tr><td colspan="%d">%s</td></tr>', $listColumnCount, $this->lang('R_DEVICE'));
      echo sprintf('<tr><td colspan="%d">%s ~ %s</td></tr>', $listColumnCount, $startDate, $endDate);
      echo '<tr>';

      foreach($listColumn as $columnName => $columnLabel) echo "<td>{$columnLabel}</td>";

      echo '</tr>';

      foreach($listArray as $listRows) {
        echo '<tr>';

        foreach($listColumn as $columnName => $columnLabel) {
          echo "<td>{$listRows[$columnName]}</td>";
        }

        echo '</tr>';
      }

      echo '</table></body></html>';
      exit;
    }
    
    public function response_default() {
      $options = $this->get_func_options('count');

      if(empty($options)) CM_Output::json('沒有可以檢視的項目');

      $this->response_count();
    }
  }
?>