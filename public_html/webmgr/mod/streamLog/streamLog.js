;(function($, cm, cmApi, cmLang) {
  var mMain = $(cm.setting.CONTAINER),
      mModName = 'streamLog',
      mCreateMain = function(data) {
        cmApi.templates_create(mModName + '_main', data.tplData, $('#columnCenter', $(cm.setting.CONTAINER)));
      },
      mCreateChart = function(chartJSON) {
        $.jqplot(chartJSON.id, [chartJSON.yData], {
          animate: true,
          animateReplot: true,
          title:{
            text: chartJSON.headline,
            show: true,
            fontFamily: '微軟正黑體',
            fontSize: 17
          },
          seriesDefaults: {
            renderer: $.jqplot.BarRenderer,
            pointLabels: {
              show: true
            },
            showLabel: true
          },
          axesDefaults:{
            min: 0,
            tickOptions: {
              showMark: false
            }
          },
          axes: {
            xaxis: {
              renderer: $.jqplot.CategoryAxisRenderer,
              ticks: chartJSON.xData,
              label: chartJSON.xLabel
            },
            yaxis: {
              label: chartJSON.yLabel,
              min: chartJSON.yMin,
              max: chartJSON.yMax
            }
          }
        });
      };
  
  if(typeof cm.mod[mModName] == 'object') {
    var mLang = cm.mod[mModName][cm.setting.VAR_LANGUAGES],
        mCallback = cm.mod[mModName][cm.setting.VAR_CALLBACK] = {};
    //設定預設的callbcak
    mCallback.count_onLoad = function(data) {
      mCreateMain.call(this, data);
      var jqMain = $('div.content-body', mMain);
      $(cmApi.templates_add(mModName + '_count', data.tplData.bodyData)).appendTo(jqMain);
      $('form', jqMain).cmForm();
      $('select[name="mode"]', jqMain).on('change', function() {
        $(this).parents('form').trigger('submit');
      });
      mCreateChart(data.tplData.bodyData.countChart);
    };
    mCallback.rank_onLoad = function(data) {
      mCreateMain.call(this, data);
      var jqMain = $('div.content-body', mMain);
      $(cmApi.templates_add(mModName + '_rank', data.tplData.bodyData)).appendTo(jqMain);
      $('form', jqMain).cmForm();
    };
    mCallback.connection_onLoad = function(data) {
      mCreateMain.call(this, data);
      var jqMain = $('div.content-body', mMain);
      $(cmApi.templates_add(mModName + '_connection', data.tplData.bodyData)).appendTo(jqMain);
      $('form', jqMain).cmForm();
    };
    mCallback.connectionDetail_onLoad = function(data) {
      mCreateMain.call(this, data);
      var jqMain = $('div.content-body', mMain);
      $(cmApi.templates_add(mModName + '_connectionDetail', data.tplData.bodyData)).appendTo(jqMain);
      $('form', jqMain).cmForm();
    };
    mCallback.country_onLoad = function(data) {
      mCreateMain.call(this, data);
      var jqMain = $('div.content-body', mMain);
      $(cmApi.templates_add(mModName + '_country', data.tplData.bodyData)).appendTo(jqMain);
      $('form', jqMain).cmForm();
      mCreateChart(data.tplData.bodyData.countryChart);
    };
    mCallback.device_onLoad = function(data) {
      mCreateMain.call(this, data);
      var jqMain = $('div.content-body', mMain);
      $(cmApi.templates_add(mModName + '_device', data.tplData.bodyData)).appendTo(jqMain);
      $('form', jqMain).cmForm();
      mCreateChart(data.tplData.bodyData.deviceChart);
    };
    mCallback.default_onLoad = function(data) {
      mCallback.count_onLoad.call(this, data);
    };
  } else {
    cmApi.alert_message(cmLang.ERROR_MODULE_NOT_FOUND);
  }
})(jQuery, window._cm, window._cm.func, window._cm.lang);