;(function($, cm, cmApi, cmLang) {
  var mMain = $(cm.setting.CONTAINER),
      mModName = 'sound';
  
  if(typeof cm.mod[mModName] == 'object') {
    var mLang = cm.mod[mModName][cm.setting.VAR_LANGUAGES],
        mCallback = cm.mod[mModName][cm.setting.VAR_CALLBACK] = {};
    //加入模組預設的callback
    for(var k in cm.tmp.cb_mod) mCallback[k] = cm.tmp.cb_mod[k];

    mCallback.list_onLoad = function(data) {
      var jqMain = $('#columnCenter', $(cm.setting.CONTAINER));
      $('.boxList', cmApi.templates_create('column_center_listSingle', data.tplData, jqMain)).cmAdminList();

      if(typeof data.tplData.topSearch == 'object') {
        var jqTopSearch = cmApi.templates_create('list_topSearch', data.tplData.topSearch, $('div.top-search', jqMain));
      }

      $('form', jqMain).cmForm();
      
      $('div#soundList').on('click', '[data-cmfield-func="file-upload"]', function(e) {
        var uploadFormId = 'upload_sounds',
            params = $(this).data(),
            acceptFileTypes = '/(\\.|\\/)(mp3|aac|wma|m4a|mp4)$/i',
            maxNumberOfFiles = 1,
            maxFileSize = 20480000,
            minFileSize = 10;
        if($('#' + uploadFormId).length > 0) $('#' + uploadFormId).remove(); 
        if($('#' + uploadFormId).length <= 0) {
          //加入這個模組產生的唯一編號
          //_addTmpId(uploadFormId);
          //產生暫存表單
          var html = '<form id="' + uploadFormId + '" class="file-upload-modal-cache" method="post" enctype="multipart/form-data" style="display: none;" action="' + params.cmurl + '">';

          html += '<input type="hidden" name="_request" value="sound" />';
          html += '<input type="hidden" name="_module" value="sound" />';
          html += '<input type="hidden" name="dao" value="Sound">';
          //html += '<input type="hidden" name="column" value="file">';

          html += '<input type="file" name="files" multiple="multiple"/>';
          html += '</form>';

          $(html).prependTo('body').fileupload({
            sequentialUploads: true,
            limitConcurrentUploads: 1,
            dataType: 'json',
            add: function (e, data) {
              var success = true,
                  errorMsg = '',
                  reg = new RegExp(acceptFileTypes, 'i');
              
              for(var k in data.files) {
                var file = data.files[k];
                
                /*if(!(reg.test(file.type) || reg.test(file.name))) {
                  success = false;
                  errorMsg = cmLang.UPLOAD_FILE_ERROR_EXT;
                } else */if(maxFileSize && file.size > maxFileSize) {
                  success = false;
                  errorMsg = $.sprintf(cmLang.UPLOAD_FILE_ERROR_TOO_BIG, cmApi.format_file_size(file.size), cmApi.format_file_size(parseInt(maxFileSize, 10)));
                } else if (typeof file.size === 'number' && file.size < minFileSize) {
                  success = false;
                  errorMsg = $.sprintf(cmLang.UPLOAD_FILE_ERROR_TOO_SMALL, cmApi.format_file_size(file.size), cmApi.format_file_size(parseInt(minFileSize, 10)));
                }

                if(success == false) break;
              }

              if(success == true) {
                var jqProcess = $('[data-cmfield-func=file-process]', 'div#soundList');
                jqProcess.fadeIn();
                $('p', jqProcess).text('5%');
                $('span', jqProcess).width('0%');
                
                var jqXHR = data.submit().error(function (jqXHR, textStatus, errorThrown) {
                  cmApi.alert_message(jqXHR.responseText, 'error');
                  setTimeout(function() {
                    $('[data-cmfield-func="file-process"]', 'div#soundList').fadeOut();
                  }, 1500);
                });        
              } else {
                cmApi.alert_message(errorMsg, 'error');
              }
            },
            progressall: function (e, data) {
              var jqProcess = $('[data-cmfield-func=file-process]', 'div#soundList');
              var processText = parseInt(data.loaded / data.total * 100, 10) + '%';
              $('p', jqProcess).text(processText);
              $('span', jqProcess).width(processText);
            },
            done: function (e, data) {
              var jqProcess = $('[data-cmfield-func=file-process]', 'div#soundList');
              $('p', jqProcess).text('100%');
              $('span', jqProcess).width('100%');

              setTimeout(function() {
                jqProcess.fadeOut();
              }, 1500);

              if(data.result.success == true) {
                cmApi.alert_message(cmLang.UPLOAD_FILE_SUCCESS, 'success');
              } else {
                cmApi.alert_message(data.result.message, 'warring');
              }
            }
          }).bind('fileuploadstop', function (e, data) {
            cmApi.call_server_api({
              history: true,
              url: location.href,
              caller: this
            });
          });
        }

        $('input[type="file"]', $('#' + uploadFormId)).trigger('click');
      });
    };
    mCallback.add_sound_onLoad = function(data) {
      cm.tmp.cb_mod.add_onLoad.call(this, data);
    };
    mCallback.add_sound_save_onLoad = function(data) {
      cm.tmp.cb_mod.add_save_onLoad.call(this, data);
    };
    //設定預設的callbcak
    mCallback.default_onLoad = function(data, textStatus, jqXHR, requestJSON) {
      mCallback.list_onLoad(data, textStatus, jqXHR, requestJSON);
    };
  } else {
    cmApi.alert_message(cmLang.ERROR_MODULE_NOT_FOUND);
  }
})(jQuery, window._cm, window._cm.func, window._cm.lang);