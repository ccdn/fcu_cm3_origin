<?php
  CM_Lang::import('streamServer', array(
    CM_Lang::CODE_TW => array(
      'R_MOD' => '伺服器',
      'R_INTRO' => '預設動作為伺服器列表',
      'R_LIST' => '伺服器列表',
      'R_LIST_INTRO' => '檢視伺服器列表',
      'R_ADD' => '新增伺服器',
      'R_ADD_INTRO' => '新增伺服器表單界面',
      'R_ADD_SAVE_INTRO' => '伺服器資料新增',
      'R_EDIT' => '編輯伺服器',
      'R_EDIT_INTRO' => '編輯伺服器表單界界面',
      'R_EDIT_SAVE_INTRO' => '伺服器資料編輯',
      'R_REMOVE' => '移除伺服器',
      'R_REMOVE_INTRO' => '伺服器資料移除',
      'R_REMOVE_L_INTRO' => '列表上多個伺服器移除',
      'FORM_GROUP_INFO' => '伺服器資訊',
      'ADD_INTRO' => '新增伺服器',
      'EDIT_INTRO' => '修改伺服器資料',
      'EDIT_L_SUCCESS' => '您所選擇的伺服器資料已更新',
      'REMOVE_SUCCESS' => '伺服器已移除',
      'REMOVE_L_SUCCESS' => '您所選擇的伺服器已移除',
      'REMOVE_CONFIRM' => '確定要移除伺服器&nbsp;<b>%s</b>&nbsp;嗎?'
    ),
    CM_Lang::CODE_CN => array(
      'R_MOD' => '伺服器',
      'R_INTRO' => '预设动作为伺服器列表',
      'R_LIST' => '伺服器列表',
      'R_LIST_INTRO' => '检视伺服器列表',
      'R_ADD' => '新增伺服器',
      'R_ADD_INTRO' => '新增伺服器表单界面',
      'R_ADD_SAVE_INTRO' => '伺服器资料新增',
      'R_EDIT' => '编辑伺服器',
      'R_EDIT_INTRO' => '编辑伺服器表单界界面',
      'R_EDIT_SAVE_INTRO' => '伺服器资料编辑',
      'R_REMOVE' => '移除伺服器',
      'R_REMOVE_INTRO' => '伺服器资料移除',
      'R_REMOVE_L_INTRO' => '列表上多个伺服器移除',
      'FORM_GROUP_INFO' => '伺服器资讯',
      'ADD_INTRO' => '新增伺服器',
      'EDIT_INTRO' => '修改伺服器资料',
      'EDIT_L_SUCCESS' => '您所选择的伺服器资料已更新',
      'REMOVE_SUCCESS' => '伺服器已移除',
      'REMOVE_L_SUCCESS' => '您所选择的伺服器已移除',
      'REMOVE_CONFIRM' => '确定要移除伺服器&nbsp;<b>%s</b>&nbsp;吗?'
    )
  ));
?>