<?php
  CM_Lang::import('catalogPicture', array(
    CM_Lang::CODE_TW => array(
      'R_MOD' => '照片分類',
      'R_INTRO' => '預設動作為分類列表',
      'CATALOG_PERMISSION_DENIED' => '您沒有變更這個目錄(編號: %d)的權限'
    ),
    CM_Lang::CODE_CN => array(
      'R_MOD' => '照片分类',
      'R_INTRO' => '预设动作为分类列表',
      'CATALOG_PERMISSION_DENIED' => '您没有变更这个目录(编号: %d)的权限'
    )
  ));
?>