;(function($, cm, cmApi, cmLang) {
  var mMain = $(cm.setting.CONTAINER),
      mModName = 'catalogPicture';
  
  if(typeof cm.mod[mModName] == 'object') {
    var mLang = cm.mod[mModName][cm.setting.VAR_LANGUAGES],
        mCallback = cm.mod[mModName][cm.setting.VAR_CALLBACK] = {};
    //加入樹狀callback
    for(var k in cm.tmp.cb_tree) mCallback[k] = cm.tmp.cb_tree[k];
    //加入模組預設的callback
    for(var k in cm.tmp.cb_mod) mCallback[k] = cm.tmp.cb_mod[k];
    //設定預設的callbcak
    mCallback.default_onLoad = function(data, textStatus, jqXHR, requestJSON) {
      cm.tmp.cb_tree.tree_list_onLoad.call(this, data, textStatus, jqXHR, requestJSON);
    };
  } else {
    cmApi.alert_message(cmLang.ERROR_MODULE_NOT_FOUND);
  }
})(jQuery, window._cm, window._cm.func, window._cm.lang);