<?php
  CM_Lang::import('soundAlbum', array(
    CM_Lang::CODE_TW => array(
      'R_MOD' => '音樂專輯',
      'R_INTRO' => '預設動作為音樂專輯列表',
      'R_LIST' => '音樂專輯列表',
      'R_LIST_INTRO' => '檢視音樂專輯列表',
      'R_ADD' => '新增音樂專輯',
      'R_ADD_INTRO' => '新增音樂專輯表單界面',
      'R_ADD_SAVE_INTRO' => '音樂專輯資料新增',
      'R_EDIT' => '編輯音樂專輯',
      'R_EDIT_INTRO' => '編輯音樂專輯表單界界面',
      'R_EDIT_SAVE_INTRO' => '音樂專輯資料編輯',
      'R_EDIT_L_STATUS_INTRO' => '列表上多個音樂專輯狀態變更',
      'R_REMOVE' => '移除音樂專輯',
      'R_REMOVE_INTRO' => '音樂專輯資料移除',
      'R_REMOVE_L_INTRO' => '列表上多個音樂專輯移除',
      'FORM_GROUP_INFO' => '音樂專輯資訊',
      'ADD_INTRO' => '新增音樂專輯',
      'EDIT_INTRO' => '修改音樂專輯資料',
      'EDIT_L_SUCCESS' => '您所選擇的音樂專輯資料已更新',
      'REMOVE_SUCCESS' => '音樂專輯已移除',
      'REMOVE_L_SUCCESS' => '您所選擇的音樂專輯已移除',
      'REMOVE_CONFIRM' => '移除專輯會<font color="red">連同底下的曲目一起移除</font>，<br />確定要移除音樂專輯&nbsp;<b>%s</b>&nbsp;嗎?',
      'LIST_INTRO' => '檢視音樂專輯列表',
      'LIST_TITLE' => '音樂專輯列表'
    ),
    CM_Lang::CODE_CN => array(
      'R_MOD' => '音乐专辑',
      'R_INTRO' => '预设动作为音乐专辑列表',
      'R_LIST' => '音乐专辑列表',
      'R_LIST_INTRO' => '检视音乐专辑列表',
      'R_ADD' => '新增音乐专辑',
      'R_ADD_INTRO' => '新增音乐专辑表单界面',
      'R_ADD_SAVE_INTRO' => '音乐专辑资料新增',
      'R_EDIT' => '编辑音乐专辑',
      'R_EDIT_INTRO' => '编辑音乐专辑表单界界面',
      'R_EDIT_SAVE_INTRO' => '音乐专辑资料编辑',
      'R_EDIT_L_STATUS_INTRO' => '列表上多个音乐专辑状态变更',
      'R_REMOVE' => '移除音乐专辑',
      'R_REMOVE_INTRO' => '音乐专辑资料移除',
      'R_REMOVE_L_INTRO' => '列表上多个音乐专辑移除',
      'FORM_GROUP_INFO' => '音乐专辑资讯',
      'ADD_INTRO' => '新增音乐专辑',
      'EDIT_INTRO' => '修改音乐专辑资料',
      'EDIT_L_SUCCESS' => '您所选择的音乐专辑资料已更新',
      'REMOVE_SUCCESS' => '音乐专辑已移除',
      'REMOVE_L_SUCCESS' => '您所选择的音乐专辑已移除',
      'REMOVE_CONFIRM' => '移除专辑会<font color="red">连同底下的曲目一起移除</font>，<br />确定要移除音乐专辑&nbsp;<b>%s</b>&nbsp;吗?',
      'LIST_INTRO' => '检视音乐专辑列表',
      'LIST_TITLE' => '音乐专辑列表'
    )
  ));
?>