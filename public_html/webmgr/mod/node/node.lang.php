<?php
  CM_Lang::import('node', array(
    CM_Lang::CODE_TW => array(
      'R_MOD' => '節點管理',
      'R_INTRO' => '預設動作為節點列表',
      'R_LIST' => '節點列表',
      'R_LIST_INTRO' => '檢視節點列表',
      'R_ADD' => '新增節點',
      'R_ADD_INTRO' => '新增節點表單界面',
      'R_ADD_SAVE_INTRO' => '節點資料新增',
      'R_EDIT' => '編輯節點',
      'R_EDIT_INTRO' => '編輯節點表單界界面',
      'R_EDIT_SAVE_INTRO' => '節點資料編輯',
      'R_EDIT_L_STATUS_INTRO' => '列表上多個節點狀態變更',
      'R_REMOVE' => '移除節點',
      'R_REMOVE_INTRO' => '節點資料移除',
      'R_REMOVE_L_INTRO' => '列表上多個節點移除',
      'FORM_GROUP_INFO' => '節點資訊',
      'FORM_GROUP_SETTING' => '節點設定',
      'ADD_INTRO' => '新增管理系統節點',
      'EDIT_INTRO' => '修改管理系統節點資料',
      'EDIT_L_SUCCESS' => '您所選擇的節點資料已更新',
      'REMOVE_SUCCESS' => '節點已移除',
      'REMOVE_L_SUCCESS' => '您所選擇的節點已移除',
      'REMOVE_CONFIRM' => '確定要移除節點&nbsp;<b>%s</b>&nbsp;嗎?',
      'TREE_FUNC_ADD' => '新增',
      'TREE_FUNC_ENABLE' => '啟用',
      'TREE_FUNC_DISABLE' => '停用',
      'TREE_FUNC_EDIT' => '修改',
      'TREE_FUNC_REMOVE' => '移除',
      'TREE_FUNC_OPEN_ALL' => '全部展開',
      'TREE_FUNC_CLOSE_ALL' => '全部摺疊',
      'TREE_FUNC_CHECKED_ENABLE' => '啟用選擇的項目',
      'TREE_FUNC_CHECKED_DISABLE' => '停用選擇的項目',
      'TREE_FUNC_CHECKED_REMOVE' => '移除選擇的項目',
      'LIST_INTRO' => '檢視節點列表',
      'LIST_TITLE' => '節點列表'
    ),
    CM_Lang::CODE_CN => array(
      'R_MOD' => '节点管理',
      'R_INTRO' => '预设动作为节点列表',
      'R_LIST' => '节点列表',
      'R_LIST_INTRO' => '检视节点列表',
      'R_ADD' => '新增节点',
      'R_ADD_INTRO' => '新增节点表单界面',
      'R_ADD_SAVE_INTRO' => '节点资料新增',
      'R_EDIT' => '编辑节点',
      'R_EDIT_INTRO' => '编辑节点表单界界面',
      'R_EDIT_SAVE_INTRO' => '节点资料编辑',
      'R_EDIT_L_STATUS_INTRO' => '列表上多个节点状态变更',
      'R_REMOVE' => '移除节点',
      'R_REMOVE_INTRO' => '节点资料移除',
      'R_REMOVE_L_INTRO' => '列表上多个节点移除',
      'FORM_GROUP_INFO' => '节点资讯',
      'FORM_GROUP_SETTING' => '节点设定',
      'ADD_INTRO' => '新增管理系统节点',
      'EDIT_INTRO' => '修改管理系统节点资料',
      'EDIT_L_SUCCESS' => '您所选择的节点资料已更新',
      'REMOVE_SUCCESS' => '节点已移除',
      'REMOVE_L_SUCCESS' => '您所选择的节点已移除',
      'REMOVE_CONFIRM' => '确定要移除节点&nbsp;<b>%s</b>&nbsp;吗?',
      'TREE_FUNC_ADD' => '新增',
      'TREE_FUNC_ENABLE' => '启用',
      'TREE_FUNC_DISABLE' => '停用',
      'TREE_FUNC_EDIT' => '修改',
      'TREE_FUNC_REMOVE' => '移除',
      'TREE_FUNC_OPEN_ALL' => '全部展开',
      'TREE_FUNC_CLOSE_ALL' => '全部摺叠',
      'TREE_FUNC_CHECKED_ENABLE' => '启用选择的项目',
      'TREE_FUNC_CHECKED_DISABLE' => '停用选择的项目',
      'TREE_FUNC_CHECKED_REMOVE' => '移除选择的项目',
      'LIST_INTRO' => '检视节点列表',
      'LIST_TITLE' => '节点列表'
    )
  ));
?>