<?php
  class modNode extends Admin_Mod_Tree {
    const MODULE_NAME = 'node';
    
    public $_mod_dao;

    public $_tree_mode = false;
    
    static public function lang($key) {
      return CM_Lang::line(self::MODULE_NAME . '.' . $key, '', CM_Conf::get('PATH_F.ADMIN_MOD') . self::MODULE_NAME . '/');
    }

    public function __construct($modName, $requestName = '', $actionName = '') {
      parent::__construct(Admin_Factory::createCMDAO(self::MODULE_NAME), $modName, $requestName, $actionName);
      //設定功能-預設功能
      $this->add_request('default', self::lang('R_MOD'), self::lang('R_INTRO'));
      //設定Breadcrumb
      $this->_breadcrumb->add(self::lang('R_MOD'), $this->get_request('default')->get_link(array(), sprintf('<i class="icofont-th-large"></i> %s', self::lang('R_MOD'))));
      $this->_mod_dao = Admin_Factory::createCMDAO(self::MODULE_NAME);
    }

    public function response_tree_add() {
      $this->tree_response_add();
    }

    public function response_tree_add_save() {
      $this->tree_response_add_save();
    }

    public function response_tree_edit() {
      $this->tree_response_edit();
    }

    public function response_tree_edit_save() {
      $this->tree_response_edit_save();
    }

    public function response_tree_edit_list_status() {
      $this->tree_response_edit_list_status();
    }

    public function response_tree_remove() {
      $this->tree_response_remove();
    }

    public function response_tree_remove_list() {
      $this->tree_response_remove_list();
    }

    public function response_tree_list() {
      $this->tree_respone_list();
    }

    public function response_tree_list_catalog() {
      $this->tree_response_list_catalog();
    }

    public function response_tree_list_sort_save() {
      $this->tree_response_list_sort_save();
    }
    
    public function response_default() {
      $this->tree_respone_list();
    }
  }
?>