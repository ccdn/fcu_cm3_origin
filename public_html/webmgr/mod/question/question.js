;(function($, cm, cmApi, cmLang) {
  var mMain = $(cm.setting.CONTAINER),
      mModName = 'question';
  
  if(typeof cm.mod[mModName] == 'object') {
    var mLang = cm.mod[mModName][cm.setting.VAR_LANGUAGES],
        mCallback = cm.mod[mModName][cm.setting.VAR_CALLBACK] = {};
    //加入目錄callback
    for(var k in cm.tmp.cb_catalog) mCallback[k] = cm.tmp.cb_catalog[k];
    //加入模組預設的callback
    for(var k in cm.tmp.cb_mod) mCallback[k] = cm.tmp.cb_mod[k];
    //設定預設的callbcak
    mCallback.default_onLoad = function(data) {
      mCallback.list_onLoad(data);
    };
  } else {
    cmApi.alert_message(cmLang.ERROR_MODULE_NOT_FOUND);
  }
})(jQuery, window._cm, window._cm.func, window._cm.lang);