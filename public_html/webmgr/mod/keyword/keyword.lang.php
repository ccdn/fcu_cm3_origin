<?php
  CM_Lang::import('keyword', array(
    CM_Lang::CODE_TW => array(
      'R_MOD' => '關鍵字',
      'R_INTRO' => '預設動作為關鍵字列表',
      'R_LIST' => '關鍵字列表',
      'R_LIST_INTRO' => '檢視關鍵字列表',
      'R_ADD' => '新增關鍵字',
      'R_ADD_INTRO' => '新增關鍵字表單界面',
      'R_ADD_SAVE_INTRO' => '關鍵字資料新增',
      'R_EDIT' => '編輯關鍵字',
      'R_EDIT_INTRO' => '編輯關鍵字表單界界面',
      'R_EDIT_SAVE_INTRO' => '關鍵字資料編輯',
      'R_REMOVE' => '移除關鍵字',
      'R_REMOVE_INTRO' => '關鍵字資料移除',
      'R_REMOVE_L_INTRO' => '列表上多個關鍵字移除',
      'FORM_GROUP_INFO' => '關鍵字資訊',
      'ADD_INTRO' => '新增關鍵字',
      'EDIT_INTRO' => '修改關鍵字資料',
      'EDIT_L_SUCCESS' => '您所選擇的關鍵字資料已更新',
      'REMOVE_SUCCESS' => '關鍵字已移除',
      'REMOVE_L_SUCCESS' => '您所選擇的關鍵字已移除',
      'REMOVE_CONFIRM' => '確定要移除關鍵字&nbsp;<b>%s</b>&nbsp;嗎?'
    ),
    CM_Lang::CODE_CN => array(
      'R_MOD' => '关键字',
      'R_INTRO' => '预设动作为关键字列表',
      'R_LIST' => '关键字列表',
      'R_LIST_INTRO' => '检视关键字列表',
      'R_ADD' => '新增关键字',
      'R_ADD_INTRO' => '新增关键字表单界面',
      'R_ADD_SAVE_INTRO' => '关键字资料新增',
      'R_EDIT' => '编辑关键字',
      'R_EDIT_INTRO' => '编辑关键字表单界界面',
      'R_EDIT_SAVE_INTRO' => '关键字资料编辑',
      'R_REMOVE' => '移除关键字',
      'R_REMOVE_INTRO' => '关键字资料移除',
      'R_REMOVE_L_INTRO' => '列表上多个关键字移除',
      'FORM_GROUP_INFO' => '关键字资讯',
      'ADD_INTRO' => '新增关键字',
      'EDIT_INTRO' => '修改关键字资料',
      'EDIT_L_SUCCESS' => '您所选择的关键字资料已更新',
      'REMOVE_SUCCESS' => '关键字已移除',
      'REMOVE_L_SUCCESS' => '您所选择的关键字已移除',
      'REMOVE_CONFIRM' => '确定要移除关键字&nbsp;<b>%s</b>&nbsp;吗?'
    )
  ));
?>