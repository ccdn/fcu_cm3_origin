<?php
  $rootPath = '/' . implode('/', array_filter(array_map(function($element) {
    if(in_array($element, array('webmgr', 'tmpl', 'js'))) return false;

    return $element;
  }, explode('/', '/AppServ/Apache2/htdocs/public_html/'))));//本機
  //}, explode('/', __DIR__))));//測試機
  //設定值Class載入
  require($rootPath . '/lib/class/CM/Conf.php');
  //設定值載入
  require($rootPath . '/lib/inc/conf.php');
  //初始化載入
  require(CM_Conf::get('PATH_F.LIB') . 'inc/init.php');
  //呼叫控制器
  require(CM_Conf::get('PATH_F.ROOT') . 'webmgr/inc/web_init.php');
  
  //Javascript設定 ---------- 開始
  CM_Conf::set('JAVASCRIPT.MODE', CM_Conf::get('DEF.DEBUG_WEBMGR'));
  CM_Conf::set('JAVASCRIPT.DEF_REQUEST', Admin::DEF_REQUEST);
  CM_Conf::set('JAVASCRIPT.CONTAINER', '#mainBody');
  CM_Conf::set('JAVASCRIPT.CURRENT_LANG', CM_Lang::get_current_code());
  CM_Conf::set('JAVASCRIPT.VAR_RECORD_ID', Admin::VAR_RECORD_ID);
  CM_Conf::set('JAVASCRIPT.VAR_GLOBAL_LANG', Admin::VAR_GLOBAL_LANG);
  CM_Conf::set('JAVASCRIPT.VAR_MODULE', Admin::VAR_MODULE);
  CM_Conf::set('JAVASCRIPT.VAR_REQUEST', Admin::VAR_REQUEST);
  CM_Conf::set('JAVASCRIPT.VAR_ACTION', Admin::VAR_ACTION);
  CM_Conf::set('JAVASCRIPT.VAR_CONFIGURE', Admin::VAR_CONFIGURE);
  CM_Conf::set('JAVASCRIPT.VAR_TEMPLATES', Admin::VAR_TEMPLATES);
  CM_Conf::set('JAVASCRIPT.VAR_LANGUAGES', Admin::VAR_LANGUAGES);
  CM_Conf::set('JAVASCRIPT.VAR_JAVASCRIPT', Admin::VAR_JAVASCRIPT);
  CM_Conf::set('JAVASCRIPT.VAR_CALLBACK', '_callback');

  CM_Conf::set('PATH_W.HTTP_SERVER', CM_Conf::get('HTTP.SERVER'));
  
  header('Content-Type: application/javascript; charset=utf-8');
  //Javascript設定 ---------- 結束
  echo 'window._cm = {};',
       'window._cm.tmpl = {};',
       'window._cm.tmp = {};',
       'window._cm.path = {};',
       'window._cm.setting = {};',
       'window._cm.lang = {};',
       'window._cm.func = {};',
       'window._cm.mod = {};';
  //預設版型
  foreach(array_merge(
    CM_Template::get_path_tpl(CM_Conf::get('PATH_F.ADMIN_VIEW'))
  ) as $key => $value) {
    echo "window._cm.tmpl.{$key} = function() {/* {$value} */}.toString();\n";
  }
  
  foreach(CM_Lang::export('Javascript') as $key => $value) echo "window._cm.lang.{$key} = '{$value}';\n";
  
  foreach(CM_Conf::export('PATH_W') as $key => $value) echo "window._cm.path.{$key} = '{$value}';\n";
  
  foreach(CM_Conf::export('JAVASCRIPT') as $key => $value) echo "window._cm.setting.{$key} = '{$value}';\n";
  //上傳檔案類型設定
  echo "window._cm.setting.UPLOAD_FILE_CATALOG = " . json_encode(array()) . ";\n";
?>