<?php
  return array(
    'id' => 'email',
    'name' => 'Mail設定',
    'desc' => '設定程式寄送信件時候的設定值',
    'column' => array(
      'smtp' => array(
        'name' => 'SMTP送信',
        'desc' => '是否要啓用SMTP送信<br />Gmail和Amazon SES請設定: <font color="green">開啓SMTP</font>',
        'column' => array(
          'f_type' => 'select',
          'f_class' => 'input-large',
          'f_options' => array(
            array('id' => 'true', 'text' => '開啓SMTP'),
            array('id' => 'false', 'text' => '不使用SMTP')
          )
        )
      ),
      'smtp_host' => array(
        'name' => 'SMTP主機',
        'desc' => 'SMTP主機的ip位置或是網址<br />Gmail請設定: <font color="green">smtp.gmail.com</font><br />Amazon SES請設定: <font color="green">email-smtp.us-east-1.amazonaws.com</font>'
      ),
      'smtp_auth' => array(
        'name' => 'SMTP驗證',
        'desc' => 'SMTP是否需要驗證<br />Gmail和Amazon SES請設定: <font color="green">需要</font>',
        'column' => array(
          'f_type' => 'select',
          'f_class' => 'input-small',
          'f_options' => array(
            array('id' => 'true', 'text' => '需要'),
            array('id' => 'false', 'text' => '不需要')
          )
        )
      ),
      'smtp_secure' => array(
        'name' => 'SMTP驗證方式',
        'desc' => 'SMTP的驗證方式設定<br />Gmail請設定: <font color="green">SSL</font><br />Amazon SES請設定: <font color="green">TLS</font>',
        'column' => array(
          'f_type' => 'select',
          'f_class' => 'input-large',
          'f_options' => array(
            array('id' => '', 'text' => '無'),
            array('id' => 'ssl', 'text' => 'SSL'),
            array('id' => 'tls', 'text' => 'TLS'),
          )
        )
      ),
      'smtp_port' => array(
        'name' => 'SMTP通訊埠',
        'desc' => 'SMTP的通訊埠Port設定<br />Gmail請設定: <font color="green">465</font><br />Amazon SES請設定: <font color="green">587</font>',
        'column' => array(
          'f_class' => 'input-small',
          'validateType' => 'digits'
        )
      ),
      'smtp_username' => array(
        'name' => 'SMTP帳號',
        'desc' => '登入SMTP用的帳號',
        'column' => array(
          'f_class' => 'input-xxlarge'
        )
      ),
      'smtp_password' => array(
        'name' => 'SMTP密碼',
        'desc' => '登入SMTP用的密碼',
        'column' => array(
          'f_class' => 'input-xxlarge'
        )
      ),
      'form_name' => array(
        'name' => '寄件人名稱',
        'desc' => '信件內的寄件人名稱'
      ),
      'form_mail' => array(
        'name' => '寄件人Mail',
        'desc' => '信件內的寄件人Mail',
        'column' => array(
          'validateType' => 'email'
        )
      ),
      'reply' => array(
        'name' => '開啓郵件回覆',
        'desc' => '是否在信件中設定郵件回覆',
        'column' => array(
          'f_type' => 'select',
          'f_class' => 'input-small',
          'f_options' => array(
            array('id' => 'true', 'text' => '開啓'),
            array('id' => 'false', 'text' => '不開啓')
          )
        )
      ),
      'reply_name' => array(
        'name' => '回覆名稱',
        'desc' => '回復信件的名稱'
      ),
      'reply_mail' => array(
        'name' => '回覆Mail',
        'desc' => '回復信件的Mail',
        'column' => array(
          'validateType' => 'email'
        )
      )
    ),
    'groups' => array(
      'smtp' => array(
        'name' => 'SMTP設定',
        'column' => array('smtp', 'smtp_host', 'smtp_auth', 'smtp_secure', 'smtp_port', 'smtp_username', 'smtp_password')
      ),
      'form' => array(
        'name' => '寄件設定',
        'column' => array('form_name', 'form_mail')
      ),
      'reply' => array(
        'name' => '回覆設定',
        'column' => array('reply', 'reply_name', 'reply_mail')
      )
    ),
    'allowcolumn' => array('form_name', 'form_mail', 'reply', 'reply_name', 'reply_mail'),
    'allow' => true
  );
?>