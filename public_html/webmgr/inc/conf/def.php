<?php
  return array(
    'id' => 'def',
    'name' => '系統設定',
    'desc' => '設定一些常用設定值',
    'column' => array(
      'admin_log' => array(
        'name' => '操作記錄',
        'desc' => '設定是否開啓後台管理系統操作記錄',
        'column' => array(
          'f_type' => 'select',
          'f_class' => 'input-small',
          'f_options' => array(
            array('id' => 'true', 'text' => '開啓'),
            array('id' => 'false', 'text' => '不使用')
          )
        )
      ),
      'debug_website' => array(
        'name' => '網站模式',
        'desc' => '設定網站的模式',
        'column' => array(
          'f_type' => 'select',
          'f_class' => 'input-xxlarge',
          'f_options' => array(
            array('id' => 'normal', 'text' => '正常模式'),
            array('id' => 'debug_server', 'text' => '伺服端除錯模式，只會顯示伺服端的錯誤訊息')
          )
        )
      ),
      'debug_webmgr' => array(
        'name' => '管理系統模式',
        'desc' => '設定管理系統的模式',
        'column' => array(
          'f_type' => 'select',
          'f_class' => 'input-xxlarge',
          'f_options' => array(
            array('id' => 'normal', 'text' => '正常模式'),
            array('id' => 'debug', 'text' => '除錯模式，JS和系統均會顯示錯誤訊息'),
            array('id' => 'debug_js', 'text' => 'JS除錯模式，只會顯示Javascript錯誤訊息'),
            array('id' => 'debug_server', 'text' => '伺服端除錯模式，只會顯示伺服端的錯誤訊息')
          )
        )
      ),
      'time_zone' => array(
        'name' => '系統時區',
        'desc' => '設定系統的時區，php 5.3以上必須設定',
        'column' => array(
          'f_type' => 'select',
          'f_class' => 'input-large',
          'f_options' => array(
            array('id' => 'Asia/Taipei', 'text' => 'Asia/Taipei')
          )
        )
      ),
      'session_life' => array(
        'name' => 'Session存活時間',
        'desc' => '設定Session多久以後會失效(單位：秒)',
        'column' => array(
          'validateType' => 'digits'
        )
      ),
      'lang_admin_def' => array(
        'name' => '管理系統語言(界面)',
        'desc' => '預設的管理系統界面的語言',
        'column' => array(
          'f_type' => 'select',
          'f_class' => 'input-large',
          'f_options' => CM_Lang::get_options()
        )
      ),
      'lang_data_admin_def' => array(
        'name' => '管理系統語言(資料)',
        'desc' => '預設的管理系統資料的語言',
        'column' => array(
          'f_type' => 'select',
          'f_class' => 'input-large',
          'f_options' => CM_Lang::get_options(true)
        )
      ),
      'service_mail' => array(
        'name' => '客服信箱',
        'desc' => '設定客服信箱(接收用)',
        'column' => array(
          'validateType' => 'email'
        )
      ),
      'shortcut_count' => array(
        'name' => '捷徑數量',
        'desc' => '顯示在快捷列上面的捷徑數量',
        'column' => array(
          'validateType' => 'digits'
        )
      ),
      'sitemode_type' => array(
        'name' => '模式',
        'desc' => '前台網站運作模式',
        'column' => array(
          'f_type' => 'radio',
          'f_options' => array(
            array('id' => 'normal', 'text' => '正常'),
            array('id' => 'auth', 'text' => '需要驗證'),
            array('id' => 'off', 'text' => '關閉')
          )
        )
      ),
      'sitemode_auth_text' => array(
        'name' => '驗證模式訊息',
        'desc' => '前台網站運作模式顯示的訊息',
        'column' => array(
          'f_type' => 'textarea',
          'rows' => 5
        )
      ),
      'sitemode_auth_account' => array(
        'name' => '驗證模式帳號',
        'desc' => '前台網站運作模式為驗證時的帳號'
      ),
      'sitemode_auth_password' => array(
        'name' => '驗證模式密碼',
        'desc' => '前台網站運作模式為驗證時的密碼'
      ),
      'sitemode_off_text' => array(
        'name' => '關閉訊息',
        'desc' => '模式選擇關閉後前台顯示的文字',
        'column' => array(
          'f_type' => 'textarea',
          'rows' => 5
        )
      ),
      'recaptcha_public_key' => array(
        'name' => '圖片驗證Public Key',
        'desc' => '從Google申請的圖片Public Key',
        'column' => array(
          'f_class' => 'input-xxlarge'
        )
      ),
      'recaptcha_private_key' => array(
        'name' => '圖片驗證Private Key',
        'desc' => '從Google申請的圖片Private Key',
        'column' => array(
          'f_class' => 'input-xxlarge'
        )
      ),
      'license_key' => array(
        'name' => 'License Key',
        'desc' => '授權Key',
        'column' => array(
          'f_class' => 'input-xxlarge'
        )
      ),
      'site_lang_mode' => array(
        'name' => '前台多語系模式',
        'desc' => '前台多語系模式，開啟後會在網址上加入該語系的id<br />多語系模式<font color="blue">關閉</font>網址：http://www.exp.com/news/<br />多語系模式<font color="blue">開啟</font>網址：http://www.exp.com/<font color="red">zh-tw/</font>news/',
        'column' => array(
          'f_type' => 'select',
          'f_class' => 'input-large',
          'f_options' => array(
            array('id' => 'true', 'text' => '開啟'),
            array('id' => 'false', 'text' => '關閉')
          )
        )
      ),
      'site_lang_default' => array(
        'name' => '前台預設語言',
        'desc' => '預設前台顯示的語言',
        'column' => array(
          'f_type' => 'select',
          'f_class' => 'input-large',
          'f_options' => CM_Lang::get_options(true)
        )
      ),
      'site_session_db' => array(
        'name' => 'Session寫入資料庫',
        'desc' => '將前台的Session寫入資料庫(會增加資料庫的負擔，除特定狀況請關閉此選項)',
        'column' => array(
          'f_type' => 'select',
          'f_class' => 'input-large',
          'f_options' => array(
            array('id' => 'true', 'text' => '開啟'),
            array('id' => 'false', 'text' => '關閉')
          )
        )
      ),
      'webmgr_session_db' => array(
        'name' => 'Session寫入資料庫',
        'desc' => '將管理系統的Session寫入資料庫(會增加資料庫的負擔，除特定狀況請關閉此選項)',
        'column' => array(
          'f_type' => 'select',
          'f_class' => 'input-large',
          'f_options' => array(
            array('id' => 'true', 'text' => '開啟'),
            array('id' => 'false', 'text' => '關閉')
          )
        )
      )
    ),
    'groups' => array(
      'normal' => array(
        'name' => '一般設定',
        'column' => array('service_mail', 'license_key', 'time_zone', 'session_life', 'recaptcha_public_key', 'recaptcha_private_key')
      ),
      'website' => array(
        'name' => '前台設定',
        'column' => array('site_lang_mode', 'site_lang_default', 'site_session_db', 'debug_website', 'sitemode_type', 'sitemode_auth_text', 'sitemode_auth_account', 'sitemode_auth_password', 'sitemode_off_text')
      ),
      'webmgr' => array(
        'name' => '管理系統設定',
        'column' => array('lang_admin_def', 'lang_data_admin_def', 'admin_log', 'shortcut_count', 'webmgr_session_db', 'debug_webmgr')
      )
    ),
    'allowcolumn' => array('admin_log', 'lang_admin_def', 'lang_data_admin_def', 'service_mail', 'shortcut_count', 'sitemode_type', 'sitemode_auth_text', 'sitemode_auth_account', 'sitemode_auth_password', 'sitemode_off_text', 'license_key', 'site_lang_mode', 'site_lang_default'),
    'allow' => true
  );
?>