<?php
  return array(
    'id' => 'upload',
    'name' => '上傳設定',
    'desc' => '設定檔案上傳全域設定(非絕對，欄位設定會覆蓋這些設定)',
    'column' => array(
      'count' => array(
        'name' => '檔案上傳數量',
        'desc' => '預設上傳檔案的數量',
        'column' => array(
          'f_class' => 'input-small',
          'validateType' => 'digits'
        )
      ),
      'auto' => array(
        'name' => '自動開始上傳',
        'desc' => '預設選擇檔案後是否直接上傳',
        'column' => array(
          'f_type' => 'select',
          'f_class' => 'input-small',
          'f_options' => array(
            array('id' => 'true', 'text' => '開啓'),
            array('id' => 'false', 'text' => '不開啓')
          )
        )
      ),
      'max_size' => array(
        'name' => '檔案最大限制',
        'desc' => '預設上傳檔案的最大限制，單位: bytes',
        'column' => array(
          'validateType' => 'digits'
        )
      ),
      'min_size' => array(
        'name' => '檔案最小限制',
        'desc' => '預設上傳檔案的最小限制，單位: bytes',
        'column' => array(
          'validateType' => 'digits'
        )
      ),
      'allow_ext' => array(
        'name' => '允許的副檔名',
        'desc' => '預設上傳檔案的副檔名',
        'column' => array(
          'f_attr' => array(
            'multiple' => 'multiple',
            'style' => 'width: 400px'
          ),
          'f_type' => 'select_mg',
          'f_options' => array(
            array('id' => 'img_files', 'text' => '圖片檔案', 'opt' => array(
              array('id' => 'gif', 'text' => 'gif'),
              array('id' => 'jpg', 'text' => 'jpg'),
              array('id' => 'jpeg', 'text' => 'jpeg'),
              array('id' => 'png', 'text' => 'png')
            )),
            array('id' => 'office_files', 'text' => 'Office檔案', 'opt' => array(
              array('id' => 'doc', 'text' => 'doc'),
              array('id' => 'docx', 'text' => 'docx'),
              array('id' => 'xls', 'text' => 'xls'),
              array('id' => 'xlsx', 'text' => 'xlsx'),
              array('id' => 'ppt', 'text' => 'ppt'),
              array('id' => 'pptx', 'text' => 'pptx')
            )),
            array('id' => 'video_files', 'text' => '影片檔案', 'opt' => array(
              array('id' => 'flv', 'text' => 'flv'),
              array('id' => 'mp4', 'text' => 'mp4'),
			  array('id' => 'mpg', 'text' => 'mpg'),
			  array('id' => 'wmv', 'text' => 'wmv'),
			  array('id' => 'mov', 'text' => 'mov'),
			  array('id' => 'avi', 'text' => 'avi'),
			  array('id' => 'vob', 'text' => 'vob')
            )),
            array('id' => 'sound_files', 'text' => '音樂檔案', 'opt' => array(
              array('id' => 'mp3', 'text' => 'mp3')
            )),
            array('id' => 'other_files', 'text' => '其他檔案', 'opt' => array(
              array('id' => 'txt', 'text' => 'txt'),
              array('id' => 'pdf', 'text' => 'pdf'),
              array('id' => 'swf', 'text' => 'swf')
            ))
          ),
          'fs_params' => array(
            'placeholder' => '請選擇允許的副檔名'
          )
        )
      )
    ),
    'groups' => array(
      'normal' => array(
        'name' => '一般設定',
        'column' => array('max_size', 'min_size', 'allow_ext')
      ),
      'multiupload' => array(
        'name' => '多檔案上傳設定',
        'column' => array('count', 'auto')
      )
    ),
    'allowcolumn' => array(),
    'allow' => false
  );
?>