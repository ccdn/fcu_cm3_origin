<?php
  $rootPath = dirname(dirname(__DIR__));
  //設定值Class載入
  require($rootPath . '/lib/class/CM/Conf.php');
  //設定值載入
  require($rootPath . '/lib/inc/conf.php');
  //初始化載入
  require(CM_Conf::get('PATH_F.LIB') . 'inc/init.php');
  //呼叫控制器
  require(CM_Conf::get('PATH_F.ROOT') . 'webmgr/inc/web_init.php');

  if(CM_DB::get_sql_array($recordArray, "SELECT `iLogRm_seq`, `cClient_ip` FROM `stream_log` WHERE `cClient_Country` = '' ORDER BY `iLogRm_seq`")) {
    $successCount = 0;
    $cache = array();

    foreach($recordArray as $recordRows) {
      if(!isset($cache[$recordRows['cClient_ip']])) {
        $cache[$recordRows['cClient_ip']] = Admin::get_country_by_ip($recordRows['cClient_ip']);
      }

      CM_DB::update('stream_log', array(
        'cClient_Country' => $cache[$recordRows['cClient_ip']]
      ), "`iLogRm_seq` = {$recordRows['iLogRm_seq']}");
      $successCount += 1;
    }

    echo "已完成轉換{$successCount}筆StreamLog";
  } else {
    echo "目前沒有需要轉換的StreamLog";
  }
?>