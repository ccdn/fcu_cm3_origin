<?php
  $rootPath = dirname(dirname(__DIR__));
  //設定值Class載入
  require($rootPath . '/lib/class/CM/Conf.php');
  //設定值載入
  require($rootPath . '/lib/inc/conf.php');
  //初始化載入
  require(CM_Conf::get('PATH_F.LIB') . 'inc/init.php');
  //呼叫控制器
  require(CM_Conf::get('PATH_F.ROOT') . 'webmgr/inc/web_init.php');
  
  $gPost = Admin::filter_input($_POST, false);
  //$gPost = Admin::filter_input($_GET, false);

  foreach(array('videoName', 'videoCount', 'videoResult', 'videoLength', 'videoFileSize', 'videoImgName') as $columnName) {
    if(!isset($gPost[$columnName]) || empty($gPost[$columnName])) {
      add_log("找不到指定的參數-{$columnName}");
      CM_Output::json("找不到指定的參數: {$columnName}");
    }
  }

  $fileDAO = Admin_Factory::createCMDAO('file');
  $videoName = explode('.', $gPost['videoName']);
  
  if(!$fileDAO->get_rows($fileRows, Admin_DAO_File::GROUP_ADD, array('recordId' => 'video/' . $gPost['videoName']))) {
    add_log("找不到指定檔案", 'video/' . $gPost['videoName']);
    CM_Output::json("找不到指定檔案");
  } elseif(empty($fileRows['dao'])) {
    add_log("找不到指定DAO", $fileRows['dao']);
    CM_Output::json("找不到指定DAO: {$fileRows['dao']}");
  }
  $daoNameArray = explode('_', $fileRows['dao']);
  
  if(count($daoNameArray) > 1) {
    $daoName = $daoNameArray[0] . '_' . ucfirst($daoNameArray[1]);
  } else {
    $daoName = $fileRows['dao'];
  }
  
  $dao = Admin_Factory::createCMDAO($daoName);
  $daoParams = array(
    'file' => 'video/' . $gPost['videoName']
  );
  $thumbnailPath = CM_Conf::get('PATH_F.UPLOAD') . 'video_thumbnail/';

  if(!in_array($gPost['videoResult'], array(2, 3))) {
    add_log('videoResult參數錯誤-必須是2(成功)或是3(失敗)');
    CM_Output::json("videoResult參數錯誤: 必須是2(成功)或是3(失敗)"); 
  } elseif(!$dao->get_rows($recordRows, 'list', $daoParams)) {
    add_log("DAO找不到指定的資料", $gPost['videoName']);
    CM_Output::json("DAO找不到指定的資料: {$gPost['videoName']}");
  } else {
    if(is_file($thumbnailPath . $gPost['videoImgName'])) {
      //$fileDAO = Admin_Factory::createCMDAO('file');
      $file = 'video_thumbnail/' . $gPost['videoImgName'];

      if(!$fileDAO->insert_rows($rMessage, $rReturn, array(
        'dao' => $fileRows['dao'],
        Admin_DAO_File::COLUMN_HEADLINE => $recordRows[Admin_DAO_Video::COLUMN_HEADLINE],
        'desc' => '',
        'size' => filesize($thumbnailPath . $gPost['videoImgName']),
        'file' => $file,
        'link' => '0'
      ), Admin_DAO_File::GROUP_ADD)) {
        add_log($rMessage);
        CM_Output::json($rMessage);
      }

      $gPost['videoImgName'] = $file;
    } else {
      $gPost['videoImgName'] = '';
    }

    if($fileDAO->get_rows_byId($fileRows, '', 'video/' . $gPost['videoName'])) {
      if($gPost['videoName'] != $videoName[0] . '.mp4') {
        if(!$fileDAO->insert_rows($rMessage, $rReturn, array(
          'dao' => $fileRows['dao'],
          Admin_DAO_File::COLUMN_HEADLINE => $fileRows[Admin_DAO_File::COLUMN_HEADLINE],
          'desc' => $fileRows['desc'],
          'size' => $fileRows['size'],
          'file' => 'video/' . $videoName[0] . '.mp4',   //音檔
          'link' => '0'
        ), Admin_DAO_File::GROUP_PROCESS))  {
          add_log($rMessage);
          CM_Output::json($rMessage);
        }
      }
    }
    
    if(!$dao->update_rows($rMessage, $rReturn, array(
      'file' => 'video/' . $videoName[0] . '.mp4',
      'file_sound' => 'video/' . $videoName[0] . '.mp4',
      'file_img' => $gPost['videoImgName'],
      'file_size' => $gPost['videoFileSize'],
      'video_length' => $gPost['videoLength'],
      'process_ok' => $gPost['videoResult'],
      'process_count' => (int)$gPost['videoCount']
    ) , $recordRows['id'], 'process')) {
      add_log($rMessage);
      CM_Output::json($rMessage);
    } else {
      //add_log('已經儲存多媒體檔案資料' . $recordRows['id'], 'video/' . $gPost['videoName']);
      CM_Output::json('已經儲存多媒體檔案資料' . $recordRows['id'], true);
    }
  }
  
  function add_log($message, $content = '') {
    $path = CM_Conf::get('PATH_F.ROOT') . 'new_ContentLog/';
    $counterPath = $path;
  
    if(!is_dir($counterPath) && !mkdir($counterPath)) {
      trigger_error("路徑{$counterPath}無法寫入", E_USER_NOTICE);
    } else {
      $nowDate = date('Y-m-d');
      //$nowDate = '2014-07-27';
      
      $fileName = time() . substr(microtime(), 2, 5) . '_' . $nowDate . '_' . $message . '.log';
      @file_put_contents($counterPath . $fileName, $content);
      //chmod($counterPath . $filename, 0777);
    }
  }
?>