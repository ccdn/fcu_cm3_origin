<?php
  class Admin_DAO extends CM_DAO {
    protected function init_column($columnName, $tableName, $columnParams, $default = '') {
      $this->_column[$columnName] = Admin_Factory::createCMColumn($columnName, array_merge($columnParams, array(
        'table' => $tableName,
        'dao' => $this->_dao_conf['daoName']
      )), $default);
      $this->set_column($columnName, array('name_full' => $this->get_column($columnName)->get_full_name($this->_table_alias)));
    }

    protected function process_rows(&$recordRows, $params, $action = 'insert', $currentRows = array()) {
      parent::process_rows($recordRows, $params, $action);
    }

    protected function process_after($recordRows, $params, $action = 'insert', $currentRows = array(), $currentLangArray = array()) {
      //處理檔案上傳
      if(!in_array($this->_dao_conf['daoName'], array('File', 'File_Info'))) {
        //在這兩個DAO不執行，避免造成無窮回圈
        $fileColumn = $this->get_columns_by_type(array('file', 'file_t'), $params['groupName']);

        if(!empty($fileColumn)) {
          $dao = null;
          $daoInfo = null;

          foreach($fileColumn as $column) {
            //判斷是否有語系
            $columnIsLang = $this->_dao_conf['tableLang'] == true && $column->table == $this->_dao_conf['tableLangName'] ? true : false;

            if(in_array($action, array('insert', 'update'))) {
              if(
                !isset($recordRows[$column->name]) || empty($recordRows[$column->name]) ||
                !isset($recordRows[$this->_dao_conf['tablePKey']]) || empty($recordRows[$this->_dao_conf['tablePKey']])
              ) continue;

              if(is_null($dao)) $dao = Admin_Factory::createCMDAO('file');

              if(is_null($daoInfo)) $daoInfo = Admin_Factory::createCMDAO('file_Info');

              $file = explode(',', $recordRows[$column->name]);
              $daoParams = array(
                'dao' => $this->_dao_conf['daoName'],
                'rId' => $recordRows[$this->_dao_conf['tablePKey']],
                'column' => $column->name
              );
              //要寫入的資料
              $data = array(
                'record_id' => $recordRows[$this->_dao_conf['tablePKey']],
                'dao' => $this->_dao_conf['daoName'],
                'column' => $column->name,
                'lang' => $columnIsLang == true ? $recordRows[$this->_dao_conf['tableLangColumn']] : '0',
                'desc' => '',
                'note' => ''
              );

              foreach($file as $fileId) {
                try {
                  CM_DB::transaction_start();
                  $dao->update_rows($rMessage, $rReturn, array(), $fileId, Admin_DAO_File::GROUP_LINK, array(
                    'columnCallback' => function($inputRows, $dataRows) {
                      return array_merge($inputRows, array('link' => $dataRows['link'] + 1));
                    },
                    'checkCallable' => function(&$rMessage, $inputRows, $dataRows) use($daoInfo, $daoParams, $data, $fileId) {
                      if($daoInfo->get_rows_byId($infoRows, Admin_DAO_File_Info::GROUP_LIST, $fileId, $daoParams)) {
                        $rMessage = '已經有資料存在';
                        return false;
                      } else {
                        $success = $daoInfo->insert_rows($rMessage, $rReturn, array_merge($data, array(
                          Admin_DAO_File_Info::COLUMN_RECORD_ID => $fileId,
                          Admin_DAO_File_Info::COLUMN_HEADLINE => $dataRows[Admin_DAO_File::COLUMN_HEADLINE],
                          'desc' => $dataRows['desc']
                        )), Admin_DAO_File_Info::GROUP_ADD);
                        return $success;
                      }
                    }
                  ));  
                  CM_DB::transaction_end();
                } catch(PDOException $e) {
                  CM_DB::transaction_rollback();
                }
              }
            } elseif($action == 'delete') {
              if(!isset($currentRows[$this->_dao_conf['tablePKey']]) || empty($currentRows[$this->_dao_conf['tablePKey']])) continue;

              if($columnIsLang == true) {
                $file = array();
                $langColumnName = $this->_dao_conf['tableLangColumn'];
                //多語系欄位
                foreach(array_filter(array_map(function($element) use($column, $langColumnName) {
                  if(!isset($element[$column->name]) || empty($element[$column->name])) return false;

                  return array_map(function($subElement) use($element, $langColumnName) {
                    return array('id' => $subElement, 'lang' => $element[$langColumnName]);
                  }, explode(',', $element[$column->name]));
                }, $currentLangArray)) as $currentFile) $file = array_merge($file, $currentFile);

                if(empty($file)) continue;
              } else {
                //無語系欄位
                if(!isset($currentRows[$column->name]) || empty($currentRows[$column->name])) continue;

                $file = array_map(function($element) {
                  return array('id' => $element);
                }, explode(',', $currentRows[$column->name]));
              }

              if(is_null($dao)) $dao = Admin_Factory::createCMDAO('file');

              if(is_null($daoInfo)) $daoInfo = Admin_Factory::createCMDAO('file_Info');

              $daoParams = array(
                'rId' => $currentRows[$this->_dao_conf['tablePKey']],
                'dao' => $this->_dao_conf['daoName'],
                'column' => $column->name
              );

              foreach($file as $fileRows) {
                $fileDaoParams = $daoParams;
                //語系加上條件
                if($columnIsLang == true && isset($fileRows['lang']) && !empty($fileRows['lang'])) $fileDaoParams['lang'] = $fileRows['lang'];

                try {
                  CM_DB::transaction_start();

                  if($daoInfo->delete_rows($rMessage, $rReturn, $fileRows['id'], $fileDaoParams)) {
                    //移除完成後
                    $isDelete = false;
                    $dao->update_rows($rfMessage, $rfReturn, array(), $fileRows['id'], Admin_DAO_File::GROUP_LINK, array(
                      'columnCallback' => function($inputRows, $dataRows) use(&$isDelete) {
                        $link = $dataRows['link'] - 1;
                        //如果連結數為0，移除原本的檔案庫的檔案
                        if($link <= 0) $isDelete = true;

                        return array_merge($inputRows, array('link' => $link));
                      }
                    ));

                    if($isDelete == true) $dao->delete_rows($rfMessage, $rfReturn, $fileRows['id']);
                  }
                  
                  CM_DB::transaction_end();
                } catch(PDOException $e) {
                  CM_DB::transaction_rollback();

                  echo $e->getMessage();
                }
              }
            }
          }
        }
      }
    }

    public function get_array(&$returnArray, $groupName, $params = array()) {
      $success = parent::get_array($returnArray, $groupName, $params);
      //自動抓檔案資訊
      if(isset($params['fileCallback']) && $params['fileCallback'] == true) {
        $fileColumn = $this->get_columns_by_type(array('file'), $groupName);

        if(!empty($fileColumn)) {
          $dao = null;
          $daoInfo = null;
          $lang = isset($params['lang']) ? $params['lang'] : CM_Lang::get_current_id(true);

          foreach($returnArray as &$returnRows) {
            $daoParams = array(
              'rId' => $returnRows[$this->_dao_conf['tablePKey']],
              'dao' => $this->_dao_conf['daoName']
            );

            foreach($fileColumn as $column) {
              if(!isset($returnRows[$column->name]) || empty($returnRows[$column->name])) continue;

              if(is_null($dao)) $dao = Admin_Factory::createCMDAO('file');

              if(is_null($daoInfo)) $daoInfo = Admin_Factory::createCMDAO('file_Info');

              $columnIsLang = $this->_dao_conf['tableLang'] == true && $column->table == $this->_dao_conf['tableLangName'] ? true : false;

              if($dao->get_array($fileArray, Admin_DAO_File::GROUP_CALLBACK, array('recordIds' => explode(',', $returnRows[$column->name])))) {
                $returnRows[$column->name] = array();

                foreach($fileArray as &$fileRows) {
                  $returnRows[$column->name][] = $fileRows[Admin_DAO_File::COLUMN_RECORD_ID];

                  if($daoInfo->get_rows($fileInfoRows, Admin_DAO_File_Info::GROUP_CALLBACK, array_merge($daoParams, array(
                    'recordId' => $fileRows[Admin_DAO_File::COLUMN_RECORD_ID],
                    'column' => $column->name,
                    'lang' => $columnIsLang == true ? $lang : '0'
                  )))) {
                    $fileRows[Admin_DAO_File::COLUMN_HEADLINE] = $fileInfoRows[Admin_DAO_File_Info::COLUMN_HEADLINE];
                    $fileRows['desc'] = $fileInfoRows['desc'];
                  }
                }

                $returnRows["{$column->name}_fattr"] = $fileArray;
              }
            }
          }
        }
      }

      return $success;
    }
  }
?>