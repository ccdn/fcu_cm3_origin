<?php
  class Admin_Dgfactor {
    static public function to_select_item_rows($itemRows) {
      $return = array();
      $return['lang'] = array(
        'SELECT_CONTENTS_PREVIEW_BTN' => CM_Lang::line('Javascript.UPLOAD_FILE_PREVIEW_BTN'),
        'SELECT_CONTENTS_REMOVE_BTN' => CM_Lang::line('Javascript.UPLOAD_FILE_REMOVE_BTN'),
        'SELECT_CONTENTS_REMOVE_CONFIRM' => CM_Lang::line('Javascript.UPLOAD_FILE_REMOVE_CONFIRM')
      );
      $return['id'] = $itemRows[Admin_DAO_Video::COLUMN_RECORD_ID];
      $return['headline'] = $itemRows[Admin_DAO_Video::COLUMN_HEADLINE];
	
	  //150428:ANN:�Y�ϰ��D
      //if(!Admin::not_empty_array($itemRows['file_fattr'])) return false;

      //$file = array_shift($itemRows['file_fattr']);
	  
	  if(!Admin::not_empty_array($itemRows['file_img_fattr'])) return false;

      $file = array_shift($itemRows['file_img_fattr']);

      if(!Admin::not_empty_array($itemRows['file_img_fattr'])) {
        $thumbnailFile = $file;
      } else {
        $thumbnailFile = array_shift($itemRows['file_img_fattr']);
      }
      
      $return['file'] = $file['file'];
      $return['thumbnail'] = CM_Html::img(CM_Format::to_thumbnail_src($thumbnailFile['file']), $return['headline'], 80, 60);

      $return['preview'] = CM_Conf::get('PATH_W.UPLOAD') . $return['file'];
      $return['desc'] = Admin::substr($itemRows['desc'], 50);
      return $return;
    }

    static public function get_interval_time($start, $end, $unit = 'month', $params = array()) {
      $params = array_merge(array(
        'format' => 'Y-m-d H:i:s'
      ), $params);
      $return = array();
      $sTime = strtotime($start);
      $eTime = strtotime($end);

      $newTime = strtotime("now +1 {$unit}", $sTime);

      if($newTime > $eTime) return $return;

      $new = date($params['format'], $newTime);
      $return[] = $new;
      return array_merge($return, self::get_interval_time(date('Y-m-d H:i:s', $newTime), $end, $unit, $params));
    }
  }
?>