<?php
  class Admin_Menu {
    static protected $_MOD;

    static public function lang($key) {
      return CM_Lang::line(__CLASS__ . '.' . $key);
    }
    
    static protected function load_mod($modName) {
      if(!isset(self::$_MOD[$modName])) {
        $modPath = CM_Conf::get('PATH_F.ADMIN_MOD') . $modName . '/' . $modName . '.mod.php';
        $modClassName = sprintf('mod%s', ucfirst($modName));
        
        if(!is_file($modPath)) return false;

        if(!class_exists($modClassName)) require($modPath);
        
        self::$_MOD[$modName] = Admin_Factory::createCMModule($modName);
      }

      return true;
    }

    static protected function to_menu_array($icon, $mod, $request, $action = '', $params = array(), $text = '', $sub = array()) {
      return array(
        'icon' => $icon,
        'text' => $text,
        'mod' => $mod,
        'request' => $request,
        'action' => $action,
        'params' => $params,
        'sub' => $sub
      );
    }

    static protected function get_menu_array() {
      $return = array();
      
      $return[] = self::to_menu_array('icofont-book', 'article', 'default');
      $return[] = self::to_menu_array('icofont-flag', 'news', 'default');
      $return[] = self::to_menu_array('icofont-link', 'link', 'default');
      $return[] = self::to_menu_array('icofont-user', 'customers', 'default', '', array(), '', array(
        self::to_menu_array('icofont-user', 'customers', 'list'),
        self::to_menu_array('icofont-group', 'customers', 'group_list'),
        self::to_menu_array('icofont-envelope', 'contact', 'default')
        //self::to_menu_array('icofont-pushpin', 'orders', 'default')
      ));
      $return[] = self::to_menu_array('icofont-shopping-cart', 'product', 'default', '', array(), '', array(
        self::to_menu_array('icofont-shopping-cart', 'product', 'list'),
        self::to_menu_array('icofont-credit-card', 'core', 'payment')
      ));
      $return[] = self::to_menu_array('icofont-flag', 'marquee', 'default');
      $return[] = self::to_menu_array('icofont-picture', 'adBanners', 'default');
      //$return[] = self::to_menu_array('icofont-picture', 'banners', 'default');
      $return[] = self::to_menu_array('icofont-tags', 'option', 'default');
      $return[] = self::to_menu_array('icofont-th-large', 'node', 'default');
      $return[] = self::to_menu_array('icofont-th-list', 'core', 'account_list', '', array(), '', array(
        self::to_menu_array('icofont-th-list', 'core', 'account_list'),
        self::to_menu_array('icofont-group', 'core', 'account_group_list'),
      ));
      $return[] = self::to_menu_array('icofont-user', 'core', 'account_info', '', array(), '', array(
        self::to_menu_array('icofont-user', 'core', 'account_info'),
        self::to_menu_array('icofont-key', 'core', 'account_password'),
        self::to_menu_array('icofont-picture', 'core', 'file')
      ));
      $return[] = self::to_menu_array('icofont-cogs', 'core', 'configure', '', array(), '', array(
        self::to_menu_array('icofont-cog', 'core', 'configure'),
        self::to_menu_array('icofont-file', 'core', 'log')
      ));
      $return[] = self::to_menu_array('icofont-off', 'core', 'cm_logoff');
      return $return;
    }

    static public function to_menu_tmpl() {
      $menu = array();

      foreach(self::get_menu_array() as $menuRows) {
        if(!self::load_mod($menuRows['mod'])) continue;

        $request = self::$_MOD[$menuRows['mod']]
          ->get_request($menuRows['request'], $menuRows['action'])
          ->set_params($menuRows['params']);

        if(empty($menuRows['text'])) {
          $request->set_text_format('<div class="helper-font-24"><i class="' . $menuRows['icon'] . '"></i></div><span class="sidebar-text">%s</span>');
        } else {
          $request->set_text('<div class="helper-font-24"><i class="' . $menuRows['icon'] . '"></i></div><span class="sidebar-text">' . $menuRows['text'] . '</span>');
        }

        $mainPermission = $request->check_permission();
        $menuSub = array();

        if(!empty($menuRows['sub'])) {
          foreach($menuRows['sub'] as $menuSubRows) {
            if(!self::load_mod($menuSubRows['mod'])) continue;

            $requestSub = self::$_MOD[$menuSubRows['mod']]
              ->get_request($menuSubRows['request'], $menuSubRows['action'])
              ->set_params($menuSubRows['params']);

            if(empty($menuSubRows['text'])) {
              $requestSub->set_text_format('<div class="helper-font-24"><i class="' . $menuSubRows['icon'] . '"></i></div><span class="sidebar-text">%s</span>');
            } else {
              $requestSub->set_text('<div class="helper-font-24"><i class="' . $menuSubRows['icon'] . '"></i></div><span class="sidebar-text">' . $menuSubRows['text'] . '</span>');
            }

            $subPermission = $requestSub->check_permission();

            if($mainPermission == true) {
              if($subPermission == false) continue;
            } else {
              if($subPermission == true) {
                $mainPermission = true;
                $request = $requestSub;
              }
            }

            if($subPermission == true) {
              $menuSub[] = array(
                'link' => $requestSub->get_link(),
                'has_sub' => false,
                'sub' => array(),
                'uClass' => ''
              );
            }      
          }
        }

        if($mainPermission == false) continue;

        $menu[] = array(
          'link' => $request->get_link(),
          'has_sub' => !empty($menuSub),
          'sub' => $menuSub,
          'uClass' => 'sub-sidebar corner-top shadow-silver-dark'
        );
      }

      return $menu;
    }
  }
?>