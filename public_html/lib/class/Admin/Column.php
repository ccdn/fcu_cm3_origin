<?php
  class Admin_Column extends CM_Column {
    protected function get_default_upload_params() {
      return array(
        'f_type' => 'file',
        'fu_url' => Admin::href_admin_link('controller.php', sprintf('%s=upload', Admin::VAR_ACTION)),
        'fu_params' => array(
          Admin::VAR_MODULE => 'core',
          Admin::VAR_REQUEST => 'upload_files'
        ),
        'fu_options' => array(
          'maxNumberOfFiles' => CM_Conf::get('UPLOAD.COUNT'),
          'maxFileSize' => CM_Conf::get('UPLOAD.MAX_SIZE'),
          'minFileSize' => CM_Conf::get('UPLOAD.MIN_SIZE'),
          'acceptFileTypes' => CM_Conf::get('UPLOAD.ALLOW_EXT')
        )
      );
    }

    protected function get_default_upload_m_params() {
      return array(
        'f_type' => 'file_m',
        'fu_url' => Admin::href_admin_link('controller.php', sprintf('%s=upload_m', Admin::VAR_ACTION)),
        'fu_params' => array(
          Admin::VAR_MODULE => 'core',
          Admin::VAR_REQUEST => 'upload_files'
        ),
        'fu_options' => array(
          'maxNumberOfFiles' => CM_Conf::get('UPLOAD.COUNT'),
          'maxFileSize' => CM_Conf::get('UPLOAD.MAX_SIZE'),
          'minFileSize' => CM_Conf::get('UPLOAD.MIN_SIZE'),
          'acceptFileTypes' => CM_Conf::get('UPLOAD.ALLOW_EXT')
        )
      );
    }
    
    protected function get_default_upload_t_params() {
      return array(
        'f_type' => 'file_t',
        'fu_url' => Admin::href_admin_link('controller.php', sprintf('%s=upload', Admin::VAR_ACTION)),
        'fu_params' => array(
          Admin::VAR_MODULE => 'core',
          Admin::VAR_REQUEST => 'upload_files'
        ),
        'fu_options' => array(
          'maxNumberOfFiles' => CM_Conf::get('UPLOAD.COUNT'),
          'maxFileSize' => CM_Conf::get('UPLOAD.MAX_SIZE'),
          'minFileSize' => CM_Conf::get('UPLOAD.MIN_SIZE'),
          'acceptFileTypes' => CM_Conf::get('UPLOAD.ALLOW_EXT')
        )
      );
    }
    
    protected function get_default_editor_params() {
      return array(
        'f_type' => 'textarea',
        'fe_enable' => true
      );
    }

    protected function get_default_address_params() {
      return array(
        'f_type' => 'text',
        'minLen' => 6,
        'maxLen' => 255
      );
    }
  }
?>