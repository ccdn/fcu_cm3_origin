<?php
  class Admin_DAO_AdminLog extends Admin_DAO {
    const TABLE_NAME = 'admin_log';
    const COLUMN_RECORD_ID = 'id';
    const COLUMN_HEADLINE = 'request_headline';
    const DEFAULT_SORT = '`create_date` DESC';
    const GROUP_ADD = 'add';
    const GROUP_EDIT = 'edit';
    const GROUP_LIST = 'list';
    const GROUP_DETAIL = 'detail';

    static public function lang($key) {
      return CM_Lang::line("DAO_AdminLog.{$key}");
    }

    public function __construct($daoName) {
      parent::__construct($daoName, self::COLUMN_RECORD_ID, self::TABLE_NAME);
      //載入自定義欄位
      //$this->init_column('columnName', 'columnTable', array('params1' => '1'), 'columnDefault');
      //載入自定義欄位群組
      //$this->set_column_group('groupName', array('column1', 'column2', 'column3'));
      //載入自定義表單群組
      //$this->set_form_group('groupName', array('column1', 'column2', 'column3'));
      $this->set_column('request_content', array(
        'f_readonlyCallback' => function($formAction, $column, $columnRows) {
          $json = json_decode($column->val);
          $return = array();

          if(!empty($json)) {
            foreach($json as $key => $value) $return[] = sprintf('<p><strong>%s：</strong></p><p>%s</p>', $key, $value);
          }

          return implode('<br />', $return);
        }
      ));
    }

    protected function get_sql($groupName, $params = array()) {
      if($params['isCount'] == true) {
        //取得筆數
        $sqlColumn = sprintf('COUNT(`%s`) AS `total`', self::COLUMN_RECORD_ID);
      } else {
        //欄位群組和一定會取出的資料
        $sqlColumn = '`' . implode('`, `', array_merge(
          array_keys($this->get_column_group($groupName)),
          array(self::COLUMN_RECORD_ID)
        )) . '`';
      }
      //如果有代入RecordId，解決排序的問題
      $this->process_recordId_sort($params, sprintf('`%s`', self::COLUMN_RECORD_ID));
      $sqlWhere = $this->get_sql_where($params);
      $sqlLimit = $params['isLimit'] == true ? sprintf('%d, %d', $params['startIndex'], $params['endIndex']) : '';
      $sqlSort = !empty($params['sort']) ? $params['sort'] : self::DEFAULT_SORT;

      return array(sprintf("SELECT %s FROM `%s`%s ORDER BY %s%s",
        $sqlColumn,
        self::TABLE_NAME,
        !empty($sqlWhere['sql']) ? ' WHERE ' . implode(' AND ', $sqlWhere['sql']) : '',
        $sqlSort,
        !empty($sqlLimit) ? ' LIMIT ' . $sqlLimit : ''
      ), $sqlWhere['params']);
    }
    
    protected function get_sql_where(array $params) {
      $return = $this->sql_where_default(sprintf('`%s`', self::COLUMN_RECORD_ID), $params);
      //帳號
      if(isset($params['account']) && !empty($params['account'])) {
        $return['sql'][] = "`account_name` LIKE :accountName";
        $return['params'][':accountName'] = "%{$params['account']}%";
      }
      //關鍵字條件
      if(isset($params['keyword']) && !empty($params['keyword'])) {
        $return['sql'][] = sprintf("(`%s` LIKE :keyword1 OR `request_name` LIKE :keyword2)", self::COLUMN_HEADLINE);
        $return['params'][':keyword1'] = "%{$params['keyword']}%";
        $return['params'][':keyword2'] = "%{$params['keyword']}%";
      }
      //日期區間條件
      if(isset($params['date']) && is_array($params['date'])) {
        $return['sql'][] = $this->sql_where_date_area('`create_date`', $params['date']['start'], $params['date']['end'], array('hasTime' => true));
      }
      
      return $return;
    }
    
    public function insert_rows(&$rMessage, &$rReturn, $recordRows, $groupName = '', $params = array()) {
      $rMessage = '';
      $timeNow = date('Y-m-d H:i:s');
      $recordRows['create_date'] = $timeNow;
      $recordRows['modify_date'] = $timeNow;
      $success = $this->process_insert($rMessage, $rReturn, $recordRows, array_merge($params, array(
        'groupName' => $groupName
      )));
      
      if($success == true) $rMessage = self::lang('ADD_SUCCESS');
      
      return $success;
    }
    
    public function update_rows(&$rMessage, &$rReturn, $recordRows, $recordId, $groupName = '', $params = array()) {
      $rMessage = '';
      $success = $this->process_update($rMessage, $rReturn, $recordRows, $recordId, array_merge($params, array(
        'groupName' => $groupName,
        'isLang' => false
      )));

      if($success == true) $rMessage = self::lang('EDIT_SUCCESS');

      return $success;
    }
    
    public function delete_rows(&$rMessage, &$rReturn, $recordId, $params = array()) {
      $rMessage = '';
      $success = $this->process_delete($rMessage, $rReturn, $recordId, array_merge($params, array(
        'isLang' => false
      )));
      
      if($success == true) $rMessage = self::lang('REMOVE_SUCCESS');
      
      return $success;
    }
  }
?>