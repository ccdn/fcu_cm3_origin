<?php
  class Admin_DAO_Stream_Log extends Admin_DAO {
    const TABLE_NAME = 'stream_log';
    const COLUMN_RECORD_ID = 'iLogRm_seq';
    const COLUMN_HEADLINE = 'cVod_server_domain';
    const DEFAULT_SORT = '`iVod_seq` ASC';
    const GROUP_LIST = 'list';

    static public function lang($key) {
      return CM_Lang::line("DAO_Stream_Server.{$key}");
    }

    public function __construct($daoName) {
      parent::__construct($daoName, self::COLUMN_RECORD_ID, self::TABLE_NAME);
      //載入自定義欄位
      //$this->init_column('columnName', 'columnTable', array('params1' => '1'), 'columnDefault');
      //載入自定義欄位群組
      //$this->set_column_group('groupName', array('column1', 'column2', 'column3'));
      //載入自定義表單群組
      //$this->set_form_group('groupName', array('column1', 'column2', 'column3'));
    }
    
    protected function get_sql($groupName, $params = array()) {
      if($params['isCount'] == true) {
        //取得筆數
        $sqlColumn = sprintf('COUNT(`%s`) AS `total`', self::COLUMN_RECORD_ID);
      } elseif(
        (isset($params['viewCount']) && !empty($params['viewCount'])) ||
        (isset($params['viewTime']) && !empty($params['viewTime']))
      ) {
        $sqlColumn = array();

        if(isset($params['viewCount']) && $params['viewCount'] == true) $sqlColumn[] = sprintf('COUNT(`%s`) AS `viewCount`', self::COLUMN_RECORD_ID);

        if(isset($params['viewTime']) && $params['viewTime'] == true) $sqlColumn[] = "SUM(`iConnected_time`) AS `viewTime`";

        $sqlColumn = implode(', ', $sqlColumn);
      } else {
        //欄位群組和一定會取出的資料
        $sqlColumn = '`' . implode('`, `', array_merge(
          array_keys($this->get_column_group($groupName)),
          array(self::COLUMN_RECORD_ID)
        )) . '`';
      }
      //如果有代入RecordId，解決排序的問題
      $this->process_recordId_sort($params, sprintf('`%s`', self::COLUMN_RECORD_ID));
      $sqlWhere = $this->get_sql_where($params);
      $sqlLimit = $params['isLimit'] == true ? sprintf('%d, %d', $params['startIndex'], $params['endIndex']) : '';
      $sqlSort = !empty($params['sort']) ? $params['sort'] : self::DEFAULT_SORT;
      return array(sprintf("SELECT %s FROM `%s`%s ORDER BY %s%s",
        $sqlColumn,
        self::TABLE_NAME,
        !empty($sqlWhere['sql']) ? ' WHERE ' . implode(' AND ', $sqlWhere['sql']) : '',
        $sqlSort,
        !empty($sqlLimit) ? ' LIMIT ' . $sqlLimit : ''
      ), $sqlWhere['params']);
    }
    
    protected function get_sql_where(array $params) {
      $return = $this->sql_where_default(sprintf('`%s`', self::COLUMN_RECORD_ID), $params);
      //關鍵字條件
      if(isset($params['keyword']) && !empty($params['keyword'])) {
        $return['sql'][] = sprintf("`%s` LIKE :keyword", self::COLUMN_HEADLINE);
        $return['params'][':keyword'] = "%{$params['keyword']}%";
      }

      if(isset($params['timestamp']) && is_array($params['timestamp'])) {
        $return['sql'][] = $this->sql_where_date_area('`tTimestamp`', $params['timestamp']['start'], $params['timestamp']['end'], array('hasTime' => true));
      }
      //判斷會員
      if(isset($params['customersId']) && !empty($params['customersId'])) {
        $return['sql'][] = "`iUsr_seq` = :customersId";
        $return['params'][':customersId'] = $params['customersId'];
      }

      if(isset($params['hour'])) {
        $return['sql'][] = "`tTimestamp` LIKE :hour";
        $return['params'][':hour'] = "% {$params['hour']}:%";
      }
      //判斷Device
      if(isset($params['device']) && in_array($params['device'], array('PC', 'Android', 'iOS'))) {
        if($params['device'] == 'PC') {
          $return['sql'][] = "(`cClient_info` LIKE '%WIN%' OR `cClient_info` LIKE '%MAC%')";
        } elseif($params['device'] == 'Android') {
          $return['sql'][] = "`cClient_info` LIKE '%Android%'";
        } else {
          $return['sql'][] = "(`cClient_info` LIKE '%iPhone%' OR `cClient_info` LIKE '%iPad%')";
        }
      }
      //判斷國別
      if(isset($params['country']) && !empty($params['country'])) {
        $return['sql'][] = "`cClient_Country` = :country";
        $return['params'][':country'] = $params['country'];
      }
      
      return $return;
    }
  }
?>