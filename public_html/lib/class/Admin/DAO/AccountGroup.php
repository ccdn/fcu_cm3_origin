<?php
  class Admin_DAO_AccountGroup extends Admin_DAO {
    const TABLE_NAME = 'admin_groups';
    const COLUMN_RECORD_ID = 'id';
    const COLUMN_HEADLINE = 'name';
    const DEFAULT_SORT = '`id` ASC';
    const GROUP_ADD = 'add';
    const GROUP_EDIT = 'edit';
    const GROUP_LIST = 'list';
    const GROUP_OPTION = 'option';
    const GROUP_PERMISSION = 'permission';

    static public function lang($key) {
      return CM_Lang::line("DAO_AccountGroup.{$key}");
    }

    public function __construct($daoName) {
      parent::__construct($daoName, self::COLUMN_RECORD_ID, self::TABLE_NAME);
      //載入自定義欄位
      //$this->init_column('columnName', 'columnTable', array('params1' => '1'), 'columnDefault');
      //載入自定義欄位群組
      //$this->set_column_group('groupName', array('column1', 'column2', 'column3'));
      //載入自定義表單群組
      //$this->set_form_group('groupName', array('column1', 'column2', 'column3'));
    }

    protected function get_sql($groupName, $params = array()) {
      if($params['isCount'] == true) {
        //取得筆數
        $sqlColumn = sprintf('COUNT(`%s`) AS `total`', self::COLUMN_RECORD_ID);
      } else {
        //欄位群組和一定會取出的資料
        $sqlColumn = '`' . implode('`, `', array_merge(
          array_keys($this->get_column_group($groupName)),
          array(self::COLUMN_RECORD_ID)
        )) . '`';
      }
      //如果有代入RecordId，解決排序的問題
      $this->process_recordId_sort($params, sprintf('`%s`', self::COLUMN_RECORD_ID));

      $sqlWhere = $this->get_sql_where($params);
      $sqlLimit = $params['isLimit'] == true ? sprintf('%d, %d', $params['startIndex'], $params['endIndex']) : '';
      $sqlSort = !empty($params['sort']) ? $params['sort'] : self::DEFAULT_SORT;

      return array(sprintf("SELECT %s FROM `%s`%s ORDER BY %s%s",
        $sqlColumn,
        self::TABLE_NAME,
        !empty($sqlWhere['sql']) ? ' WHERE ' . implode(' AND ', $sqlWhere['sql']) : '',
        $sqlSort,
        !empty($sqlLimit) ? ' LIMIT ' . $sqlLimit : ''
      ), $sqlWhere['params']);
    }
    
    protected function get_sql_where(array $params) {
      $return = $this->sql_where_default(sprintf('`%s`', self::COLUMN_RECORD_ID), $params);
      //關鍵字條件
      if(isset($params['keyword']) && !empty($params['keyword'])) {
        $return['sql'][] = sprintf("`%s` LIKE :keyword", self::COLUMN_HEADLINE);
        $return['params'][':keyword'] = "%{$params['keyword']}%";
      }
      
      return $return; 
    }

    public function init_options() {
      foreach(array('video', 'sound', 'picture') as $catalogName) {
        $catalogNameLarge = ucfirst($catalogName);
        $catalogDAO = Admin_Factory::createCMDAO("catalog_{$catalogNameLarge}");
        $catalogOptions = array();
        $catalogOptions[] = array('id' => '-1', 'text' => '--全部分類都可以看--');
        $catalogOptions[] = array('id' => '0', 'text' => '根目錄');

        if($catalogDAO->get_array_tree_childrens($catalogArray, $catalogIdArray, constant("Admin_DAO_Catalog_{$catalogNameLarge}::GROUP_LIST"), '0')) {
          foreach($catalogArray as $catalogId => $catalogRows) {
            $catalogOptions[] = array('id' => $catalogId, 'text' => implode('&nbsp;&raquo;&nbsp;', array_map(function($element) use($catalogNameLarge) {
              return sprintf('[%s]%s', $element[constant("Admin_DAO_Catalog_{$catalogNameLarge}::COLUMN_RECORD_ID")], $element[constant("Admin_DAO_Catalog_{$catalogNameLarge}::COLUMN_HEADLINE")]);
            }, Admin_DAO_Catalog_Video::get_catalog_parents($catalogId, $catalogArray))));
          }
        }
        
        $this->set_options("{$catalogName}_catalog", $catalogOptions);
        $this->set_column("{$catalogName}_catalog", array(
          'f_attr' => array(
            'style' => 'width: 90%'
          ),
          'f_options' => $this->get_options("{$catalogName}_catalog")
        ));
      }
    }

    public function insert_rows(&$rMessage, &$rReturn, $recordRows, $groupName = '', $params = array()) {
      $rMessage = '';
      $success = $this->process_insert($rMessage, $rReturn, $recordRows, array_merge($params, array(
        'groupName' => $groupName
      )));
      
      if($success == true) $rMessage = self::lang('ADD_SUCCESS');
      
      return $success;
    }

    public function update_rows(&$rMessage, &$rReturn, $recordRows, $recordId, $groupName = '', $params = array()) {
      $rMessage = '';
      $success = $this->process_update($rMessage, $rReturn, $recordRows, $recordId, array_merge($params, array(
        'groupName' => $groupName
      )));

      if($success == true) $rMessage = self::lang('EDIT_SUCCESS');

      return $success;
    }

    public function delete_rows(&$rMessage, &$rReturn, $recordId, $params = array()) {
      $accountDAO = Admin_Factory::createCMDAO('account');
      $rMessage = '';
      $success = $this->process_delete($rMessage, $rReturn, $recordId, array_merge($params, array(
        'checkCallable' => function(&$rMessage, $inputRows, $dataRows) use($accountDAO, $recordId) {
          //檢查要移除的群組底下有無帳號
          $total = $accountDAO->get_rows($totalRows, Admin_DAO_Account::GROUP_LIST, array(
            'groupId' => $recordId,
            'isCount' => true
          )) ? $totalRows['total'] : 0;

          if($total > 0) {
            $rMessage = sprintf(Admin_DAO_AccountGroup::lang('ERROR_HAS_ACCOUNT'), $dataRows[Admin_DAO_AccountGroup::COLUMN_HEADLINE], $total);
            return false;
          } else {
            return true;
          }
        }
      )));
      
      if($success == true) $rMessage = self::lang('REMOVE_SUCCESS');
      
      return $success;
    }
  }
?>