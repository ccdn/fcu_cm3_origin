<?php
  class Admin_DAO_IndexSetting extends CM_DAO_IndexSetting {
    public function __construct($daoName) {
      parent::__construct($daoName);
    }
    
    protected function init_column($columnName, $columnTable, $columnParams, $default = '') {
      $this->_column[$columnName] = Admin_Factory::createCMColumn($columnName, array_merge($columnParams, array(
        'table' => $columnTable
      )), $default);
      $this->set_column($columnName, array('name_full' => $this->get_column($columnName)->get_full_name($this->_table_alias)));
    }

    public function get_array(&$returnArray, $groupName, $params = array()) {
      $success = parent::get_array($returnArray, $groupName, $params);

      if(isset($params['get_item']) && $params['get_item'] == true) {
        $tmpDAO = array();

        foreach($returnArray as $key => $rows) {
          $returnArray[$key]['items'] = array();

          if(!isset($rows['item_id']) || empty($rows['item_id'])) continue;

          if(!isset($tmpDAO[$rows['dao']])) $tmpDAO[$rows['dao']] = Admin_Factory::createCMDAO($rows['dao']);

          if($tmpDAO[$rows['dao']]->get_array($itemArray, constant(sprintf('CM_DAO_%s::GROUP_LIST', ucfirst($rows['dao']))), array(
            'recordIds' => explode(',', $rows['item_id'])
          ))) {
            $converColumn = array($this, "convert_{$rows['dao']}_column");

            if(is_callable($converColumn)) {
              foreach($itemArray as $itemKey => $itemRows) $itemArray[$itemKey] = call_user_func($converColumn, $itemRows);
            }

            $returnArray[$key]['items'] = $itemArray;
          }
        }
      }
      
      return $success;
    }
  }
?>