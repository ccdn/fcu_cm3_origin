<?php
  class Admin_DAO_File extends Admin_DAO {
    const TABLE_NAME = 'file';
    const COLUMN_RECORD_ID = 'file';
    const COLUMN_HEADLINE = 'name';
    const DEFAULT_SORT = '`create_date` DESC';
    const GROUP_ADD = 'add';
    const GROUP_EDIT = 'edit';
    const GROUP_LIST = 'list';
    const GROUP_LINK = 'link';
    const GROUP_CALLBACK = 'callback';
    const GROUP_PROCESS = 'process';

    static public function lang($key) {
      return CM_Lang::line("DAO_File.{$key}");
    }

    public function __construct($daoName) {
      parent::__construct($daoName, self::COLUMN_RECORD_ID, self::TABLE_NAME);
      //載入自定義欄位
      //$this->init_column('columnName', 'columnTable', array('params1' => '1'), 'columnDefault');
      //載入自定義欄位群組
      //$this->set_column_group('groupName', array('column1', 'column2', 'column3'));
      $this->set_column_group(self::GROUP_LINK, array('link', 'modify_date'));
      //載入自定義表單群組
      //$this->set_form_group('groupName', array('column1', 'column2', 'column3'));
      $this->set_column('desc', array('f_attr' => array('style' => 'width: 450px; height: 150px;')));
    }

    protected function get_sql($groupName, $params = array()) {
      if($params['isCount'] == true) {
        //取得筆數
        $sqlColumn = sprintf('COUNT(`%s`) AS `total`', self::COLUMN_RECORD_ID);
      } else {
        //欄位群組和一定會取出的資料
        $sqlColumn = '`' . implode('`, `', array_merge(
          array_keys($this->get_column_group($groupName)),
          array(self::COLUMN_RECORD_ID)
        )) . '`';
      }
      //如果有代入RecordId，解決排序的問題
      $this->process_recordId_sort_s($params, sprintf('`%s`', self::COLUMN_RECORD_ID));

      $sqlWhere = $this->get_sql_where($params);
      $sqlLimit = $params['isLimit'] == true ? sprintf('%d, %d', $params['startIndex'], $params['endIndex']) : '';
      $sqlSort = !empty($params['sort']) ? $params['sort'] : self::DEFAULT_SORT;

      return array(sprintf("SELECT %s FROM `%s`%s ORDER BY %s%s",
        $sqlColumn,
        self::TABLE_NAME,
        !empty($sqlWhere['sql']) ? ' WHERE ' . implode(' AND ', $sqlWhere['sql']) : '',
        $sqlSort,
        !empty($sqlLimit) ? ' LIMIT ' . $sqlLimit : ''
      ), $sqlWhere['params']);
    }
    
    protected function get_sql_where(array $params) {
      $return = $this->sql_where_default_s(sprintf('`%s`', self::COLUMN_RECORD_ID), $params);
      //關鍵字條件
      if(isset($params['keyword']) && !empty($params['keyword'])) {
        $return['sql'][] = sprintf("`%s` LIKE :keyword", self::COLUMN_HEADLINE);
        $return['params'][':keyword'] = "%{$params['keyword']}%";
      }

      return $return; 
    }

    public function insert_rows(&$rMessage, &$rReturn, $recordRows, $groupName = '', $params = array()) {
      $rMessage = '';
      $timeNow = date('Y-m-d H:i:s');
      $recordRows = array_merge($recordRows, array(
        'create_date' => $timeNow,
        'modify_date' => $timeNow
      ));
      $success = $this->process_insert($rMessage, $rReturn, $recordRows, array_merge($params, array(
        'groupName' => $groupName
      )));
      
      if($success == true) $rMessage = self::lang('ADD_SUCCESS');
      
      return $success;
    }

    public function update_rows(&$rMessage, &$rReturn, $recordRows, $recordId, $groupName = '', $params = array()) {
      $rMessage = '';
      $recordRows = array_merge($recordRows, array(
        'modify_date' => date('Y-m-d H:i:s')
      ));
      $success = $this->process_update($rMessage, $rReturn, $recordRows, $recordId, array_merge($params, array(
        'groupName' => $groupName
      )));

      if($success == true) $rMessage = self::lang('EDIT_SUCCESS');

      return $success;
    }

    public function delete_rows(&$rMessage, &$rReturn, $recordId, $params = array()) {
      $success = $this->process_delete($rMessage, $rReturn, $recordId, $params);
      
      if($success == true) {
        $t_fileNameArray = explode('video/', $rReturn['currentRows'][self::COLUMN_RECORD_ID]);
        $filePath = array();

        if(!empty($t_fileNameArray[1])) {
          $t_fileNameSplit = explode('.', $t_fileNameArray[1]);
          
          for($i = 1; $i <= 4; $i++) {
            $filePath[] = CM_Conf::get('PATH_F.UPLOAD') . 'new_Content/' . $t_fileNameSplit[0] . '_' . $i . '.' . $t_fileNameSplit[1];
          }
        } else {
          $filePath[] = CM_Conf::get('PATH_F.UPLOAD') . $rReturn['currentRows'][self::COLUMN_RECORD_ID];
        }

        foreach($filePath AS $filePathRows) {
          if(is_file($filePathRows)) @unlink($filePathRows);
        }
        
        $rMessage = self::lang('REMOVE_SUCCESS'); 
      }
      
      return $success;
    }
  }
?>