<?php
  class Admin_DAO_Contact extends Admin_DAO {
    const TABLE_NAME = 'contact';
    const COLUMN_RECORD_ID = 'id';
    const COLUMN_HEADLINE = 'name';
    const COLUMN_STATUS = 'status';
    const COLUMN_CREATE_DATE = 'create_date';
    const COLUMN_MODIFY_DATE = 'modify_date';
    const DEFAULT_SORT = '`id` ASC';
    const GROUP_ADD = 'add';
    const GROUP_EDIT = 'edit';
    const GROUP_LIST = 'list';
    const GROUP_STATUS = 'status';
    const GROUP_SITE_ADD = 'site_add';
    const GROUP_SITE_MAIL = 'site_mail';

    static protected function password_encode($password) {
      return md5($password . 'password_check');
    }

    static public function lang($key) {
      return CM_Lang::line("DAO_Contact.{$key}");
    }

    public function __construct($daoName) {
      parent::__construct($daoName, self::COLUMN_RECORD_ID, self::TABLE_NAME);
      //載入自定義欄位
      //$this->init_column('columnName', 'columnTable', array('params1' => '1'), 'columnDefault');
      //載入自定義欄位群組
      $this->init_column(self::COLUMN_CREATE_DATE, self::TABLE_NAME, array('label' => self::lang('COLUMN_CREATE_DATE')), 'record_time');
      $this->init_column(self::COLUMN_MODIFY_DATE, self::TABLE_NAME, array('label' => self::lang('COLUMN_MODIFY_DATE')), 'record_time');
      
      $this->set_column_group(self::GROUP_ADD, array(self::COLUMN_HEADLINE, 'sex', 'email', 'phone', 'cellphone', 'company', 'note', 'ip', self::COLUMN_STATUS, self::COLUMN_CREATE_DATE, self::COLUMN_MODIFY_DATE));
      $this->set_column_group(self::GROUP_EDIT, array(self::COLUMN_HEADLINE, 'sex', 'email', 'phone', 'company', 'note', self::COLUMN_STATUS, self::COLUMN_MODIFY_DATE));
      $this->set_column_group(self::GROUP_LIST, array(self::COLUMN_RECORD_ID, self::COLUMN_HEADLINE, 'email', 'ip', self::COLUMN_STATUS, self::COLUMN_CREATE_DATE));
      $this->set_column_group(self::GROUP_STATUS, array(self::COLUMN_STATUS, self::COLUMN_MODIFY_DATE));
      
      $this->set_column_group(self::GROUP_SITE_MAIL, array(self::COLUMN_HEADLINE, 'phone', 'cellphone', 'sex', 'email', 'company', 'note'));
      $this->set_column_group(self::GROUP_SITE_ADD, array(self::COLUMN_HEADLINE, 'phone', 'cellphone', 'sex', 'email', 'company', 'note', 'ip', self::COLUMN_STATUS, self::COLUMN_CREATE_DATE, self::COLUMN_MODIFY_DATE));
      
      //載入自定義表單群組
      $this->set_options('status', array(
        array('id' => '1', 'text' => self::lang('COLUMN_STATUS_1')),
        array('id' => '2', 'text' => self::lang('COLUMN_STATUS_2')),
        array('id' => '3', 'text' => self::lang('COLUMN_STATUS_3')),
        array('id' => '4', 'text' => self::lang('COLUMN_STATUS_4'))
      ));
      $this->set_column(self::COLUMN_STATUS, array('f_options' => $this->get_options('status')));
      
      $this->set_options('sex', array(
        array('id' => '1', 'text' => self::lang('COLUMN_SEX_1')),
        array('id' => '2', 'text' => self::lang('COLUMN_SEX_2'))
      ));
      $this->set_column('sex', array('f_options' => $this->get_options('sex')));
    }
    
    protected function get_sql($groupName, $params = array()) {
      if($params['isCount'] == true) {
        //取得筆數
        $sqlColumn = sprintf('COUNT(`%s`) AS `total`', self::COLUMN_RECORD_ID);
        $sqlLimit = '0, 1';
      } else {
        //欄位群組和一定會取出的資料
        $sqlColumn = '`' . implode('`, `', array_merge(
          array_keys($this->get_column_group($groupName)),
          array(self::COLUMN_RECORD_ID)
        )) . '`';
        $sqlLimit = $params['isLimit'] == true ? sprintf('%d, %d', $params['startIndex'], $params['endIndex']) : '';
      }
      //如果有代入RecordId，解決排序的問題
      $this->process_recordId_sort($params, sprintf('`%s`', self::COLUMN_RECORD_ID));
      $sqlWhere = $this->get_sql_where($params);
      $sqlSort = !empty($params['sort']) ? $params['sort'] : '`id` DESC';
      return array(sprintf("SELECT %s FROM `%s`%s ORDER BY %s%s",
        $sqlColumn,
        self::TABLE_NAME,
        !empty($sqlWhere['sql']) ? ' WHERE ' . implode(' AND ', $sqlWhere['sql']) : '',
        $sqlSort,
        !empty($sqlLimit) ? ' LIMIT ' . $sqlLimit : ''
      ), $sqlWhere['params']);
    }
    
    protected function get_sql_where(array $params) {
      $return = $this->sql_where_default(sprintf('`%s`', self::COLUMN_RECORD_ID), $params);
      //判斷信箱(帳號)
      if(isset($params['email']) && !empty($params['email'])) {
        $return['sql'][] = "`email` = :email";
        $return['params'][':email'] = $params['email'];
      }
      //判斷密碼
      if(isset($params['password']) && !empty($params['password'])) {
        $return['sql'][] = "`password` = :password";
        $return['params'][':password'] = self::password_encode($params['password']);
      }
      //狀態條件
      if(isset($params['status']) && !empty($params['status'])) {
        $return['sql'][] = sprintf("`%s` = :status", self::COLUMN_STATUS);
        $return['params'][':status'] = $params['status'];
      }
      //關鍵字條件
      if(isset($params['keyword']) && !empty($params['keyword'])) {
        $return['sql'][] = sprintf("`%s` LIKE :keyword", self::COLUMN_HEADLINE);
        $return['params'][':keyword'] = "%{$params['keyword']}%";
      }
      
      return $return;
    }
    
    public function insert_rows(&$rMessage, &$rReturn, $recordRows, $groupName = '', $params = array()) {     
      $rMessage = '';
      $timeNow = date('Y-m-d H:i:s');
      $recordRows = array_merge($recordRows, array(
        'ip' => CM::get_ip_address(),
        'create_date' => $timeNow,
        'modify_date' => $timeNow
      ));
      $success = $this->process_insert($rMessage, $rReturn, $recordRows, array_merge($params, array(
        'groupName' => $groupName
      )));
      
      if($success == true) $rMessage = self::lang('ADD_SUCCESS');
      
      return $success;
    }

    public function update_rows(&$rMessage, &$rReturn, $recordRows, $recordId, $groupName = '', $params = array()) {
      $rMessage = '';
      $recordRows = array_merge($recordRows, array(
        'modify_date' => date('Y-m-d H:i:s')
      ));
      $success = $this->process_update($rMessage, $rReturn, $recordRows, $recordId, array_merge($params, array(
        'groupName' => $groupName
      )));

      if($success == true) $rMessage = self::lang('EDIT_SUCCESS');

      return $success;
    }

    public function delete_rows(&$rMessage, &$rReturn, $recordId, $params = array()) {
      $rMessage = '';
      $success = $this->process_delete($rMessage, $rReturn, $recordId, $params);
      
      if($success == true) $rMessage = self::lang('REMOVE_SUCCESS');
      
      return $success;
    }
  }
?>