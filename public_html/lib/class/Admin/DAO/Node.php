<?php
  class Admin_DAO_Node extends Admin_DAO {
    const TABLE_NAME = 'node';
    const TABLE_LANG_NAME = 'node_lang';
    const COLUMN_RECORD_ID = 'id';
    const COLUMN_PARENT_ID = 'parent_id';
    const COLUMN_HEADLINE = 'headline';
    const COLUMN_STATUS = 'status';
    const COLUMN_SORT = 'sort';
    const GROUP_ID = 'id';
    const GROUP_ADD = 'add';
    const GROUP_EDIT = 'edit';
    const GROUP_LIST = 'list';
    const GROUP_STATUS = 'status';
    const GROUP_SORT = 'sort';
    const GROUP_PARENT = 'parent';

    static public function lang($key) {
      return CM_Lang::line("DAO_Node.{$key}");
    }
    
    public function __construct($daoName) {
      //設定資料庫別名
      $this->set_alias(self::TABLE_NAME, 'p');
      $this->set_alias(self::TABLE_LANG_NAME, 'pd');

      parent::__construct($daoName, self::COLUMN_RECORD_ID, self::TABLE_NAME, array(
        'tableLang' => true,
        'tableLangName' => self::TABLE_LANG_NAME,
        'tableLangColumn' => 'lang'
      ));
      //載入自定義欄位
      //$this->init_column('columnName', 'columnTable', array('params1' => '1'), 'columnDefault');
      //載入自定義欄位群組
      //$this->set_column_group('groupName', array('column1', 'column2', 'column3'));
      //載入自定義表單群組
      //$this->set_form_group('groupName', array('column1', 'column2', 'column3'));
      $this->set_options('status', array(
        array('id' => '1', 'text' => self::lang('COLUMN_STATUS_ENABLE')),
        array('id' => '2', 'text' => self::lang('COLUMN_STATUS_DISABLE'))
      ));
      $this->set_column(self::COLUMN_STATUS, array('f_options' => $this->get_options('status')));
      $this->set_column('lang_status', array('f_options' => $this->get_options('status')));
      $this->set_column('menu_status', array('f_options' => $this->get_options('status')));
    }
    
    protected function get_sql($groupName, $params = array()) {
      if($params['isCount'] == true) {
        //取得筆數
        $sqlColumn = sprintf('COUNT(`p`.`%s`) AS `total`', self::COLUMN_RECORD_ID);
      } else {
        $columnArray = $this->get_column_group($groupName);
        $sqlColumn = array();
        //如果索引沒有在列表內則新增
        if(!array_key_exists(self::COLUMN_RECORD_ID, $columnArray)) $sqlColumn[] = $this->get_column(self::COLUMN_RECORD_ID)->get_full_name($this->_table_alias);
        
        foreach($this->get_column_group($groupName) as $columnName => $column) $sqlColumn[] = $column->get_full_name($this->_table_alias);
        
        $sqlColumn = implode(', ', $sqlColumn);
      }
      //如果有代入RecordId，解決排序的問題
      $this->process_recordId_sort($params, sprintf('`p`.`%s`', self::COLUMN_RECORD_ID));
      $sqlWhere = $this->get_sql_where($params);
      $sqlLimit = $params['isLimit'] == true ? sprintf('%d, %d', $params['startIndex'], $params['endIndex']) : '';
      $sqlSort = !empty($params['sort']) ? $params['sort'] : sprintf('`p`.`%s` ASC, `p`.`%s` ASC, `p`.`%s` ASC', self::COLUMN_PARENT_ID, self::COLUMN_SORT, self::COLUMN_RECORD_ID);
      
      return array(sprintf("SELECT %s FROM `%s` AS `p` LEFT JOIN `%s` AS `pd` ON `p`.`%s` = `pd`.`%s` AND `pd`.`lang` = %d%s ORDER BY %s%s",
        $sqlColumn,
        self::TABLE_NAME,
        self::TABLE_LANG_NAME,
        self::COLUMN_RECORD_ID,
        self::COLUMN_RECORD_ID,
        isset($params['lang']) ? $params['lang'] : CM_Lang::get_current_id(true),
        !empty($sqlWhere['sql']) ? ' WHERE ' . implode(' AND ', $sqlWhere['sql']) : '',
        $sqlSort,
        !empty($sqlLimit) ? ' LIMIT ' . $sqlLimit : ''
      ), $sqlWhere['params']) ;
    }
    
    protected function get_sql_where(array $params) {
      $return = $this->sql_where_default(sprintf('`p`.`%s`', self::COLUMN_RECORD_ID), $params);
      //指定目錄
      if(isset($params[self::COLUMN_PARENT_ID])) {
        $return['sql'][] = sprintf("`p`.`%s` = :parentId", self::COLUMN_PARENT_ID);
        $return['params'][':parentId'] = (int)$params[self::COLUMN_PARENT_ID];
      }
      //狀態條件
      if(isset($params['status']) && !empty($params['status'])) {
        $return['sql'][] = sprintf("`p`.`%s` = :status", self::COLUMN_STATUS);
        $return['params'][':status'] = $params['status'];
      }
      //關鍵字條件
      if(isset($params['keyword']) && !empty($params['keyword'])) {
        $return['sql'][] = sprintf("(`pd`.`%s` LIKE :keyword1 OR `p`.`path_name` LIKE :keyword2 OR `p`.`page_name` LIKE :keyword3)", self::COLUMN_HEADLINE);
        $return['params'][':keyword1'] = "%{$params['keyword']}%";
        $return['params'][':keyword2'] = "%{$params['keyword']}%";
        $return['params'][':keyword3'] = "%{$params['keyword']}%";
      }
      
      return $return;
    }

    public function get_array_tree_children(&$returnArray, &$returnIdArray, $groupName, $recordId, $params = array(), $treeParams = array()) {
      $returnArray = array();
      $returnIdArray = array();
      $treeParams = array_merge(array(
        'columnRecordIdName' => self::COLUMN_RECORD_ID,
        'columnParentIdName' => self::COLUMN_PARENT_ID,
        'daoParams' => $params,
        'groupName' => $groupName
      ), $treeParams);
      $returnIdArray = $this->tree_func_children($recordId, $returnArray, $treeParams);

      if(empty($returnArray) || empty($returnIdArray)) {
        return false;
      } else {
        return true;
      }
    }

    public function get_array_tree_childrens(&$returnArray, &$returnIdArray, $groupName, $recordId, $params = array(), $treeParams = array()) {
      $returnArray = array();
      $returnIdArray = array();
      $treeParams = array_merge(array(
        'columnRecordIdName' => self::COLUMN_RECORD_ID,
        'columnParentIdName' => self::COLUMN_PARENT_ID,
        'daoParams' => $params,
        'groupName' => $groupName
      ), $treeParams);
      $returnIdArray = $this->tree_func_childrens($recordId, $returnArray, $treeParams);

      if(empty($returnArray) || empty($returnIdArray)) {
        return false;
      } else {
        return true;
      }
    }

    public function get_array_tree_parent(&$returnArray, &$returnIdArray, $groupName, $recordId, $params = array(), $treeParams = array()) {
      $returnArray = array();
      $returnIdArray = array();
      $treeParams = array_merge(array(
        'columnRecordIdName' => self::COLUMN_RECORD_ID,
        'columnParentIdName' => self::COLUMN_PARENT_ID,
        'daoParams' => $params,
        'groupName' => $groupName
      ), $treeParams);
      $returnIdArray = $this->tree_func_parent($recordId, $returnArray, $treeParams);

      if(empty($returnArray) || empty($returnIdArray)) {
        return false;
      } else {
        return true;
      }
    }

    public function get_array_tree_parents(&$returnArray, &$returnIdArray, $groupName, $recordId, $params = array(), $treeParams = array()) {
      $returnArray = array();
      $returnIdArray = array();
      $treeParams = array_merge(array(
        'columnRecordIdName' => self::COLUMN_RECORD_ID,
        'columnParentIdName' => self::COLUMN_PARENT_ID,
        'daoParams' => $params,
        'groupName' => $groupName
      ), $treeParams);
      $returnIdArray = $this->tree_func_parents($recordId, $returnArray, $treeParams);

      if(empty($returnArray) || empty($returnIdArray)) {
        return false;
      } else {
        return true;
      }
    }
    
    public function insert_rows(&$rMessage, &$rReturn, $recordRows, $groupName = '', $params = array()) {
      $rMessage = '';
      $timeNow = date('Y-m-d H:i:s');
      $recordRows = array_merge($recordRows, array(
        'create_date' => $timeNow,
        'modify_date' => $timeNow
      ));
      $success = $this->process_insert($rMessage, $rReturn, $recordRows, array_merge($params, array(
        'groupName' => $groupName
      )));
      
      if($success == true) $rMessage = self::lang('ADD_SUCCESS');
      
      return $success;
    }

    public function update_rows(&$rMessage, &$rReturn, $recordRows, $recordId, $groupName = '', $params = array()) {
      $rMessage = '';
      $recordRows = array_merge($recordRows, array(
        'modify_date' => date('Y-m-d H:i:s')
      ));
      $success = $this->process_update($rMessage, $rReturn, $recordRows, $recordId, array_merge($params, array(
        'groupName' => $groupName
      )));

      if($success == true) $rMessage = self::lang('EDIT_SUCCESS');

      return $success;
    }

    public function delete_rows(&$rMessage, &$rReturn, $recordId, $params = array()) {
      $that = $this;
      $rMessage = '';

      if(!isset($params['checkCallable']) || !is_callable($params['checkCallable'])) {
        $params['checkCallable'] = function(&$rMessage, $inputRows, $dataRows, $langDataArray = array()) use($that) {
          $total = $that->get_rows($totalRows, '', array(Admin_DAO_Node::COLUMN_PARENT_ID => $inputRows[Admin_DAO_Node::COLUMN_RECORD_ID], 'isCount' => true)) ? $totalRows['total'] : 0;

          if($total > 0) {
            $rMessage = Admin_DAO_Node::lang('REMOVE_ERROR');
            return false;
          } else {
            return true;
          }
        };
      }

      $success = $this->process_delete($rMessage, $rReturn, $recordId, $params);
      
      if($success == true) $rMessage = self::lang('REMOVE_SUCCESS');
      
      return $success;
    }
  }
?>