<?php
  class Admin_DAO_SoundAlbum extends Admin_DAO {
    const TABLE_NAME = 'sound_album';
    const COLUMN_RECORD_ID = 'id';
    const COLUMN_HEADLINE = 'headline';
    const COLUMN_STATUS = 'status';
    const GROUP_ADD = 'add';
    const GROUP_EDIT = 'edit';
    const GROUP_LIST = 'list';
    const GROUP_OPTION = 'option';
    const GROUP_STATUS = 'status';

    static public function lang($key) {
      return CM_Lang::line("DAO_SoundAlbum.{$key}");
    }

    public function __construct($daoName) {
      parent::__construct($daoName, self::COLUMN_RECORD_ID, self::TABLE_NAME);
      //載入自定義欄位
      //$this->init_column('columnName', 'columnTable', array('params1' => '1'), 'columnDefault');
      //載入自定義欄位群組
      //$this->set_column_group('groupName', array('column1', 'column2', 'column3'));
      //載入自定義表單群組
      //$this->set_form_group('groupName', array('column1', 'column2', 'column3'));
      $this->set_options('status', array(
        array('id' => '1', 'text' => self::lang('COLUMN_STATUS_ENABLE')),
        array('id' => '2', 'text' => self::lang('COLUMN_STATUS_DISABLE'))
      ));
	  
	   //新增語言
	  $this->set_options('attr_2', array(
		array('id' => '0', 'text' => self::lang('COLUMN_LANG_0')),
        array('id' => '1', 'text' => self::lang('COLUMN_LANG_1')),
        array('id' => '2', 'text' => self::lang('COLUMN_LANG_2')),
		array('id' => '3', 'text' => self::lang('COLUMN_LANG_3'))
      ));
	  
	  //新增授權使用
	  $this->set_options('login_status', array(
        array('id' => '1', 'text' => self::lang('COLUMN_LOGIN_STATUS_1')),
        array('id' => '2', 'text' => self::lang('COLUMN_LOGIN_STATUS_2'))
      ));
	  
      $this->set_column(self::COLUMN_STATUS, array(
        'f_options' => $this->get_options('status'),
        'f_class' => 'input-small'
      ));
	  
	  $this->set_column('attr_2', array(
        'f_options' => $this->get_options('attr_2'),
        'f_class' => 'input-small'
      ));
	  
	  $this->set_column('login_status', array(
        'f_options' => $this->get_options('login_status'),
        'f_class' => 'input-xlarge'
      ));
	  
    }
    
    protected function get_sql($groupName, $params = array()) {
      if($params['isCount'] == true) {
        //取得筆數
        $sqlColumn = sprintf('COUNT(`%s`) AS `total`', self::COLUMN_RECORD_ID);
        $sqlLimit = '0, 1';
      } else {
        //欄位群組和一定會取出的資料
        $sqlColumn = '`' . implode('`, `', array_merge(
          array(self::COLUMN_RECORD_ID),
          array_keys($this->get_column_group($groupName))
        )) . '`';
        $sqlLimit = $params['isLimit'] == true ? sprintf('%d, %d', $params['startIndex'], $params['endIndex']) : '';
      }
      //如果有代入RecordId，解決排序的問題
      $this->process_recordId_sort($params, sprintf('`%s`', self::COLUMN_RECORD_ID));
      $sqlWhere = $this->get_sql_where($params);
      $sqlSort = !empty($params['sort']) ? $params['sort'] : '`id` DESC';
      return array(sprintf("SELECT %s FROM `%s`%s ORDER BY %s%s",
        $sqlColumn,
        self::TABLE_NAME,
        !empty($sqlWhere['sql']) ? ' WHERE ' . implode(' AND ', $sqlWhere['sql']) : '',
        $sqlSort,
        !empty($sqlLimit) ? ' LIMIT ' . $sqlLimit : ''
      ), $sqlWhere['params']);
    }
    
    protected function get_sql_where(array $params) {
      $return = $this->sql_where_default(sprintf('`%s`', self::COLUMN_RECORD_ID), $params);

      if(isset($params['status']) && !empty($params['status'])) {
        $return['sql'][] = sprintf("`%s` = :status", self::COLUMN_STATUS);
        $return['params'][':status'] = $params['status'];
      }
      //關鍵字條件
      if(isset($params['keyword']) && !empty($params['keyword'])) {
        $return['sql'][] = sprintf("`%s` LIKE :keyword", self::COLUMN_HEADLINE);
        $return['params'][':keyword'] = "%{$params['keyword']}%";
      }
	  
	  //發行出版日區間(很難換算= =)
	  $releasedateSql='';
	  if(!empty($params['releaseDate']['start']) || !empty($params['releaseDate']['end']) ){
		
		if(isset($params['releaseDate']['start']) && !empty($params['releaseDate']['start'])) {
			$releasedateSql.=" Date(`p`.`release_date`) >= '".$params['releaseDate']['start']."'";
		}
		if(!empty($params['releaseDate']['start']) && !empty($params['releaseDate']['end']) ){
			$releasedateSql.=" AND ";
		}
		if(isset($params['releaseDate']['end']) && !empty($params['releaseDate']['end']) ) {
			$releasedateSql.=" Date(`p`.`release_date`) <= '".$params['releaseDate']['end']."' ";
		}
		$return['sql'][] = $releasedateSql;
		//$return['conn'][] = "{$params['datetype']}";
	  }
      
      return $return;
    }

    public function init_options($allowCatalog) {
      $catalogDAO = Admin_Factory::createCMDAO('catalog_Sound');
      $catalogOptions = array();
      $catalogAll = in_array('-1', $allowCatalog);

      if($catalogDAO->get_array_tree_childrens($catalogArray, $catalogIdArray, Admin_DAO_Catalog_Sound::GROUP_LIST, '0')) {
        foreach($catalogArray as $catalogId => $catalogRows) {
          if($catalogAll == true || in_array($catalogId, $allowCatalog)) {
            $catalogOptions[] = array('id' => $catalogId, 'text' => implode('&nbsp;&raquo;&nbsp;', array_map(function($element) {
              return sprintf('[%s]%s', $element[Admin_DAO_Catalog_Sound::COLUMN_RECORD_ID], $element[Admin_DAO_Catalog_Sound::COLUMN_HEADLINE]);
            }, Admin_DAO_Catalog_Sound::get_catalog_parents($catalogId, $catalogArray))));  
          }
        }
      }

      $this->set_options('catalog', $catalogOptions);
      $this->set_column('catalog', array(
        'f_attr' => array(
          'style' => 'width: 90%'
        ),
        'f_options' => $this->get_options('catalog')
      ));
    }
    
    public function insert_rows(&$rMessage, &$rReturn, $recordRows, $groupName = '', $params = array()) {      
      $rMessage = '';
      $timeNow = date('Y-m-d H:i:s');
      $recordRows = array_merge($recordRows, array(
        'create_date' => $timeNow,
        'modify_date' => $timeNow
      ));
      $success = $this->process_insert($rMessage, $rReturn, $recordRows, array_merge($params, array(
        'groupName' => $groupName
      )));
      
      if($success == true) $rMessage = self::lang('ADD_SUCCESS');
      
      return $success;
    }

    public function update_rows(&$rMessage, &$rReturn, $recordRows, $recordId, $groupName = '', $params = array()) {
      $rMessage = '';
      $recordRows = array_merge($recordRows, array(
        'modify_date' => date('Y-m-d H:i:s')
      ));
      $success = $this->process_update($rMessage, $rReturn, $recordRows, $recordId, array_merge($params, array(
        'groupName' => $groupName
      )));

      if($success == true) $rMessage = self::lang('EDIT_SUCCESS');

      return $success;
    }

    public function delete_rows(&$rMessage, &$rReturn, $recordId, $params = array()) {
      $rMessage = '';
      $success = $this->process_delete($rMessage, $rReturn, $recordId, $params);
      
      if($success == true) {
        $dao = Admin_Factory::createCMDAO('sound');
        $errorMessage = array();

        if($dao->get_array($soundArray, '', array('albumId' => $recordId))) {
          foreach($soundArray as $soundRows) {
            if(!$dao->delete_rows($eMessage, $rReturn, $soundRows[Admin_DAO_Sound::COLUMN_RECORD_ID])) $errorMessage[] = $eMessage;
          }
        }

        if(empty($errorMessage)) {
          $rMessage = self::lang('REMOVE_SUCCESS'); 
        } else {
          $success = false;
          $rMessage = implode('<br />', $errorMessage);
        }
      }
      
      return $success;
    }
  }
?>