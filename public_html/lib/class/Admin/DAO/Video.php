<?php
  class Admin_DAO_Video extends Admin_DAO {
    const TABLE_NAME = 'video';
    const TABLE_LANG_NAME = 'video_lang';
    const COLUMN_RECORD_ID = 'id';
    const COLUMN_HEADLINE = 'headline';
    const COLUMN_STATUS = 'status';
    const GROUP_ADD = 'add';
    const GROUP_EDIT = 'edit';
    const GROUP_LIST = 'list';
	const GROUP_LIST_DATA = 'list_data';
    const GROUP_STATUS = 'status';
    const GROUP_OPTION = 'option';
    const GROUP_PROCESS = 'process';
    const GROUP_ADD_LIST = 'add_list';
    const GROUP_CATALOG = 'catalog';
    const GROUP_EXPORT = 'export';
    const GROUP_ORG_ADD = 'org_add';
	const GROUP_ADD_MASS = 'add_mass';

    static public function lang($key) {
      return CM_Lang::line("DAO_Video.{$key}");
    }
    
    public function __construct($daoName) {
      //設定資料庫別名
      $this->set_alias(self::TABLE_NAME, 'p');
      $this->set_alias(self::TABLE_LANG_NAME, 'pd');

      parent::__construct($daoName, self::COLUMN_RECORD_ID, self::TABLE_NAME, array(
        'tableLang' => true,
        'tableLangName' => self::TABLE_LANG_NAME,
        'tableLangColumn' => 'lang'
      ));
      //載入自定義欄位
      //$this->init_column('columnName', 'columnTable', array('params1' => '1'), 'columnDefault');
      //載入自定義欄位群組
      $this->set_column_group(self::GROUP_CATALOG, array('catalog', 'modify_date'));
      //$this->set_column_group(self::GROUP_EXPORT, array(self::COLUMN_RECORD_ID, self::COLUMN_HEADLINE, 'desc', 'author', 'release', 'file', 'file_img', 'catalog', 'copyright', 'copyright_status', 'start_date', 'end_date', 'release_date', 'video_original', 'owner', 'status', 'login_status', 'create_date', 'modify_date'));
	  //150209:ANN:修正輸出欄位
	  $this->set_column_group(self::GROUP_EXPORT, array(self::COLUMN_RECORD_ID, self::COLUMN_HEADLINE,'headline_sub_4','headline_sub_3','headline_sub_2','desc', 'author','headline_sub_1', 'release', 'file', 'file_img', 'catalog', 'copyright', 'copyright_status', 'start_date', 'end_date', 'release_date','attr_1','attr_2', 'attr_4', 'video_original', 'owner', 'status', 'login_status', 'create_date', 'modify_date'));
      //150626:ANN:不轉檔寫入欄位修正
	  //$this->set_column_group(self::GROUP_ORG_ADD, array('file', 'file_img', 'catalog', 'copyright', 'copyright_status', 'start_date', 'end_date', 'release_date', 'attr_1', 'attr_2', 'attr_3', 'process_ok', 'process_count', 'video_original', 'owner', 'status', 'login_status', 'create_date', 'modify_date', self::COLUMN_HEADLINE, 'headline_sub_1', 'headline_sub_2', 'headline_sub_3', 'desc', 'author', 'release', 'tags', 'lang_status'));
	  $this->set_column_group(self::GROUP_ORG_ADD, array('file', 'file_img', 'catalog', 'start_date', 'end_date', 'release_date', 'attr_1', 'attr_2', 'video_original', 'owner', 'status', 'login_status', 'create_date', 'modify_date', self::COLUMN_HEADLINE, 'headline_sub_1', 'headline_sub_2', 'headline_sub_3', 'desc', 'author', 'release', 'lang_status','attr_4','headline_sub_4','process_ok','video_length','file_size'));
      
	  //$this->set_column_group('groupName', array('column1', 'column2', 'column3'));
	  //修改需要欄位
	  //$this->set_column_group(self::GROUP_ADD, array('file', 'file_img', 'catalog', 'start_date', 'end_date', 'release_date', 'attr_1', 'attr_2', 'video_original', 'owner', 'status', 'login_status', 'create_date', 'modify_date', self::COLUMN_HEADLINE, 'headline_sub_1', 'headline_sub_2', 'headline_sub_3', 'desc', 'author', 'release', 'lang_status','attr_4','headline_sub_4'));
	  $this->set_column_group(self::GROUP_ADD, array('catalog', 'start_date', 'end_date', 'release_date', 'attr_1', 'attr_2', 'video_original', 'owner', 'status', 'login_status', 'create_date', 'modify_date', self::COLUMN_HEADLINE, 'headline_sub_1', 'headline_sub_2', 'headline_sub_3', 'desc', 'author', 'release', 'lang_status','attr_4','headline_sub_4'));
	  $this->set_column_group(self::GROUP_ADD_MASS, array('file', 'file_img', 'catalog', 'start_date', 'end_date', 'release_date', 'attr_1', 'attr_2', 'video_original', 'owner', 'status', 'login_status', 'create_date', 'modify_date', self::COLUMN_HEADLINE, 'headline_sub_1', 'headline_sub_2', 'headline_sub_3', 'desc', 'author', 'release', 'lang_status','attr_4','headline_sub_4'));
	  $this->set_column_group(self::GROUP_EDIT, array('file', 'file_img', 'catalog', 'start_date', 'end_date', 'release_date', 'attr_1', 'attr_2', 'video_original', 'status', 'login_status', 'modify_date', self::COLUMN_HEADLINE, 'headline_sub_1', 'headline_sub_2', 'headline_sub_3', 'desc', 'author', 'release', 'lang_status','attr_4','headline_sub_4'));
	  
      //載入自定義表單群組
      //$this->set_form_group('groupName', array('column1', 'column2', 'column3'));
	  //修改順序
	  //$this->set_form_group('info', array('file', 'file_img', self::COLUMN_HEADLINE, 'headline_sub_4', 'headline_sub_3', 'headline_sub_2', 'desc', 'author', 'headline_sub_1', 'release'));
	  //$this->set_form_group('setting', array('catalog', 'download_status', 'start_date', 'end_date', 'release_date', 'attr_1', 'attr_2', 'video_original', 'status', 'login_status', 'attr_4', 'lang_status'));
	  
	  $this->set_form_group('add_info', array(self::COLUMN_HEADLINE, 'headline_sub_4', 'headline_sub_3', 'headline_sub_2', 'desc', 'author', 'headline_sub_1', 'release'));
      $this->set_form_group('add_setting', array('catalog', 'download_status', 'start_date', 'end_date', 'release_date', 'attr_1', 'attr_2', 'video_original', 'login_status','attr_4'));
      $this->set_form_group('add_video', array('file', 'file_img', 'status', 'lang_status'));
	  
	  //新增語言
	  $this->set_options('attr_2', array(
        array('id' => '1', 'text' => self::lang('COLUMN_LANG_1')),
        array('id' => '2', 'text' => self::lang('COLUMN_LANG_2')),
		array('id' => '3', 'text' => self::lang('COLUMN_LANG_3'))
      ));
	  
      $this->set_options('status', array(
        array('id' => '1', 'text' => self::lang('COLUMN_STATUS_ENABLE')),
        array('id' => '2', 'text' => self::lang('COLUMN_STATUS_DISABLE'))
      ));
      $this->set_options('login_status', array(
        array('id' => '1', 'text' => self::lang('COLUMN_LOGIN_STATUS_1')),
        array('id' => '2', 'text' => self::lang('COLUMN_LOGIN_STATUS_2'))
      ));
      $this->set_options('copyright_status', array(
        array('id' => '1', 'text' => self::lang('COLUMN_COPYRIGHT_STATUS_1')),
        array('id' => '2', 'text' => self::lang('COLUMN_COPYRIGHT_STATUS_2'))
      ));
      $this->set_options('video_original', array(
        array('id' => '1', 'text' => self::lang('COLUMN_VIDEO_ORIGINAL_1')),
        array('id' => '2', 'text' => self::lang('COLUMN_VIDEO_ORIGINAL_2'))
      ));
      $this->set_options('process_ok', array(
        array('id' => '2', 'text' => self::lang('COLUMN_PROCESS_OK_2')),
        array('id' => '3', 'text' => self::lang('COLUMN_PROCESS_OK_3')),
        array('id' => '4', 'text' => self::lang('COLUMN_PROCESS_OK_4'))
      ));
	  
	  $this->set_column('attr_2', array(
        'f_options' => $this->get_options('attr_2'),
        'f_class' => 'input-small'
      ));

      $this->set_column(self::COLUMN_STATUS, array(
        'f_options' => $this->get_options('status'),
        'f_class' => 'input-small'
      ));
      $this->set_column('login_status', array(
        'f_options' => $this->get_options('login_status'),
        'f_class' => 'input-xlarge'
      ));
      $this->set_column('lang_status', array(
        'f_options' => $this->get_options('status'),
        'f_class' => 'input-small'
      ));
      $this->set_column('copyright_status', array(
        'f_options' => $this->get_options('copyright_status'),
        'f_class' => 'input-small'
      ));
      $this->set_column('video_original', array(
        'f_options' => $this->get_options('video_original'),
        'f_class' => 'input-small'
      ));
      $this->set_column('process_ok', array(
        'f_options' => $this->get_options('process_ok'),
        'f_class' => 'input-small'
      ));
    }

    protected function get_sql($groupName, $params = array()) {
      if($params['isCount'] == true) {
        //取得筆數
        $sqlColumn = sprintf('COUNT(`p`.`%s`) AS `total`', self::COLUMN_RECORD_ID);
      } else {
        $columnArray = $this->get_column_group($groupName);
        $sqlColumn = array();
        //如果索引沒有在列表內則新增
        if(!array_key_exists(self::COLUMN_RECORD_ID, $columnArray)) $sqlColumn[] = $this->get_column(self::COLUMN_RECORD_ID)->get_full_name($this->_table_alias);

        if(!array_key_exists('type', $columnArray)) $sqlColumn[] = $this->get_column('type')->get_full_name($this->_table_alias);
        
        foreach($this->get_column_group($groupName) as $columnName => $column) $sqlColumn[] = $column->get_full_name($this->_table_alias);
        
        $sqlColumn = implode(', ', $sqlColumn);
      }

      if($params['isCount'] != true) {
        $sqlColumn .= sprintf(', `p`.`%s` AS `_pkey`', Admin_DAO_Video::COLUMN_RECORD_ID);

        if(isset($params['viewCount']) && is_array($params['viewCount'])) {
          $sqlWhere = '';

          if(isset($params['viewCustomersId']) && !empty($params['viewCustomersId'])) $sqlWhere .= sprintf(" AND `iUsr_seq` = %d", $params['viewCustomersId']);

          if(!empty($params['viewCount']['start']) || !empty($params['viewCount']['end'])) {
            $start = isset($params['viewCount']['start']) && !empty($params['viewCount']['start']) ? $params['viewCount']['start'] : '1970-01-01';
            $end = isset($params['viewCount']['end']) && !empty($params['viewCount']['end']) ? $params['viewCount']['end'] : date('Y-m-d');
            $sqlWhere .= " AND `tTimestamp` BETWEEN '{$start} 00:00:00' AND '{$end} 23:59:59'";
          }

          $sqlColumn .= sprintf(", (SELECT COUNT(`%s`) FROM `%s` WHERE `iMda_seq` = `_pkey`{$sqlWhere}) AS `viewCount`",
            Admin_DAO_Stream_Log::COLUMN_RECORD_ID,
            Admin_DAO_Stream_Log::TABLE_NAME
          );
        }

        if(isset($params['viewTime']) && is_array($params['viewTime'])) {
          $sqlWhere = '';

          if(isset($params['viewCustomersId']) && !empty($params['viewCustomersId'])) $sqlWhere .= sprintf(" AND `iUsr_seq` = %d", $params['viewCustomersId']);

          if(!empty($params['viewTime']['start']) || !empty($params['viewTime']['end'])) {
            $start = isset($params['viewTime']['start']) && !empty($params['viewTime']['start']) ? $params['viewTime']['start'] : '1970-01-01';
            $end = isset($params['viewTime']['end']) && !empty($params['viewTime']['end']) ? $params['viewTime']['end'] : date('Y-m-d');
            $sqlWhere .= " AND `tTimestamp` BETWEEN '{$start} 00:00:00' AND '{$end} 23:59:59'";
          }

          $sqlColumn .= sprintf(", (SELECT SUM(`iConnected_time`) FROM `%s` WHERE `iMda_seq` = `_pkey`{$sqlWhere}) AS `viewTime`",
            Admin_DAO_Stream_Log::TABLE_NAME
          );
        }

        if(isset($params['subSort']) && !empty($params['subSort'])) $params['sort'] = $params['subSort'];
      }
      //如果有代入RecordId，解決排序的問題
      $this->process_recordId_sort($params, sprintf('`p`.`%s`', self::COLUMN_RECORD_ID));
      $sqlWhere = $this->get_sql_where($params);
      $sqlLimit = $params['isLimit'] == true ? sprintf('%d, %d', $params['startIndex'], $params['endIndex']) : '';
      $sqlSort = !empty($params['sort']) ? $params['sort'] : sprintf('`p`.`%s` DESC', self::COLUMN_RECORD_ID);
      
      return array(sprintf("SELECT %s FROM `%s` AS `p` LEFT JOIN `%s` AS `pd` ON `p`.`%s` = `pd`.`%s` AND `pd`.`lang` = %d%s ORDER BY %s%s",
        $sqlColumn,
        self::TABLE_NAME,
        self::TABLE_LANG_NAME,
        self::COLUMN_RECORD_ID,
        self::COLUMN_RECORD_ID,
        isset($params['lang']) ? $params['lang'] : CM_Lang::get_current_id(true),
        !empty($sqlWhere['sql']) ? ' WHERE ' . implode(' AND ', $sqlWhere['sql']) : '',
        $sqlSort,
        !empty($sqlLimit) ? ' LIMIT ' . $sqlLimit : ''
      ), $sqlWhere['params']) ;
    }
    
    protected function get_sql_where(array $params) {
      $return = $this->sql_where_default(sprintf('`p`.`%s`', self::COLUMN_RECORD_ID), $params);
      //狀態條件
      if(isset($params['status']) && !empty($params['status'])) {
        $return['sql'][] = sprintf("`p`.`%s` = :status", self::COLUMN_STATUS);
        $return['params'][':status'] = $params['status'];
      }

      if(isset($params['allowCatalog']) && Admin::not_empty_array($params['allowCatalog']) && in_array('-1', $params['allowCatalog']) == false) {
        $return['sql'][] = sprintf("(`p`.`catalog` = '' OR %s)", $this->sql_where_find_in_set($params['allowCatalog'], "`p`.`catalog`"));
      }

      if(isset($params['catalog']) && !empty($params['catalog'])) {
        $return['sql'][] = "FIND_IN_SET(:catalog, `p`.`catalog`)";
        $return['params'][':catalog'] = $params['catalog'];
      }
      //版權宣告
      if(isset($params['copyright']) && !empty($params['copyright'])) {
        $return['sql'][] = "`p`.`copyright` = :copyright";
        $return['params'][':copyright'] = $params['copyright'];
      }
      //檔案名稱
      if(isset($params['file']) && !empty($params['file'])) {
        $return['sql'][] = "`p`.`file` = :file";
        $return['params'][':file'] = $params['file'];
      }
      //關鍵字條件
      if(isset($params['keyword']) && !empty($params['keyword'])) {
        /*$return['sql'][] = sprintf("(`pd`.`%s` LIKE :keyword1 OR `pd`.`desc` LIKE :keyword2)", self::COLUMN_HEADLINE);
        $return['params'][':keyword1'] = "%{$params['keyword']}%";
        $return['params'][':keyword2'] = "%{$params['keyword']}%";*/
		//150209:ANN:它加了一堆條件...影片名稱、相關題名、系列名、關鍵字、影片說明、作者、其他參與者、出版者/主辦單位、影片來源
		//$return['sql'][] = sprintf("(`pd`.`%s` LIKE :keyword1 OR `pd`.`desc` LIKE :keyword2 OR `p`.`attr_4` LIKE :keyword2 OR `pd`.`headline_sub_3` LIKE :keyword2 OR `pd`.`headline_sub_2` LIKE :keyword2 OR `pd`.`desc` LIKE :keyword2 OR `pd`.`author` LIKE :keyword2 OR `pd`.`headline_sub_1` LIKE :keyword2 OR `pd`.`release` LIKE :keyword2 OR `pd`.`headline_sub_4` LIKE :keyword2)", self::COLUMN_HEADLINE);
		$return['sql'][] = sprintf("(`pd`.`%s` LIKE :keyword1 OR `pd`.`desc` LIKE :keyword2 OR `p`.`attr_4` LIKE :keyword3 OR `pd`.`headline_sub_3` LIKE :keyword4 OR `pd`.`headline_sub_2` LIKE :keyword5 OR `pd`.`desc` LIKE :keyword6 OR `pd`.`author` LIKE :keyword7 OR `pd`.`headline_sub_1` LIKE :keyword8 OR `pd`.`release` LIKE :keyword9 OR `pd`.`headline_sub_4` LIKE :keyword10)", self::COLUMN_HEADLINE);
		$return['params'][':keyword1'] = "%{$params['keyword']}%";
        $return['params'][':keyword2'] = "%{$params['keyword']}%";
		$return['params'][':keyword3'] = "%{$params['keyword']}%";
		$return['params'][':keyword4'] = "%{$params['keyword']}%";
		$return['params'][':keyword5'] = "%{$params['keyword']}%";
		$return['params'][':keyword6'] = "%{$params['keyword']}%";
		$return['params'][':keyword7'] = "%{$params['keyword']}%";
		$return['params'][':keyword8'] = "%{$params['keyword']}%";
		$return['params'][':keyword9'] = "%{$params['keyword']}%";
		$return['params'][':keyword10'] = "%{$params['keyword']}%";
      }
	  //語言
      if(isset($params['attr_2']) && !empty($params['attr_2'])) {
        $return['sql'][] = "`p`.`attr_2` = :attr_2";
        $return['params'][':attr_2'] = $params['attr_2'];
      }
	  //授權使用範圍
      if(isset($params['login_status']) && !empty($params['login_status'])) {
        $return['sql'][] = "`p`.`login_status` = :login_status";
        $return['params'][':login_status'] = $params['login_status'];
      }
	  
	  //上架日期區間
	  $dateSql='';
	  if(!empty($params['date']['start']) || !empty($params['date']['end']) ){
		
		if(isset($params['date']['start']) && !empty($params['date']['start'])) {
			$dateSql.=" Date(`p`.`start_date`) >= '".$params['date']['start']."'";
		}
		if(!empty($params['date']['start']) && !empty($params['date']['end']) ){
			$dateSql.=" AND ";
		}
		if(isset($params['date']['end']) && !empty($params['date']['end']) ) {
			$dateSql.=" Date(`p`.`start_date`) <= '".$params['date']['end']."' ";
		}
		$return['sql'][] = $dateSql;
		//$return['conn'][] = "{$params['datetype']}";
		
	  }
	  
	   //發行出版日區間(很難換算= =)
	  $releasedateSql='';
	  if(!empty($params['releaseDate']['start']) || !empty($params['releaseDate']['end']) ){
		
		if(isset($params['releaseDate']['start']) && !empty($params['releaseDate']['start'])) {
			$releasedateSql.=" Date(`p`.`release_date`) >= '".$params['releaseDate']['start']."'";
		}
		if(!empty($params['releaseDate']['start']) && !empty($params['releaseDate']['end']) ){
			$releasedateSql.=" AND ";
		}
		if(isset($params['releaseDate']['end']) && !empty($params['releaseDate']['end']) ) {
			$releasedateSql.=" Date(`p`.`release_date`) <= '".$params['releaseDate']['end']."' ";
		}
		$return['sql'][] = $releasedateSql;
		//$return['conn'][] = "{$params['datetype']}";
	  }
	  
	  //print_r($return);	  
      
      return $return;
    }
    
    public function init_options($allowCatalog) {
      $catalogDAO = Admin_Factory::createCMDAO('catalog_Video');
      $catalogOptions = array();
      $catalogAll = in_array('-1', $allowCatalog);

      if($catalogDAO->get_array_tree_childrens($catalogArray, $catalogIdArray, Admin_DAO_Catalog_Video::GROUP_LIST, '0')) {
        foreach($catalogArray as $catalogId => $catalogRows) {
          if($catalogAll == true || in_array($catalogId, $allowCatalog)) {
            $catalogOptions[] = array('id' => $catalogId, 'text' => implode('&nbsp;&raquo;&nbsp;', array_map(function($element) {
              return sprintf('[%s]%s', $element[Admin_DAO_Catalog_Video::COLUMN_RECORD_ID], $element[Admin_DAO_Catalog_Video::COLUMN_HEADLINE]);
            }, Admin_DAO_Catalog_Video::get_catalog_parents($catalogId, $catalogArray))));  
          }
        }
      }

      $this->set_options('catalog', $catalogOptions);
      $this->set_column('catalog', array(
        'f_attr' => array(
          'style' => 'width: 90%'
        ),
        'f_options' => $this->get_options('catalog')
      ));
      $this->set_options('copyright', array_map(function($element) {
        return array(
          'id' => $element[Admin_DAO_Copyright::COLUMN_RECORD_ID],
          'text' => $element[Admin_DAO_Copyright::COLUMN_HEADLINE]
        );
      }, Admin_Factory::createCMDAO('copyright')->get_array($recordArray, Admin_DAO_Copyright::GROUP_LIST) ? $recordArray : array()) );
      $this->set_column('copyright', array(
        'f_class' => 'input-large',
        'f_options' => $this->get_options('copyright')
      ));
      $this->get_column('file')->set_column(array('fu_params' => array('prefix' => 'video')));
    }

    public function insert_rows(&$rMessage, &$rReturn, $recordRows, $groupName = '', $params = array()) {
      $rMessage = '';
      $timeNow = date('Y-m-d H:i:s');
      $recordRows = array_merge($recordRows, array(
        'create_date' => $timeNow,
        'modify_date' => $timeNow
      ));
      $success = $this->process_insert($rMessage, $rReturn, $recordRows, array_merge($params, array(
        'groupName' => $groupName
      )));
      
      if($success == true) $rMessage = self::lang('ADD_SUCCESS');
      
      return $success;
    }

    public function update_rows(&$rMessage, &$rReturn, $recordRows, $recordId, $groupName = '', $params = array()) {
      $rMessage = '';
      $recordRows = array_merge($recordRows, array(
        'modify_date' => date('Y-m-d H:i:s')
      ));
      $success = $this->process_update($rMessage, $rReturn, $recordRows, $recordId, array_merge($params, array(
        'groupName' => $groupName
      )));

      if($success == true) $rMessage = self::lang('EDIT_SUCCESS');

      return $success;
    }

    public function delete_rows(&$rMessage, &$rReturn, $recordId, $params = array()) {
      $rMessage = '';
      $success = $this->process_delete($rMessage, $rReturn, $recordId, $params);
      
      if($success == true) $rMessage = self::lang('REMOVE_SUCCESS');
      
      return $success;
    }
  }
?>