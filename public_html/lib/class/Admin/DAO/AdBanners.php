<?php
  class Admin_DAO_AdBanners extends Admin_DAO {
    const TABLE_NAME = 'adbanners';
    const TABLE_LANG_NAME = 'adbanners_lang';
    const COLUMN_RECORD_ID = 'id';
    const COLUMN_HEADLINE = 'headline';
    const COLUMN_STATUS = 'status';
    const COLUMN_CREATE_DATE = 'create_date';
    const COLUMN_MODIFY_DATE = 'modify_date';
    const GROUP_ADD = 'add';
    const GROUP_EDIT = 'edit';
    const GROUP_LIST = 'list';
    const GROUP_STATUS = 'status';
    const GROUP_BANNER = 'banner';
    const GROUP_SORT_EDIT = 'sort_edit';
    const GROUP_SORT_LOG = 'sort_log';

    static public function lang($key) {
      return CM_Lang::line("DAO_AdBanners.{$key}");
    }

    public function __construct($daoName) {
      //設定資料庫別名
      $this->set_alias(self::TABLE_NAME, 'p');
      $this->set_alias(self::TABLE_LANG_NAME, 'pd');
      
      parent::__construct($daoName, self::COLUMN_RECORD_ID, self::TABLE_NAME, array(
        'tableLang' => true,
        'tableLangName' => self::TABLE_LANG_NAME,
        'tableLangColumn' => 'lang'
      ));
      
      //版位
      $this->set_options('banner_section', array(
        array('id' => 1, 'cId' => 37, 'text' => '[900x160]' . self::lang('COLUMN_BANNER_SECTION_01')),
        array('id' => 2, 'cId' => 37, 'text' => '[210x705]' . self::lang('COLUMN_BANNER_SECTION_02'))
      ));
      $this->set_options('status', array(
        array('id' => '1', 'text' => self::lang('COLUMN_STATUS_ENABLE')),
        array('id' => '2', 'text' => self::lang('COLUMN_STATUS_DISABLE'))
      ));
      $this->set_options('target', array(
        array('id' => '1', 'text' => self::lang('COLUMN_TARGET_1')),
        array('id' => '2', 'text' => self::lang('COLUMN_TARGET_2'))
      ));
      $this->set_column(self::COLUMN_STATUS, array('f_options' => $this->get_options('status')));
      $this->set_column('lang_status', array('f_options' => $this->get_options('status')));
      $this->set_column('target', array('f_options' => $this->get_options('target')));
      $this->set_column('banner_section', array('f_options' => $this->get_options('banner_section')));
      //載入自定義欄位
      //$this->init_column('columnName', 'columnTable', array('params1' => '1'), 'columnDefault');
      //載入自定義欄位群組
      //$this->set_column_group('groupName', array('column1', 'column2', 'column3'));
      //載入自定義表單群組
      //$this->set_form_group('groupName', array('column1', 'column2', 'column3'));
    }

    protected function get_sql($groupName, $params = array()) {
      if($params['isCount'] == true) {
        //取得筆數
        $sqlColumn = sprintf('COUNT(`p`.`%s`) AS `total`', self::COLUMN_RECORD_ID);
      } else {
        $columnArray = $this->get_column_group($groupName);
        $sqlColumn = array();
        //如果索引沒有在列表內則新增
        if(!array_key_exists(self::COLUMN_RECORD_ID, $columnArray)) $sqlColumn[] = $this->get_column(self::COLUMN_RECORD_ID)->get_full_name($this->_table_alias);
        
        foreach($this->get_column_group($groupName) as $columnName => $column) $sqlColumn[] = $column->get_full_name($this->_table_alias);
        
        $sqlColumn = implode(', ', $sqlColumn);
      }
      //如果有代入RecordId，解決排序的問題
      $this->process_recordId_sort($params, sprintf('`p`.`%s`', self::COLUMN_RECORD_ID));
      $sqlWhere = $this->get_sql_where($params);
      $sqlLimit = $params['isLimit'] == true ? sprintf('%d, %d', $params['startIndex'], $params['endIndex']) : '';
      $sqlSort = !empty($params['sort']) ? $params['sort'] : sprintf('`p`.`sort` DESC, `p`.`%s` DESC', self::COLUMN_RECORD_ID);

      return array(sprintf("SELECT %s FROM `%s` AS `p` LEFT JOIN `%s` AS `pd` ON `p`.`%s` = `pd`.`%s` AND `pd`.`lang` = %d%s ORDER BY %s%s",
        $sqlColumn,
        self::TABLE_NAME,
        self::TABLE_LANG_NAME,
        self::COLUMN_RECORD_ID,
        self::COLUMN_RECORD_ID,
        isset($params['lang']) ? $params['lang'] : CM_Lang::get_current_id(true),
        !empty($sqlWhere['sql']) ? ' WHERE ' . implode(' AND ', $sqlWhere['sql']) : '',
        $sqlSort,
        !empty($sqlLimit) ? ' LIMIT ' . $sqlLimit : ''
      ), $sqlWhere['params']) ;
    }
    
    protected function get_sql_where(array $params) {
      $return = $this->sql_where_default(sprintf('`p`.`%s`', self::COLUMN_RECORD_ID), $params);
      //指定目錄
      if(isset($params['catalog']) && !empty($params['catalog'])) {
        $return['sql'][] = "`p`.`catalog` = :catalog";
        $return['params'][':catalog'] = (int)$params['catalog'];
      }
      //指定多個目錄
      if(isset($params['catalogs'])) {
        $return['sql'][] = $this->sql_where_record_in($params['catalogs'], "`p`.`catalog`");
      }
      //版位條件
      if(isset($params['banner_section']) && !empty($params['banner_section'])) {
        $return['sql'][] = sprintf("`p`.`%s` = :banner_section", 'banner_section');
        $return['params'][':banner_section'] = $params['banner_section'];
      }
      //狀態條件
      if(isset($params['status']) && !empty($params['status'])) {
        $return['sql'][] = sprintf("`p`.`%s` = :status", self::COLUMN_STATUS);
        $return['params'][':status'] = $params['status'];
      }
      //關鍵字條件
      if(isset($params['keyword']) && !empty($params['keyword'])) {
        $return['sql'][] = sprintf("(`pd`.`%s` LIKE :keyword)", self::COLUMN_HEADLINE);
        $return['params'][':keyword'] = "%{$params['keyword']}%";
      }
      
      return $return;
    }

    public function get_catalog_array(&$returnArray, $catalogId, $groupName, $params = array()) {
      return $this->get_array($returnArray, $groupName, array_merge($params, array(
        'catalog' => $catalogId
      )));
    }

    public function get_catalog_rows(&$returnRows, $catalogId, $groupName, $params = array()) {
      return $this->get_rows($returnRows, $groupName, array_merge($params, array(
        'catalog' => $catalogId
      )));
    }
    
    public function insert_rows(&$rMessage, &$rReturn, $recordRows, $groupName = '', $params = array()) {
      $timeNow = date('Y-m-d H:i:s');
      $recordRows = array_merge($recordRows, array(
        'create_date' => $timeNow,
        'modify_date' => $timeNow
      ));
      $success = $this->process_insert($rMessage, $rReturn, $recordRows, array_merge($params, array('groupName' => $groupName)));
      
      if($success == true) $rMessage = self::lang('ADD_SUCCESS');
      
      return $success;
    }
    
    public function update_rows(&$rMessage, &$rReturn, $recordRows, $recordId, $groupName = '', $params = array()) {
      $recordRows = array_merge($recordRows, array(
        'modify_date' => date('Y-m-d H:i:s')
      ));
      $success = $this->process_update($rMessage, $rReturn, $recordRows, $recordId, array_merge($params, array('groupName' => $groupName)));

      if($success == true) $rMessage = self::lang('EDIT_SUCCESS');

      return $success;
    }
    
    public function delete_rows(&$rMessage, &$rReturn, $recordId, $params = array()) {
      $success = $this->process_delete($rMessage, $rReturn, $recordId, $params);
      
      if($success == true) $rMessage = self::lang('REMOVE_SUCCESS');
      
      return $success;
    }
  }
?>