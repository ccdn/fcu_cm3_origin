<?php
  class Admin_Form extends CM_Form {

    protected $_dao_conf = null;

    protected $_dao_rows = array();

    protected $_fileinfo_dao = null;
    
    public function set_form_dao(CM_DAO $dao, $groupName = '') {
      parent::set_form_dao($dao, $groupName);
      //加入DAO的設定
      $this->_dao_conf = $dao->get_dao_conf();
    }

    public function set_form_default($defaultRows) {
      parent::set_form_default($defaultRows);
      //如果有DAO的設定，存下特定的值
      if(!is_null($this->_dao_conf)) {
        $this->_dao_rows['recordId'] = isset($defaultRows[$this->_dao_conf['tablePKey']]) ? $defaultRows[$this->_dao_conf['tablePKey']] : '';

        if($this->_dao_conf['tableLang'] == true) $this->_dao_rows['lang'] = !isset($defaultRows[$this->_dao_conf['tableLangColumn']]) || empty($defaultRows[$this->_dao_conf['tableLangColumn']]) ? $defaultRows[$this->_dao_conf['tableLangColumn']] : CM_Lang::get_current_id(true);
      }
    }

    public function set_form_record_id($recordId) {
      $this->_record_id = $recordId;
    }

    protected function get_field_textarea($column, $attr, $params) {
      if(isset($column->fe_enable) && $column->fe_enable == true) {
        $attr['data-cmfield'] = 'editor';
        $attr['data-options'] = Admin::json_encode($column->fe_options);
      }
      
      return parent::get_field_textarea($column, $attr, $params);
    }
    /**
     * 取得檔案欄位
     */
    protected function get_field_file($column, $attr, $params) {
      $columnParams = array(
        'dao' => $column->dao,
        'column' => $column->name
      );
      $file = array();
	  
	  if(isset($this->_dao_rows['recordId']) && !empty($this->_dao_rows['recordId'])) $columnParams['rId'] = $this->_dao_rows['recordId'];

      if(!empty($column->val) && isset($this->_dao_rows['recordId']) && !empty($this->_dao_rows['recordId'])) {
        if(is_null($this->_fileinfo_dao)) $this->_fileinfo_dao = Admin_Factory::createCMDAO('file_Info');

        $columnParams['rId'] = $this->_dao_rows['recordId'];
        $daoParams = $columnParams;
        $daoParams['recordIds'] = explode(',', $column->val);

        if($this->_dao_conf['tableLang'] == true) $daoParams['lang'] = $this->_dao_rows['lang'];

        if($this->_fileinfo_dao->get_array($fileArray, '', $daoParams)) $file = Admin_Format::to_upload_file_list($fileArray);
      }
      //參數預設值設定
      $attr['data-options'] = Admin::json_encode(array_merge($column->fu_options, array(
        'acceptFileTypes' => "/(\.|\/)(" . str_replace(',', '|', $column->fu_options['acceptFileTypes']) . ")$/i"
      )));
      $attr['data-params'] = Admin::json_encode(array_merge($column->fu_params, $columnParams));
      $attr['data-url'] = $column->fu_url;
      $attr['data-cmfield'] = 'upload';

      if(!isset($column->f_form_params)) $column->f_form_params = array();

      $column->f_form_params['tip'] = implode('<br />', $this->get_file_field_tip($column->fu_options));
      
      return CM_Template::get_tpl_html('upload_column', array(
        'lang' => CM_Lang::export('Javascript'),
        'files' => $file,
        'upload_field' => $this->get_field_hidden($column, $attr, array())
      ));
    }
	 /**
     * 取得檔案欄位(串流)
     */
    protected function get_field_file_t($column, $attr, $params) {
      $columnParams = array(
        'dao' => $column->dao,
        'column' => $column->name
      );
      $file = array();

      if(isset($this->_dao_rows['recordId']) && !empty($this->_dao_rows['recordId'])) $columnParams['rId'] = $this->_dao_rows['recordId'];

      if(!empty($column->val) && isset($this->_dao_rows['recordId']) && !empty($this->_dao_rows['recordId'])) {
        if(is_null($this->_fileinfo_dao)) $this->_fileinfo_dao = Admin_Factory::createCMDAO('file_Info');

        $columnParams['rId'] = $this->_dao_rows['recordId'];
        $daoParams = $columnParams;
        $daoParams['recordIds'] = explode(',', $column->val);

        if($this->_dao_conf['tableLang'] == true) $daoParams['lang'] = $this->_dao_rows['lang'];

        if($this->_fileinfo_dao->get_array($fileArray, '', $daoParams)) $file = Admin_Format::to_upload_file_list($fileArray);
      }
      //參數預設值設定
      $attr['data-options'] = Admin::json_encode(array_merge($column->fu_options, array(
        'acceptFileTypes' => "/(\.|\/)(" . str_replace(',', '|', $column->fu_options['acceptFileTypes']) . ")$/i"
      )));
      $attr['data-params'] = Admin::json_encode(array_merge($column->fu_params, $columnParams));
      $attr['data-url'] = $column->fu_url;
      $attr['data-cmfield'] = 'upload';

      if(!isset($column->f_form_params)) $column->f_form_params = array();

      $column->f_form_params['tip'] = implode('<br />', $this->get_file_field_tip($column->fu_options));
      
      $dao = Admin_Factory::createCMDAO(strtolower($columnParams['dao']));
      
      foreach($file AS &$fileRows) {
        $fileNameArray = explode('video/', $fileRows['file']);
        $params = json_decode($fileRows['params'], true);
        
        if(!empty($fileNameArray[1]) && !empty($params['record_id']) && $dao->get_rows_byId($dataRows, '', $params['record_id'], array()) && $dataRows['process_count'] > 0) {
          $fileTNameArray = explode('.', $fileNameArray[1]);
          $fileName = CM_Conf::get('PATH_F.UPLOAD') . 'new_Content/' . $fileTNameArray[0] . '_' . $dataRows['process_count'] . '.' . $fileTNameArray[1];
          if(!is_file($fileName)) $fileRows['file'] = '';
        } else {
          $fileRows['file'] = '';
        }
      }
      return CM_Template::get_tpl_html('upload_column2', array(
        'lang' => CM_Lang::export('Javascript'),
        'files' => $file,
        'upload_field' => $this->get_field_hidden($column, $attr, array())
      ));
    }
    /**
     * 取得顯示欄位
     */
    protected function get_field_readonly($column, $attr, $params) {
      return $column->val;
    }
    /**
     * 取得簡易檔案欄位
     */
    protected function get_field_simple_file($column, $attr, $params) {
      $columnParams = array(
        'dao' => $column->dao,
        'column' => $column->name,
        '_request' => 'simple_upload',
        '_model' => 'core'
      );
      $file = array();
      
      if(!empty($column->val)) {
        if(is_null($this->_dao)) $this->_dao = Admin_Factory::createCMDAO($columnParams['dao']);

        $columnParams['rId'] = $this->_dao_rows['recordId'];
        $files = explode(',', $column->val);

        if($this->_dao_conf['tableLang'] == true) $columnParams['lang'] = $this->_dao_rows['lang'];

        $file = array_map(function($element) use($columnParams) {
          return array_shift(Admin_Format::to_simple_upload_file_list(array(array_merge($columnParams, array(
            'file' => $element
          )))));
        }, $files);

      }
      //參數預設值設定
      $attr['data-options'] = Admin::json_encode(array_merge($column->fu_options, array(
        'acceptFileTypes' => "/(\.|\/)(" . str_replace(',', '|', $column->fu_options['acceptFileTypes']) . ")$/i"
      )));
      $attr['data-params'] = Admin::json_encode(array_merge($column->fu_params, $columnParams));
      $attr['data-url'] = !empty($column->fu_url) ? $column->fu_url : Admin::href_link('/controller.php', array());
      $attr['data-cmfield'] = 'upload';

      if(!isset($column->f_form_params)) $column->f_form_params = array();

      $column->f_form_params['tip'] = implode('<br />', $this->get_file_field_tip($column->fu_options));
      
      return CM_Template::get_tpl_html('simpleUpload_column', array(
        'lang' => CM_Lang::export('Javascript'),
        'files' => $file,
        'upload_field' => $this->get_field_hidden($column, $attr, array())
      ));
    }
  }
?>