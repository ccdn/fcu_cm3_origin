<?php
  class Admin_Menu {
    static protected $_MOD;

    static public function lang($key) {
      return CM_Lang::line(__CLASS__ . '.' . $key);
    }
    
    static protected function load_mod($modName) {
      if(!isset(self::$_MOD[$modName])) {
        $modPath = CM_Conf::get('PATH_F.ADMIN_MOD') . $modName . '/' . $modName . '.mod.php';
        $modClassName = sprintf('mod%s', ucfirst($modName));
        
        if(!is_file($modPath)) return false;

        if(!class_exists($modClassName)) require($modPath);
        
        self::$_MOD[$modName] = Admin_Factory::createCMModule($modName);
      }

      return true;
    }

    static protected function to_menu_array($icon, $mod, $request, $action = '', $params = array(), $text = '', $sub = array()) {
      return array(
        'icon' => $icon,
        'text' => $text,
        'mod' => $mod,
        'request' => $request,
        'action' => $action,
        'params' => $params,
        'sub' => $sub
      );
    }

    static protected function get_menu_array() {
      $return = array();
      $return[] = self::to_menu_array('icofont-cogs', 'core', 'account_list', '', array(), '系統管理', array(
        self::to_menu_array('icofont-th-list', 'core', 'account_list'),
        self::to_menu_array('icofont-group', 'core', 'account_group_list'),
        self::to_menu_array('icofont-cog', 'core', 'configure'),
        self::to_menu_array('icofont-file', 'core', 'log'),
        self::to_menu_array('icofont-cloud', 'streamServer', 'list', '', array(), '串流伺服器'),
        self::to_menu_array('icofont-bar-chart', 'streamLog', 'default')
      ));
      //$return[] = self::to_menu_array('icofont-user', 'customers', 'default', '', array(), '', array(
        //self::to_menu_array('icofont-user', 'customers', 'list'),
        //self::to_menu_array('icofont-group', 'customers', 'group_list')
        // self::to_menu_array('icofont-envelope', 'contact', 'default')
        //self::to_menu_array('icofont-pushpin', 'orders', 'default')
      //));
      $return[] = self::to_menu_array('icofont-folder-close', 'catalogVideo', 'default', '', array(), '類別管理', array(
        /*self::to_menu_array('icofont-info-sign', 'copyright', 'default'),*/ //版權拿掉
        self::to_menu_array('icofont-folder-close', 'catalogVideo', 'default'),
        self::to_menu_array('icofont-folder-close', 'catalogSound', 'default'),
        /*self::to_menu_array('icofont-folder-close', 'catalogPicture', 'default')*/ //照片拿掉
      ));
	  $return[] = self::to_menu_array('icofont-music', 'soundAlbum', 'default');
      $return[] = self::to_menu_array('icofont-film', 'video', 'default');
      $return[] = self::to_menu_array('icofont-film', 'subject', 'video_carousel');
      //$return[] = self::to_menu_array('icofont-picture', 'adBanners', 'default');//廣告拿掉
	  //專題活動拿掉
      /*$return[] = self::to_menu_array('icofont-fire', 'subject', 'default', '', array(), '', array(
        self::to_menu_array('icofont-fire', 'subject', 'default'),
        self::to_menu_array('icofont-key', 'keyword', 'default')
      ));*/
	  $return[] = self::to_menu_array('icofont-key', 'keyword', 'default');
      $return[] = self::to_menu_array('icofont-info-sign', 'link', 'default', '', array(), '一般資訊', array(
        self::to_menu_array('icofont-link', 'link', 'default'),
        self::to_menu_array('icofont-sitemap', 'option', 'list', '', array(Admin::VAR_PATH => 1), '連結分類'),
        self::to_menu_array('icofont-exclamation-sign', 'question', 'default'),
        self::to_menu_array('icofont-sitemap', 'option', 'list', '', array(Admin::VAR_PATH => 2), '問題分類'),
		self::to_menu_array('icofont-exclamation-sign', 'news', 'default'),
		self::to_menu_array('icofont-sitemap', 'option', 'list', '', array(Admin::VAR_PATH => 3), '消息分類'),
		self::to_menu_array('icofont-exclamation-sign', 'contact', 'default'),
      ));
      $return[] = self::to_menu_array('icofont-tags', 'option', 'default');
      $return[] = self::to_menu_array('icofont-th-large', 'node', 'default');
      $return[] = self::to_menu_array('icofont-user', 'core', 'account_info', '', array(), '', array(
        self::to_menu_array('icofont-key', 'core', 'account_password'),
        self::to_menu_array('icofont-picture', 'core', 'file')
      ));
      $return[] = self::to_menu_array('icofont-off', 'core', 'cm_logoff');
      return $return;
    }

    static public function to_menu_tmpl() {
      $menu = array();

      foreach(self::get_menu_array() as $menuRows) {
        if(!self::load_mod($menuRows['mod'])) continue;

        $request = self::$_MOD[$menuRows['mod']]
          ->get_request($menuRows['request'], $menuRows['action'])
          ->set_params($menuRows['params']);

        if(empty($menuRows['text'])) {
          $request->set_text_format('<div class="helper-font-24"><i class="' . $menuRows['icon'] . '"></i></div><span class="sidebar-text">%s</span>');
        } else {
          $request->set_text('<div class="helper-font-24"><i class="' . $menuRows['icon'] . '"></i></div><span class="sidebar-text">' . $menuRows['text'] . '</span>');
        }

        $mainPermission = $request->check_permission();
        $menuSub = array();

        if(!empty($menuRows['sub'])) {
          foreach($menuRows['sub'] as $menuSubRows) {
            if(!self::load_mod($menuSubRows['mod'])) continue;

            $requestSub = self::$_MOD[$menuSubRows['mod']]
              ->get_request($menuSubRows['request'], $menuSubRows['action'])
              ->set_params($menuSubRows['params']);

            if(empty($menuSubRows['text'])) {
              $requestSub->set_text_format('<div class="helper-font-24"><i class="' . $menuSubRows['icon'] . '"></i></div><span class="sidebar-text">%s</span>');
            } else {
              $requestSub->set_text('<div class="helper-font-24"><i class="' . $menuSubRows['icon'] . '"></i></div><span class="sidebar-text">' . $menuSubRows['text'] . '</span>');
            }

            $subPermission = $requestSub->check_permission();

            if($mainPermission == true) {
              if($subPermission == false) continue;
            } else {
              if($subPermission == true) {
                $mainPermission = true;
                $request = $requestSub;
              }
            }

            if($subPermission == true) {
              $menuSub[] = array(
                'link' => $requestSub->get_link(),
                'has_sub' => false,
                'sub' => array(),
                'uClass' => ''
              );
            }      
          }
        }

        if($mainPermission == false) continue;

        $menu[] = array(
          'link' => $request->get_link(),
          'has_sub' => !empty($menuSub),
          'sub' => $menuSub,
          'uClass' => 'sub-sidebar corner-top shadow-silver-dark'
        );
      }

      return $menu;
    }
  }
?>