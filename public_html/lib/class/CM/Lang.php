<?php
  /**
   * 多語系類別
   *
   * @package ContentManager
   * @author CloudChen <cloud@dgfactor.com.tw>
   * @since 3.0
   *
   */
  class CM_Lang {
    /**
     * 取得繁體中文代號
     */
    const CODE_TW = 'zh-tw';
    /**
     * 取得簡體中文代號
     */
    const CODE_CN = 'zh-cn';
    /**
     * 取得英語代號
     */
    const CODE_EN = 'en';
    
    /**
     * 語系檔案位置
     */
    static protected $_LANG_PATH = array();
    /**
     * 目前語系(介面)
     */
    static protected $_LANG_CURRENT = self::CODE_TW;
    /**
     * 語系內容(介面)
     */
    static protected $_LANG = array();
    /**
     * 語系種類(介面)
     */
    static protected $_LANG_LIST = array(
      self::CODE_TW => array('id' => 1, 'text' => '正體中文'),
      self::CODE_CN => array('id' => 2, 'text' => '简体中文')
    );
    /**
     * 目前語系(資料)
     */
    static protected $_DATA_LANG_CURRENT = self::CODE_TW;
    /**
     * 語系內容(資料)
     */
    static protected $_DATA_LANG = array();
    /**
     * 語系種類(資料)
     */
    static protected $_DATA_LANG_LIST = array(
      self::CODE_TW => array('id' => 1, 'text' => '正體中文')
      // self::CODE_CN => array('id' => 2, 'text' => '简体中文')
    );
    /**
     * 設定語系
     */
    static public function set_lang($options, $isData = false) {
      if($isData == false) {
        self::$_LANG_LIST = $options;
      } else {
        self::$_DATA_LANG_LIST = $options;
      }
    }
    /**
     * 設定語系檔位置
     */
    static public function set_lang_path($path) {
      self::$_LANG_PATH[] = $path;
    }
    /**
     * 語系代號是否存在
     */
    static public function code_exists($langCode, $isData = false) {
      if($isData == false) {
        return array_key_exists($langCode, self::$_LANG_LIST);
      } else {
        return array_key_exists($langCode, self::$_DATA_LANG_LIST);
      }      
    }
    /**
     * 語系編號是否存在
     */
    static public function id_exists($langId, $isData = false) {
      $result = false;
      $list = ($isData == false) ? self::$_LANG_LIST : self::$_DATA_LANG_LIST;
      
      foreach($list as $index => $value) {
        if($value['id'] == $langId) {
          $result = true;
          break;
        }
      }
      
      return $result;
    }
    /**
     * 取得目前語系編號
     */
    static public function get_current_id($isData = false) {
      if($isData == false) {
        return self::$_LANG_LIST[self::$_LANG_CURRENT]['id'];
      } else {
        return self::$_DATA_LANG_LIST[self::$_DATA_LANG_CURRENT]['id'];
      }
    }
    /**
     * 取得目前語系代號
     */
    static public function get_current_code($isData = false) {
      if($isData == false) {
        return self::$_LANG_CURRENT;
      } else {
        return self::$_DATA_LANG_CURRENT;
      }
    }
    /**
     * 取得語系類型選項(編號)
     */
    static public function get_options($isData = false) {
      $returnOptions = array();
      $list = ($isData == false) ? self::$_LANG_LIST : self::$_DATA_LANG_LIST;
      
      foreach($list as $value) $returnOptions[] = $value;
      
      return $returnOptions;
    }
    /**
     * 取得語系類型選項(代號)
     */
    static public function get_code_options($isData = false) {
      $returnOptions = array();
      $list = ($isData == false) ? self::$_LANG_LIST : self::$_DATA_LANG_LIST;
      
      foreach($list as $key => $value) $returnOptions[] = array('id' => $key, 'text' => $value['text']);
      
      return $returnOptions;
    }
    /**
     * 語系編號轉代號
     */
    static public function id_to_code($id, $isData = false) {
      $options = $isData == true ? self::$_DATA_LANG_LIST : self::$_LANG_LIST;

      foreach($options as $key => $value) {
        if($value['id'] == $id) return $key;
      }

      return null;
    }
    /**
     * 用代號取得語系內容
     */
    static public function get_lang_byCode($code, $isData = false) {
      if($isData == true) {
        return self::$_DATA_LANG_LIST[$code];
      } else {
        return self::$_LANG_LIST[$code];
      }
    }
    /**
     * 設定目前語系
     */
    static public function current($code = '', $isData = false) {
      if(empty($code)) {
        return $isData == false ? self::$_LANG_CURRENT : self::$_DATA_LANG_CURRENT;
      } else {
        if(self::code_exists($code, $isData)) {
          if($isData == false) {
            self::$_LANG_CURRENT = $code;
          } else {
            self::$_DATA_LANG_CURRENT = $code;
          }
        } else {
          CM::trigger_error(sprintf('語系 %s 沒有被定義', $code));
        }
      }
    }
    /**
     * 載入語系內容
     */
    static public function import($langName, $inputLang, $currentLang = '') {
      if(empty($currentLang)) $currentLang = self::$_LANG_CURRENT;
      
      if(isset($inputLang[$currentLang])) {
        self::$_LANG[$langName][$currentLang] = $inputLang[$currentLang];
      } else {
        CM::trigger_error(sprintf('%s - %s 沒有資料', $langName, $currentLang));
      }
    }
    /**
     * 讀取語系內容
     */
    static public function load($langName, $currentLang, $includePath = '') {
      if(empty($currentLang)) $currentLang = self::$_LANG_CURRENT;
      
      if(!isset(self::$_LANG[$langName][$currentLang])) {
        $fileName = str_replace('_', '/', $langName) . '.lang.php';
        
        if(empty($includePath)) {
          //沒有設定路徑，使用初始設定的路徑
          foreach(self::$_LANG_PATH as $path) {
            $langPath = $path . $fileName;
            
            if(is_file($langPath)) {
              require($langPath);
              break;
            }
          }
        } else {
          //使用設定的路徑
          $langPath = $includePath . $fileName;
          
          if(is_file($langPath)) require($langPath);
        }
      }
    }
    /**
     * 顯示多語系文字
     */
    static public function line($langStr, $currentLang = '', $includePath = '') {
      if(empty($currentLang)) $currentLang = self::$_LANG_CURRENT;
      
      // $returnLine = CM_Conf::get('DEF.LANG_NULL');
      $returnLine = sprintf("{%s}", $langStr);
      list($langName, $langKey) = explode('.', $langStr);
      self::load($langName, $currentLang, $includePath);
      
      if(isset(self::$_LANG[$langName][$currentLang][$langKey])) $returnLine = self::$_LANG[$langName][$currentLang][$langKey];
      
      return $returnLine;
    }
    /**
     * 輸出多語系文字
     */
    static public function export($langName, $currentLang = '') {
      if(empty($currentLang)) $currentLang = self::$_LANG_CURRENT;
      
      $returnLang = array();
      self::load($langName, $currentLang);
      
      if(isset(self::$_LANG[$langName]) || isset(self::$_LANG[$langName][$currentLang])) $returnLang = self::$_LANG[$langName][$currentLang];
      
      return $returnLang;
    }
  }
?>