<?php
  class Site_DAO_Option extends Site_DAO {
  
	const TABLE_NAME = 'options';
    const TABLE_LANG_NAME = 'options_lang';
    const COLUMN_RECORD_ID = 'id';
	const COLUMN_CATALOG_ID = 'catalog';
	const COLUMN_HEADLINE = 'headline';
    const COLUMN_STATUS = 'status';
	
	const GROUP_ADD = 'add';
    const GROUP_EDIT = 'edit';
    const GROUP_LIST = 'list';
    const GROUP_STATUS = 'status';
    const GROUP_OPTION = 'option';
    const GROUP_CATALOG = 'catalog';
    const GROUP_PROGRAM_EDITCLASS_ADD = 'program_editclass_add';
    const GROUP_PROGRAM_EDITCLASS_ADD_SAVE = 'program_editclass_add_save';
  

    static public function lang($key) {
      return CM_Lang::line("DAO_Option.{$key}");
    }
	
	static public function get_status_options() {
      return array(
        array('id' => '1', 'text' => self::lang('COLUMN_STATUS_ENABLE')),
        array('id' => '0', 'text' => self::lang('COLUMN_STATUS_DISABLE'))
      );
    }
	
    public function __construct($daoName) {
	  //設定資料庫別名
      $this->set_alias(self::TABLE_NAME, 'p');
      $this->set_alias(self::TABLE_LANG_NAME, 'pd');
      //parent::__construct($daoName, self::COLUMN_RECORD_ID, self::TABLE_NAME);
	  
	  parent::__construct($daoName, self::COLUMN_RECORD_ID, self::TABLE_NAME, array(
        'tableLang' => true,
        'tableLangName' => self::TABLE_LANG_NAME,
        'tableLangColumn' => 'lang'
      ));
	  
	  $this->init_column(self::COLUMN_RECORD_ID, array(self::TABLE_NAME, self::TABLE_LANG_NAME), array(
        'label' => self::lang('COLUMN_ID')
      ), 'record_id');
	  $this->init_column(self::COLUMN_CATALOG_ID, self::TABLE_NAME, array(
        'label' => self::lang('COLUMN_CATALOG_ID'),
        'readonly' => true
      ));
      $this->init_column('lang', self::TABLE_LANG_NAME, array(
        'label' => self::lang('COLUMN_LANG'),
        'readonly' => true
      ));
      $this->init_column(self::COLUMN_HEADLINE, self::TABLE_LANG_NAME, array(
        'label' => self::lang('COLUMN_HEADLINE')
      ), 'name');
	  $this->init_column('val', self::TABLE_LANG_NAME, array(
        'label' => self::lang('COLUMN_VAL'),
        'required' => false
      ), 'name');
      $this->init_column('file', self::TABLE_LANG_NAME, array(
        'label' => self::lang('COLUMN_FILE'),
        'fs_options' => array(
          'expireDays' => 5
        )
      ), 'select_file');
      $this->init_column('note', self::TABLE_LANG_NAME, array(
        'label' => self::lang('COLUMN_NOTE'),
        'f_type' => 'textarea'
      ));
       $this->init_column(self::COLUMN_STATUS, self::TABLE_NAME, array(
        'label' => self::lang('COLUMN_STATUS'),
        'f_type' => 'select',
        'f_class' => 'input-small',
        'f_options' => self::get_status_options()
      ));
      $this->init_column('lang_status', self::TABLE_LANG_NAME, array(
        'label' => self::lang('COLUMN_LANG_STATUS'),
        'f_type' => 'select',
        'f_class' => 'input-small',
        'f_options' => self::get_status_options()
      ));
      $this->init_column('create_date', self::TABLE_NAME, array('label' => self::lang('COLUMN_CREATE_DATE')), 'record_time');
      $this->init_column('modify_date', self::TABLE_NAME, array('label' => self::lang('COLUMN_MODIFY_DATE')), 'record_time');
	  
      /*$this->set_options('status', array(
        array('id' => '1', 'text' => self::lang('COLUMN_STATUS_ENABLE')),
        array('id' => '2', 'text' => self::lang('COLUMN_STATUS_DISABLE'))
      ));
      $this->set_column(self::COLUMN_STATUS, array('f_options' => $this->get_options('status')));
      $this->set_column('lang_status', array('f_options' => $this->get_options('status')));*/
	  
	  //載入自定義欄位群組
	  $this->set_column_group(self::GROUP_ADD, array(self::COLUMN_RECORD_ID, self::COLUMN_CATALOG_ID, 'lang', self::COLUMN_HEADLINE, 'val', self::COLUMN_STATUS, 'lang_status', 'create_date', 'modify_date'));
      $this->set_column_group(self::GROUP_EDIT, array(self::COLUMN_RECORD_ID, self::COLUMN_HEADLINE, 'val', self::COLUMN_STATUS, 'lang_status', 'modify_date'));
      $this->set_column_group(self::GROUP_LIST, array(self::COLUMN_RECORD_ID, self::COLUMN_HEADLINE, 'val', self::COLUMN_STATUS, 'create_date', 'modify_date'));
      $this->set_column_group(self::GROUP_STATUS, array('status', 'modify_date'));
      $this->set_column_group(self::GROUP_OPTION, array(self::COLUMN_RECORD_ID, self::COLUMN_HEADLINE, 'val'));
      $this->set_column_group(self::GROUP_CATALOG, array(self::COLUMN_RECORD_ID, self::COLUMN_HEADLINE, 'file'));
      $this->set_column_group(self::GROUP_PROGRAM_EDITCLASS_ADD, array(self::COLUMN_HEADLINE));
      $this->set_column_group(self::GROUP_PROGRAM_EDITCLASS_ADD_SAVE, array(self::COLUMN_RECORD_ID, self::COLUMN_CATALOG_ID, 'lang', self::COLUMN_HEADLINE, 'create_date', 'modify_date'));
      //$this->set_form_group('info', array(self::COLUMN_HEADLINE, 'val', 'file', 'note'));
      $this->set_form_group('info', array(self::COLUMN_HEADLINE, 'val'));
      $this->set_form_group('setting', array(self::COLUMN_STATUS, 'lang_status'));
	  
    }
    
    protected function get_sql($groupName, $params = array()) {
      if($params['isCount'] == true) {
        //取得筆數
        $sqlColumn = sprintf('COUNT(`p`.`%s`) AS `total`', self::COLUMN_RECORD_ID);
      } else {
        $columnArray = $this->get_column_group($groupName);
		//print_r($columnArray);
        
		$sqlColumn = array();
        //如果索引沒有在列表內則新增
        //if(!array_key_exists(self::COLUMN_RECORD_ID, $columnArray)) $sqlColumn[] = $this->get_column(self::COLUMN_RECORD_ID)->get_full_name($this->_table_alias);
        //foreach($this->get_column_group($groupName) as $columnName => $column) $sqlColumn[] = $column->get_full_name($this->_table_alias);
       
		//如果索引沒有在列表內則新增
        if(!array_key_exists(self::COLUMN_RECORD_ID, $columnArray)) $sqlColumn[] = $this->get_column(self::COLUMN_RECORD_ID)->name_full . ' AS `' . self::COLUMN_RECORD_ID . '`';
        //如果分類編號沒有在列表內則新增
        if(!array_key_exists(self::COLUMN_CATALOG_ID, $columnArray)) $sqlColumn[] = $this->get_column(self::COLUMN_CATALOG_ID)->name_full . ' AS `' . self::COLUMN_CATALOG_ID . '`';
        
        foreach($this->get_column_group($groupName) as $columnName => $column) $sqlColumn[] = $column->name_full . ' AS `' . $columnName . '`';
		
        $sqlColumn = implode(', ', $sqlColumn);
      }
	  
      //如果有代入RecordId，解決排序的問題
      $this->process_recordId_sort($params, sprintf('`p`.`%s`', self::COLUMN_RECORD_ID));
      $sqlWhere = $this->get_sql_where($params);
      $sqlLimit = $params['isLimit'] == true ? sprintf('%d, %d', $params['startIndex'], $params['endIndex']) : '';
      $sqlSort = !empty($params['sort']) ? $params['sort'] : sprintf('`p`.`%s` ASC', self::COLUMN_RECORD_ID);	  
      
      if(isset($params['statusShowAll']) && $params['statusShowAll'] == true) {
		echo 111;
        return sprintf("SELECT %s FROM `%s` AS `p` LEFT JOIN `%s` AS `pd` ON `p`.`%s` = `pd`.`%s` AND `pd`.`lang` = %d%s ORDER BY %s%s",
          $sqlColumn,
          self::TABLE_NAME,
          self::TABLE_LANG_NAME,
          self::COLUMN_RECORD_ID,
          self::COLUMN_RECORD_ID,
          isset($params['lang']) ? $params['lang'] : CM_Lang::get_current_id(true),
          !empty($sqlWhere) ? ' WHERE ' . $sqlWhere : '',
          $sqlSort,
          !empty($sqlLimit) ? ' LIMIT ' . $sqlLimit : ''
        );
      } else {
		/*
		echo sprintf("SELECT %s FROM `%s` AS `p`, `%s` AS `pd` WHERE `p`.`%s` = `pd`.`%s` AND `p`.`%s` = '1' AND `pd`.`lang_status` = '1' AND `pd`.`lang` = %d%s ORDER BY %s%s",
          $sqlColumn,
          self::TABLE_NAME,
          self::TABLE_LANG_NAME,
          self::COLUMN_RECORD_ID,
          self::COLUMN_RECORD_ID,
          self::COLUMN_STATUS,
          isset($params['lang']) ? $params['lang'] : CM_Lang::get_current_id(true),
          !empty($sqlWhere['sql']) ? ' WHERE ' . implode(' AND ', $sqlWhere['sql']) : '',
          $sqlSort,
          !empty($sqlLimit) ? ' LIMIT ' . $sqlLimit : ''
        );
		*/
        return array(sprintf("SELECT %s FROM `%s` AS `p`, `%s` AS `pd` WHERE `p`.`%s` = `pd`.`%s` AND `p`.`%s` = '1' AND `pd`.`lang_status` = '1' AND `pd`.`lang` = %d%s ORDER BY %s%s",
          $sqlColumn,
          self::TABLE_NAME,
          self::TABLE_LANG_NAME,
          self::COLUMN_RECORD_ID,
          self::COLUMN_RECORD_ID,
          self::COLUMN_STATUS,
          isset($params['lang']) ? $params['lang'] : CM_Lang::get_current_id(true),
          //!empty($sqlWhere['sql']) ? ' WHERE ' . implode(' AND ', $sqlWhere['sql']) : '',
		  !empty($sqlWhere['sql']) ? ' AND ' . implode(' AND ', $sqlWhere['sql']) : '',
          $sqlSort,
          !empty($sqlLimit) ? ' LIMIT ' . $sqlLimit : ''
        ), $sqlWhere['params']) ;
		
      }
    }
    
    protected function get_sql_where(array $params) {
      $return = $this->sql_where_default(sprintf('`p`.`%s`', self::COLUMN_RECORD_ID), $params);
	  
      //指定目錄
      if(isset($params['catalog']) && !empty($params['catalog'])) {
		$return['sql'][] =  sprintf("`p`.`%s` = :catalog",  self::COLUMN_CATALOG_ID);
        $return['params'][':catalog'] = (int)$params['catalog'];
      }
      //指定多個目錄
      if(isset($params['catalogs'])) {
        $return['sql'][] = $this->sql_where_record_in($params['catalogs'], "`p`.`catalog`");
      }
      //狀態條件
      if(isset($params['status']) && !empty($params['status'])) {
        $return['sql'][] = sprintf("`p`.`%s` = :status", self::COLUMN_STATUS);
        $return['params'][':status'] = $params['status'];
      }
      //關鍵字條件
      if(isset($params['keyword']) && !empty($params['keyword'])) {
        $return['sql'][] = sprintf("`pd`.`%s` LIKE :keyword", self::COLUMN_HEADLINE);
        $return['params'][':keyword'] = "%{$params['keyword']}%";
      }
	  
	  if(isset($params['val']) && !empty($params['val'])){
		$return['sql'][] = sprintf("`pd`.`%s` LIKE :val", self::COLUMN_HEADLINE);
        $return['params'][':val'] = "%{$params['val']}%";
	  }
     
      return $return;
    }
    
     public function get_options($catalogId, $group = '') {
      if(empty($group)) $group = CM_DAO_Option::GROUP_OPTION;
      
      $params = array(
        'catalog' => $catalogId,
        'isLimit' => false
      );
      
      return $this->get_array($options, $group, $params) ? $options : array();
    }
	
	
	
  }
?>