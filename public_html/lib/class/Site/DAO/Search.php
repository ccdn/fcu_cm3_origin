<?php
  class Site_DAO_Search extends Site_DAO {
    const TABLE_NAME = 'video';
    const TABLE_LANG_NAME = 'video_lang';
    const COLUMN_RECORD_ID = 'id';
	const COLUMN_CATALOG_ID = 'catalog';
    const COLUMN_HEADLINE = 'headline';
	const COLUMN_DESC = 'desc';
	const COLUMN_AUTHOR = 'author';
    const COLUMN_STATUS = 'status';
    const GROUP_ADD = 'add';
    const GROUP_EDIT = 'edit';
    const GROUP_LIST = 'list';
    const GROUP_STATUS = 'status';
    const GROUP_OPTION = 'option';
	const GROUP_IFRAME = 'iframe';
	const GROUP_DETAIL = 'detail';
	const GROUP_FRONT_LIST = 'f_list';
	const GROUP_SEARCH = 'search';
	const COLUMN_PROCESSOK ='process_ok';
	const COLUMN_BOOK ='headline_sub_3';
	//const COLUMN_TAGS ='tags';
	const COLUMN_TAGS ='headline_sub_2';//關鍵字
	const COLUMN_PUBLISHER = 'release';
	const COLUMN_ATTR1 = 'attr_1';//索書號
	const COLUMN_LANGUAGE = 'attr_2';//語言
	const COLUMN_HEADLINE_SUB_1 = 'headline_sub_1';//其他參與者
	

    static public function lang($key) {
      return CM_Lang::line("DAO_Video.{$key}");
    }
    
    public function __construct($daoName) {
      //設定資料庫別名
      $this->set_alias(self::TABLE_NAME, 'p');
      $this->set_alias(self::TABLE_LANG_NAME, 'pd');

      parent::__construct($daoName, self::COLUMN_RECORD_ID, self::TABLE_NAME, array(
        'tableLang' => true,
        'tableLangName' => self::TABLE_LANG_NAME,
        'tableLangColumn' => 'lang'
      ));
      //載入自定義欄位
      //$this->init_column('columnName', 'columnTable', array('params1' => '1'), 'columnDefault');
      //載入自定義欄位群組
      //$this->set_column_group('groupName', array('column1', 'column2', 'column3'));
      //載入自定義表單群組
      //$this->set_form_group('groupName', array('column1', 'column2', 'column3'));
      $this->set_options('status', array(
        array('id' => '1', 'text' => self::lang('COLUMN_STATUS_ENABLE')),
        array('id' => '2', 'text' => self::lang('COLUMN_STATUS_DISABLE'))
      ));
      $this->set_options('type', array(
        array('id' => '1', 'text' => self::lang('COLUMN_TYPE_1')),
        array('id' => '2', 'text' => self::lang('COLUMN_TYPE_2')),
        array('id' => '3', 'text' => self::lang('COLUMN_TYPE_3'))
      ));
      $this->set_options('copyright_status', array(
        array('id' => '1', 'text' => self::lang('COLUMN_COPYRIGHT_STATUS_1')),
        array('id' => '2', 'text' => self::lang('COLUMN_COPYRIGHT_STATUS_2'))
      ));

      $this->set_column(self::COLUMN_STATUS, array('f_options' => $this->get_options('status')));
      $this->set_column('lang_status', array('f_options' => $this->get_options('status')));
      $this->set_column('copyright_status', array('f_options' => $this->get_options('copyright_status')));
      $this->set_column('type', array('f_options' => $this->get_options('type')));
	  
	  //載入自定義欄位群組
	  $this->set_column_group(self::GROUP_DETAIL, array('file', 'file_img', 'catalog', 'copyright', 'copyright_status', 'start_date', 'end_date', 'release_date', 'modify_date', self::COLUMN_HEADLINE, 'desc', 'author', 'release','video_length','login_status'));
	  $this->set_column_group(self::GROUP_IFRAME, array('file', 'file_img', 'catalog', 'copyright', 'copyright_status', 'start_date', 'end_date', 'release_date', 'status', 'modify_date', self::COLUMN_HEADLINE, 'desc', 'author', 'release', 'lang_status','process_count','video_length','login_status')); 
	  $this->set_column_group(self::GROUP_FRONT_LIST, array(self::COLUMN_RECORD_ID, self::COLUMN_HEADLINE, 'desc', 'file', 'file_img', 'author','start_date','video_length')); 
	  $this->set_column_group(self::GROUP_SEARCH, array('date_start', 'date_end'));//check欄位
	  
    }

    protected function get_sql($groupName, $params = array()) {
	  
	  /*//沒有id不正常就不跑了
	  if(empty($params['recordId'])) {
		echo "操作錯誤";
		die();
      }*/
	
      if($params['isCount'] == true) {
        //取得筆數
        $sqlColumn = sprintf('COUNT(`p`.`%s`) AS `total`', self::COLUMN_RECORD_ID);
		
      } else {
        $columnArray = $this->get_column_group($groupName);
        $sqlColumn = array();
        //如果索引沒有在列表內則新增
        if(!array_key_exists(self::COLUMN_RECORD_ID, $columnArray)) $sqlColumn[] = $this->get_column(self::COLUMN_RECORD_ID)->get_full_name($this->_table_alias);

        if(!array_key_exists('type', $columnArray)) $sqlColumn[] = $this->get_column('type')->get_full_name($this->_table_alias);
        
        foreach($this->get_column_group($groupName) as $columnName => $column) $sqlColumn[] = $column->get_full_name($this->_table_alias);
        
        $sqlColumn = implode(', ', $sqlColumn);
		
		
		
		//if( strpos($params['sort'],"click")!== FALSE ){
		//	echo("<script>console.log('PHP: ". $params['od']."');</script>");
			
			//加入log計算
			//預設是total,除非是有加上週/月/年的選擇
			//if($params['od']=='clickcount')$params['od']='t';//一般頁按點擊次數排序
			$logrange = $this->getLogRange($params['od']);
			$sqlColumn .= ',(select SUM('.$logrange['range'].') from stream_week_log sw where sw.`iMda_seq`=p.id'.$logrange['plus_sql'].' group by `iMda_seq` )as clickcount';
		//}
		
		//$sqlColumn .= ',(CASE WHEN sum(w3 ) is null then 0 else sum(w3) end )as clickcount';
		//echo $sqlColumn;
      }
      //如果有代入RecordId，解決排序的問題
      $this->process_recordId_sort($params, sprintf('`p`.`%s`', self::COLUMN_RECORD_ID));
      $sqlWhere = $this->get_sql_where($params);
	  $sqlWhereSwitch = $this->get_sql_where_switch($params);//搜尋頁加的多餘選項
      //$sqlLimit = $params['isLimit'] == true ? sprintf('%d, %d', $params['startIndex'], $params['endIndex']) : '';
	  $sqlLimit = !empty($params['offset']) ? sprintf('%d, %d', $params['startIndex'], $params['endIndex']) : '';
      $sqlSort = !empty($params['sort']) ? $params['sort'] : sprintf('`p`.`%s` DESC', self::COLUMN_RECORD_ID);
	  
	  $switchSql="";
	  for($i=0;$i<count($sqlWhereSwitch['sql']);$i++){
		$switchSql .= $sqlWhereSwitch['conn'][$i];
		$switchSql .= " ".$sqlWhereSwitch['sql'][$i];
	  }
	  
	 
	  //補上多餘搜尋變數
	  if(!empty($sqlWhereSwitch)){
		if(!empty($sqlWhereSwitch['params'])){
			$sqlWhere['params'] = array_merge($sqlWhere['params'],$sqlWhereSwitch['params']);
		}
	  }
	  /*
	   print_r( array(sprintf("SELECT %s FROM `%s` AS `p` LEFT JOIN `%s` AS `pd` ON `p`.`%s` = `pd`.`%s` AND `pd`.`lang` = %d%s %s ORDER BY %s%s",
        $sqlColumn,
        self::TABLE_NAME,
        self::TABLE_LANG_NAME,
        self::COLUMN_RECORD_ID,
        self::COLUMN_RECORD_ID,
        isset($params['lang']) ? $params['lang'] : CM_Lang::get_current_id(true),
        !empty($sqlWhere['sql']) ? ' WHERE ' . implode(' AND ', $sqlWhere['sql']) : '',
		!empty($sqlWhereSwitch['sql']) ? $switchSql : '',
        $sqlSort,
        !empty($sqlLimit) ? ' LIMIT ' . $sqlLimit : ''
      ), $sqlWhere['params']) );
	  */

	 //加了clickcount之後一般筆數判斷很詭異，所以讓他不要用order去判斷... 
	 if($params['isCount'] == true) {
		return array(sprintf("SELECT %s FROM `%s` AS `p` LEFT JOIN `%s` AS `pd` ON `p`.`%s` = `pd`.`%s` AND `pd`.`lang` = %d%s %s %s",
        $sqlColumn,
        self::TABLE_NAME,
        self::TABLE_LANG_NAME,
        self::COLUMN_RECORD_ID,
        self::COLUMN_RECORD_ID,
        isset($params['lang']) ? $params['lang'] : CM_Lang::get_current_id(true),
        !empty($sqlWhere['sql']) ? ' WHERE ' . implode(' AND ', $sqlWhere['sql']) : '',
		!empty($sqlWhereSwitch['sql']) ? $switchSql : '',
        !empty($sqlLimit) ? ' LIMIT ' . $sqlLimit : ''
      ), $sqlWhere['params']) ;
	 }else{
		return array(sprintf("SELECT %s FROM `%s` AS `p` LEFT JOIN `%s` AS `pd` ON `p`.`%s` = `pd`.`%s` AND `pd`.`lang` = %d%s %s ORDER BY %s%s",
        $sqlColumn,
        self::TABLE_NAME,
        self::TABLE_LANG_NAME,
        self::COLUMN_RECORD_ID,
        self::COLUMN_RECORD_ID,
        isset($params['lang']) ? $params['lang'] : CM_Lang::get_current_id(true),
        !empty($sqlWhere['sql']) ? ' WHERE ' . implode(' AND ', $sqlWhere['sql']) : '',
		!empty($sqlWhereSwitch['sql']) ? $switchSql : '',
        $sqlSort,
        !empty($sqlLimit) ? ' LIMIT ' . $sqlLimit : ''
      ), $sqlWhere['params']) ;
	 }
	 //原本沒有clickcount是這樣寫
      //return array(sprintf("SELECT %s FROM `%s` AS `p` LEFT JOIN `%s` AS `pd` ON `p`.`%s` = `pd`.`%s` AND `pd`.`lang` = %d%s ORDER BY %s%s",
	/*  return array(sprintf("SELECT %s FROM `%s` AS `p` LEFT JOIN `%s` AS `pd` ON `p`.`%s` = `pd`.`%s` AND `pd`.`lang` = %d LEFT JOIN `stream_week_log` sw ON sw.iMda_seq=p.id %s ORDER BY %s%s",
        $sqlColumn,
        self::TABLE_NAME,
        self::TABLE_LANG_NAME,
        self::COLUMN_RECORD_ID,
        self::COLUMN_RECORD_ID,
        isset($params['lang']) ? $params['lang'] : CM_Lang::get_current_id(true),
        !empty($sqlWhere['sql']) ? ' WHERE ' . implode(' AND ', $sqlWhere['sql']) : '',
        $sqlSort,
        !empty($sqlLimit) ? ' LIMIT ' . $sqlLimit : ''
      ), $sqlWhere['params']) ;*/
    }
    
    protected function get_sql_where(array $params) {
      $return = $this->sql_where_default(sprintf('`p`.`%s`', self::COLUMN_RECORD_ID), $params);
      // 狀態條件
      /*if(isset($params['status']) && !empty($params['status'])) {
        $return['sql'][] = sprintf("`p`.`%s` = :status", self::COLUMN_STATUS);
        $return['params'][':status'] = $params['status'];
      }*/
	  
	  //必要條件
      $return['sql'][] = sprintf("`p`.`%s` =1", self::COLUMN_STATUS);
	  $return['sql'][] = sprintf("`p`.`%s` ='2'", self::COLUMN_PROCESSOK);//因為欄位格式記得設成字串
	  
	  // 上下架
	  if(isset($params['date']) && !empty($params['date'])) {
		$return['sql'][] = "`p`.`start_date` <= date_add('".$params['date']."' , INTERVAL 1 day) AND `p`.`end_date` >= '".$params['date']."'";
      }
      //檔案類型
      if(isset($params['type']) && !empty($params['type'])) {
        $return['sql'][] = "`p`.`type` = :type";
        $return['params'][':type'] = $params['type'];
      }
	  //分類
	  if(isset($params['catalog']) && !empty($params['catalog'])) {
        //$return['sql'][] = $this->sql_where_record_in($params['catalogs'], "`p`.`catalog`");
		$return['sql'][] = "FIND_IN_SET(:catalog,`p`.`catalog`)";
        $return['params'][':catalog'] = (int)$params['catalog'];
      }
	  
      //關鍵字條件
	  //echo ("<script>console.log('kc: ".$params['kc']."');</script>");
      if(isset($params['keyword']) && !empty($params['keyword'])) {
		if(isset($params['kc']) && $params['kc']=='n'){
			//標題
			$return['sql'][] = sprintf("(`pd`.`%s` LIKE :keyword1)", self::COLUMN_HEADLINE);
			$return['params'][':keyword1'] = "%{$params['keyword']}%";
		}else if(isset($params['kc']) && $params['kc']=='k'){
			//簡介
			$return['sql'][] = sprintf("(`pd`.`%s` LIKE :keyword1)", self::COLUMN_DESC);
			$return['params'][':keyword1'] = "%{$params['keyword']}%";
		}else if(isset($params['kc']) && $params['kc']=='as'){
			//相關作者
			$return['sql'][] = sprintf("(`pd`.`%s` LIKE :keyword1)", self::COLUMN_AUTHOR);
			$return['params'][':keyword1'] = "%{$params['keyword']}%";
		}else if(isset($params['kc']) && $params['kc']=='ser'){
			//叢書名/系列名
			$return['sql'][] = sprintf("(`pd`.`%s` LIKE :keyword1)", self::COLUMN_BOOK);
			$return['params'][':keyword1'] = "%{$params['keyword']}%";
		}else if(isset($params['kc']) && $params['kc']=='sub'){
			//標題/主題(舊資料=關鍵字)**這裡的判斷可能要再想一下,好像會有多個關鍵字...
			$return['sql'][] = sprintf("(`pd`.`%s` LIKE :keyword1)", self::COLUMN_TAGS);
			$return['params'][':keyword1'] = "%{$params['keyword']}%";
		}else if(isset($params['kc']) && $params['kc']=='ot'){
			//其他參與者
			$return['sql'][] = sprintf("(`pd`.`%s` LIKE :keyword1)", self::COLUMN_HEADLINE_SUB_1);
			$return['params'][':keyword1'] = "%{$params['keyword']}%";
		}else{
			//標題&簡介(預設)
			$return['sql'][] = sprintf("(`pd`.`%s` LIKE :keyword1 OR `pd`.`%s` LIKE :keyword2)", self::COLUMN_HEADLINE,self::COLUMN_DESC);
			$return['params'][':keyword1'] = "%{$params['keyword']}%";
			$return['params'][':keyword2'] = "%{$params['keyword']}%";
		}
      }
	  
      return $return;
    }
	
	//針對會換成and,or,not的項目
	protected function get_sql_where_switch(array $params) {
	  
	  //出版者
	  if(isset($params['publisher']) && !empty($params['publisher'])) {
		$return['sql'][] = sprintf("(`pd`.`%s` LIKE :publisher)", self::COLUMN_PUBLISHER);
		$return['params'][':publisher'] = "%{$params['publisher']}%";
		$return['conn'][] = "{$params['pubtype']}";
      }
	  
	  //語言
	  if(isset($params['language']) && !empty($params['language'])) {
		$return['sql'][] = sprintf("`p`.`%s` = :language", self::COLUMN_LANGUAGE);
		$return['conn'][] = "{$params['langtype']}";
		$return['params'][':language'] = $params['language'];
      }
	  
	  //索書號
	  if(isset($params['attr_1']) && !empty($params['attr_1'])) {
		$return['sql'][] = sprintf("`p`.`%s` LIKE :attr_1", self::COLUMN_ATTR1);
		$return['params'][':attr_1'] = "%{$params['attr_1']}%";
		$return['conn'][] = "{$params['attr_1_type']}";
      }
	  
	  
	  //上架日期區間
	  $dateSql='';
	  if(!empty($params['date_start']) || !empty($params['date_end']) ){
		if(isset($params['date_start']) && !empty($params['date_start'])) {
			$dateSql.=" Date(`p`.`start_date`) >= '".$params['date_start']."'";
		}
		if(!empty($params['date_start']) && !empty($params['date_end']) ){
			$dateSql.=" AND ";
		}
		if(isset($params['date_end']) && !empty($params['date_end']) ) {
			$dateSql.=" Date(`p`.`start_date`) <= '".$params['date_end']."' ";
		}
		$return['sql'][] = $dateSql;
		$return['conn'][] = "{$params['datetype']}";
		//$return['params'][':date_start'] = "%{$params['date_start']}%";
		//$return['params'][':date_end'] = "%{$params['date_end']}%";
	  }
	  print_r($return);
      return $return;
    }
	
	public function choose_catalog($typeId){
		  $type = array(
			1 => 'catalog_Video',
			2 => 'catalog_Sound',
			3 => 'catalog_Picture'
		  );
		  //$catalogDAO = Admin_Factory::createCMDAO($type[$typeId]);
		  $catalogDAO = Site_Factory::createCMDAO($type[$typeId]);
		  $catalogParams = array(
			'sort' => sprintf('`parent_id` ASC, `p`.`sort` DESC, `%s` ASC', Site_DAO_Catalog_Video::COLUMN_RECORD_ID),
			'status' => 1
		  );
		  $catalogOptions = array();
		  $catalogDAO->get_array($catalogArray, Site_DAO_Catalog_Video::GROUP_LIST, $catalogParams);
		  //print_r($catalogArray);
		  foreach($catalogArray as $catalogRows) {
			 $daoParams = array_merge(
				$catalogParams,
				array('tree' => array(
				  'func' => 'parent_all',
				  'id' => $catalogRows[Site_DAO_Catalog_Video::COLUMN_RECORD_ID]
				))
			  );
			  //利用text判斷第幾階層
			  $text = implode('&nbsp;&raquo;&nbsp;', array_map(function($element) {
				return $element[Site_DAO_Catalog_Video::COLUMN_HEADLINE];
			  }, $catalogDAO->get_array($parentCatalogArray, Admin_DAO_Catalog_Video::GROUP_LIST, $daoParams) ? array_merge($parentCatalogArray, array($catalogRows)) : array($catalogRows)));
			  $lv = substr_count($text,'&nbsp;&raquo;&nbsp;');
			  $catalogOptions[] = array(
				'id' => $catalogRows[Site_DAO_Catalog_Video::COLUMN_RECORD_ID],
				'lv' => $lv,
				'parent_id' => $catalogRows['parent_id'],
				'headline' => $catalogRows[Site_DAO_Catalog_Video::COLUMN_HEADLINE]
				//'text' => $text
			  );
		  }
		  //return $catalogArray;
		  return $catalogOptions;
	}
    
    public function init_options($typeId) {
      $type = array(
        1 => 'catalog_Video',
        2 => 'catalog_Sound',
        3 => 'catalog_Picture'
      );
      $catalogDAO = Admin_Factory::createCMDAO($type[$typeId]);
      $catalogParams = array(
        'sort' => sprintf('`parent_id` ASC, `%s` ASC', Admin_DAO_Catalog_Video::COLUMN_RECORD_ID)
      );
      $catalogOptions = array();

      if($catalogDAO->get_array($catalogArray, Admin_DAO_Catalog_Video::GROUP_LIST, $catalogParams)) {
        foreach($catalogArray as $catalogRows) {
          $daoParams = array_merge(
            $catalogParams,
            array('tree' => array(
              'func' => 'parent_all',
              'id' => $catalogRows[Admin_DAO_Catalog_Video::COLUMN_RECORD_ID]
            ))
          );
          $text = implode('&nbsp;&raquo;&nbsp;', array_map(function($element) {
            return $element[Admin_DAO_Catalog_Video::COLUMN_HEADLINE];
          }, $catalogDAO->get_array($parentCatalogArray, Admin_DAO_Catalog_Video::GROUP_LIST, $daoParams) ? array_merge($parentCatalogArray, array($catalogRows)) : array($catalogRows)));
          $catalogOptions[] = array(
            'id' => $catalogRows[Admin_DAO_Catalog_Video::COLUMN_RECORD_ID],
            'text' => $text
          );
        }
      }

      $this->set_options('catalog', $catalogOptions);
      $this->set_column('catalog', array(
        'f_class' => 'input-xxlarge',
        'f_options' => $this->get_options('catalog')
      ));
      $this->set_options('copyright', array_map(function($element) {
        return array(
          'id' => $element[Admin_DAO_Copyright::COLUMN_RECORD_ID],
          'text' => $element[Admin_DAO_Copyright::COLUMN_HEADLINE]
        );
      }, Admin_Factory::createCMDAO('copyright')->get_array($recordArray, Admin_DAO_Copyright::GROUP_LIST) ? $recordArray : array()) );
      $this->set_column('copyright', array(
        'f_class' => 'input-large',
        'f_options' => $this->get_options('copyright')
      ));

      $allfiles = array(
        1 => 'mp4',
        2 => 'mp4',
        3 => 'gif,jpg,jpeg,png'
      );
      $fileColumn = $this->get_column('file');
      $fileColumn->fu_options['acceptFileTypes'] = $allfiles[$typeId];
    }

    public function insert_rows(&$rMessage, &$rReturn, $recordRows, $groupName = '', $params = array()) {
      $rMessage = '';
      $timeNow = date('Y-m-d H:i:s');
      $recordRows = array_merge($recordRows, array(
        'create_date' => $timeNow,
        'modify_date' => $timeNow
      ));
      $success = $this->process_insert($rMessage, $rReturn, $recordRows, array_merge($params, array(
        'groupName' => $groupName
      )));
      
      if($success == true) $rMessage = self::lang('ADD_SUCCESS');
      
      return $success;
    }

    public function update_rows(&$rMessage, &$rReturn, $recordRows, $recordId, $groupName = '', $params = array()) {
      $rMessage = '';
      $recordRows = array_merge($recordRows, array(
        'modify_date' => date('Y-m-d H:i:s')
      ));
      $success = $this->process_update($rMessage, $rReturn, $recordRows, $recordId, array_merge($params, array(
        'groupName' => $groupName
      )));

      if($success == true) $rMessage = self::lang('EDIT_SUCCESS');

      return $success;
    }

    public function delete_rows(&$rMessage, &$rReturn, $recordId, $params = array()) {
      $rMessage = '';
      $success = $this->process_delete($rMessage, $rReturn, $recordId, $params);
      
      if($success == true) $rMessage = self::lang('REMOVE_SUCCESS');
      
      return $success;
    }
	
	public function getLogRange($logType='t'){
		$where=' and year='.date('Y');//其他三種都要，只有全部不用
		//全部
		if($logType=='t'){
			$colStart=1;
			$colEnd=54;
			$where='';
		//週
		}else if($logType=='w'){
			$this_week = strftime("%U", mktime(0,0,0,date('m'),date('d'),date('Y')))+1;
			$colStart=$this_week;
			$colEnd=$this_week;
		//月
		}else if($logType=='m'){
			$colStart=$this_month_week_start = strftime("%U", mktime(0,0,0,date('m'),1,date('Y')))+1;
			$colEnd=$this_month_week_end = strftime("%U", mktime(0,0,0,date('m'),date('t'),date('Y')))+1;
		//年
		}else if($logType=='y'){
			$colStart=1;
			$colEnd=54;
		}else{
		//全部(其他阿哩不達的都是統計全部)
			$colStart=1;
			$colEnd=54;
			$where='';
		}
		
		$tempw=0;
		$columns='';
		for($i=$colStart;$i<=$colEnd;$i++){
			if($tempw==0){
				$columns.='w'.$i;
			}else{
				$columns.='+w'.$i;
			}
			$tempw++;
		}
		$log_range = array(
			'range' => $columns,
			'plus_sql' => $where
		  );
		//echo "col=".$columns."<br>";
		//echo "sql=".$where."<br>";
		//print_r($log_range);
		return $log_range;
	}
	
  }
?>