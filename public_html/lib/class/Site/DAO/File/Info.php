<?php
  class Site_DAO_File_Info extends Site_DAO {
    const TABLE_NAME = 'file_info';
    const COLUMN_RECORD_ID = 'file';
    const COLUMN_HEADLINE = 'name';
    const DEFAULT_SORT = '`dao` ASC, `column` ASC, `record_id` ASC';
    const GROUP_ADD = 'add';
    const GROUP_EDIT = 'edit';
    const GROUP_LIST = 'list';
    const GROUP_CALLBACK = 'callback';

    static public function lang($key) {
      return CM_Lang::line("DAO_File_Info.{$key}");
    }

    public function __construct($daoName) {
      parent::__construct($daoName, self::COLUMN_RECORD_ID, self::TABLE_NAME);
      //載入自定義欄位
      //$this->init_column('columnName', 'columnTable', array('params1' => '1'), 'columnDefault');
      //載入自定義欄位群組
      //$this->set_column_group('groupName', array('column1', 'column2', 'column3'));
      //載入自定義表單群組
      //$this->set_form_group('groupName', array('column1', 'column2', 'column3'));
      $this->set_column('desc', array('f_attr' => array('style' => 'width: 450px; height: 150px;')));
      $this->set_column('note', array('f_attr' => array('style' => 'width: 450px; height: 150px;')));
    }

    protected function get_sql($groupName, $params = array()) {
      if($params['isCount'] == true) {
        //取得筆數
        $sqlColumn = sprintf('COUNT(`%s`) AS `total`', self::COLUMN_RECORD_ID);
      } else {
        //欄位群組和一定會取出的資料
        $sqlColumn = '`' . implode('`, `', array_merge(
          array_keys($this->get_column_group($groupName)),
          array(self::COLUMN_RECORD_ID)
        )) . '`';
      }
      //如果有代入RecordId，解決排序的問題
      $this->process_recordId_sort_s($params, sprintf('`%s`', self::COLUMN_RECORD_ID));

      $sqlWhere = $this->get_sql_where($params);
      $sqlLimit = $params['isLimit'] == true ? sprintf('%d, %d', $params['startIndex'], $params['endIndex']) : '';
      $sqlSort = !empty($params['sort']) ? $params['sort'] : self::DEFAULT_SORT;

      return array(sprintf("SELECT %s FROM `%s`%s ORDER BY %s%s",
        $sqlColumn,
        self::TABLE_NAME,
        !empty($sqlWhere['sql']) ? ' WHERE ' . implode(' AND ', $sqlWhere['sql']) : '',
        $sqlSort,
        !empty($sqlLimit) ? ' LIMIT ' . $sqlLimit : ''
      ), $sqlWhere['params']);
    }
    
    protected function get_sql_where(array $params) {
      $return = $this->sql_where_default_s(sprintf('`%s`', self::COLUMN_RECORD_ID), $params);

      if(isset($params['lang']) && !empty($params['lang'])) {
        $return['sql'][] = "`lang` = :lang";
        $return['params'][':lang'] = $params['lang'];
      }
      
      if(isset($params['rId']) && !empty($params['rId'])) {
        $return['sql'][] = "`record_id` = :rId";
        $return['params'][':rId'] = $params['rId'];
      }

      if(isset($params['dao']) && !empty($params['dao'])) {
        $return['sql'][] = "`dao` = :dao";
        $return['params'][':dao'] = $params['dao'];
      }

      if(isset($params['column']) && !empty($params['column'])) {
        $return['sql'][] = "`column` = :column";
        $return['params'][':column'] = $params['column'];
      }
      //關鍵字條件
      if(isset($params['keyword']) && !empty($params['keyword'])) {
        $return['sql'][] = sprintf("`%s` LIKE :keyword", self::COLUMN_HEADLINE);
        $return['params'][':keyword'] = "%{$params['keyword']}%";
      }

      return $return; 
    }
  }
?>