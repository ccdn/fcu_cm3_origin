<?php
  class Site_DAO_Node extends Site_DAO {
    const TABLE_NAME = 'node';
    const TABLE_LANG_NAME = 'node_lang';
    const COLUMN_RECORD_ID = 'id';
    const COLUMN_PARENT_ID = 'parent_id';
    const COLUMN_HEADLINE = 'headline';
    const COLUMN_STATUS = 'status';
    const GROUP_ID = 'id';
    const GROUP_ADD = 'add';
    const GROUP_EDIT = 'edit';
    const GROUP_LIST = 'list';
    const GROUP_STATUS = 'status';

    static public function lang($key) {
      return CM_Lang::line("DAO_Node.{$key}");
    }
    
    public function __construct($daoName) {
      //設定資料庫別名
      $this->set_alias(self::TABLE_NAME, 'p');
      $this->set_alias(self::TABLE_LANG_NAME, 'pd');

      parent::__construct($daoName, self::COLUMN_RECORD_ID, self::TABLE_NAME, array(
        'tableLang' => true,
        'tableLangName' => self::TABLE_LANG_NAME,
        'tableLangColumn' => 'lang'
      ));
      //載入自定義欄位
      //$this->init_column('columnName', 'columnTable', array('params1' => '1'), 'columnDefault');
      //載入自定義欄位群組
      //$this->set_column_group('groupName', array('column1', 'column2', 'column3'));
      //載入自定義表單群組
      //$this->set_form_group('groupName', array('column1', 'column2', 'column3'));
    }

    public function tree_func_children($id) {
      //下一層全部
      $params = array(
        self::COLUMN_PARENT_ID => $id
      );
      $returnId = array();
      
      if($this->get_array($recordArray, self::GROUP_ID, $params)) {
        foreach($recordArray as $recordRows) $returnId[] = $recordRows[self::COLUMN_RECORD_ID];
      }
      
      return $returnId;
    }
    
    public function tree_func_children_all($id) {
      //全部下層
      $params = array(
        self::COLUMN_PARENT_ID => $id
      );
      $returnId = array();
      
      if($this->get_array($recordArray, self::GROUP_ID, $params)) {
        foreach($recordArray as $recordRows) {
          $returnId[] = $recordRows[self::COLUMN_RECORD_ID];
          $returnId = array_merge($returnId, $this->tree_func_children_all($recordRows[self::COLUMN_RECORD_ID]));
        }
      }
      
      return $returnId;
    }
    
    public function tree_func_parent($id) {
      //上一層
      $returnId = array();
      //根目錄
      if($id == 0 || $id == '0') return $returnId;
      
      if(
        $this->get_rows_byId($currentRows, self::GROUP_ID, $id) &&
        $this->get_array($recordArray, self::GROUP_ID, array('recordId' => $currentRows[self::COLUMN_PARENT_ID]))
      ) {
        foreach($recordArray as $recordRows) $returnId[] = $recordRows[self::COLUMN_RECORD_ID];
      }
      
      return $returnId;
    }
    
    public function tree_func_parent_all($id, $parentId = '') {
      //全部上層
      $returnId = array();
      //根目錄
      if($id == 0 || $id == '0') return $returnId;
      
      if(empty($parentId) && $this->get_rows_byId($currentRows, self::GROUP_ID, $id)) $parentId = $currentRows[self::COLUMN_PARENT_ID];
      
      if(!empty($parentId) && $this->get_array($recordArray, self::GROUP_ID, array('recordId' => $parentId))) {
        foreach($recordArray as $recordRows) {
          $returnId[] = $recordRows[self::COLUMN_RECORD_ID];
          $returnId = array_merge($this->tree_func_parent_all($recordRows[self::COLUMN_RECORD_ID], $recordRows[self::COLUMN_PARENT_ID]), $returnId);
        }
      }
      
      return $returnId;
    }
    
    public function tree_func_current($id) {
      //同一層全部
      
    }
    
    protected function get_sql($groupName, $params = array()) {
      if($params['isCount'] == true) {
        //取得筆數
        $sqlColumn = sprintf('COUNT(`p`.`%s`) AS `total`', self::COLUMN_RECORD_ID);
      } else {
        $columnArray = $this->get_column_group($groupName);
        $sqlColumn = array();
        //如果索引沒有在列表內則新增
        if(!array_key_exists(self::COLUMN_RECORD_ID, $columnArray)) $sqlColumn[] = $this->get_column(self::COLUMN_RECORD_ID)->get_full_name($this->_table_alias);
        
        foreach($this->get_column_group($groupName) as $columnName => $column) $sqlColumn[] = $column->get_full_name($this->_table_alias);
        
        $sqlColumn = implode(', ', $sqlColumn);
      }
      //樹狀架構功能
      if(
        isset($params['tree']) &&
        isset($params['tree']['func']) &&
        isset($params['tree']['id'])
      ) {
        $callback = array($this, "tree_func_{$params['tree']['func']}");
        
        if(is_callable($callback)) $params['recordIds'] = call_user_func($callback, $params['tree']['id']);
      }
      
      //如果有代入RecordId，解決排序的問題
      $this->process_recordId_sort($params, sprintf('`p`.`%s`', self::COLUMN_RECORD_ID));
      $sqlWhere = $this->get_sql_where($params);
      $sqlLimit = $params['isLimit'] == true ? sprintf('%d, %d', $params['startIndex'], $params['endIndex']) : '';
      $sqlSort = !empty($params['sort']) ? $params['sort'] : sprintf('`p`.`%s` ASC, `p`.`sort` ASC, `p`.`%s` ASC', self::COLUMN_PARENT_ID, self::COLUMN_RECORD_ID);

      return array(sprintf("SELECT %s FROM `%s` AS `p` LEFT JOIN `%s` AS `pd` ON `p`.`%s` = `pd`.`%s` AND `pd`.`lang` = %d%s ORDER BY %s%s",
        $sqlColumn,
        self::TABLE_NAME,
        self::TABLE_LANG_NAME,
        self::COLUMN_RECORD_ID,
        self::COLUMN_RECORD_ID,
        isset($params['lang']) ? $params['lang'] : CM_Lang::get_current_id(true),
        !empty($sqlWhere['sql']) ? ' WHERE ' . implode(' AND ', $sqlWhere['sql']) : '',
        $sqlSort,
        !empty($sqlLimit) ? ' LIMIT ' . $sqlLimit : ''
      ), $sqlWhere['params']) ;
    }
    
    protected function get_sql_where(array $params) {
      $return = $this->sql_where_default(sprintf('`p`.`%s`', self::COLUMN_RECORD_ID), $params);

      if(isset($params[self::COLUMN_PARENT_ID]) && !empty($params[self::COLUMN_PARENT_ID])) {
        $return['sql'][] = sprintf("`p`.`parent_id` = :parent_id", self::COLUMN_STATUS);
        $return['params'][':parent_id'] = $params[self::COLUMN_PARENT_ID];
      }
      //狀態條件
      if(isset($params['status']) && !empty($params['status'])) {
        $return['sql'][] = sprintf("`p`.`%s` = :status", self::COLUMN_STATUS);
        $return['params'][':status'] = $params['status'];
      }
      //關鍵字條件
      if(isset($params['keyword']) && !empty($params['keyword'])) {
        $return['sql'][] = sprintf("(`pd`.`%s` LIKE :keyword1 OR `p`.`path_name` LIKE :keyword2 OR `p`.`page_name` LIKE :keyword3)", self::COLUMN_HEADLINE);
        $return['params'][':keyword1'] = "%{$params['keyword']}%";
        $return['params'][':keyword2'] = "%{$params['keyword']}%";
        $return['params'][':keyword3'] = "%{$params['keyword']}%";
      }
      
      return $return;
    }
  }
?>