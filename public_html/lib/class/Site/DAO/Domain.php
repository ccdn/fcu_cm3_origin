<?php
  class Site_DAO_Domain extends Site_DAO {
    const TABLE_NAME = 'domain';
	const TABLE_LANG_NAME = 'domain_lang';
    const COLUMN_RECORD_ID = 'id';
    const COLUMN_HEADLINE = 'domain';
    const COLUMN_STATUS = 'status';
    const COLUMN_MODIFY_DATE = 'modify_date';
    const DEFAULT_SORT = '`id` ASC';
    const GROUP_ADD = 'add';
    const GROUP_EDIT = 'edit';
    const GROUP_INFO = 'info';
    const GROUP_LOGIN = 'login';
    const GROUP_LIST = 'list';

   /* static protected function password_encode($password) {
      return md5($password . 'password_check');
    }*/

    /*static public function lang($key) {
      return CM_Lang::line("DAO_Domain.{$key}");
    }*/

     public function __construct($daoName) {
      //設定資料庫別名
      //$this->set_alias(self::TABLE_NAME, 'p');
      //$this->set_alias(self::TABLE_LANG_NAME, 'pd');
      
      /*parent::__construct($daoName, self::COLUMN_RECORD_ID, self::TABLE_NAME, array(
        'tableLang' => true,
        'tableLangName' => self::TABLE_LANG_NAME,
        'tableLangColumn' => 'lang'
      ));*/
	  parent::__construct($daoName, self::COLUMN_RECORD_ID, self::TABLE_NAME);
	
      //載入自定義欄位
      //$this->init_column('columnName', 'columnTable', array('params1' => '1'), 'columnDefault');
      //載入自定義欄位群組
      $this->set_column_group(self::GROUP_LIST, array(self::COLUMN_RECORD_ID, self::COLUMN_HEADLINE, self::COLUMN_STATUS, self::COLUMN_MODIFY_DATE));
     
    }
    
    protected function get_sql($groupName, $params = array()) {
	
      if($params['isCount'] == true) {
        //取得筆數
        $sqlColumn = sprintf('COUNT(`%s`) AS `total`', self::COLUMN_RECORD_ID);
      } else {
        $columnArray = $this->get_column_group($groupName);
        $sqlColumn = array();
        //如果索引沒有在列表內則新增
        if(!array_key_exists(self::COLUMN_RECORD_ID, $columnArray)) $sqlColumn[] = $this->get_column(self::COLUMN_RECORD_ID)->get_full_name($this->_table_alias);
        
        foreach($this->get_column_group($groupName) as $columnName => $column) $sqlColumn[] = $column->get_full_name($this->_table_alias);
        
        $sqlColumn = implode(', ', $sqlColumn);
      }
	  
	  //如果有代入RecordId，解決排序的問題
      $this->process_recordId_sort($params, sprintf('`%s`', self::COLUMN_RECORD_ID));
      $sqlWhere = $this->get_sql_where($params);
      $sqlLimit = $params['isLimit'] == true ? sprintf('%d, %d', $params['startIndex'], $params['endIndex']) : '';
      $sqlSort = !empty($params['sort']) ? $params['sort'] : sprintf('`%s` ASC', self::COLUMN_RECORD_ID);
	  
	  
      return array(sprintf("SELECT %s FROM `%s` WHERE `%s` = '1'%s ORDER BY %s%s",
        $sqlColumn,
        self::TABLE_NAME,
        self::COLUMN_STATUS,
        !empty($sqlWhere['sql']) ? ' AND ' . implode(' AND ', $sqlWhere['sql']) : '',
        $sqlSort,
        !empty($sqlLimit) ? ' LIMIT ' . $sqlLimit : ''
      ), $sqlWhere['params']);
    }
    
    protected function get_sql_where(array $params) {
      //$return = $this->sql_where_default(sprintf('`%s`', self::COLUMN_RECORD_ID), $params);
	  $return = $this->sql_where_default(sprintf('`%s`', self::COLUMN_RECORD_ID), $params);
      
      return $return;
    }
    
    
  }
?>