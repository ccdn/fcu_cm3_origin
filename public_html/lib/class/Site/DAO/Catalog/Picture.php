<?php
  class Site_DAO_Catalog_Picture extends Site_DAO {
    const TABLE_NAME = 'catalog_picture';
    const TABLE_LANG_NAME = 'catalog_picture_lang';
    const COLUMN_RECORD_ID = 'id';
    const COLUMN_PARENT_ID = 'parent_id';
    const COLUMN_HEADLINE = 'headline';
    const COLUMN_STATUS = 'status';
    const GROUP_ID = 'id';
    const GROUP_ADD = 'add';
    const GROUP_EDIT = 'edit';
    const GROUP_LIST = 'list';
    const GROUP_STATUS = 'status';
    const GROUP_OPTION = 'option';
	const GROUP_DIR = 'dir';

    static public function lang($key) {
      return CM_Lang::line("DAO_Catalog_Picture.{$key}");
    }

    public function __construct($daoName) {
      //設定資料庫別名
      $this->set_alias(self::TABLE_NAME, 'p');
      $this->set_alias(self::TABLE_LANG_NAME, 'pd');
      
      parent::__construct($daoName, self::COLUMN_RECORD_ID, self::TABLE_NAME, array(
        'tableLang' => true,
        'tableLangName' => self::TABLE_LANG_NAME,
        'tableLangColumn' => 'lang'
      ));
      
      $this->set_options('status', array(
        array('id' => '1', 'text' => self::lang('COLUMN_STATUS_ENABLE')),
        array('id' => '2', 'text' => self::lang('COLUMN_STATUS_DISABLE'))
      ));
      $this->set_column(self::COLUMN_STATUS, array('f_options' => $this->get_options('status')));
      $this->set_column('lang_status', array('f_options' => $this->get_options('status')));
	  
	  //載入自定義欄位群組
	  $this->set_column_group(self::GROUP_DIR, array(self::COLUMN_RECORD_ID, self::COLUMN_HEADLINE));
	  $this->set_column_group(self::GROUP_LIST, array(self::COLUMN_RECORD_ID, self::COLUMN_HEADLINE,'sort', self::COLUMN_PARENT_ID)); 
    }

    protected function get_sql($groupName, $params = array()) {
      if($params['isCount'] == true) {
        //取得筆數
        $sqlColumn = sprintf('COUNT(`p`.`%s`) AS `total`', self::COLUMN_RECORD_ID);
      } else {
        $columnArray = $this->get_column_group($groupName);
        $sqlColumn = array();
        //如果索引沒有在列表內則新增
        if(!array_key_exists(self::COLUMN_RECORD_ID, $columnArray)) $sqlColumn[] = $this->get_column(self::COLUMN_RECORD_ID)->get_full_name($this->_table_alias);
        
        foreach($this->get_column_group($groupName) as $columnName => $column) $sqlColumn[] = $column->get_full_name($this->_table_alias);
        
        $sqlColumn = implode(', ', $sqlColumn);
      }
      //樹狀架構功能
      if(
        isset($params['tree']) &&
        isset($params['tree']['func']) &&
        isset($params['tree']['id'])
      ) {
        $callback = array($this, "tree_func_{$params['tree']['func']}");
        $funcParams = isset($params['tree']['params']) ? $params['tree']['params'] : array();
        
        if(is_callable($callback)) $params['recordIds'] = call_user_func($callback, $params['tree']['id'], $funcParams);
      }
      //如果有代入RecordId，解決排序的問題
      $this->process_recordId_sort($params, sprintf('`p`.`%s`', self::COLUMN_RECORD_ID));
      $sqlWhere = $this->get_sql_where($params);
      $sqlLimit = $params['isLimit'] == true ? sprintf('%d, %d', $params['startIndex'], $params['endIndex']) : '';
      $sqlSort = !empty($params['sort']) ? $params['sort'] : sprintf('`p`.`sort` DESC, `p`.`%s` ASC', self::COLUMN_RECORD_ID);
	  /*
	  print_r(array(sprintf("SELECT %s FROM `%s` AS `p` LEFT JOIN `%s` AS `pd` ON `p`.`%s` = `pd`.`%s` AND `pd`.`lang` = %d%s ORDER BY %s%s",
        $sqlColumn,
        self::TABLE_NAME,
        self::TABLE_LANG_NAME,
        self::COLUMN_RECORD_ID,
        self::COLUMN_RECORD_ID,
        isset($params['lang']) ? $params['lang'] : CM_Lang::get_current_id(true),
        !empty($sqlWhere['sql']) ? ' WHERE ' . implode(' AND ', $sqlWhere['sql']) : '',
        $sqlSort,
        !empty($sqlLimit) ? ' LIMIT ' . $sqlLimit : ''
      ), $sqlWhere['params'])
	  );*/

      return array(sprintf("SELECT %s FROM `%s` AS `p` LEFT JOIN `%s` AS `pd` ON `p`.`%s` = `pd`.`%s` AND `pd`.`lang` = %d%s ORDER BY %s%s",
        $sqlColumn,
        self::TABLE_NAME,
        self::TABLE_LANG_NAME,
        self::COLUMN_RECORD_ID,
        self::COLUMN_RECORD_ID,
        isset($params['lang']) ? $params['lang'] : CM_Lang::get_current_id(true),
        !empty($sqlWhere['sql']) ? ' WHERE ' . implode(' AND ', $sqlWhere['sql']) : '',
        $sqlSort,
        !empty($sqlLimit) ? ' LIMIT ' . $sqlLimit : ''
      ), $sqlWhere['params']) ;
    }
    
    protected function get_sql_where(array $params) {
      $return = $this->sql_where_default(sprintf('`p`.`%s`', self::COLUMN_RECORD_ID), $params);
      //指定目錄
      if(isset($params[self::COLUMN_PARENT_ID])) {
		$return['sql'][] = sprintf("`p`.`%s` = :parentId", self::COLUMN_PARENT_ID);
		$return['params'][':parentId'] = (int)$params[self::COLUMN_PARENT_ID];
      }
      //狀態條件
      if(isset($params['status']) && !empty($params['status'])) {
        $return['sql'][] = sprintf("`p`.`%s` = :status", self::COLUMN_STATUS);
        $return['params'][':status'] = $params['status'];
      }
      //關鍵字條件
      if(isset($params['keyword']) && !empty($params['keyword'])) {
        $return['sql'][] = sprintf("`pd`.`%s` LIKE :keyword", self::COLUMN_HEADLINE);
        $return['params'][':keyword'] = "%{$params['keyword']}%";
      }
      
      return $return;
    }

    public function tree_func_children($id, $params = array()) {
      //下一層全部
      $daoParams = array(
        self::COLUMN_PARENT_ID => $id
      );
      $returnId = array();
      
      if($this->get_array($recordArray, self::GROUP_ID, $daoParams)) {
        foreach($recordArray as $recordRows) $returnId[] = $recordRows[self::COLUMN_RECORD_ID];
      }
      
      return $returnId;
    }
    
    public function tree_func_children_all($id, $params = array()) {
      //全部下層
      $daoParams = array(
        self::COLUMN_PARENT_ID => $id
      );
      $returnId = array();
      
      if($this->get_array($recordArray, self::GROUP_ID, $daoParams)) {
        foreach($recordArray as $recordRows) {
          $returnId[] = $recordRows[self::COLUMN_RECORD_ID];
          $returnId = array_merge($returnId, $this->tree_func_children_all($recordRows[self::COLUMN_RECORD_ID], $params));
        }
      }
      
      return $returnId;
    }
    
    public function tree_func_parent($id, $params = array()) {
      //上一層
      $returnId = array();
      //根目錄
      if($id == 0 || $id == '0') return $returnId;
      
      if(
        $this->get_rows_byId($currentRows, self::GROUP_ID, $id) &&
        $this->get_array($recordArray, self::GROUP_ID, array('recordId' => $currentRows[self::COLUMN_PARENT_ID]))
      ) {
        foreach($recordArray as $recordRows) $returnId[] = $recordRows[self::COLUMN_RECORD_ID];
      }
      
      return $returnId;
    }
    
    public function tree_func_parent_all($id, $params = array(), $parentId = '') {
      $params = array_merge(array(
        'rootId' => '0'
      ), $params);
      //全部上層
      $returnId = array();
      //根目錄
      if($id == $params['rootId'] || $parentId == $params['rootId']) return $returnId;

      if(empty($parentId) && $this->get_rows_byId($currentRows, self::GROUP_ID, $id)) {
        if($currentRows[self::COLUMN_PARENT_ID] != $params['rootId']) $parentId = $currentRows[self::COLUMN_PARENT_ID];
      }

      if(!empty($parentId) && $this->get_rows_byId($recordRows, self::GROUP_ID, $parentId)) {
        $returnId[] = $recordRows[self::COLUMN_RECORD_ID];
        $returnId = array_merge($this->tree_func_parent_all($recordRows[self::COLUMN_RECORD_ID], $params, $recordRows[self::COLUMN_PARENT_ID]), $returnId);
      }
      
      return $returnId;
    }
    
    public function insert_rows(&$rMessage, &$rReturn, $recordRows, $groupName = '', $params = array()) {
      $timeNow = date('Y-m-d H:i:s');
      $recordRows = array_merge($recordRows, array(
        'create_date' => $timeNow,
        'modify_date' => $timeNow
      ));
      $success = $this->process_insert($rMessage, $rReturn, $recordRows, array_merge($params, array('groupName' => $groupName)));
      
      if($success == true) $rMessage = self::lang('ADD_SUCCESS');
      
      return $success;
    }
    
    public function update_rows(&$rMessage, &$rReturn, $recordRows, $recordId, $groupName = '', $params = array()) {
      $recordRows = array_merge($recordRows, array(
        'modify_date' => date('Y-m-d H:i:s')
      ));
      $success = $this->process_update($rMessage, $rReturn, $recordRows, $recordId, array_merge($params, array('groupName' => $groupName)));

      if($success == true) $rMessage = self::lang('EDIT_SUCCESS');

      return $success;
    }
    
    public function delete_rows(&$rMessage, &$rReturn, $recordId, $params = array()) {
      $success = $this->process_delete($rMessage, $rReturn, $recordId, $params);
      
      if($success == true) $rMessage = self::lang('REMOVE_SUCCESS');
      
      return $success;
    }
  }
?>