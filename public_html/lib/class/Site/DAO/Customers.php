<?php
  class Site_DAO_Customers extends Site_DAO {
    const TABLE_NAME = 'customers';
    const COLUMN_RECORD_ID = 'id';
    const COLUMN_HEADLINE = 'name';
    const COLUMN_STATUS = 'status';
    const COLUMN_CREATE_DATE = 'create_date';
    const COLUMN_MODIFY_DATE = 'modify_date';
    const DEFAULT_SORT = '`id` ASC';
    const GROUP_ADD = 'add';
    const GROUP_EDIT = 'edit';
    const GROUP_INFO = 'info';
    const GROUP_LOGIN = 'login';
    const GROUP_LIST = 'list';
    const GROUP_PASSWORD = 'password';

    static protected function password_encode($password) {
      return md5($password . 'password_check');
    }

    static public function lang($key) {
      return CM_Lang::line("DAO_Customers.{$key}");
    }

    public function __construct($daoName) {
      parent::__construct($daoName, self::COLUMN_RECORD_ID, self::TABLE_NAME);
      //載入自定義欄位
      //$this->init_column('columnName', 'columnTable', array('params1' => '1'), 'columnDefault');
      //載入自定義欄位群組
      $this->set_column_group(self::GROUP_ADD, array(self::COLUMN_HEADLINE, 'file', 'sex', 'bir', 'email', 'password', 'phone', 'cellphone', 'address', 'newsletter', 'ip', self::COLUMN_STATUS, 'last_login_date', self::COLUMN_CREATE_DATE, self::COLUMN_MODIFY_DATE));
      $this->set_column_group(self::GROUP_EDIT, array(self::COLUMN_HEADLINE, 'file', 'sex', 'bir', 'email', 'phone', 'cellphone', 'address', 'newsletter', self::COLUMN_MODIFY_DATE));
      $this->set_column_group(self::GROUP_LIST, array(self::COLUMN_RECORD_ID, self::COLUMN_HEADLINE, 'file', 'email', 'ip', self::COLUMN_STATUS, 'login_count', 'login_last_date', self::COLUMN_CREATE_DATE));
      $this->set_column_group(self::GROUP_LOGIN, array('email', 'password', 'last_login_date'));
      $this->set_column_group(self::GROUP_PASSWORD, array('password', 'password_new'));
      $this->set_column_group(self::GROUP_INFO, array(self::COLUMN_HEADLINE, 'file', 'sex', 'phone', 'cellphone', 'email'));
      $this->set_column_group('last_login', array('login_count', 'login_last_date'));
      //載入自定義表單群組
      $this->set_options('sex', array(
        array('id' => '1', 'text' => self::lang('COLUMN_SEX_1')),
        array('id' => '2', 'text' => self::lang('COLUMN_SEX_2'))
      ));
      $this->set_column('sex', array('f_options' => $this->get_options('sex')));
      
      $this->set_options('newsletter', array(
        array('id' => '1', 'text' => self::lang('COLUMN_NEWSLETTER_1')),
        array('id' => '0', 'text' => self::lang('COLUMN_NEWSLETTER_2'))
      ));
      $this->set_column('newsletter', array('f_options' => $this->get_options('newsletter')));
    }
    
    protected function get_sql($groupName, $params = array()) {
      if($params['isCount'] == true) {
        //取得筆數
        $sqlColumn = sprintf('COUNT(`%s`) AS `total`', self::COLUMN_RECORD_ID);
        $sqlLimit = '0, 1';
      } else {
        //欄位群組和一定會取出的資料
        $sqlColumn = '`' . implode('`, `', array_merge(
          array_keys($this->get_column_group($groupName)),
          array(self::COLUMN_RECORD_ID)
        )) . '`';
        $sqlLimit = $params['isLimit'] == true ? sprintf('%d, %d', $params['startIndex'], $params['endIndex']) : '';
      }
      //如果有代入RecordId，解決排序的問題
      $this->process_recordId_sort($params, sprintf('`%s`', self::COLUMN_RECORD_ID));
      $sqlWhere = $this->get_sql_where($params);
      $sqlSort = !empty($params['sort']) ? $params['sort'] : '`id` DESC';
      return array(sprintf("SELECT %s FROM `%s` WHERE `%s` = '1'%s ORDER BY %s%s",
        $sqlColumn,
        self::TABLE_NAME,
        self::COLUMN_STATUS,
        !empty($sqlWhere['sql']) ? ' AND ' . implode(' AND ', $sqlWhere['sql']) : '',
        $sqlSort,
        !empty($sqlLimit) ? ' LIMIT ' . $sqlLimit : ''
      ), $sqlWhere['params']);
    }
    
    protected function get_sql_where(array $params) {
      $return = $this->sql_where_default(sprintf('`%s`', self::COLUMN_RECORD_ID), $params);
      //判斷信箱(帳號)
      if(isset($params['email']) && !empty($params['email'])) {
        $return['sql'][] = "`email` = :email";
        $return['params'][':email'] = $params['email'];
      }
      //判斷密碼
      if(isset($params['password']) && !empty($params['password'])) {
        $return['sql'][] = "`password` = :password";
        $return['params'][':password'] = self::password_encode($params['password']);
      }
      //關鍵字條件
      if(isset($params['keyword']) && !empty($params['keyword'])) {
        $return['sql'][] = sprintf("`%s` LIKE :keyword", self::COLUMN_HEADLINE);
        $return['params'][':keyword'] = "%{$params['keyword']}%";
      }
      
      return $return;
    }
    
    public function insert_rows(&$rMessage, &$rReturn, $recordRows, $groupName = '', $params = array()) {
      //密碼修改會經過加密
      if(isset($recordRows['password'])) $recordRows['password'] = self::password_encode($recordRows['password']);
      
      if(isset($recordRows['email']) && $this->get_rows($tmpRows, $groupName, array('email' => $recordRows['email']))) {
        $rMessage = sprintf(self::lang('ERROR_ACCOUNT_EMAIL_DUPLICATE'), $recordRows['email']);
        return false;
      }
            
      $rMessage = '';
      $timeNow = date('Y-m-d H:i:s');
      $recordRows = array_merge($recordRows, array(
        'ip' => Site::get_ip_address(),
        'create_date' => $timeNow,
        'modify_date' => $timeNow
      ));
      $success = $this->process_insert($rMessage, $rReturn, $recordRows, array_merge($params, array(
        'groupName' => $groupName
      )));
      
      if($success == true) $rMessage = self::lang('ADD_SUCCESS');
      
      return $success;
    }

    public function update_rows(&$rMessage, &$rReturn, $recordRows, $recordId, $groupName = '', $params = array()) {
      //密碼修改會經過加密
      if(isset($recordRows['password'])) $recordRows['password'] = self::password_encode($recordRows['password']);
      
      if(isset($recordRows['email']) && $this->get_rows($tmpRows, $groupName, array('email' => $recordRows['email'], 'exceptionId' => $recordId))) {
        $rMessage = sprintf(self::lang('ERROR_ACCOUNT_EMAIL_DUPLICATE'), $recordRows['email']);
        return false;
      }

      $rMessage = '';
      $recordRows = array_merge($recordRows, array(
        'modify_date' => date('Y-m-d H:i:s')
      ));
      $success = $this->process_update($rMessage, $rReturn, $recordRows, $recordId, array_merge($params, array(
        'groupName' => $groupName
      )));

      if($success == true) $rMessage = self::lang('EDIT_SUCCESS');

      return $success;
    }

    public function delete_rows(&$rMessage, &$rReturn, $recordId, $params = array()) {
      $rMessage = '';
      $success = $this->process_delete($rMessage, $rReturn, $recordId, $params);
      
      if($success == true) $rMessage = self::lang('REMOVE_SUCCESS');
      
      return $success;
    }
  }
?>