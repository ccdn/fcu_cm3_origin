<?php
  class Site_DAO_Sound extends Site_DAO {
    const TABLE_NAME = 'sound';
    const COLUMN_RECORD_ID = 'id';
    const COLUMN_HEADLINE = 'headline';
    const COLUMN_STATUS = 'status';
    const GROUP_ADD = 'add';
    const GROUP_EDIT = 'edit';
    const GROUP_LIST = 'list';
    const GROUP_OPTION = 'option';
    const GROUP_STATUS = 'status';
    const GROUP_ADD_LIST = 'add_list';
	const COLUMN_PROCESSOK ='process_ok';

    static public function lang($key) {
      return CM_Lang::line("DAO_Sound.{$key}");
    }

    public function __construct($daoName) {
      parent::__construct($daoName, self::COLUMN_RECORD_ID, self::TABLE_NAME);
      //載入自定義欄位
      //$this->init_column('columnName', 'columnTable', array('params1' => '1'), 'columnDefault');
      //載入自定義欄位群組
      //$this->set_column_group('groupName', array('column1', 'column2', 'column3'));
      //載入自定義表單群組
      $this->set_form_group('add_sound', array('file', 'status', 'lang_status'));
      
      $this->set_options('process_ok', array(
        array('id' => '2', 'text' => self::lang('COLUMN_PROCESS_OK_2')),
        array('id' => '3', 'text' => self::lang('COLUMN_PROCESS_OK_3')),
        array('id' => '4', 'text' => self::lang('COLUMN_PROCESS_OK_4'))
      ));
      $this->set_options('status', array(
        array('id' => '1', 'text' => self::lang('COLUMN_STATUS_ENABLE')),
        array('id' => '2', 'text' => self::lang('COLUMN_STATUS_DISABLE'))
      ));
      
      $this->set_column(self::COLUMN_STATUS, array(
        'f_options' => $this->get_options('status'),
        'f_class' => 'input-small'
      ));
      $this->set_column('process_ok', array(
        'f_options' => $this->get_options('process_ok'),
        'f_class' => 'input-small'
      ));
    }
    
    protected function get_sql($groupName, $params = array()) {
      if($params['isCount'] == true) {
        //取得筆數
        $sqlColumn = sprintf('COUNT(`%s`) AS `total`', self::COLUMN_RECORD_ID);
        $sqlLimit = '0, 1';
      } else {
        //欄位群組和一定會取出的資料
        $sqlColumn = '`' . implode('`, `', array_merge(
          array(self::COLUMN_RECORD_ID),
          array_keys($this->get_column_group($groupName))
        )) . '`';
        $sqlLimit = $params['isLimit'] == true ? sprintf('%d, %d', $params['startIndex'], $params['endIndex']) : '';
      }
      //如果有代入RecordId，解決排序的問題
      $this->process_recordId_sort($params, sprintf('`%s`', self::COLUMN_RECORD_ID));
      $sqlWhere = $this->get_sql_where($params);
      //$sqlSort = !empty($params['sort']) ? $params['sort'] : '`sort` DESC, `id` DESC';
	  //150623:從小排到大
	  $sqlSort = !empty($params['sort']) ? $params['sort'] : '`sort`, `id` DESC';
      return array(sprintf("SELECT %s FROM `%s`%s ORDER BY %s%s",
        $sqlColumn,
        self::TABLE_NAME,
        !empty($sqlWhere['sql']) ? ' WHERE ' . implode(' AND ', $sqlWhere['sql']) : '',
        $sqlSort,
        !empty($sqlLimit) ? ' LIMIT ' . $sqlLimit : ''
      ), $sqlWhere['params']);
    }
    
    protected function get_sql_where(array $params) {
      $return = $this->sql_where_default(sprintf('`%s`', self::COLUMN_RECORD_ID), $params);
	  
	  //必要條件
      $return['sql'][] = sprintf("`%s` =1", self::COLUMN_STATUS);
	  $return['sql'][] = sprintf("`%s` ='2'", self::COLUMN_PROCESSOK);//因為欄位格式記得設成字串

      //檔案名稱
      if(isset($params['file']) && !empty($params['file'])) {
        $return['sql'][] = "`file` = :file";
        $return['params'][':file'] = $params['file'];
      }
      
      if(isset($params['albumId']) && !empty($params['albumId'])) {
        $return['sql'][] = "`album_id` = :albumId";
        $return['params'][':albumId'] = $params['albumId'];
      }

      /*if(isset($params['status']) && !empty($params['status'])) {
        $return['sql'][] = sprintf("`%s` LIKE :status", self::COLUMN_STATUS);
        $return['params'][':status'] = "%{$params['status']}%";
      }*/
      //關鍵字條件
      if(isset($params['keyword']) && !empty($params['keyword'])) {
        $return['sql'][] = sprintf("`%s` LIKE :keyword", self::COLUMN_HEADLINE);
        $return['params'][':keyword'] = "%{$params['keyword']}%";
      }
      
      return $return;
    }

    public function init_options() {
      
    }
    
    public function insert_rows(&$rMessage, &$rReturn, $recordRows, $groupName = '', $params = array()) {      
      $rMessage = '';
      $timeNow = date('Y-m-d H:i:s');
      $recordRows = array_merge($recordRows, array(
        'create_date' => $timeNow,
        'modify_date' => $timeNow
      ));
      $success = $this->process_insert($rMessage, $rReturn, $recordRows, array_merge($params, array(
        'groupName' => $groupName
      )));
      
      if($success == true) $rMessage = self::lang('ADD_SUCCESS');
      
      return $success;
    }

    public function update_rows(&$rMessage, &$rReturn, $recordRows, $recordId, $groupName = '', $params = array()) {
      $rMessage = '';
      $recordRows = array_merge($recordRows, array(
        'modify_date' => date('Y-m-d H:i:s')
      ));
      $success = $this->process_update($rMessage, $rReturn, $recordRows, $recordId, array_merge($params, array(
        'groupName' => $groupName
      )));

      if($success == true) $rMessage = self::lang('EDIT_SUCCESS');

      return $success;
    }

    public function delete_rows(&$rMessage, &$rReturn, $recordId, $params = array()) {
      $rMessage = '';
      $success = $this->process_delete($rMessage, $rReturn, $recordId, $params);
      
      if($success == true) $rMessage = self::lang('REMOVE_SUCCESS');
      
      return $success;
    }
  }
?>