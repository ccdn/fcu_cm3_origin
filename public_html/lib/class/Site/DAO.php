<?php
  class Site_DAO extends CM_DAO {
    protected function init_column($columnName, $tableName, $columnParams, $default = '') {
      $this->_column[$columnName] = Site_Factory::createCMColumn($columnName, array_merge($columnParams, array(
        'table' => $tableName,
        'dao' => $this->_dao_conf['daoName'],
        'f_class' => 'form-control' //bootstrap V3要加入
        // 'f_finalCallback' => function($formAction, $column, $columbValue, $columnArray) {
        //   return "<p class=\"form-control-static\">{$columbValue}</p>";
        // }
      )), $default);
      $this->set_column($columnName, array('name_full' => $this->get_column($columnName)->get_full_name($this->_table_alias)));

      if(in_array($this->_column[$columnName]->f_type, array('radio', 'checkbox'))) $this->_column[$columnName]->f_class = '';
    }

    public function get_array(&$returnArray, $groupName, $params = array()) {
      $success = parent::get_array($returnArray, $groupName, $params);
      //自動抓檔案資訊
      if(isset($params['fileCallback']) && $params['fileCallback'] == true) {
        $fileColumn = $this->get_columns_by_type(array('file'), $groupName);

        if(!empty($fileColumn)) {
          $dao = null;
          $daoInfo = null;
          $lang = isset($params['lang']) ? $params['lang'] : CM_Lang::get_current_id(true);

          foreach($returnArray as &$returnRows) {
            $daoParams = array(
              'rId' => $returnRows[$this->_dao_conf['tablePKey']],
              'dao' => $this->_dao_conf['daoName']
            );

            foreach($fileColumn as $column) {
              if(!isset($returnRows[$column->name]) || empty($returnRows[$column->name])) continue;

              if(is_null($dao)) $dao = Site_Factory::createCMDAO('file');

              if(is_null($daoInfo)) $daoInfo = Site_Factory::createCMDAO('file_Info');

              $columnIsLang = $this->_dao_conf['tableLang'] == true && $column->table == $this->_dao_conf['tableLangName'] ? true : false;

              if($dao->get_array($fileArray, Site_DAO_File::GROUP_CALLBACK, array('recordIds' => explode(',', $returnRows[$column->name])))) {
                $returnRows[$column->name] = array();

                foreach($fileArray as &$fileRows) {
                  $returnRows[$column->name][] = $fileRows[Site_DAO_File::COLUMN_RECORD_ID];

                  if($daoInfo->get_rows($fileInfoRows, Site_DAO_File_Info::GROUP_CALLBACK, array_merge($daoParams, array(
                    'recordId' => $fileRows[Site_DAO_File::COLUMN_RECORD_ID],
                    'column' => $column->name,
                    'lang' => $columnIsLang == true ? $lang : '0'
                  )))) {
                    $fileRows[Site_DAO_File::COLUMN_HEADLINE] = $fileInfoRows[Site_DAO_File_Info::COLUMN_HEADLINE];
                    $fileRows['desc'] = $fileInfoRows['desc'];
                  }

                  if(!isset($params['fileCallbackDescFilter']) || $params['fileCallbackDescFilter'] == true) {
                    $fileRows[Site_DAO_File::COLUMN_HEADLINE] = Site_Format::to_html_attr($fileRows[Site_DAO_File::COLUMN_HEADLINE]);
                    $fileRows['desc'] = Site_Format::to_html_attr($fileRows['desc']);
                  }
                }

                $returnRows["{$column->name}_fattr"] = $fileArray;
              }
            }
          }
        }
      }

      return $success;
    }
  }
?>