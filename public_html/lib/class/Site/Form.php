<?php
  class Site_Form extends CM_Form {
  
    protected $_dao_conf = null;

    protected $_dao_rows = array();

    protected $_fileinfo_dao = null;
    
    public function set_form_dao(CM_DAO $dao, $groupName = '') {
      parent::set_form_dao($dao, $groupName);
      //加入DAO的設定
      $this->_dao_conf = $dao->get_dao_conf();
    }

    public function set_form_default($defaultRows) {
      parent::set_form_default($defaultRows);
      //如果有DAO的設定，存下特定的值
      if(!is_null($this->_dao_conf)) {
        $this->_dao_rows['recordId'] = isset($defaultRows[$this->_dao_conf['tablePKey']]) ? $defaultRows[$this->_dao_conf['tablePKey']] : '';

        if($this->_dao_conf['tableLang'] == true) $this->_dao_rows['lang'] = !isset($defaultRows[$this->_dao_conf['tableLangColumn']]) || empty($defaultRows[$this->_dao_conf['tableLangColumn']]) ? $defaultRows[$this->_dao_conf['tableLangColumn']] : CM_Lang::get_current_id(true);
      }
    }
    /**
     * 取得簡易檔案欄位
     */
    protected function get_field_simple_file($column, $attr, $params) {
      $columnParams = array(
        'dao' => $column->dao,
        'column' => $column->name,
        '_request' => 'upload'
      );
      $file = array();
      
      if(!empty($column->val)) {
        if(is_null($this->_dao)) $this->_dao = Site_Factory::createCMDAO($columnParams['dao']);

        $columnParams['rId'] = $this->_dao_rows['recordId'];
        $files = explode(',', $column->val);

        if($this->_dao_conf['tableLang'] == true) $columnParams['lang'] = $this->_dao_rows['lang'];

        $file = array_map(function($element) use($columnParams) {
          return array_shift(Site_Format::to_simple_upload_file_list(array(array_merge($columnParams, array(
            'file' => $element
          )))));
        }, $files);

      }
      //參數預設值設定
      $attr['data-options'] = Site::json_encode(array_merge($column->fu_options, array(
        'acceptFileTypes' => "/(\.|\/)(" . str_replace(',', '|', $column->fu_options['acceptFileTypes']) . ")$/i"
      )));
      $attr['data-params'] = Site::json_encode(array_merge($column->fu_params, $columnParams));
      $attr['data-url'] = !empty($column->fu_url) ? $column->fu_url : Site::href_link('/controller.php', array());
      $attr['data-cmfield'] = 'upload';

      if(!isset($column->f_form_params)) $column->f_form_params = array();

      $column->f_form_params['tip'] = implode('<br />', $this->get_file_field_tip($column->fu_options));
      
      return CM_Template::get_tpl_html('javascript_upload_column', array(
        'lang' => CM_Lang::export('Javascript'),
        'files' => $file,
        'upload_field' => $this->get_field_hidden($column, $attr, array())
      ));
    }
  }
?>