<?php

/*
 * This file is part of Mustache.php.
 *
 * (c) 2012 Justin Hileman
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * Mustache Template filesystem Loader implementation.
 *
 * A FilesystemLoader instance loads Mustache Template source from the filesystem by name:
 *
 *   $loader = new Mustache_Loader_FilesystemLoader(dirname(__FILE__).'/views');
 *   $tpl = $loader->load('foo'); // equivalent to `file_get_contents(dirname(__FILE__).'/views/foo.mustache');
 *
 * This is probably the most useful Mustache Loader implementation. It can be used for partials and normal Templates:
 *
 *   $m = new Mustache(array(
 *      'loader'      => new Mustache_Loader_FilesystemLoader(dirname(__FILE__).'/views'),
 *      'partials_loader' => new Mustache_Loader_FilesystemLoader(dirname(__FILE__).'/views/partials'),
 *   ));
 */
class Mustache_Loader_CMFilesystemLoader implements Mustache_Loader
{
  private $baseDir;
  private $extension = '.html';
  private $templates = array();
  /**
   * Mustache filesystem Loader constructor.
   *
   * Passing an $options array allows overriding certain Loader options during instantiation:
   *
   *   $options = array(
   *     // The filename extension used for Mustache templates. Defaults to '.mustache'
   *     'extension' => '.ms',
   *   );
   *
   * @throws Mustache_Exception_RuntimeException if $baseDir does not exist.
   *
   * @param string $baseDir Base directory containing Mustache template files.
   * @param array  $options Array of Loader options (default: array())
   */
  public function __construct(array $baseDir, array $options = array())
  {
    foreach($baseDir as $key => $path) $this->baseDir[] = rtrim(realpath($path), '/');
    /*
    if (!is_dir($this->baseDir)) {
      throw new Mustache_Exception_RuntimeException(sprintf('FilesystemLoader baseDir must be a directory: %s', $baseDir));
    }
    */

    if (array_key_exists('extension', $options)) {
      if (empty($options['extension'])) {
        $this->extension = '';
      } else {
        $this->extension = '.' . ltrim($options['extension'], '.');
      }
    }
  }

  /**
   * Load a Template by name.
   *
   *   $loader = new Mustache_Loader_FilesystemLoader(dirname(__FILE__).'/views');
   *   $loader->load('admin/dashboard'); // loads "./views/admin/dashboard.mustache";
   *
   * @param string $name
   *
   * @return string Mustache Template source
   */
  public function load($name)
  {
    return $this->loadFile($name);
  }

  /**
   * Helper function for loading a Mustache file by name.
   *
   * @throws Mustache_Exception_UnknownTemplateException If a template file is not found.
   *
   * @param string $name
   *
   * @return string Mustache Template source
   */
  protected function loadFile($name)
  {
    if(!isset($this->templates[$name])) {
      $hasFile = false;

      foreach($this->baseDir as $path) {
        $fileName = $this->getFileName($name, $path);
        
        if(file_exists($fileName)) {
          $hasFile = true;
          $this->templates[$name] = preg_replace('/[\n\r\t]/', '', file_get_contents($fileName));
          break; 
        }
      }

      if ($hasFile == false) throw new Mustache_Exception_UnknownTemplateException($name);
    }

    return $this->templates[$name];
  }

  /**
   * Helper function for getting a Mustache template file name.
   *
   * @param string $name
   *
   * @return string Template file name
   */
  protected function getFileName($name, $baseDir)
  {
    $fileName = $baseDir . '/' . str_replace('_', '/', $name);

    if (substr($fileName, 0 - strlen($this->extension)) !== $this->extension) {
      $fileName .= $this->extension;
    }

    return $fileName;
  }
}
