<?php
  //SMTP送信 - 是否要啓用SMTP送信 Gmail和Amazon SES請設定: 開啓SMTP
  CM_Conf::set('EMAIL.SMTP', 'false');
  //SMTP主機 - SMTP主機的ip位置或是網址 Gmail請設定: smtp.gmail.com Amazon SES請設定: email-smtp.us-east-1.amazonaws.com
  CM_Conf::set('EMAIL.SMTP_HOST', 'localhost');
  //SMTP驗證 - SMTP是否需要驗證 Gmail和Amazon SES請設定: 需要
  CM_Conf::set('EMAIL.SMTP_AUTH', 'false');
  //SMTP驗證方式 - SMTP的驗證方式設定 Gmail請設定: SSL Amazon SES請設定: TLS
  CM_Conf::set('EMAIL.SMTP_SECURE', '');
  //SMTP通訊埠 - SMTP的通訊埠Port設定 Gmail請設定: 465 Amazon SES請設定: 587
  CM_Conf::set('EMAIL.SMTP_PORT', '25');
  //SMTP帳號 - 登入SMTP用的帳號
  CM_Conf::set('EMAIL.SMTP_USERNAME', '');
  //SMTP密碼 - 登入SMTP用的密碼
  CM_Conf::set('EMAIL.SMTP_PASSWORD', '');
  //寄件人名稱 - 信件內的寄件人名稱
  CM_Conf::set('EMAIL.FORM_NAME', 'ContentManager Ver3.0');
  //寄件人Mail - 信件內的寄件人Mail
  CM_Conf::set('EMAIL.FORM_MAIL', 'service@dgfactor.com');
  //開啓郵件回覆 - 是否在信件中設定郵件回覆
  CM_Conf::set('EMAIL.REPLY', 'true');
  //回覆名稱 - 回復信件的名稱
  CM_Conf::set('EMAIL.REPLY_NAME', 'ContentManager Ver3.0');
  //回覆Mail - 回復信件的Mail
  CM_Conf::set('EMAIL.REPLY_MAIL', 'service@dgfactor.com');
