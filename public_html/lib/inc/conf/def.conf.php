<?php
  //操作記錄 - 設定是否開啓後台管理系統操作記錄
  CM_Conf::set('DEF.ADMIN_LOG', '');
  //網站模式 - 設定網站的模式
  CM_Conf::set('DEF.DEBUG_WEBSITE', 'debug_server');
  //管理系統模式 - 設定管理系統的模式
  CM_Conf::set('DEF.DEBUG_WEBMGR', 'debug_server');
  //系統時區 - 設定系統的時區，php 5.3以上必須設定
  CM_Conf::set('DEF.TIME_ZONE', 'Asia/Taipei');
  //Session存活時間 - 設定Session多久以後會失效(單位：秒)
  CM_Conf::set('DEF.SESSION_LIFE', '7200');
  //管理系統語言(界面) - 預設的管理系統界面的語言
  CM_Conf::set('DEF.LANG_ADMIN_DEF', '');
  //管理系統語言(資料) - 預設的管理系統資料的語言
  CM_Conf::set('DEF.LANG_DATA_ADMIN_DEF', '');
  //客服信箱 - 設定客服信箱(接收用)
  CM_Conf::set('DEF.SERVICE_MAIL', 'service@ccdntech.com');
  //捷徑數量 - 顯示在快捷列上面的捷徑數量
  CM_Conf::set('DEF.SHORTCUT_COUNT', '');
  //模式 - 前台網站運作模式
  CM_Conf::set('DEF.SITEMODE_TYPE', 'normal');
  //驗證模式訊息 - 前台網站運作模式顯示的訊息
  CM_Conf::set('DEF.SITEMODE_AUTH_TEXT', '本網站目前不開放，
請登入後繼續');
  //驗證模式帳號 - 前台網站運作模式為驗證時的帳號
  CM_Conf::set('DEF.SITEMODE_AUTH_ACCOUNT', 'dgfactor');
  //驗證模式密碼 - 前台網站運作模式為驗證時的密碼
  CM_Conf::set('DEF.SITEMODE_AUTH_PASSWORD', '12853714');
  //關閉訊息 - 模式選擇關閉後前台顯示的文字
  CM_Conf::set('DEF.SITEMODE_OFF_TEXT', '親愛的用戶您好，
本網站因為某因素暫時關閉，
請聯絡網站管理者或是稍後在試！');
  //圖片驗證Public Key - 從Google申請的圖片Public Key
  CM_Conf::set('DEF.RECAPTCHA_PUBLIC_KEY', '6Lem4_MSAAAAAOi_UObwcTOE0yi9lmOpuzI3rocw');
  //圖片驗證Private Key - 從Google申請的圖片Private Key
  CM_Conf::set('DEF.RECAPTCHA_PRIVATE_KEY', '6Lem4_MSAAAAACC1bQPEpZUWDnzmr8Jhlg6RNVMy');
  //License Key - 授權Key
  CM_Conf::set('DEF.LICENSE_KEY', 'VAlQWlkFVlMHWQBeAgEJBAMBAQsAClMEAgdUUlIPVgMZB1dNUgpID08EBlgKWVMAAxILCVMAB19VDlUDBgAJV1VcBFVeBAVaUQ1eWlgDBFVVUEIMAwMKBVdWXQBfGAICAAUAWVELBgAKCANaXQEHW1BWVwRTAFMKBQEPUwMF');
  //前台多語系模式 - 前台多語系模式，開啟後會在網址上加入該語系的id 多語系模式關閉網址：http://www.exp.com/news/ 多語系模式開啟網址：http://www.exp.com/zh-tw/news/
  CM_Conf::set('DEF.SITE_LANG_MODE', 'true');
  //前台預設語言 - 預設前台顯示的語言
  CM_Conf::set('DEF.SITE_LANG_DEFAULT', '1');
  //Session寫入資料庫 - 將前台的Session寫入資料庫(會增加資料庫的負擔，除特定狀況請關閉此選項)
  CM_Conf::set('DEF.SITE_SESSION_DB', 'false');
  //Session寫入資料庫 - 將管理系統的Session寫入資料庫(會增加資料庫的負擔，除特定狀況請關閉此選項)
  CM_Conf::set('DEF.WEBMGR_SESSION_DB', 'false');
