<?php
  //檔案上傳數量 - 預設上傳檔案的數量
  CM_Conf::set('UPLOAD.COUNT', '1');
  //自動開始上傳 - 預設選擇檔案後是否直接上傳
  CM_Conf::set('UPLOAD.AUTO', 'true');
  //檔案最大限制 - 預設上傳檔案的最大限制，單位: bytes
  CM_Conf::set('UPLOAD.MAX_SIZE', '2050000000');
  //檔案最小限制 - 預設上傳檔案的最小限制，單位: bytes
  CM_Conf::set('UPLOAD.MIN_SIZE', '10');
  //允許的副檔名 - 預設上傳檔案的副檔名
  CM_Conf::set('UPLOAD.ALLOW_EXT', 'gif,jpg,jpeg,png,doc,docx,xls,xlsx,ppt,pptx,flv,mp4,mpg,wmv,vob,avi,mov,mp3,txt,pdf,swf,wma');
