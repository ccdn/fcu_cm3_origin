<?php
  $daoKey = 'DAO_AccountGroup';
  $daoColumnRecordId = 'id';
  $daoColumnHeadline = 'name';
  
  return array(
    'column' => array(
      $daoColumnRecordId => array(
        'default' => 'record_id',
        'params' => array('label' => CM_Lang::line("{$daoKey}.COLUMN_ID"))
      ),
      $daoColumnHeadline => array(
        'default' => 'name',
        'params' => array('label' => CM_Lang::line("{$daoKey}.COLUMN_NAME"))
      ),
      'permission' => array(
        'params' => array(
          'label' => CM_Lang::line("{$daoKey}.COLUMN_PERMISSION"),
          'f_type' => 'textarea'
        )
      ),
      'video_catalog' => array(
        'params' => array(
          'label' => CM_Lang::line("{$daoKey}.COLUMN_VIDEO_CATALOG"),
          'required' => true,
          'f_type' => 'select_m',
          'f_form_params' => array('tip' => CM_Lang::line("{$daoKey}.COLUMN_VIDEO_CATALOG_TIP"))
        )
      ),
      'sound_catalog' => array(
        'params' => array(
          'label' => CM_Lang::line("{$daoKey}.COLUMN_SOUND_CATALOG"),
          'required' => true,
          'f_type' => 'select_m',
          'f_form_params' => array('tip' => CM_Lang::line("{$daoKey}.COLUMN_SOUND_CATALOG_TIP"))
        )
      ),
      'picture_catalog' => array(
        'params' => array(
          'label' => CM_Lang::line("{$daoKey}.COLUMN_PICTURE_CATALOG"),
          'required' => true,
          'f_type' => 'select_m',
          'f_form_params' => array('tip' => CM_Lang::line("{$daoKey}.COLUMN_PICTURE_CATALOG_TIP"))
        )
      )
    ),
    'column_group' => array(
      'add' => array($daoColumnHeadline, 'video_catalog', 'sound_catalog', 'picture_catalog'),
      'edit' => array($daoColumnHeadline, 'video_catalog', 'sound_catalog', 'picture_catalog'),
      'list' => array($daoColumnRecordId, $daoColumnHeadline, 'video_catalog', 'sound_catalog', 'picture_catalog'),
      'option' => array($daoColumnRecordId, $daoColumnHeadline),
      'permission' => array($daoColumnHeadline, 'permission')
    ),
    'form_group' => array()
  );
?>