<?php
  $daoKey = 'DAO_Question';
  $daoColumnRecordId = 'id';
  $daoColumnHeadline = 'headline';

  return array(
    'column' => array(
      $daoColumnRecordId => array(
        'default' => 'record_id',
        'params' => array('label' => CM_Lang::line("{$daoKey}.COLUMN_ID"))
      ),
      'catalog' => array(
        'params' => array(
          'label' => CM_Lang::line("{$daoKey}.COLUMN_CATALOG"),
          'f_type' => 'select'
        )
      ),
      'sort' => array(
        'params' => array(
          'label' => CM_Lang::line("{$daoKey}.COLUMN_SORT"),
          'validateType' => 'digits'
        )
      ),
      'status' => array(
        'params' => array(
          'label' => CM_Lang::line("{$daoKey}.COLUMN_STATUS"),
          'f_type' => 'select'
        )
      ),
      'create_date' => array(
        'params' => array('label' => CM_Lang::line("{$daoKey}.COLUMN_CREATE_DATE"))
      ),
      'modify_date' => array(
        'params' => array('label' => CM_Lang::line("{$daoKey}.COLUMN_MODIFY_DATE"))
      )
    ),
    'column_lang' => array(
      'lang' => array(
        'params' => array(
          'label' => CM_Lang::line("{$daoKey}.COLUMN_LANG"),
          'readonly' => true
        )
      ),
      $daoColumnHeadline => array(
        'default' => 'name',
        'params' => array('label' => CM_Lang::line("{$daoKey}.COLUMN_HEADLINE"))
      ),
      'desc' => array(
        'default' => 'editor',
        'params' => array('label' => CM_Lang::line("{$daoKey}.COLUMN_DESC"))
      ),
      'lang_status' => array(
        'params' => array(
          'label' => CM_Lang::line("{$daoKey}.COLUMN_LANG_STATUS"),
          'f_type' => 'hidden'
        )
      )
    ),
    'column_group' => array(
      'add' => array('catalog', 'sort', 'status', 'create_date', 'modify_date', $daoColumnHeadline, 'desc', 'lang_status'),
      'edit' => array('catalog', 'sort', 'status', 'modify_date', $daoColumnHeadline, 'desc', 'lang_status'),
      'list' => array($daoColumnRecordId, $daoColumnHeadline, 'sort', 'status', 'create_date'),
      'status' => array('status', 'modify_date')
    ),
    'form_group' => array(
      'info' => array($daoColumnHeadline, 'desc'),
      'setting' => array('catalog', 'sort', 'status', 'lang_status')
    )
  );
?>