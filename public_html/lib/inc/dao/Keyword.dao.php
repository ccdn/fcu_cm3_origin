<?php
  $daoKey = 'DAO_Keyword';
  $daoColumnRecordId = 'id';
  $daoColumnHeadline = 'headline';
  
  return array(
    'column' => array(
      $daoColumnRecordId => array(
        'default' => 'record_id',
        'params' => array('label' => CM_Lang::line("{$daoKey}.COLUMN_ID"))
      ),
      $daoColumnHeadline => array(
        'default' => 'name',
        'params' => array('label' => CM_Lang::line("{$daoKey}.COLUMN_HEADLINE"))
      ),
      'search_count' => array(
        'params' => array(
          'label' => CM_Lang::line("{$daoKey}.COLUMN_SEARCH_COUNT"),
          'validateType' => 'digits'
        )
      ),
      'create_date' => array(
        'params' => array('label' => CM_Lang::line("{$daoKey}.COLUMN_CREATE_DATE"))
      ),
      'modify_date' => array(
        'params' => array('label' => CM_Lang::line("{$daoKey}.COLUMN_MODIFY_DATE"))
      )
    ),
    'column_group' => array(
      'add' => array($daoColumnHeadline, 'search_count', 'create_date', 'modify_date'),
      'edit' => array($daoColumnHeadline, 'search_count', 'modify_date'),
      'list' => array($daoColumnRecordId, $daoColumnHeadline, 'search_count', 'create_date'),
      'option' => array($daoColumnRecordId, $daoColumnHeadline)
    ),
    'form_group' => array(
      'info' => array($daoColumnHeadline, 'search_count')
    )
  );
?>