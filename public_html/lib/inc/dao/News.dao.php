<?php
  $daoKey = 'DAO_News';
  $daoColumnRecordId = 'id';
  $daoColumnHeadline = 'headline';
      
  return array(
    'column' => array(
      $daoColumnRecordId => array(
        'default' => 'record_id',
        'params' => array('label' => CM_Lang::line("{$daoKey}.COLUMN_ID"))
      ),
      'catalog' => array(
      	'params' => array(
      	'label' => CM_Lang::line("{$daoKey}.COLUMN_CATALOG"),
      	'f_type' => 'select'
      	)
      ),
      'date' => array(
        'default' => 'date',
        'params' => array('label' => CM_Lang::line("{$daoKey}.COLUMN_DATE"))
      ),
      'sort' => array(
        'params' => array(
          'label' => CM_Lang::line("{$daoKey}.COLUMN_SORT"),
          'validateType' => 'digits',
          'f_class' => 'input-small'
        )
      ),
      'status' => array(
        'params' => array(
          'label' => CM_Lang::line("{$daoKey}.COLUMN_STATUS"),
          'f_type' => 'select'
        )
      ),
      'create_date' => array(
        'params' => array('label' => CM_Lang::line("{$daoKey}.COLUMN_CREATE_DATE"))
      ),
      'modify_date' => array(
        'params' => array('label' => CM_Lang::line("{$daoKey}.COLUMN_MODIFY_DATE"))
      ),
      'start_date' => array(
      		'default' => 'datetime',
      		'params' => array(
      				'label' => CM_Lang::line("{$daoKey}.COLUMN_START_DATE"),
      				'required' => true
      		)
      ),
      'end_date' => array(
      		'default' => 'datetime',
      		'params' => array(
      				'label' => CM_Lang::line("{$daoKey}.COLUMN_END_DATE"),
      				'required' => true
      		)
      )
    ),
    'column_lang' => array(
      'lang' => array(
        'params' => array(
          'label' => CM_Lang::line("{$daoKey}.COLUMN_LANG"),
          'readonly' => true
        )
      ),
      $daoColumnHeadline => array(
        'default' => 'name',
        'params' => array('label' => CM_Lang::line("{$daoKey}.COLUMN_HEADLINE"))
      ),
      'desc' => array(
      		'default' => 'editor',
      		'params' => array('label' => CM_Lang::line("{$daoKey}.COLUMN_DESC"))
      ),     
      'file' => array(
        'default' => 'upload',
        'params' => array(
          'label' => CM_Lang::line("{$daoKey}.COLUMN_FILE"),
          'fu_options' => array(          
            'maxNumberOfFiles' => 1
          )
        )
      ),
      'lang_status' => array(
        'params' => array(
          'label' => CM_Lang::line("{$daoKey}.COLUMN_LANG_STATUS"),
          'f_type' => 'select',
          'f_options' => array()
        )
      ),
    ),
    'column_group' => array(
      'add' => array($daoColumnRecordId, 'catalog', 'file', $daoColumnHeadline, 'desc', 'sort', 'status', 'lang_status', 'create_date', 'modify_date', 'start_date', 'end_date'),
      'edit' => array($daoColumnRecordId, 'catalog', 'file', 'sort', $daoColumnHeadline, 'desc', 'status', 'lang_status', 'modify_date', 'start_date', 'end_date'),
      'list' => array($daoColumnRecordId, $daoColumnHeadline, 'sort', 'status', 'start_date', 'end_date'),
      'status' => array('status', 'modify_date'),
      //後臺SORT編輯
      'sort_edit' => array($daoColumnRecordId, 'sort'),
      'sort_log' => array($daoColumnRecordId, $daoColumnHeadline, 'sort')
    ),
    'form_group' => array(
      'info' => array($daoColumnHeadline, 'desc', 'start_date', 'end_date', 'file'),
      'setting' => array('catalog', 'sort', 'status', 'lang_status')
    )
  );
?>