<?php
  $daoKey = 'DAO_File';
  $daoColumnRecordId = 'file';
  $daoColumnHeadline = 'name';
  return array(
    'column' => array(
      $daoColumnRecordId => array(
        'params' => array(
          'label' => CM_Lang::line("{$daoKey}.COLUMN_FILE"),
          'readonly' => true
        )
      ),
      $daoColumnHeadline => array(
        'params' => array('label' => CM_Lang::line("{$daoKey}.COLUMN_NAME"))
      ),
      'desc' => array(
        'params' => array(
          'label' => CM_Lang::line("{$daoKey}.COLUMN_DESC"),
          'f_type' => 'textarea'
        )
      ),
      'size' => array(
        'params' => array(
          'label' => CM_Lang::line("{$daoKey}.COLUMN_SIZE"),
          'readonly' => true
        )
      ),
      'link' => array(
        'params' => array(
          'label' => CM_Lang::line("{$daoKey}.COLUMN_LINK"),
          'validateType' => 'digits'
        )
      ),
	  'dao' => array(
        'params' => array(
          'label' => CM_Lang::line("{$daoKey}.COLUMN_DAO")
        )
      ),
      'create_date' => array(
        'params' => array('label' => CM_Lang::line("{$daoKey}.COLUMN_CREATE_DATE"))
      ),
      'modify_date' => array(
        'params' => array('label' => CM_Lang::line("{$daoKey}.COLUMN_MODIFY_DATE"))
      )
    ),
    'column_group' => array(
      //'add' => array($daoColumnRecordId, $daoColumnHeadline, 'desc', 'size', 'link', 'create_date', 'modify_date'),
	  //150407:ANN:�s�W���
	  'add' => array($daoColumnRecordId,'dao',$daoColumnHeadline, 'desc', 'size', 'link', 'create_date', 'modify_date'),
      'edit' => array($daoColumnHeadline, 'desc', 'modify_date'),
      'list' => array($daoColumnRecordId, $daoColumnHeadline, 'size', 'create_date'),
      'option' => array($daoColumnRecordId, $daoColumnHeadline),
      'callback' => array($daoColumnHeadline, 'desc', 'size'),
	  //api
      'process' => array($daoColumnRecordId, 'dao', $daoColumnHeadline, 'desc', 'size', 'link', 'create_date', 'modify_date')
    ),
    'form_group' => array(
      'info' => array($daoColumnHeadline, 'desc')
    )
  );
?>