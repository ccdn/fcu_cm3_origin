<?php
  $daoKey = 'DAO_Option';
  $daoColumnRecordId = 'id';
  $daoColumnHeadline = 'headline';
      
  return array(
    'column' => array(
      $daoColumnRecordId => array(
        'default' => 'record_id',
        'params' => array('label' => CM_Lang::line("{$daoKey}.COLUMN_ID"))
      ),
      'catalog' => array(
        'default' => 'record_id',
        'params' => array('label' => CM_Lang::line("{$daoKey}.COLUMN_CATALOG_ID"))
      ),
      'status' => array(
        'params' => array(
          'label' => CM_Lang::line("{$daoKey}.COLUMN_STATUS"),
          'f_type' => 'select',
          'f_class' => 'input-small',
        )
      ),
      'create_date' => array(
        'params' => array('label' => CM_Lang::line("{$daoKey}.COLUMN_CREATE_DATE"))
      ),
      'modify_date' => array(
        'params' => array('label' => CM_Lang::line("{$daoKey}.COLUMN_MODIFY_DATE"))
      )
    ),
    'column_lang' => array(
      'lang' => array(
        'params' => array(
          'label' => CM_Lang::line("{$daoKey}.COLUMN_LANG"),
          'readonly' => true
        )
      ),
      $daoColumnHeadline => array(
        'default' => 'name',
        'params' => array('label' => CM_Lang::line("{$daoKey}.COLUMN_HEADLINE"))
      ),
      'val' => array(
        'default' => 'name',
        'params' => array(
          'label' => CM_Lang::line("{$daoKey}.COLUMN_VAL"),
          'required' => false
        )
      ),
      'file' => array(
        'default' => 'upload',
        'params' => array(
          'label' => CM_Lang::line("{$daoKey}.COLUMN_FILE"),
          'fu_options' => array(          
            'maxNumberOfFiles' => 1
          )
        )
      ),
      'note' => array(
        'params' => array(
          'label' => CM_Lang::line("{$daoKey}.COLUMN_NOTE"),
          'f_type' => 'textarea'
        )
      ),
      'lang_status' => array(
        'params' => array(
          'label' => CM_Lang::line("{$daoKey}.COLUMN_LANG_STATUS"),
          'f_type' => 'select',
          'f_class' => 'input-small',
          'f_options' => array()
        )
      ),
    ),
    'column_group' => array(
      'add' => array($daoColumnRecordId, 'catalog', $daoColumnHeadline, 'val', 'file', 'note', 'status', 'lang_status', 'create_date', 'modify_date'),
      'edit' => array($daoColumnRecordId, $daoColumnHeadline, 'val', 'file', 'note', 'status', 'lang_status', 'modify_date'),
      'list' => array($daoColumnRecordId, $daoColumnHeadline, 'status', 'create_date', 'modify_date'),
      'status' => array('status', 'modify_date'),
      'option' => array($daoColumnRecordId, $daoColumnHeadline, 'val'),
      'catalog' => array($daoColumnRecordId, $daoColumnHeadline, 'file')
    ),
    'form_group' => array(
      'info' => array($daoColumnHeadline, 'file', 'val', 'note'),
      'setting' => array('status', 'lang_status')
    )
  );
?>