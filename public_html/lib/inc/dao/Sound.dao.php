<?php
  $daoKey = 'DAO_Sound';
  $daoColumnRecordId = 'id';
  $daoColumnHeadline = 'headline';
  
  return array(
    'column' => array(
      $daoColumnRecordId => array(
        'default' => 'record_id',
        'params' => array('label' => CM_Lang::line("{$daoKey}.COLUMN_ID"))
      ),
      'album_id' => array(
        'params' => array('label' => CM_Lang::line("{$daoKey}.COLUMN_ALBUM_ID"))
      ),
      $daoColumnHeadline => array(
        'default' => 'name',
        'params' => array('label' => CM_Lang::line("{$daoKey}.COLUMN_HEADLINE"))
      ),
      'file' => array(
        'default' => 'upload',
		//150407:ANN:換預設樣版(但它會判斷有沒有轉出mp4，沒轉出來不能修改檔案)
		//'default' => 'upload_t',
        'params' => array(
          'label' => CM_Lang::line("{$daoKey}.COLUMN_FILE"),
          'required' => true,
          'fu_options' => array(        
            'maxNumberOfFiles' => 1,
            'acceptFileTypes' => 'mp3,wma'
          ),
          'fu_params' => array(
            'prefix' => 'video'
          )
        )
      ),
      'desc' => array(
        'default' => 'editor',
        'params' => array('label' => CM_Lang::line("{$daoKey}.COLUMN_DESC"))
      ),
      'attr_1' => array(
        'default' => 'name',
        'params' => array(
          'label' => CM_Lang::line("{$daoKey}.COLUMN_ATTR_1"),
          'required' => false,
		  //'f_form_params' =>  array('tip' => CM_Lang::line("{$daoKey}.COLUMN_TIP_ATTR_1"))
        )
      ),
      'attr_2' => array(
        'default' => 'name',
        'params' => array(
          'label' => CM_Lang::line("{$daoKey}.COLUMN_ATTR_2"),
          'required' => false
        )
      ),
      'attr_3' => array(
        'default' => 'name',
        'params' => array(
          'label' => CM_Lang::line("{$daoKey}.COLUMN_ATTR_3"),
          'required' => false,
		  'f_form_params' =>  array('tip' => CM_Lang::line("{$daoKey}.COLUMN_TIP_ATTR_3"))
        )
      ),
      'attr_4' => array(
        'default' => 'name',
        'params' => array(
          'label' => CM_Lang::line("{$daoKey}.COLUMN_ATTR_4"),
		  'f_type' => 'select',
          'required' => false
        )
      ),
      'attr_5' => array(
        'default' => 'name',
        'params' => array(
          'label' => CM_Lang::line("{$daoKey}.COLUMN_ATTR_5"),
          'required' => false
        )
      ),
      'attr_6' => array(
        'default' => 'name',
        'params' => array(
          'label' => CM_Lang::line("{$daoKey}.COLUMN_ATTR_6"),
          'required' => false
        )
      ),
      'attr_7' => array(
        'default' => 'name',
        'params' => array(
          'label' => CM_Lang::line("{$daoKey}.COLUMN_ATTR_7"),
          'required' => false
        )
      ),
	  'process_ok' => array(
        'params' => array(
          'label' => CM_Lang::line("{$daoKey}.COLUMN_PROCESS_OK"),
          'f_type' => 'select'
        )
      ),
      'process_count' => array(
        'params' => array(
          'label' => CM_Lang::line("{$daoKey}.COLUMN_PROCESS_COUNT"),
          'validateType' => 'digits'
        )
      ),
      'sort' => array(
        'params' => array(
          'label' => CM_Lang::line("{$daoKey}.COLUMN_SORT"),
          'validateType' => 'digits'
        )
      ),
      'status' => array(
        'params' => array(
          'label' => CM_Lang::line("{$daoKey}.COLUMN_STATUS"),
          'f_type' => 'select'
        )
      ),
      'create_date' => array(
        'params' => array('label' => CM_Lang::line("{$daoKey}.COLUMN_CREATE_DATE"))
      ),
      'modify_date' => array(
        'params' => array('label' => CM_Lang::line("{$daoKey}.COLUMN_MODIFY_DATE"))
      )
    ),
    'column_group' => array(
      //'add' => array('album_id', $daoColumnHeadline, 'file', 'desc', 'attr_1', 'attr_2', 'attr_3', 'attr_4', 'attr_5', 'attr_6', 'attr_7', 'sort', 'status', 'create_date', 'modify_date'),
	  //'add' => array('album_id', $daoColumnHeadline, 'desc', 'attr_1', 'attr_2', 'attr_3', 'attr_4', 'attr_5', 'attr_6', 'attr_7', 'sort', 'status', 'create_date', 'modify_date'),
	  //'add' => array('album_id', $daoColumnHeadline, 'desc', 'attr_1', 'attr_2', 'attr_3', 'attr_4', 'attr_7', 'sort', 'status', 'create_date', 'modify_date'),
	  //150728:ANN:又改
	  'add' => array('album_id', $daoColumnHeadline, 'attr_1', 'sort', 'status', 'create_date', 'modify_date'),
      'add_list' => array('album_id', 'file', $daoColumnHeadline, 'desc', 'attr_1', 'attr_2', 'attr_3', 'attr_4', 'attr_5', 'attr_6', 'attr_7', 'sort', 'status', 'create_date', 'modify_date'),
	  //'edit' => array($daoColumnHeadline, 'file', 'desc', 'attr_1', 'attr_2', 'attr_3', 'attr_4', 'attr_5', 'attr_6', 'attr_7', 'sort', 'status', 'modify_date'),
	  //'edit' => array($daoColumnHeadline, 'file', 'desc', 'attr_1', 'attr_2', 'attr_3', 'attr_4', 'attr_7', 'sort', 'status', 'modify_date'),
	  //150728:ANN:又改
	  'edit' => array($daoColumnHeadline, 'file', 'attr_1', 'sort', 'status', 'modify_date'),
      //'list' => array($daoColumnRecordId, $daoColumnHeadline, 'attr_1', 'attr_2', 'attr_3', 'attr_4', 'attr_5', 'sort', 'status', 'create_date'),
	  //'list' => array($daoColumnRecordId, $daoColumnHeadline, 'attr_1', 'attr_2', 'attr_3', 'attr_4', 'attr_5', 'sort', 'status', 'process_ok', 'create_date'),
	  'list' => array($daoColumnRecordId, $daoColumnHeadline, 'sort', 'status', 'process_ok', 'create_date'),
      'option' => array($daoColumnRecordId, $daoColumnHeadline),
	  'status' => array('status', 'modify_date'),
	  'sound' => array('file'),
	  'process' => array('file', 'process_ok', 'process_count', 'modify_date'),
    ),
    'form_group' => array(
      'info' => array($daoColumnHeadline, 'file', 'desc', 'attr_1', 'attr_2', 'attr_3', 'attr_4', 'attr_5', 'attr_6', 'attr_7', 'sort', 'status')
    )
  );
?>