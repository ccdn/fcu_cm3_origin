<?php
  $daoKey = 'DAO_AdminShortcut';
  $daoTableName = 'admin_shortcut';
  $daoColumnRecordId = 'id';
  $daoColumnHeadline = 'headline';
  
  return array(
    'column' => array(
      $daoColumnRecordId => array(
        'default' => 'record_id',
        'params' => array('label' => CM_Lang::line("{$daoKey}.COLUMN_ID"))
      ),
      'account_id' => array(
        'default' => 'record_id',
        'params' => array('label' => CM_Lang::line("{$daoKey}.COLUMN_ACCOUNT_ID"))
      ),
      $daoColumnHeadline => array(
        'default' => 'name',
        'params' => array('label' => CM_Lang::line("{$daoKey}.COLUMN_HEADLINE"))
      ),
      'url' => array(
        'default' => 'url',
        'params' => array(
          'label' => CM_Lang::line("{$daoKey}.COLUMN_URL"),
          'required' => true
        )
      ),
      'sort' => array(
        'params' => array(
          'label' => CM_Lang::line("{$daoKey}.COLUMN_SORT"),
          'validateType' => 'digits'
        )
      ),
      'create_date' => array(
        'params' => array('label' => CM_Lang::line("{$daoKey}.COLUMN_CREATE_DATE"))
      ),
      'modify_date' => array(
        'params' => array('label' => CM_Lang::line("{$daoKey}.COLUMN_MODIFY_DATE"))
      )
    ),
    'column_group' => array(
      'add' => array($daoColumnHeadline, 'account_id', 'url', 'sort', 'create_date', 'modify_date'),
      'edit' => array($daoColumnHeadline, 'sort'),
      'list' => array($daoColumnRecordId, $daoColumnHeadline, 'url', 'sort', 'create_date', 'modify_date')
    ),
    'form_group' => array(
      'info' => array($daoColumnHeadline, 'sort')
    )
  );
?>