<?php
  $daoKey = 'DAO_Customers_Group';
  $daoColumnRecordId = 'id';
  $daoColumnHeadline = 'name';
  
  return array(
    'column' => array(
      $daoColumnRecordId => array(
        'default' => 'record_id',
        'params' => array('label' => CM_Lang::line("{$daoKey}.COLUMN_ID"))
      ),
      $daoColumnHeadline => array(
        'default' => 'name',
        'params' => array('label' => CM_Lang::line("{$daoKey}.COLUMN_NAME"))
      ),
      'permission' => array(
        'params' => array(
          'label' => CM_Lang::line("{$daoKey}.COLUMN_PERMISSION"),
          'f_type' => 'select_m'
        )
      ),
      'catalog' => array(
        'params' => array(
          'label' => CM_Lang::line("{$daoKey}.COLUMN_CATALOG"),
          'f_type' => 'select_m'
        )
      )
    ),
    'column_group' => array(
      'add' => array($daoColumnHeadline, 'permission', 'catalog'),
      'edit' => array($daoColumnHeadline, 'permission', 'catalog'),
      'list' => array($daoColumnRecordId, $daoColumnHeadline, 'permission', 'catalog'),
      'option' => array($daoColumnRecordId, $daoColumnHeadline)
    ),
    'form_group' => array()
  );
?>