<?php
  $daoKey = 'DAO_AdBanners';
  $daoColumnRecordId = 'id';
  $daoColumnHeadline = 'headline';
   
  return array(
    'column' => array(
      $daoColumnRecordId => array(
        'default' => 'record_id',
        'params' => array('label' => CM_Lang::line("{$daoKey}.COLUMN_ID"))
      ),
      'banner_section' => array(
        'params' => array(
          'label' => CM_Lang::line("{$daoKey}.COLUMN_BANNER_SECTION"),
          'required' => true,
          'f_type' => 'select',
          'f_class' => 'input-xlarge'
        )
      ),
      'file' => array(
        'default' => 'upload',
        'params' => array(
          'label' => CM_Lang::line("{$daoKey}.COLUMN_FILE"),
          'fu_options' => array(          
            'maxNumberOfFiles' => 1,
            'acceptFileTypes' => 'gif,jpg,jpeg,png'
          ),
          'required' => true
        )
      ),
      'url' => array(
        'default' => 'url',
        'params' => array(
          'label' => CM_Lang::line("{$daoKey}.COLUMN_URL"),
          'required' => true,
          'f_class' => 'input-xxlarge'
        )
      ),
      'target' => array(
        'params' => array(
          'label' => CM_Lang::line("{$daoKey}.COLUMN_TARGET"),
          'f_type' => 'select'
        )
      ),
      'status' => array(
        'params' => array(
          'label' => CM_Lang::line("{$daoKey}.COLUMN_STATUS"),
          'f_type' => 'select',
          'f_class' => 'input-small',
        )
      ),
      'sort' => array(
        'params' => array(
          'label' => CM_Lang::line("{$daoKey}.COLUMN_SORT"),
          'validateType' => 'digits',
          'f_class' => 'input-small'
        )
      ),
      'start_date' => array(
        'default' => 'date',
        'params' => array(
          'label' => CM_Lang::line("{$daoKey}.COLUMN_DATE_START"),
          'required' => true,
          'val' => date('Y-m-d')
        )
      ),
      'end_date' => array(
        'default' => 'date',
        'params' => array(
          'label' => CM_Lang::line("{$daoKey}.COLUMN_DATE_END"),
          'required' => true,
          'val' => date('Y-m-d', mktime(0, 0, 0, date('m'), date('d'), date('Y') + 1))
        )
      ),
      'create_date' => array(
        'params' => array('label' => CM_Lang::line("{$daoKey}.COLUMN_CREATE_DATE"))
      ),
      'modify_date' => array(
        'params' => array('label' => CM_Lang::line("{$daoKey}.COLUMN_MODIFY_DATE"))
      )
    ),
    'column_lang' => array(
      'lang' => array(
        'params' => array(
          'label' => CM_Lang::line("{$daoKey}.COLUMN_LANG"),
          'readonly' => true
        )
      ),
      $daoColumnHeadline => array(
        'default' => 'name',
        'params' => array('label' => CM_Lang::line("{$daoKey}.COLUMN_NAME"))
      ),
      'lang_status' => array(
        'params' => array(
          'label' => CM_Lang::line("{$daoKey}.COLUMN_LANG_STATUS"),
          'f_type' => 'select',
          'f_options' => array()
        )
      ),
    ),
    'column_group' => array(
      'add' => array($daoColumnRecordId, $daoColumnHeadline, 'banner_section', 'file', 'url', 'target', 'sort', 'status', 'start_date', 'end_date', 'create_date', 'modify_date'),
      'edit' => array($daoColumnHeadline, 'banner_section', 'file', 'url', 'target', 'sort', 'status', 'start_date', 'end_date', 'modify_date'),
      'list' => array($daoColumnRecordId, 'banner_section', 'file', $daoColumnHeadline, 'sort', 'status', 'start_date', 'end_date'),
      'banner' => array($daoColumnRecordId, $daoColumnHeadline, 'file', 'url', 'target'),
      'status' => array('status', 'modify_date'),
      //後臺SORT編輯
      'sort_edit' => array($daoColumnRecordId, 'sort'),
      'sort_log' => array($daoColumnRecordId, $daoColumnHeadline, 'sort')
    ),
    'form_group' => array(
      'info' => array($daoColumnHeadline, 'banner_section', 'file', 'url', 'target'),
      'setting' => array('sort', 'start_date', 'end_date', 'status', 'lang_status')
    )
  );
?>