<?php
  $daoKey = 'DAO_Link';
  $daoColumnRecordId = 'id';
  $daoColumnHeadline = 'headline';
      
  return array(
    'column' => array(
      $daoColumnRecordId => array(
        'default' => 'record_id',
        'params' => array('label' => CM_Lang::line("{$daoKey}.COLUMN_ID"))
      ),
      'catalog' => array(
        'params' => array(
          'label' => CM_Lang::line("{$daoKey}.COLUMN_CATALOG"),
          'f_type' => 'select'
        )
      ),
      'date' => array(
        'default' => 'date',
        'params' => array('label' => CM_Lang::line("{$daoKey}.COLUMN_DATE"))
      ),
      'sort' => array(
        'params' => array(
          'label' => CM_Lang::line("{$daoKey}.COLUMN_SORT"),
          'validateType' => 'digits',
          'f_class' => 'input-small'
        )
      ),
      'status' => array(
        'params' => array(
          'label' => CM_Lang::line("{$daoKey}.COLUMN_STATUS"),
          'f_type' => 'select'
        )
      ),
      'create_date' => array(
        'params' => array('label' => CM_Lang::line("{$daoKey}.COLUMN_CREATE_DATE"))
      ),
      'modify_date' => array(
        'params' => array('label' => CM_Lang::line("{$daoKey}.COLUMN_MODIFY_DATE"))
      )
    ),
    'column_lang' => array(
      'lang' => array(
        'params' => array(
          'label' => CM_Lang::line("{$daoKey}.COLUMN_LANG"),
          'readonly' => true
        )
      ),
      $daoColumnHeadline => array(
        'default' => 'name',
        'params' => array('label' => CM_Lang::line("{$daoKey}.COLUMN_LINK_TEXT"))
      ),
      'file' => array(
        'default' => 'upload',
        'params' => array(
          'label' => CM_Lang::line("{$daoKey}.COLUMN_FILE"),
          'fu_options' => array(          
            'maxNumberOfFiles' => 1
          )
        )
      ),
	  'desc' => array(
      		'default' => 'editor',
      		'params' => array('label' => CM_Lang::line("{$daoKey}.COLUMN_DESC"))
      ), 
      'url' => array(
        'default' => 'url',
        'params' => array(
          'label' => CM_Lang::line("{$daoKey}.COLUMN_LINK_URL"),
          'required' => true,
          'f_class' => 'input-xxlarge'
        )
      ),
      'lang_status' => array(
        'params' => array(
          'label' => CM_Lang::line("{$daoKey}.COLUMN_LANG_STATUS"),
          'f_type' => 'select',
          'f_options' => array()
        )
      ),
    ),
    'column_group' => array(
      'add' => array($daoColumnRecordId, 'catalog', 'file', $daoColumnHeadline, 'desc', 'url', 'sort', 'status', 'lang_status', 'create_date', 'modify_date'),
      'edit' => array($daoColumnRecordId, 'catalog', 'file', 'sort', $daoColumnHeadline, 'desc', 'url', 'status', 'lang_status', 'modify_date'),
      'list' => array($daoColumnRecordId, $daoColumnHeadline, 'sort', 'file', 'status'),
      'status' => array('status', 'modify_date'),
      //後臺SORT編輯
      'sort_edit' => array($daoColumnRecordId, 'sort'),
      'sort_log' => array($daoColumnRecordId, $daoColumnHeadline, 'sort')
    ),
    'form_group' => array(
      'info' => array($daoColumnHeadline, 'desc','file', 'url'),
      'setting' => array('catalog', 'sort', 'status', 'lang_status')
    )
  );
?>