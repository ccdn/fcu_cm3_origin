<?php
  $daoKey = 'DAO_Video';
  $daoColumnRecordId = 'id';
  $daoColumnHeadline = 'headline';

  return array(
    'column' => array(
      $daoColumnRecordId => array(
        'default' => 'record_id',
        'params' => array('label' => CM_Lang::line("{$daoKey}.COLUMN_ID"))
      ),
      'file' => array(
        //'default' => 'upload',
		'default' => 'upload_t',
        'params' => array(
          'label' => CM_Lang::line("{$daoKey}.COLUMN_FILE"),
          'required' => true,
          'fu_options' => array(        
            'maxNumberOfFiles' => 1,
            'acceptFileTypes' => 'mp4,mpg,wmv,vob,avi,mov,mp3'
          )
        )
      ),
      'file_size' => array(
        'params' => array(
          'label' => CM_Lang::line("{$daoKey}.COLUMN_FILE_SIZE"),
          'validateType' => 'digits'
        )
      ),
      'file_img' => array(
        'default' => 'upload',
        'params' => array(
          'label' => CM_Lang::line("{$daoKey}.COLUMN_FILE_IMG"),
          'fu_options' => array(
            'maxNumberOfFiles' => 1,
            'acceptFileTypes' => 'gif,jpg,jpeg,png'
          )
        )
      ),
      'type' => array(
        'params' => array(
          'label' => CM_Lang::line("{$daoKey}.COLUMN_TYPE"),
          'f_type' => 'select'
        )
      ),
      'catalog' => array(
        'params' => array(
          'label' => CM_Lang::line("{$daoKey}.COLUMN_CATALOG"),
          'f_type' => 'select_m',
		  'f_form_params' =>  array('tip' => CM_Lang::line("{$daoKey}.COLUMN_TIP_CATALOG"))
        )
      ),
      'copyright' => array(
        'params' => array(
          'label' => CM_Lang::line("{$daoKey}.COLUMN_COPYRIGHT"),
          'f_type' => 'select'
        )
      ),
      'copyright_status' => array(
        'params' => array(
          'label' => CM_Lang::line("{$daoKey}.COLUMN_COPYRIGHT_STATUS"),
          'f_type' => 'select'
        )
      ),
      'download_status' => array(
        'params' => array(
          'label' => CM_Lang::line("{$daoKey}.COLUMN_DOWNLOAD_STATUS"),
          'f_type' => 'select'
        )
      ),
      'start_date' => array(
        'default' => 'datetime',
        'params' => array('label' => CM_Lang::line("{$daoKey}.COLUMN_START_DATE"))
      ),
      'end_date' => array(
        'default' => 'datetime',
        'params' => array('label' => CM_Lang::line("{$daoKey}.COLUMN_END_DATE"))
      ),
      'release_date' => array(
        //'default' => 'datetime',
		//'default' => 'date',
        'params' => array(
			'label' => CM_Lang::line("{$daoKey}.COLUMN_RELEASE_DATE"),
			'required' => false,
			'f_form_params' =>  array('tip' => CM_Lang::line("{$daoKey}.COLUMN_TIP_RELEASE_DATE"))
		),

      ),
      'attr_1' => array(
        'default' => 'name',
        'params' => array(
          'label' => CM_Lang::line("{$daoKey}.COLUMN_ATTR_1"),
          'required' => false,
		  'f_form_params' =>  array('tip' => CM_Lang::line("{$daoKey}.COLUMN_TIP_ATTR_1"))
        )
      ),
      /*'attr_2' => array(
        'default' => 'name',
        'params' => array(
          'label' => CM_Lang::line("{$daoKey}.COLUMN_ATTR_2"),
          'required' => false
        )
      ),*/
	  'attr_2' => array(
        'params' => array(
          'label' => CM_Lang::line("{$daoKey}.COLUMN_ATTR_2"),
          'f_type' => 'select'
        )
      ),
      'attr_3' => array(
        'default' => 'name',
        'params' => array(
          'label' => CM_Lang::line("{$daoKey}.COLUMN_ATTR_3"),
          'required' => false
        )
      ),
	  //150209:ANN:新增影片來源
	  'attr_4' => array(
        'default' => 'name',
        'params' => array(
          'label' => CM_Lang::line("{$daoKey}.COLUMN_ATTR_4"),
          'required' => false
        )
      ),
      'video_length' => array(
        'params' => array(
          'label' => CM_Lang::line("{$daoKey}.COLUMN_VIDEO_LENGTH"),
          'validateType' => 'digits'
        )
      ),
      'video_original' => array(
        'params' => array(
          'label' => CM_Lang::line("{$daoKey}.COLUMN_VIDEO_ORIGINAL"),
          'f_type' => 'select'
        )
      ),
      'process_ok' => array(
        'params' => array(
          'label' => CM_Lang::line("{$daoKey}.COLUMN_PROCESS_OK"),
          'f_type' => 'select'
        )
      ),
      'process_count' => array(
        'params' => array(
          'label' => CM_Lang::line("{$daoKey}.COLUMN_PROCESS_COUNT"),
          'validateType' => 'digits'
        )
      ),
      'owner' => array(
        'params' => array('label' => CM_Lang::line("{$daoKey}.COLUMN_OWNER"))
      ),
      'status' => array(
        'params' => array(
          'label' => CM_Lang::line("{$daoKey}.COLUMN_STATUS"),
          'f_type' => 'select'
        )
      ),
      'login_status' => array(
        'params' => array(
          'label' => CM_Lang::line("{$daoKey}.COLUMN_LOGIN_STATUS"),
          'f_type' => 'select'
        )
      ),
      'create_date' => array(
        'params' => array('label' => CM_Lang::line("{$daoKey}.COLUMN_CREATE_DATE"))
      ),
      'modify_date' => array(
        'params' => array('label' => CM_Lang::line("{$daoKey}.COLUMN_MODIFY_DATE"))
      )
    ),
    'column_lang' => array(
      'lang' => array(
        'params' => array(
          'label' => CM_Lang::line("{$daoKey}.COLUMN_LANG"),
          'readonly' => true
        )
      ),
      $daoColumnHeadline => array(
        'default' => 'name',
        'params' => array('label' => CM_Lang::line("{$daoKey}.COLUMN_HEADLINE"))
      ),
      'headline_sub_1' => array(
        'default' => 'name',
        'params' => array(
          'label' => CM_Lang::line("{$daoKey}.COLUMN_HEADLINE_SUB_1"),
          'required' => false,
		  'f_form_params' =>  array('tip' => CM_Lang::line("{$daoKey}.COLUMN_TIP_SUB_1"))
        )
      ),
      'headline_sub_2' => array(
        'default' => 'name',
        'params' => array(
          'label' => CM_Lang::line("{$daoKey}.COLUMN_HEADLINE_SUB_2"),
          'required' => false,
		  'f_form_params' =>  array('tip' => CM_Lang::line("{$daoKey}.COLUMN_TIP_SUB_2"))
        )
      ),
      'headline_sub_3' => array(
        'default' => 'name',
        'params' => array(
          'label' => CM_Lang::line("{$daoKey}.COLUMN_HEADLINE_SUB_3"),
          'required' => false
        )
      ),
	  //150209:ANN:新增相關題名
	  'headline_sub_4' => array(
        'default' => 'name',
        'params' => array(
          'label' => CM_Lang::line("{$daoKey}.COLUMN_HEADLINE_SUB_4"),
          'required' => false
        )
      ),
      'desc' => array(
        'default' => 'editor',
        'params' => array('label' => CM_Lang::line("{$daoKey}.COLUMN_DESC"))
      ),
      'author' => array(
        'default' => 'name',
        'params' => array(
		'label' => CM_Lang::line("{$daoKey}.COLUMN_AUTHOR"),
		'required' => false
		)
      ),
      'release' => array(
        'default' => 'name',
        'params' => array('label' => CM_Lang::line("{$daoKey}.COLUMN_RELEASE"))
      ),
      'tags' => array(
        'default' => 'name',
        'params' => array(
          'label' => CM_Lang::line("{$daoKey}.COLUMN_TAGS"),
          'required' => false
        )
      ),
      'lang_status' => array(
        'params' => array(
          'label' => CM_Lang::line("{$daoKey}.COLUMN_LANG_STATUS"),
          'f_type' => 'hidden'
        )
      )
    ),
    'column_group' => array(
      //'add' => array('file', 'file_img', 'catalog', 'copyright', 'copyright_status', 'start_date', 'end_date', 'release_date', 'attr_1', 'attr_2', 'attr_3', 'video_original', 'owner', 'status', 'login_status', 'create_date', 'modify_date', $daoColumnHeadline, 'headline_sub_1', 'headline_sub_2', 'headline_sub_3', 'desc', 'author', 'release', 'tags', 'lang_status'),
	  //150316:ANN:檔案和metadata分離
	  'add' => array('catalog', 'copyright', 'copyright_status', 'start_date', 'end_date', 'release_date', 'attr_1', 'attr_2', 'attr_3', 'video_original', 'owner', 'status', 'login_status', 'create_date', 'modify_date', $daoColumnHeadline, 'headline_sub_1', 'headline_sub_2', 'headline_sub_3', 'desc', 'author', 'release', 'tags', 'lang_status'),
      'edit' => array('file', 'file_img', 'catalog', 'copyright', 'copyright_status', 'start_date', 'end_date', 'release_date', 'attr_1', 'attr_2', 'attr_3', 'video_original', 'status', 'login_status', 'modify_date', $daoColumnHeadline, 'headline_sub_1', 'headline_sub_2', 'headline_sub_3', 'desc', 'author', 'release', 'tags', 'lang_status'),
      'list' => array($daoColumnRecordId, $daoColumnHeadline, 'video_length', 'file', 'file_img', 'file_size', 'start_date', 'end_date', 'status', 'process_ok'),
      'status' => array('status', 'modify_date'),
      'option' => array($daoColumnRecordId, $daoColumnHeadline),
      //'process' => array('file_img', 'file_size', 'video_length', 'process_ok', 'process_count', 'modify_date'),
	  //150407:ANN:上傳修正
	  'process' => array('file', 'file_img', 'file_size', 'video_length', 'process_ok', 'process_count', 'modify_date'),
      'add_list' => array($daoColumnHeadline, 'owner', 'file', 'status', 'lang_status', 'start_date', 'end_date', 'release_date','create_date', 'modify_date'),
	  //150316:ANN:檔案和metadata分離
	  'video' => array('file', 'file_img', 'status', 'lang_status'),
	  //150428:ANN:輪播縮圖修正
	  'list_data' => array($daoColumnRecordId, $daoColumnHeadline, 'video_length', 'file', 'file_img', 'file_size', 'start_date', 'end_date', 'status', 'process_ok')
    ),
    'form_group' => array(
      //'info' => array('file', 'file_img', $daoColumnHeadline, 'headline_sub_1', 'headline_sub_3', 'headline_sub_2', 'desc', 'author', 'release', 'tags'),
      //'setting' => array('catalog', 'copyright', 'copyright_status', 'download_status', 'start_date', 'end_date', 'release_date', 'attr_1', 'attr_2', 'attr_3', 'video_original', 'status', 'login_status', 'lang_status')
	  'info' =>  array('file', 'file_img', $daoColumnHeadline, 'headline_sub_4', 'headline_sub_3', 'headline_sub_2', 'desc', 'author', 'headline_sub_1', 'release'),
	  'setting' => array('catalog', 'download_status', 'start_date', 'end_date', 'release_date', 'attr_1', 'attr_2', 'video_original', 'status', 'login_status', 'attr_4', 'lang_status')
    )
  );
?>