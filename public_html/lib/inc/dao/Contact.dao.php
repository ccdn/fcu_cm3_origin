<?php
  $daoKey = 'DAO_Contact';
  $daoColumnRecordId = 'id';
  $daoColumnHeadline = 'name';

      
  return array(
    'column' => array(
      $daoColumnRecordId => array(
        'default' => 'record_id',
        'params' => array('label' => CM_Lang::line("{$daoKey}.COLUMN_ID"))
      ),
      'sex' => array(
        'params' => array(
          'label' => CM_Lang::line("{$daoKey}.COLUMN_SEX"),
          'f_type' => 'select'
        )
      ),
      $daoColumnHeadline => array(
        'default' => 'name',
        'params' => array('label' => CM_Lang::line("{$daoKey}.COLUMN_NAME"))
      ),
      'email' => array(
        'default' => 'email',
        'params' => array('label' => CM_Lang::line("{$daoKey}.COLUMN_EMAIL"))
      ),
      'phone' => array(
        'default' => 'phone',
        'params' => array('label' => CM_Lang::line("{$daoKey}.COLUMN_PHONE"))
      ),
      'cellphone' => array(
        'default' => 'cellphone',
        'params' => array(
          'label' => CM_Lang::line("{$daoKey}.COLUMN_CELLPHONE"),
          'required' => true
        )
      ),
      'company' => array(
        'default' => 'name',
        'params' => array(
          'label' => CM_Lang::line("{$daoKey}.COLUMN_COMPANY"),
          'required' => false
        )
      ),
      'note' => array(
        'params' => array(
          'label' => CM_Lang::line("{$daoKey}.COLUMN_NOTE"),
          'f_type' => 'textarea',
          'required' => true
        )
      ),
      'ip' => array(
        'params' => array(
          'label' => CM_Lang::line("{$daoKey}.COLUMN_IP"),
          'f_type' => 'readonly'
        )
      ),
      'status' => array(
        'params' => array(
          'label' => CM_Lang::line("{$daoKey}.COLUMN_STATUS"),
          'f_type' => 'select',
          'f_class' => 'input-small',
        )
      ),
      'create_date' => array(
        'params' => array('label' => CM_Lang::line("{$daoKey}.COLUMN_CREATE_DATE"))
      ),
      'modify_date' => array(
        'params' => array('label' => CM_Lang::line("{$daoKey}.COLUMN_MODIFY_DATE"))
      )
    ),
    'column_group' => array(
      'add' => array($daoColumnHeadline, 'sex', 'email', 'phone', 'cellphone', 'company', 'note', 'ip', 'status', 'create_date', 'modify_date'),
      'edit' => array($daoColumnHeadline, 'sex', 'email', 'phone', 'company', 'note', 'status', 'modify_date'),
      'list' => array($daoColumnRecordId, $daoColumnHeadline, 'email', 'ip', 'status', 'create_date'),
      'status' => array('status', 'modify_date'),
      'site_mail' => array($daoColumnHeadline, 'phone', 'cellphone', 'sex', 'email', 'company', 'note'),
      'site_add' => array($daoColumnHeadline, 'phone', 'cellphone', 'sex', 'email', 'company', 'note', 'ip', 'status', 'create_date', 'modify_date')
    ),
    'form_group' => array(
      'info' => array($daoColumnHeadline, 'phone', 'cellphone', 'sex', 'email', 'company', 'note', 'status'),
      'site_info' => array($daoColumnHeadline, 'phone',  'cellphone', 'sex', 'email', 'company', 'note')
    )
  );
?>