<?php
  $daoKey = 'DAO_Search';
  $daoColumnRecordId = 'id';
  $daoColumnHeadline = 'headline';

  return array(
    'column' => array(
      $daoColumnRecordId => array(
        'default' => 'record_id',
        'params' => array('label' => CM_Lang::line("{$daoKey}.COLUMN_ID"))
      ),
      'file' => array(
        'default' => 'upload',
        'params' => array(
          'label' => CM_Lang::line("{$daoKey}.COLUMN_FILE"),
          'required' => true,
          'fu_options' => array(          
            'maxNumberOfFiles' => 1
          )
        )
      ),
      'file_img' => array(
        'default' => 'upload',
        'params' => array(
          'label' => CM_Lang::line("{$daoKey}.COLUMN_FILE_IMG"),
          'fu_options' => array(          
            'maxNumberOfFiles' => 1,
            'acceptFileTypes' => 'gif,jpg,jpeg,png'
          )
        )
      ),
      'type' => array(
        'params' => array(
          'label' => CM_Lang::line("{$daoKey}.COLUMN_TYPE"),
          'f_type' => 'select'
        )
      ),
      'catalog' => array(
        'params' => array(
          'label' => CM_Lang::line("{$daoKey}.COLUMN_CATALOG"),
          'f_type' => 'select_m'
        )
      ),
      'copyright' => array(
        'params' => array(
          'label' => CM_Lang::line("{$daoKey}.COLUMN_COPYRIGHT"),
          'f_type' => 'select'
        )
      ),
      'copyright_status' => array(
        'params' => array(
          'label' => CM_Lang::line("{$daoKey}.COLUMN_COPYRIGHT_STATUS"),
          'f_type' => 'select'
        )
      ),
      'download_status' => array(
        'params' => array(
          'label' => CM_Lang::line("{$daoKey}.COLUMN_DOWNLOAD_STATUS"),
          'f_type' => 'select'
        )
      ),
      'start_date' => array(
        'default' => 'datetime',
        'params' => array('label' => CM_Lang::line("{$daoKey}.COLUMN_START_DATE"))
      ),
      'end_date' => array(
        'default' => 'datetime',
        'params' => array('label' => CM_Lang::line("{$daoKey}.COLUMN_END_DATE"))
      ),
      'release_date' => array(
        'default' => 'datetime',
        'params' => array('label' => CM_Lang::line("{$daoKey}.COLUMN_RELEASE_DATE"))
      ),
      'video_length' => array(
        'params' => array(
          'label' => CM_Lang::line("{$daoKey}.COLUMN_VIDEO_LENGTH"),
          'validateType' => 'digits'
        )
      ),
      'process_ok' => array(
        'params' => array(
          'label' => CM_Lang::line("{$daoKey}.COLUMN_PROCESS_OK"),
          'f_type' => 'select'
        )
      ),
      'process_count' => array(
        'params' => array(
          'label' => CM_Lang::line("{$daoKey}.COLUMN_PROCESS_COUNT"),
          'validateType' => 'digits'
        )
      ),
      'owner' => array(
        'params' => array('label' => CM_Lang::line("{$daoKey}.COLUMN_OWNER"))
      ),
      'status' => array(
        'params' => array(
          'label' => CM_Lang::line("{$daoKey}.COLUMN_STATUS"),
          'f_type' => 'select'
        )
      ),
      'login_status' => array(
        'params' => array(
          'label' => CM_Lang::line("{$daoKey}.COLUMN_LOGIN_STATUS"),
          'f_type' => 'select'
        )
      ),
      'create_date' => array(
        'params' => array('label' => CM_Lang::line("{$daoKey}.COLUMN_CREATE_DATE"))
      ),
      'modify_date' => array(
        'params' => array('label' => CM_Lang::line("{$daoKey}.COLUMN_MODIFY_DATE"))
      )
    ),
    'column_lang' => array(
      'lang' => array(
        'params' => array(
          'label' => CM_Lang::line("{$daoKey}.COLUMN_LANG"),
          'readonly' => true
        )
      ),
      $daoColumnHeadline => array(
        'default' => 'name',
        'params' => array('label' => CM_Lang::line("{$daoKey}.COLUMN_HEADLINE"))
      ),
      'desc' => array(
        'default' => 'editor',
        'params' => array('label' => CM_Lang::line("{$daoKey}.COLUMN_DESC"))
      ),
      'author' => array(
        'default' => 'name',
        'params' => array('label' => CM_Lang::line("{$daoKey}.COLUMN_AUTHOR"))
      ),
      'release' => array(
        'default' => 'name',
        'params' => array('label' => CM_Lang::line("{$daoKey}.COLUMN_RELEASE"))
      ),
      'lang_status' => array(
        'params' => array(
          'label' => CM_Lang::line("{$daoKey}.COLUMN_LANG_STATUS"),
          'f_type' => 'hidden'
        )
      )
    ),
    'column_group' => array(
      'add' => array('file', 'file_img', 'type', 'catalog', 'copyright', 'copyright_status', 'start_date', 'end_date', 'release_date', 'owner', 'status', 'login_status', 'create_date', 'modify_date', $daoColumnHeadline, 'desc', 'author', 'release', 'lang_status'),
      'edit' => array('file', 'file_img', 'catalog', 'copyright', 'copyright_status', 'start_date', 'end_date', 'release_date', 'status', 'login_status', 'modify_date', $daoColumnHeadline, 'desc', 'author', 'release', 'lang_status'),
      'list' => array($daoColumnRecordId, $daoColumnHeadline, 'desc', 'file', 'file_img', 'owner', 'start_date', 'end_date', 'status', 'process_ok'),
      'status' => array('status', 'modify_date'),
      'option' => array($daoColumnRecordId, $daoColumnHeadline),
      'process' => array('video_length', 'process_ok', 'process_count', 'modify_date'),
      'add_list' => array($daoColumnHeadline, 'owner', 'type', 'file', 'status', 'lang_status', 'start_date', 'end_date', 'create_date', 'modify_date')
    ),
    'form_group' => array(
      'info' => array('file', 'file_img', $daoColumnHeadline, 'desc', 'author', 'release'),
      'setting' => array('catalog', 'copyright', 'copyright_status', 'download_status', 'start_date', 'end_date', 'release_date', 'status', 'login_status', 'lang_status')
    )
  );
?>