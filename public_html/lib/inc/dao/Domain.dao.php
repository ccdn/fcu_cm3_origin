<?php
  $daoKey = 'DAO_Domain';
  $daoColumnRecordId = 'id';
  $daoColumnHeadline = 'domain';

  return array(
    'column' => array(
      $daoColumnRecordId => array(
        'default' => 'record_id',
        'params' => array('label' => CM_Lang::line("{$daoKey}.COLUMN_ID"))
      ),
      $daoColumnHeadline => array(
        'default' => 'domain',
        'params' => array('label' => CM_Lang::line("{$daoKey}.COLUMN_NAME"))
      ),
      'status' => array(
        'params' => array(
          'label' => CM_Lang::line("{$daoKey}.COLUMN_STATUS"),
          'f_type' => 'select'
        )
      ),
      'modify_date' => array(
        'params' => array('label' => CM_Lang::line("{$daoKey}.COLUMN_MODIFY_DATE"))
      )
    ),
    'column_group' => array(
      'list' => array($daoColumnRecordId, $daoColumnHeadline, 'status', 'modify_date'),
    ),
    'form_group' => array(
      'info' => array($daoColumnHeadline, 'bir', 'sex', 'phone', 'cellphone', 'address', 'note'),
      'setting' => array('group_id', 'newsletter', 'bonus', 'email', 'password', 'status'),
      'site_info' => array($daoColumnHeadline, 'bir', 'sex', 'phone', 'cellphone', 'address'),
      'site_setting' => array('email', 'password', 'newsletter')
    )
  );
?>