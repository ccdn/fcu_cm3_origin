<?php
  $daoKey = 'DAO_Keyword_Filter';
  $daoColumnRecordId = 'id';
  $daoColumnHeadline = 'headline';
  
  return array(
    'column' => array(
      
      $daoColumnHeadline => array(
        'default' => 'name',
        'params' => array('label' => CM_Lang::line("{$daoKey}.COLUMN_HEADLINE"))
      )
      
    ),
    
  );
?>