<?php
  $daoKey = 'DAO_Stream_Log';
  $daoTableName = 'stream_log';
  $daoColumnRecordId = 'iLogRm_seq';
  $daoColumnHeadline = 'cFilename';
  
  return array(
    'column' => array(
      $daoColumnRecordId => array(
        'default' => 'record_id',
        'params' => array('label' => CM_Lang::line("{$daoKey}.COLUMN_ID"))
      ),
      $daoColumnHeadline => array(
        'default' => 'name',
        'params' => array('label' => CM_Lang::line("{$daoKey}.COLUMN_HEADLINE"))
      ),
      'tTimestamp' => array(
        'params' => array('label' => CM_Lang::line("{$daoKey}.COLUMN_T_TIMESTAMP"))
      ),
      'iMda_seq' => array(
        'params' => array('label' => CM_Lang::line("{$daoKey}.COLUMN_I_MDA_SEQ"))
      ),
      'iUsr_seq' => array(
        'params' => array('label' => CM_Lang::line("{$daoKey}.COLUMN_I_USR_SEQ"))
      ),
      'iUgp_seq' => array(
        'params' => array('label' => CM_Lang::line("{$daoKey}.COLUMN_I_UGP_SEQ"))
      ),
      'iConnected_time' => array(
        'params' => array('label' => CM_Lang::line("{$daoKey}.COLUMN_I_CONNECTED_TIME"))
      ),
      'cClient_ip' => array(
        'params' => array('label' => CM_Lang::line("{$daoKey}.COLUMN_C_CLIENT_IP"))
      ),
      'cClient_info' => array(
        'params' => array('label' => CM_Lang::line("{$daoKey}.COLUMN_C_CLIENT_INFO"))
      ),
      'iBytes_sent' => array(
        'params' => array('label' => CM_Lang::line("{$daoKey}.COLUMN_I_BYTES_SENT"))
      ),
      'cProtocol' => array(
        'params' => array('label' => CM_Lang::line("{$daoKey}.COLUMN_C_PROTOCOL"))
      ),
      'iFile_size' => array(
        'params' => array('label' => CM_Lang::line("{$daoKey}.COLUMN_I_FILE_SIZE"))
      )
    ),
    'column_group' => array(
      'list' => array($daoColumnRecordId, $daoColumnHeadline)
    ),
    'form_group' => array(
      'info' => array()
    )
  );
?>