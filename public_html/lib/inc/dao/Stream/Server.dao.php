<?php
  $daoKey = 'DAO_Stream_Server';
  $daoTableName = 'stream_server';
  $daoColumnRecordId = 'iVod_seq';
  $daoColumnHeadline = 'cVod_server_domain';
  
  return array(
    'column' => array(
      $daoColumnRecordId => array(
        'default' => 'record_id',
        'params' => array('label' => CM_Lang::line("{$daoKey}.COLUMN_ID"))
      ),
      $daoColumnHeadline => array(
        'default' => 'name',
        'params' => array('label' => CM_Lang::line("{$daoKey}.COLUMN_HEADLINE"))
      )
    ),
    'column_group' => array(
      'add' => array($daoColumnHeadline),
      'edit' => array($daoColumnHeadline),
      'list' => array($daoColumnRecordId, $daoColumnHeadline)
    ),
    'form_group' => array(
      'info' => array($daoColumnHeadline)
    )
  );
?>