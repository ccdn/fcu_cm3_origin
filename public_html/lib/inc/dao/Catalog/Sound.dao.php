<?php
  $daoKey = 'DAO_Catalog_Sound';
  $daoColumnRecordId = 'id';
  $daoColumnHeadline = 'headline';

  return array(
    'column' => array(
      $daoColumnRecordId => array(
        'default' => 'record_id',
        'params' => array('label' => CM_Lang::line("{$daoKey}.COLUMN_ID"))
      ),
      'parent_id' => array(
        //'default' => 'record_id',
        'params' => array('label' => CM_Lang::line("{$daoKey}.COLUMN_PARENT_ID"))
      ),
      'sort' => array(
        'params' => array(
          'label' => CM_Lang::line("{$daoKey}.COLUMN_SORT"),
          'validateType' => 'digits',
          'f_type' => 'hidden'
        )
      ),
      'status' => array(
        'params' => array(
          'label' => CM_Lang::line("{$daoKey}.COLUMN_STATUS"),
          'f_type' => 'select'
        )
      ),
      'create_date' => array(
        'params' => array('label' => CM_Lang::line("{$daoKey}.COLUMN_CREATE_DATE"))
      ),
      'modify_date' => array(
        'params' => array('label' => CM_Lang::line("{$daoKey}.COLUMN_MODIFY_DATE"))
      )
    ),
    'column_lang' => array(
      'lang' => array(
        'params' => array(
          'label' => CM_Lang::line("{$daoKey}.COLUMN_LANG"),
          'readonly' => true
        )
      ),
      $daoColumnHeadline => array(
        'default' => 'name',
        'params' => array('label' => CM_Lang::line("{$daoKey}.COLUMN_HEADLINE"))
      ),
      'lang_status' => array(
        'params' => array(
          'label' => CM_Lang::line("{$daoKey}.COLUMN_LANG_STATUS"),
          'f_type' => 'hidden'
        )
      )
    ),
    'column_group' => array(
      'add' => array('parent_id', 'status', 'create_date', 'modify_date', $daoColumnHeadline, 'lang_status'),
      'edit' => array('status', 'modify_date', $daoColumnHeadline, 'desc', 'file', 'lang_status'),
      'list' => array($daoColumnRecordId, $daoColumnHeadline, 'status'),
      'status' => array('status', 'modify_date'),
      'option' => array($daoColumnRecordId, $daoColumnHeadline),
      'sort' => array('sort', 'modify_date'),
      'parent' => array('parent_id', 'modify_date')
    ),
    'form_group' => array(
      'info' => array($daoColumnHeadline),
      'setting' => array('status', 'lang_status')
    )
  );
?>