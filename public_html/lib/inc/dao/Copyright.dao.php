<?php
  $daoKey = 'DAO_Copyright';
  $daoColumnRecordId = 'id';
  $daoColumnHeadline = 'headline';

  return array(
    'column' => array(
      $daoColumnRecordId => array(
        'default' => 'record_id',
        'params' => array('label' => CM_Lang::line("{$daoKey}.COLUMN_ID"))
      ),
      'sort' => array(
        'params' => array(
          'label' => CM_Lang::line("{$daoKey}.COLUMN_SORT"),
          'validateType' => 'digits',
          'f_type' => 'hidden'
        )
      ),
      'status' => array(
        'params' => array(
          'label' => CM_Lang::line("{$daoKey}.COLUMN_STATUS"),
          'f_type' => 'select'
        )
      ),
      'create_date' => array(
        'params' => array('label' => CM_Lang::line("{$daoKey}.COLUMN_CREATE_DATE"))
      ),
      'modify_date' => array(
        'params' => array('label' => CM_Lang::line("{$daoKey}.COLUMN_MODIFY_DATE"))
      )
    ),
    'column_lang' => array(
      'lang' => array(
        'params' => array(
          'label' => CM_Lang::line("{$daoKey}.COLUMN_LANG"),
          'readonly' => true
        )
      ),
      $daoColumnHeadline => array(
        'default' => 'name',
        'params' => array('label' => CM_Lang::line("{$daoKey}.COLUMN_HEADLINE"))
      ),
      'desc' => array(
        'default' => 'editor',
        'params' => array('label' => CM_Lang::line("{$daoKey}.COLUMN_DESC"))
      ),
      'file' => array(
        'default' => 'upload',
        'params' => array(
          'label' => CM_Lang::line("{$daoKey}.COLUMN_FILE"),
          'fu_options' => array(          
            'maxNumberOfFiles' => 1
          )
        )
      ),
      'lang_status' => array(
        'params' => array(
          'label' => CM_Lang::line("{$daoKey}.COLUMN_LANG_STATUS"),
          'f_type' => 'hidden'
        )
      )
    ),
    'column_group' => array(
      'add' => array('sort', 'status', 'create_date', 'modify_date', $daoColumnHeadline, 'desc', 'file', 'lang_status'),
      'edit' => array('sort', 'status', 'modify_date', $daoColumnHeadline, 'desc', 'file', 'lang_status'),
      'list' => array($daoColumnRecordId, $daoColumnHeadline, 'sort', 'status'),
      'status' => array('status', 'modify_date'),
      'option' => array($daoColumnRecordId, $daoColumnHeadline)
    ),
    'form_group' => array(
      'info' => array($daoColumnHeadline, 'desc', 'file'),
      'setting' => array('sort', 'status', 'lang_status')
    )
  );
?>