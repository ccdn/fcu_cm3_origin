<?php
  $daoKey = 'DAO_Node';
  $daoColumnRecordId = 'id';
  $daoColumnHeadline = 'headline';
      
  return array(
    'column' => array(
      $daoColumnRecordId => array(
        'default' => 'record_id',
        'params' => array('label' => CM_Lang::line("{$daoKey}.COLUMN_ID"))
      ),
      'parent_id' => array(
        'default' => 'name',
        'params' => array('label' => CM_Lang::line("{$daoKey}.COLUMN_PARENT_ID"))
      ),
      'path_name' => array(
        'default' => 'name',
        'params' => array('label' => CM_Lang::line("{$daoKey}.COLUMN_PATH_NAME"))
      ),
      'page_name' => array(
        'default' => 'name',
        'params' => array('label' => CM_Lang::line("{$daoKey}.COLUMN_PAGE_NAME"))
      ),
      'page_func' => array(
        'default' => 'name',
        'params' => array(
          'label' => CM_Lang::line("{$daoKey}.COLUMN_PAGE_FUNC"),
          'required' => false
        )
      ),
      'mod_params' => array(
        'params' => array(
          'label' => CM_Lang::line("{$daoKey}.COLUMN_MOD_PARAMS"),
          'f_type' => 'textarea'
        )
      ),
      'sort' => array(
        'params' => array(
          'label' => CM_Lang::line("{$daoKey}.COLUMN_SORT"),
          'validateType' => 'digits',
          'f_class' => 'input-small'
        )
      ),
      'menu_status' => array(
        'params' => array(
          'label' => CM_Lang::line("{$daoKey}.COLUMN_MENU_STATUS"),
          'f_type' => 'select',
          'f_class' => 'input-small'
        )
      ),
      'status' => array(
        'params' => array(
          'label' => CM_Lang::line("{$daoKey}.COLUMN_STATUS"),
          'f_type' => 'select',
          'f_class' => 'input-small'
        )
      ),
      'create_date' => array(
        'params' => array('label' => CM_Lang::line("{$daoKey}.COLUMN_CREATE_DATE"))
      ),
      'modify_date' => array(
        'params' => array('label' => CM_Lang::line("{$daoKey}.COLUMN_MODIFY_DATE"))
      )
    ),
    'column_lang' => array(
      'lang' => array(
        'params' => array(
          'label' => CM_Lang::line("{$daoKey}.COLUMN_LANG"),
          'readonly' => true
        )
      ),
      $daoColumnHeadline => array(
        'default' => 'name',
        'params' => array('label' => CM_Lang::line("{$daoKey}.COLUMN_HEADLINE"))
      ),
      'lang_status' => array(
        'params' => array(
          'label' => CM_Lang::line("{$daoKey}.COLUMN_LANG_STATUS"),
          'f_type' => 'select',
          'f_options' => array(),
          'f_class' => 'input-small',
        )
      ),
    ),
    'column_group' => array(
      'id' => array($daoColumnRecordId, 'parent_id', $daoColumnHeadline),
      'add' => array($daoColumnRecordId, $daoColumnHeadline, 'parent_id', 'path_name', 'page_name', 'page_func', 'mod_params', 'sort', 'status', 'lang_status', 'create_date', 'modify_date'),
      'edit' => array($daoColumnRecordId, $daoColumnHeadline, 'path_name', 'page_name', 'page_func', 'mod_params', 'status', 'lang_status', 'modify_date'),
      'list' => array($daoColumnRecordId, 'parent_id', $daoColumnHeadline, 'path_name', 'page_name', 'page_func', 'mod_params', 'sort', 'status'),
      'status' => array('status', 'modify_date'),
      'sort' => array('sort', 'modify_date'),
      'parent' => array('parent_id', 'modify_date')
    ),
    'form_group' => array(
      'info' => array($daoColumnHeadline, 'path_name', 'page_name', 'page_func', 'mod_params', 'status', 'lang_status')
    ) 
  );
?>