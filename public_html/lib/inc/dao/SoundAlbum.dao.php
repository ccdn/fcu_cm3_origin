<?php
  $daoKey = 'DAO_SoundAlbum';
  $daoColumnRecordId = 'id';
  $daoColumnHeadline = 'headline';
  
  return array(
    'column' => array(
      $daoColumnRecordId => array(
        'default' => 'record_id',
        'params' => array('label' => CM_Lang::line("{$daoKey}.COLUMN_ID"))
      ),
      $daoColumnHeadline => array(
        'default' => 'name',
        'params' => array('label' => CM_Lang::line("{$daoKey}.COLUMN_HEADLINE"))
      ),
      'catalog' => array(
        'params' => array(
          'label' => CM_Lang::line("{$daoKey}.COLUMN_CATALOG"),
          'f_type' => 'select_m'
        )
      ),
      'file' => array(
        'default' => 'upload',
        'params' => array(
          'label' => CM_Lang::line("{$daoKey}.COLUMN_FILE"),
          'required' => true,
          'fu_options' => array(        
            'maxNumberOfFiles' => 1,
            'acceptFileTypes' => 'jpg,png'
          )
        )
      ),
      'desc' => array(
        'default' => 'editor',
        'params' => array('label' => CM_Lang::line("{$daoKey}.COLUMN_DESC"))
      ),
      'class' => array(
        'default' => 'name',
        'params' => array(
          'label' => CM_Lang::line("{$daoKey}.COLUMN_CLASS"),
          'required' => false
        )
      ),
      'issuer' => array(
        'default' => 'name',
        'params' => array(
          'label' => CM_Lang::line("{$daoKey}.COLUMN_ISSUER"),
          'required' => false
        )
      ),
	   //製作單位
	  'headline_sub_1' => array(
        'default' => 'name',
        'params' => array(
          'label' => CM_Lang::line("{$daoKey}.COLUMN_HEADLINE_SUB_1"),
          'required' => false
        )
      ),
	  //關鍵字
      'headline_sub_2' => array(
        'default' => 'name',
        'params' => array(
          'label' => CM_Lang::line("{$daoKey}.COLUMN_HEADLINE_SUB_2"),
          'required' => false,
		  'f_form_params' =>  array('tip' => CM_Lang::line("{$daoKey}.COLUMN_TIP_SUB_2"))
        )
      ),
	  //系列名
      'headline_sub_3' => array(
        'default' => 'name',
        'params' => array(
          'label' => CM_Lang::line("{$daoKey}.COLUMN_HEADLINE_SUB_3"),
          'required' => false
        )
      ),
	  //相關題名
	  'headline_sub_4' => array(
        'default' => 'name',
        'params' => array(
          'label' => CM_Lang::line("{$daoKey}.COLUMN_HEADLINE_SUB_4"),
          'required' => false
        )
      ),
	  //索書號
	  'attr_1' => array(
        'default' => 'name',
        'params' => array(
          'label' => CM_Lang::line("{$daoKey}.COLUMN_ATTR_1"),
          'required' => false,
		  'f_form_params' =>  array('tip' => CM_Lang::line("{$daoKey}.COLUMN_TIP_ATTR_1"))
        )
      ),
	  //語言
	  'attr_2' => array(
        'params' => array(
          'label' => CM_Lang::line("{$daoKey}.COLUMN_ATTR_2"),
          'f_type' => 'select'
        )
      ),
	  //聲音資訊來源
	  'attr_3' => array(
        'default' => 'name',
        'params' => array(
          'label' => CM_Lang::line("{$daoKey}.COLUMN_ATTR_3"),
          'required' => false
        )
      ),
      'publish_date' => array(
        'default' => 'date',
        'params' => array('label' => CM_Lang::line("{$daoKey}.COLUMN_PUBLISH_DATE"))
      ),
      'start_date' => array(
        'default' => 'date',
        'params' => array('label' => CM_Lang::line("{$daoKey}.COLUMN_START_DATE"))
      ),
      'end_date' => array(
        'default' => 'date',
        'params' => array('label' => CM_Lang::line("{$daoKey}.COLUMN_END_DATE"))
      ),
      'status' => array(
        'params' => array(
          'label' => CM_Lang::line("{$daoKey}.COLUMN_STATUS"),
          'f_type' => 'select'
        )
      ),
	  //發行/出版日期
	  'release_date' => array(
        'params' => array(
			'label' => CM_Lang::line("{$daoKey}.COLUMN_RELEASE_DATE"),
			'required' => false,
			'f_form_params' =>  array('tip' => CM_Lang::line("{$daoKey}.COLUMN_TIP_RELEASE_DATE"))
		)
	  ),
      'status' => array(
        'params' => array(
          'label' => CM_Lang::line("{$daoKey}.COLUMN_STATUS"),
          'f_type' => 'select'
        )
      ),
	  'login_status' => array(
        'params' => array(
          'label' => CM_Lang::line("{$daoKey}.COLUMN_LOGIN_STATUS"),
          'f_type' => 'select'
        )
      ),
      'create_date' => array(
        'params' => array('label' => CM_Lang::line("{$daoKey}.COLUMN_CREATE_DATE"))
      ),
      'modify_date' => array(
        'params' => array('label' => CM_Lang::line("{$daoKey}.COLUMN_MODIFY_DATE"))
      )
    ),
    'column_group' => array(
      //'add' => array($daoColumnHeadline, 'catalog', 'file', 'desc', 'class', 'issuer', 'publish_date', 'start_date', 'end_date', 'status', 'create_date', 'modify_date'),
	  'add' => array($daoColumnHeadline, 'headline_sub_1', 'headline_sub_2', 'headline_sub_3', 'headline_sub_4', 'catalog', 'file', 'desc', 'issuer', 'attr_1', 'attr_2', 'attr_3', 'release_date', 'start_date', 'end_date', 'status', 'login_status', 'create_date', 'modify_date'),
      //'edit' => array($daoColumnHeadline, 'catalog', 'file', 'desc', 'class', 'issuer', 'publish_date', 'start_date', 'end_date', 'status', 'modify_date'),
	  'edit' => array($daoColumnHeadline,'file', 'headline_sub_1', 'headline_sub_2', 'headline_sub_3', 'headline_sub_4', 'desc', 'catalog', 'issuer','attr_1', 'attr_2', 'attr_3', 'release_date', 'start_date', 'end_date', 'status', 'login_status', 'modify_date'),
      //'list' => array($daoColumnRecordId, 'file', $daoColumnHeadline, 'publish_date', 'start_date', 'end_date', 'status', 'create_date'),
	  'list' => array($daoColumnRecordId, 'file', $daoColumnHeadline, 'start_date', 'end_date', 'status', 'create_date'),
      'option' => array($daoColumnRecordId, $daoColumnHeadline),
      'status' => array('status', 'modify_date')
    ),
    'form_group' => array(
      //'info' => array($daoColumnHeadline, 'catalog', 'file', 'desc', 'class', 'issuer', 'publish_date', 'start_date', 'end_date', 'status')
	  'info' => array($daoColumnHeadline, 'file','headline_sub_4', 'headline_sub_3', 'headline_sub_1', 'desc', 'catalog', 'headline_sub_2', 'issuer', 'release_date', 'start_date', 'end_date','attr_2', 'attr_1', 'status', 'login_status', 'attr_3' )
    )
  );
?>