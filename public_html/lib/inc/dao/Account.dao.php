<?php
  $daoKey = 'DAO_Account';
  $daoColumnRecordId = 'id';
  $daoColumnHeadline = 'name';
  
  return array(
    'column' => array(
      $daoColumnRecordId => array(
        'default' => 'record_id',
        'params' => array('label' => CM_Lang::line("{$daoKey}.COLUMN_ID"))
      ),
      'group_id' => array(
        'params' => array(
          'label' => CM_Lang::line("{$daoKey}.COLUMN_GROUPS_ID"),
          'required' => true,
          'f_type' => 'select_m'
        )
      ),
      $daoColumnHeadline => array(
        'default' => 'name',
        'params' => array('label' => CM_Lang::line("{$daoKey}.COLUMN_NAME"))
      ),
      'picture' => array(
        'default' => 'upload',
        'params' => array(
          'label' => CM_Lang::line("{$daoKey}.COLUMN_PICTURE"),
          'fu_options' => array(          
            'maxNumberOfFiles' => 1,
            'acceptFileTypes' => 'gif,jpg,jpeg,png'
          )
        )
      ),
      'email' => array(
        'default' => 'email',
        'params' => array('label' => CM_Lang::line("{$daoKey}.COLUMN_EMAIL"))
      ),
      'password' => array(
        'default' => 'password',
        'params' => array(
          'label' => CM_Lang::line("{$daoKey}.COLUMN_PASSWORD"),
          'f_confirm' => false
        )
      ),
      'password_new' => array(
        'default' => 'password',
        'params' => array(
          'label' => CM_Lang::line("{$daoKey}.COLUMN_PASSWORD_NEW"),
          'disable' => true,
          'f_confirm' => false
        )
      ),
      'phone' => array(
        'default' => 'phone',
        'params' => array('label' => CM_Lang::line("{$daoKey}.COLUMN_PHONE"))
      ),
      'cellphone' => array(
        'default' => 'cellphone',
        'params' => array('label' => CM_Lang::line("{$daoKey}.COLUMN_CELLPHONE"))
      ),
      'note' => array(
        'params' => array(
          'label' => CM_Lang::line("{$daoKey}.COLUMN_NOTE"),
          'f_type' => 'textarea'
        )
      ),
      'permission' => array(
        'params' => array(
          'label' => CM_Lang::line("{$daoKey}.COLUMN_NOTE"),
          'f_type' => 'textarea'
        )
      ),
      'status' => array(
        'params' => array(
          'label' => CM_Lang::line("{$daoKey}.COLUMN_STATUS"),
          'f_type' => 'select'
        )
      ),
      'create_date' => array(
        'params' => array('label' => CM_Lang::line("{$daoKey}.COLUMN_CREATE_DATE"))
      ),
      'modify_date' => array(
        'params' => array('label' => CM_Lang::line("{$daoKey}.COLUMN_MODIFY_DATE"))
      )
    ),
    'column_group' => array(
      'add' => array($daoColumnHeadline, 'group_id', 'email', 'password', 'status', 'create_date', 'modify_date'),
      'edit' => array($daoColumnHeadline, 'group_id', 'email', 'status', 'modify_date'),
      'list' => array($daoColumnRecordId, $daoColumnHeadline, 'group_id', 'email', 'status', 'create_date'),
      'export' => array($daoColumnRecordId, $daoColumnHeadline, 'group_id', 'email', 'status', 'create_date', 'modify_date'),
      'option' => array($daoColumnRecordId, $daoColumnHeadline)
    ),
    'form_group' => array(
      'account_contact' => array($daoColumnHeadline),
      'account_setting' => array('group_id', 'email', 'password', 'status', 'note')
    )
  );
?>