<?php
  $daoKey = 'DAO_File_Info';
  $daoColumnRecordId = 'file';
  $daoColumnHeadline = 'name';
  return array(
    'column' => array(
      $daoColumnRecordId => array(
        'params' => array(
          'label' => CM_Lang::line("{$daoKey}.COLUMN_FILE"),
          'readonly' => true
        )
      ),
      'record_id' => array(
        'params' => array(
          'label' => CM_Lang::line("{$daoKey}.COLUMN_RECORD_ID"),
          'readonly' => true
        )
      ),
      'dao' => array(
        'params' => array(
          'label' => CM_Lang::line("{$daoKey}.COLUMN_DAO"),
          'readonly' => true
        )
      ),
      'column' => array(
        'params' => array(
          'label' => CM_Lang::line("{$daoKey}.COLUMN_COLUMN"),
          'readonly' => true
        )
      ),
      'lang' => array(
        'params' => array(
          'label' => CM_Lang::line("{$daoKey}.COLUMN_LANG"),
          'readonly' => true
        )
      ),
      $daoColumnHeadline => array(
        'default' => 'name',
        'params' => array('label' => CM_Lang::line("{$daoKey}.COLUMN_HEADLINE"))
      ),
      'desc' => array(
        'params' => array(
          'label' => CM_Lang::line("{$daoKey}.COLUMN_DESC"),
          'f_type' => 'textarea'
        )
      ),
      'note' => array(
        'params' => array(
          'label' => CM_Lang::line("{$daoKey}.COLUMN_NOTE"),
          'f_type' => 'textarea'
        )
      )
    ),
    'column_group' => array(
      'add' => array($daoColumnRecordId, 'record_id', 'dao', 'column', 'lang', $daoColumnHeadline, 'desc', 'note'),
      'edit' => array($daoColumnHeadline, 'desc', 'note'),
      'list' => array($daoColumnRecordId, $daoColumnHeadline, 'desc', 'note'),
      'option' => array($daoColumnRecordId, $daoColumnHeadline),
      'callback' => array($daoColumnHeadline, 'desc')
    ),
    'form_group' => array(
      'info' => array($daoColumnHeadline, 'desc', 'note')
    )
  );
?>