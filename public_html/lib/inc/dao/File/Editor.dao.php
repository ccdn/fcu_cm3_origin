<?php
  $daoKey = 'DAO_File_Editor';
  $daoColumnRecordId = 'id';
  $daoColumnHeadline = 'headline';

  return array(
    'column' => array(
      $daoColumnRecordId => array(
        'default' => 'record_id',
        'params' => array('label' => CM_Lang::line("{$daoKey}.COLUMN_ID"))
      ),
      'file' => array(
        'default' => 'upload',
        'params' => array(
          'label' => CM_Lang::line("{$daoKey}.COLUMN_FILE"),
          'required' => true,
          'fu_options' => array(          
            'maxNumberOfFiles' => 1
          )
        )
      ),
      $daoColumnHeadline => array(
        'default' => 'name',
        'params' => array(
        'label' => CM_Lang::line("{$daoKey}.COLUMN_HEADLINE"))
      ),
      'permission' => array(
        'params' => array(
          'label' => CM_Lang::line("{$daoKey}.COLUMN_PERMISSION"),
          'required' => true,
          'f_type' => 'select'
        )
      ),
      'owner' => array(
        'params' => array(
          'label' => CM_Lang::line("{$daoKey}.COLUMN_OWNER"),
          'required' => true,
          'readonly' => true
        )
      ),
      'create_date' => array(
        'params' => array('label' => CM_Lang::line("{$daoKey}.COLUMN_CREATE_DATE"))
      ),
      'modify_date' => array(
        'params' => array('label' => CM_Lang::line("{$daoKey}.COLUMN_MODIFY_DATE"))
      )
    ),
    'column_group' => array(
      'add' => array('file', $daoColumnHeadline, 'permission', 'owner', 'create_date', 'modify_date'),
      'edit' => array('file', $daoColumnHeadline, 'permission', 'modify_date'),
      'list' => array($daoColumnRecordId, 'file', $daoColumnHeadline, 'permission', 'owner', 'create_date'),
      'option' => array($daoColumnRecordId, $daoColumnHeadline)
    ),
    'form_group' => array(
      'info' => array('file', $daoColumnHeadline, 'permission')
    )
  );
?>