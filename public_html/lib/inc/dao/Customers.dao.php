<?php
  $daoKey = 'DAO_Customers';
  $daoColumnRecordId = 'id';
  $daoColumnHeadline = 'name';

  return array(
    'column' => array(
      $daoColumnRecordId => array(
        'default' => 'record_id',
        'params' => array('label' => CM_Lang::line("{$daoKey}.COLUMN_ID"))
      ),
      'group_id' => array(
        'params' => array(
          'label' => CM_Lang::line("{$daoKey}.COLUMN_GROUP"),
          'f_type' => 'select_m'
        )
      ),
      $daoColumnHeadline => array(
        'default' => 'name',
        'params' => array('label' => CM_Lang::line("{$daoKey}.COLUMN_NAME"))
      ),
      'file' => array(
        'params' => array(
          'f_type' => 'simple_file',
          'label' => CM_Lang::line("{$daoKey}.COLUMN_FILE"),
          'fu_options' => array(          
            'maxNumberOfFiles' => 1
          ),
          'fu_params' => array(
            'prefix' => 'simple_upload',
            'ext' => 'jpg,png,jpeg'
          ),
          'tip_bofore' => ''
        )
      ),
      'sex' => array(
        'params' => array(
          'label' => CM_Lang::line("{$daoKey}.COLUMN_SEX"),
          'f_type' => 'select'
        )
      ),
      'bir' => array(
        'default' => 'date',
        'params' => array('label' => CM_Lang::line("{$daoKey}.COLUMN_BIR"))
      ),
      'email' => array(
        'default' => 'email',
        'params' => array('label' => CM_Lang::line("{$daoKey}.COLUMN_EMAIL"))
      ),
      'password' => array(
        'default' => 'password',
        'params' => array(
          'label' => CM_Lang::line("{$daoKey}.COLUMN_PASSWORD"),
          'f_confirm' => false
        )
      ),
      'password_new' => array(
        'default' => 'password',
        'params' => array(
          'label' => CM_Lang::line("{$daoKey}.COLUMN_PASSWORD_NEW"),
          'disable' => true,
          'f_confirm' => false
        )
      ),
      'phone' => array(
        'default' => 'phone',
        'params' => array('label' => CM_Lang::line("{$daoKey}.COLUMN_PHONE"))
      ),
      'cellphone' => array(
        'default' => 'cellphone',
        'params' => array('label' => CM_Lang::line("{$daoKey}.COLUMN_CELLPHONE"))
      ),
      'address' => array(
        'default' => 'address',
        'params' => array(
          'label' => CM_Lang::line("{$daoKey}.COLUMN_ADDRESS"),
          'f_class' => 'input-xxlarge'
        )
      ),
      'note' => array(
        'params' => array(
          'label' => CM_Lang::line("{$daoKey}.COLUMN_NOTE"),
          'f_type' => 'textarea'
        )
      ),
      'newsletter' => array(
        'params' => array(
          'label' => CM_Lang::line("{$daoKey}.COLUMN_NEWSLETTER"),
          'f_type' => 'select'
        )
      ),
      'bonus' => array(
        'params' => array(
          'label' => CM_Lang::line("{$daoKey}.COLUMN_BONUS"),
          'validateType' => 'digits',
          'f_type' => 'hidden'
        )
      ),
      'ip' => array(
        'params' => array(
          'label' => CM_Lang::line("{$daoKey}.COLUMN_IP"),
          'f_type' => 'readonly'
        )
      ),
      'login_count' => array(
        'params' => array(
          'label' => CM_Lang::line("{$daoKey}.COLUMN_LOGIN_COUNT"),
          'f_type' => 'readonly'
        )
      ),
      'login_last_date' => array(
        'params' => array(
          'label' => CM_Lang::line("{$daoKey}.COLUMN_LOGIN_LAST_DATE"),
          'f_type' => 'readonly'
        )
      ),
      'status' => array(
        'params' => array(
          'label' => CM_Lang::line("{$daoKey}.COLUMN_STATUS"),
          'f_type' => 'select'
        )
      ),
      'create_date' => array(
        'params' => array('label' => CM_Lang::line("{$daoKey}.COLUMN_CREATE_DATE"))
      ),
      'modify_date' => array(
        'params' => array('label' => CM_Lang::line("{$daoKey}.COLUMN_MODIFY_DATE"))
      )
    ),
    'column_group' => array(
      'login' => array('email', 'password'),
      'password' => array('password', 'password_new'),
      'password_reset' => array('password_new'),
      'info' => array($daoColumnHeadline, 'sex', 'phone', 'cellphone', 'email'),
      'add' => array($daoColumnHeadline, 'group_id', 'sex', 'bir', 'email', 'password', 'phone', 'cellphone', 'address', 'newsletter', 'ip', 'status', 'last_login_date', 'create_date', 'modify_date'),
      'edit' => array($daoColumnHeadline, 'group_id', 'sex', 'bir', 'email', 'phone', 'cellphone', 'address', 'newsletter', 'bonus', 'note', 'status', 'modify_date'),
      'list' => array($daoColumnRecordId, $daoColumnHeadline, 'group_id', 'email', 'ip', 'status', 'login_count', 'login_last_date'),
      'export' => array($daoColumnHeadline, 'group_id', 'sex', 'email', 'password', 'phone', 'cellphone', 'address', 'note', 'newsletter', 'ip', 'status', 'login_last_date', 'create_date', 'modify_date'),
      'status' => array('status', 'modify_date'),
      'site_edit' => array($daoColumnHeadline, 'sex', 'bir', 'email', 'phone', 'cellphone', 'address', 'newsletter', 'modify_date'),
      'site_info' => array($daoColumnHeadline, 'sex', 'bir', 'email', 'phone', 'cellphone', 'address', 'bonus')
    ),
    'form_group' => array(
      'info' => array($daoColumnHeadline, 'bir', 'sex', 'phone', 'cellphone', 'address', 'note'),
      'setting' => array('group_id', 'newsletter', 'bonus', 'email', 'password', 'status'),
      'site_info' => array($daoColumnHeadline, 'bir', 'sex', 'phone', 'cellphone', 'address'),
      'site_setting' => array('email', 'password', 'newsletter')
    )
  );
?>