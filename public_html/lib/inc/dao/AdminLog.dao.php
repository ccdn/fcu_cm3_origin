<?php
  $daoKey = 'DAO_AdminLog';
  $daoColumnRecordId = 'id';
  $daoColumnHeadline = 'request_headline';
  
  return array(
    'column' => array(
      $daoColumnRecordId => array(
        'default' => 'record_id',
        'params' => array('label' => CM_Lang::line("{$daoKey}.COLUMN_ID"))
      ),
      'account_id' => array(
        'default' => 'record_id',
        'params' => array('label' => CM_Lang::line("{$daoKey}.COLUMN_ACCOUNT_ID"))
      ),
      'account_name' => array(
        'params' => array(
          'label' => CM_Lang::line("{$daoKey}.COLUMN_ACCOUNT_NAME"),
          'readonly' => true
        )
      ),
      'request_name' => array(
        'params' => array(
          'label' => CM_Lang::line("{$daoKey}.COLUMN_REQUEST_NAME"),
          'readonly' => true
        )
      ),
      'mod' => array(
        'params' => array(
          'label' => CM_Lang::line("{$daoKey}.COLUMN_MOD"),
          'readonly' => true
        )
      ),
      'request' => array(
        'params' => array(
          'label' => CM_Lang::line("{$daoKey}.COLUMN_REQUEST"),
          'readonly' => true
        )
      ),
      'action' => array(
        'params' => array(
          'label' => CM_Lang::line("{$daoKey}.COLUMN_ACTION"),
          'readonly' => true
        )
      ),
      $daoColumnHeadline => array(
        'default' => 'name',
        'params' => array('label' => CM_Lang::line("{$daoKey}.COLUMN_HEADLINE"))
      ),
      'request_content' => array(
        'params' => array(
          'label' => CM_Lang::line("{$daoKey}.COLUMN_REQUEST_CONTENT"),
          'readonly' => true
        )
      ),
      'create_date' => array(
        'params' => array('label' => CM_Lang::line("{$daoKey}.COLUMN_CREATE_DATE"))
      )
    ),
    'column_group' => array(
      'add' => array($daoColumnHeadline, 'account_id', 'account_name', 'request_name', 'mod', 'request', 'action', 'request_content', 'create_date'),
      'edit' => array(),
      'list' => array($daoColumnRecordId, 'account_id', 'account_name', 'request_name', $daoColumnHeadline, 'create_date'),
      'detail' => array('create_date', 'account_id', 'account_name', $daoColumnHeadline, 'request_name', 'request_content')
    ),
    'form_group' => array()
  );
?>