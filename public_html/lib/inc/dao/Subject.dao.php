<?php
  $daoKey = 'DAO_Subject';
  $daoColumnRecordId = 'id';
  $daoColumnHeadline = 'headline';

  return array(
    'column' => array(
      $daoColumnRecordId => array(
        'default' => 'record_id',
        'params' => array('label' => CM_Lang::line("{$daoKey}.COLUMN_ID"))
      ),
      'video' => array(
        'params' => array(
          'label' => CM_Lang::line("{$daoKey}.COLUMN_VIDEO")
        )
      ),
      'status' => array(
        'params' => array(
          'label' => CM_Lang::line("{$daoKey}.COLUMN_STATUS"),
          'f_type' => 'select'
        )
      ),
      'create_date' => array(
        'params' => array('label' => CM_Lang::line("{$daoKey}.COLUMN_CREATE_DATE"))
      ),
      'modify_date' => array(
        'params' => array('label' => CM_Lang::line("{$daoKey}.COLUMN_MODIFY_DATE"))
      )
    ),
    'column_lang' => array(
      'lang' => array(
        'params' => array(
          'label' => CM_Lang::line("{$daoKey}.COLUMN_LANG"),
          'readonly' => true
        )
      ),
      $daoColumnHeadline => array(
        'default' => 'name',
        'params' => array('label' => CM_Lang::line("{$daoKey}.COLUMN_HEADLINE"))
      ),
      'lang_status' => array(
        'params' => array(
          'label' => CM_Lang::line("{$daoKey}.COLUMN_LANG_STATUS"),
          'f_type' => 'hidden'
        )
      )
    ),
    'column_group' => array(
      'add' => array('video', 'status', 'create_date', 'modify_date', $daoColumnHeadline, 'lang_status'),
      'edit' => array('video', 'status', 'modify_date', $daoColumnHeadline, 'lang_status'),
      'list' => array($daoColumnRecordId, $daoColumnHeadline, 'sort', 'status', 'create_date'),
      'status' => array('status', 'modify_date')
    ),
    'form_group' => array(
      'info' => array($daoColumnHeadline, 'video'),
      'setting' => array('status', 'lang_status')
    )
  );
?>