<?php
  //路徑設定 ---------- 開始
  //根目錄: 一般設定為 '' ， 程式位於子目錄時設定為 '子目錄/'
  CM_Conf::set('PATH.ROOT', '');
  //管理系統目錄名稱
  CM_Conf::set('PATH.WEBMGR', 'webmgr/');
  //網站目錄名稱 
  CM_Conf::set('PATH.WEBSITE', 'website/');
  //網域名稱
  //CM_Conf::set('HTTP.HOST', '61.63.6.139:8081');
  CM_Conf::set('HTTP.HOST', 'localhost');
  //網址路徑
  CM_Conf::set('HTTP.SERVER', 'http://' . CM_Conf::get('HTTP.HOST') . '/');
  //程式根目錄 實體路徑
  //CM_Conf::set('PATH_F.ROOT', '/home/fcu/public_html/' . CM_Conf::get('PATH.ROOT'));//測試
  CM_Conf::set('PATH_F.ROOT', '/AppServ/Apache2/htdocs/public_html/' . CM_Conf::get('PATH.ROOT'));//本機
  //程式根目錄 相對路徑
  CM_Conf::set('PATH_W.ROOT', CM_Conf::get('PATH.ROOT'));
  //上傳檔案目錄 實體路徑
  CM_Conf::set('PATH_F.UPLOAD', CM_Conf::get('PATH_F.ROOT') . 'upload/');
  //上傳檔案根目錄 相對路徑
  CM_Conf::set('PATH_W.UPLOAD', CM_Conf::get('PATH_W.ROOT') . 'upload/');
  //縮圖目錄 實體路徑
  CM_Conf::set('PATH_F.UPLOAD_CACHE', CM_Conf::get('PATH_F.UPLOAD') . 'cache/');
  //縮圖目錄 相對路徑
  CM_Conf::set('PATH_W.UPLOAD_CACHE', CM_Conf::get('PATH_W.UPLOAD') . 'cache/');
  //函式庫目錄 實體路徑
  CM_Conf::set('PATH_F.LIB', CM_Conf::get('PATH_F.ROOT') . 'lib/');
  //函式庫目錄 實體路徑
  CM_Conf::set('PATH_F.LIB_CLASS', CM_Conf::get('PATH_F.LIB') . 'class/');
  //樣板目錄 實體路徑
  CM_Conf::set('PATH_F.LIB_TMPL', CM_Conf::get('PATH_F.LIB') . 'tmpl/');
  //語系目錄 實體路徑
  CM_Conf::set('PATH_F.LIB_LANG', CM_Conf::get('PATH_F.LIB') . 'lang/');
  //外部程式目錄 實體路徑
  CM_Conf::set('PATH_F.LIB_VENDOR', CM_Conf::get('PATH_F.LIB') . 'vendor/');
  //設定檔目錄 實體路徑
  CM_Conf::set('PATH_F.LIB_CONF', CM_Conf::get('PATH_F.LIB') . 'inc/conf/');
  //DAO設定目錄 實體路徑
  CM_Conf::set('PATH_F.LIB_DAO', CM_Conf::get('PATH_F.LIB') . 'inc/dao/');
  //路徑設定 ---------- 結束

  //資料庫設定 ---------- 開始
  //資料庫主機位置
  CM_Conf::set('DB.HOST', 'localhost');
  //資料庫名稱
  //CM_Conf::set('DB.NAME', 'ccdntech');//測試
  CM_Conf::set('DB.NAME', 'ccdn2');//本機
  //資料庫使用者名稱
  //CM_Conf::set('DB.USERNAME', 'ccdntech');//測試
  CM_Conf::set('DB.USERNAME', 'root');//本機
  //資料庫密碼
  //CM_Conf::set('DB.PASSWORD', 'PQIk9GLcpaAz');//測試
  CM_Conf::set('DB.PASSWORD', 'ccdntech');//本機
  //串流server
  CM_Conf::set('STREAMSERVER.HOST', '61.63.6.139');
  //資料庫設定 ---------- 結束
?>