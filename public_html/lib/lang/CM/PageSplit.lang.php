<?php
  CM_Lang::import('CM_PageSplit', array(
    CM_Lang::CODE_TW => array(
      'PAGE_INFO' => '第&nbsp;<b>%d</b>&nbsp;頁, 共&nbsp;<b>%d</b>&nbsp;頁(共&nbsp;<b>%d</b>&nbsp;筆資料)',
      'TOP_PAGE' => '第一頁',
      'BOTTOM_PAGE' => '最後頁',
      'PREV_PAGE' => '←&nbsp;上一頁',
      'NEXT_PAGE' => '下一頁&nbsp;→'
    ),
    CM_Lang::CODE_CN => array(
      'PAGE_INFO' => '第&nbsp;<b>%d</b>&nbsp;页, 共&nbsp;<b>%d</b>&nbsp;页(共&nbsp;<b>%d</b>&nbsp;笔资料)',
      'TOP_PAGE' => '第一页',
      'BOTTOM_PAGE' => '最後页',
      'PREV_PAGE' => '←&nbsp;上一页',
      'NEXT_PAGE' => '下一页&nbsp;→'
    ),
    CM_Lang::CODE_EN => array(
    
    )
  ));
?>