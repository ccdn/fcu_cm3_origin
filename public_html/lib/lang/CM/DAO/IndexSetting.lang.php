<?php
  CM_Lang::import('CM_DAO_IndexSetting', array(
    CM_Lang::CODE_TW => array(
      'COLUMN_ID' => '編號',
      'COLUMN_HEADLINE' => '設定名稱',
      'COLUMN_ITEM_ID' => '內容編號',
      'ADD_SUCCESS' => '資料已經新增',
      'EDIT_SUCCESS' => '資料已經儲存',
      'REMOVE_SUCCESS' => '資料已經移除'
    ),
    CM_Lang::CODE_CN => array(
      'COLUMN_ID' => '编号',
      'COLUMN_HEADLINE' => '设定名称',
      'COLUMN_ITEM_ID' => '内容编号',
      'ADD_SUCCESS' => '资料已经新增',
      'EDIT_SUCCESS' => '资料已经储存',
      'REMOVE_SUCCESS' => '资料已经移除'
    ),
    CM_Lang::CODE_EN => array(
    
    )
  ));
?>