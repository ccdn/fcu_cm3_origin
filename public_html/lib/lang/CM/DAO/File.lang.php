<?php
  CM_Lang::import('CM_DAO_File', array(
    CM_Lang::CODE_TW => array(
      'COLUMN_ID' => '編號',
      'COLUMN_FILE' => '檔案',
      'COLUMN_NAME' => '檔案名稱',
      'COLUMN_SIZE' => '大小',
      'COLUMN_LINK' => '連接數',
      'COLUMN_CREATE_DATE' => '建立日期',
      'COLUMN_MODIFY_DATE' => '最後修改日期',
      'ADD_SUCCESS' => '檔案已經上傳',
      'EDIT_SUCCESS' => '檔案資料已經存檔',
      'REMOVE_SUCCESS' => '檔案已經移除'
    ),
    CM_Lang::CODE_CN => array(
      'COLUMN_ID' => '编号',
      'COLUMN_FILE' => '档案',
      'COLUMN_NAME' => '档案名称',
      'COLUMN_SIZE' => '大小',
      'COLUMN_LINK' => '连接数',
      'COLUMN_CREATE_DATE' => '建立日期',
      'COLUMN_MODIFY_DATE' => '最後修改日期',
      'ADD_SUCCESS' => '档案已经上传',
      'EDIT_SUCCESS' => '档案资料已经存档',
      'REMOVE_SUCCESS' => '档案已经移除'
    )
  ));
?>