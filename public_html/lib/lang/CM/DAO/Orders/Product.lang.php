<?php
  CM_Lang::import('CM_DAO_Orders_Product', array(
    CM_Lang::CODE_TW => array(
      'COLUMN_ID' => '編號',
      'COLUMN_ORDERS_ID' => '訂單編號',
      'COLUMN_CUSTOMERS_ID' => '購買人編號',
      'COLUMN_COMPANY_ID' => '商家編號',
      'COLUMN_PRODUCT_ID' => '產品編號',
      'COLUMN_HEADLINE' => '產品名稱',
      'COLUMN_QTY' => '數量',
      'COLUMN_PRICE' => '單價(值)',
      'COLUMN_BONUS' => '公益金額',
      'COLUMN_TOTAL' => '小計(值)',
      'COLUMN_PRICE_TEXT' => '單價',
      'COLUMN_TOTAL_TEXT' => '小計',
      'COLUMN_STATUS' => '出貨狀態',
      'COLUMN_STATUS_1' => '未出貨',
      'COLUMN_STATUS_2' => '處理中',
      'COLUMN_STATUS_3' => '貨已送出',
      'ADD_SUCCESS' => '訂單產品資料已建立',
      'EDIT_SUCCESS' => '訂單產品資料已儲存',
      'REMOVE_SUCCESS' => '訂單產品已移除',
      'ERROR_FORM_NOT_FOUND' => '找不到指定的表單'
    ),
    CM_Lang::CODE_CN => array(
      'COLUMN_ID' => '编号',
      'COLUMN_ORDERS_ID' => '订单编号',
      'COLUMN_CUSTOMERS_ID' => '购买人编号',
      'COLUMN_COMPANY_ID' => '商家编号',
      'COLUMN_PRODUCT_ID' => '产品编号',
      'COLUMN_HEADLINE' => '产品名称',
      'COLUMN_QTY' => '数量',
      'COLUMN_PRICE' => '单价(值)',
      'COLUMN_BONUS' => '公益金额',
      'COLUMN_TOTAL' => '小计(值)',
      'COLUMN_PRICE_TEXT' => '单价',
      'COLUMN_TOTAL_TEXT' => '小计',
      'COLUMN_STATUS' => '出货状态',
      'COLUMN_STATUS_1' => '未出货',
      'COLUMN_STATUS_2' => '处理中',
      'COLUMN_STATUS_3' => '货已送出',
      'ADD_SUCCESS' => '订单产品资料已建立',
      'EDIT_SUCCESS' => '订单产品资料已储存',
      'REMOVE_SUCCESS' => '订单产品已移除',
      'ERROR_FORM_NOT_FOUND' => '找不到指定的表单'
    )
  ));
?>