<?php
  CM_Lang::import('CM_DAO_Orders_Total', array(
    CM_Lang::CODE_TW => array(
      'COLUMN_ID' => '編號',
      'COLUMN_ORDERS_ID' => '訂單編號',
      'COLUMN_CODES' => '代號',
      'COLUMN_HEADLINE' => '標題',
      'COLUMN_TEXT' => '內容',
      'COLUMN_VALUE' => '單價(值)',
      'COLUMN_SORT' => '排序',
      'ADD_SUCCESS' => '訂單總計資料已建立',
      'EDIT_SUCCESS' => '訂單總計資料已儲存',
      'REMOVE_SUCCESS' => '訂單總計已移除',
      'ERROR_FORM_NOT_FOUND' => '找不到指定的表單'
    ),
    CM_Lang::CODE_CN => array(
      'COLUMN_ID' => '编号',
      'COLUMN_ORDERS_ID' => '订单编号',
      'COLUMN_CODES' => '代号',
      'COLUMN_HEADLINE' => '标题',
      'COLUMN_TEXT' => '内容',
      'COLUMN_VALUE' => '单价(值)',
      'COLUMN_SORT' => '排序',
      'ADD_SUCCESS' => '订单总计资料已建立',
      'EDIT_SUCCESS' => '订单总计资料已储存',
      'REMOVE_SUCCESS' => '订单总计已移除',
      'ERROR_FORM_NOT_FOUND' => '找不到指定的表单'
    )
  ));
?>