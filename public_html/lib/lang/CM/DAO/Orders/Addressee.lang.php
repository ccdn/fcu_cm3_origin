<?php
  CM_Lang::import('CM_DAO_Orders_Addressee', array(
    CM_Lang::CODE_TW => array(
      'COLUMN_ID' => '編號',
      'COLUMN_NAME' => '真實姓名',
      'COLUMN_EMAIL' => '信箱',
      'COLUMN_PHONE' => '聯絡電話',
      'COLUMN_CELLPHONE' => '手機',
      'COLUMN_ADDRESS' => '地址',
      'COLUMN_NOTE' => '備註',
      'COLUMN_CREATE_DATE' => '建立時間',
      'COLUMN_MODIFY_DATE' => '最後修改時間',
      'ADD_SUCCESS' => '訂單寄件人資料已建立',
      'EDIT_SUCCESS' => '訂單寄件人資已儲存',
      'REMOVE_SUCCESS' => '訂單寄件人已移除',
      'ERROR_FORM_NOT_FOUND' => '找不到指定的表單'
    ),
    CM_Lang::CODE_CN => array(
      'COLUMN_ID' => '编号',
      'COLUMN_NAME' => '真实姓名',
      'COLUMN_EMAIL' => '信箱',
      'COLUMN_PHONE' => '联络电话',
      'COLUMN_CELLPHONE' => '手机',
      'COLUMN_ADDRESS' => '地址',
      'COLUMN_NOTE' => '备注',
      'COLUMN_CREATE_DATE' => '建立时间',
      'COLUMN_MODIFY_DATE' => '最後修改时间',
      'ADD_SUCCESS' => '订单寄件人资料已建立',
      'EDIT_SUCCESS' => '订单寄件人资已储存',
      'REMOVE_SUCCESS' => '订单寄件人已移除',
      'ERROR_FORM_NOT_FOUND' => '找不到指定的表单'
    )
  ));
?>