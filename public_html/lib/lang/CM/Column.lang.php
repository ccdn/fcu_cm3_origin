<?php
  CM_Lang::import('CM_Column', array(
    CM_Lang::CODE_TW => array(
      'TYPE_NOT_SUPPORT' => '不支援指定的類型：%s',
      'DEF_SELECT_M_PLACEHOLDER' => '按一下這裡選擇項目(多選)',
      'DEF_SELECT_MG_PLACEHOLDER' => '按一下這裡選擇項目(多選)'
    ),
    CM_Lang::CODE_CN => array(
      'TYPE_NOT_SUPPORT' => '不支援指定的类型：%s',
      'DEF_SELECT_M_PLACEHOLDER' => '按一下这里选择项目(多选)',
      'DEF_SELECT_MG_PLACEHOLDER' => '按一下这里选择项目(多选)'
    ),
    CM_Lang::CODE_EN => array(
    
    )
  ));
?>