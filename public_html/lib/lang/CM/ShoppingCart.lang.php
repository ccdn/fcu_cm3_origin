<?php
  CM_Lang::import('CM_ShoppingCart', array(
    CM_Lang::CODE_TW => array(
      'ADD_ITEM_SUCCESS' => '<b>%s</b>&nbsp;已經加入購物車',
      'EDIT_ITEM_SUCCESS' => '購物車內&nbsp;<b>%s</b>&nbsp;資料已更新',
      'REMOVE_ITEM_SUCCESS' => '<b>%s</b>&nbsp;已經從購物車中移除',
      'CLEAR_SUCCESS' => '購物車內容已清空',
      'PROCESS_TOTAL_SUCCESS' => '購物車計算完成',
      'TOTAL_ROWS_SUBTOTAL' => '小計',
      'TOTAL_ROWS_SUBTOTAL_TEXT' => 'NT<b>$%d</b>',
      'TOTAL_ROWS_TOTAL' => '總計',
      'TOTAL_ROWS_TOTAL_TEXT' => 'NT<b>$%d</b>',
      'ERROR_ITEM_NOT_FOUND' => '購物車內沒有指定的商品',
      'ERROR_ITEM_EMPTY' => '購物車是空的'
    ),
    CM_Lang::CODE_CN => array(
      'ADD_ITEM_SUCCESS' => '<b>%s</b>&nbsp;已经加入购物车',
      'EDIT_ITEM_SUCCESS' => '购物车内&nbsp;<b>%s</b>&nbsp;资料已更新',
      'REMOVE_ITEM_SUCCESS' => '<b>%s</b>&nbsp;已经从购物车中移除',
      'CLEAR_SUCCESS' => '购物车内容已清空',
      'PROCESS_TOTAL_SUCCESS' => '购物车计算完成',
      'TOTAL_ROWS_SUBTOTAL' => '小计',
      'TOTAL_ROWS_SUBTOTAL_TEXT' => 'NT<b>$%d</b>',
      'TOTAL_ROWS_TOTAL' => '总计',
      'TOTAL_ROWS_TOTAL_TEXT' => 'NT<b>$%d</b>',
      'ERROR_ITEM_NOT_FOUND' => '购物车内没有指定的商品',
      'ERROR_ITEM_EMPTY' => '购物车是空的'
    )
  ));
?>