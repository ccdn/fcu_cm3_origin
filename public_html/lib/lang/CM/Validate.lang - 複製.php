<?php
  CM_Lang::import('CM_Validate', array(
    CM_Lang::CODE_TW => array(
      'EMPTY' => '%s沒有資料輸入',
      'REQUIRED' => '%s為必填',
      'FORMAT_MIN' => '至少必須輸入%d個字元',
      'FORMAT_FIXED' => '只能固定輸入 %d 個字元',
      'FORMAT_RANGE' => '必須輸入介於%d到%d字元之間',
      'FORMAT_PHONE' => '%s的電話號碼格式錯誤',
      'FORMAT_CELLPHONE' => '%s格式錯誤，請輸入正確的手機格式 範例：0911-111-111',
      'FORMAT_EMAIL' => '%s的信箱格式錯誤',
      'FORMAT_DATE' => '%s的日期格式錯誤，必須是YYYY-MM-DD',
      'FORMAT_DIGITS' => '%s只能輸入數字',
      'FORMAT_ENG' => '%s只能輸入英文字母',
      'FORMAT_ENG_NUM' => '%s只能輸入英文和數字',
      'FORMAT_ROC_CITIZENID' => '%s格式錯誤，請輸入正確的身分證字號',
      'FORMAT_ROC_CITIZENID_A1' => '%s首字英文錯誤，請輸入正確的身分證字號',
      'FORMAT_ROC_CITIZENID_A2' => '%s後九碼只能輸入數字，請輸入正確的身分證字號'
    ),
    CM_Lang::CODE_CN => array(
      'EMPTY' => '%s没有资料输入',
      'REQUIRED' => '%s为必填',
      'FORMAT_MIN' => '至少必须输入%d个字元',
      'FORMAT_FIXED' => '只能固定输入 %d 个字元',
      'FORMAT_RANGE' => '必须输入介於%d到%d字元之间',
      'FORMAT_PHONE' => '%s的电话号码格式错误',
      'FORMAT_CELLPHONE' => '%s格式错误，请输入正确的手机格式 范例：0911-111-111',
      'FORMAT_EMAIL' => '%s的信箱格式错误',
      'FORMAT_DATE' => '%s的日期格式错误，必须是YYYY-MM-DD',
      'FORMAT_DIGITS' => '%s只能输入数字',
      'FORMAT_ENG' => '%s只能输入英文字母',
      'FORMAT_ENG_NUM' => '%s只能输入英文和数字',
      'FORMAT_ROC_CITIZENID' => '%s格式错误，请输入正确的身分证字号',
      'FORMAT_ROC_CITIZENID_A1' => '%s首字英文错误，请输入正确的身分证字号',
      'FORMAT_ROC_CITIZENID_A2' => '%s後九码只能输入数字，请输入正确的身分证字号'
    ),
    CM_Lang::CODE_EN => array(
    
    )
  ));
?>