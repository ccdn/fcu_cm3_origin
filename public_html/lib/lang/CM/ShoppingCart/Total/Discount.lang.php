<?php
  CM_Lang::import('CM_ShoppingCart_Total_Discount', array(
    CM_Lang::CODE_TW => array(
      'NAME' => '滿額折扣',
      'DESC' => '滿額折扣模組，可以設定一組以上的折扣',
      'TOTAL_ROWS_DISCOUNT' => '滿<b>%d</b>折<b>%d</b>',
      'TOTAL_ROWS_DISCOUNT_TEXT' => '<font color="red">-NT<b>$%d</b></font>'
    ),
    CM_Lang::CODE_CN => array(
      'NAME' => '满额折扣',
      'DESC' => '满额折扣模组，可以设定一组以上的折扣',
      'TOTAL_ROWS_DISCOUNT' => '满<b>%d</b>折<b>%d</b>',
      'TOTAL_ROWS_DISCOUNT_TEXT' => '<font color="red">-NT<b>$%d</b></font>'
    )
  ));
?>