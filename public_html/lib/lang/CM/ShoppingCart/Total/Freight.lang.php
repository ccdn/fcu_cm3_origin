<?php
  CM_Lang::import('CM_ShoppingCart_Total_Freight', array(
    CM_Lang::CODE_TW => array(
      'NAME' => '運費',
      'DESC' => '運費計算模組',
      'TOTAL_ROWS_FREIGHT' => '運費',
      'TOTAL_ROWS_FREIGHT_TEXT' => '+NT<b>$%d</b>',
      'TOTAL_ROWS_DISCOUNT' => '滿NT<b>$%d</b>免運費',
      'TOTAL_ROWS_DISCOUNT_TEXT' => '+NT<b>$0</b>'
    ),
    CM_Lang::CODE_CN => array(
      'NAME' => '运费',
      'DESC' => '运费计算模组',
      'TOTAL_ROWS_FREIGHT' => '运费',
      'TOTAL_ROWS_FREIGHT_TEXT' => '+NT<b>$%d</b>',
      'TOTAL_ROWS_DISCOUNT' => '满NT<b>$%d</b>免运费',
      'TOTAL_ROWS_DISCOUNT_TEXT' => '+NT<b>$0</b>'
    )
  ));
?>