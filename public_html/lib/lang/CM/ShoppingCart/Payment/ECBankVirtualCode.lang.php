<?php
  CM_Lang::import('CM_ShoppingCart_Payment_ECBankVirtualCode', array(
    CM_Lang::CODE_TW => array(
      'NAME' => '超商代碼繳款',
      'DESC' => 'ECBank(綠界金流)超商代碼付款模組',
      'SELECT_INTRO' => '系統將提供您一組繳款代碼，您可前往全家便利商店、萊爾富 或 OK便利商店，輸入代碼列印明條後，至櫃台完成繳款。<br />(詳細操作說明: <a target="_blank" href="http://www.ecbank.com.tw/expenses-famiport.htm">全家</a>、<a target="_blank" href="http://www.ecbank.com.tw/expenses-life-et.htm">萊爾富</a>、<a target="_blank" href="http://www.ecbank.com.tw/expenses-okgo.htm">OKGO</a>)',
      'COLUMN_MER_ID' => 'ECBank商店代號',
      'COLUMN_PAYMENT_TYPE' => '付款方式',
      'COLUMN_OB_SOB' => '訂單編號',
      'COLUMN_TSR' => '交易單號',
      'COLUMN_AMT' => '交易金額',
      'COLUMN_PAYER_AMT' => '交易金額',
      'COLUMN_EXPIRE_DATE' => '繳費截止日期',
      'COLUMN_SUCC' => '交易狀態',
      'COLUMN_PAYER_BANK' => '付款人銀行代碼',
      'COLUMN_PAYER_ACC' => '付款人銀行帳號後5碼',
      'COLUMN_PROC_DATE' => '處理日期',
      'COLUMN_PROC_TIME' => '處理時間',
      'COLUMN_PROC_TAC' => '交易驗證壓碼',
      'ERROR_GET_CODE' => '取號失敗，錯誤碼: %s',
      'ERROR_SN' => '序號不正確',
      'ERROR_AUTH' => '授權失敗',
      'ERROR_AUTH_REMOTE' => '伺服器驗證失敗'
    ),
    CM_Lang::CODE_CN => array(
      'NAME' => '超商代码缴款',
      'DESC' => 'ECBank(绿界金流)超商代码付款模组',
      'SELECT_INTRO' => '系统将提供您一组缴款代码，您可前往全家便利商店丶莱尔富 或 OK便利商店，输入代码列印明条後，至柜台完成缴款。<br />(详细操作说明: <a target="_blank" href="http://www.ecbank.com.tw/expenses-famiport.htm">全家</a>丶<a target="_blank" href="http://www.ecbank.com.tw/expenses-life-et.htm">莱尔富</a>丶<a target="_blank" href="http://www.ecbank.com.tw/expenses-okgo.htm">OKGO</a>)',
      'COLUMN_MER_ID' => 'ECBank商店代号',
      'COLUMN_PAYMENT_TYPE' => '付款方式',
      'COLUMN_OB_SOB' => '订单编号',
      'COLUMN_TSR' => '交易单号',
      'COLUMN_AMT' => '交易金额',
      'COLUMN_PAYER_AMT' => '交易金额',
      'COLUMN_EXPIRE_DATE' => '缴费截止日期',
      'COLUMN_SUCC' => '交易状态',
      'COLUMN_PAYER_BANK' => '付款人银行代码',
      'COLUMN_PAYER_ACC' => '付款人银行帐号後5码',
      'COLUMN_PROC_DATE' => '处理日期',
      'COLUMN_PROC_TIME' => '处理时间',
      'COLUMN_PROC_TAC' => '交易验证压码',
      'ERROR_GET_CODE' => '取号失败，错误码: %s',
      'ERROR_SN' => '序号不正确',
      'ERROR_AUTH' => '授权失败',
      'ERROR_AUTH_REMOTE' => '伺服器验证失败'
    )
  ));
?>