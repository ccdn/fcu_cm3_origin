<?php
  CM_Lang::import('CM_ShoppingCart_Payment_ATM', array(
    CM_Lang::CODE_TW => array(
      'NAME' => 'ATM轉帳',
      'PAYMENT_TITLE' => 'ATM轉帳資訊',
      'PRINT_BTN' => '列印本頁',
      'NEXT_BTN' => '完成訂購',
      'GROUP_ARTICLE' => '文案設定',
      'COLUMN_INTRO' => '付款說明',
      'COLUMN_INTRO_DESC' => '付款說明'
    ),
    CM_Lang::CODE_CN => array(
      'NAME' => 'ATM转帐',
      'PAYMENT_TITLE' => 'ATM转帐资讯',
      'PRINT_BTN' => '列印本页',
      'NEXT_BTN' => '完成订购',
      'GROUP_ARTICLE' => '文案设定',
      'COLUMN_INTRO' => '付款说明',
      'COLUMN_INTRO_DESC' => '付款说明'
    )
  ));
?>