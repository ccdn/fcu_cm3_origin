<?php
  CM_Lang::import('CM_ShoppingCart_Payment_ECBankIbon', array(
    CM_Lang::CODE_TW => array(
      'NAME' => '7-11&nbsp;iBon',
      'DESC' => 'ECBank(綠界金流)透過7-11&nbsp;iBon付款模組',
      'SELECT_INTRO' => '系統將提供您一組繳款代碼，您可前往7-11，輸入代碼列印明條後，至櫃台完成繳款。<a target="_blank" href="http://www.ecbank.com.tw/expenses-ibon.htm">(詳細操作說明)</a>',
      'ITEM_NAME' => '%s 線上商店',
      'COLUMN_MER_ID' => 'ECBank商店代號',
      'COLUMN_PAYMENT_TYPE' => '付款方式',
      'COLUMN_OB_SOB' => '訂單編號',
      'COLUMN_TSR' => '交易單號',
      'COLUMN_PAYNO' => '繳費代碼',
      'COLUMN_AMT' => '交易金額',
      'COLUMN_SUCC' => '交易狀態',
      'COLUMN_PAYFROM' => '繳費超商',
      'COLUMN_PROC_DATE' => '處理日期',
      'COLUMN_PROC_TIME' => '處理時間',
      'COLUMN_PROC_TAC' => '交易驗證壓碼',
      'ERROR_GET_CODE' => '取號失敗，錯誤碼: %s',
      'ERROR_SN' => '序號不正確',
      'ERROR_AUTH' => '授權失敗',
      'ERROR_AUTH_REMOTE' => '伺服器驗證失敗'
    ),
    CM_Lang::CODE_CN => array(
      'NAME' => '7-11&nbsp;iBon',
      'DESC' => 'ECBank(绿界金流)透过7-11&nbsp;iBon付款模组',
      'SELECT_INTRO' => '系统将提供您一组缴款代码，您可前往7-11，输入代码列印明条後，至柜台完成缴款。<a target="_blank" href="http://www.ecbank.com.tw/expenses-ibon.htm">(详细操作说明)</a>',
      'ITEM_NAME' => '%s 线上商店',
      'COLUMN_MER_ID' => 'ECBank商店代号',
      'COLUMN_PAYMENT_TYPE' => '付款方式',
      'COLUMN_OB_SOB' => '订单编号',
      'COLUMN_TSR' => '交易单号',
      'COLUMN_PAYNO' => '缴费代码',
      'COLUMN_AMT' => '交易金额',
      'COLUMN_SUCC' => '交易状态',
      'COLUMN_PAYFROM' => '缴费超商',
      'COLUMN_PROC_DATE' => '处理日期',
      'COLUMN_PROC_TIME' => '处理时间',
      'COLUMN_PROC_TAC' => '交易验证压码',
      'ERROR_GET_CODE' => '取号失败，错误码: %s',
      'ERROR_SN' => '序号不正确',
      'ERROR_AUTH' => '授权失败',
      'ERROR_AUTH_REMOTE' => '伺服器验证失败'
    )
  ));
?>