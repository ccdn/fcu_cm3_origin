<?php
  CM_Lang::import('CM_ShoppingCart_Payment_ECBankVirtualAccount', array(
    CM_Lang::CODE_TW => array(
      'NAME' => '虛擬帳號(ATM)',
      'DESC' => 'ECBank(綠界金流)虛擬帳號付款模組',
      'SELECT_INTRO' => '系統將提供一組專門為了此次交易使用的帳號資料，您可至ATM點選非約定轉帳，正確輸入即可完成繳款。',
      'COLUMN_MER_ID' => 'ECBank商店代號',
      'COLUMN_PAYMENT_TYPE' => '付款方式',
      'COLUMN_OB_SOB' => '訂單編號',
      'COLUMN_TSR' => '交易單號',
      'COLUMN_AMT' => '交易金額',
      'COLUMN_PAYER_AMT' => '交易金額',
      'COLUMN_EXPIRE_DATE' => '繳費截止日期',
      'COLUMN_SUCC' => '交易狀態',
      'COLUMN_PAYER_BANK' => '付款人銀行代碼',
      'COLUMN_PAYER_ACC' => '付款人銀行帳號後5碼',
      'COLUMN_PROC_DATE' => '處理日期',
      'COLUMN_PROC_TIME' => '處理時間',
      'COLUMN_PROC_TAC' => '交易驗證壓碼',
      'ERROR_GET_CODE' => '取號失敗，錯誤碼: %s',
      'ERROR_SN' => '序號不正確',
      'ERROR_AUTH' => '授權失敗',
      'ERROR_AUTH_REMOTE' => '伺服器驗證失敗'
    ),
    CM_Lang::CODE_CN => array(
      'NAME' => '虚拟帐号(ATM)',
      'DESC' => 'ECBank(绿界金流)虚拟帐号付款模组',
      'SELECT_INTRO' => '系统将提供一组专门为了此次交易使用的帐号资料，您可至ATM点选非约定转帐，正确输入即可完成缴款。',
      'COLUMN_MER_ID' => 'ECBank商店代号',
      'COLUMN_PAYMENT_TYPE' => '付款方式',
      'COLUMN_OB_SOB' => '订单编号',
      'COLUMN_TSR' => '交易单号',
      'COLUMN_AMT' => '交易金额',
      'COLUMN_PAYER_AMT' => '交易金额',
      'COLUMN_EXPIRE_DATE' => '缴费截止日期',
      'COLUMN_SUCC' => '交易状态',
      'COLUMN_PAYER_BANK' => '付款人银行代码',
      'COLUMN_PAYER_ACC' => '付款人银行帐号後5码',
      'COLUMN_PROC_DATE' => '处理日期',
      'COLUMN_PROC_TIME' => '处理时间',
      'COLUMN_PROC_TAC' => '交易验证压码',
      'ERROR_GET_CODE' => '取号失败，错误码: %s',
      'ERROR_SN' => '序号不正确',
      'ERROR_AUTH' => '授权失败',
      'ERROR_AUTH_REMOTE' => '伺服器验证失败'
    )
  ));
?>