<?php
  CM_Lang::import('CM_Template', array(
    CM_Lang::CODE_TW => array(
      'FILE_NOT_READ' => '檔案 %s 無法讀取',
      'DIR_NOT_READ' => '路徑 %s 不存在或無法讀取'
    ),
    CM_Lang::CODE_CN => array(
      'FILE_NOT_READ' => '档案 %s 无法读取',
      'DIR_NOT_READ' => '路径 %s 不存在或无法读取'
    ),
    CM_Lang::CODE_EN => array(
    
    )
  ));
?>