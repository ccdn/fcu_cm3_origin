<?php
  CM_Lang::import('CM_Form', array(
    CM_Lang::CODE_TW => array(
      'UPLOAD_M_SELECT_FILE' => '請選擇檔案',
      'UPLOAD_TIP_FILE_COUNT' => '可選擇 <b>%d</b> 個檔案',
      'UPLOAD_TIP_ALLOW_EXT' => '允許的副檔名 <b>%s</b>',
      'UPLOAD_TIP_SIZE' => '上傳大小限制 <b>%d</b> ~ <b>%d</b> (MB)',
      'UPLOAD_LIST_VIEW_BTN' => '檢視檔案',
      'UPLOAD_LIST_REMOVE_BTN' => '移除檔案',
      'UPLOAD_LIST_REMOVE_CONFIRM' => '確定要移除檔案&nbsp;<b>%s</b>?',
      'CONFIRM_LABEL' => '%s確認',
      'ERROR_GROUP_NOT_FOUND' => '找不到指定的表單群組',
      'ERROR_UPLOAD_URL_EMPTY' => '表單欄位&nbsp;%s&nbsp;的上傳的路徑沒有設定',
      'ERROR_TYPE_NOT_SUPPORT' => '指定的表單類型&nbsp;%s&nbsp;無效'
    ),
    CM_Lang::CODE_CN => array(
      'UPLOAD_M_SELECT_FILE' => '请选择档案',
      'UPLOAD_TIP_FILE_COUNT' => '可选择 <b>%d</b> 个档案',
      'UPLOAD_TIP_ALLOW_EXT' => '允许的副档名 <b>%s</b>',
      'UPLOAD_TIP_SIZE' => '上传大小限制 <b>%d</b> ~ <b>%d</b> (MB)',
      'UPLOAD_LIST_VIEW_BTN' => '检视档案',
      'UPLOAD_LIST_REMOVE_BTN' => '移除档案',
      'UPLOAD_LIST_REMOVE_CONFIRM' => '確定要移除檔案&nbsp;<b>%s</b>?',
      'CONFIRM_LABEL' => '%s确认',
      'ERROR_GROUP_NOT_FOUND' => '找不到指定的表单群组',
      'ERROR_UPLOAD_URL_EMPTY' => '表单栏位&nbsp;%s&nbsp;的上传的路径没有设定',
      'ERROR_TYPE_NOT_SUPPORT' => '指定的表单类型&nbsp;%s&nbsp;无效'
    )
  ));
?>