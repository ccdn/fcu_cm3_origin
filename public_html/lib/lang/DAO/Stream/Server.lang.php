<?php
  CM_Lang::import('DAO_Stream_Server', array(
    CM_Lang::CODE_TW => array(
      'COLUMN_ID' => '編號',
      'COLUMN_HEADLINE' => '伺服器IP',
      'ADD_SUCCESS' => '伺服器資料已經新增',
      'EDIT_SUCCESS' => '伺服器資料已經儲存',
      'REMOVE_SUCCESS' => '伺服器資料已經移除'
    ),
    CM_Lang::CODE_CN => array(
      'COLUMN_ID' => '编号',
      'COLUMN_HEADLINE' => '伺服器IP',
      'ADD_SUCCESS' => '伺服器资料已经新增',
      'EDIT_SUCCESS' => '伺服器资料已经储存',
      'REMOVE_SUCCESS' => '伺服器资料已经移除'
    )
  ));
?>