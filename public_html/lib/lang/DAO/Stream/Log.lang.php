<?php
  CM_Lang::import('DAO_Stream_Log', array(
    CM_Lang::CODE_TW => array(
      'COLUMN_ID' => '編號',
      'COLUMN_HEADLINE' => '檔案名稱',
      'COLUMN_T_TIMESTAMP' => '開始播放時間',
      'COLUMN_I_MDA_SEQ' => '影片編號',
      'COLUMN_I_USR_SEQ' => '會員編號',
      'COLUMN_I_UGP_SEQ' => '群組編號',
      'COLUMN_I_CONNECTED_TIME' => '播放時間',
      'COLUMN_C_CLIENT_IP' => 'ip位址',
      'COLUMN_C_CLIENT_INFO' => '觀看裝置',
      'COLUMN_I_BYTES_SENT' => '傳送檔案大小',
      'COLUMN_C_PROTOCOL' => '串流協定',
      'COLUMN_I_FILE_SIZE' => '檔案大小'
    ),
    CM_Lang::CODE_CN => array(
      'COLUMN_ID' => '编号',
      'COLUMN_HEADLINE' => '档案名称',
      'COLUMN_T_TIMESTAMP' => '开始播放时间',
      'COLUMN_I_MDA_SEQ' => '影片编号',
      'COLUMN_I_USR_SEQ' => '会员编号',
      'COLUMN_I_UGP_SEQ' => '群组编号',
      'COLUMN_I_CONNECTED_TIME' => '播放时间',
      'COLUMN_C_CLIENT_IP' => 'ip位址',
      'COLUMN_C_CLIENT_INFO' => '观看装置',
      'COLUMN_I_BYTES_SENT' => '传送档案大小',
      'COLUMN_C_PROTOCOL' => '串流协定',
      'COLUMN_I_FILE_SIZE' => '档案大小'
    )
  ));
?>