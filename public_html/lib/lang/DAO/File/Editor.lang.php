<?php
  CM_Lang::import('DAO_File_Editor', array(
    CM_Lang::CODE_TW => array(
      'COLUMN_ID' => '編號',
      'COLUMN_FILE' => '檔案',
      'COLUMN_HEADLINE' => '標題',
      'COLUMN_PERMISSION' => '權限',
      'COLUMN_PERMISSION_1' => '只有擁有者可使用',
      'COLUMN_PERMISSION_2' => '全部人都可以使用',
      'COLUMN_OWNER' => '擁有者',
      'COLUMN_CREATE_DATE' => '建立日期',
      'COLUMN_MODIFY_DATE' => '最後修改日期',
      'ADD_SUCCESS' => '檔案已經新增',
      'EDIT_SUCCESS' => '檔案已經存檔',
      'REMOVE_SUCCESS' => '檔案已經移除'
    ),
    CM_Lang::CODE_CN => array(
      'COLUMN_ID' => '编号',
      'COLUMN_FILE' => '档案',
      'COLUMN_HEADLINE' => '标题',
      'COLUMN_PERMISSION' => '权限',
      'COLUMN_PERMISSION_1' => '只有拥有者可使用',
      'COLUMN_PERMISSION_2' => '全部人都可以使用',
      'COLUMN_OWNER' => '拥有者',
      'COLUMN_CREATE_DATE' => '建立日期',
      'COLUMN_MODIFY_DATE' => '最後修改日期',
      'ADD_SUCCESS' => '档案已经新增',
      'EDIT_SUCCESS' => '档案已经存档',
      'REMOVE_SUCCESS' => '档案已经移除'
    )
  ));
?>