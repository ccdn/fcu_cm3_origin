<?php
  CM_Lang::import('DAO_File_Info', array(
    CM_Lang::CODE_TW => array(
      'COLUMN_FILE' => '檔案',
      'COLUMN_RECORD_ID' => '資料物件編號',
      'COLUMN_DAO' => '資料物件',
      'COLUMN_COLUMN' => '欄位',
      'COLUMN_LANG' => '語系',
      'COLUMN_HEADLINE' => '檔案標題',
      'COLUMN_DESC' => '檔案說明',
      'COLUMN_NOTE' => '備註',
      'ADD_SUCCESS' => '檔案資訊已經新增',
      'EDIT_SUCCESS' => '檔案資訊已經存檔',
      'REMOVE_SUCCESS' => '檔案資訊已經移除'
    ),
    CM_Lang::CODE_CN => array(
      'COLUMN_FILE' => '档案',
      'COLUMN_RECORD_ID' => '资料物件编号',
      'COLUMN_DAO' => '资料物件',
      'COLUMN_COLUMN' => '栏位',
      'COLUMN_LANG' => '语系',
      'COLUMN_HEADLINE' => '档案标题',
      'COLUMN_DESC' => '档案说明',
      'COLUMN_NOTE' => '备注',
      'ADD_SUCCESS' => '档案资讯已经新增',
      'EDIT_SUCCESS' => '档案资讯已经存档',
      'REMOVE_SUCCESS' => '档案资讯已经移除'
    )
  ));
?>