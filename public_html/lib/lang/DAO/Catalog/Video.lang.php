<?php
  CM_Lang::import('DAO_Catalog_Video', array(
    CM_Lang::CODE_TW => array(
      'COLUMN_ID' => '編號',
      'COLUMN_PARENT_ID' => '上一層',
      'COLUMN_LANG' => '語系',
      'COLUMN_HEADLINE' => '影片分類',
      'COLUMN_SORT' => '排序',
      'COLUMN_STATUS' => '狀態',
      'COLUMN_STATUS_ENABLE' => '啟用',
      'COLUMN_STATUS_DISABLE' => '停用',
      'COLUMN_CREATE_DATE' => '建立日期',
      'COLUMN_MODIFY_DATE' => '最後修改日期',
      'COLUMN_LANG_STATUS' => '語系開關',
      'ADD_SUCCESS' => '分類已經新增',
      'EDIT_SUCCESS' => '分類資料已經儲存',
      'REMOVE_SUCCESS' => '分類已經移除'
    ),
    CM_Lang::CODE_CN => array(
      'COLUMN_ID' => '编号',
      'COLUMN_PARENT_ID' => '上一层',
      'COLUMN_LANG' => '语系',
      'COLUMN_HEADLINE' => '影片分类',
      'COLUMN_SORT' => '排序',
      'COLUMN_STATUS' => '状态',
      'COLUMN_STATUS_ENABLE' => '启用',
      'COLUMN_STATUS_DISABLE' => '停用',
      'COLUMN_CREATE_DATE' => '建立日期',
      'COLUMN_MODIFY_DATE' => '最後修改日期',
      'COLUMN_LANG_STATUS' => '语系开关',
      'ADD_SUCCESS' => '分类已经新增',
      'EDIT_SUCCESS' => '分类资料已经储存',
      'REMOVE_SUCCESS' => '分类已经移除'
    )
  ));
?>