<?php
  CM_Lang::import('DAO_AdBanners', array(
    CM_Lang::CODE_TW => array(
      'COLUMN_ID' => '編號',
      'COLUMN_LANG' => '語系',
      'COLUMN_NAME' => '廣告名稱',
      'COLUMN_BANNER_SECTION' => '版位',
      'COLUMN_BANNER_SECTION_01' => '首頁輪播廣告',
      'COLUMN_BANNER_SECTION_02' => '側邊攔廣告',
      'COLUMN_FILE' => '圖檔',
      'COLUMN_URL' => '連結',
      'COLUMN_TARGET' => '開啓方式',
      'COLUMN_TARGET_1' => '原視窗開啓',
      'COLUMN_TARGET_2' => '另開視窗',
      'COLUMN_SORT' => '排序',
      'COLUMN_STATUS' => '狀態',
      'COLUMN_STATUS_ENABLE' => '啟用',
      'COLUMN_STATUS_DISABLE' => '停用',  
      'COLUMN_DATE_START' => '上架日期',
      'COLUMN_DATE_END' => '下架日期',
      'COLUMN_CREATE_DATE' => '建立時間',
      'COLUMN_MODIFY_DATE' => '最後修改時間',
      'ADD_SUCCESS' => '廣告資料已建立',
      'EDIT_SUCCESS' => '廣告資已儲存',
      'REMOVE_SUCCESS' => '廣告已移除',
      'ERROR_FORM_NOT_FOUND' => '找不到指定的表單'
    ),
    CM_Lang::CODE_CN => array(
      'COLUMN_ID' => '编号',
      'COLUMN_LANG' => '语系',
      'COLUMN_NAME' => '广告名称',
      'COLUMN_BANNER_SECTION' => '版位',
      'COLUMN_BANNER_SECTION_01' => '首页轮播广告',
      'COLUMN_BANNER_SECTION_02' => '侧边拦广告',
      'COLUMN_FILE' => '图档',
      'COLUMN_URL' => '连结',
      'COLUMN_TARGET' => '开啓方式',
      'COLUMN_TARGET_1' => '原视窗开啓',
      'COLUMN_TARGET_2' => '另开视窗',
      'COLUMN_SORT' => '排序',
      'COLUMN_STATUS' => '状态',
      'COLUMN_STATUS_ENABLE' => '启用',
      'COLUMN_STATUS_DISABLE' => '停用', 
      'COLUMN_DATE_START' => '上架日期',
      'COLUMN_DATE_END' => '下架日期',
      'COLUMN_CREATE_DATE' => '建立时间',
      'COLUMN_MODIFY_DATE' => '最後修改时间',
      'ADD_SUCCESS' => '广告资料已建立',
      'EDIT_SUCCESS' => '广告资已储存',
      'REMOVE_SUCCESS' => '广告已移除',
      'ERROR_FORM_NOT_FOUND' => '找不到指定的表单'
    ),
    CM_Lang::CODE_EN => array(
    
    )
  ));
?>