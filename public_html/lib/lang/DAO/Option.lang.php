<?php
  CM_Lang::import('DAO_Option', array(
    CM_Lang::CODE_TW => array(
      'COLUMN_ID' => '編號',
      'COLUMN_CATALOG_ID' => '分類',
      'COLUMN_LANG' => '語系',
      'COLUMN_HEADLINE' => '選項名稱',
      'COLUMN_VAL' => '選項值',
      'COLUMN_FILE' => '附加檔案',
      'COLUMN_NOTE' => '註解',
      'COLUMN_STATUS' => '狀態',
      'COLUMN_STATUS_ENABLE' => '啟用',
      'COLUMN_STATUS_DISABLE' => '停用',
      'COLUMN_CREATE_DATE' => '建立日期',
      'COLUMN_MODIFY_DATE' => '最後修改日期',
      'COLUMN_LANG_STATUS' => '語系開關',
      'ADD_SUCCESS' => '選項已經新增',
      'EDIT_SUCCESS' => '選項資料已經儲存',
      'REMOVE_SUCCESS' => '選項已經移除'
    ),
    CM_Lang::CODE_CN => array(
      'COLUMN_ID' => '编号',
      'COLUMN_CATALOG_ID' => '分类',
      'COLUMN_LANG' => '语系',
      'COLUMN_HEADLINE' => '选项名称',
      'COLUMN_VAL' => '选项值',
      'COLUMN_FILE' => '附加档案',
      'COLUMN_NOTE' => '注解',
      'COLUMN_STATUS' => '状态',
      'COLUMN_STATUS_ENABLE' => '启用',
      'COLUMN_STATUS_DISABLE' => '停用',
      'COLUMN_CREATE_DATE' => '建立日期',
      'COLUMN_MODIFY_DATE' => '最後修改日期',
      'COLUMN_LANG_STATUS' => '语系开关',
      'ADD_SUCCESS' => '选项已经新增',
      'EDIT_SUCCESS' => '选项资料已经储存',
      'REMOVE_SUCCESS' => '选项已经移除'
    ),
    CM_Lang::CODE_EN => array(
    
    )
  ));
?>