<?php
  CM_Lang::import('DAO_AdminLog', array(
    CM_Lang::CODE_TW => array(
      'COLUMN_ID' => '編號',
      'COLUMN_ACCOUNT_ID' => '操作帳號編號',
      'COLUMN_ACCOUNT_NAME' => '操作帳號名稱',
      'COLUMN_REQUEST_NAME' => '動作名稱',
      'COLUMN_MOD' => '模組',
      'COLUMN_REQUEST' => '功能',
      'COLUMN_ACTION' => '動作',
      'COLUMN_HEADLINE' => '操作標題',
      'COLUMN_REQUEST_CONTENT' => '內容',
      'COLUMN_CREATE_DATE' => '操作時間',
      'ADD_SUCCESS' => 'Log資料已建立',
      'EDIT_SUCCESS' => 'Log資已儲存',
      'REMOVE_SUCCESS' => 'Log已移除',
      'ERROR_FORM_NOT_FOUND' => '找不到指定的表單'
    ),
    CM_Lang::CODE_CN => array(
      'COLUMN_ID' => '编号',
      'COLUMN_ACCOUNT_ID' => '操作帐号编号',
      'COLUMN_ACCOUNT_NAME' => '操作帐号名称',
      'COLUMN_REQUEST_NAME' => '动作名称',
      'COLUMN_MOD' => '模组',
      'COLUMN_REQUEST' => '功能',
      'COLUMN_ACTION' => '动作',
      'COLUMN_HEADLINE' => '操作标题',
      'COLUMN_REQUEST_CONTENT' => '内容',
      'COLUMN_CREATE_DATE' => '操作时间',
      'ADD_SUCCESS' => 'Log资料已建立',
      'EDIT_SUCCESS' => 'Log资已储存',
      'REMOVE_SUCCESS' => 'Log已移除',
      'ERROR_FORM_NOT_FOUND' => '找不到指定的表单'
    ),
    CM_Lang::CODE_EN => array(
    
    )
  ));
?>