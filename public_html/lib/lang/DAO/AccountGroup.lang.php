<?php
  CM_Lang::import('DAO_AccountGroup', array(
    CM_Lang::CODE_TW => array(
      'COLUMN_ID' => '角色編號',
      'COLUMN_NAME' => '角色名稱',
      'COLUMN_PERMISSION' => '權限',
      'COLUMN_VIDEO_CATALOG' => '影片分類權限',
      'COLUMN_VIDEO_CATALOG_TIP' => '選擇的分類包含下面所有分類',
      'COLUMN_SOUND_CATALOG' => '聲音分類權限',
      'COLUMN_SOUND_CATALOG_TIP' => '選擇的分類包含下面所有分類',
      'COLUMN_PICTURE_CATALOG' => '相簿分類權限',
      'COLUMN_PICTURE_CATALOG_TIP' => '選擇的分類包含下面所有分類',
      'ADD_SUCCESS' => '角色資料已經新增',
      'EDIT_SUCCESS' => '角色資料已經儲存',
      'REMOVE_SUCCESS' => '角色已移除',
      'ERROR_HAS_ACCOUNT' => '角色&nbsp;<b>%s</b>&nbsp;底下還有&nbsp;<b>%d</b>&nbsp;個帳號，請將這些帳號移除後再刪除這個角色'
    ),
    CM_Lang::CODE_CN => array(
      'COLUMN_ID' => '角色编号',
      'COLUMN_NAME' => '角色名称',
      'COLUMN_PERMISSION' => '权限',
      'COLUMN_VIDEO_CATALOG' => '影片分类权限',
      'COLUMN_VIDEO_CATALOG_TIP' => '选择的分类包含下面所有分类',
      'COLUMN_SOUND_CATALOG' => '声音分类权限',
      'COLUMN_SOUND_CATALOG_TIP' => '选择的分类包含下面所有分类',
      'COLUMN_PICTURE_CATALOG' => '相簿分类权限',
      'COLUMN_PICTURE_CATALOG_TIP' => '选择的分类包含下面所有分类',
      'ADD_SUCCESS' => '角色资料已经新增',
      'EDIT_SUCCESS' => '角色资料已经储存',
      'REMOVE_SUCCESS' => '角色已移除',
      'ERROR_HAS_ACCOUNT' => '角色&nbsp;<b>%s</b>&nbsp;底下还有&nbsp;<b>%d</b>&nbsp;个帐号，请将这些帐号移除後再删除这个角色'
    ),
    CM_Lang::CODE_EN => array(
    
    )
  ));
?>