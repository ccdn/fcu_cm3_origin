<?php
  CM_Lang::import('DAO_News', array(
    CM_Lang::CODE_TW => array(
      'COLUMN_ID' => '編號',
      'COLUMN_CATALOG' => '分類',
      'COLUMN_LANG' => '語系',
      'COLUMN_DATE' => '發佈日期',
      'COLUMN_LINK_TEXT' => '連結文字',
      'COLUMN_LINK_URL' => '連結URL',
      'COLUMN_FILE' => '附件',
      'COLUMN_SORT' => '排序',
      'COLUMN_STATUS' => '狀態',
      'COLUMN_STATUS_ENABLE' => '啟用',
      'COLUMN_STATUS_DISABLE' => '停用',
      'COLUMN_CREATE_DATE' => '建立日期',
      'COLUMN_MODIFY_DATE' => '最後修改日期',
      'COLUMN_HEADLINE' => '標題',
      'COLUMN_DESC' => '內文',
      'COLUMN_START_DATE' => '上架時間',
      'COLUMN_END_DATE' => '下架時間',
      'COLUMN_LANG_STATUS' => '語系開關',
      'ADD_SUCCESS' => '最新消息已經新增',
      'EDIT_SUCCESS' => '最新消息資料已經儲存',
      'REMOVE_SUCCESS' => '最新消息已經移除'
    ),
    CM_Lang::CODE_CN => array(
      'COLUMN_ID' => '编号',
      'COLUMN_CATALOG' => '分类',
      'COLUMN_LANG' => '语系',
      'COLUMN_DATE' => '发布日期',
      'COLUMN_LINK_TEXT' => '连结文字',
      'COLUMN_LINK_URL' => '连结URL',
      'COLUMN_FILE' => '附件',
      'COLUMN_SORT' => '排序',
      'COLUMN_STATUS' => '状态',
      'COLUMN_STATUS_ENABLE' => '启用',
      'COLUMN_STATUS_DISABLE' => '停用',
      'COLUMN_CREATE_DATE' => '建立日期',
      'COLUMN_MODIFY_DATE' => '最後修改日期',
      'COLUMN_HEADLINE' => '标题',
      'COLUMN_DESC' => '内文',
      'COLUMN_START_DATE' => '上架时间',
      'COLUMN_END_DATE' => '下架时间',
      'COLUMN_LANG_STATUS' => '语系开关',
      'ADD_SUCCESS' => '最新消息已经新增',
      'EDIT_SUCCESS' => '最新消息资料已经储存',
      'REMOVE_SUCCESS' => '最新消息已经移除'
    ),
    CM_Lang::CODE_EN => array(
    
    )
  ));
?>