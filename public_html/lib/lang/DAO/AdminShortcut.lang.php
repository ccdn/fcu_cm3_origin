<?php
  CM_Lang::import('DAO_AdminShortcut', array(
    CM_Lang::CODE_TW => array(
      'COLUMN_ID' => '編號',
      'COLUMN_ACCOUNT_ID' => '管理者編號',
      'COLUMN_HEADLINE' => '捷徑名稱',
      'COLUMN_URL' => '連結',
      'COLUMN_SORT' => '排序',
      'COLUMN_CREATE_DATE' => '建立日期',
      'COLUMN_MODIFY_DATE' => '最後修改時間',
      'ADD_SUCCESS' => '捷徑已經新增',
      'EDIT_SUCCESS' => '捷徑資料已經儲存',
      'REMOVE_SUCCESS' => '捷徑已經移除'
    ),
    CM_Lang::CODE_CN => array(
      'COLUMN_ID' => '编号',
      'COLUMN_ACCOUNT_ID' => '管理者编号',
      'COLUMN_HEADLINE' => '捷径名称',
      'COLUMN_URL' => '连结',
      'COLUMN_SORT' => '排序',
      'COLUMN_CREATE_DATE' => '建立日期',
      'COLUMN_MODIFY_DATE' => '最後修改时间',
      'ADD_SUCCESS' => '捷径已经新增',
      'EDIT_SUCCESS' => '捷径资料已经储存',
      'REMOVE_SUCCESS' => '捷径已经移除'
    ),
    CM_Lang::CODE_EN => array(
    
    )
  ));
?>