<?php
  CM_Lang::import('DAO_Keyword', array(
    CM_Lang::CODE_TW => array(
      'COLUMN_ID' => '編號',
      'COLUMN_HEADLINE' => '關鍵字',
      'COLUMN_SEARCH_COUNT' => '搜尋次數',
      'COLUMN_CREATE_DATE' => '建立日期',
      'COLUMN_MODIFY_DATE' => '最後修改日期',
      'ADD_SUCCESS' => '關鍵字資料已經新增',
      'EDIT_SUCCESS' => '關鍵字資料已經儲存',
      'REMOVE_SUCCESS' => '關鍵字資料已經移除'
    ),
    CM_Lang::CODE_CN => array(
      'COLUMN_ID' => '编号',
      'COLUMN_HEADLINE' => '关键字',
      'COLUMN_SEARCH_COUNT' => '搜寻次数',
      'COLUMN_CREATE_DATE' => '建立日期',
      'COLUMN_MODIFY_DATE' => '最後修改日期',
      'ADD_SUCCESS' => '关键字资料已经新增',
      'EDIT_SUCCESS' => '关键字资料已经储存',
      'REMOVE_SUCCESS' => '关键字资料已经移除'
    )
  ));
?>