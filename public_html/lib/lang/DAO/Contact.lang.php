<?php
  CM_Lang::import('DAO_Contact', array(
    CM_Lang::CODE_TW => array(
      'COLUMN_ID' => '編號',
      'COLUMN_NAME' => '姓名',
      'COLUMN_SEX' => '性別',
      'COLUMN_SEX_1' => '男',
      'COLUMN_SEX_2' => '女',
      'COLUMN_EMAIL' => 'E-Mail',
      'COLUMN_PHONE' => '市話',
      'COLUMN_CELLPHONE' => '手機',
      'COLUMN_COMPANY' => '所屬單位',
      'COLUMN_NOTE' => '聯絡內容',
      'COLUMN_IP' => '註冊位址',
      'COLUMN_STATUS' => '狀態',
      'COLUMN_STATUS_1' => '新資料',
      'COLUMN_STATUS_2' => '已閱讀',
      'COLUMN_STATUS_3' => '已聯絡',
      'COLUMN_STATUS_4' => '無效資料',
      'COLUMN_CREATE_DATE' => '建立時間',
      'COLUMN_MODIFY_DATE' => '最後修改時間',
      'ADD_SUCCESS' => '聯絡我們資料已建立',
      'EDIT_SUCCESS' => '聯絡我們資已儲存',
      'REMOVE_SUCCESS' => '聯絡我們已移除',
      'ERROR_FORM_NOT_FOUND' => '找不到指定的表單'
    ),
    CM_Lang::CODE_CN => array(
      'COLUMN_ID' => '编号',
      'COLUMN_NAME' => '姓名',
      'COLUMN_SEX' => '性别',
      'COLUMN_SEX_1' => '男',
      'COLUMN_SEX_2' => '女',
      'COLUMN_EMAIL' => 'E-Mail',
      'COLUMN_PHONE' => '市话',
      'COLUMN_CELLPHONE' => '手机',
      'COLUMN_COMPANY' => '所属单位',
      'COLUMN_NOTE' => '联络内容',
      'COLUMN_IP' => '注册位址',
      'COLUMN_STATUS' => '状态',
      'COLUMN_STATUS_1' => '新资料',
      'COLUMN_STATUS_2' => '已阅读',
      'COLUMN_STATUS_3' => '已联络',
      'COLUMN_STATUS_4' => '无效资料',
      'COLUMN_CREATE_DATE' => '建立时间',
      'COLUMN_MODIFY_DATE' => '最後修改时间',
      'ADD_SUCCESS' => '联络我们资料已建立',
      'EDIT_SUCCESS' => '联络我们资已储存',
      'REMOVE_SUCCESS' => '联络我们已移除',
      'ERROR_FORM_NOT_FOUND' => '找不到指定的表单'
    ),
    CM_Lang::CODE_EN => array(
    
    )
  ));
?>