<?php
  CM_Lang::import('DAO_Customers_Group', array(
    CM_Lang::CODE_TW => array(
      'COLUMN_ID' => '群組編號',
      'COLUMN_NAME' => '群組名稱',
      'COLUMN_PERMISSION' => '無法觀看的版權',
      'COLUMN_CATALOG' => '無法觀看的分類',
      'COLUMN_PERMISSION_TIP' => '沒有選擇代表都可以看',
      'ADD_SUCCESS' => '群組資料已經新增',
      'EDIT_SUCCESS' => '群組資料已經儲存',
      'REMOVE_SUCCESS' => '群組已移除',
      'ERROR_HAS_ACCOUNT' => '群組&nbsp;<b>%s</b>&nbsp;底下還有&nbsp;<b>%d</b>&nbsp;個帳號，請將這些帳號移除後再刪除這個群組'
    ),
    CM_Lang::CODE_CN => array(
      'COLUMN_ID' => '群组编号',
      'COLUMN_NAME' => '群组名称',
      'COLUMN_PERMISSION' => '无法观看的版权',
      'COLUMN_CATALOG' => '无法观看的分类',
      'COLUMN_PERMISSION_TIP' => '没有选择代表都可以看',
      'ADD_SUCCESS' => '群组资料已经新增',
      'EDIT_SUCCESS' => '群组资料已经储存',
      'REMOVE_SUCCESS' => '群组已移除',
      'ERROR_HAS_ACCOUNT' => '群组&nbsp;<b>%s</b>&nbsp;底下还有&nbsp;<b>%d</b>&nbsp;个帐号，请将这些帐号移除後再删除这个群组'
    )
  ));
?>