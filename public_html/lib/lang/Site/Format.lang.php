<?php
  CM_Lang::import('Site_Format', array(
    CM_Lang::CODE_TW => array(
      'PRODUCT_PRICE_LABEL' => '售價',
      'PRODUCT_PRICE_ORG_LABEL' => '建議售價',
      'PRODUCT_PRICE_ERROR' => '暫時缺貨'
    ),
    CM_Lang::CODE_CN => array(
      'PRODUCT_PRICE_LABEL' => '售价',
      'PRODUCT_PRICE_ORG_LABEL' => '建议售价',
      'PRODUCT_PRICE_ERROR' => '暂时缺货'
    )
  ));
?>