<?php
  CM_Lang::import('Admin_Mod_Tree', array(
    CM_Lang::CODE_TW => array(
      'R_LIST' => '分類列表',
      'R_LIST_INTRO' => '檢視分類列表',
      'R_LIST_CATALOG_INTRO' => '檢視分類底下列表',
      'R_LIST_SORT_SAVE_INTRO' => '編輯分類排序',
      'R_ADD' => '新增分類',
      'R_ADD_INTRO' => '新增分類表單界面',
      'R_ADD_SAVE_INTRO' => '分類資料新增',
      'R_EDIT' => '編輯分類',
      'R_EDIT_INTRO' => '編輯分類表單界界面',
      'R_EDIT_SAVE_INTRO' => '分類資料編輯',
      'R_EDIT_L_STATUS_INTRO' => '列表上多個分類狀態變更',
      'R_EDIT_SORT_CHANGE_INTRO' => '分類排序互換',
      'R_REMOVE' => '移除分類',
      'R_REMOVE_INTRO' => '分類資料移除',
      'R_REMOVE_L_INTRO' => '列表上多個分類移除',
      'FORM_GROUP_INFO' => '分類資訊',
      'FORM_GROUP_SETTING' => '分類設定',
      'ADD_INTRO' => '新增分類資料',
      'EDIT_INTRO' => '修改分類資料',
      'EDIT_L_SUCCESS' => '您所選擇的分類資料已更新',
      'REMOVE_SUCCESS' => '分類已移除',
      'REMOVE_L_SUCCESS' => '您所選擇的分類已移除',
      'REMOVE_CONFIRM' => '確定要移除分類&nbsp;<b>%s</b>&nbsp;嗎?',
      'TREE_FUNC_ADD' => '新增',
      'TREE_FUNC_ENABLE' => '啟用',
      'TREE_FUNC_DISABLE' => '停用',
      'TREE_FUNC_EDIT' => '修改',
      'TREE_FUNC_REMOVE' => '移除',
      'TREE_FUNC_OPEN_ALL' => '全部展開',
      'TREE_FUNC_CLOSE_ALL' => '全部摺疊',
      'TREE_FUNC_CHECKED_ENABLE' => '啟用選擇的項目',
      'TREE_FUNC_CHECKED_DISABLE' => '停用選擇的項目',
      'TREE_FUNC_CHECKED_REMOVE' => '移除選擇的項目',
      'BTN_REMOVE_SEARCH' => '移除搜尋結果'
    ),
    CM_Lang::CODE_CN => array(
      'R_LIST' => '分类列表',
      'R_LIST_INTRO' => '检视分类列表',
      'R_LIST_CATALOG_INTRO' => '检视分类底下列表',
      'R_LIST_SORT_SAVE_INTRO' => '编辑分类排序',
      'R_ADD' => '新增分类',
      'R_ADD_INTRO' => '新增分类表单界面',
      'R_ADD_SAVE_INTRO' => '分类资料新增',
      'R_EDIT' => '编辑分类',
      'R_EDIT_INTRO' => '编辑分类表单界界面',
      'R_EDIT_SAVE_INTRO' => '分类资料编辑',
      'R_EDIT_L_STATUS_INTRO' => '列表上多个分类状态变更',
      'R_EDIT_SORT_CHANGE_INTRO' => '分类排序互换',
      'R_REMOVE' => '移除分类',
      'R_REMOVE_INTRO' => '分类资料移除',
      'R_REMOVE_L_INTRO' => '列表上多个分类移除',
      'FORM_GROUP_INFO' => '分类资讯',
      'FORM_GROUP_SETTING' => '分类设定',
      'ADD_INTRO' => '新增分类资料',
      'EDIT_INTRO' => '修改分类资料',
      'EDIT_L_SUCCESS' => '您所选择的分类资料已更新',
      'REMOVE_SUCCESS' => '分类已移除',
      'REMOVE_L_SUCCESS' => '您所选择的分类已移除',
      'REMOVE_CONFIRM' => '确定要移除分类&nbsp;<b>%s</b>&nbsp;吗?',
      'TREE_FUNC_ADD' => '新增',
      'TREE_FUNC_ENABLE' => '启用',
      'TREE_FUNC_DISABLE' => '停用',
      'TREE_FUNC_EDIT' => '修改',
      'TREE_FUNC_REMOVE' => '移除',
      'TREE_FUNC_OPEN_ALL' => '全部展开',
      'TREE_FUNC_CLOSE_ALL' => '全部摺叠',
      'TREE_FUNC_CHECKED_ENABLE' => '启用选择的项目',
      'TREE_FUNC_CHECKED_DISABLE' => '停用选择的项目',
      'TREE_FUNC_CHECKED_REMOVE' => '移除选择的项目',
      'BTN_REMOVE_SEARCH' => '移除搜寻结果'
    )
  ));
?>