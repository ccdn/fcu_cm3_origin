<?php
  CM_Lang::import('CM', array(
    CM_Lang::CODE_TW => array(
      'SITE_NAME' => 'ContentManager Ver3.0(Beta)',
      'OPTIONS_NOT_FOUND' => '無',
      'CAPTCHA_LABEL' => '驗證圖片',
      'CAPTCHA_IMG_DESC' => '請輸入圖片中問題的答案',
      'CAPTCHA_ANS_LABEL' => '答案',
      'CAPTCHA_ANS_PLACEHOLDER' => '請輸入上圖問題的答案',
      'CAPTCHA_VALIDATE_ERROR' => '您輸入的驗證答案有誤',
      'ERROR_DATA_NOT_FOUND' => '找不到指定的資料',
      'ERROR_MESSAGE' => '系統發生錯誤, 請聯絡開發人員',
      'ERROR_MESSAGE_ALL' => '錯誤訊息: %s<br />發生錯誤的檔案:&nbsp;%s&nbsp;在&nbsp;<b>%s</b>&nbsp;行',
      'ERROR_UPLOAD' => '上傳檔案錯誤',
      'ERROR_UPLOAD_EMPTY' => '沒有檔案上傳',
      'ERROR_UPLOAD_PATH' => '上傳路徑無法寫入',
      'ERROR_UPLOAD_SIZE' => '上傳的檔案超過 %d Bytes',
      'ERROR_UPLOAD_NO_COMPLETE' => '上傳的檔案僅被部分上傳',
      'ERROR_UPLOAD_NO_FILE' => '沒有檔案上傳',
      'ERROR_UPLOAD_NOT_FOUND' => '上傳的檔案不合法',
      'ERROR_UPLOAD_EXT' => '不允許的副檔名',
      'ERROR_UPLOAD_COPY' => '複製檔案失敗',
      'ERROR_COLUMN_VALUE_EMPTY' => '%s 沒有輸入',
      'ERROR_PAYMENT_NOT_FOUND' => '找不到指定的付款模組',
      'ERROR_DB_ERROR' => '資料發生錯誤，請稍後再試'
    ),
    CM_Lang::CODE_CN => array(
      'SITE_NAME' => 'ContentManager Ver3.0(Beta)',
      'OPTIONS_NOT_FOUND' => '无',
      'CAPTCHA_LABEL' => '验证图片',
      'CAPTCHA_IMG_DESC' => '请输入图片中问题的答案',
      'CAPTCHA_ANS_LABEL' => '答案',
      'CAPTCHA_ANS_PLACEHOLDER' => '请输入上图问题的答案',
      'CAPTCHA_VALIDATE_ERROR' => '您输入的验证答案有误',
      'ERROR_DATA_NOT_FOUND' => '找不到指定的资料',
      'ERROR_MESSAGE' => '系统发生错误, 请联络开发人员',
      'ERROR_MESSAGE_ALL' => '错误讯息: %s<br />发生错误的档案:&nbsp;%s&nbsp;在&nbsp;<b>%s</b>&nbsp;行',
      'ERROR_UPLOAD' => '上传档案错误',
      'ERROR_UPLOAD_EMPTY' => '没有档案上传',
      'ERROR_UPLOAD_PATH' => '上传路径无法写入',
      'ERROR_UPLOAD_SIZE' => '上传的档案超过 %d Bytes',
      'ERROR_UPLOAD_NO_COMPLETE' => '上传的档案仅被部分上传',
      'ERROR_UPLOAD_NO_FILE' => '没有档案上传',
      'ERROR_UPLOAD_NOT_FOUND' => '上传的档案不合法',
      'ERROR_UPLOAD_EXT' => '不允许的副档名',
      'ERROR_UPLOAD_COPY' => '复制档案失败',
      'ERROR_COLUMN_VALUE_EMPTY' => '%s 没有输入',
      'ERROR_PAYMENT_NOT_FOUND' => '找不到指定的付款模组',
      'ERROR_DB_ERROR' => '资料发生错误，请稍後再试'
    ),
    CM_Lang::CODE_EN => array(
    
    )
  ));
?>